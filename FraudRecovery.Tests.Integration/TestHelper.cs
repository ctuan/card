﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FraudRecovery.Tests.Integration
{
	public static class TestHelper
	{
		public readonly static string[] TestSvCardNumbers = { "7777083002943493", "7777083002953493", "7777083002964149" };
		public readonly static string[] TestSvCardPins = { "33151451", "25567528", "56387208" };
		public const string UnitedStatesTippingMarketId = "99030169997";
		public const string USMid = "99910439997";
		//protected const string USMid = "99030169997";

		public const string TestOrderId = "D1B80M50V2394L7TQ6GFAQAQ31";

		public const string StoreId = "00303";
		public const string TerminalId = "0001";

		public const string TestTransactionSvcCardNumber = "7777079661318020";
		public const int TestTransactionCardId = 80194856;

		public static readonly int[] TestCardId =
			{
				80196719,
				80196743,
				80196718,
				80196717,
				80196712,
				80196714,
				80196715,
				80196716,
			};


		public static string Domain
		{
			get { return ConfigurationManager.AppSettings["ApiDomain"]; }
		}

		public static string TransactionRevokeUrl(this string id)
		{
			return string.Format("{0}/cards/transaction/{1}/revoke", Domain, id);
		}
		public static string TransactionRefundUrl(this string id)
		{
			return string.Format("{0}/cards/transaction/{1}/refund", Domain, id);
		}
		public static string TransactionFreezeUrl(this string id)
		{
			return string.Format("{0}/cards/transaction/{1}/freeze", Domain, id);
		}
		public static string TransactionUnfresszeUrl(this string id)
		{
			return string.Format("{0}/cards/transaction/{1}/unfreeze", Domain, id);
		}


		public static string CardBalanceUrl(this string id)
		{
			return string.Format("{0}/cards/{1}/card-balance", Domain, id);
		}

		public static string CardRevokeUrl(this string id)
		{
			return string.Format("{0}/cards/{1}/revoke-amount", Domain, id);
		}

		public static string CardCashOutUrl(this string id)
		{
			return string.Format("{0}/cards/{1}/cash-out", Domain, id);
		}

		public static string CardFreezeUrl(this string id)
		{
			return string.Format("{0}/cards/{1}/freeze", Domain, id);
		}

		public static string CardUnfreezeUrl(this string id)
		{
			return string.Format("{0}/cards/{1}/unfreeze", Domain, id);
		}

		public static string LoadCardUrl(this string id)
		{
			return string.Format("{0}/cards/{1}/load-amount", Domain, id);
		}
	}
}
