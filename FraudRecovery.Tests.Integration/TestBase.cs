﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.Api.Sdk.WebRequestWrapper;
using Starbucks.FraudRecovery.Common.Models;

namespace FraudRecovery.Tests.Integration
{
	public class TestBase
	{
		protected const decimal TestInitBalance = 97.5m;
        protected int TestTimeout = 3000;

		protected string CardIdRelatedTestSetup(IWebRequestWrapper client, int cardId)
		{
			string cardIdStr = cardId.ToString();

			var result = QueryCardBalance(client, cardIdStr);
			decimal balance = result.Result.EndingBalance;

			if (result.Result.LockAmount > 0)
			{
				VerifyUnfreezeCard(client, cardIdStr, result.Result.LockAmount);
			}

			if (balance != 0)
			{
				VerifyRedeemCard(client, cardIdStr, balance);
				VerifyCardBalance(client, cardIdStr, 0);
			}


			VerifyReloadCard(client, cardIdStr, TestInitBalance);
			VerifyCardBalance(client, cardIdStr, TestInitBalance);

			return cardIdStr;
		}

		public void VerifyReloadCard(IWebRequestWrapper client, string cardId, decimal amount)
		{
			string url = cardId.LoadCardUrl();
            var raw = client.Post<decimal, SvcCardLoadAmountResponse>(url, amount, TestTimeout);
            Assert.IsTrue(raw != null);

            var response = raw.Result;
			Assert.IsTrue(response != null && response.Name == "CardLoadAmount" && response.Status == OperationStatus.Succeeded);
			Assert.IsTrue(response.Result != null && response.Result.Amount == amount);
		}


		public void VerifyRedeemCard(IWebRequestWrapper client, string cardId, decimal amount)
		{
			string url = cardId.CardRevokeUrl();
            var raw = client.Post<decimal, SvcCardRevokeAmountResponse>(url, amount, TestTimeout);
            Assert.IsTrue(raw != null);

            var response = raw.Result;
			Assert.IsTrue(response != null && response.Name == "CardRevokeAmount" && response.Status == OperationStatus.Succeeded);
			Assert.IsTrue(response.Result != null && response.Result.Amount == amount);
		}

		protected void VerifyCashOut(IWebRequestWrapper client, string cardId, decimal expectedAmount)
		{
			string url = cardId.CardCashOutUrl();
            var raw = client.Post<SvcCardCashOutResponse>(url, TestTimeout);
            Assert.IsTrue(raw != null);

            var response = raw.Result;
			Assert.IsTrue(response != null && response.Name == "CardCashOut" && response.Status == OperationStatus.Succeeded);
			Assert.IsTrue(response.Result != null && response.Result.Amount == expectedAmount);
		}


		protected decimal GetCardBalance(IWebRequestWrapper client, string cardId)
		{
			try
			{
				var response = QueryCardBalance(client, cardId);
				return response.Result.EndingBalance;
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.ToString());
				throw;
			}

		}

		protected SvcCardGetBalanceResponse QueryCardBalance(IWebRequestWrapper client, string cardId)
		{
			try
			{
				string url = cardId.CardBalanceUrl();
                var raw = client.Get<SvcCardGetBalanceResponse>(url, TestTimeout);
                Assert.IsTrue(raw != null);

                var response = raw.Result;
				Assert.IsTrue(response != null && response.Name == "CardGetBalance" && response.Status == OperationStatus.Succeeded);
				Assert.IsTrue(response.Result != null);

				return response;
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.ToString());
				throw;
			}

		}
		
		protected decimal VerifyCardBalance(IWebRequestWrapper client, string cardId, decimal expectedAmount)
		{
			decimal balance = GetCardBalance(client, cardId);
			Assert.IsTrue( balance == expectedAmount);
			return balance;
		}

		protected void VerifyFreezeCard(IWebRequestWrapper client, string cardId, decimal amount)
		{
			string url = cardId.CardFreezeUrl();

            var raw = client.Post<SvcCardFreezeResponse>(url, TestTimeout);
            Assert.IsTrue(raw != null);

            var response = raw.Result;
			Assert.IsTrue(response != null && response.Status == OperationStatus.Succeeded);
			Assert.IsTrue(response.Name == "CardFreeze");
			Assert.IsTrue(response.Reason == null && response.Result != null);
			Assert.IsTrue(response.Result.LockAmount == amount);
		}


		protected void VerifyUnfreezeCard(IWebRequestWrapper client, string cardId, decimal amount)
		{
			string url = cardId.CardUnfreezeUrl();

            var raw = client.Post<SvcCardUnfreezeResponse>(url, TestTimeout);
            Assert.IsTrue(raw != null);

            var response = raw.Result;
			Assert.IsTrue(response != null && response.Name == "CardUnfreeze" && response.Status == OperationStatus.Succeeded);
			Assert.IsTrue(response.Reason == null && response.Result != null);
			Assert.IsTrue(response.Result.LockAmount == 0 && response.Result.EndingBalance == amount);
		}
	}

}
