﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.Api.Sdk.WebRequestWrapper;
using Starbucks.FraudRecovery.Common.Models;

namespace FraudRecovery.Tests.Integration
{
	[TestClass]
	public class FraudRecoveryApiTests : TestBase
	{
		#region transaction related tests

		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_Transaction_Revoke_Happen()
		{
			string transactionId = TestHelper.TestOrderId;
			string url = transactionId.TransactionRevokeUrl();

			var client = new WebRequestWrapper();

			var raw = client.Post<TransactionRevokeResponse>(url);
            Assert.IsTrue(raw != null);

            var response = raw.Result;
			Assert.IsTrue(response != null && response.Name == "TransactionRevoke" && response.Status == OperationStatus.Succeeded);
			Assert.IsTrue(response.Results != null && response.Results.Count == 1);
			Assert.IsTrue(response.Results[0].Status == OperationStatus.Succeeded);
		}


		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_Transaction_Refund_Happen()
		{
			string transactionId = TestHelper.TestOrderId;

			string url = transactionId.TransactionRefundUrl();

			var client = new WebRequestWrapper();
			string cardId = CardIdRelatedTestSetup(client, TestHelper.TestTransactionCardId);

			var raw = client.Post<TransactionRefundResponse>(url);
            Assert.IsTrue(raw != null);

            var response = raw.Result;
			Assert.IsTrue(response != null && response.Name == "TransactionRefund" && response.Status == OperationStatus.Succeeded);
			Assert.IsTrue(response.Result != null);
			Assert.IsTrue(!string.IsNullOrWhiteSpace(response.Result.Amount) && response.Result.Amount == "10.00");
			//Assert.IsTrue(!string.IsNullOrWhiteSpace(response.Result.DateTime));
			Assert.IsTrue(!string.IsNullOrWhiteSpace(response.Result.PaymentGatewayId));
			Assert.IsTrue(!string.IsNullOrWhiteSpace(response.Result.Decision));
			Assert.IsTrue(response.Result.Decision.Equals("Accept", StringComparison.OrdinalIgnoreCase));
		}

		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_Transaction_Freeze_Happen()
		{
			var client = new WebRequestWrapper();
			string cardId = CardIdRelatedTestSetup(client, TestHelper.TestTransactionCardId);

			string transactionId = TestHelper.TestOrderId;
			string url = transactionId.TransactionFreezeUrl();

			var raw = client.Post<TransactionFreezeResponse>(url);
            Assert.IsTrue(raw != null);
            var response = raw.Result;
			Assert.IsTrue(response != null && response.Name == "TransactionFreeze" && response.Status == OperationStatus.Succeeded);
			Assert.IsTrue(response.Results != null && response.Results.Count == 1);
			Assert.IsTrue(response.FailureCount == 0 && response.TotalOperations == 1);
			Assert.IsTrue(response.Results[0].Status == OperationStatus.Succeeded);
			Assert.IsTrue(response.Results[0].Reason == null && response.Results[0].Result != null);

			Assert.IsTrue(response.Results[0].Result.LockAmount == TestInitBalance);
			Assert.IsTrue(response.Results[0].Result.EndingBalance == TestInitBalance);
		}


		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_Transaction_UnFreeze_Happen()
		{
			var client = new WebRequestWrapper();
			string cardId = CardIdRelatedTestSetup(client, TestHelper.TestTransactionCardId);

			string transactionId = TestHelper.TestOrderId;
			string url = transactionId.TransactionFreezeUrl();

			var raw = client.Post<TransactionFreezeResponse>(url);
            Assert.IsTrue(raw != null);

            var freeze = raw.Result;
			Assert.IsTrue(freeze != null && freeze.Name == "TransactionFreeze" && freeze.Status == OperationStatus.Succeeded);

			url = transactionId.TransactionUnfresszeUrl();
			var rawResponse = client.Post<TransactionUnfreezeResponse>(url);
            Assert.IsTrue(rawResponse != null);

            var response = rawResponse.Result;
			Assert.IsTrue(response != null && response.Name == "TransactionUnfreeze" && response.Status == OperationStatus.Succeeded);
			Assert.IsTrue(response.Results != null && response.Results.Count == 1);
			Assert.IsTrue(response.FailureCount == 0 && response.TotalOperations == 1);
			Assert.IsTrue(response.Results[0].Status == OperationStatus.Succeeded);
			Assert.IsTrue(response.Results[0].Reason == null && response.Results[0].Result != null);

			Assert.IsTrue(response.Results[0].Result.LockAmount == 0);
			Assert.IsTrue(response.Results[0].Result.EndingBalance == TestInitBalance);
		}

		#endregion

		#region card related tests

		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_Card_Balance()
		{
			var client = new WebRequestWrapper();
			string cardId = CardIdRelatedTestSetup(client, TestHelper.TestCardId[0]);

			decimal balance = GetCardBalance(client, cardId);
			Assert.IsTrue(balance == TestInitBalance);
		}

		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_Card_Revoke()
		{
			var client = new WebRequestWrapper();
			string cardId = CardIdRelatedTestSetup(client, TestHelper.TestCardId[0]);

			decimal revokeAmount = 5.1m;

			VerifyRedeemCard(client, cardId, revokeAmount);
			VerifyCardBalance(client, cardId, TestInitBalance - revokeAmount);
		}

		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		[Ignore]
		public void FraudRecovery_Card_CashOut()
		{
			var client = new WebRequestWrapper();
			string cardId = CardIdRelatedTestSetup(client, TestHelper.TestCardId[0]);

			VerifyCashOut(client,cardId, TestInitBalance);
			VerifyCardBalance(client, cardId, 0);
		}

		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_Card_Freeze()
		{
			var client = new WebRequestWrapper();
			string cardId = CardIdRelatedTestSetup(client, TestHelper.TestCardId[3]);
			
			VerifyFreezeCard(client, cardId, TestInitBalance);

			VerifyUnfreezeCard(client, cardId, TestInitBalance);
		}

		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_Card_UnFreeze()
		{
			var client = new WebRequestWrapper();
			string cardId = CardIdRelatedTestSetup(client, TestHelper.TestCardId[3]);

			VerifyFreezeCard(client, cardId, TestInitBalance);
			VerifyUnfreezeCard(client, cardId, TestInitBalance);

			VerifyCardBalance(client, cardId, TestInitBalance);
		}


		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_Card_LoadCard()
		{
			var client = new WebRequestWrapper();
			string cardId = CardIdRelatedTestSetup(client, TestHelper.TestCardId[0]);
			
			decimal reloadAmount = 5.5m;
			VerifyReloadCard(client, cardId, reloadAmount);
			VerifyCardBalance(client, cardId, reloadAmount + TestInitBalance);
		}

		#endregion


	}
}
