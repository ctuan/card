@echo OFF

if [%1] == [local] goto setlocalNugetServer
if [%1] == [global] goto setglobalNugetServer

::set nuGetPushOptions=-ApiKey 734823ED-7DD5-4AB5-8172-A297D09D1551 -src "http://mstmp00678.sbweb.prod"
set nuGetPushOptions=-ApiKey 734823ED-7DD5-4AB5-8172-A297D09D1551 -src "http://nuget.starbucks.net"
set nugetFolder="%solutionFolder%\.nuget"
goto endOfArgs

:setlocalNugetServer
set nugetFolder="%solutionFolder%\.nuget"
set nuGetPushOptions=-ApiKey 734823ED-7DD5-4AB5-8172-A297D09D1551 -src "http://nuget.starbucks.net"
goto endOfArgs

:setglobalNugetServer
set nugetFolder="C:\bin"
set nuGetPushOptions=-ApiKey 6842BDE0-D22C-4764-8B89-705604891896 -src "http://nuget.starbucks.net"

:endOfArgs

:: This must be run from the solution folder.
set solutionFolder=%CD%

:: Use date /t and time /t from command line to get format of your date and
:: time; change substring below as needed.

:: This will create a timestamp like yyyy-mm-yy-hh-mm-ss.
set TIMESTAMP=%DATE:~10,4%-%DATE:~4,2%-%DATE:~7,2%-%TIME:~0,2%-%TIME:~3,2%-%TIME:~6,2%

:: Create new directory
set outputDirectory="%solutionFolder%\CardPackages\%TIMESTAMP%"
md %outputDirectory%

set nugetPackOptions=-Symbols -IncludeReferencedProjects -OutputDirectory %outputDirectory%

:: Build Solution

msbuild /t:Rebuild Card.WebApi.sln

:: Pack the projects

::
::  CardIssuer
::
cd %solutionFolder%\Providers\CardIssuer\LogCounter.Provider.Common
%nugetFolder%\NuGet.exe pack LogCounter.Provider.Common.csproj %nugetPackOptions%

cd %solutionFolder%\Providers\CardIssuer\CardIssuer.Dal.Common
%nugetFolder%\NuGet.exe pack CardIssuer.Dal.Common.csproj %nugetPackOptions%

cd %solutionFolder%\Providers\CardIssuer\CardIssuer.Dal.SvDot.Common
%nugetFolder%\NuGet.exe pack CardIssuer.Dal.SvDot.Common.csproj %nugetPackOptions%

cd %solutionFolder%\Providers\CardIssuer\LogCounter.Provider
%nugetFolder%\NuGet.exe pack LogCounter.Provider.csproj %nugetPackOptions%

cd %solutionFolder%\Providers\CardIssuer\CardIssuer.Dal.SvDot
%nugetFolder%\NuGet.exe pack CardIssuer.Dal.SvDot.csproj %nugetPackOptions%

::
::  CardProvider
::
cd /d %solutionFolder%\Providers\CardProvider\Card.Provider.Common
%nugetFolder%\NuGet.exe pack Card.Provider.Common.csproj %nugetPackOptions%

cd /d %solutionFolder%\Providers\CardProvider\Card.Dal.Common
%nugetFolder%\NuGet.exe pack Card.Dal.Common.csproj %nugetPackOptions%

cd /d %solutionFolder%\Providers\CardProvider\Card.Dal.Sql
%nugetFolder%\NuGet.exe pack Card.Dal.Sql.csproj %nugetPackOptions%

cd /d %solutionFolder%\Providers\CardProvider\Card.Dal.Sql.EntLibLess
%nugetFolder%\NuGet.exe pack Card.Dal.Sql.EntLibLess.csproj %nugetPackOptions%

cd /d %solutionFolder%\Providers\CardProvider\Card.Provider
%nugetFolder%\NuGet.exe pack Card.Provider.csproj %nugetPackOptions%

::
:: CardTransaction
::

cd %solutionFolder%\Providers\CardTransaction\SecurePayment.Provider.Common
%nugetFolder%\NuGet.exe pack SecurePayment.Provider.Common.csproj %nugetPackOptions%

cd %solutionFolder%\Providers\CardTransaction\SecurePayPalPayment.Provider.Common
%nugetFolder%\NuGet.exe pack SecurePayPalPayment.Provider.Common.csproj %nugetPackOptions%

cd %solutionFolder%\Providers\CardTransaction\CardTransaction.Provider.Common
%nugetFolder%\NuGet.exe pack CardTransaction.Provider.Common.csproj %nugetPackOptions%

cd %solutionFolder%\Providers\CardTransaction\SecurePayPalPayment.Provider
%nugetFolder%\NuGet.exe pack SecurePayPalPayment.Provider.csproj %nugetPackOptions%

cd %solutionFolder%\Providers\CardTransaction\CardTransaction.Provider
%nugetFolder%\NuGet.exe pack CardTransaction.Provider.csproj %nugetPackOptions%

::
:: Fraud Reconvery projects
::

cd /d %solutionFolder%\Providers\FraudRecovery\FraudRecovery.Dal.Common
%nugetFolder%\NuGet.exe pack FraudRecovery.Dal.Common.csproj %nugetPackOptions%

cd /d %solutionFolder%\Providers\FraudRecovery\FraudRecovery.Dal
%nugetFolder%\NuGet.exe pack FraudRecovery.Dal.csproj %nugetPackOptions%

cd /d %solutionFolder%\Providers\FraudRecovery\FraudRecovery.Common
%nugetFolder%\NuGet.exe pack FraudRecovery.Common.csproj %nugetPackOptions%

cd /d %solutionFolder%\Providers\FraudRecovery\FraudRecovery.Provider
%nugetFolder%\NuGet.exe pack FraudRecovery.Provider.csproj %nugetPackOptions%
@echo.

:: Publish the packages

cd /d %outputDirectory%
%nugetFolder%\NuGet.exe push *.nupkg %nugetPushOptions%

cd /d %solutionFolder%