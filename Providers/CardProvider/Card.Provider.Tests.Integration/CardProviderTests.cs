﻿using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Starbucks.Card.Dal.Sql;
using Starbucks.Card.Dal.Sql.Configuration;
using Starbucks.Card.Provider;
using Starbucks.Card.Provider.Common.Enums;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.Card.Provider.Configuration;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.LogCounter.Provider.Common;
using Starbucks.MessageBroker.Common;
using Starbucks.OpenApi.Logging.Common;

namespace Card.Provider.Tests.Integration
{
    [TestClass]
    public class CardProviderTests
    {
        private CardDal _dal;
        private CardProvider _provider;

        // msharp - this test is woefully out of date, I'll resume fixing it later.
        [Ignore]
        [TestMethod]
        public void Can_Get_Card()
        {
            string userId = "2DE4F092-4408-4EE2-A3AA-FB45F85277F8";
            string cardId = "8C6772F092D810A9";
            IAssociatedCard card = _provider.GetCardById(userId, cardId, VisibilityLevel.Decrypted, false, "US", "US");
            Assert.IsNotNull(card, "Card should not be null");
        }

        [TestInitialize]
        public void Setup()
        {
            var section = ConfigurationManager.GetSection("cardDalSettings") as CardDalSqlSettingsSection;
            var settingsProvider = new Starbucks.Settings.Provider.SettingsProvider();
            _dal = new CardDal(section, settingsProvider);

            ICardIssuerDal cardIssueDal = GetCardIssuerDal();
            ILogCounterManager lcmnger = null;
            IMessageBroker mb = null;
            CardProviderSettingsSection settings = ConfigurationManager.GetSection("cardProviderSec") as CardProviderSettingsSection;
            ILogRepository lr = null;
            IMessageListener ml = null;
            _provider = new CardProvider(_dal, cardIssueDal, lcmnger, mb, settingsProvider, settings, lr, ml, null, null, null, null, null);
        }

        private ICardIssuerDal GetCardIssuerDal()
        {
            var mi = new Mock<IMerchantInfo>();
            mi.SetupGet(x => x.SubMarketId).Returns("US");

            var mock = new Mock<ICardIssuerDal>();
            mock.Setup(x => x.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(mi.Object);

            return mock.Object;
        }
    }
}