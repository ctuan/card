﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.Card.Dal.Sql.EntLibLess;
using Starbucks.Card.Dal.Sql.EntLibLess.Configuration;
using Starbucks.Card.Provider;
using Starbucks.Card.Provider.Common;
using Starbucks.Card.Provider.Common.Exceptions;
using Starbucks.Platform.Security;

namespace Card.Provider.UnitTest
{
    [TestClass]
    public class SelectedCardProviderTests
    {
        private const string ClientId = "qwerty";

        private readonly string _anotherUserCard = Encryption.EncryptCardId(972);
        private readonly int[] _cardIds = new[] { 80220633, 80220634 };
        private ISelectedCardProvider _provider;
        private string _userId = "BA966072-8E56-445C-97FE-7018A8B6A81B";

        private string[] CardIds
        {
            get { return _cardIds.Select(Encryption.EncryptCardId).ToArray(); }
        }

        private IEnumerable<string> AllTestCards
        {
            get
            {
                var allCards = new List<string>(CardIds) { _anotherUserCard };
                return allCards.ToArray();
            }
        }


        [TestInitialize]
        public void Init()
        {
            var config =
                ConfigurationManager.GetSection("cardDalEntLibLessSqlSettingsSection") as
                CardDalEntLibLessSqlSettingsSection;
            var selectedCardDal =
                new SelectedCardDal(config);
            _provider = new SelectedCardProvider(selectedCardDal);
        }

        [TestMethod]
        public void CanInsertExistingUserAndCard()
        {
            var cards = new[] { CardIds.First() };

            _provider.RemoveSelectedCards(_userId, ClientId, CardIds);
            _provider.AssociateSelectedCardsWithClient(_userId, ClientId, cards);
            string[] crds = _provider.GetSelectedCards(_userId, ClientId).ToArray();
            Assert.IsTrue(crds.Count() == 1);
            Assert.AreEqual(crds.First(), CardIds.First());
        }

        [TestMethod]
        public void CanNotInsertNonExisitngCards()
        {
            var cards = new[] { Encryption.EncryptCardId(888888888), Encryption.EncryptCardId(999999999) };

            try
            {
                _provider.AssociateSelectedCardsWithClient(_userId, ClientId, cards);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void CanNotInsertIfCardDoesNotBelongToUser()
        {
            var cards = new[] { _anotherUserCard };

            try
            {
                _provider.AssociateSelectedCardsWithClient(_userId, ClientId, cards);
                string[] cardList = _provider.GetSelectedCards(_userId, ClientId).ToArray();
                Assert.IsTrue(cardList.All(c => c != _anotherUserCard));
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void CanNotInsertNonExisitngUser()
        {
            _userId = Guid.NewGuid().ToString().Substring(32);

            try
            {
                _provider.AssociateSelectedCardsWithClient(_userId, ClientId, CardIds);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void CanInsertValidRecords()
        {
            try
            {
                _provider.AssociateSelectedCardsWithClient(_userId, ClientId, CardIds);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void CanDisconnectOneSelectedCard()
        {
            CanInsertValidRecords();

            var cards = new[] { CardIds.First() };

            try
            {
                _provider.RemoveSelectedCards(_userId, ClientId, cards);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void CanDisconnectSelectedCards()
        {
            CanInsertValidRecords();

            try
            {
                _provider.RemoveSelectedCards(_userId, ClientId, CardIds);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void CanGetSelectedCards()
        {
            CanInsertValidRecords();
            string[] cards = _provider.GetSelectedCards(_userId, ClientId).ToArray();

            Assert.IsTrue(cards.Length > 0 && (cards.Any(c => c == CardIds[0]) || cards.Any(c => c == CardIds[1])));
        }

        [TestMethod]
        public void CanGetSelectedCardsByUserId()
        {
            CanInsertValidRecords();
            var cards = _provider.GetSelectedCards(_userId).ToArray();

            Assert.IsTrue(cards.Length > 0 && (cards.Any(c => c.CardId == CardIds[0]) || cards.Any(c => c.CardId == CardIds[1])));
        }

        [TestMethod]
        public void CanUpsertSelectedCards()
        {
            _provider.RemoveSelectedCards(_userId, ClientId, AllTestCards);
            string lastCard = CardIds.Last();
            string firstCard = CardIds.First();
            _provider.AssociateSelectedCardsWithClient(_userId, ClientId, new[] { lastCard });

            _provider.AssociateSelectedCardsWithClient(_userId, ClientId, new[] { firstCard });
            string[] cards = _provider.GetSelectedCards(_userId, ClientId).ToArray();
            Assert.IsTrue(cards.Count() == 1 && cards.All(c => c == firstCard));
        }

        [TestMethod]
        public void CanUpsertMultipleSelectedCards()
        {
            _provider.RemoveSelectedCards(_userId, ClientId, AllTestCards);
            string lastCard = CardIds.Last();
            _provider.AssociateSelectedCardsWithClient(_userId, ClientId, new[] { lastCard });

            _provider.AssociateSelectedCardsWithClient(_userId, ClientId, CardIds);
            string[] cards = _provider.GetSelectedCards(_userId, ClientId).ToArray();
            Assert.IsTrue(cards.All(c => CardIds.Contains(c)));
        }

        [TestMethod]
        public void CanVerifyIfCardLinked()
        {
            _provider.AssociateSelectedCardsWithClient(_userId, ClientId, CardIds);
            Assert.IsTrue(_provider.IsCardLinked(_userId, CardIds.First()));
        }

        [TestMethod]
        [ExpectedException(typeof(CardProviderValidationException))]
        public void UserClientCardValidation_ProperlyThrows()
        {
            _provider.AssociateSelectedCardsWithClient(null, ClientId, CardIds);
            _provider.AssociateSelectedCardsWithClient(_userId, null, CardIds);
            _provider.AssociateSelectedCardsWithClient(_userId, ClientId, null);
        }
    }
}