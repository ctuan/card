﻿using System;
using System.Collections.Generic;
using Starbucks.Card.Dal.Common.Models;
using Starbucks.Card.Provider.Common.Models;
using IAssociatedCard = Starbucks.Card.Provider.Common.Models.IAssociatedCard;
using IAutoReloadProfile = Starbucks.Card.Dal.Common.Models.IAutoReloadProfile;
using ICard = Starbucks.Card.Provider.Common.Models.ICard;
using IFraudCheckCardData = Starbucks.Card.Dal.Common.Models.IFraudCheckCardData;
using IFraudCheckUserData = Starbucks.Card.Dal.Common.Models.IFraudCheckUserData;
using Starbucks.PaymentMethod.Provider.Common.Models;

namespace Starbucks.Card.Dal.Common.Interfaces
{
    public interface ICardDal
    {
        IAssociatedCard GetCardByNumber(string cardNumber);
        IAssociatedCard GetCard(string cardId);
        IAssociatedCard GetCard(string userId, string cardId, bool includeAssociatedCards = false);
        bool LoadCardAssociation(IAssociatedCard card, string userId = null);
        IEnumerable<IAssociatedCard> GetAssociatedCards(string userId);
        string UpsertCard(ICard card);
        void AddCardAssociation(string userId, string cardId, IStarbucksCardHeader cardHeader, bool registered);
        bool IsCardNumberAlreadyRegistered(string cardNumber);
        IAutoReloadProfile GetAutoReload(string userId, string autoReloadId);
        string UpdateAutoReload(IAutoReloadProfile autoReloadProfile);
        string SetupAutoReload(IAutoReloadProfile autoReloadProfile);
        void UpsertAutoReloadBilling(string userId, string autoReloadId, string paymentMethodId);
        ICardBalance GetBalanceByNumber(string cardNumber);
        ICardBalance GetBalanceByCardId(string cardId);
        int UpsertCardBalanceByDecryptedNumber(string cardNumber, decimal balance, DateTime balanceDate,
                                               string currency);
        int UpsertCardBalance(string cardId, decimal balance, DateTime balanceDate, string currency);
        //IEnumerable<IStarbucksCardImage> GetCardImages(string cardNumber);
        int UpsertCardTransactionHistory(ICardTransaction cardTransaction);
        int UpsertCardTransactionHistory(string cardNumber, ICardTransaction cardTransaction);
        IProgramPromoCode GetPromoCodeByCardNumber(string cardNumber);
        bool UpdateUserRegistrationSource(string userId, string registrationSource);
        bool UpdateUserRegistrationSource(string userId, ICardRegistrationSource cardRegSource=null);
        bool IsMarketingRegSourceCodeValid(string marketingRegSourceCode);
        bool IsPlatformRegSourceCodeValid(string platformRegSourceCode);
        bool IsPromoCodeActive(string promoCode, string subMarket);
        IPromotionProperties GetPromoSettings(string promoCode);
        string GetPromoCode(string subMarket);
        void UpdateAutoReloadStatus(string userId, string autoReloadId, int status);
        void DisableAutoReload(string userId, string autoReloadId, DateTime disableUntilDate);
        bool IsAutoReloadAssociatedToPaymentMethod(string userId, string autoReloadId);
        void LogAutoReloadStatus(string userId, string cardId, string autoReloadId, string autoReloadStatus, string message);
        void UpdateCardAssociation(string userId, string cardId, bool? isRegistered, bool? isDefault, string nickname);
        void UpdateCardNickName(string userId, string cardId, string nickname);
        void UnregisterCard(string userId, string cardId);
        void DeleteCardAssociation(string userId, string cardId);
        string GetCardIdByCardNumber(string cardNumber);
        string GetAutoReloadId(string userId, string cardId);

        bool GetEmailPreference(string userId);

        bool IsCardAssociatedWithUser(string userId, int unencryptedCardId);
        string GetCardNumberById(string decryptedCardId);
        IFraudCheckUserData GetUserDataForFraudCheck(string userId);
        IFraudCheckCardData GetCardDataForFraudCheck(string userId, string cardId);
        IUserAccount GetUserAccount(string userId);
        long ClearPaymentMethodSurrogateAndPaypalAccountNumbers(string userId, IPaymentMethod payment);
    }
}
