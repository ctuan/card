﻿using System.Collections.Generic;
using Starbucks.Card.Dal.Common.Models;

namespace Starbucks.Card.Dal.Common.Interfaces
{
    public interface ISelectedCardDal
    {
        IEnumerable<string> GetSelectedCards(string userId, string clientId);
        IEnumerable<ISelectedCard> GetSelectedCards(string userId);
        void AssociateSelectedCardsWithClient(string userId, string clientId, IEnumerable<string> cardIds);
        void RemoveSelectedCards(string userId, string clientId, IEnumerable<string> cardIds);
        bool IsCardLinked(string userId, string cardId);
    }
}