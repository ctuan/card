﻿namespace Starbucks.Card.Dal.Common
{
    public enum PaymentType
    {
        None = 0,
        Visa = 1,
        MasterCard = 2,
        Amex = 3,
        Discover = 4,
        ACH = 5,
        PayPal = 6,
        LostUS = 7,
        LostCAN = 8,
        LostGB = 9,
        Costcenter = 10
    }

    public enum PaymentTypeGroup
    {
        None = 0,
        CreditCard = 1,
        Checking = 2,
        Lost = 3,
        Internal = 4,
        SvcCard = 5
    }

    public enum AutoReloadStatus
    {
        Inactive = 0,
        Active = 1,
        Suspended = 2,
        Disabled = 3,
        Temporary = 4
    };
}
