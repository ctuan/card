﻿namespace Starbucks.Card.Dal.Common.Models
{
    public interface ISelectedCard
    {
        string UserId { get; set; }
        string ClientId { get; set; }
        string CardId { get; set; }
    }
}