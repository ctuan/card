﻿using System;

namespace Starbucks.Card.Dal.Common.Models
{
    public interface IAutoReloadProfile
    {
        string UserId { get; set; }
        string CardId { get; set; }
        string AutoReloadType { get; set; }
        int? Day { get; set; }
        decimal? TriggerAmount { get; set; }
        decimal Amount { get; set; }
        string PaymentType { get; set; }
        string PaymentMethodId { get; set; }
        string AutoReloadId { get; set; }
        int Status { get; set; }
        DateTime? DisableUntilDate { get; set; }
        DateTime? StoppedDate { get; set; }
        string BillingAgreementId { get; set; }
        DateTime? CreatedDate { get; set; }
        DateTime? LastChangedDate { get; set; }
    }
}
