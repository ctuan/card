﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.Card.Dal.Common.Models
{
    public interface IFraudCheckCardData
    {
        string Nickname { get; set; }
        bool IsDefaultSvcCard { get; set; }
        int TotalCardsRegistered { get; set; }
    }
}
