﻿using System;

namespace Starbucks.Card.Dal.Common.Models
{
    public interface ICardBalance
    {
        int CardBalanceId { get; set; }
        string CardId { get; set; }
        decimal? Balance { get; set; }
        DateTime? BalanceDate { get; set; }
        string Currency { get; set; }
    }
}
