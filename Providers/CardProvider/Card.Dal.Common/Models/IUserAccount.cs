﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.Card.Dal.Common.Models
{
    public interface IUserAccount
    {
        /// <summary>
        /// The GUID identifying the user account
        /// </summary>
        string UserId { get; set; }

        /// <summary>
        /// The user name on the account for log-in purposes
        /// </summary>
        /// <value>The name of the user.</value>
        string UserName { get; set; }

        /// <summary>
        /// The user's password for log-in purposes
        /// </summary>
        /// <value>The password.</value>
        string Password { get; set; }

        /// <summary>
        /// The first name of the user
        /// </summary>
        string FirstName { get; set; }

        /// <summary>
        /// The last name of the user
        /// </summary>
        string LastName { get; set; }

        /// <summary>
        /// The email address of user
        /// </summary>
        string EmailAddress { get; set; }

        /// <summary>
        /// The user's secret question for validating identity, such as when restting a forgotten password.
        /// </summary>
        string SecretQuestion { get; set; }

        /// <summary>
        /// The answer to the user's secret question
        /// </summary>
        string SecretAnswer { get; set; }

        /// <summary>
        /// The user account status
        /// </summary>
        //AccountStatus Status { get; set; }

        /// <summary>
        /// Identifies how the account was created.
        /// </summary>
        //AccountCreationSource CreationSource { get; set; }

        /// <summary>
        /// Starbucks partner number
        /// </summary>
        string PartnerNumber { get; set; }

        /// <summary>
        /// Telecomm entries from the telecomm table
        /// </summary>
       // IEnumerable<ITelecomm> TelecommEntries { get; set; }

        /// <summary>
        /// The submarket of the user
        /// </summary>
        string SubMarket { get; set; }
    }
}
