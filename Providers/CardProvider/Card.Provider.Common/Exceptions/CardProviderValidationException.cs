﻿using System;

namespace Starbucks.Card.Provider.Common.Exceptions
{    
    public class CardProviderValidationException : Exception
    {

        public string Code { get; private set; }
        public CardProviderValidationException(string code, string message)
            : base(message)
        {
            Code = code;
        }

     
    }
}
