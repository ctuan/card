﻿using System;

namespace Starbucks.Card.Provider.Common.Exceptions
{
    public class CardProviderException : Exception
    {
        private readonly string _code;       

        public string Code
        {
            get { return _code; }
        }      

        public CardProviderException(string code, string message)
            : base(message)
        {
            _code = code;
        }

        public CardProviderException(string code, string message, Exception innerException)
            : base(message, innerException)
        {
            _code = code;            
        }
    }
}
