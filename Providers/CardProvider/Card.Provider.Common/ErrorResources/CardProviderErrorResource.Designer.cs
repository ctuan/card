﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Starbucks.Card.Provider.Common.ErrorResources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class CardProviderErrorResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal CardProviderErrorResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Starbucks.Card.Provider.Common.ErrorResources.CardProviderErrorResource", typeof(CardProviderErrorResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        public static string AutoReloadNotAssociatedToPaymentMethodCode {
            get {
                return ResourceManager.GetString("AutoReloadNotAssociatedToPaymentMethodCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot enable AutoReload because there is no associated payment method..
        /// </summary>
        public static string AutoReloadNotAssociatedToPaymentMethodMessage {
            get {
                return ResourceManager.GetString("AutoReloadNotAssociatedToPaymentMethodMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 2.
        /// </summary>
        public static string AutoReloadNotFoundCode {
            get {
                return ResourceManager.GetString("AutoReloadNotFoundCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Auto reload not found.
        /// </summary>
        public static string AutoReloadNotFoundMessage {
            get {
                return ResourceManager.GetString("AutoReloadNotFoundMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 16.
        /// </summary>
        public static string CannotAutoReloadMarketCode {
            get {
                return ResourceManager.GetString("CannotAutoReloadMarketCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot AutoReload due to market.
        /// </summary>
        public static string CannotAutoReloadMarketMessage {
            get {
                return ResourceManager.GetString("CannotAutoReloadMarketMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 14.
        /// </summary>
        public static string CannotRegisterInvalidCardClassCode {
            get {
                return ResourceManager.GetString("CannotRegisterInvalidCardClassCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot register card class.
        /// </summary>
        public static string CannotRegisterInvalidCardClassMessage {
            get {
                return ResourceManager.GetString("CannotRegisterInvalidCardClassMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 6.
        /// </summary>
        public static string CannotRegisterInvalidCardNumberCode {
            get {
                return ResourceManager.GetString("CannotRegisterInvalidCardNumberCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot register card since card number is not valid.
        /// </summary>
        public static string CannotRegisterInvalidCardNumberMessage {
            get {
                return ResourceManager.GetString("CannotRegisterInvalidCardNumberMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 17.
        /// </summary>
        public static string CannotTransferAfterReloadCode {
            get {
                return ResourceManager.GetString("CannotTransferAfterReloadCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot transfer after reload..
        /// </summary>
        public static string CannotTransferAfterReloadMessage {
            get {
                return ResourceManager.GetString("CannotTransferAfterReloadMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 12.
        /// </summary>
        public static string CannotTransferFromCardClassCode {
            get {
                return ResourceManager.GetString("CannotTransferFromCardClassCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot transfer from card class.
        /// </summary>
        public static string CannotTransferFromCardClassMessage {
            get {
                return ResourceManager.GetString("CannotTransferFromCardClassMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 15.
        /// </summary>
        public static string CannotTransferMarketCode {
            get {
                return ResourceManager.GetString("CannotTransferMarketCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot transfer due to market.
        /// </summary>
        public static string CannotTransferMarketMessage {
            get {
                return ResourceManager.GetString("CannotTransferMarketMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 13.
        /// </summary>
        public static string CannotTransferToCardClassCode {
            get {
                return ResourceManager.GetString("CannotTransferToCardClassCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot transfer to card class.
        /// </summary>
        public static string CannotTransferToCardClassMessage {
            get {
                return ResourceManager.GetString("CannotTransferToCardClassMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3.
        /// </summary>
        public static string CannotUnregisterDuettoCardCode {
            get {
                return ResourceManager.GetString("CannotUnregisterDuettoCardCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot unregister duetto card.
        /// </summary>
        public static string CannotUnregisterDuettoCardMessage {
            get {
                return ResourceManager.GetString("CannotUnregisterDuettoCardMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 11.
        /// </summary>
        public static string CardIsAlreadyRegisteredCode {
            get {
                return ResourceManager.GetString("CardIsAlreadyRegisteredCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Card is already registered.
        /// </summary>
        public static string CardIsAlreadyRegisteredMessage {
            get {
                return ResourceManager.GetString("CardIsAlreadyRegisteredMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1001.
        /// </summary>
        public static string CardNotFoundCode {
            get {
                return ResourceManager.GetString("CardNotFoundCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Card not found.
        /// </summary>
        public static string CardNotFoundMessage {
            get {
                return ResourceManager.GetString("CardNotFoundMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 9.
        /// </summary>
        public static string CardNotRegisteredToUserCode {
            get {
                return ResourceManager.GetString("CardNotRegisteredToUserCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Card is not registered to user.
        /// </summary>
        public static string CardNotRegisteredToUserMessage {
            get {
                return ResourceManager.GetString("CardNotRegisteredToUserMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 10.
        /// </summary>
        public static string InsufficientFundsForTransactionCode {
            get {
                return ResourceManager.GetString("InsufficientFundsForTransactionCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid funds for transaction message.
        /// </summary>
        public static string InsufficientFundsForTransactionMessage {
            get {
                return ResourceManager.GetString("InsufficientFundsForTransactionMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 18.
        /// </summary>
        public static string InvalidAmountCode {
            get {
                return ResourceManager.GetString("InvalidAmountCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid amount.
        /// </summary>
        public static string InvalidAmountMessage {
            get {
                return ResourceManager.GetString("InvalidAmountMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 8.
        /// </summary>
        public static string InvalidCardNumberCode {
            get {
                return ResourceManager.GetString("InvalidCardNumberCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid card number.
        /// </summary>
        public static string InvalidCardNumberMessage {
            get {
                return ResourceManager.GetString("InvalidCardNumberMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 19.
        /// </summary>
        public static string InvalidCardTransferCode {
            get {
                return ResourceManager.GetString("InvalidCardTransferCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid card transfer.
        /// </summary>
        public static string InvalidCardTransferMessage {
            get {
                return ResourceManager.GetString("InvalidCardTransferMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 7.
        /// </summary>
        public static string InvalidPinCode {
            get {
                return ResourceManager.GetString("InvalidPinCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid pin.
        /// </summary>
        public static string InvalidPinMessage {
            get {
                return ResourceManager.GetString("InvalidPinMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1002.
        /// </summary>
        public static string InvalidSubMarketCode {
            get {
                return ResourceManager.GetString("InvalidSubMarketCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid card market.
        /// </summary>
        public static string InvalidSubMarketMessage {
            get {
                return ResourceManager.GetString("InvalidSubMarketMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 20.
        /// </summary>
        public static string RequestDeniedCode {
            get {
                return ResourceManager.GetString("RequestDeniedCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Transfer denied by Accertify.
        /// </summary>
        public static string RequestDeniedMessage {
            get {
                return ResourceManager.GetString("RequestDeniedMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request not permitted by this account.
        /// </summary>
        public static string RequestNotPermittedByThisAccountMessage {
            get {
                return ResourceManager.GetString("RequestNotPermittedByThisAccountMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to Find the Promotion Code for the Specified Submarket.
        /// </summary>
        public static string UnableToFindPromoCode {
            get {
                return ResourceManager.GetString("UnableToFindPromoCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1003.
        /// </summary>
        public static string UnableToFindPromoCodeMessage {
            get {
                return ResourceManager.GetString("UnableToFindPromoCodeMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 4.
        /// </summary>
        public static string UnknownErrorCode {
            get {
                return ResourceManager.GetString("UnknownErrorCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An unknown internal error has occured.
        /// </summary>
        public static string UnknownErrorMessage {
            get {
                return ResourceManager.GetString("UnknownErrorMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 5.
        /// </summary>
        public static string UnregisterNonZeroBalanceCode {
            get {
                return ResourceManager.GetString("UnregisterNonZeroBalanceCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot unregiser a digital card that has a balance greater than zero..
        /// </summary>
        public static string UnregisterNonZeroBalanceMessage {
            get {
                return ResourceManager.GetString("UnregisterNonZeroBalanceMessage", resourceCulture);
            }
        }
    }
}
