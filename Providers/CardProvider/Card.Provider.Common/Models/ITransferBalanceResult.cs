﻿using System.Collections.Generic;

namespace Starbucks.Card.Provider.Common.Models
{
    public interface ITransferBalanceResult
    {
        IStarbucksCardTransaction StarbucksCardTransaction { get; set; }

        IDictionary< string, decimal> Balances { get; set; } 
    }
}
