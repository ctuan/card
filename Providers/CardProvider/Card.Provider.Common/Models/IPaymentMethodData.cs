﻿namespace Starbucks.Card.Provider.Common.Models
{
    public interface  IPaymentMethodData
    {
        string Cvn { get; set; }
        string PaymentMethodType { get; set; }
        string PaymentMethodId { get; set; }
        string BillingAddressId { get; set; }
    }
}
