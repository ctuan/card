﻿using System;
using System.Collections.Generic;

namespace Starbucks.Card.Provider.Common.Models
{
    public interface IPromotionProperties
    {      
        string UserId { get; set; }     
        string CardId { get; set; }        
        string Submarket { get; set; }        
        string PromoUrl { get; set; }
        string PromoCode { get; set; }        
        bool PinRequired { get; set; }        
        string CardRange { get; set; }        
        DateTime EndDate { get; set; }        
        string CountryCodes { get; set; }
        List<IPromoFeature> PromoFeatures { get; set; }
    }

}
