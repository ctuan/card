﻿using System;

namespace Starbucks.Card.Provider.Common.Models
{
    public interface IStarbucksCardBalance
    {
        string CardId { get; set; }
        decimal? Balance { get; set; }
        DateTime? BalanceDate { get; set; }
        string BalanceCurrencyCode { get; set; }
        string CardNumber { get; set; }
    }
}
