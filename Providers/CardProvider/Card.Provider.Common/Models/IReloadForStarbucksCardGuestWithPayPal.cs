﻿namespace Starbucks.Card.Provider.Common.Models
{
    public interface IReloadForStarbucksCardGuestWithPayPal : IReloadForStarbucksCardGuest 
    {
        string EmailAddress { get; set; }
    }
}
