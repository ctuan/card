﻿namespace Starbucks.Card.Provider.Common.Models
{
    public interface IRiskWithoutIovation
    {
        string Platform { get; set; }
        string Market { get; set; }
        bool? IsLoggedIn { get; set; }
        string CcAgentName { get; set; }
    }

    public interface IRisk : IRiskWithoutIovation
    {
        IReputation IovationFields { get; set; }
    }

    public interface IReputation
    {
        string IpAddress { get; set; }
        string DeviceFingerprint { get; set; }
    }
}
