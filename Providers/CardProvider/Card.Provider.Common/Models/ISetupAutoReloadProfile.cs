﻿namespace Starbucks.Card.Provider.Common.Models
{
    public interface ISetupAutoReloadProfile : IBaseAutoReloadProfile 
    {        
        string PayPalEcToken { get; set; }
    }
}
