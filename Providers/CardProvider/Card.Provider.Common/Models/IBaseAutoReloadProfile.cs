﻿namespace Starbucks.Card.Provider.Common.Models
{
    public  interface IBaseAutoReloadProfile
    {
        string UserId { get; set; }
        string CardId { get; set; }
        string AutoReloadType { get; set; }
        int? Day { get; set; }
        decimal? TriggerAmount { get; set; }
        decimal Amount { get; set; }
        string PaymentType { get; set; }
        string PaymentMethodId { get; set; }
    }
}
