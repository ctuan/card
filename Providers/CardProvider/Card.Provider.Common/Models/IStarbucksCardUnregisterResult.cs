﻿namespace Starbucks.Card.Provider.Common.Models
{
    public interface IStarbucksCardUnregisterResult
    {
        string CardId { get; set; }
    }
}
