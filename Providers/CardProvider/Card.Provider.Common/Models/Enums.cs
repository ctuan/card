﻿namespace Starbucks.Card.Provider.Common.Models
{
    public enum RegistrationResult
    {
        SuccessfulRegistration = 1,  //Card was successfully registered to the user.
        CardAlreadyRegistered = 2,  //Card was already registered.
        InvalidCardNumber = 3,  //Card number provided was invalid.
        InvalidPin = 4,  //Card PIN provided was invalid or did not match PIN on card.
        Error = 5  //Other error.

    }
}
