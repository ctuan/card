﻿namespace Starbucks.Card.Provider.Common.Models
{
    public interface IActivateAndReloadCardResonse
    {        
        ICardTransaction CardActivationTransaction { get; set; }        
        ICardTransaction CardReloadTransaction { get; set; }
    }
}
