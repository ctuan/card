﻿
namespace Starbucks.Card.Provider.Common.Models
{
    public interface ICardRegistrationSource
    {
        string Platform { get; set; }
        string Marketing { get; set; }
    }
}
