﻿namespace Starbucks.Card.Provider.Common.Models
{
    public interface ISelectedCard
    {
        string UserId { get; set; }
        string ClientId { get; set; }
        string CardId { get; set; }
    }
}