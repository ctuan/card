﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.Card.Provider.Common.Models
{
    public interface IFraudCheckUserData
    {
        string UserId { get; set; }

        Int32 AgeOfAccountSinceCreation { get; set; }

        bool IsPartner { get; set; }

        int BirthDay { get; set; }

        int BirthMonth { get; set; }

        bool eMailSignUp { get; set; }

        bool MailSignUp { get; set; }

        bool TextMessageSignUp { get; set; }

        bool TwitterSignUp { get; set; }

        bool FacebookSignUp { get; set; }
    }
}
