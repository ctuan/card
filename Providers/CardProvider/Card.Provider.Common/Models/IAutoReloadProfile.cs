﻿using System;

namespace Starbucks.Card.Provider.Common.Models
{
    public interface IAutoReloadProfile : IBaseAutoReloadProfile 
    {
        string AutoReloadId { get; set; }

        int Status { get; set; }
        
        DateTime? DisableUntilDate { get; set; }

        DateTime? StoppedDate { get; set; }

        string BillingAgreementId { get; set; }
    }
}
