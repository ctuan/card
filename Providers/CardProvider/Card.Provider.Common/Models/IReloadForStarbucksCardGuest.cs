﻿namespace Starbucks.Card.Provider.Common.Models
{
    public interface IReloadForStarbucksCardGuest
    {
        decimal Amount { get; set; }
        string PaymentMethodId { get; set; }
        string SessionId { get; set; }
    }
}
