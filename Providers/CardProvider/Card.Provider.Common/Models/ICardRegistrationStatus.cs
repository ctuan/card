﻿using System;

namespace Starbucks.Card.Provider.Common.Models
{
    public interface ICardRegistrationStatus
    {
        string CardId { get; set; }
        string CardNumber { get; set; }
        bool Successful { get; set; }
        string Code { get; set; }
        string Message { get; set; }
        Type ExceptionType { get; set; } 
    }
}
