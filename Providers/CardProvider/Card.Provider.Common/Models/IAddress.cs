﻿namespace Starbucks.Card.Provider.Common.Models
{
    public interface IAddress
    {
        /// <summary>
        /// Line 1 of the address
        /// </summary>
        string AddressLine1 { get; set; }

        /// <summary>
        /// Line 2 of the address
        /// </summary>
        string AddressLine2 { get; set; }

        /// <summary>
        /// The address city
        /// </summary>
        string City { get; set; }

        /// <summary>
        /// The subdivision within the country, such as a state, province or region
        /// </summary>
        string CountrySubdivision { get; set; }

        /// <summary>
        /// The long description for subdivision within the country, such as a state, province or region
        /// </summary>
        string CountrySubdivisionDescription { get; set; }

        /// <summary>
        /// The postal code
        /// </summary>
        string PostalCode { get; set; }

        /// <summary>
        /// The country
        /// </summary>
        string Country { get; set; }

        /// <summary>
        /// Count name
        /// </summary>
        string CountryName { get; set; }
    }
}
