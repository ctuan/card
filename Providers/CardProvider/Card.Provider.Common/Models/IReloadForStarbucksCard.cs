﻿namespace Starbucks.Card.Provider.Common.Models
{
    public interface IReloadForStarbucksCard
    {        
        string Type { get; set; }        
        decimal Amount { get; set; }        
        string PaymentMethodId { get; set; }
        string SessionId { get; set; }      
    }
}
