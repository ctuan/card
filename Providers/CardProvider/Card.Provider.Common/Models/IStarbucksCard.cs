﻿using System;
using System.Collections.Generic;

namespace Starbucks.Card.Provider.Common.Models
{
    public interface IStarbucksCard
    {
        string CardId { get; set; }
        string CardNumber { get; set; }
        string Nickname { get; set; }
        string CardClass { get; set; }
        string CardType { get; set; }
        string CurrencyCode { get; set; }
        string SubmarketCode { get; set; }
        decimal? Balance { get; set; }
        DateTime? BalanceDate { get; set; }
        string BalanceCurrencyCode { get; set; }
        //string ImageIconUrl { get; set; }
        //string ImageUrl { get; set; }
        IEnumerable<IStarbucksCardImage> CardImages { get; set; } 
        string RegisteredUserId { get; set; }
        bool? IsDefaultCard { get; set; }
        bool? IsPartnerCard { get; set; }
        IAutoReloadProfile AutoReloadProfile { get; set; }
        bool? IsDigitalCard { get; set; }
        IEnumerable<string> PassbookSerialNumbers { get; set; }
        string AutoReloadId { get; set; }
        DateTime? RegistrationDate { get; set; }
    }
}
