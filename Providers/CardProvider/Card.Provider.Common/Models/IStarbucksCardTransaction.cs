﻿using System;

namespace Starbucks.Card.Provider.Common.Models
{
    public interface IStarbucksCardTransaction
    {
        string CardId { get; set; }
        string TransactionId { get; set; }
        decimal Amount { get; set; }
        string CurrencyCode { get; set; }
        string Description { get; set; }
        DateTime TransactionDate { get; set; }       
    }
}
