﻿namespace Starbucks.Card.Provider.Common.Models
{
    public interface IProgramPromoCode
    {
        string ProgramId { get; set; }
        string Name { get; set; }
        string PromoCode { get; set; }
        string MarketingRegistrationSource { get; set; }
    }
}
