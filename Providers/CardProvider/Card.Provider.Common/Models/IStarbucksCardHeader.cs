﻿namespace Starbucks.Card.Provider.Common.Models
{
    public interface IStarbucksCardHeader
    {
        string CardNumber { get; set; }
        string CardPin { get; set; }
        string Nickname { get; set; }
        string Submarket { get; set; }
        bool Primary { get; set; }
        bool Register { get; set; }
        ICardRegistrationSource RegistrationSource { get; set; }
        IRiskWithoutIovation Risk { get; set; }
    }
}
