﻿using System;

namespace Starbucks.Card.Provider.Common.Models
{
    public interface IGuestStarbucksCardBalance
    {
        string CardNumber { get; set; }
        decimal? Balance { get; set; }
        DateTime? BalanceDate { get; set; }
        string BalanceCurrencyCode { get; set; }
    }
}
