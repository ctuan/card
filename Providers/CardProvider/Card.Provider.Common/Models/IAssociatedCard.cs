﻿using System;


namespace Starbucks.Card.Provider.Common.Models
{

    public interface IAssociatedCard :  ICard 
    {
        string AutoReloadId { get; set; }
        DateTime? RegistrationDate { get; set; }
        string RegistrationSource { get; set; }
      
        string RegisteredUserId { get; set; }
        string Nickname { get; set; }
        bool IsDefault { get; set; }
        bool IsOwner { get; set; }
        string PlatformRegSourceCode { get; set; }
        string MarketingRegSourceCode { get; set; }

        IAutoReloadProfile AutoReloadProfile { get; set; }        
    }
}
