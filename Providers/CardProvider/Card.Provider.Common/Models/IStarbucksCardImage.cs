﻿using Starbucks.Card.Provider.Common.Enums;

namespace Starbucks.Card.Provider.Common.Models
{
    
    public interface IStarbucksCardImage
    {
        CardImageType Type { get; set; }
        string Uri { get; set; }
        //string ImageIcon { get; set; }
        //string ImageSmall { get; set; }
        //string ImageMedium { get; set; }
        //string ImageLarge { get; set; }
        //// mobile
        //string ImageIconMobile { get; set; }
        //string ImageSmallMobile { get; set; }
        //string ImageMediumMobile { get; set; }
        //string ImageLargeMobile { get; set; }
    }
}
