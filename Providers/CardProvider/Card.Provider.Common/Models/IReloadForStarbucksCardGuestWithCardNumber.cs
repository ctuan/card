﻿namespace Starbucks.Card.Provider.Common.Models
{
    public interface IReloadForStarbucksCardGuestWithCardNumber : IReloadForStarbucksCardGuest
    {
        string EmailAddress { get; set; }
        string FirstSix { get; set; }
        string LastFour { get; set; }
        string CardNumber { get; set; }
    }
}
