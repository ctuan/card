﻿using System;
using System.Collections.Generic;

namespace Starbucks.Card.Provider.Common.Models
{
    public enum CardType
    { 
        None = 0,     
        Standard = 1,     
        Duetto = 2,     
        Gold = 3,     
        Tier3
    }
    
    public enum DuettoType
    {
        None = 0,
        Chase = 1,
        Rbc = 2
    }
    public interface ICard
    {
        bool Active { get; set; }      
        decimal? Balance { get; set; }
        DateTime? BalanceDate { get; set; }
        string BalanceCurrency { get; set; }
        string CardId { get; set; }
        CardType Type { get; set; }
        string Class { get; set; }
        string Currency { get; set; }
        DateTime? ExpirationDate { get; set; }
        string Name { get; set; }
        string Number { get; set; }
        string Pin { get; set; }
        bool PinValidated { get; set; }
        string BatchCode { get; set; }
        string SubMarketCode { get; set; }
        int? CardRangeId { get; set; }
        bool IsPartner { get; set; }
        IEnumerable<IStarbucksCardImage> CardImages {get;}
        bool IsDigitalCard { get; set; }
        IEnumerable<string> Actions { get; set; } 

    }
}
