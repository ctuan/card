namespace Starbucks.Card.Provider.Common.Models
{
    public interface IStarbucksCardStatus
    {
        string CardId { get; set; }
        bool Valid { get; set; }
    }
}
