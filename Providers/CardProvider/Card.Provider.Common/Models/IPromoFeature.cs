﻿namespace Starbucks.Card.Provider.Common.Models
{
    public interface IPromoFeature
    {      
        string Name { get; set; }     
        string Value { get; set; }
        string Description { get; set; }
    }
}
