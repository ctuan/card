﻿namespace Starbucks.Card.Provider.Common.Enums
{
    public enum VisibilityLevel
    { None = 0, //no pin or cardnumber
        Encrypted, //encrypted pin and cardnumber
        Masked, //masked card number and NO pin
        Decrypted //decrypted card and pin
    }
}
