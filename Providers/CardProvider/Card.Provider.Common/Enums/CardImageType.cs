﻿namespace Starbucks.Card.Provider.Common.Enums
{
    public enum CardImageType
    {
        ImageIcon,
        ImageSmall,
        ImageMedium,
        ImageLarge,
        iosThumb,
        iosThumbHighRes,
        iosLarge,
        iosLargeHighRes,
        iosImageStripMedium,
        iosImageStripLarge,
        androidThumbMdpi,
        androidThumbHdpi,
        androidThumbXhdpi,
        androidThumbXxhdpi,
        androidFullMdpi,
        androidFullHdpi,
        androidFullXhdpi,
        androidFullXxhdpi,
        ImageStrip
    }
}
