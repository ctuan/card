﻿using System;
using System.Collections.Generic;
using Starbucks.Card.Provider.Common.Enums;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.PaymentMethod.Provider.Common.Models;

namespace Starbucks.Card.Provider.Common
{


    public interface ICardProvider
    {
        #region GetCards
        ICard GetCardByNumberAndPin(string cardNumber, string pin, VisibilityLevel visibilityLevel, string market);

        IAssociatedCard GetCardById(string userId, string cardId, VisibilityLevel visibilityLevel,
                                    bool includeAutoReload, string market, string userMarket, bool includeAssociatedCards = false, string platform = "unknown");

        //IAssociatedCard GetAssociatedCard(string cardNumber, string market);
        IEnumerable<IStarbucksCardImage> GetStarbucksCardImageUrlByCardNumber(string cardNumber);
        IEnumerable<IAssociatedCard> GetCards(string userId, VisibilityLevel visibilityLevel, bool includeAutoReload, bool includeRealTimeBalance, string market, string userMarket);
        IAssociatedCard GetPrimaryCard(string userId, VisibilityLevel visibilityLevel, bool includeAutoReload, string market, string userMarket);
        IEnumerable<IStarbucksCardStatus> GetStatusesForUserCards(string userId, IEnumerable<string> cardIds);
        string GetCardIdByCardNumber(string cardNumber);
        #endregion

        #region Balance
        IStarbucksCardBalance GetCardBalance(string userId, string cardId, string market, string userMarket);
        IStarbucksCardBalance GetCardBalanceByCardNumberPinNumber(string cardNumber, string pin, string market);
        IStarbucksCardBalance GetCardBalanceRealTime(string userId, string cardId, string market, string userMarket);
        IStarbucksCardBalance GetCardBalanceByCardNumberPinNumberRealTime(string cardNumber, string pin, string market);
        #endregion

        #region Registration
        string RegisterStarbucksCard(string userId, IStarbucksCardHeader cardHeader, string userMarket);
        string ActivateAndRegisterCard(string userId, string userMarket, IStarbucksCardHeader cardHeader);
        IEnumerable<ICardRegistrationStatus> RegisterMultipleCards(string userId,
                                                                   IEnumerable<IStarbucksCardHeader> cardHeader,
                                                                   string userMarket);
        void AddCardAssociation(string userId, string cardId, IStarbucksCardHeader cardHeader, string market);
        IAssociatedCard GetCardById(string userId, string cardId, string cardNumber, string market, string userMarket);
        #endregion

        #region AutoReload
        IAutoReloadProfile SetupAutoReload(string userId, string cardId, IAutoReloadProfile autoReloadProfile, string market, string email = null, IRisk risk = null);
        IAutoReloadProfile UpdateAutoReload(string userId, string cardId, IAutoReloadProfile autoReloadProfile, string market, string email = null, IRisk risk = null);
        void EnableAutoReload(string userId, string cardId, string market, IRisk risk = null);
        void DisableAutoReload(string userId, string cardId, string market, IRisk risk = null);
        #endregion

        #region ExternalProviderHelpers
        int UpsertCardBalanceByDecryptedNumber(string cardNumber, decimal balance, DateTime balanceDate, string currency);
        int UpsertCardBalance(string cardId, decimal balance, DateTime balanceDate, string currency);

        int UpsertCardTransactionHistory(ICardTransaction cardTransaction);
        int UpsertCardTransactionHistory(string cardNumber, ICardTransaction cardTransaction);
        #endregion

        #region UpdateCard
        void UpdateCardNickname(string userId, string cardId, string nickname);
        void UpdateDefaultCard(string userId, string cardId);
        #endregion

        #region Unregister
        bool UnregisterCard(string userId, string cardId, bool keepInLibrary, string userMarket, IRisk risk = null);
        #endregion

        #region Transfer
        IEnumerable<IStarbucksCardBalance> TransferRegistered(string userId, string sourceCardId, string targetCardId, decimal? amount, string userMarket, string email = null, IRisk risk = null);
        //IEnumerable<IStarbucksCardBalance> TransferRegisteredCardBalanceToAssociatedCard(string userId, string sourceCardId,
        //                                                                  string destinationCardId);
        //IEnumerable<IStarbucksCardBalance> TransferRegisteredCardAmountToAssociatedCard(string userId, string sourceCardId, string destinationCardId, decimal? amount);
        #endregion

        #region Tipping

        void UpsertPendingTip(IPendingTip tip);

        #endregion

        #region Fraud

        IFraudCheckUserData GetUserDataForFraudCheck(string userId);

        IFraudCheckCardData GetCardDataForFraudCheck(string userId, string cardId);
        string ClearPaymentMethodSurrogateAndPaypalAccountNumbers(string userId, IPaymentMethod paymentMethod);

        #endregion
    }

    public interface IPendingTip
    {
        decimal TipAmount { get; set; }
        DateTime ExpirationDate { get; set; }
        long RedemptionTransactionId { get; set; }
    }
}
