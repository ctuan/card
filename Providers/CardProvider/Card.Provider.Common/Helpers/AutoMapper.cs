﻿using AutoMapper;

namespace Starbucks.Card.Provider.Common.Helpers
{
    public static class AutoMapper
    {
        public static T2 Map<T1,T2>(this T1 source)
        {
            Mapper.CreateMap<T1, T2>();
            
            var result = Mapper.Map<T1, T2>(source);

            return result;
        }
    }
}