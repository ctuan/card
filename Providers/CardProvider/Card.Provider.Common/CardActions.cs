﻿using System;
using System.Collections.Generic;

namespace Starbucks.Card.Provider.Common
{
    public class CardActions
    {      
        
        public static string Transfer = "Transfer";
        public static string AutoReload = "AutoReload";
        public static string Reload = "Reload";

        //TODO: Externalize
        public static IEnumerable<string> GetValidActions(string userMarket, string userCurrency, string cardCurrency)
        {
            
            var actions = new List<string>();

            if (!IsValidCurrency(userCurrency) || !IsValidCurrency(cardCurrency))
                return actions;

            if (CanAutoreload(userMarket, userCurrency, cardCurrency))
                actions.Add(AutoReload);

            if (CanTransfer(userMarket, userCurrency, cardCurrency))
                actions.Add(Transfer);

            if (CanReload(userMarket, userCurrency, cardCurrency))
                actions.Add(Reload);

            return actions;
        }

        public static bool CanAutoreload(string userSubMarket, string userCurrency, string cardCurrency)
        {
            if (userCurrency == "BRL" || cardCurrency == "BRL")
            {
                return false;
            }
            return userCurrency.Equals(cardCurrency, StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool CanTransfer(string userSubMarket, string userCurrency, string cardCurrency)
        {
            return userSubMarket != "ROI" && userCurrency.Equals(cardCurrency, StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool CanReload(string userSubMarket, string userCurrency, string cardCurrency)
        {
            //Can only reload DE from DE can reload any other combination
            if (userSubMarket == "DE")
            {
                return userSubMarket == "DE";
            }
            return true;

        }

        public static bool IsValidCurrency(string market)
        {
            if (string.IsNullOrEmpty(market)) return false;

            switch (market.ToUpper())
            {
                case "USD":
                case "CAD":
                case "GBP":
                case "EUR":
                case "BRL":                
                    return true;
                default:
                    return false;
            }
        }
            
    }
}
