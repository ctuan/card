﻿using System.Collections.Generic;
using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Provider.Common
{
    public interface ISelectedCardProvider
    {
        IEnumerable<string> GetSelectedCards(string userId, string clientId);
        IEnumerable<ISelectedCard> GetSelectedCards(string userId);
        void AssociateSelectedCardsWithClient(string userId, string clientId, IEnumerable<string> cardIds);
        void RemoveSelectedCards(string userId, string clientId, IEnumerable<string> cardIds);
        bool IsCardLinked(string userId, string cardId);
    }
}