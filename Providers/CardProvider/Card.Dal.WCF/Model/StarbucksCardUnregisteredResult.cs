﻿using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Dal.WCF.Model
{
    public class StarbucksCardUnregisterResult : IStarbucksCardUnregisterResult
    {
        public string CardId { get; set; }
    }
}
