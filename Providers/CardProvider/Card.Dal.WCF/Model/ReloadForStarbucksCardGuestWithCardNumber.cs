﻿using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Dal.WCF.Model
{
    public class ReloadForStarbucksCardGuestWithCardNumber : IReloadForStarbucksCardGuestWithCardNumber
    {
        public decimal Amount { get; set; }
        public string PaymentMethodId { get; set; }
        public string SessionId { get; set; }        
        public string EmailAddress { get; set; }
        public string FirstSix { get; set; }
        public string LastFour { get; set; }
        public string CardNumber { get; set; }
    }
}
