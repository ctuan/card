﻿using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Dal.WCF.Model
{
    public class CardRegistrationSource : ICardRegistrationSource
    {
        public string Platform { get; set; }
        public string Marketing { get; set; }
    }
}
