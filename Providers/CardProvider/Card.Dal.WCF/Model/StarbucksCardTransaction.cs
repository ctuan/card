﻿using System;
using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Dal.WCF.Model
{
    public class StarbucksCardTransaction : IStarbucksCardTransaction
    {
        public string CardId { get; set; }
        public string TransactionId { get; set; }
        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }
        public string Description { get; set; }
        public DateTime TransactionDate { get; set; }
    }
}
