﻿using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Dal.Wcf.Model
{
    public class Address : IAddress
    {
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string CountrySubdivision { get; set; }
        public string CountrySubdivisionDescription { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string CountryName { get; set; }
    }
}
