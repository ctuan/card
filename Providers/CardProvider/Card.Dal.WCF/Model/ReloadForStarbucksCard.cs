﻿using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Dal.WCF.Model
{
    public class ReloadForStarbucksCard : IReloadForStarbucksCard
    {
        public string Type { get; set; }
        public decimal Amount { get; set; }
        public string PaymentMethodId { get; set; }
        public string SessionId { get; set; }
    }

}
