﻿using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Dal.WCF.Model
{
    public class ReloadForStarbucksCardWithGuestPayPal : IReloadForStarbucksCardGuestWithPayPal

    {
        public decimal Amount { get; set; }
        public string PaymentMethodId { get; set; }
        public string SessionId { get; set; }  
        public string EmailAddress { get; set; }
    }
}
