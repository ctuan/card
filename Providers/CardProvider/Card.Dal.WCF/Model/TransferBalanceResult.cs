﻿using System.Collections.Generic;
using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Dal.WCF.Model
{
    public class TransferBalanceResult :ITransferBalanceResult
    {
        public IStarbucksCardTransaction StarbucksCardTransaction { get; set; }
        public IDictionary<string, decimal> Balances { get; set; }
    }
}
