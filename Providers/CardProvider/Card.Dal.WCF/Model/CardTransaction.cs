﻿using System;
using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Dal.Wcf.Model
{
    public class CardTransaction : ICardTransaction 
    {
        public string CardId { get; set; }
        public string TransactionId { get; set; }
        public string AuthorizationCode { get; set; }
        public string CardClass { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public string Description { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal BeginningBalance { get; set; }
        public decimal EndingBalance { get; set; }
        public string RequestCode { get; set; }
        public string ResponseCode { get; set; }

        public bool TransactionSucceeded
        {
            get
            {
                int returnCode;
                if (int.TryParse(ResponseCode, out returnCode))
                {
                    //if the returnCode is a 0, then we have successfully processed, so now reload
                    return returnCode == 0;
                }
                return false;
            }
        }

        public ICardPromotion Promotion { get; set; }
        public string Pin { get; set; }
        public string CardNumber { get; set; }
        public string BaseCurrency { get; set; }
        public decimal BeginingBalanceInBaseCurrency { get; set; }
        public decimal EndingBalanceInBaseCurrency { get; set; }
        public decimal AmountInBaseCurrency { get; set; }
    }
}
