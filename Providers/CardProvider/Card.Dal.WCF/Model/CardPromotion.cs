﻿using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Dal.Wcf.Model
{
    public class CardPromotion : ICardPromotion 
    {
        public string Code { get; set; }
        public decimal Amount { get; set; }
        public string Message { get; set; }
    }
}
