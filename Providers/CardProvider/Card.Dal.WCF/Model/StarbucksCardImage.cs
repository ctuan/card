﻿using Starbucks.Card.Provider.Common.Enums;
using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Dal.WCF.Model
{
    public class StarbucksCardImage : IStarbucksCardImage
    {
        //public string ImageIcon { get; set; }
        //public string ImageSmall { get; set; }
        //public string ImageMedium { get; set; }
        //public string ImageLarge { get; set; }
        //public string ImageIconMobile { get; set; }
        //public string ImageSmallMobile { get; set; }
        //public string ImageMediumMobile { get; set; }
        //public string ImageLargeMobile { get; set; }
        public CardImageType Type { get; set; }
        public string Uri { get; set; }
    }
}
