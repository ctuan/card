﻿using System;
using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Dal.WCF.Model
{
    public class GuestStarbucksCardBalance : IGuestStarbucksCardBalance
    {
        public string CardNumber { get; set; }
        public decimal? Balance { get; set; }
        public DateTime? BalanceDate { get; set; }
        public string BalanceCurrencyCode { get; set; }
    }
}
