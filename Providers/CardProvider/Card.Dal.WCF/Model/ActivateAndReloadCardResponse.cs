﻿using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Dal.Wcf.Model
{
    public class ActivateAndReloadCardResponse : IActivateAndReloadCardResonse
    {
        public ICardTransaction CardActivationTransaction { get; set; }
        public ICardTransaction CardReloadTransaction { get; set; }
    }
}
