﻿using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Dal.WCF.Model
{
    public class StarbucksCardStatus : IStarbucksCardStatus
    {
        public string CardId { get; set; }
        public bool Valid { get; set; }
    }
}
