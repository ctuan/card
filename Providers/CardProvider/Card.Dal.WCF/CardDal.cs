﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.ServiceModel;
//using System.ServiceModel.Web;
//using Starbucks.Card.Dal.Common.Interfaces;
//using Starbucks.Card.Dal.WCF.Model;
//using Starbucks.Card.Dal.WCF.Settings;
//using Starbucks.Card.Provider.Common.ErrorResources;
//using Starbucks.Card.Provider.Common.Exceptions;
//using Starbucks.Card.Provider.Common.Models;
//using Starbucks.OpenApi.ServiceExtensions.Culture;
//using Starbucks.Platform.Bom;
//using Starbucks.Platform.Bom.Shared;
//using Starbucks.Platform.Exceptions;
//using Starbucks.Services.CardManagement;
//using ServiceProxies.CardService.Wcf;
//using VisibilityLevel = Starbucks.Card.Provider.Common.Enums.VisibilityLevel;
//using Starbucks.ServiceProxies.Extensions;
//using CardTransaction = Starbucks.Platform.Bom.Shared.CardTransaction;
//using IAddress = Starbucks.Card.Provider.Common.Models.IAddress;

//namespace Starbucks.Card.Dal.WCF
//{
//    public class CardDal : ICardDal
//    {

//        private static readonly Dictionary<string, string> CultureConversion; 
//        static CardDal()
//        {
//            CultureConversion = new Dictionary<string, string>
//                {
//                    {"en-us", "MOUS"},
//                    {"en-ca", "MOCA"},
//                    {"en-uk", "MOUK"},
//                    {"fr-ca", "MOCA"}
//                };
//        }

//        private static string CurrentSubMarket()
//        {
//            string culture = CultureContext.CurrentCulture;
//            if(String.IsNullOrEmpty(culture ))return "MOUS";
//            culture = culture.ToLowerInvariant();
//            if (CultureConversion.ContainsKey(culture))
//            {
//                return CultureConversion[culture];
//            }
//            return "MOUS";
//        }

//        public IStarbucksCard GetCardByNumberAndPin(string cardNumber, string pin, string currentSubMarket)
//        {
//            try
//            {
//               // Starbucks.Platform.Bom.Shared.Card card = null;
//                var card = CardService.GetChannelFactory  (CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName 
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password)
//                    .Use (c=>c.GetCardByNumberAndPin(cardNumber, pin, CurrentSubMarket()));                   
                
//                //CardService.Use(cs => card = cs.GetCardByNumberAndPin(cardNumber, pin, CultureContext.CurrentSubMarket));
//                return card == null ? null : card.ToModel();
//            }
//            catch (FaultException fault)
//            {
//                var cardProviderException = fault.ToCardProviderException();
//                throw cardProviderException;
//            }
//        }

//        public decimal? GetBalanceByNumber(string cardNumber, string pin, string currentSubMarket)
//        {
//            try
//            {
               
//                 decimal? balance = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                     , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                     , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(cs => cs.GetBalanceByNumber(cardNumber, pin, CurrentSubMarket()));
//                return balance;
//            }
//            catch (FaultException fault)
//            {
//                var cardProviderException = fault.ToCardProviderException();
//                throw cardProviderException;
//            }
//        }

//        public IEnumerable<IStarbucksCardImage> GetStarbucksCardImageUrlByCardNumber(string cardNumber)
//        {
//            try
//            {
//                List<IStarbucksCardImage> images = null;
//                 var card = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(client => client.GetCardByNumber(cardNumber, CurrentSubMarket()));
//                 if (card != null)
//                     images = card.ToStarbucksCardImage();
//                return images;
//            }
//            catch (FaultException fault)
//            {
//                var cardProviderException = fault.ToCardProviderException();
//                throw cardProviderException;
//            }
//        }

//        public IEnumerable<IStarbucksCard> GetAssociatedCardsByVisibilityLevel(string userId, VisibilityLevel visibilityLevel)
//        {
//            try
//            {
             
//                //CardRepository.GetAssociatedCards(userId, VisibilityLevel.Decrypted);
//                //Added new method to SOAP Call
//                List<AssociatedCard> associatedCards = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(                   
//                    client =>  client.GetAssociatedCardsByVisibilityLevel(userId, visibilityLevel.ToServer()  ));                    
//                //only get cards registered to user
//                var cards = (associatedCards.Where(associatedCard => associatedCard.RegisteredUserId == userId)
//                                            .Select(associatedCard => associatedCard.ToModel())).Cast<IStarbucksCard>()
//                                                                                                .ToList();
//                return cards;
//            }
//            catch (FaultException fault)
//            {
//                var cardProviderException = fault.ToCardProviderException();
//                throw cardProviderException;
//            }
//        }        

//        public IAutoReloadProfile GetAutoReload(string userId, string autoReloadId)
//        {
//            try
//            {
         
//              IAutoReloadProfile    arProfile = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(cs => cs.GetAutoReload(userId, autoReloadId).ToModel());
//                return arProfile;
//            }
//            catch (FaultException fault)
//            {
//                var cardProviderException = fault.ToCardProviderException();
//                throw cardProviderException;
//            }
//        }

//        public decimal? GetBalanceRegistered(string userId, string cardId, string currentSubMarket)
//        {
//            try
//            {               
//               decimal?  result = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(cs => cs.GetBalanceRegistered(userId, cardId, CurrentSubMarket()));
//                return result;
//            }
//            catch (FaultException fault)
//            {
//                //549 Invalid SubMarket Code
//                var cardProviderException = fault.ToCardProviderException();
//                throw cardProviderException;
//            }
//        }

//        public IStarbucksCard GetCardByCardIdUserId(string cardId, string userId)
//        {
//            try
//            {               
//                //Platform.Bom.Shared.Card registeredCard = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                //    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                //    , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(cs =>  cs.GetCardByCardIdUserId(cardId, userId));

//                var cards = GetAssociatedCardsByVisibilityLevel(userId, VisibilityLevel.Decrypted);
//                var registeredCard = cards.SingleOrDefault(p => p.CardId == cardId && p.RegisteredUserId == userId );
//                return registeredCard ;
//            }
//            catch (FaultException fault)
//            {
//                var cardProviderException = fault.ToCardProviderException();
//                throw cardProviderException;
//            }

//        }

//        public string GetDecryptedCardNumber(string userId, string cardId)
//        {
//            try
//            {
//                var cardNumber = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                                                                  , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                                                                  , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(cs =>  cs.GetDecryptedCardNumber(userId, cardId));
//                return cardNumber;
//            }
//            catch (FaultException fault)
//            {
//                var cardProviderException = fault.ToCardProviderException();
//                throw cardProviderException;
//            }
//        }


//        public IStarbucksCardBalance RefreshStarbucksCardBalance(string userId, string cardId)
//        {
//            var starbucksCardBalance = new StarbucksCardBalance();
//            try
//            {
//                var balance = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(client => client.GetBalanceRegistered(userId, cardId, CurrentSubMarket())
//                    );
//                starbucksCardBalance.Balance = balance;
//                starbucksCardBalance.BalanceDate = DateTime.Now;
//            }
//            catch (FaultException faultException)
//            {
//                var cardProviderException = faultException.ToCardProviderException();
//                throw cardProviderException;
//            }
//            catch (Exception ex)
//            {
//                throw new CardProviderException(CardProviderErrorResource.UnknownErrorCode, CardProviderErrorResource.UnknownErrorMessage, ex);
//            }

//            return starbucksCardBalance;
//        }


//        public IStarbucksCardBalance RefreshStarbucksCardBalanceByCardNumberPinNumber(string cardNumber, string pin)
//        {
//            var starbucksCardBalance = new StarbucksCardBalance(); //{ CardId = cardId };
//            try
//            {
//                var balance = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(client => client.GetBalanceByNumber(cardNumber, pin, CurrentSubMarket()));

//                 starbucksCardBalance.Balance = balance;
//                 starbucksCardBalance.BalanceDate = DateTime.Now;
//            }
//            catch (FaultException faultException)
//            {
//                var cardProviderException = faultException.ToCardProviderException();
//                throw cardProviderException;
//            }
//            catch (Exception ex)
//            {
//                throw new CardProviderException(CardProviderErrorResource.UnknownErrorCode, CardProviderErrorResource.UnknownErrorMessage, ex);
//            }

//            return starbucksCardBalance;
//        }


//        public IEnumerable<IStarbucksCardTransaction> GetStarbucksCardTransactions(string userId, string cardId)
//        {
//            var transactionHistory = new List<IStarbucksCardTransaction>();
//            try
//            {
//                var cardTransactions = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(client => client.GetTransactionHistoryRegistered(userId, cardId, CurrentSubMarket()));
//                if (cardTransactions != null)
//                {
//                    transactionHistory.AddRange(
//                        cardTransactions.Select(transaction => transaction.ToModel())
//                                        .OrderByDescending(transaction => transaction.TransactionDate));
//                }
//            }
//            catch (Exception ex)
//            {
//                throw new CardProviderException(CardProviderErrorResource.UnknownErrorCode, CardProviderErrorResource.UnknownErrorMessage, ex);
//            }

//            return transactionHistory;
//        }

//        public string RegisterStarbucksCard(string userId, string cardNumber, string pin)
//        {
//            string newCardId ;
//            try
//            {
//                newCardId = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(client => client.RegisterCardByNumberUsingDefaultAddress(
//                            userId,
//                            cardNumber,
//                            pin,
//                            string.Format("My Card ({0})", cardNumber.Substring(12, 4)),
//                            CurrentSubMarket(),
//                            false)
//                    );
//            }
//            catch (FaultException faultException)
//            {
//                var cardProviderException = faultException.ToCardProviderException();
//                throw cardProviderException;
//            }
//            catch (Exception ex)
//            {
//                throw new CardProviderException(CardProviderErrorResource.UnknownErrorCode, CardProviderErrorResource.UnknownErrorMessage, ex);
//            }
//            return newCardId;
//        }

//        public string ActivateAndRegisterCard(string userId)
//        {            
//            try
//            {
                 
//              string newCardId = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(client => client.ActivateAndRegisterCard(userId, CurrentSubMarket()));
//                return newCardId;
//            }
//            catch (FaultException faultException)
//            {
//                var cardProviderException = faultException.ToCardProviderException();
//                throw cardProviderException;
//            }
//            catch (Exception ex)
//            {
//                throw new CardProviderException(CardProviderErrorResource.UnknownErrorCode, CardProviderErrorResource.UnknownErrorMessage, ex);
//            }
//        }


//        public IStarbucksCardUnregisterResult UnregisterStarbucksCard(string userId, string cardId)
//        {
//            ResponseStatus responseStatus ;
//            try
//            {
//                responseStatus = CardService.GetChannelFactory(
//                    CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password)
//                                            .Use(client => client.UnregisterCardWithStatus(userId, cardId, false));
//            }
//            catch (Exception ex)
//            {
//                throw new CardProviderException(CardProviderErrorResource.GeneralPaymentErrorMessage, CardProviderErrorResource.GeneralPaymentErrorMessage, ex);
//            }
//            if (responseStatus.Success)
//            {
//                return new StarbucksCardUnregisterResult {CardId = cardId};
//            }

//            if (responseStatus.Errors.ContainsKey(ResponseStatusErrorCode.UnregisterCard_DigitalCardHasRemainingBalance))
//            {
//                throw new CardProviderValidationException(CardProviderErrorResource.UnregisterNonZeroBalanceMessage,
//                                                          CardProviderErrorResource.UnregisterNonZeroBalanceCode);
//            }
//            //TODO Evaluate another way to handle multiple errors
//            // Not a success and not the digital card error; We'll have to re-evaluate this handling but for now throw a generic error
//            var errors = string.Join(",", responseStatus.Errors.Select(k => string.Format("{0}:{1}", k.Key, k.Value)));
//            throw new CardProviderException(CardProviderErrorResource.UnknownErrorCode, CardProviderErrorResource.UnknownErrorMessage, new Exception(errors));
//        }

//        public IEnumerable<IStarbucksCardStatus> GetStatusesForUserCards(string userId, IEnumerable<string> cardIds)
//        {
//            var cards = cardIds as string[] ?? cardIds.ToArray();            

//            try
//            {
//                var cardStatuses = new List<StarbucksCardStatus>();
//                var registeredCards =  CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(client =>client.GetRegisteredCards(userId));

//                //var cardStatuses = cards.Select(cardId => registeredCards.Exists(p => p.CardId == cardId) ? new StarbucksCardStatus() {CardId = cardId, Valid = true} : new StarbucksCardStatus() {CardId = cardId, Valid = false}).Cast<IStarbucksCardStatus>().ToList();

//                cardStatuses.AddRange(
//                      registeredCards.Select(registeredCard =>
//                          new StarbucksCardStatus { CardId = registeredCard.CardId, Valid = true }));

//                cardStatuses.AddRange(
//                    cards.Except(cardStatuses.Select(cs => cs.CardId)).Select(
//                        c => new StarbucksCardStatus { CardId = c, Valid = false }));
                
//                return cardStatuses;
//            }
//            catch (Exception ex)
//            {
//                throw new CardProviderException(CardProviderErrorResource.UnknownErrorCode, CardProviderErrorResource.UnknownErrorMessage, ex);
//            }
//        }

//        public IAutoReloadProfile UpdateAutoReload(string userId, string cardId, IAutoReloadProfile autoReloadProfile)
//        {
//            try
//            {
//                return UpsertAutoReload(userId, cardId, autoReloadProfile);
//            }
//            catch (FaultException faultException)
//            {
//                var cardProviderException = faultException.ToCardProviderException();
//                throw cardProviderException;
//            }
//        }

//        public IAutoReloadProfile SetupAutoReload(string userId, string cardId, IAutoReloadProfile autoReloadProfile)
//        {
//            try
//            {
//                return UpsertAutoReload(userId, cardId, autoReloadProfile);
//            }
//            catch (FaultException faultException)
//            {
//                var cardProviderException = faultException.ToCardProviderException();
//                throw cardProviderException;
//            }
//        }

//        public IStarbucksCardTransaction ReloadStarbucksCard(string userId, string cardId,
//                                                             IReloadForStarbucksCard reloadForStarbucksCard)
//        {
//            CardTransaction cardTransaction;
//            try
//            {  
//                var action = GetConsumerKeyFromContext() + "-ReloadStarbucksCard";
//                 var isPaypal = reloadForStarbucksCard.Type.Equals("paypal",
//                                                                      StringComparison.CurrentCultureIgnoreCase);

//                if (isPaypal )
//                    cardTransaction = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                        , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                        , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(client =>ReloadWithPaypal(userId, cardId, reloadForStarbucksCard, client, action));
//                else                
//                    cardTransaction = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                        , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                        , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(client => ReloadWithCreditCard(userId, cardId, reloadForStarbucksCard, client,action));                                                
//            }
//            catch (PaymentException paymentException)
//            {
//                throw new CardProviderException(CardProviderErrorResource.GeneralPaymentErrorCode, CardProviderErrorResource.GeneralPaymentErrorMessage, paymentException);
//            }
//            catch (FaultException faultException)
//            {
//                var cardProviderException = faultException.ToCardProviderException();
//                throw cardProviderException;
//            }
//            catch (Exception ex)
//            {
//                throw new CardProviderException(CardProviderErrorResource.UnknownErrorCode, CardProviderErrorResource.UnknownErrorMessage, ex);
//            }


//            return cardTransaction.ToModel();
//        }

//        private static string GetConsumerKeyFromContext()
//        {
//            string consumerKey = null;
//            if (WebOperationContext.Current != null)
//            {
//                var headers = WebOperationContext.Current.IncomingRequest.Headers;
//                var authorization = headers.GetValues("Authorization");
//                if (authorization != null)
//                {
//                    var consumerKeyValuePair = authorization.FirstOrDefault(a => a.Contains("oauth_consumer_key"));
//                    if (!string.IsNullOrWhiteSpace(consumerKeyValuePair))
//                    {
//                        consumerKey = consumerKeyValuePair.Split('=')[1];
//                        consumerKey = consumerKey.Trim(new[] { '"' });
//                    }
//                }
//            }
//            return consumerKey;
//        }

//        private static CardTransaction ReloadWithCreditCard(string userId, string cardId,
//                                                  IReloadForStarbucksCard reloadForStarbucksCard, ICardService client, string action)
//        {
//            var cardTransaction = client.ReloadAssociatedUsingSavedPayment(
//                userId, cardId, reloadForStarbucksCard.Amount, reloadForStarbucksCard.PaymentMethodId,
//                String.Empty, CurrentSubMarket(), action);
//            return cardTransaction;
//        }

//        private static CardTransaction ReloadWithPaypal(string userId, string cardId,
//                                                        IReloadForStarbucksCard reloadForStarbucksCard, ICardService client, string action)
//        {
//            var cardTransaction = client.ReloadAssociatedWithPayPalToken(
//                userId, cardId, reloadForStarbucksCard.Amount, reloadForStarbucksCard.PaymentMethodId,
//               CurrentSubMarket(), action);
//            return cardTransaction;
//        }

      

//        private static IAutoReloadProfile UpsertAutoReload(string userId, string cardId,
//                                                           IAutoReloadProfile autoReloadProfile)
//        {
//            string autoReloadId;
//            string billingAgreementId = (autoReloadProfile.GetType() == typeof (IAutoReloadProfile)
//                                                     ? ((AutoReloadProfile) autoReloadProfile).BillingAgreementId
//                                                     : null);

//            string paymentMethodId = autoReloadProfile.PaymentMethodId;

//            if (autoReloadProfile.AutoReloadType.Equals("Amount", StringComparison.InvariantCultureIgnoreCase))
//            {
//                autoReloadId = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(client =>
//                        autoReloadProfile.TriggerAmount != null ? client.SetAutoReloadAmountBasedWithPaymentMethod(
//                            userId,
//                            cardId,
//                            autoReloadProfile.Amount,
//                            autoReloadProfile.TriggerAmount.Value,
//                            paymentMethodId,
//                            false,
//                            null) : null);
//            }
//            else if (autoReloadProfile.AutoReloadType.Equals("Date", StringComparison.InvariantCultureIgnoreCase))
//            {
//                autoReloadId = CardService.GetChannelFactory(
//                    CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(client => autoReloadProfile.Day != null ? client.SetAutoReloadWithPaymentMethod(
//                        userId,
//                        cardId,
//                        autoReloadProfile.Amount,
//                        autoReloadProfile.Day.Value,
//                        paymentMethodId,
//                        false,
//                        null) : null);
//            }
//            else
//            {
//                return null;
//            }

//            var userId2 = userId;
//            var autoReloadId2 = autoReloadId;
//            IAutoReloadProfile returnValue = CardService.GetChannelFactory(
//                CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(client => client.GetAutoReload(userId2, autoReloadId2).ToModel());

//            if (billingAgreementId != null)
//                        returnValue.BillingAgreementId = billingAgreementId;

//            return returnValue;
//        }

//        public void EnableAutoReload(string userId, string cardId)
//        {
//            Platform.Bom.Shared.Card card = GetAutoReloadCard(cardId, userId);                       
//            try
//            {
//                CardService.GetChannelFactory(
//                    CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password)
//                           .Use(client => client.EnableAutoReload(card.AutoReloadId, userId));
//            }
//            catch (FaultException faultException)
//            {
//                var cardProviderException = faultException.ToCardProviderException();
//                throw cardProviderException;
//            }
//        }

//        public void DisableAutoReload(string userId, string cardId)
//        {
//            Platform.Bom.Shared.Card card =  GetAutoReloadCard(cardId, userId );                       
//            try
//            {
//                CardService.GetChannelFactory(
//                    CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password)
//                           .Use(client => client.DisableAutoReload(card.AutoReloadId, userId, DateTime.Now.AddYears(100)));
//            }
//            catch (FaultException faultException)
//            {
//                var cardProviderException = faultException.ToCardProviderException();
//                throw cardProviderException;
//            }          
//        }

//        private static Platform.Bom.Shared.Card GetAutoReloadCard(string cardId,string userId)
//        {
//            Platform.Bom.Shared.Card card;
//            try
//            {
//                card = CardService.GetChannelFactory(
//                    CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password)
//                                  .Use(client => client.GetCard(cardId));
//            }
//            catch (FaultException faultException)
//            {
//                var cardProviderException = faultException.ToCardProviderException();
//                throw cardProviderException;
//            }
            
//            if (card == null)
//                throw new CardProviderException(CardProviderErrorResource.CardNotFoundCode, CardProviderErrorResource.CardNotFoundMessage);
//            if (string.IsNullOrEmpty(card.CardId))
//                throw new CardProviderException(CardProviderErrorResource.CardNotFoundCode, CardProviderErrorResource.CardNotFoundMessage);
//            if (card.RegisteredUserId != userId)
//                throw new CardProviderException(CardProviderErrorResource.CardNotRegisteredToUserCode, CardProviderErrorResource.CardNotRegisteredToUserMessage);

//            if (string.IsNullOrEmpty(card.AutoReloadId))
//                throw new CardProviderException(CardProviderErrorResource.AutoReloadNotFoundCode, CardProviderErrorResource.AutoReloadNotFoundMessage);

//            return card;
//        }


//        public ITransferBalanceResult TransferRegisteredCardBalanceToAssociatedCard(string userId, string sourceCardId,
//                                                                                    string destinationCardId,
//                                                                                    string currentSubMarket)
//        {
//            ITransferBalanceResult transferResult = null;
//            try
//            {
//                var result = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(client =>client.TransferRegisteredCardBalanceToAssociatedCard(userId, sourceCardId,
//                                                                                          destinationCardId,
//                                                                                          CurrentSubMarket()));
//                if (result != null)
//                    transferResult = new TransferBalanceResult { StarbucksCardTransaction = result.ToModel() };
//            }
//            catch (Exception ex)
//            {
//                throw new CardProviderException(CardProviderErrorResource.UnknownErrorCode,
//                                                CardProviderErrorResource.UnknownErrorMessage, ex);
//            }

//            return transferResult;
//        }

//        public ITransferBalanceResult TransferRegisteredCardAmountToAssociatedCard(string userId, string sourceCardId,
//                                                                                   string destinationCardId
//                                                                                   , decimal? amount,
//                                                                                   string currentSubMarket)
//        {
//            ITransferBalanceResult transferResult = null;
//            try
//            {
//                if (!amount.HasValue) return null;
//                 var result = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(client =>client.TransferRegisteredCardAmountToAssociatedCard(userId,
//                                                                                         sourceCardId,
//                                                                                         destinationCardId,
//                                                                                         amount.Value,
//                                                                                        CurrentSubMarket()));
//                if (result != null)
//                    transferResult = new TransferBalanceResult {StarbucksCardTransaction = result.ToModel()};
//            }
//            catch (Exception ex)
//            {
//                throw new CardProviderException(CardProviderErrorResource.UnknownErrorCode,
//                                                CardProviderErrorResource.UnknownErrorMessage, ex);
//            }

//            return transferResult;
//        }

//        public ICardRegistrationStatus RegisterCardByNumberUsingDefaultAddress(string userId, string cardNumber,
//                                                                               string cardPin,
//                                                                               string nickName, string currentSubmarket,
//                                                                               bool makeDefault)
//        {
//            var registrationStatus = new CardRegistrationStatus {CardNumber = cardNumber, Successful = false};
//            try
//            {
//                registrationStatus.CardId = CardService.GetChannelFactory(
//                    CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password)
//                                                       .Use(
//                                                           client =>
//                                                           client.RegisterCardByNumberUsingDefaultAddress(userId,
//                                                                                                          cardNumber,
//                                                                                                          cardPin,
//                                                                                                          string.Empty,
//                                                                                                         CurrentSubMarket(),
//                                                                                                          false));
//                registrationStatus.Code = "0";
//                registrationStatus.Successful = true;
//            }
//            catch (FaultException faultException)
//            {
//                var cardException = faultException.ToCardProviderException();
//                registrationStatus.Successful = false;
//                registrationStatus.Message = cardException.Message;
//                registrationStatus.Code = cardException.Code;
//            }
//            catch (Exception ex)
//            {              
//                registrationStatus.Successful = false;
//                registrationStatus.Message = ex.Message;                              
//            }           
//            return registrationStatus;

//        }

//        public IStarbucksCardTransaction ReloadAssociatedUsingSavedPayment(string userId, string cardId, decimal amount,
//                                                                           string paymentMethodId, string cvn, string submarket)
//        {
//            IStarbucksCardTransaction transaction = null;
//            try
//            {
//                var result = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(client => client.ReloadAssociatedUsingSavedPayment(userId, cardId, amount, paymentMethodId, cvn, CurrentSubMarket())); 
//                if (result != null) transaction = result.ToModel();
//            }
//            catch (Exception ex)
//            {
//                throw new CardProviderException(CardProviderErrorResource.UnknownErrorCode,
//                                                CardProviderErrorResource.UnknownErrorMessage, ex);
//            }

//            return transaction;
//        }

//        public IStarbucksCardTransaction ReloadAssociatedWithPayPalBillingId(string userId, string cardId, decimal amount,
//                                                                             string billingId, string submarket)
//        {
//            IStarbucksCardTransaction transaction = null;
//            try
//            {
//                 var result = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(client => client.ReloadAssociatedWithPayPalBillingId(userId, cardId, amount, billingId, CurrentSubMarket())); 
//                if (result != null) transaction = result.ToModel();
//            }
//            catch (Exception ex)
//            {
//                throw new CardProviderException(CardProviderErrorResource.UnknownErrorCode,
//                                                CardProviderErrorResource.UnknownErrorMessage, ex);
//            }

//            return transaction;
//        }


//        public IStarbucksCardTransaction ReloadStarbucksCardByCardNumberPin(string cardNumber, string pin, decimal amount, string paymentMethodId, string paymentType, string submarket, string billingAddressId)
//        {          
//            IStarbucksCardTransaction transaction = null;
//            try
//            {  var billingInfo = new BillingInfo
//                {
//                    PaymentMethod = new PaymentMethod { PaymentMethodId = paymentMethodId, PaymentType = (PaymentType)Enum.Parse(typeof(PaymentType), paymentType, true), IsTemporary = true, BillingAddressId = billingAddressId   }                    
//                            };
//                 var result = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                    , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(client => client.ReloadByNumberPinless(cardNumber, amount, billingInfo, CurrentSubMarket()));
//                if (result != null) transaction = result.ToModel();
//            }
//            catch (Exception ex)
//            {
//                throw new CardProviderException(CardProviderErrorResource.UnknownErrorCode,
//                                                CardProviderErrorResource.UnknownErrorMessage, ex);
//            }

//            return transaction;
//        }
        

//        public IActivateAndReloadCardResonse ActivateAndReloadCardAsGift(decimal amount, string paymentMethodId, string paymentType, string submarket)
//        {
//           // IActivateAndReloadCardResonse resonse = null;
//            try
//            {
//                var bi = new BillingInfo
//                    {
//                       PaymentMethod = new PaymentMethod { PaymentMethodId = paymentMethodId, PaymentType = (PaymentType)Enum.Parse(typeof(PaymentType), paymentType, true) }
//                    };
                    

//                var result = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                   , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                   , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(client => client.ActivateAndReloadCardAsGift(amount, bi, CurrentSubMarket()));
//                return result.ToModel();
//            }
//            catch (Exception ex)
//            {

//                throw new CardProviderException(CardProviderErrorResource.UnknownErrorCode,
//                                                CardProviderErrorResource.UnknownErrorMessage, ex);
//            }
//        }

//        public ICardTransaction ReloadAsGiftContribution(string cardNumber, decimal amount, string paymentMethodId, string paymentType, string submarket)
//        {
//            //ICardTransaction cardTransaction = null;
//            try
//            {
//                var bi = new BillingInfo
//                    {
//                    PaymentMethod = new PaymentMethod { PaymentMethodId = paymentMethodId, PaymentType = (PaymentType)Enum.Parse(typeof(PaymentType), paymentType, true) }
//                };


//                var result = CardService.GetChannelFactory(CardProviderSettingsSection.Settings.CardServiceSettings.ServiceConfigurationName
//                   , CardProviderSettingsSection.Settings.CardServiceSettings.Username
//                   , CardProviderSettingsSection.Settings.CardServiceSettings.Password).Use(client => client.ReloadAsGiftContribution(cardNumber, amount, bi, CurrentSubMarket()));
//                return result.ToModelCardTransaction();
//            }
//            catch (Exception ex)
//            {

//                throw new CardProviderException(CardProviderErrorResource.UnknownErrorCode,
//                                                CardProviderErrorResource.UnknownErrorMessage, ex);
//            }
//        }
//    }
//}

