﻿using System.ServiceModel;
using Starbucks.Services.CardManagement;
using Wcf.Common;

namespace Card.Provider.WCF.Provider
{
    public static class CardService
    {
        private static ChannelFactory<ICardService> _channelFactory;

        public static ChannelFactory<ICardService> ChannelFactory
        {
            get
            {
                if (_channelFactory == null)
                {
                    _channelFactory = new ChannelFactory<ICardService>("ICardService");
                    if (_channelFactory.Credentials != null)
                    {
                        _channelFactory.Credentials.UserName.UserName = CardProviderSettingsSection.Settings.CardServiceUserName;
                        _channelFactory.Credentials.UserName.Password = CardProviderSettingsSection.Settings.CardServicePassword;
                    }
                    _channelFactory.Endpoint.Behaviors.Add(new CultureContextEndpointBehavior());
                }
                return _channelFactory;
            }
        }

        public static void Use(Wcf.Common.UseServiceDelegate<ICardService> codeblock)
        {
            Wcf.Common.Service<ICardService>.Use(codeblock, ChannelFactory);
        }

    }

   
}
