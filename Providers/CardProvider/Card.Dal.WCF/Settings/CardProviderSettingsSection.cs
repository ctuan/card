﻿using Starbucks.ServiceProxies.CardService.Wcf.Configuration;
using System.Configuration;

namespace Starbucks.Card.Dal.WCF.Settings
{
    public class CardProviderSettingsSection : ConfigurationSection
    {
        private static CardProviderSettingsSection _settings;
        public static CardProviderSettingsSection Settings
        {
            get
            {
                return _settings ??
                       (_settings =
                        ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection);
            }
        }
        [ConfigurationProperty("cardServiceSettings", IsRequired = true)]
        public CardServiceElement CardServiceSettings
        {
            get
            {
                return this["cardServiceSettings"] as CardServiceElement;
            }
            set
            {
                this["serviceUserName"] = value;
            }
        }

        [ConfigurationProperty("starbucksCardWebImagePaths")]
        public ImagePathFormatSet StarbucksCardWebImagePaths
        {
            get
            {
                return (ImagePathFormatSet)this["starbucksCardWebImagePaths"];
            }
            set
            {
                this["starbucksCardWebImagePaths"] = value;
            }
        }

        [ConfigurationProperty("starbucksCardMobileImagePaths")]
        public ImagePathFormatSet StarbucksCardMobileImagePaths
        {
            get
            {
                return (ImagePathFormatSet)this["starbucksCardMobileImagePaths"];
            }
            set
            {
                this["starbucksCardMobileImagePaths"] = value;
            }
        }

        [ConfigurationProperty("starbucksCardAndroidImagePaths")]
        public ImagePathFormatSet StarbucksCardAndroidImagePaths
        {
            get
            {
                return (ImagePathFormatSet)this["starbucksCardAndroidImagePaths"];
            }
            set
            {
                this["starbucksCardAndroidImagePaths"] = value;
            }
        }

        [ConfigurationProperty("starbucksCardAndroidThumbImagePaths")]
        public ImagePathFormatSet StarbucksCardAndroidThumbImagePaths
        {
            get
            {
                return (ImagePathFormatSet)this["starbucksCardAndroidThumbImagePaths"];
            }
            set
            {
                this["starbucksCardAndroidThumbImagePaths"] = value;
            }
        }
    }
    public class ImagePathFormatSet : ConfigurationElement
    {
        [ConfigurationProperty("icon", IsRequired = false)]
        public string Icon
        {
            get
            {
                return (string)this["icon"];
            }
            set
            {
                this["icon"] = value;
            }
        }

        [ConfigurationProperty("small", IsRequired = false)]
        public string Small
        {
            get
            {
                return (string)this["small"];
            }
            set
            {
                this["small"] = value;
            }
        }

        [ConfigurationProperty("medium", IsRequired = false)]
        public string Medium
        {
            get
            {
                return (string)this["medium"];
            }
            set
            {
                this["medium"] = value;
            }
        }

        [ConfigurationProperty("large", IsRequired = false)]
        public string Large
        {
            get
            {
                return (string)this["large"];
            }
            set
            {
                this["large"] = value;
            }
        }
    }
}
