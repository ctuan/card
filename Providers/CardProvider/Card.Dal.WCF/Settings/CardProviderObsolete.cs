﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using Card.Provider.Exceptions;
using Card.Provider.Interfaces;
using Card.Provider.WCF.Model;
using Starbucks.Api.Services.Extensions.ErrorHandling ;
using Starbucks.Api.Utilities;
using Starbucks.Platform.Bom.Shared;
using Starbucks.Platform.Exceptions;
using Starbucks.Services.CardManagement;
using CardRegistrationStatus = Starbucks.Api.Services.Profile.DataContracts.CardRegistrationStatus;
using StarbucksCardUnregisterResult = Starbucks.Api.Services.Profile.DataContracts.StarbucksCardUnregisterResult;

namespace Card.Provider.WCF.Provider
{
    public class CardProviderObsolete : ICardProvider 
    {     
        public IStarbucksCard GetStarbucksCardByNumberAndPin(string cardNumber, string pin)
        {
            try
            {                
                Starbucks.Platform.Bom.Shared.Card card = null;
                CardService.Use(client =>
                    {
                        card = client.GetCardByNumberAndPin(cardNumber, pin, CultureContext.CurrentSubMarket);
                        if (card == null) return;
                        if (!card.Balance.HasValue)
                        {
                            var result = client.GetBalanceByNumber(cardNumber, pin, CultureContext.CurrentSubMarket);
                            card.Balance = result;
                            card.BalanceDate = DateTime.Now;
                        }
                        card.Number = cardNumber;
                    });

                return card == null ? null : card.ToModel();
            }
            catch (FaultException fault)
            {
                var cardProviderException = fault.ToCardProviderException();
                throw cardProviderException;                
            }         
        }

        public IEnumerable< IStarbucksCardImage> GetStarbucksCardImageUrlByCardNumber(string cardNumber)
        {
            try
            {
                List<IStarbucksCardImage> images = null;
                CardService.Use(client =>
                {
                    var card = client.GetCardByNumber(cardNumber, CultureContext.CurrentSubMarket);
                    if (card != null)
                        images = card.ToStarbucksCardImage();
                });

                return images;
            }
            catch (FaultException fault)
            {
                var cardProviderException = fault.ToCardProviderException();
                throw cardProviderException;                
            }         
        }

        public IEnumerable<IStarbucksCard> GetStarbucksCards(string userId, bool enableBalanceUpdatesInCompleteProfile)
        {
            // Retrieve the stabucks cards
            var cards = new List<IStarbucksCard>( );
            try
            {
                // Retrieve the associates cards & loop adding the registered cards to the output
                // The visibility needs to be decrypted in order to provide the card number
                List<AssociatedCard> associatedCards = null;//CardRepository.GetAssociatedCards(userId, VisibilityLevel.Decrypted);
                //Added new method to SOAP Call
                CardService.Use(client => associatedCards = client.GetAssociatedCardsByVisibilityLevel(userId, Card.Provider.Enums.VisibilityLevel.Decrypted.ToServer()));
                foreach (var associatedCard in associatedCards)
                {
                    // If the card is not registerd to the current user, don't add it to the output but keep going
                    if (associatedCard.RegisteredUserId != userId)
                    {
                        continue;
                    }

                    // The card is registered to the user, convert it to our output type and get the
                    // Autoreload profile if one exists
                    var card = associatedCard.ToModel() ;
                    if (!string.IsNullOrEmpty(associatedCard.AutoReloadId))
                    {
                        IAutoReloadProfile arProfile = null;
                        var card1 = associatedCard;
                        CardService.Use(cs => arProfile = cs.GetAutoReload(userId, card1.AutoReloadId).ToModel());
                        card.AutoReloadProfile = arProfile;

                        // Autoreload profile doesn't include payment type, look up the payment type and add it to the profile.
                        // IMPORTANT! it cannot be retrieved from the list of payment mehtods already retrieved because
                        // paypal is currently filtered out of calls to get the list of user payment methods so a new
                        // call to get that specific id must be made
                        
                        //var arPaymentMethod = CardRepository.GetPaymentMethod(userId, card.AutoReloadProfile.PaymentMethodId);
                        //if (arPaymentMethod != null)
                        //{
                        //    card.AutoReloadProfile.PaymentType = arPaymentMethod.PaymentType.ToString(); // 
                        //}
                        //Factored to controller to use PaymentProvider
                        //SetPaymentMethod(card.AutoReloadProfile, userId);
                    }
                    
                    cards.Add(card);
                }

                // If we should be enabling balance updates during this process, go get updated balances for cards with a null balance or null balance date
                //moved to f(x) param
                //bool enableBalanceUpdatesInCompleteProfile = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EnableBalanceUpdatesInCompleteProfile"));
                if (enableBalanceUpdatesInCompleteProfile)
                {
                    var cardsWithoutBalance = cards.Where(c => !c.Balance.HasValue || !c.BalanceDate.HasValue);
                    if (cardsWithoutBalance.Any())
                    {

                        CardService.Use(client =>
                        {
                            foreach (var card in cards)
                            {
                                card.Balance = client.GetBalanceRegistered(userId, card.CardId, CultureContext.CurrentSubMarket);
                                card.BalanceDate = DateTime.Now;
                            }
                        });
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex.ToWebFaultException(new Dictionary<string, string> { { "userId", userId }, { "submarket", CultureContext.CurrentSubMarket } });
            }


            return cards;
        }                                                                      
       
        public IEnumerable<IStarbucksCardStatus> GetStatusesForUserCards(string userId, IEnumerable<string> cardIds)
        {
            // No cards sent in so none for us to return original list as valid

            var cards = cardIds as string[] ?? cardIds.ToArray();
            if (!cards.ToArray().Any())
            {
                return new List<IStarbucksCardStatus>();
            }

            try
            {
               
                var cardStatuses = new List<IStarbucksCardStatus>( );
                CardService.Use(client =>
                {
                    var registeredCards = client.GetRegisteredCards(userId);
                    cardStatuses.AddRange(
                        registeredCards.Select(registeredCard =>
                            new StarbucksCardStatus { CardId = registeredCard.CardId, Valid = true }));
                });

                cardStatuses.AddRange(
                    cards.Except(cardStatuses.Select(cs => cs.CardId)).Select(
                        c => new StarbucksCardStatus  { CardId = c, Valid = false } ));
                return cardStatuses;
            }
            catch (Exception ex)
            {
                throw new CardProviderException(CardProviderErrorResource.UnknownErrorMessage, CardProviderErrorResource.UnknownErrorCode, ex );
            }
        }

        public IStarbucksCard GetStarbucksCardById(string userId, string cardId)
        {
            IStarbucksCard sbuxCard;
            try
            {
                Starbucks.Platform.Bom.Shared.Card registeredCard = null; //CardRepository.GetCard(cardId);
                CardService.Use(cs => registeredCard = cs.GetCardByCardIdUserId(cardId, userId));
         

                if (registeredCard == null) return null;

                // check for non-null Balance value
                if (!registeredCard.Balance.HasValue || !registeredCard.BalanceDate.HasValue)
                {
                    CardService.Use(client =>
                        {
                            // Refresh balance is an explicit call to ValueLink, and can occasionally fail
                            // wrap this call to muffle any errors, and return Balance = null if they occur
                            try
                            {
                                // if balance is null (ie: a newly registered card),
                                // we need to make the call to get the true value
                                registeredCard.Balance = client.GetBalanceRegistered(userId,
                                                                                     cardId,
                                                                                     CultureContext.CurrentSubMarket);
                                registeredCard.BalanceDate = DateTime.Now;
                                // retrieve the card again to make sure we have the correct balance currency code / other information
                                // this can be an issue since new cards will hava a bal currency of null returned from initial call, once we populate the balane
                                // we want to return an accurate balance currency code so that the caller is not making a bad balance currency assumption
                                //registeredCard = CardRepository.GetCard(cardId);                                
                                registeredCard = client.GetCardByCardIdUserId(cardId, userId);

                            }
                                // ReSharper disable EmptyGeneralCatchClause
                            catch
                                // ReSharper restore EmptyGeneralCatchClause
                            {
                                // do nothing. An error occured so leave Balance as null
                            }
                        });

                }
                // convert the old CardService.Card object to the new data contract
                sbuxCard = registeredCard.ToModel();

                // make sure card number is set...
                // overwrite first CardNumber field with the value from the secure call
                //sbuxCard.CardNumber = CardRepository.GetDecryptedCardNumber(cardId);
                CardService.Use(cs => sbuxCard.CardNumber = cs.GetDecryptedCardNumber(userId, cardId));

                // additionally, we'd like to return AutoReload information if available
                // so do the lookup here and attach the information if possible
                var reload = GetAutoReloadProfile(userId, registeredCard.AutoReloadId);
                if (reload != null)
                {
                    sbuxCard.AutoReloadProfile = reload;

                    // Unfortunately, AutoReload doesnt contain the payment method type (Account, PayPal, etc) but we need that information for display purposes
                    // make an addition AccountService call here to get the associated PaymentMethod, and tack on the PaymentType value.
                    //Factored to controller to use PaymentProvider
                    //SetPaymentMethod(sbuxCard.AutoReloadProfile, userId);
                }
            }
            catch (Exception ex)
            {
                throw new CardProviderException(CardProviderErrorResource.UnknownErrorMessage,
                                                CardProviderErrorResource.UnknownErrorCode, ex);
            }

            return sbuxCard;
        }

        public IStarbucksCardBalance RefreshStarbucksCardBalance(string userId, string cardId)
        {
            var starbucksCardBalance = new StarbucksCardBalance { CardId = cardId };
            try
            {
                CardService.Use(client =>
                    {
                        var balance = client.GetBalanceRegistered(userId, cardId, CultureContext.CurrentSubMarket);
                        var card = client.GetCard(cardId);
                        starbucksCardBalance.Balance = balance;
                        starbucksCardBalance.BalanceDate = DateTime.Now;
                        starbucksCardBalance.BalanceCurrencyCode = card.BalanceCurrency;
                    });
            }
            catch (FaultException faultException)
            {
                var cardProviderException = faultException.ToCardProviderException();
                throw cardProviderException;                
            }
            catch (Exception ex)
            {
                throw new CardProviderException(CardProviderErrorResource.UnknownErrorMessage,
                                                CardProviderErrorResource.UnknownErrorCode, ex);
            }

            return starbucksCardBalance;
        }

        public IStarbucksCardBalance RefreshStarbucksCardBalanceByCardNumberPinNumber(string cardNumber, string pin)
        {
            var starbucksCardBalance = new Model.StarbucksCardBalance(); //{ CardId = cardId };
            try
            {
                CardService.Use(client =>
                {
                    var balance = client.GetBalanceByNumber( cardNumber , pin, CultureContext.CurrentSubMarket);
                    var card = GetStarbucksCardByNumberAndPin(cardNumber, pin);
                    starbucksCardBalance.Balance = balance;
                    starbucksCardBalance.BalanceDate = DateTime.Now;
                    starbucksCardBalance.BalanceCurrencyCode = card.BalanceCurrencyCode;
                    starbucksCardBalance.CardId = card.CardId;
                });
            }
            catch (FaultException faultException)
            {
                var cardProviderException = faultException.ToCardProviderException();
                throw cardProviderException;
            }
            catch (Exception ex)
            {
                throw new CardProviderException(CardProviderErrorResource.UnknownErrorMessage,
                                                CardProviderErrorResource.UnknownErrorCode, ex);
            }

            return starbucksCardBalance;
        }

        public IEnumerable<IStarbucksCardTransaction> GetStarbucksCardTransactions(string userId, string cardId)
        {
            var transactionHistory = new List<IStarbucksCardTransaction>( ) ;
            try
            {
                CardService.Use(client =>
                {
                    var cardTransactions = client.GetTransactionHistoryRegistered(userId, cardId, CultureContext.CurrentSubMarket);
                    if (cardTransactions != null)
                    {
                        transactionHistory.AddRange(cardTransactions.Select(transaction => transaction.ToModel()).OrderByDescending(transaction => transaction.TransactionDate));
                    }

                });
            }
            catch (Exception ex)
            {
                throw new CardProviderException(CardProviderErrorResource.UnknownErrorMessage,
                                                CardProviderErrorResource.UnknownErrorCode, ex);
            }

            return transactionHistory;
        }

        public IStarbucksCard RegisterStarbucksCard(string userId, string cardNumber, string pin)
        {      
            string newCardId = null;
            try
            {
                CardService.Use(client =>
                    {
                        newCardId = client.RegisterCardByNumberUsingDefaultAddress(
                            userId,
                            cardNumber,
                            pin,
                            string.Format("My Card ({0})", cardNumber.Substring(12, 4)),
                            CultureContext.CurrentSubMarket,
                            false);
                    });
            }
            catch (FaultException faultException)
            {
                var cardProviderException = faultException.ToCardProviderException();
                throw cardProviderException;                
            }
            catch (Exception ex)
            {
                throw new CardProviderException(CardProviderErrorResource.UnknownErrorMessage,
                                                CardProviderErrorResource.UnknownErrorCode, ex);
            }

            return GetStarbucksCardById(userId, newCardId);
        }


        public IStarbucksCard ActivateAndRegisterCard(string userId)
        {
            string newCardId = null;

            try
            {
                CardService.Use(client =>
                {
                    newCardId = client.ActivateAndRegisterCard(userId, CultureContext.CurrentSubMarket);
                });
            }
            catch (FaultException faultException)
            {
                var cardProviderException = faultException.ToCardProviderException();
                throw cardProviderException;                
            }
            catch (Exception ex)
            {
                throw new CardProviderException(CardProviderErrorResource.UnknownErrorMessage,
                                                CardProviderErrorResource.UnknownErrorCode, ex);
            }

            return GetStarbucksCardById(userId, newCardId);
        }

        public IEnumerable<ICardRegistrationStatus> RegisterMultipleStarbucksCards(string userId, IEnumerable<IStarbucksCardNumberAndPin> cards)
        {
            //bool hasFailures = false;
            var multipleCardRegistrationStatus = new List<ICardRegistrationStatus>();
            foreach (var card in cards)
            {
                var currentCard = card;
                var registrationStatus = new CardRegistrationStatus { CardNumber = currentCard.CardNumber, Successful = false };
                CardService.Use(client =>
                {
                    try
                    {
                        registrationStatus.CardId = client.RegisterCardByNumberUsingDefaultAddress(
                            userId,
                            currentCard.CardNumber,
                            currentCard.CardPin,
                            string.Empty,
                            CultureContext.CurrentSubMarket,
                            false);

                        registrationStatus.Code = "0";
                        registrationStatus.Successful = true;
                    }
                    catch (Exception ex)
                    {
                        //hasFailures = true;
                        var webFault = ex.ToWebFaultException();
                        registrationStatus.Successful = false;
                        registrationStatus.Code = webFault.Detail.Code;
                        registrationStatus.Message = webFault.Detail.Message;
                        // Don't throw it.. just move along and do the next one so we can return a full set
                        // TODO:
                        // Log this?
                    }
                    multipleCardRegistrationStatus.Add(registrationStatus.ToModel());
                });

            }

            //TODO Factor up in the Service call
            //if (hasFailures)
            //{
            //    var accountDataProvider = new Starbucks.Platform.Dal.AccountManagement.Sql.SqlAccountDataProvider(ProfilesConnectionStringname);
            //    var user = accountDataProvider.GetUserAccount(userId);
            //    try
            //    {
            //        EmailHelper.SendEmailForMultipleRegistrationFailures(user.UserName, user.EmailAddress, multipleCardRegistrationStatus);
            //    }
            //    catch (Exception ex)
            //    {
            //        throw ex.ToWebFaultException();
            //    }

            //}

            return multipleCardRegistrationStatus;
        }

        public IStarbucksCardUnregisterResult UnregisterStarbucksCard(string userId, string cardId)
        {
            ResponseStatus responseStatus = null;
            CardService.Use(client =>
            {
                try
                {
                    responseStatus = client.UnregisterCardWithStatus(userId, cardId, false);
                }
                catch (Exception ex)
                {
                    //throw ex.ToWebFaultException(new Dictionary<string, string> { { "userId", userId }, { "cardId", cardId } });
                    throw new CardProviderException(CardProviderErrorResource.GeneralPaymentErrorMessage,
                                                    CardProviderErrorResource.GeneralPaymentErrorMessage, ex);
                }

            });

            if (responseStatus.Success)
            {
                return new StarbucksCardUnregisterResult { CardId = cardId }.ToModel() ;
            }

            if (responseStatus.Errors.ContainsKey(ResponseStatusErrorCode.UnregisterCard_DigitalCardHasRemainingBalance))
            {
                throw new CardProviderValidationException(CardProviderErrorResource.UnregisterNonZeroBalanceMessage,
                                                          CardProviderErrorResource.UnregisterNonZeroBalanceCode);
            }
            //TODO Evaluate another way to handle multiple errors
            // Not a success and not the digital card error; We'll have to re-evaluate this handling but for now throw a generic error
            var errors = string.Join(",", responseStatus.Errors.Select(k => string.Format("{0}:{1}", k.Key, k.Value) ));
            throw new CardProviderException(CardProviderErrorResource.UnknownErrorMessage,
                                            CardProviderErrorResource.UnknownErrorCode , new Exception( errors) );
        }       

        public IStarbucksCardTransaction ReloadStarbucksCard(string userId, string cardId,
                                                             IReloadForStarbucksCard reloadForStarbucksCard)
        {
            CardTransaction cardTransaction = null;
            try
            {
                CardService.Use(client =>
                    {
                        var action = GetConsumerKeyFromContext() + "-ReloadStarbucksCard";
                        var isPaypal = reloadForStarbucksCard.Type.Equals("paypal",
                                                                          StringComparison.CurrentCultureIgnoreCase);

                        cardTransaction = isPaypal
                                              ? ReloadWithPaypal(userId, cardId, reloadForStarbucksCard, client, action)
                                              : ReloadWithCreditCard(userId, cardId, reloadForStarbucksCard, client,
                                                                     action);
                    });
            }
            catch (PaymentException paymentException)
            {
                throw new CardProviderException(CardProviderErrorResource.GeneralPaymentErrorMessage,
                                                CardProviderErrorResource.GeneralPaymentErrorCode, paymentException);
            }
            catch (FaultException faultException)
            {
                var cardProviderException = faultException.ToCardProviderException();
                throw cardProviderException;                
            }
            catch (Exception ex)
            {
                throw new CardProviderException(CardProviderErrorResource.UnknownErrorMessage,
                                                CardProviderErrorResource.UnknownErrorCode, ex);
            }


            return cardTransaction == null ? null : cardTransaction.ToModel() ;
        }

        //TODO implement this
        public IStarbucksCardTransaction ReloadStarbucksCardByCardNumberPin(string cardNumber, string pin,
                                                                            IReloadForStarbucksCard reloadForStarbucksCard)
        {
            CardTransaction cardTransaction = null;
            try
            {
                CardService.Use(client =>
                {
                    var action = GetConsumerKeyFromContext() + "-ReloadStarbucksCard";
                    //var isPaypal = reloadForStarbucksCard.Type.Equals("paypal",
                    //                                                  StringComparison.CurrentCultureIgnoreCase);

                    //cardTransaction = isPaypal
                    //                      ? ReloadWithPaypal(userId, cardId, reloadForStarbucksCard, client, action)
                    //                      : ReloadWithCreditCard(userId, cardId, reloadForStarbucksCard, client,
                    //                                             action);

                    
                });
            }
            catch (PaymentException paymentException)
            {
                throw new CardProviderException(CardProviderErrorResource.GeneralPaymentErrorMessage,
                                                CardProviderErrorResource.GeneralPaymentErrorCode, paymentException);
            }
            catch (FaultException faultException)
            {
                var cardProviderException = faultException.ToCardProviderException();
                throw cardProviderException;
            }
            catch (Exception ex)
            {
                throw new CardProviderException(CardProviderErrorResource.UnknownErrorMessage,
                                                CardProviderErrorResource.UnknownErrorCode, ex);
            }


            return cardTransaction.ToModel();
        }

        public IStarbucksCardTransaction ReloadStarbucksCardUsingPayPalRefTransaction(string userId, string cardId,
                                                                                      IReloadForStarbucksCard reloadForStarbucksCard)
        {
            CardTransaction cardTransaction = null;
            try
            {
                CardService.Use(client =>
                {
                    var action = GetConsumerKeyFromContext() + "-ReloadStarbucksCardUsingPayPalRefTransaction";
                    cardTransaction = client.ReloadAssociatedWithPayPalBillingId(userId, cardId, reloadForStarbucksCard.Amount,
                        reloadForStarbucksCard.PaymentMethodId, CultureContext.CurrentSubMarket, action);
                });
            }
            catch (PaymentException paymentException)
            {
                throw new CardProviderException(CardProviderErrorResource.GeneralPaymentErrorMessage,
                                                CardProviderErrorResource.GeneralPaymentErrorCode, paymentException);
            }
            catch (FaultException faultException)
            {
                var cardProviderException = faultException.ToCardProviderException();
                throw cardProviderException;                
            }
            catch (Exception ex)
            {
                throw new CardProviderException(CardProviderErrorResource.UnknownErrorMessage,
                                                CardProviderErrorResource.UnknownErrorCode, ex);
            }


            return cardTransaction.ToModel() ;
        }

        public IAutoReloadProfile SetupAutoReload(string userId, string cardId, IAutoReloadProfile autoReloadProfile)
        {
            try
            {
                return UpsertAutoReload(userId, cardId, autoReloadProfile);
            }
            catch (FaultException faultException)
            {
                var cardProviderException = faultException.ToCardProviderException();
                throw cardProviderException;               
            }
        }

        public IAutoReloadProfile UpdateAutoReload(string userId, string cardId, IAutoReloadProfile autoReloadProfile)
        {
            try
            {

                return UpsertAutoReload(userId, cardId, autoReloadProfile);
            }
            catch (FaultException faultException)
            {
                var cardProviderException = faultException.ToCardProviderException();
                throw cardProviderException;               
            }
        }
       
        public void EnableAutoReload(string userId, string cardId)
        {
            try
            {
                CardService.Use(client =>
                {
                    var card = client.GetCard(cardId);
                    if (card == null)
                        throw new CardProviderException(CardProviderErrorResource.CardNotFoundMessage,
                                                        CardProviderErrorResource.CardNotFoundCode);
                    if (!card.RegisteredUserId.Equals(userId))
                        throw new CardProviderException(CardProviderErrorResource.CardNotRegisteredToUserMessage,
                                                        CardProviderErrorResource.CardNotRegisteredToUserCode);

                    if (string.IsNullOrEmpty( card.AutoReloadId) )
                        throw new CardProviderValidationException(
                    CardProviderErrorResource.AutoReloadNotFoundMessage ,
                    CardProviderErrorResource.AutoReloadNotFoundCode);

                    client.EnableAutoReload(card.AutoReloadId, userId);
                    //autoReloadProfile = client.GetAutoReload(userId, autoReloadId).ToModel() ;
                });
                //if (CardRepository.IsAutoReloadAssociatedToPaymentMethod(autoReloadId, userId))
                //{
                //    CardRepository.UpdateAutoReloadStatus(autoReloadId, userId, AutoReloadStatus.Active);
                //    var autoReloadProfile = CardRepository.GetAutoreload(userId, autoReloadId).ToModel();
                //    // Autoreload profile doesn't include payment type, look up the payment type and add it to the profile.
                //    // IMPORTANT! it cannot be retrieved from the list of payment mehtods already retrieved because
                //    // paypal is currently filtered out of calls to get the list of user payment methods so a new
                //    // call to get that specific id must be made

                //     var arPaymentMethod = CardRepository.GetPaymentMethod(userId, autoReloadProfile.PaymentMethodId);
                //     if (arPaymentMethod != null)
                //     {
                //         autoReloadProfile.PaymentType = arPaymentMethod.PaymentType.ToString();
                //     }
                //    return autoReloadProfile;
                //}
                //IAutoReloadProfile autoReloadProfile = null;
                //CardService.Use(cs =>
                //    {
                //        cs.EnableAutoReload(autoReloadId, userId);
                //        autoReloadProfile = cs.GetAutoReload(userId, autoReloadId).ToModel();
                //    });
                //SetPaymentMethod(autoReloadProfile, userId);
                //return autoReloadProfile;

                //throw new CardProviderValidationException(
                //    CardProviderErrorResource.AutoReloadNotAssociatedToPaymentMethodMessage,
                //    CardProviderErrorResource.AutoReloadNotAssociatedToPaymentMethodCode);
            }
            catch (FaultException  faultException)
            {
                var cardProviderException = faultException.ToCardProviderException();
                throw cardProviderException;                
            }
            catch (Exception ex)
            {
                throw new CardProviderException(CardProviderErrorResource.UnknownErrorMessage,
                                               CardProviderErrorResource.UnknownErrorCode,
                                               ex);
            }

        }
      
        public void DisableAutoReload(string userId, string cardId)
        {
            try
            {
                //IAutoReloadProfile autoReloadProfile = null;

                CardService.Use(client =>
                    {
                        var card = client.GetCard(cardId);
                        if (card == null)
                            throw new CardProviderException(CardProviderErrorResource.CardNotFoundMessage,
                                                            CardProviderErrorResource.CardNotFoundCode);
                        if (!card.RegisteredUserId.Equals(userId))
                            throw new CardProviderException(CardProviderErrorResource.CardNotRegisteredToUserMessage,
                                                            CardProviderErrorResource.CardNotRegisteredToUserCode);
                        client.DisableAutoReload(card.AutoReloadId , userId, DateTime.Now.AddYears(100));
                        //autoReloadProfile = client.GetAutoReload(userId, autoReloadId).ToModel() ;
                    });
                                                
                // Autoreload profile doesn't include payment type, look up the payment type and add it to the profil, e.
                // IMPORTANT! it cannot be retrieved from the list of payment mehtods already retrieved because
                // paypal is currently filtered out of calls to get the list of user payment methods so a new
                // call to get that specific id must be made

                //var arPaymentMethod = CardRepository.GetPaymentMethod(userId, autoReloadProfile.PaymentMethodId);
                //if (arPaymentMethod != null)
                //{
                //    autoReloadProfile.PaymentType = arPaymentMethod.PaymentType.ToString();
                //}
                //SetPaymentMethod(autoReloadProfile, userId);
                //return autoReloadProfile;
            }
            catch (FaultException faultException)
            {
                var cardProviderException = faultException.ToCardProviderException();
                throw cardProviderException;                
            }
            catch (Exception ex)
            {
                throw new CardProviderException(CardProviderErrorResource.UnknownErrorMessage,
                                               CardProviderErrorResource.UnknownErrorCode,
                                               ex);
            }
        }

        public ITransferBalanceResult TransferStarbucksCardBalance(string userId, string fromCardId, string toCardId,
                                                                      decimal? amount)
        {

            ITransferBalanceResult transferResult = null;
            try
            {
                CardService.Use(client =>
                    {
                        CardTransaction result;
                        if (!amount.HasValue)
                            result = client.TransferRegisteredCardBalanceToAssociatedCard(userId, fromCardId, toCardId,
                                                                                          CultureContext
                                                                                              .CurrentSubMarket);
                        else
                        {
                            result = client.TransferRegisteredCardAmountToAssociatedCard(userId, fromCardId, toCardId,
                                                                                         amount.Value,
                                                                                         CultureContext.CurrentSubMarket);
                        }
                        if (result != null)
                        {
                            transferResult = new TransferBalanceResult {StarbucksCardTransaction = result.ToModel()};

                            var sourceBalance = client.GetBalanceRegistered(userId, fromCardId,
                                                                            CultureContext.CurrentSubMarket);


                            var destinationBalance = client.GetBalanceRegistered(userId, toCardId,
                                                                            CultureContext.CurrentSubMarket);

                            transferResult.Balances = new Dictionary<string, decimal>
                                {
                                    {fromCardId, sourceBalance},
                                    {toCardId, destinationBalance}
                                };
                        }

                    });
            }
            catch (Exception ex)
            {
                throw new CardProviderException(CardProviderErrorResource.UnknownErrorMessage,
                                                CardProviderErrorResource.UnknownErrorCode,
                                                ex);
            }

            return transferResult;
        }


        private static string GetConsumerKeyFromContext()
        {
            string consumerKey = null;
            if (WebOperationContext.Current != null)
            {
                var headers = WebOperationContext.Current.IncomingRequest.Headers;
                var authorization = headers.GetValues("Authorization");
                if (authorization != null)
                {
                    var consumerKeyValuePair = authorization.FirstOrDefault(a => a.Contains("oauth_consumer_key"));
                    if (!string.IsNullOrWhiteSpace(consumerKeyValuePair))
                    {
                        consumerKey = consumerKeyValuePair.Split('=')[1];
                        consumerKey = consumerKey.Trim(new[] { '"' });
                    }
                }
            }
            return consumerKey;
        }

        private static CardTransaction ReloadWithCreditCard(string userId, string cardId,
                                                  IReloadForStarbucksCard reloadForStarbucksCard, ICardService client, string action)
        {
            var cardTransaction = client.ReloadAssociatedUsingSavedPayment(
                userId, cardId, reloadForStarbucksCard.Amount, reloadForStarbucksCard.PaymentMethodId,
                String.Empty, CultureContext.CurrentSubMarket, action);
            return cardTransaction;
        }

        private static CardTransaction ReloadWithPaypal(string userId, string cardId,
                                                        IReloadForStarbucksCard reloadForStarbucksCard, ICardService client, string action)
        {
            var cardTransaction = client.ReloadAssociatedWithPayPalToken(
                userId, cardId, reloadForStarbucksCard.Amount, reloadForStarbucksCard.PaymentMethodId,
                CultureContext.CurrentSubMarket, action);
            return cardTransaction;
        }

        //TODO Need billing info here
        //private static CardTransaction ReloadWithCardNumber(string cardNumber, string pin,
        //                                       IReloadForStarbucksCard reloadForStarbucksCard, ICardService client, string action)
        //{

        //    var cardTransaction = client.ReloadByNumberPinless(  
        //        cardNumber,  reloadForStarbucksCard.Amount, reloadForStarbucksCard.PaymentMethodId,
        //        String.Empty, CultureContext.CurrentSubMarket, action);
        //    return cardTransaction;
        //}

        //private static CardTransaction ReloadWithCardNumberPaypal(string cardNumber, string pin,
        //                                                IReloadForStarbucksCard reloadForStarbucksCard, ICardService client, string action)
        //{
        //    var cardTransaction = client.ReloadAssociatedWithPayPalToken(
        //        userId, cardId, reloadForStarbucksCard.Amount, reloadForStarbucksCard.PaymentMethodId,
        //        CultureContext.CurrentSubMarket, action);
        //    return cardTransaction;
        //}

        private IAutoReloadProfile UpsertAutoReload(string userId, string cardId, IBaseAutoReloadProfile autoReloadProfile)
        {
            //Starbucks.Platform.Bom.Shared.PaymentMethod paymentMethod = null;
            IAutoReloadProfile returnValue = null;

            CardService.Use(client =>
            {
                string autoReloadId;
                string billingAgreementId = (autoReloadProfile.GetType() == typeof(IAutoReloadProfile) ? ((Model.AutoReloadProfile)autoReloadProfile).BillingAgreementId : null);

                // default to the supplied PaymentMethodId assuming that PaymentType != "PayPal"
                string paymentMethodId = autoReloadProfile.PaymentMethodId;

                //TODO This logic should go away with new paypal imp
                // Handle the PayPal case               
                //if (autoReloadProfile.PaymentType.Equals("PayPal", StringComparison.InvariantCultureIgnoreCase))
                //{
                //    // card will drives the currency
                //    if (autoReloadProfile.GetType() == typeof(SetupAutoReloadProfile))
                //        billingAgreementId = client.CreatePayPalBillingAgreement(((ISetupAutoReloadProfile)autoReloadProfile).PayPalECToken, cardId);

                //    // Create a new PaymentMethod using the BillingAgreementId as the AccountNumber
                //    paymentMethod = new Starbucks.Platform.Bom.Shared.PaymentMethod
                //    {
                //        Nickname = "PayPal",
                //        PaymentType = PaymentType.PayPal,
                //        AccountNumber = billingAgreementId,
                //        UserId = userId
                //    };

                   
                //    // save the PaymentMethod and get the new paymentMethodId to use for setting autoreload
                //    AccountService.Use(accountClient =>
                //    {
                //        paymentMethodId = accountClient.SavePaymentMethod(paymentMethod, CreateToken(userId));
                //    });
                //}
                //else
                //{
                //    // just retrieve the pre-existing PaymentMethod
                //    paymentMethod = CardRepository.GetPaymentMethod(userId, paymentMethodId);
                //}

                if (autoReloadProfile.AutoReloadType.Equals("Amount", StringComparison.InvariantCultureIgnoreCase))
                {
                    autoReloadId = client.SetAutoReloadAmountBasedWithPaymentMethod(
                        userId,
                        cardId,
                        autoReloadProfile.Amount,
                        autoReloadProfile.TriggerAmount.Value,
                        paymentMethodId,
                        false,
                        null);
                }
                else
                {
                    autoReloadId = client.SetAutoReloadWithPaymentMethod(
                        userId,
                        cardId,
                        autoReloadProfile.Amount,
                        autoReloadProfile.Day.Value,
                        paymentMethodId,
                        false,
                        null);
                }

                returnValue  = client.GetAutoReload(userId, autoReloadId).ToModel() ;

                //returnValue = CardRepository.GetAutoreload(userId, autoReloadId).ToModel()  ;
                

                // Autoreload profile doesn't include payment type, look up the payment type and add it to the profile.
                // IMPORTANT! it cannot be retrieved from the list of payment mehtods already retrieved because
                // paypal is currently filtered out of calls to get the list of user payment methods so a new
                // call to get that specific id must be made

                //Factored to controller to use PaymentProvider
                //SetPaymentMethod(returnValue, userId);

                // add in billingAgreementId if this was a paypal autoreload setup request
                if (billingAgreementId != null)
                    returnValue.BillingAgreementId = billingAgreementId;
            });

            return returnValue;
        }

        private static IAutoReloadProfile GetAutoReloadProfile(string userId, string autoReloadId)
        {
            IAutoReloadProfile autoReloadProfile = null;
            CardService.Use(client => autoReloadProfile = client.GetAutoReload(userId, autoReloadId).ToModel() );
            return autoReloadProfile;
        }

        //private static void SetPaymentMethod(IAutoReloadProfile autoReloadProfile , string userId )
        //{
        //    var arPaymentMethod = new PaymentMethod(); // CardRepository.GetPaymentMethod(userId, autoReloadProfile.PaymentMethodId);
        //    if (arPaymentMethod != null)
        //    {
        //        //autoReloadProfile.PaymentType = arPaymentMethod.PaymentType.ToString();
        //    }
        //}
    }
  
}
