﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Starbucks.Card.Dal.WCF.Model;
using Starbucks.Card.Dal.WCF.Settings;
using Starbucks.Card.Dal.Wcf.Model;
using Starbucks.Card.Provider.Common.Enums;
using Starbucks.Card.Provider.Common.ErrorResources;
using Starbucks.Card.Provider.Common.Exceptions;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.Platform.Bom.Shared;
using Starbucks.Platform.Exceptions;
using AutoReloadProfile = Starbucks.Card.Dal.WCF.Model.AutoReloadProfile;
using CardPromotion = Starbucks.Card.Dal.Wcf.Model.CardPromotion;
using CardTransaction = Starbucks.Card.Dal.Wcf.Model.CardTransaction;
using StarbucksCard = Starbucks.Card.Dal.WCF.Model.StarbucksCard;
using StarbucksCardImage = Starbucks.Card.Dal.WCF.Model.StarbucksCardImage;
using StarbucksCardTransaction = Starbucks.Card.Dal.WCF.Model.StarbucksCardTransaction;
using VisibilityLevel = Starbucks.Card.Provider.Common.Enums.VisibilityLevel;

namespace Starbucks.Card.Dal.WCF
{
    public static class Converters
    {
       //public static CardProviderException ToCardProviderException(this FaultException faultException)
       // {
       //     int faultCode ;
       //     int.TryParse(faultException.Code.Name , out faultCode  );           

       //     switch ((KnownFault) faultCode)
       //     {
       //         case KnownFault.Payment_method_not_associated_to_account :
       //             return new CardProviderException(CardProviderErrorResource.AutoReloadNotAssociatedToPaymentMethodCode, CardProviderErrorResource.AutoReloadNotAssociatedToPaymentMethodMessage, faultException);
       //         case KnownFault.Card_is_already_registered:
       //             return new CardProviderException(CardProviderErrorResource.CardIsAlreadyRegisteredCode, CardProviderErrorResource.CardIsAlreadyRegisteredMessage, faultException);
       //         case KnownFault.Card_is_not_registered_to_user :
       //             return new CardProviderException(CardProviderErrorResource.CardNotRegisteredToUserCode, CardProviderErrorResource.CardIsAlreadyRegisteredMessage, faultException);
       //         case KnownFault.Insufficient_funds_for_transaction:
       //             //Insufficient funds
       //             return new CardProviderException(CardProviderErrorResource.InsufficientFundsForTransactionCode, CardProviderErrorResource.InsufficientFundsForTransactionMessage, faultException);
       //         case KnownFault.Credit_card_number_does_not_match_type :
       //             return new CardProviderException(CardProviderErrorResource.CreditCardNumberDoesNotMatchTypeCode, CardProviderErrorResource.CreditCardNumberDoesNotMatchTypeMessage, faultException);
       //         case KnownFault.Credit_card_number_is_invalid :
       //             return new CardProviderException(CardProviderErrorResource.CreditCardNumberInvalidMessage, CardProviderErrorResource.CreditCardNumberInvalidMessage, faultException);
       //         case KnownFault.General_payment_error :
       //             return new CardProviderException(CardProviderErrorResource.GeneralPaymentErrorCode, CardProviderErrorResource.GeneralPaymentErrorMessage, faultException);
                
       //         case KnownFault.No_registration_address_on_file:
       //             return new CardProviderException(CardProviderErrorResource.NoRegistrationAddressOnFileCode, CardProviderErrorResource.NoRegistrationAddressOnFileMessage, faultException);
       //         case KnownFault.Account_is_closed:
       //             //Account closed
       //             return new CardProviderException(CardProviderErrorResource.AccountIsClosedCode, CardProviderErrorResource.AccountIsClosedMessage, faultException);
       //         case KnownFault.Invalid_card_number:
       //             //Unknown account
       //             return new CardProviderException(CardProviderErrorResource.InvalidCardNumberCode, CardProviderErrorResource.InvalidCardNumberMessage, faultException);
       //         case KnownFault.Card_is_inactive:
       //             //Inactive account
       //             return new CardProviderException(CardProviderErrorResource.CardIsInactiveCode, CardProviderErrorResource.CardIsInactiveMessage, faultException);
       //         //knownFault = KnownFault.Card_is_inactive;
       //         //break;
       //         case KnownFault.Card_reported_as_lost_or_stolen:
       //             //Lost or stolen
       //             return new CardProviderException(CardProviderErrorResource.CardReportedLostStolenCode, CardProviderErrorResource.CardReportedLostStolenMessage, faultException);
       //         //knownFault = KnownFault.Card_reported_as_lost_or_stolen;
       //         //break;
       //         case KnownFault.Maximum_balance_exceeded:
       //             //Max balance
       //             return new CardProviderException(CardProviderErrorResource.MaximumBalanceExceededCode, CardProviderErrorResource.MaximumBalanceExceededMessage, faultException);
       //         //knownFault = KnownFault.Maximum_balance_exceeded;
       //         //break;
       //         case KnownFault.InvalidAmount_MoreOrLess_than_MinOrMax_amount_specified:
       //             return new CardProviderException(CardProviderErrorResource.InvalidAmountMoreOrLessThanMinOrMaxAmountSpecifiedCode, CardProviderErrorResource.InvalidAmountMoreOrLessThanMinOrMaxAmountSpecifiedMessage, faultException);
       //         //knownFault = KnownFault.InvalidAmount_MoreOrLess_than_MinOrMax_amount_specified;
       //         //break;
       //         case KnownFault.Request_not_permitted_by_this_account:
       //             //Request not permitted by this account
       //             return new CardProviderException(CardProviderErrorResource.RequestNotPermittedByThisAccountCode, CardProviderErrorResource.RequestNotPermittedByThisAccountMessage, faultException);
       //         //knownFault = KnownFault.Request_not_permitted_by_this_account;
       //         //break;
       //         case KnownFault.Card_reported_lost_or_stolen_internet_disabled:
       //             //Lost or stolen (disabled)
       //             return new CardProviderException(CardProviderErrorResource.CardReportedLostStolenInternetDisabledCode, CardProviderErrorResource.CardReportedLostStolenInternetDisabledMessage, faultException);
       //         //knownFault = KnownFault.Card_reported_lost_or_stolen_internet_disabled;
       //         //break;
       //         case KnownFault.Invalid_PIN:
       //             //Invalid PIN
       //             return new CardProviderException(CardProviderErrorResource.InvalidPinCode, CardProviderErrorResource.InvalidPinMessage, faultException);
       //         //knownFault = KnownFault.Invalid_PIN;
       //         //break;
       //         case KnownFault.ValueLink_host_down:
       //             //Interactive and VL timeout
       //             return new CardProviderException(CardProviderErrorResource.ValueLinkHostDownCode, CardProviderErrorResource.ValueLinkHostDownMessage, faultException);
       //         //knownFault = KnownFault.ValueLink_host_down;
       //         //break;
       //         case KnownFault.Invalid_submarket_code :
       //             return new CardProviderException(CardProviderErrorResource.InvalidSubMarketMessage, CardProviderErrorResource.InvalidSubMarketCode, faultException);
       //         default:
       //             return new CardProviderException(CardProviderErrorResource.UnknownErrorCode, CardProviderErrorResource.UnknownErrorMessage, faultException);
       //     }
       // }

        public static Platform.Bom.Shared.VisibilityLevel ToServer(this VisibilityLevel visibilityLevel)
        {
            switch (visibilityLevel)
            {
                case VisibilityLevel.Decrypted:
                    return Platform.Bom.Shared.VisibilityLevel.Decrypted;
                case VisibilityLevel.Encrypted:
                    return Platform.Bom.Shared.VisibilityLevel.Encrypted;
                case VisibilityLevel.Masked:
                    return Platform.Bom.Shared.VisibilityLevel.Masked;
                case VisibilityLevel.None:
                    return Platform.Bom.Shared.VisibilityLevel.None;
                 default :
                    return Platform.Bom.Shared.VisibilityLevel.None;
            }
        }

        public static AutoReloadProfile ToModel(
            this Platform.Bom.AutoReload  serverAutoReloadProfile)
        {
            return new AutoReloadProfile
                {
                    Amount = serverAutoReloadProfile.Amount,
                    AutoReloadId = serverAutoReloadProfile.AutoReloadId,
                    AutoReloadType = GetAutoReloadType(serverAutoReloadProfile.Type),
                    //BillingAgreementId = serverAutoReloadProfile.BillingAgreementId,
                    CardId = serverAutoReloadProfile.CardId,
                    Day = serverAutoReloadProfile.Day,
                    DisableUntilDate = serverAutoReloadProfile.DisableUntilDate,
                    PaymentMethodId = serverAutoReloadProfile.PaymentMethodId,
                    //PaymentType = serverAutoReloadProfile.PaymentType,
                   // Status = GetAutoReloadStatus(serverAutoReloadProfile.Status),
                    StoppedDate = serverAutoReloadProfile.StoppedDate,
                    TriggerAmount = serverAutoReloadProfile.TriggerAmount,
                    UserId = serverAutoReloadProfile.UserId
                };
        }

        private static string GetAutoReloadType(Platform.Bom.AutoReloadType autoReloadType)
        {
            switch (autoReloadType)
            {
                case Platform.Bom.AutoReloadType.Amount:
                    return "Amount";
                case Platform.Bom.AutoReloadType.Date:
                    return "Date";
                default:
                    return "None";
            }
        }

/*
        private static string GetAutoReloadStatus(Platform.Bom.AutoReloadStatus autoReloadStatus)
        {
            switch (autoReloadStatus)
            {
                case Platform.Bom.AutoReloadStatus.Active:
                    return "Active";
                case Platform.Bom.AutoReloadStatus.Disabled:
                    return "Disabled";
                case Platform.Bom.AutoReloadStatus.Inactive:
                    return "Inactive";
                case Platform.Bom.AutoReloadStatus.Suspended:
                    return "Suspended";
                case Platform.Bom.AutoReloadStatus.Temporary:
                    return "Temporary";
                default:
                    return "None";
            }
        }
*/

        //public static Api.Services.Profile.DataContracts.AutoReloadProfile  ToServer(
        //    this  IAutoReloadProfile autoReloadProfile)
        //{
        //    return new Api.Services.Profile.DataContracts.AutoReloadProfile
        //    {
        //        Amount = autoReloadProfile.Amount,
        //        AutoReloadId = autoReloadProfile.AutoReloadId,
        //        AutoReloadType = autoReloadProfile.AutoReloadType,
        //        BillingAgreementId = autoReloadProfile.BillingAgreementId,
        //        CardId = autoReloadProfile.CardId,
        //        Day = autoReloadProfile.Day,
        //        DisableUntilDate = autoReloadProfile.DisableUntilDate,
        //        PaymentMethodId = autoReloadProfile.PaymentMethodId,
        //        PaymentType = autoReloadProfile.PaymentType,
        //        Status = autoReloadProfile.Status,
        //        StoppedDate = autoReloadProfile.StoppedDate,
        //        TriggerAmount = autoReloadProfile.TriggerAmount,
        //        UserId = autoReloadProfile.UserId
        //    };
        //}

        public static StarbucksCard ToModel(
            this Platform.Bom.Shared.Card  card)
        {
            return new StarbucksCard
                {
                    CardId = card.CardId,
                    CardNumber = card.Number,
                    // if the card has a Nickname, return it. Otherwise, use the last 4 digits.
                    //Nickname = card.Name == string.Empty ? string.Format("My Card ({0})", card.Number.Substring(12, 4)) : card.Name,
                    CardClass = card.Class,
                    CardType = card.Type.ToString(),
                    CurrencyCode = card.Currency,
                    BalanceCurrencyCode = card.BalanceCurrency,
                    SubmarketCode = card.SubMarket.SubMarketCode,
                    Balance = card.Balance,
                    BalanceDate = card.BalanceDate,
                    CardImages = card.ToStarbucksCardImage(),                   
                    RegisteredUserId = card.RegisteredUserId,
                   // IsDefaultCard = false,
                    IsPartnerCard = IsPartnerCard(card),
                   // IsDigitalCard = card.IsDigitalCard,
                    AutoReloadId = card.AutoReloadId 
                   
                };
        }

        public static StarbucksCard ToModel(
            this AssociatedCard  card)
        {
            return new StarbucksCard
            {
                CardId = card.CardId,
                CardNumber = card.Number,
                // if the card has a Nickname, return it. Otherwise, use the last 4 digits.
                Nickname = card.NickName == string.Empty ? string.Format("My Card ({0})", card.Number.Substring(12, 4)) : card.NickName ,
                CardClass = card.Class,
                CardType = card.Type.ToString(),
                CurrencyCode = card.Currency,
                BalanceCurrencyCode = card.BalanceCurrency,
                SubmarketCode = card.SubMarket.SubMarketCode,                               
                CardImages = card.ToStarbucksCardImage(),
                RegisteredUserId = card.RegisteredUserId,
                IsDefaultCard = card.Default ,
                IsPartnerCard = IsPartnerCard(card),
                IsDigitalCard = card.IsDigitalCard,
                AutoReloadId = card.AutoReloadId,
                RegistrationDate = card.RegistrationDate 

            };
        }

        private static bool IsPartnerCard(Platform.Bom.Shared.ICard card)
        {
            return (card.Class.Equals("69") || card.Class.Equals("29") || card.Class.Equals("32"));
        }

        public static List<IStarbucksCardImage> ToStarbucksCardImage(
            this Platform.Bom.Shared.Card card)
        {
            return new List<IStarbucksCardImage>
                {
                    new StarbucksCardImage
                        {
                            Type = CardImageType.ImageIcon,
                            Uri =
                                string.Format(CardProviderSettingsSection.Settings.StarbucksCardWebImagePaths.Icon,
                                              card.Name)
                        },
                         new StarbucksCardImage
                        {
                            Type = CardImageType.ImageSmall,
                            Uri =
                                string.Format(CardProviderSettingsSection.Settings.StarbucksCardWebImagePaths.Small,
                                              card.Name)
                        }
                        ,
                         new StarbucksCardImage
                        {
                            Type = CardImageType.ImageMedium,
                            Uri =
                                string.Format(CardProviderSettingsSection.Settings.StarbucksCardWebImagePaths.Medium,
                                              card.Name)
                        }
                        ,
                         new StarbucksCardImage
                        {
                            Type = CardImageType.ImageLarge,
                            Uri =
                                string.Format(CardProviderSettingsSection.Settings.StarbucksCardWebImagePaths.Large,
                                              card.Name)
                        }
                        ,
                         new StarbucksCardImage
                        {
                            Type = CardImageType.iosThumb ,
                            Uri =
                                string.Format(CardProviderSettingsSection.Settings.StarbucksCardMobileImagePaths.Icon,
                                              card.Name)
                        }
                        ,
                         new StarbucksCardImage
                        {
                            Type = CardImageType.iosThumbHighRes ,
                            Uri =
                                string.Format(CardProviderSettingsSection.Settings.StarbucksCardMobileImagePaths.Small,
                                              card.Name)
                        }
                        ,
                         new StarbucksCardImage
                        {
                            Type = CardImageType.iosLarge ,
                            Uri =
                                string.Format(CardProviderSettingsSection.Settings.StarbucksCardMobileImagePaths.Medium ,
                                              card.Name)
                        }
                        ,
                         new StarbucksCardImage
                        {
                            Type = CardImageType.iosLargeHighRes,
                            Uri =
                                string.Format(CardProviderSettingsSection.Settings.StarbucksCardMobileImagePaths.Large ,
                                              card.Name)
                        }   
                        ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.androidFullMdpi,
                            Uri =
                                string.Format(CardProviderSettingsSection.Settings.StarbucksCardAndroidImagePaths.Icon,
                                              card.Name)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.androidFullHdpi,
                            Uri =
                                string.Format(CardProviderSettingsSection.Settings.StarbucksCardAndroidImagePaths.Small,
                                              card.Name)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.androidFullXhdpi,
                            Uri =
                                string.Format(CardProviderSettingsSection.Settings.StarbucksCardAndroidImagePaths.Medium,
                                              card.Name)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.androidFullXxhdpi,
                            Uri =
                                string.Format(CardProviderSettingsSection.Settings.StarbucksCardAndroidImagePaths.Large,
                                              card.Name)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.androidThumbMdpi,
                            Uri =
                                string.Format(CardProviderSettingsSection.Settings.StarbucksCardAndroidThumbImagePaths.Icon,
                                              card.Name)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.androidThumbHdpi,
                            Uri =
                                string.Format(CardProviderSettingsSection.Settings.StarbucksCardAndroidThumbImagePaths.Small,
                                              card.Name)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.androidThumbXhdpi,
                            Uri =
                                string.Format(CardProviderSettingsSection.Settings.StarbucksCardAndroidThumbImagePaths.Medium,
                                              card.Name)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.androidThumbXxhdpi,
                            Uri =
                                string.Format(CardProviderSettingsSection.Settings.StarbucksCardAndroidThumbImagePaths.Large,
                                              card.Name)
                        }                   
                };            
        }

        //public static StarbucksCardStatus ToModel(
        //    this Api.Services.Profile.DataContracts.StarbucksCardStatus serverStarbucksCardStatus)
        //{
        //    return new StarbucksCardStatus
        //        {
        //            CardId = serverStarbucksCardStatus.CardId,
        //            Valid = serverStarbucksCardStatus.Valid
        //        };
        //}

        //public static StarbucksCardBalance ToModel(
        //    this Api.Services.Profile.DataContracts.StarbucksCardBalance starbucksCardBalance)
        //{
        //    return new StarbucksCardBalance
        //        {
        //            Balance = starbucksCardBalance.Balance,
        //            BalanceCurrencyCode = starbucksCardBalance.BalanceCurrencyCode,
        //            BalanceDate = starbucksCardBalance.BalanceDate,
        //            CardId = starbucksCardBalance.CardId
        //        };
        //}

        //public static CardRegistrationStatus ToModel(
        //    this Api.Services.Profile.DataContracts.CardRegistrationStatus cardRegistrationStatus)
        //{
        //    return new CardRegistrationStatus
        //        {
        //            CardId = cardRegistrationStatus.CardId,
        //            CardNumber = cardRegistrationStatus.CardNumber,
        //            Code = cardRegistrationStatus.Code,
        //            Message = cardRegistrationStatus.Message,
        //            Successful = cardRegistrationStatus.Successful
        //        };
        //}

        //public static StarbucksCardUnregisterResult ToModel(
        //    this Api.Services.Profile.DataContracts.StarbucksCardUnregisterResult
        //        starbucksCardUnregisterResult)
        //{
        //    return new StarbucksCardUnregisterResult
        //        {
        //            CardId = starbucksCardUnregisterResult.CardId
        //        };
        //}

        public static ActivateAndReloadCardResponse ToModel(
            this Services.CardManagement.ActivateAndReloadCardResponse activateAndReloadCardResponse)
        {
            if (activateAndReloadCardResponse == null) throw new ArgumentNullException("activateAndReloadCardResponse");

            return new ActivateAndReloadCardResponse
                {
                    CardActivationTransaction =
                        activateAndReloadCardResponse.CardActivationTransaction.ToModelCardTransaction(),
                    CardReloadTransaction = activateAndReloadCardResponse.CardReloadTransaction.ToModelCardTransaction()
                };
        }

        public static ICardTransaction ToModelCardTransaction(
            this Platform.Bom.Shared.CardTransaction cardTransaction)
        {
            if (cardTransaction == null) throw new ArgumentNullException("cardTransaction");
            return new CardTransaction
            {                
                Amount = cardTransaction.Amount,
                CardId = cardTransaction.CardId,
                Currency = cardTransaction.Currency,
                Description = cardTransaction.Description,
                TransactionDate = cardTransaction.TransactionDate,
                TransactionId = cardTransaction.TransactionId,
                AmountInBaseCurrency = cardTransaction.AmountInBaseCurrency ,
                AuthorizationCode = cardTransaction.AuthorizationCode ,
                BaseCurrency = cardTransaction.BaseCurrency ,
                BeginingBalanceInBaseCurrency = cardTransaction.BeginingBalanceInBaseCurrency ,
                BeginningBalance = cardTransaction.BeginningBalance ,
                CardClass = cardTransaction.CardClass ,
                CardNumber = cardTransaction.CardNumber ,
                EndingBalance = cardTransaction.EndingBalance ,
                EndingBalanceInBaseCurrency = cardTransaction.EndingBalanceInBaseCurrency ,
                Pin = cardTransaction.Pin ,
                Promotion = cardTransaction.Promotion.ToModel() ,
                RequestCode = cardTransaction.RequestCode ,
                ResponseCode = cardTransaction.ResponseCode 
                
            };
        }

        public static ICardPromotion ToModel(this Platform.Bom.Shared.CardPromotion cardPromotion)
        {
            if (cardPromotion == null) return new CardPromotion();
            return new CardPromotion
                {
                    Amount = cardPromotion.Amount,
                    Code = cardPromotion.Code,
                    Message = cardPromotion.Message
                };
        }

        public static StarbucksCardTransaction ToModel(
            this Platform.Bom.Shared.CardTransaction starbucksCardTransaction)
        {
            if (starbucksCardTransaction == null) throw new ArgumentNullException("starbucksCardTransaction");
            return new StarbucksCardTransaction
                {
                    Amount = starbucksCardTransaction.Amount,
                    CardId = starbucksCardTransaction.CardId,
                    CurrencyCode = starbucksCardTransaction.Currency,
                    Description = starbucksCardTransaction.Description,
                    TransactionDate = starbucksCardTransaction.TransactionDate,
                    TransactionId = starbucksCardTransaction.TransactionId
                };
        }

        public static StarbucksCardHeader ToServer(
            this IStarbucksCardHeader starbucksCardHeader)
        {
            var cardHeader = new StarbucksCardHeader
            {
                CardNumber = starbucksCardHeader.CardNumber,
                CardPin = starbucksCardHeader.CardPin,
                Nickname = starbucksCardHeader.Nickname,
                Primary = starbucksCardHeader.Primary,
                Submarket = starbucksCardHeader.Submarket
            };
            cardHeader.RegistrationSource = new CardRegistrationSource();
            if (starbucksCardHeader.RegistrationSource != null)
            {
                cardHeader.RegistrationSource.Platform = starbucksCardHeader.RegistrationSource.Platform;
                cardHeader.RegistrationSource.Marketing = starbucksCardHeader.RegistrationSource.Marketing;
            }
            return cardHeader;
        }
        
        public static ReloadForStarbucksCard ToServer(
            this IReloadForStarbucksCard reloadForStarbucksCard)
        {
            return new ReloadForStarbucksCard
                {
                    Amount = reloadForStarbucksCard.Amount,
                    SessionId  = reloadForStarbucksCard.SessionId ,                    
                    PaymentMethodId = reloadForStarbucksCard.PaymentMethodId,
                    Type = reloadForStarbucksCard.Type
                };
        }

        public static SetupAutoReloadProfile ToServer(
            this ISetupAutoReloadProfile setupAutoReloadProfile)
        {
            return new SetupAutoReloadProfile
                {
                    Amount = setupAutoReloadProfile.Amount,
                    AutoReloadType = setupAutoReloadProfile.AutoReloadType,
                    CardId = setupAutoReloadProfile.CardId,
                    Day = setupAutoReloadProfile.Day,
                    PaymentMethodId = setupAutoReloadProfile.PaymentMethodId,
                    PaymentType = setupAutoReloadProfile.PaymentType,
                    PayPalEcToken = setupAutoReloadProfile.PayPalEcToken,
                    TriggerAmount = setupAutoReloadProfile.TriggerAmount,
                    UserId = setupAutoReloadProfile.UserId
                };
        }
    }


}
