﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.Card.Dal.Common.Models;
using Starbucks.Card.Dal.Sql.EntLibLess.Configuration;
using Starbucks.Card.Dal.Sql.EntLibLess.Models;
using Starbucks.Platform.Security;

namespace Starbucks.Card.Dal.Sql.EntLibLess
{
    public class SelectedCardDal : ISelectedCardDal
    {
        private readonly CardDalEntLibLessSqlSettingsSection _cardDalEntLibLessSqlSettingsSection;

        public SelectedCardDal(CardDalEntLibLessSqlSettingsSection cardDalEntLibLessSqlSettingsSection)
        {
            if (cardDalEntLibLessSqlSettingsSection == null)
                throw new ArgumentNullException("cardDalEntLibLessSqlSettingsSection");
            _cardDalEntLibLessSqlSettingsSection = cardDalEntLibLessSqlSettingsSection;
        }

        private string ConnectionString
        {
            get
            {
                return
                    ConfigurationManager.ConnectionStrings[_cardDalEntLibLessSqlSettingsSection.ConnectionStringName]
                        .ConnectionString;
            }
        }

        public IEnumerable<string> GetSelectedCards(string userId, string clientId)
        {
            if (userId == null)
            {
                throw new ArgumentNullException("userId");
            }
            if (clientId == null)
            {
                throw new ArgumentNullException("clientId");
            }

            var cards = new List<string>();
            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand("SelectedCards_Get", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@userID", userId);
                    command.Parameters.AddWithValue("@clientID", clientId);
                    connection.Open();
                    using (IDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            string card = Encryption.EncryptCardId(reader.GetInt32(reader.GetOrdinal("CardId")));
                            cards.Add(card);
                        }
                    }
                }
                return cards;
            }
        }


        public void AssociateSelectedCardsWithClient(string userId, string clientId, IEnumerable<string> cardIds)
        {
            ExecuteNonQuery(userId, clientId, GetDataTable(cardIds), "SelectedCards_Upsert");
        }

        public void RemoveSelectedCards(string userId, string clientId, IEnumerable<string> cardIds)
        {
            ExecuteNonQuery(userId, clientId, GetDataTable(cardIds), "SelectedCards_Disconnect");
        }

        public bool IsCardLinked(string userId, string cardId)
        {
            if (userId == null)
            {
                throw new ArgumentNullException("userId");
            }
            if (cardId == null)
            {
                throw new ArgumentNullException("cardId");
            }

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand("SelectedCards_Linked", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@userID", userId);
                    command.Parameters.AddWithValue("@cardID", Encryption.DecryptCardId(cardId));
                    connection.Open();
                    var result = (int) command.ExecuteScalar();

                    return result > 0;
                }
            }
        }

        public IEnumerable<ISelectedCard> GetSelectedCards(string userId)
        {
            if (userId == null)
            {
                throw new ArgumentNullException("userId");
            }

            var cards = new List<ISelectedCard>();
            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand("SelectedCards_GetAll", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@userID", userId);
                    connection.Open();
                    using (IDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            ISelectedCard card = new SelectedCard
                                {
                                    CardId = Encryption.EncryptCardId(reader.GetInt32(reader.GetOrdinal("CardId"))),
                                    ClientId = reader.GetString(reader.GetOrdinal("ClientId")),
                                    UserId = reader.GetString(reader.GetOrdinal("UserId"))
                                };
                            cards.Add(card);
                        }
                    }
                }
                return cards;
            }
        }

        private void ExecuteNonQuery(string userId, string clientId, DataTable cards, string commandName)
        {
            if (userId == null)
            {
                throw new ArgumentNullException("userId");
            }
            if (clientId == null)
            {
                throw new ArgumentNullException("clientId");
            }
            if (cards == null || cards.Rows.Count == 0)
            {
                throw new ArgumentNullException("cards");
            }

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(commandName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@userID", userId);
                    command.Parameters.AddWithValue("@clientID", clientId);
                    SqlParameter cardsParam = command.Parameters.AddWithValue("@selectedCards", cards);
                    cardsParam.SqlDbType = SqlDbType.Structured;
                    connection.Open();

                    command.ExecuteNonQuery();
                }
            }
        }

        private static DataTable GetDataTable(out DataColumn[] dataColumns)
        {
            var cards = new DataTable();
            dataColumns = new[]
                {
                    new DataColumn {ColumnName = "CardId", DataType = typeof (int)},
                    new DataColumn {ColumnName = "NewCardId", DataType = typeof (string)}
                };
            cards.Columns.AddRange(dataColumns);
            return cards;
        }

        private static DataTable GetDataTable(IEnumerable<string> cardIds)
        {
            DataColumn[] dataColumns;
            DataTable cards = GetDataTable(out dataColumns);

            foreach (string cardId in cardIds.Distinct())
            {
                DataRow row = cards.NewRow();
                row[dataColumns[0]] = Encryption.DecryptCardId(cardId);
                row[dataColumns[1]] = Encryption.DecryptCardId(cardId);
                cards.Rows.Add(row);
            }
            return cards;
        }
    }
}