﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.Card.Dal.Common.Models;
using Starbucks.Card.Dal.Sql.EntLibLess.Configuration;
using Starbucks.Card.Dal.Sql.EntLibLess.Models;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.Platform.Security;
using IAutoReloadProfile = Starbucks.Card.Dal.Common.Models.IAutoReloadProfile;
using IFraudCheckCardData = Starbucks.Card.Dal.Common.Models.IFraudCheckCardData;
using IFraudCheckUserData = Starbucks.Card.Dal.Common.Models.IFraudCheckUserData;
using Starbucks.PaymentMethod.Provider.Common.Models;

namespace Starbucks.Card.Dal.Sql.EntLibLess
{
    public class CardDal : ICardDal
    {
        private readonly CardDalEntLibLessSqlSettingsSection _cardDalEntLibLessSqlSettingsSection;

        private string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings[_cardDalEntLibLessSqlSettingsSection.ConnectionStringName].ConnectionString; }
        }

        public CardDal(CardDalEntLibLessSqlSettingsSection cardDalEntLibLessSqlSettingsSection)
        {            
            if (cardDalEntLibLessSqlSettingsSection == null)
                throw new ArgumentNullException("cardDalEntLibLessSqlSettingsSection");
            _cardDalEntLibLessSqlSettingsSection = cardDalEntLibLessSqlSettingsSection;
        }

        public IAssociatedCard GetCardByNumber(string cardNumber)
        {
            if (String.IsNullOrWhiteSpace(cardNumber))
            {
                return null;
            }

            AssociatedCard card = null;
            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand("GetCardByNumber", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@Number", Encryption.EncryptCardNumber(cardNumber));
                    connection.Open();
                    using (var reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (reader.Read())
                        {
                            card = new AssociatedCard();
                            MapCardRow(card, reader);
                            MapAssociatedCard(card, reader);
                        }
                        reader.Close();
                    }
                }
            }
            return card;
        }

        public IAssociatedCard GetCard(string cardId)
        {
            throw new NotImplementedException();
        }

        public IAssociatedCard GetCard(string userId, string cardId, bool includeAssociatedCards = false)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IAssociatedCard> GetAssociatedCards(string userId)
        {
            throw new NotImplementedException();
        }

        public void AddCardAssociation(string userId, string cardId, IStarbucksCardHeader cardHeader, bool registered)
        {
            throw new NotImplementedException();
        }

        public bool IsCardNumberAlreadyRegistered(string cardNumber)
        {
            throw new NotImplementedException();
        }

        public IAutoReloadProfile GetAutoReload(string userId, string autoReloadId)
        {
            throw new NotImplementedException();
        }

        public string UpdateAutoReload(IAutoReloadProfile autoReloadProfile)
        {
            throw new NotImplementedException();
        }

        public string SetupAutoReload(IAutoReloadProfile autoReloadProfile)
        {
            throw new NotImplementedException();
        }

        public void UpsertAutoReloadBilling(string userId, string autoReloadId, string paymentMethodId)
        {
            throw new NotImplementedException();
        }

        public ICardBalance GetBalanceByNumber(string cardNumber)
        {
            throw new NotImplementedException();
        }

        public ICardBalance GetBalanceByCardId(string cardId)
        {
            throw new NotImplementedException();
        }

        public int UpsertCardBalanceByDecryptedNumber(string cardNumber, decimal balance, DateTime balanceDate, string currency)
        {
            throw new NotImplementedException();
        }

        public int UpsertCardBalance(string cardId, decimal balance, DateTime balanceDate, string currency)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IStarbucksCardImage> GetCardImages(string cardNumber)
        {
            throw new NotImplementedException();
        }

        public IProgramPromoCode GetPromoCodeByCardNumber(string cardNumber)
        {
            throw new NotImplementedException();
        }

        public bool UpdateUserRegistrationSource(string userId, string registrationSource)
        {
            throw new NotImplementedException();
        }

        public bool IsPromoCodeActive(string promoCode, string subMarket)
        {
            throw new NotImplementedException();
        }

        public IPromotionProperties GetPromoSettings(string promoCode)
        {
            throw new NotImplementedException();
        }

        public string GetPromoCode(string subMarket)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProgramPromoCode> GetPromoCodes()
        {
            throw new NotImplementedException();
        }

        public void UpdateAutoReloadStatus(string userId, string autoReloadId, int status)
        {
            throw new NotImplementedException();
        }

        public void DisableAutoReload(string userId, string autoReloadId, DateTime disableUntilDate)
        {
            throw new NotImplementedException();
        }

        public bool IsAutoReloadAssociatedToPaymentMethod(string userId, string autoReloadId)
        {
            throw new NotImplementedException();
        }

        public void LogAutoReloadStatus(string userId, string cardId, string autoReloadId, string autoReloadStatus, string message)
        {
            throw new NotImplementedException();
        }

        public void UpdateCardAssociation(string userId, string cardId, bool? isRegistered, bool? isDefault, string nickname)
        {
            throw new NotImplementedException();
        }

        public void UnregisterCard(string userId, string cardId)
        {
            throw new NotImplementedException();
        }

        public void DeleteCardAssociation(string userId, string cardId)
        {
            throw new NotImplementedException();
        }

        public string GetCardIdByCardNumber(string cardNumber)
        {
            string cardId = string.Empty;
            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand("SELECT dbo.GetCardId(@CardNumber)", connection))
                {
                    command.CommandType = CommandType.Text;

                    command.Parameters.AddWithValue("@CardNumber", Starbucks.Platform.Security.Encryption.EncryptCardNumber(cardNumber));
                    connection.Open();
                    object result = command.ExecuteScalar();
                    if (result is System.DBNull)
                        cardId = string.Empty;
                    else
                        cardId = Encryption.EncryptCardId(int.Parse(result.ToString()));
                    connection.Close();
                }
            }
            return cardId;
        }

        public string GetAutoReloadId(string userId, string cardId)
        {
            throw new NotImplementedException();
        }

        public bool GetEmailPreference(string userId)
        {
            throw new NotImplementedException();
        }

        public bool IsCardAssociatedWithUser(string userId, int unencryptedCardId)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand("CardBelongsToUser", connection) { CommandType = CommandType.StoredProcedure })
                {
                    command.Parameters.AddWithValue("@userId", userId);
                    command.Parameters.AddWithValue("@cardId", unencryptedCardId);

                    connection.Open();
                    object result = command.ExecuteScalar();
                    connection.Close();
                    return !(result is DBNull) && Convert.ToBoolean(result);
                }
            }
        }

        public int UpsertCardTransactionHistory(string cardNumber, ICardTransaction cardTransaction)
        {
            throw new NotImplementedException();
        }

        public int UpsertCardTransactionHistory(ICardTransaction cardTransaction)
        {
            throw new NotImplementedException();
        }

        public string UpsertCard(ICard card)
        {
            throw new NotImplementedException();
        }

        public IFraudCheckUserData GetUserDataForFraudCheck(string userId)
        {
            throw new NotImplementedException();
        }

        public IFraudCheckCardData GetCardDataForFraudCheck(string userId, string cardId)
        {
            throw new NotImplementedException();
        }

        private static DateTime? UtcCutoffTime { get; set; }

        private static void MapAssociatedCard(IAssociatedCard associatedCard, IDataRecord record)
        {
            associatedCard.AutoReloadId = record.GetStringExt("AutoReloadId");
            associatedCard.RegistrationDate = record.GetDateTimeExt("RegistrationDate");
            associatedCard.RegistrationSource = record.GetStringExt("RegistrationSource");
            //associatedCard.IsDigitalCard = IsDigitalCard(card.BatchCode);
            associatedCard.RegisteredUserId = record.GetStringExt("RegisteredUserId");
            var nickName = record.GetStringExt("Nickname");
            associatedCard.Nickname = string.IsNullOrEmpty(nickName) ? string.Format("My Card ({0})", Encryption.DecryptCardNumber(associatedCard.Number).Substring(12, 4)) : nickName;
            associatedCard.IsDefault = record.GetBoolExt("Default") ?? false;
        }

        private static void MapCardRow(ICard card, IDataRecord record)
        {
            card.CardId = Encryption.EncryptCardId(record.GetInt32(record.GetOrdinal("CardId")));
            card.Name = record.GetStringExt("Name");
            card.Currency = record.GetStringExt("Currency");
            card.Class = record["Class"].ToString();
            //Check card class for eCard.
            //card.FormFactor = (eCardClassCodes != null && eCardClassCodes[card.Class] != null) ? (int)CardFormFactor.eCard : (int)CardFormFactor.Physical;
            card.Type = (CardType)Enum.Parse(typeof(CardType), record.GetInt32Ext("CardTypeId").GetValueOrDefault().ToString(CultureInfo.InvariantCulture));
            card.Active = record.GetBoolExt("Active") ?? false;

            card.Balance = record.GetDecimalExt("Balance");
            card.BalanceDate = record.GetDateTimeExt("BalanceDate");
            if (card.BalanceDate.HasValue && (UtcCutoffTime.HasValue && card.BalanceDate >= UtcCutoffTime.Value))
            {
                card.BalanceDate = new DateTime(card.BalanceDate.Value.Ticks, DateTimeKind.Utc).ToLocalTime();
            }

            card.BalanceCurrency = record.GetStringExt("BalanceCurrency");
            card.Number = record.GetStringExt("Number");
            card.Pin = record.GetStringExt("PIN");
            card.PinValidated = record.GetBoolExt("PINValidated") ?? false;
            card.ExpirationDate = record.GetDateTimeExt("ExpirationDate");

            card.BatchCode = record.GetStringExt("PromoCode");
            card.CardRangeId = record.GetInt32Ext("CardRangeId");
            card.SubMarketCode = record.GetStringExt("SubMarketCode");
            //card.CardImages = StarbucksCardImages(card.Name);
            //card.CardImages = StarbucksCardImages(card.Name);
            //card.IsPartner = IsPartnerCard(card.Class);
            //card.IsDigitalCard = IsDigitalCard(card.BatchCode);
            //TODO: Move to provider
            //if (reader.Table.Columns.Contains("CardRangeId"))
            //{
            //    card.CardrangeId = reader["CardRangeId"].FromDBNullable<Int32>(Convert.ToInt32);
            //    card.IsECard = IsECard(card.CardrangeId);
            //}

            //string imageName = card.Name;

            //If it's a duetto card, then use "duetto" as the name.
            //if (card.Type.Equals(CardType.Duetto)) imageName = "duetto";

            //Set the different image file names.
            //card.ImageSmall = BuildCardImageName(imageName, CardImageSize.Small);
            //card.ImageMedium = BuildCardImageName(imageName, CardImageSize.Medium);
            //card.ImageLarge = BuildCardImageName(imageName, CardImageSize.Large);
            //card.ImageIcon = BuildCardImageName(imageName, CardImageSize.Icon);

            // TODO: Remove Mobile
            //card.ImageMobile = BuildCardImageName(imageName, CardImageSize.Mobile);


            //TODO: Move to provider
            // Fix the currency if it's blank.
            //if (string.IsNullOrEmpty(card.Currency))
            //{
            //    try
            //    {
            //        CardRepository.ValidateAndSaveCard(Encryption.ORCADecrypt(card.Number), Encryption.ORCADecrypt(card.Pin), card.SubMarket.SubMarketCode, ref card);
            //    }
            //    catch (ValueLinkException vex)
            //    {
            //        // Couldn't contact ValueLink to fix the currency. Kind of sucks,
            //        // but it's not a show-stopper.
            //        Logger.Write(vex.ToString());
            //    }
            //}

            //card.Visibility = visibility;
            //Handle visibility
            //switch (visibility)
            //{
            //    case VisibilityLevel.Decrypted:
            //        //decrypted  cardnumber and pin
            //        card.Number = Encryption.ORCADecrypt(card.Number);
            //        card.Pin = Encryption.ORCADecrypt(card.Pin);
            //        break;
            //    case VisibilityLevel.Masked:
            //        //mask number and hide pin
            //        card.Number = CardUtil.MaskCardNumber(Encryption.ORCADecrypt(card.Number));
            //        card.Pin = string.Empty;
            //        break;
            //    default:
            //        //Encrypted
            //        break;
            //}
        }


        public bool UpdateUserRegistrationSource(string userId, ICardRegistrationSource cardRegSource= null)
        {
            throw new NotImplementedException();
        }
        public bool IsMarketingRegSourceCodeValid(string marketingRegSourceCode)
        {
            throw new NotImplementedException();
        }
        public bool IsPlatformRegSourceCodeValid(string platformRegSourceCode)
        {
            throw new NotImplementedException();
        }
         
        public void UpdateCardNickName(string userId, string cardId, string nickname)
        {
            throw new NotImplementedException();
        }


        public bool LoadCardAssociation(IAssociatedCard card, string userId = null)
        {
            throw new NotImplementedException();
        }

        public string GetCardNumberById(string decryptedCardId)
        {
            //[dbo].[GetCardNumber] 
            string cardNumber;
            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand("SELECT dbo.GetCardNumber(@CardId)", connection))
                {
                    command.CommandType = CommandType.Text;

                    command.Parameters.AddWithValue("@CardId", decryptedCardId);
                    connection.Open();
                    object result = command.ExecuteScalar();
                    if (result is System.DBNull)
                        cardNumber = string.Empty;
                    else
                        cardNumber = result.ToString() ;
                    connection.Close();
                }
            }
            return cardNumber;
        }

        public IUserAccount GetUserAccount(string userId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// ClearPaymentMethodSurrogateAndPaypalAccountNumbers (This function should expose from PaymentMethod.Dal)
        /// </summary>
        /// <param name="paymentMethod">The payment method to update</param>
        /// <returns>Returns the payment method ID</returns>
        /// 

        /*
         PDATE [PaymentMethod]
        SET [Nickname]        = ISNULL(@Nickname,      Nickname)
             ,[NameOnCard]      = ISNULL(@NameOnCard,    NameOnCard)
             ,[AccountNumber]   = ISNULL(@AccountNumber, AccountNumber)
             ,[IsDefault]       = ISNULL(@IsDefault,     [IsDefault])
             ,[ExpirationYear]  = @ExpirationYear
             ,[ExpirationMonth] = @ExpirationMonth
             ,[RoutingNumber]   = @RoutingNumber
             ,[BankName]        = @BankName
          ,[PaymentTypeId]   = @PaymentTypeId
          ,[AddressId]       = ISNULL(@AddressId, AddressId)
             ,[Active]          = @Active
             ,[DateModified]    = GETDATE()
             ,[FirstSix]        = ISNULL(@FirstSix, FirstSix)
             ,[LastFour]        = ISNULL(@LastFour, LastFour)
             ,[SurrogateAccountNumber] = ISNULL(@SurrogateAccountNumber, SurrogateAccountNumber)
             ,[IsTemporary] = @IsTemporary
             ,[IsFraudChecked] = 0
           WHERE UserId = @UserId AND PaymentMethodId = @PaymentMethodId
         */

        public long ClearPaymentMethodSurrogateAndPaypalAccountNumbers(string userId, IPaymentMethod paymentMethod)
        {
            using (var connection = new SqlConnection(ConnectionString))
            using (var command = new SqlCommand("UpsertPaymentMethod", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                connection.Open();

                command.Parameters.AddWithValue("@userId", userId ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("@PaymentMethodId", Encryption.DecryptCardId(paymentMethod.PaymentMethodId));
                command.Parameters.AddWithValue("@AddressId", paymentMethod.BillingAddressId ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("@PaymentTypeId", paymentMethod.PaymentType);
                command.Parameters.AddWithValue("@ExpirationYear", paymentMethod.ExpirationYear);
                command.Parameters.AddWithValue("@ExpirationMonth", paymentMethod.ExpirationMonth);
                command.Parameters.AddWithValue("@RoutingNumber", paymentMethod.RoutingNumber ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("@BankName", paymentMethod.BankName ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("@IsTemporary", paymentMethod.IsTemporary);

                command.Parameters.AddWithValue("@SurrogateAccountNumber", "");
                command.Parameters.AddWithValue("@AccountNumber", "");

                return Convert.ToInt64(command.ExecuteScalar());
            }
        }


    }
}
