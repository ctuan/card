﻿using System;
using System.Collections.Generic;
using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Dal.Sql.EntLibLess.Models
{
    public class Card : ICard
    {
        public bool Active { get; set; }
        public decimal? Balance { get; set; }
        public DateTime? BalanceDate { get; set; }
        public string BalanceCurrency { get; set; }
        public string CardId { get; set; }
        public CardType Type { get; set; }
        public string Class { get; set; }
        public string Currency { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string Pin { get; set; }
        public bool PinValidated { get; set; }       
        public string BatchCode { get; set; }
        public string SubMarketCode { get; set; }
        public int? CardRangeId { get; set; }
        public bool IsPartner { get;  set; }
        public IEnumerable<IStarbucksCardImage> CardImages { get; set; }
        public bool IsDigitalCard{get; set;}
        public IEnumerable<string> Actions { get; set; }
    }
}
