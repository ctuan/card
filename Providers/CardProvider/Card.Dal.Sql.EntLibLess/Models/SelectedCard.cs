﻿using Starbucks.Card.Dal.Common.Models;

namespace Starbucks.Card.Dal.Sql.EntLibLess.Models
{
    public class SelectedCard : ISelectedCard
    {
        public string UserId { get; set; }

        public string ClientId { get; set; }

        public string CardId { get; set; }
    }
}