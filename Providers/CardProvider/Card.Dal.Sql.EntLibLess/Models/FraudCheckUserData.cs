﻿using Starbucks.Card.Dal.Common.Models;

namespace Starbucks.Card.Dal.Sql.EntLibLess.Models
{
    public class FraudCheckUserData : IFraudCheckUserData
    {
        public string UserId { get; set; }
        public int AgeOfAccountSinceCreation { get; set; }
        public bool IsPartner { get; set; }
        public int BirthDay { get; set; }
        public int BirthMonth { get; set; }
        public bool eMailSignUp { get; set; }
        public bool MailSignUp { get; set; }
        public bool TextMessageSignUp { get; set; }
        public bool TwitterSignUp { get; set; }
        public bool FacebookSignUp { get; set; }
    }
}
