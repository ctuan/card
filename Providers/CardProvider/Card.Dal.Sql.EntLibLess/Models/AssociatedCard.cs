﻿using System;
using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Dal.Sql.EntLibLess.Models
{
    public class AssociatedCard :  Card, IAssociatedCard
    {
        public string AutoReloadId { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public string RegistrationSource { get; set; }        
        public string RegisteredUserId { get; set; }
        public string Nickname { get; set; }
        public bool IsDefault { get; set; }
        public bool IsOwner { get; set; }
        public string PlatformRegSourceCode { get; set; }
        public string MarketingRegSourceCode { get; set; }
        public IAutoReloadProfile AutoReloadProfile { get; set; }
    }
}
