﻿using System;
using System.Data;

namespace Starbucks.Card.Dal.Sql.EntLibLess
{
    public static class DataRecordExtension
    {
        public static string GetStringExt(this IDataRecord record, string colName)
        {
            var ord = record.GetOrdinalExt(colName);
            if (ord < 0) return string.Empty;
            return record.IsDBNull(ord) ? null : record.GetString(ord);
        }

        public static int? GetInt32Ext(this IDataRecord record, string colName)
        {
            var ord = record.GetOrdinalExt(colName);
            if (ord < 0) return null;           
            if (record.IsDBNull(ord)) return null;
            return record.GetInt32(ord);
        }

        public static Int16? GetInt16Ext(this IDataRecord record, string colName)
        {
            var ord = record.GetOrdinalExt(colName);
            if (ord < 0) return null;
            if (record.IsDBNull(ord)) return null;
            return record.GetInt16( ord);
        }

        public static Byte? GetByteExt(this IDataRecord record, string colName)
        {
            var ord = record.GetOrdinalExt(colName);
            if (ord < 0) return null;
            if (record.IsDBNull(ord)) return null;
            return record.GetByte(ord);
        }

        public static decimal? GetDecimalExt(this IDataRecord record, string colName)
        {
            var ord = record.GetOrdinalExt(colName);
            if (ord < 0) return null;           
            if (record.IsDBNull(ord)) return null;
            return record.GetDecimal(ord);
        }

        public static bool? GetBoolExt(this IDataRecord record, string colName)
        {
            var ord = record.GetOrdinalExt(colName);
            if (ord < 0) return null;  
            if (record.IsDBNull(ord)) return null;
            return record.GetBoolean(ord);
        }

        public static DateTime? GetDateTimeExt(this IDataRecord record, string colName)
        {
            var ord = record.GetOrdinalExt( colName);
            if (ord < 0) return null;
            if (record.IsDBNull(ord)) return null;
            return record.GetDateTime( ord);
        }

        public static int GetOrdinalExt(this IDataRecord record, string colName)
        {
            try
            {
                return record.GetOrdinal(colName);
            }
            catch (IndexOutOfRangeException)
            {
                return -1;
            }
        }
    }
}
