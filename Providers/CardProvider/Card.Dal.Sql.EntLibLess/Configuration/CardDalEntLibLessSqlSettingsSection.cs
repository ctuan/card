﻿using System.Configuration;

namespace Starbucks.Card.Dal.Sql.EntLibLess.Configuration
{
    public class CardDalEntLibLessSqlSettingsSection : ConfigurationSection
    {      
        [ConfigurationProperty("connectionStringName")]
        public string ConnectionStringName
        {
            get { return (string)this["connectionStringName"]; }
            set { this["connectionStringName"] = value; }
        }

    }
}
