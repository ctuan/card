﻿using System.Configuration;

namespace Starbucks.Card.Dal.Sql
{
    #region PromotionConfigSection

    /// <summary>
    /// Get's Promotion Settings Collection
    /// </summary>
    public class PromoSectionConfig : ConfigurationSection
    {
        /// <summary>
        /// Get's the Promoton Collection of Settings
        /// </summary>
        [ConfigurationProperty("Promotions")]
        public PromotionCollection PromotionCollection
        {
            get { return this["Promotions"] as PromotionCollection; }
        }

    }

    /// <summary>
    /// Get's the Promotion Settings
    /// </summary>
    public class PromotionConfigProperties : ConfigurationElement
    {
        /// <summary>
        /// Get's the PromoCode
        /// </summary>
        [ConfigurationProperty("promoCode", DefaultValue = "", IsKey = true)]
        public string PromoCode
        {
            get { return this["promoCode"] as string; }
            set { this["promoCode"] = value; }
        }

        /// <summary>
        /// Get's the promotion Country Codes
        /// </summary>
        [ConfigurationProperty("countryCodes", DefaultValue = "", IsKey = true)]
        public string CountryCodes
        {
            get { return this["countryCodes"] as string; }
            set { this["countryCodes"] = value; }
        }

        /// <summary>
        /// Get's the Pin Required
        /// </summary>
        [ConfigurationProperty("pinRequired", DefaultValue = "", IsKey = true)]
        public string PinRequired
        {
            get { return this["pinRequired"] as string; }
            set { this["pinRequired"] = value; }
        }

        /// <summary>
        /// Get's the Promotion CardRange
        /// </summary>
        [ConfigurationProperty("cardRange", DefaultValue = "", IsKey = true)]
        public string CardRange
        {
            get { return this["cardRange"] as string; }
            set { this["cardRange"] = value; }
        }

        /// <summary>
        /// Get's Promotion Card EndDate
        /// </summary>
        [ConfigurationProperty("endDate", DefaultValue = "", IsKey = true)]
        public string EndDate
        {
            get { return this["endDate"] as string; }
            set { this["endDate"] = value; }
        }

        /// <summary>
        /// Get's the Promotion Features
        /// </summary>
        [ConfigurationProperty("Features")]
        public FeaturesCollection Features
        {
            get { return this["Features"] as FeaturesCollection; }
        }
    }

    /// <summary>
    /// Get's the Promotion Collection
    /// </summary>
    [ConfigurationCollection(typeof(PromotionConfigProperties), AddItemName = "Promotion")]
    public class PromotionCollection : ConfigurationElementCollection
    {
        /// <summary>
        /// Returns the promotion properties
        /// </summary>
        /// <returns></returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new PromotionConfigProperties();
        }

        /// <summary>
        /// Set's the promotion properties
        /// </summary>
        /// <param name="promoCode">promoCode</param>
        /// <param name="countryCodes">countryCodes</param>
        /// <param name="pinRequired">PinRequired</param>
        /// <param name="cardRange">cardRange</param>
        /// <param name="endDate">endDate</param>
        public void Add(string promoCode, string countryCodes, string pinRequired, string cardRange, string endDate)
        {
            var newElement = CreateNewElement() as PromotionConfigProperties;
            newElement.PromoCode = promoCode;
            newElement.CountryCodes = countryCodes;
            newElement.PinRequired = pinRequired;
            newElement.CardRange = cardRange;
            newElement.EndDate = endDate;
            base.BaseAdd(newElement);
        }

        /// <summary>
        /// Get's the PromoCode
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            var elem = element as PromotionConfigProperties;
            return elem.PromoCode;
        }

    }

    /// <summary>
    /// Get's the Promotion Feature
    /// </summary>
    public class Feature : ConfigurationElement
    {
        /// <summary>
        /// Get's the Feature Name
        /// </summary>
        [ConfigurationProperty("name", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string Name
        {
            get { return this["name"] as string; }
            set { this["name"] = value; }
        }

        /// <summary>
        /// Get's the Feature Description
        /// </summary>
        [ConfigurationProperty("description", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string Description
        {
            get { return this["description"] as string; }
            set { this["description"] = value; }
        }

        /// <summary>
        /// Get's the Feature Description
        /// </summary>
        [ConfigurationProperty("value", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string Value
        {
            get { return this["value"] as string; }
            set { this["value"] = value; }
        }
    }

    /// <summary>
    /// Get's the Feature Collection
    /// </summary>
    [ConfigurationCollection(typeof(Feature), AddItemName = "feature")]
    public class FeaturesCollection : ConfigurationElementCollection
    {
        /// <summary>
        /// Returns the promotion features
        /// </summary>
        /// <returns></returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new Feature();
        }

        /// <summary>
        /// Set's the promotion features
        /// </summary>
        /// <param name="name">name</param>
        /// <param name="value">value</param>
        /// <param name="description"></param>
        public void Add(string name, string value, string description)
        {
            var newElement = CreateNewElement() as Feature;
            newElement.Name = name;
            newElement.Value = value;
            newElement.Description = description;
            base.BaseAdd(newElement);
        }

        /// <summary>
        /// Returns the Feature Name
        /// </summary>
        /// <param name="element">element</param>
        /// <returns></returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            var elem = element as Feature;
            if (elem != null)
                return elem.Name;

            return null;
        }
    }

    #endregion
}
