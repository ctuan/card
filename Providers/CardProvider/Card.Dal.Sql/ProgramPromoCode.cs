﻿using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Dal.Sql
{
    public class ProgramPromoCode:IProgramPromoCode
    {
        public string ProgramId { get; set; }
        public string Name { get; set; }
        public string PromoCode { get; set; }
        public string MarketingRegistrationSource { get; set; }
    }
}
