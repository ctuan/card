﻿using System.Runtime.Serialization;
using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Dal.Sql
{
   
    /// <summary>
    /// List of promofeatures.
    /// </summary>
    [DataContract]
    public class PromoFeature : IPromoFeature 
    {
        /// <summary>
        /// Name of the feature
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Feature value
        /// </summary>
        [DataMember]
        public string Value { get; set; }

        /// <summary>
        /// Feature Description
        /// </summary>
        [DataMember]
        public string Description { get; set; }
    }
}
