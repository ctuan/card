﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Dal.Sql
{
   
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class PromotionProperties : IPromotionProperties 
    {
        /// <summary>
        /// The UserId
        /// </summary>
        [DataMember]
        public string UserId { get; set; }

        /// <summary>
        /// The CardId
        /// </summary>
        [DataMember]
        public string CardId { get; set; }

        /// <summary>
        /// The Submarket
        /// </summary>
        [DataMember]
        public string Submarket { get; set; }

        /// <summary>
        /// Gets the PromoUrl
        /// </summary>
        /// <remarks></remarks>
        [DataMember]
        public string PromoUrl { get; set; }

        /// <summary>
        /// Gets the PromoCode
        /// </summary>
        /// <remarks></remarks>
        [DataMember]
        public string PromoCode { get; set; }

        /// <summary>
        /// Gets the PINRequired
        /// </summary>
        /// <remarks></remarks>
        [DataMember]
        public bool PinRequired { get; set; }

        /// <summary>
        /// Gets the Card Range.
        /// </summary>
        /// <remarks></remarks>
        [DataMember]
        public string CardRange { get; set; }

        /// <summary>
        /// Gets the EndDate
        /// </summary>
        /// <remarks></remarks>
        [DataMember]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Get's the promotion country codes.
        /// </summary>
        [DataMember]
        public string CountryCodes { get; set; }

        /// <summary>
        /// List of PromoFeatures
        /// </summary>
        [DataMember]
        public List<IPromoFeature> PromoFeatures { get; set; }

    }
}
