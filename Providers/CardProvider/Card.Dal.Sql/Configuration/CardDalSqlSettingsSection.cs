﻿using System.Configuration;

namespace Starbucks.Card.Dal.Sql.Configuration
{
    public class CardDalSqlSettingsSection : ConfigurationSection
    {        
        [ConfigurationProperty("connectionStringName")]
        public string ConnectionStringName
        {
            get { return (string)this["connectionStringName"]; }
            set { this["connectionStringName"] = value; }
        }
    }
}
