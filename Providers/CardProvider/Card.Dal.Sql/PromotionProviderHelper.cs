﻿using System;
using System.Configuration;

namespace Starbucks.Card.Dal.Sql
{
    /// <summary>
    /// Promo Config Settings 
    /// </summary>
    public enum PromotionDataSourceType
    {
        Config,
        Database,
        SiteCore
    }

    /// <summary>
    /// Promo Config Provider.
    /// </summary>
    public static class PromotionProviderHelper
    {
        // todo: This class should not exist. Its existence indicates that dependencies need to be refactored.

        private static PromoSectionConfig _promoConfigSettings = default(PromoSectionConfig);

        public static PromoSectionConfig GetPromotionSettings(PromotionDataSourceType promotionDataSourceType)
        {
            try
            {
                switch (promotionDataSourceType)
                {
                    case PromotionDataSourceType.Config:

                        if (ConfigurationManager.GetSection("PromoConfigSection") == null)
                        {
                            //const string errorMessage = "PromoConfigSection is not configurated correctly.";
                            // Logger.Write(errorMessage);
                        }
                        else
                        {
                            _promoConfigSettings = (PromoSectionConfig)ConfigurationManager.GetSection("PromoConfigSection");
                        }
                        break;
                    case PromotionDataSourceType.Database:
                        // TODO: When the database provides above same settings than above code(Config Settings) will be removed.
                        _promoConfigSettings = null;
                        break;
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "Error in PromoConfigSection, the details are " + ex.Message;
                // Logger.Write(errorMessage);
            }
            return _promoConfigSettings;
        }
    }
}
