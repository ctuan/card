﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Starbucks.Card.Dal.Common.Configuration;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.Platform.Security;

namespace Starbucks.Card.Dal.Sql
{
    public class SelectedCardDal : ISelectedCardDal
    {
        private Database _database;

        public SelectedCardDal(CardDalSqlSettingsSection cardDalSqlSettingsSection)
        {
            _database = DatabaseFactory.CreateDatabase(cardDalSqlSettingsSection.ConnectionStringName);
        }

        /// <summary>
        ///     The Starbucks database instance against which to call operations.
        /// </summary>
        protected virtual Database StarbucksDatabase
        {
            get { return _database; }
            set { _database = value; }
        }

        public IEnumerable<string> GetSelectedCards(string userId, string clientId)
        {
            if (userId == null)
            {
                throw new ArgumentNullException("userId");
            }
            if (clientId == null)
            {
                throw new ArgumentNullException("clientId");
            }

            var cards = new List<string>();
            using (DbCommand command = _database.GetStoredProcCommand("SelectedCards_Get"))
            {
                _database.AddInParameter(command, "@userID", DbType.String, userId);
                _database.AddInParameter(command, "@clientID", DbType.String, clientId);

                using (IDataReader reader = _database.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        var card = Encryption.EncryptCardId(reader.GetInt32(reader.GetOrdinal("CardId")));
                        cards.Add(card);
                    }
                }
            }
            return cards;
        }

        public void AssociateSelectedCardsWithClient(string userId, string clientId, string[] cardIds)
        {
            ExecuteNonQuery(userId, clientId, GetDataTable(cardIds), "SelectedCards_Upsert");
        }

        public void UpdateSelectedCards(string userId, string clientId, IDictionary<string, string> cardIds)
        {
            ExecuteNonQuery(userId, clientId, GetDataTable(cardIds), "SelectedCards_Update");
        }

        public void RemoveSelectedCards(string userId, string clientId, string[] cardIds)
        {
            ExecuteNonQuery(userId, clientId, GetDataTable(cardIds), "SelectedCards_Disconnect");
        }

        private void ExecuteNonQuery(string userId, string clientId, DataTable cards, string commandName)
        {
            if (userId == null)
            {
                throw new ArgumentNullException("userId");
            }
            if (clientId == null)
            {
                throw new ArgumentNullException("clientId");
            }
            if (cards == null || cards.Rows.Count == 0)
            {
                throw new ArgumentNullException("cards");
            }

            using (DbCommand command = _database.GetStoredProcCommand(commandName))
            {
                _database.AddInParameter(command, "@userID", DbType.String, userId);
                _database.AddInParameter(command, "@clientID", DbType.String, clientId);

                var sqlCommand = command as SqlCommand;

                if (sqlCommand != null)
                {
                    SqlParameter cardsParam = sqlCommand.Parameters.AddWithValue("@selectedCards", cards);
                    cardsParam.SqlDbType = SqlDbType.Structured;
                }

                _database.ExecuteNonQuery(command);
            }
        }

        private static DataTable GetDataTable(out DataColumn[] dataColumns)
        {
            var cards = new DataTable();
            dataColumns = new[]
                {
                    new DataColumn {ColumnName = "CardId", DataType = typeof (int)},
                    new DataColumn {ColumnName = "NewCardId", DataType = typeof (string)}
                };
            cards.Columns.AddRange(dataColumns);
            return cards;
        }

        private static DataTable GetDataTable(IEnumerable<string> cardIds)
        {
            DataColumn[] dataColumns;
            DataTable cards = GetDataTable(out dataColumns);

            foreach (var cardId in cardIds.Distinct())
            {
                DataRow row = cards.NewRow();
                row[dataColumns[0]] = Encryption.DecryptCardId(cardId);
                row[dataColumns[1]] = Encryption.DecryptCardId(cardId);
                cards.Rows.Add(row);
            }
            return cards;
        }

        private static DataTable GetDataTable(IEnumerable<KeyValuePair<string, string>> cardNumbers)
        {
            DataColumn[] dataColumns;
            DataTable cards = GetDataTable(out dataColumns);

            foreach (var cardNumber in cardNumbers.Distinct())
            {
                DataRow row = cards.NewRow();
                row[dataColumns[0]] = Encryption.DecryptCardId(cardNumber.Key);
                row[dataColumns[1]] = Encryption.DecryptCardId(cardNumber.Value);
                cards.Rows.Add(row);
            }
            return cards;
        }
    }
}