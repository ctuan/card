﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.Card.Dal.Common.Models;
using Starbucks.Card.Dal.Sql.Configuration;
using Starbucks.Card.Dal.Sql.Models;
using Starbucks.Card.Provider.Common.Exceptions;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.Platform.Security;
using Starbucks.Settings.Provider.Common;
using IFraudCheckCardData = Starbucks.Card.Dal.Common.Models.IFraudCheckCardData;
using IFraudCheckUserData = Starbucks.Card.Dal.Common.Models.IFraudCheckUserData;
using Starbucks.PaymentMethod.Provider.Common.Models;


namespace Starbucks.Card.Dal.Sql
{
    public class CardDal : ICardDal
    {
        private readonly string _connectionString;
        private readonly ISettingsProvider _settingsProvider;

        public CardDal(CardDalSqlSettingsSection cardDalSqlSettingsSection, ISettingsProvider settingsProvider)
        {
            _settingsProvider = settingsProvider;
            _connectionString = ConfigurationManager.ConnectionStrings[cardDalSqlSettingsSection.ConnectionStringName].ConnectionString;
        }

        static CardDal()
        {
            DateTime utcCutoffTime;
            UtcCutoffTime = DateTime.TryParse(ConfigurationManager.AppSettings["UTCCutoffTime"], out utcCutoffTime)
                               ? utcCutoffTime
                               : (DateTime?)null;
        }

        #region GetCard

        public IAssociatedCard GetCardByNumber(string cardNumber)
        {
            string encryptedCardNumber = Encryption.EncryptCardNumber(cardNumber);
            AssociatedCard card = null;
            using (var connection = new SqlConnection(_connectionString))
            using (var command = GetCommand("GetCardByNumber", connection))
            {
                connection.Open();
                command.Parameters.AddWithValue("@Number", encryptedCardNumber);
                using (IDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        card = new AssociatedCard();
                        MapCardRow(card, reader);
                        MapAssociatedCard(card, reader);
                    }
                }
            }
            return card;
        }

        private SqlCommand GetCommand(string sproc, SqlConnection connection)
        {
            return new SqlCommand(sproc, connection) { CommandType = CommandType.StoredProcedure };
        }

        public IAssociatedCard GetCard(string userId, string cardId, bool includeAssociatedCards = false)
        {
            var card = GetCard(cardId);
            if (card == null) return null;

            if (!includeAssociatedCards)
            {
                // Return card only if it is registered to this user.
                if (!userId.Equals(card.RegisteredUserId, StringComparison.InvariantCultureIgnoreCase))
                    return null;

                LoadCardAssociation(card);
                card.IsOwner = true;
            }
            else
            {
                // TODO: test: Does this work for cards not created by the userId user?
                var foundAssociatedCard = LoadCardAssociation(card, userId);
                if (!foundAssociatedCard)
                {
                    // This card is not associated with this user.
                    return null;
                }

                if (userId.Equals(card.RegisteredUserId, StringComparison.InvariantCultureIgnoreCase))
                    card.IsOwner = true;
                else
                    card.IsOwner = false;
            }
            return card;
        }


        public IAssociatedCard GetCard(string cardId)
        {
            int decryptedCardId = Encryption.DecryptCardId(cardId);
            AssociatedCard card = null;
            using (var connection = new SqlConnection(_connectionString))
            using (var command = GetCommand("GetCard", connection))
            {
                connection.Open();
                command.Parameters.AddWithValue("@CardId", decryptedCardId);

                using (IDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        card = new AssociatedCard();
                        MapCardRow(card, reader);
                        MapAssociatedCard(card, reader);
                    }
                }
            }
            return card;
        }

        public IEnumerable<IAssociatedCard> GetAssociatedCards(string userId)
        {
            var cards = new List<AssociatedCard>();
            using (var connection = new SqlConnection(_connectionString))
            using (var command = GetCommand("GetAssociatedCards", connection))
            {
                connection.Open();
                command.Parameters.AddWithValue("@UserID", userId);
                using (IDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var card = new AssociatedCard();
                        MapCardRow(card, reader);
                        MapAssociatedCard(card, reader);
                        MapAssociatedCardNickNameDefault(card, reader);
                        card.IsOwner = (userId.Equals(card.RegisteredUserId, StringComparison.InvariantCultureIgnoreCase));
                        //card.CardImages = StarbucksCardImages(card.Name);
                        card.IsPartner = IsPartnerCard(card.Class);
                        cards.Add(card);
                    }
                }
            }
            return cards;
        }

        private static bool IsPartnerCard(string cardClass)
        {
            return (cardClass.Equals("69") || cardClass.Equals("29") || cardClass.Equals("32"));
        }

        private static readonly NameValueCollection DigitalCardPromoCodes = (NameValueCollection)ConfigurationManager.GetSection("digitalCardPromoCodes");
        public static bool IsDigitalCard(string promoCode)
        {
            // They keys should always be a number and not have casing issues, see if the key exists in the collection 
            // If so this should be a digital card

            if (DigitalCardPromoCodes == null)
            {
                throw new Exception("Digital Promo code configuration section is missing in the configuration.");
            }

            bool returnValue = false;
            if (promoCode != null)
            {
                returnValue = DigitalCardPromoCodes.Cast<string>().Contains(promoCode);
            }
            return returnValue;
        }



        #endregion

        #region SaveCard

        public string UpsertCard(ICard card)
        {
            //We'll call a stored procedure to get the card data.
            const string sqlCommand = "UpsertCard";
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand(sqlCommand, connection))
            {
                connection.Open();
                dbCommand.Parameters.AddWithValue("@CardID",
                                             card.CardId != null ? Encryption.DecryptCardId(card.CardId) : -1);
                dbCommand.Parameters.AddWithValue("@Number",
                                             Encryption.EncryptCardNumber(card.Number));
                //Encrypt the card number.
                dbCommand.Parameters.AddWithValue("@DecryptedNumber",
                                             Encryption.DecryptCardNumber(card.Number));
                //Make sure the card number is decrypted.
                dbCommand.Parameters.AddWithValue("@PIN",
                                             card.Pin != null ? Encryption.EncryptPin(card.Pin) : null);
                //Encrypt the PIN.
                dbCommand.Parameters.AddWithValue("@Name", card.Name);
                dbCommand.Parameters.AddWithValue("@Currency", card.Currency);
                dbCommand.Parameters.AddWithValue("@SubMarketCode",
                                             string.IsNullOrEmpty(card.SubMarketCode) ? null : card.SubMarketCode);
                dbCommand.Parameters.AddWithValue("@CardTypeID", (int)card.Type);
                dbCommand.Parameters.AddWithValue("@Class", card.Class);
                dbCommand.Parameters.AddWithValue("@Active", card.Active);
                dbCommand.Parameters.AddWithValue("@PINValidated", card.PinValidated);

                if (card.ExpirationDate.HasValue)
                {
                    dbCommand.Parameters.AddWithValue("@ExpirationDate", card.ExpirationDate);
                }

                int cardId = Convert.ToInt32(dbCommand.ExecuteScalar());
                return Encryption.EncryptCardId(cardId);
            }
        }

        public void AddCardAssociation(string userId, string cardId, IStarbucksCardHeader cardHeader, bool registered)
        {
            // if empty set it to null to comply with db constraint.
            cardHeader.RegistrationSource.Platform = (cardHeader.RegistrationSource.Platform == string.Empty)
                ? null
                : cardHeader.RegistrationSource.Platform;
            cardHeader.RegistrationSource.Marketing = (cardHeader.RegistrationSource.Marketing == string.Empty)
                ? null
                : cardHeader.RegistrationSource.Marketing;
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand("AddCardAssociation", connection))
            {
                connection.Open();

                dbCommand.Parameters.AddWithValue("@UserId", userId);
                dbCommand.Parameters.AddWithValue("@CardId", Encryption.DecryptCardId(cardId));
                dbCommand.Parameters.AddWithValue("@IsRegistered", registered);
                dbCommand.Parameters.AddWithValue("@IsDefault", cardHeader.Primary);
                dbCommand.Parameters.AddWithValue("@Nickname", cardHeader.Nickname);
                dbCommand.Parameters.AddWithValue("@PlatformRegSource", cardHeader.RegistrationSource.Platform);
                dbCommand.Parameters.AddWithValue("@MarketingRegSource", cardHeader.RegistrationSource.Marketing);

                dbCommand.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Gets the promocode and the marketing registration source by passing card number
        /// </summary>
        /// <param name="cardNumber">Card number</param>
        /// <returns>An instance of ProgramPromoCode</returns>
        public IProgramPromoCode GetPromoCodeByCardNumber(string cardNumber)
        {
            var promo = new ProgramPromoCode();
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = new SqlCommand("SELECT PromoCode, MarketingRegSource FROM dbo.GetPromoCodeAndRegSource(@CardNumber)", connection) { CommandType = CommandType.Text })
            {
                connection.Open();
                dbCommand.Parameters.AddWithValue("@CardNumber", cardNumber);

                using (IDataReader reader = dbCommand.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        promo.PromoCode = reader.GetStringExt("PromoCode");
                        promo.MarketingRegistrationSource = reader.GetStringExt("MarketingRegSource");
                    }
                }
                return promo;
            }
        }

        public bool IsCardNumberAlreadyRegistered(string cardNumber)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = new SqlCommand("SELECT dbo.IsCardNumberAlreadyRegistered(@CardNumber)", connection) { CommandType = CommandType.Text })
            {
                connection.Open();
                dbCommand.Parameters.AddWithValue("@CardNumber", Encryption.EncryptCardNumber(cardNumber));

                var result = dbCommand.ExecuteScalar();
                return !(result is DBNull) && Convert.ToBoolean(result);
            }
        }

        #endregion

        #region CardBalance

        public ICardBalance GetBalanceByNumber(string cardNumber)
        {
            string encryptedCardNumber = Encryption.EncryptCardNumber(cardNumber);
            using (var connection = new SqlConnection(_connectionString))
            using (var command = GetCommand("GetCardBalanceByNumber", connection))
            {
                connection.Open();
                command.Parameters.AddWithValue("@Number", encryptedCardNumber);
                CardBalance cardBalance = null;
                using (IDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        cardBalance = new CardBalance();
                        MapCardBalance(cardBalance, reader);
                    }
                }
                return cardBalance;
            }
        }

        public ICardBalance GetBalanceByCardId(string cardId)
        {
            int encryptedCardNumber = Encryption.DecryptCardId(cardId);
            using (var connection = new SqlConnection(_connectionString))
            using (var command = GetCommand("GetCardBalance", connection))
            {
                connection.Open();
                command.Parameters.AddWithValue("@CardId", encryptedCardNumber);
                CardBalance cardBalance = null;
                using (IDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        cardBalance = new CardBalance();
                        MapCardBalance(cardBalance, reader);
                    }
                }
                return cardBalance;
            }
        }

        public int UpsertCardBalanceByDecryptedNumber(string cardNumber, decimal balance, DateTime balanceDate, string currency)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand("UpsertCardBalanceByNumber", connection))
            {
                connection.Open();
                dbCommand.Parameters.AddWithValue("@CardNumber", Encryption.EncryptCardNumber(cardNumber));
                dbCommand.Parameters.AddWithValue("@Balance", balance);
                dbCommand.Parameters.AddWithValue("@BalanceDate", balanceDate);
                dbCommand.Parameters.AddWithValue("@Currency", currency);

                return Convert.ToInt32(dbCommand.ExecuteScalar());
            }
        }

        public int UpsertCardBalance(string cardId, decimal balance, DateTime balanceDate, string currency)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand("UpsertCardBalance", connection))
            {
                connection.Open();
                dbCommand.Parameters.AddWithValue("@CardId", Encryption.DecryptCardId(cardId));
                dbCommand.Parameters.AddWithValue("@Balance", balance);
                dbCommand.Parameters.AddWithValue("@BalanceDate", balanceDate);
                dbCommand.Parameters.AddWithValue("@Currency", currency);
                ////Enlist in a transaction using the TransactionScopeFactory.
                //using (var scope = TransactionScopeFactory.Create())
                //{
                var result = Convert.ToInt32(dbCommand.ExecuteScalar());
                //Complete the transaction.
                // scope.Complete();
                //}
                return result;
            }
        }

        #endregion

        #region AutoReload

        public string GetAutoReloadId(string userId, string cardId)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = new SqlCommand("SELECT dbo.GetAutoreloadId(@UserId, @CardId)", connection) { CommandType = CommandType.Text })
            {
                connection.Open();
                dbCommand.Parameters.AddWithValue("@userID", userId);
                dbCommand.Parameters.AddWithValue("@CardId", Encryption.DecryptCardId(cardId));

                object result = dbCommand.ExecuteScalar();
                return result is System.DBNull ? null : result.ToString();
            }
        }

        public Common.Models.IAutoReloadProfile GetAutoReload(string userId, string autoReloadId)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var command = GetCommand("GetAutoreloadProfile", connection))
            {
                connection.Open();
                command.Parameters.AddWithValue("@userID", userId);
                command.Parameters.AddWithValue("@arID", autoReloadId);
                Common.Models.IAutoReloadProfile autoReloadProfile = null;
                using (IDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        autoReloadProfile = new AutoReloadProfile();
                        MapAutoReloadProfile(autoReloadProfile, reader);
                    }
                }
                return autoReloadProfile;
            }
        }

        public string UpdateAutoReload(Common.Models.IAutoReloadProfile autoReloadProfile)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var command = GetCommand("UpsertAutoreloadProfile", connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@userId", autoReloadProfile.UserId);
                command.Parameters.AddWithValue("@cardID", Encryption.DecryptCardId(autoReloadProfile.CardId));
                //@cardID INT, 
                command.Parameters.AddWithValue("@arID", autoReloadProfile.AutoReloadId); //@arID NVARCHAR (255)=null, 
                command.Parameters.AddWithValue("@reloadType", autoReloadProfile.AutoReloadType);
                //@reloadType NVARCHAR (10), 
                command.Parameters.AddWithValue("@reloadAmount", autoReloadProfile.Amount);
                //@reloadAmount MONEY, 
                command.Parameters.AddWithValue("@status", autoReloadProfile.Status);
                //@status TINYINT, 
                command.Parameters.AddWithValue("@triggerAmount", autoReloadProfile.TriggerAmount);
                //@triggerAmount MONEY=null, 
                command.Parameters.AddWithValue("@reloadDay", autoReloadProfile.Day);
                //@reloadDay INT=null, 
                command.Parameters.AddWithValue("@dateStopped", autoReloadProfile.StoppedDate);
                //@dateStopped DATETIME=null,
                command.Parameters.AddWithValue("@DisableUntilDate", autoReloadProfile.DisableUntilDate); //@DisableUntilDate DATETIME=null,
                //_cardDatabase.AddInParameter(command, "@site",  autoReloadProfile.UserId);            //@site nvarchar(10) = null, 
                //_cardDatabase.AddInParameter(command, "@submarket",  autoReloadProfile.);            //@submarket nvarchar(10) = null, 
                //_cardDatabase.AddInParameter(command, "@language",  autoReloadProfile.UserId);            //@language nvarchar(10) = null

                command.ExecuteNonQuery();
            }
            return autoReloadProfile.AutoReloadId;
        }

        public string SetupAutoReload(Common.Models.IAutoReloadProfile autoReloadProfile)
        {
            return UpdateAutoReload(autoReloadProfile);
        }


        public void UpsertAutoReloadBilling(string userId, string autoReloadId, string paymentMethodId)
        {
            Int32 pmi;
            try
            {
                pmi = Encryption.DecryptCardId(paymentMethodId);
            }
            catch (Exception)
            {
                throw new CardProviderException(
                             Card.Provider.Common.ErrorResources.CardProviderErrorResource
                                 .AutoReloadNotAssociatedToPaymentMethodCode,
                             Card.Provider.Common.ErrorResources.CardProviderErrorResource
                                 .AutoReloadNotAssociatedToPaymentMethodMessage);
            }
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                using (var dbCommand = GetCommand("UpsertAutoreloadPaymentMethod", connection))
                {
                    connection.Open();
                    dbCommand.Parameters.AddWithValue("@userID", userId);
                    dbCommand.Parameters.AddWithValue("@arID", autoReloadId);
                    dbCommand.Parameters.AddWithValue("@PaymentMethodId", pmi);

                    dbCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlException)
            {
                if (sqlException.Errors.Count > 0)
                {
                    switch (sqlException.Errors[0].Number)
                    {
                        case 547:
                            throw new CardProviderException(
                                Card.Provider.Common.ErrorResources.CardProviderErrorResource
                                    .AutoReloadNotAssociatedToPaymentMethodCode,
                                Card.Provider.Common.ErrorResources.CardProviderErrorResource
                                    .AutoReloadNotAssociatedToPaymentMethodMessage);
                    }
                }
                throw;
            }
        }


        public void UpdateAutoReloadStatus(string userId, string autoReloadId, int status)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var command = GetCommand("UpdateAutoreloadStatus", connection))
            {
                connection.Open();
                command.Parameters.AddWithValue("@userID", userId);
                command.Parameters.AddWithValue("@arID", autoReloadId);
                command.Parameters.AddWithValue("@status", status);
                command.ExecuteNonQuery();
            }
        }


        public void DisableAutoReload(string userId, string autoReloadId, DateTime disableUntilDate)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand("DisableAutoreload", connection))
            {
                connection.Open();

                dbCommand.Parameters.AddWithValue("@userID", userId);
                dbCommand.Parameters.AddWithValue("@arID", autoReloadId);
                dbCommand.Parameters.AddWithValue("@DisableUntilDate", disableUntilDate);
                dbCommand.ExecuteNonQuery();
            }
        }


        public bool IsAutoReloadAssociatedToPaymentMethod(string userId, string autoReloadId)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = new SqlCommand("SELECT dbo.IsAutoReloadAssociatedToPaymentMethod(@AutoReloadId, @UserId)", connection) { CommandType = CommandType.Text })
            {
                connection.Open();

                dbCommand.Parameters.AddWithValue("@AutoReloadId", autoReloadId);
                dbCommand.Parameters.AddWithValue("@UserId", userId);

                object result = dbCommand.ExecuteScalar();

                return Convert.ToBoolean(result);
            }
        }

        public void LogAutoReloadStatus(string userId, string cardId, string autoReloadId, string autoReloadStatus, string message)
        {
            const string sqlCommand = "LogAutoreloadStatus";
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand(sqlCommand, connection))
            {
                connection.Open();

                dbCommand.Parameters.AddWithValue("@userId", userId);
                dbCommand.Parameters.AddWithValue("@cardId", cardId);
                dbCommand.Parameters.AddWithValue("@autoReloadId", autoReloadId);
                dbCommand.Parameters.AddWithValue("@autoReloadStatus", autoReloadStatus);
                dbCommand.Parameters.AddWithValue("@message", message);

                dbCommand.ExecuteNonQuery();
            }
        }

        public void UpdateCardAssociation(string userId, string cardId, bool? isRegistered, bool? isDefault, string nickname)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand("UpdateCardAssociation", connection))
            {
                connection.Open();

                dbCommand.Parameters.AddWithValue("@UserId", userId);
                dbCommand.Parameters.AddWithValue("@CardId", Encryption.DecryptCardId(cardId));
                dbCommand.Parameters.AddWithValue("@IsRegistered", isRegistered.HasValue ? isRegistered : (object)DBNull.Value);
                dbCommand.Parameters.AddWithValue("@IsDefault", isDefault.HasValue ? isDefault : (object)DBNull.Value);
                dbCommand.Parameters.AddWithValue("@Nickname", string.IsNullOrEmpty(nickname) ? (object)DBNull.Value : nickname);
                dbCommand.ExecuteNonQuery();
            }
        }

        public void UpdateCardNickName(string userId, string cardId, string nickname)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand("UpdateCardNickName", connection))
            {
                connection.Open();

                dbCommand.Parameters.AddWithValue("@UserId", userId);
                dbCommand.Parameters.AddWithValue("@CardId", Encryption.DecryptCardId(cardId));
                dbCommand.Parameters.AddWithValue("@Nickname", string.IsNullOrEmpty(nickname) ? (object)DBNull.Value : nickname);
                dbCommand.ExecuteNonQuery();
            }
        }

        #endregion

        private static DateTime? UtcCutoffTime { get; set; }

        public bool GetEmailPreference(string userId)
        {
            bool emailPreference = default(bool);

            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand("GetEmailPreference", connection))
            {
                connection.Open();

                dbCommand.Parameters.AddWithValue("@userID", userId);

                using (IDataReader dr = dbCommand.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        emailPreference = (dr.GetStringExt("opted_in") == "1");
                    }
                }

                return emailPreference;
            }
        }

        public bool IsCardAssociatedWithUser(string userId, int unencryptedCardId)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var command = GetCommand("CardBelongsToUser", connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@userId", userId);
                command.Parameters.AddWithValue("@cardId", unencryptedCardId);

                object result = command.ExecuteScalar();
                return !(result is DBNull) && Convert.ToBoolean(result);
            }
        }

        //Call Value link to unregister in provider
        //if success call here to unregister card if not duetto
        //Todo: LoyaltyHost?
        public void UnregisterCard(string userId, string cardId)
        {
            const string sqlCommand = "UpdateCardAssociation";
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand(sqlCommand, connection))
            {
                connection.Open();

                dbCommand.Parameters.AddWithValue("@userID", userId);
                dbCommand.Parameters.AddWithValue("@CardId", Encryption.DecryptCardId(cardId));
                dbCommand.Parameters.AddWithValue("@IsRegistered", false);
                dbCommand.ExecuteScalar();
            }
        }

        public void DeleteCardAssociation(string userId, string cardId)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand("DeleteCardAssociation", connection))
            {
                connection.Open();

                dbCommand.Parameters.AddWithValue("@UserId", userId);
                dbCommand.Parameters.AddWithValue("@CardId", Encryption.DecryptCardId(cardId));
                dbCommand.ExecuteNonQuery();
            }
        }

        public string GetCardIdByCardNumber(string cardNumber)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = new SqlCommand("SELECT dbo.GetCardId(@CardNumber)", connection) { CommandType = CommandType.Text })
            {
                connection.Open();

                dbCommand.Parameters.AddWithValue("@CardNumber", Encryption.EncryptCardNumber(cardNumber));

                object result = dbCommand.ExecuteScalar();
                return result is System.DBNull ? null : result.ToString();
            }
        }

        public int UpsertCardTransactionHistory(string cardNumber, ICardTransaction cardTransaction)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand("UpsertCardTransactionHistoryByNumber", connection))
            {
                connection.Open();

                dbCommand.Parameters.AddWithValue("@CardNumber", Encryption.EncryptCardNumber(cardNumber));
                dbCommand.Parameters.AddWithValue("@AuthCode", cardTransaction.AuthorizationCode);
                dbCommand.Parameters.AddWithValue("@TransId", cardTransaction.TransactionId);
                dbCommand.Parameters.AddWithValue("@TransAmount", cardTransaction.Amount);
                dbCommand.Parameters.AddWithValue("@TransCurrency", cardTransaction.Currency);
                dbCommand.Parameters.AddWithValue("@TransDescription", cardTransaction.Description);
                dbCommand.Parameters.AddWithValue("@TransDate", cardTransaction.TransactionDate);
                dbCommand.Parameters.AddWithValue("@TransBeginningBalance", cardTransaction.BeginningBalance);
                dbCommand.Parameters.AddWithValue("@TransEndingBalance", cardTransaction.EndingBalance);
                dbCommand.Parameters.AddWithValue("@TransRequestCode", cardTransaction.RequestCode);

                int returnValue = -1;

                object result = dbCommand.ExecuteScalar();

                if (result != null)
                {
                    returnValue = Convert.ToInt32(result);
                }

                return returnValue;
            }
        }

        public int UpsertCardTransactionHistory(ICardTransaction cardTransaction)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand("UpsertCardTransactionHistory", connection))
            {
                connection.Open();

                dbCommand.Parameters.AddWithValue("@CardId", Encryption.DecryptCardId(cardTransaction.CardId));
                dbCommand.Parameters.AddWithValue("@AuthorizationCode", cardTransaction.AuthorizationCode);
                dbCommand.Parameters.AddWithValue("@TransactionId", cardTransaction.TransactionId);
                dbCommand.Parameters.AddWithValue("@Amount", cardTransaction.Amount);
                dbCommand.Parameters.AddWithValue("@Currency", cardTransaction.Currency);
                dbCommand.Parameters.AddWithValue("@Description", cardTransaction.Description);
                dbCommand.Parameters.AddWithValue("@TransactionDate", cardTransaction.TransactionDate);
                dbCommand.Parameters.AddWithValue("@BeginningBalance", cardTransaction.BeginningBalance);
                dbCommand.Parameters.AddWithValue("@EndingBalance", cardTransaction.EndingBalance);
                dbCommand.Parameters.AddWithValue("@RequestCode", cardTransaction.RequestCode);

                //Enlist in a transaction using the TransactionScopeFactory.
                //using (var scope = TransactionScopeFactory.Create())
                //{
                int returnValue = Convert.ToInt32(dbCommand.ExecuteScalar());
                //Complete the transaction.
                //scope.Complete();
                //}
                return returnValue;
            }
        }

        public int UpsertPartialTransferTransactionDetails(int cardAmountTransactionId, string toCardTransactionId,
                                                           DateTime toCardTransactionDate
                                                           , string fromCardTransactionId,
                                                           DateTime? fromCardTransactionDate)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand("UpsertPartialTransferTransactionDetails", connection))
            {
                connection.Open();

                dbCommand.Parameters.AddWithValue("@CardAmountTransactionId", cardAmountTransactionId);
                dbCommand.Parameters.AddWithValue("@ToCardTransactionId", toCardTransactionId);
                dbCommand.Parameters.AddWithValue("@ToCardTransactionDate", toCardTransactionDate);
                dbCommand.Parameters.AddWithValue("@FromCardTransactionId", fromCardTransactionId);
                dbCommand.Parameters.AddWithValue("@FromCardTransactionDate", fromCardTransactionDate);

                int returnValue = -1;

                object result = dbCommand.ExecuteScalar();

                if (result != null)
                {
                    returnValue = (int)result;
                }

                return returnValue;
            }
        }

        /// <summary>
        /// Updates the Registration Source on the CustomProperty table 
        /// </summary>
        /// <param name="userId"> user id</param>     
        /// <param name="cardRegSource"> An instance of CardRegistrationSource for platform and marketing registration source</param>
        /// <returns> A flag indicating if any value is returned.</returns>
        public bool UpdateUserRegistrationSource(string userId, ICardRegistrationSource cardRegSource)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand("InsertRegistrationSource", connection))
            {
                connection.Open();

                dbCommand.Parameters.AddWithValue("@UserID", userId);
                dbCommand.Parameters.AddWithValue("@MarketingRegSource", cardRegSource.Marketing);
                dbCommand.Parameters.AddWithValue("@PlatformRegSource", cardRegSource.Platform);

                int status = Convert.ToInt32(dbCommand.ExecuteScalar());
                return (status > 0);
            }
        }

        /// <summary>
        /// Updates the Registration Source on the UserObject 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="registrationSource"></param>
        /// <returns></returns>
        public bool UpdateUserRegistrationSource(string userId, string registrationSource)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand("UpdateUserRegSource", connection))
            {
                connection.Open();

                dbCommand.Parameters.AddWithValue("@UserID", userId);
                dbCommand.Parameters.AddWithValue("@RegSource", registrationSource);

                int status = Convert.ToInt32(dbCommand.ExecuteScalar());
                return (status > 0);
            }
        }

        #region Promotion

        private static readonly NameValueCollection SubmarketPromoMappings = (NameValueCollection)ConfigurationManager.GetSection("submarketPromoMap");

        public string GetPromoCode(string subMarket)
        {
            return (SubmarketPromoMappings != null && SubmarketPromoMappings[subMarket] != null) ? SubmarketPromoMappings[subMarket] : string.Empty;
        }

        /// <summary>
        /// Validate the promotion is active.
        /// </summary>
        /// <param name="promoCode">promoCode</param>
        /// <param name="submarket">submarket</param>
        /// <returns></returns>
        public bool IsPromoCodeActive(string promoCode, string submarket)
        {
            bool isPromoCodeActive = default(bool);


            if (!string.IsNullOrEmpty(promoCode))
            {
                var promoConfigSettings = GetPromoSettings(promoCode);

                if (promoConfigSettings != default(PromotionProperties) &&
                    promoConfigSettings.EndDate.Date >= DateTime.Now.Date &&
                     GetPromotionCountryCodes(promoConfigSettings.CountryCodes).Contains(submarket))
                {
                    isPromoCodeActive = true;
                    //Logger.Write(string.Format("PromoCode:{0} is active for this UserId:{1} and CardId:{2}",
                    //    promoCode, promoConfigSettings.UserId, promoConfigSettings.CardId));
                }
            }
            return isPromoCodeActive;
        }

        public IPromotionProperties GetPromoSettings(string promoCode)
        {
            IPromotionProperties promoSettings = new PromotionProperties();

            var promotionSettingsList = GetPromotionSettingsList();
            if (!string.IsNullOrEmpty(promoCode) && promotionSettingsList != null
                && promotionSettingsList.PromotionCollection != null)
            {
                var promotionProperties = promotionSettingsList.PromotionCollection.Cast<PromotionConfigProperties>().FirstOrDefault(dr => dr.PromoCode == promoCode);

                if (promotionProperties != null)
                {
                    promoSettings.PromoCode = promotionProperties.PromoCode;
                    promoSettings.EndDate = Convert.ToDateTime(promotionProperties.EndDate);
                    promoSettings.CountryCodes = promotionProperties.CountryCodes;
                    promoSettings.CardRange = promotionProperties.CardRange;
                    promoSettings.PinRequired = Convert.ToBoolean(promotionProperties.PinRequired);

                    promoSettings.PromoFeatures = new List<IPromoFeature>();

                    foreach (Feature feature in promotionProperties.Features)
                    {
                        IPromoFeature promoFeature = new PromoFeature();
                        promoFeature.Name = feature.Name;
                        promoFeature.Value = feature.Value;
                        promoFeature.Description = feature.Description;

                        promoSettings.PromoFeatures.Add(promoFeature);
                    }
                }
            }
            return promoSettings;
        }

        /// <summary>
        /// Get's the promotion config settings list.
        /// </summary>
        /// <returns></returns>
        private static PromoSectionConfig GetPromotionSettingsList()
        {
            PromoSectionConfig promoConfigList;

            string promotionDataSourceType = ConfigurationManager.AppSettings["PromotionDataSourceType"];

            if (Enum.IsDefined(typeof(PromotionDataSourceType), promotionDataSourceType))
            {
                var sourceType = (PromotionDataSourceType)Enum.Parse(typeof(PromotionDataSourceType), promotionDataSourceType, true);
                promoConfigList = PromotionProviderHelper.GetPromotionSettings(sourceType);
            }
            else
            {
                // Load Default Settings as Config.
                promoConfigList = PromotionProviderHelper.GetPromotionSettings(PromotionDataSourceType.Config);
            }
            return promoConfigList;
        }

        /// <summary>
        /// Get's the country codes.
        /// </summary>
        /// <param name="countryCodes">countryCodes</param>
        /// <returns></returns>
        private static IEnumerable<string> GetPromotionCountryCodes(string countryCodes)
        {
            string[] promotionCountryCodes = null;
            if (!string.IsNullOrEmpty(countryCodes))
            {
                countryCodes = countryCodes.Replace(Environment.NewLine, "").Replace(" ", "");
                promotionCountryCodes = countryCodes.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            }
            return promotionCountryCodes;
        }

        #endregion

        public IFraudCheckUserData GetUserDataForFraudCheck(string userId)
        {
            FraudCheckUserData result = null;

            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand("GetUserDataForFraudCheck", connection))
            {
                connection.Open();
                dbCommand.Parameters.AddWithValue("@UserId", userId);

                using (IDataReader reader = dbCommand.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        result = new FraudCheckUserData();
                        result.UserId = reader.GetStringExt("UserId");
                        result.AgeOfAccountSinceCreation = reader.GetInt32Ext("AgeOfAccountSinceCreation") ?? 0;

                        result.IsPartner = (reader.GetInt32Ext("IsPartner") ?? 0) == 1;
                        result.BirthDay = reader.GetInt32Ext("BirthDay") ?? 0;
                        result.BirthMonth = reader.GetInt32Ext("BirthMonth") ?? 0;
                        result.eMailSignUp = reader.GetBoolExt("eMailSignUp") ?? false;
                        result.MailSignUp = reader.GetBoolExt("mailSignUp") ?? false;
                        result.TextMessageSignUp = (reader.GetInt32Ext("TextMessageSignUp") ?? 0) == 1;
                        result.TwitterSignUp = (reader.GetInt32Ext("TwitterSignUp") ?? 0) == 1;
                        result.FacebookSignUp = (reader.GetInt32Ext("FacebookSignUp") ?? 0) == 1;
                    }
                }
            }

            return result;
        }

        public IFraudCheckCardData GetCardDataForFraudCheck(string userId, string cardId)
        {
            IFraudCheckCardData result = null;

            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand("GetCardDataForFraudCheck", connection))
            {
                connection.Open();
                dbCommand.Parameters.AddWithValue("@UserId", userId);
                dbCommand.Parameters.AddWithValue("@CardId", Encryption.DecryptCardId(cardId));

                using (IDataReader reader = dbCommand.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        result = new FraudCheckCardData();
                        result.Nickname = reader.GetStringExt("CardNickName");
                        result.IsDefaultSvcCard = reader.GetBoolExt("IsDefault") ?? false;
                        result.TotalCardsRegistered = reader.GetInt32Ext("TotalCardsRegistered") ?? 0;
                    }
                }
            }
            
            return result;
        }

        private static void MapAutoReloadProfile(Common.Models.IAutoReloadProfile autoReloadProfile, IDataRecord record)
        {
            autoReloadProfile.AutoReloadId = record.GetStringExt("g_auto_id");
            autoReloadProfile.UserId = record.GetStringExt("g_user_id");
            autoReloadProfile.CardId = Encryption.EncryptCardId(record.GetInt32Ext("CardId").GetValueOrDefault());
            autoReloadProfile.AutoReloadType = record.GetStringExt("u_auto_type");
            autoReloadProfile.Amount = record.GetDecimalExt("m_amount") ?? 0M;
            autoReloadProfile.Status = record.GetByteExt("i_status") ?? 0;
            autoReloadProfile.TriggerAmount = record.GetDecimalExt("m_trigger_amount");
            autoReloadProfile.Day = record.GetByteExt("i_trigger_date");
            autoReloadProfile.StoppedDate = record.GetDateTimeExt("d_date_stopped");
            autoReloadProfile.CreatedDate = record.GetDateTimeExt("d_date_created");
            autoReloadProfile.LastChangedDate = record.GetDateTimeExt("d_date_lastchanged");
            autoReloadProfile.DisableUntilDate = record.GetDateTimeExt("DisableUntilDate");
            autoReloadProfile.PaymentMethodId = Encryption.EncryptCardId(record.GetInt32Ext("PaymentMethodId").GetValueOrDefault());
        }

        private static void MapCardBalance(ICardBalance cardBalance, IDataRecord record)
        {
            cardBalance.CardBalanceId = record.GetInt32Ext("CardBalanceId") ?? 0;
            cardBalance.CardId = Encryption.EncryptCardId(record.GetInt32Ext("CardId").GetValueOrDefault());
            cardBalance.Balance = record.GetDecimalExt("Balance");
            cardBalance.BalanceDate = record.GetDateTimeExt("BalanceDate", DateTimeKind.Utc);
            cardBalance.Currency = record.GetStringExt("Currency");

        }

        private static void MapAssociatedCard(IAssociatedCard associatedCard, IDataRecord record)
        {
            associatedCard.AutoReloadId = record.GetStringExt("AutoReloadId");
            associatedCard.RegistrationDate = record.GetDateTimeExt("RegistrationDate");
            associatedCard.RegistrationSource = record.GetStringExt("RegistrationSource");
            //associatedCard.IsDigitalCard = IsDigitalCard(card.BatchCode);
            associatedCard.RegisteredUserId = record.GetStringExt("RegisteredUserId");
        }

        private static void MapAssociatedCardNickNameDefault(IAssociatedCard associatedCard, IDataRecord record)
        {
            var nickName = record.GetStringExt("Nickname");
            associatedCard.Nickname = string.IsNullOrEmpty(nickName) ? string.Format("My Card ({0})", Encryption.DecryptCardNumber(associatedCard.Number).Substring(12, 4)) : nickName;
            associatedCard.IsDefault = record.GetBoolExt("Default") ?? false;
            associatedCard.PlatformRegSourceCode = record.GetStringExt("PlatformRegSourceCode");
            associatedCard.MarketingRegSourceCode = record.GetStringExt("MarketingRegSourceCode");
        }
        public bool IsPlatformRegSourceCodeValid(string platformRegSourceCode)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand("ValidatePlatformRegSourceCode", connection))
            {
                connection.Open();

                dbCommand.Parameters.AddWithValue("@PlatformRegSourceCode", platformRegSourceCode);

                return (Convert.ToInt32(dbCommand.ExecuteScalar()) == 1);
            }
        }
        public bool IsMarketingRegSourceCodeValid(string marketingRegSourceCode)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = GetCommand("ValidateMarketingRegSourceCode", connection))
            {
                connection.Open();

                dbCommand.Parameters.AddWithValue("@MarketingRegSourceCode", marketingRegSourceCode);

                return (Convert.ToInt32(dbCommand.ExecuteScalar()) == 1);
            }
        }
        public bool LoadCardAssociation(IAssociatedCard card, string userId = null)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var command = GetCommand("GetCardAssociation", connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@CardId", Encryption.DecryptCardId(card.CardId));
                if (string.IsNullOrEmpty(userId))
                {
                    userId = card.RegisteredUserId;
                }
                command.Parameters.AddWithValue("@UserId", userId);

                //two fields are missing from get user
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        MapAssociatedCardNickNameDefault(card, reader);
                        return true;
                    }
                    return false;
                }
            }
        }


        private void MapCardRow(ICard card, IDataRecord record)
        {
            card.CardId = Encryption.EncryptCardId(record.GetInt32(record.GetOrdinal("CardId")));
            card.Name = record.GetStringExt("Name");
            card.Currency = record.GetStringExt("Currency");
            card.Class = record["Class"].ToString();
            //Check card class for eCard.
            //card.FormFactor = (eCardClassCodes != null && eCardClassCodes[card.Class] != null) ? (int)CardFormFactor.eCard : (int)CardFormFactor.Physical;
            card.Type =
                (CardType)
                Enum.Parse(typeof(CardType),
                           record.GetInt32Ext("CardTypeId").GetValueOrDefault().ToString(CultureInfo.InvariantCulture));
            card.Active = record.GetBoolExt("Active") ?? false;

            card.Balance = record.GetDecimalExt("Balance");
            card.BalanceDate = record.GetDateTimeExt("BalanceDate", DateTimeKind.Utc);

            card.BalanceCurrency = record.GetStringExt("BalanceCurrency");
            card.Number = record.GetStringExt("Number");
            card.Pin = record.GetStringExt("PIN");
            card.PinValidated = record.GetBoolExt("PINValidated") ?? false;
            card.ExpirationDate = record.GetDateTimeExt("ExpirationDate");

            card.BatchCode = record.GetStringExt("PromoCode");
            //card.CardRangeId = record.GetInt32Ext("CardRangeId");
            var subMarketCode = record.GetStringExt("SubMarketCode");
            card.SubMarketCode = string.IsNullOrEmpty(subMarketCode)
                                     ? _settingsProvider.GetMarketCodeByCurrencyCode(card.Currency)
                                     : _settingsProvider.GetStandardizedSubMarketCode(subMarketCode);
            card.IsPartner = IsPartnerCard(card.Class);
            card.IsDigitalCard = IsDigitalCard(card.BatchCode);
        }

        public string GetCardNumberById(string decryptedCardId)
        {
            //[dbo].[GetCardNumber] 
            using (var connection = new SqlConnection(_connectionString))
            using (var dbCommand = new SqlCommand("SELECT dbo.GetCardNumber(@CardId)", connection) { CommandType = CommandType.Text })
            {
                dbCommand.Parameters.AddWithValue("@CardId", decryptedCardId);

                object result = dbCommand.ExecuteScalar();
                return result is System.DBNull ? null : result.ToString();
            }
        }

        public IUserAccount GetUserAccount(string userId)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var command = GetCommand("GetUserAccountById", connection))
            {
                connection.Open();
                command.Parameters.AddWithValue("UserId", userId);

                using (var dataReader = command.ExecuteReader())
                {
                    UserAccount returnAccount = null;

                    if (dataReader.Read())
                    {
                        returnAccount = new UserAccount
                        {
                            UserId = dataReader["g_user_id"].ToString(),
                            UserName = dataReader["u_logon_name"].ToString(),
                            Password = dataReader["u_user_security_password"].ToString(),
                            EmailAddress = dataReader["u_email_address"] == DBNull.Value ? null : dataReader["u_email_address"].ToString(),
                            SubMarket = dataReader["SubMarket_id"] == DBNull.Value ? null : dataReader["SubMarket_id"].ToString(),
                        };
                    }
                    return returnAccount; 
                }
            }
        }

        /// <summary>
        /// ClearPaymentMethodSurrogateAndPaypalAccountNumbers (This function should expose from PaymentMethod.Dal)
        /// </summary>
        /// <param name="paymentMethod">The payment method to update</param>
        /// <returns>Returns the payment method ID</returns>
        /// 

        /*
         PDATE [PaymentMethod]
        SET [Nickname]        = ISNULL(@Nickname,      Nickname)
             ,[NameOnCard]      = ISNULL(@NameOnCard,    NameOnCard)
             ,[AccountNumber]   = ISNULL(@AccountNumber, AccountNumber)
             ,[IsDefault]       = ISNULL(@IsDefault,     [IsDefault])
             ,[ExpirationYear]  = @ExpirationYear
             ,[ExpirationMonth] = @ExpirationMonth
             ,[RoutingNumber]   = @RoutingNumber
             ,[BankName]        = @BankName
          ,[PaymentTypeId]   = @PaymentTypeId
          ,[AddressId]       = ISNULL(@AddressId, AddressId)
             ,[Active]          = @Active
             ,[DateModified]    = GETDATE()
             ,[FirstSix]        = ISNULL(@FirstSix, FirstSix)
             ,[LastFour]        = ISNULL(@LastFour, LastFour)
             ,[SurrogateAccountNumber] = ISNULL(@SurrogateAccountNumber, SurrogateAccountNumber)
             ,[IsTemporary] = @IsTemporary
             ,[IsFraudChecked] = 0
           WHERE UserId = @UserId AND PaymentMethodId = @PaymentMethodId
         */

        public long ClearPaymentMethodSurrogateAndPaypalAccountNumbers(string userId, IPaymentMethod paymentMethod)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var command = new SqlCommand("UpsertPaymentMethod", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                connection.Open();

                command.Parameters.AddWithValue("@userId", userId ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("@PaymentMethodId", Encryption.DecryptCardId(paymentMethod.PaymentMethodId));
                command.Parameters.AddWithValue("@AddressId", paymentMethod.BillingAddressId ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("@PaymentTypeId", paymentMethod.PaymentTypeId);
                command.Parameters.AddWithValue("@ExpirationYear", paymentMethod.ExpirationYear);
                command.Parameters.AddWithValue("@ExpirationMonth", paymentMethod.ExpirationMonth);
                command.Parameters.AddWithValue("@RoutingNumber", paymentMethod.RoutingNumber ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("@BankName", paymentMethod.BankName ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("@IsTemporary", paymentMethod.IsTemporary);

                command.Parameters.AddWithValue("@SurrogateAccountNumber", "");
                command.Parameters.AddWithValue("@AccountNumber", "");

                return Convert.ToInt64(command.ExecuteScalar());
            }
        }

    }
}
