﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.Card.Dal.Common.Models;

namespace Starbucks.Card.Dal.Sql.Models
{
    public class FraudCheckCardData : IFraudCheckCardData
    {
        public string Nickname { get; set; }
        public bool IsDefaultSvcCard { get; set; }
        public int TotalCardsRegistered { get; set; }
    }
}
