﻿using Starbucks.Card.Provider.Common.Enums;
using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Dal.Sql.Models
{
    public class StarbucksCardImage : IStarbucksCardImage
    {
        public CardImageType Type { get; set; }
        public string Uri { get; set; }
    }
}
