﻿using Starbucks.Card.Dal.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.Card.Dal.Sql.Models
{
    public class UserAccount : IUserAccount
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailAddress { get; set; }

        public string SecretQuestion { get; set; }

        public string SecretAnswer { get; set; }

        //public AccountStatus Status { get; set; }

        //public AccountCreationSource CreationSource { get; set; }

        public string PartnerNumber { get; set; }

        //public IEnumerable<ITelecomm> TelecommEntries { get; set; }

        public string SubMarket { get; set; }
    }
}
