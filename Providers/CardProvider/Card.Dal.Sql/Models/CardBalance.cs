﻿using System;
using Starbucks.Card.Dal.Common.Models;

namespace Starbucks.Card.Dal.Sql.Models
{
    public class CardBalance :ICardBalance
    {
        public int CardBalanceId { get; set; }
        public string CardId { get; set; }
        public decimal? Balance { get; set; }
        public DateTime? BalanceDate { get; set; }
        public string Currency { get; set; }
    }
}
