﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Account.Provider.Common;
using Account.Provider.Common.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NServiceBus;
using Starbucks.Card.Dal.Common;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.Card.Dal.Common.Models;
using Starbucks.Card.Provider;
using Starbucks.Card.Provider.Common.Enums;
using Starbucks.Card.Provider.Common.Exceptions;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.Card.Provider.Common.ErrorResources;
using Starbucks.Card.Provider.Configuration;
using Starbucks.Card.Provider.DalModels;
using Starbucks.Card.Provider.Models;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.FraudData.QueueService.Common.Commands;
using Starbucks.LogCounter.Provider.Common;
using Starbucks.MessageBroker.Common;
using Starbucks.OpenApi.Logging.Common;
using Starbucks.PaymentMethod.Provider.Common;
using Starbucks.PaymentMethod.Provider.Common.Models;
using Starbucks.Settings.Provider.Common;
using IAutoReloadProfile = Starbucks.Card.Provider.Common.Models.IAutoReloadProfile;
using ICardTransaction = Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction;

namespace Card.Provider.UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        private static CardProviderSettingsSection _settings;
        public static CardProviderSettingsSection Settings
        {
            get
            {
                return _settings ??
                       (_settings =
                        ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection);
            }
        }

        #region CardMasks

        [TestMethod]
        public void GetCardMasked()
        {
            const string cardNumber = "7777064110727700";
            const string pin = "26990217";

            var result = GetCardByCardNumberPinNumber(cardNumber, pin, VisibilityLevel.Masked);
            Assert.IsTrue(result.Number.StartsWith("x"));
            Assert.IsTrue(string.IsNullOrEmpty(result.Pin));
        }

        [TestMethod]
        public void GetCardDecrypted()
        {
            const string cardNumber = "7777064110727700";
            const string pin = "26990217";

            var result = GetCardByCardNumberPinNumber(cardNumber, pin, VisibilityLevel.Decrypted);
            Assert.AreEqual(result.Number, cardNumber);
            Assert.AreEqual(result.Pin, pin);
        }

        [TestMethod]
        public void GetCardEncrypted()
        {
            const string cardNumber = "7777064110727700";
            const string pin = "26990217";

            var result = GetCardByCardNumberPinNumber(cardNumber, pin, VisibilityLevel.Encrypted);
            Assert.AreEqual(result.Number, "836074FF9AD61DAB99F7991A26BFF6A3");
            Assert.AreEqual(result.Pin, "86617AF19AD218AD");
        }


        [TestMethod]
        public void GetCardNone()
        {
            const string cardNumber = "7777064110727700";
            const string pin = "26990217";

            var result = GetCardByCardNumberPinNumber(cardNumber, pin, VisibilityLevel.None);
            Assert.IsTrue(string.IsNullOrEmpty(result.Number));
            Assert.IsTrue(string.IsNullOrEmpty(result.Pin));
        }

        #endregion

        #region CardByNumberAndPin
        private static ICard GetCardByCardNumberPinNumber(string cardNumber, string pinNumber, VisibilityLevel visibilityLevel = VisibilityLevel.Decrypted)
        {
            var card = new Mock<IAssociatedCard>();
            card.SetupProperty(p => p.Number, "836074FF9AD61DAB99F7991A26BFF6A3").SetupProperty(p => p.Pin, "86617AF19AD218AD");

            var cardDal = new Mock<ICardDal>();
            var log = new Mock<ILogCounterManager>();
            var cardIssuer = new Mock<ICardIssuerDal>();
            var messageBroker = new Mock<IMessageBroker>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            //ICardIssuerDal cardIssuer, ILogCounterManager logCounterManager
            cardDal.Setup(cd => cd.GetCardByNumber(It.IsAny<string>()))
                   .Returns(card.Object);

            var balance = new Mock<ICardBalance>();
            balance.SetupProperty(p => p.Balance, 100M);
            cardDal.Setup(cd => cd.GetBalanceByNumber(It.IsAny<string>())).Returns(balance.Object);
            var nsb = new Mock<IBus>();

            var merchantInfo = new Mock<IMerchantInfo>();
            merchantInfo.SetupProperty(p => p.SubMarketId, "US");
            cardIssuer.Setup(p => p.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var provider = new CardProvider(cardDal.Object, cardIssuer.Object, log.Object, messageBroker.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);

            var result = provider.GetCardByNumberAndPin(cardNumber, pinNumber, visibilityLevel);
            return result;
        }

        [TestMethod]
        public void GetCardByCardNumberAndPin()
        {
            var result = GetCardByCardNumberPinNumber("0123456789101112", "26990217");

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetCardByCardNumberAndPinInvalidCardNumber()
        {
            CardProviderValidationException cardProviderValidationException = null;
            try
            {
                GetCardByCardNumberPinNumber("012346789101112", "26990217");
            }
            catch (CardProviderValidationException cpve)
            {
                cardProviderValidationException = cpve;
            }

            Assert.IsNotNull(cardProviderValidationException);
            Assert.AreEqual(cardProviderValidationException.Code,
                            CardProviderValidationErrorResource.ValidationCardNumberMissingCode);
            Assert.AreEqual(cardProviderValidationException.Message,
                            CardProviderValidationErrorResource.ValidationCardNumberMissingMessage);
        }

        [TestMethod]
        public void GetCardByCardNumberAndPinMissingCardNumber()
        {
            CardProviderValidationException cardProviderValidationException = null;
            try
            {
                GetCardByCardNumberPinNumber("", "01235678");
            }
            catch (CardProviderValidationException cpve)
            {
                cardProviderValidationException = cpve;
            }

            Assert.IsNotNull(cardProviderValidationException);
            Assert.AreEqual(cardProviderValidationException.Code,
                            CardProviderValidationErrorResource.ValidationCardNumberMissingCode);
            Assert.AreEqual(cardProviderValidationException.Message,
                            CardProviderValidationErrorResource.ValidationCardNumberMissingMessage);
        }

        [TestMethod]
        public void GetCardByCardNumberAndPinInvalidPinNumber()
        {
            CardProviderValidationException cardProviderValidationException = null;

            try
            {
                GetCardByCardNumberPinNumber("0123456789101213", "1235678");
            }
            catch (CardProviderValidationException cpve)
            {
                cardProviderValidationException = cpve;
            }
            Assert.IsNotNull(cardProviderValidationException);
            Assert.AreEqual(cardProviderValidationException.Code,
                            CardProviderValidationErrorResource.ValidationCardPinMissingCode);
            Assert.AreEqual(cardProviderValidationException.Message,
                            CardProviderValidationErrorResource.ValidationCardPinMissingMessage);
        }

        [TestMethod]
        public void GetCardByCardNumberAndPinMissingPinNumber()
        {
            CardProviderValidationException cardProviderValidationException = null;

            try
            {
                GetCardByCardNumberPinNumber("0123456789101213", "");
            }
            catch (CardProviderValidationException cpve)
            {
                cardProviderValidationException = cpve;
            }
            Assert.IsNotNull(cardProviderValidationException);
            Assert.AreEqual(cardProviderValidationException.Code,
                            CardProviderValidationErrorResource.ValidationCardPinMissingCode);
            Assert.AreEqual(cardProviderValidationException.Message,
                            CardProviderValidationErrorResource.ValidationCardPinMissingMessage);
        }


        [TestMethod]
        public void GetCardWithoutBalance()
        {
            var cardDal = new Mock<ICardDal>();
            var card = new Mock<IAssociatedCard>().SetupProperty(p => p.RegisteredUserId, "1").SetupProperty(p => p.CardId, "23");
            cardDal.Setup(cd => cd.GetCard(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).Returns(card.Object);

            var cardIssuerDal = new Mock<ICardIssuerDal>();

            var merchantInfo = new Mock<IMerchantInfo>()
               .SetupProperty(mi => mi.MarketId, "US").SetupProperty(mi => mi.Mid, "123456789");
            cardIssuerDal.Setup(cid => cid.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var cardTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>()
                        .SetupProperty(p => p.ResponseCode, "00")
                        .SetupProperty(p => p.BeginningBalance, 100M);
            cardTransaction.SetupGet(p => p.TransactionSucceeded).Returns(true);
            cardIssuerDal.Setup(cid => cid.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>())).Returns(cardTransaction.Object);


            var messageBrokerDal = new Mock<IMessageBroker>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);


            card.SetupProperty(p => p.Number, "836074FF9AD61DAB99F7991A26BFF6A3").SetupProperty(p => p.Pin, "86617AF19AD218AD");


            //ICardIssuerDal cardIssuer, ILogCounterManager logCounterManager
            cardDal.Setup(cd => cd.GetCardByNumber(It.IsAny<string>()))
                   .Returns(card.Object);

            var balance = new Mock<ICardBalance>();
            balance.SetupProperty(p => p.Balance, 100M);
            cardDal.Setup(cd => cd.GetBalanceByNumber(It.IsAny<string>())).Returns(balance.Object);


            var result = provider.GetCardByNumberAndPin("0123456789101213", "26990217", VisibilityLevel.Decrypted);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetCardByNumberAndPinCardNotFound()
        {
            var card = new Mock<IAssociatedCard>();
            card.SetupProperty(p => p.Number, "836074FF9AD61DAB99F7991A26BFF6A3").SetupProperty(p => p.Pin, "86617AF19AD218AD");

            var cardDal = new Mock<ICardDal>();
            var log = new Mock<ILogCounterManager>();
            var cardIssuer = new Mock<ICardIssuerDal>();
            var messageBroker = new Mock<IMessageBroker>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            //ICardIssuerDal cardIssuer, ILogCounterManager logCounterManager
            cardDal.Setup(cd => cd.GetCardByNumber(It.IsAny<string>()));

            var balance = new Mock<ICardBalance>();
            balance.SetupProperty(p => p.Balance, 100M);
            cardDal.Setup(cd => cd.GetBalanceByNumber(It.IsAny<string>())).Returns(balance.Object);

            var cardBalanceTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>();
            cardBalanceTransaction.SetupProperty(p => p.ResponseCode, "00").SetupProperty(p => p.CardClass, "225");

            cardBalanceTransaction.SetupGet(p => p.TransactionSucceeded).Returns(true);
            cardIssuer.Setup(ci => ci.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>())).Returns(cardBalanceTransaction.Object);

            var merchantInfo = new Mock<IMerchantInfo>();
            merchantInfo.SetupProperty(p => p.SubMarketId, "US");
            cardIssuer.Setup(p => p.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuer.Object, log.Object, messageBroker.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);

            CardProviderException cardProviderException = null;
            try
            {
                provider.GetCardByNumberAndPin("0123456789101112", "12345678", VisibilityLevel.Decrypted);
            }
            catch (CardProviderException cpe)
            {
                cardProviderException = cpe;
            }
            Assert.IsNotNull(cardProviderException);
            Assert.AreEqual(cardProviderException.Code, CardProviderErrorResource.CardNotFoundCode);
        }

        #endregion

        #region CardByUserIdCardId
        private static IAssociatedCard GetCardByCardId(string userId, string cardId)
        {
            var card = new Mock<IAssociatedCard>();
            var cardBalance = new Mock<ICardBalance>();
            card.SetupProperty(p => p.RegisteredUserId, userId)
                .SetupProperty(p => p.AutoReloadId, "123")
                .SetupProperty(p => p.Balance, 100M)
                .SetupProperty(p => p.BalanceDate, DateTime.Now)
                .SetupProperty(p => p.Currency, "USD")
            .SetupProperty(p => p.BalanceCurrency, "USD");
            var cardDal = new Mock<ICardDal>();
            cardDal.Setup(cd => cd.GetCard(userId, It.IsAny<string>(), It.IsAny<bool>()))
                   .Returns(card.Object);


            var autoReload = new Mock<Starbucks.Card.Dal.Common.Models.IAutoReloadProfile>();
            cardDal.Setup(cd => cd.GetAutoReload(It.IsAny<string>(), It.IsAny<string>())).Returns(autoReload.Object);

            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var merchantInfo = new Mock<IMerchantInfo>();
            merchantInfo.SetupProperty(p => p.CurrencyCode, "USD");
            cardIssuerDal.Setup(p => p.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);

            var result = provider.GetCardById(userId, cardId, VisibilityLevel.Decrypted, true, null, null);
            return result;
        }

        [TestMethod]
        public void GetCardByUserIdCardId()
        {
            var result = GetCardByCardId("123", "123");
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetCardByUserIdCardIdInvalidUser()
        {
            CardProviderValidationException cardProviderValidationException = null;
            try
            {
                GetCardByCardId("", "134");
            }
            catch (CardProviderValidationException exception)
            {
                cardProviderValidationException = exception;
            }

            Assert.IsNotNull(cardProviderValidationException);
            Assert.AreEqual(cardProviderValidationException.Code, CardProviderValidationErrorResource.ValidationCardNoUserCode);
            Assert.AreEqual(cardProviderValidationException.Message, CardProviderValidationErrorResource.ValidationCardNoUserMessage);
        }

        [TestMethod]
        public void GetCardByUserIdCardIdInvalidCard()
        {
            CardProviderValidationException cardProviderValidationException = null;
            try
            {
                GetCardByCardId("134", "");
            }
            catch (CardProviderValidationException exception)
            {
                cardProviderValidationException = exception;
            }

            Assert.IsNotNull(cardProviderValidationException);
            Assert.AreEqual(cardProviderValidationException.Code, CardProviderValidationErrorResource.ValidationCardCardIdMissingCode);
            Assert.AreEqual(cardProviderValidationException.Message, CardProviderValidationErrorResource.ValidationCardCardIdMissingMessage);
        }
        #endregion

        #region GetStarbucksCardImageUrlByCardNumber
        [TestMethod]
        public void GetStarbucksCardImageUrlByCardNumber()
        {
            var images = new Mock<IEnumerable<IStarbucksCardImage>>();
            var card = new Mock<IAssociatedCard>();
            var cardDal = new Mock<ICardDal>();
            cardDal.Setup(cd => cd.GetCardByNumber(It.IsAny<string>()))
                   .Returns(card.Object);


            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);

            var result = provider.GetStarbucksCardImageUrlByCardNumber("0123456789101212");
            Assert.IsNotNull(result);
        }
        #endregion

        #region GetStarbucksCards

        [TestMethod]
        public void GetStarbucksCards()
        {
            var cards = new Mock<List<IAssociatedCard>>();

            var card = new Mock<IAssociatedCard>();
            card.SetupAllProperties();
            card.SetupProperty(p => p.AutoReloadId, "123")
                .SetupProperty(p => p.Balance, 100M)
                .SetupProperty(p => p.BalanceDate, DateTime.Now)
                .SetupProperty(p => p.RegisteredUserId, "123")
                .SetupProperty(p => p.IsOwner, true);

            cards.Object.Add(card.Object);

            var card2 = new Mock<IAssociatedCard>();
            card.SetupAllProperties();
            card.SetupProperty(p => p.AutoReloadId, "123")
                .SetupProperty(p => p.RegisteredUserId, "123")
                .SetupProperty(p => p.IsOwner, true);

            cards.Object.Add(card2.Object);


            var autoReload = new Mock<Starbucks.Card.Dal.Common.Models.IAutoReloadProfile>();

            var cardDal = new Mock<ICardDal>();
            cardDal.Setup(cd => cd.GetAssociatedCards(It.IsAny<string>()))
                   .Returns(cards.Object);


            cardDal.Setup(cd => cd.GetAutoReload(It.IsAny<string>(), It.IsAny<string>())).Returns(autoReload.Object);


            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var merchantInfo = new Mock<IMerchantInfo>()
              .SetupProperty(mi => mi.MarketId, "US").SetupProperty(mi => mi.Mid, "123456789");
            cardIssuerDal.Setup(cid => cid.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var cardTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>()
                        .SetupProperty(p => p.ResponseCode, "00")
                        .SetupProperty(p => p.BeginningBalance, 100M);
            cardTransaction.SetupGet(p => p.TransactionSucceeded).Returns(true);
            cardIssuerDal.Setup(cid => cid.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>())).Returns(cardTransaction.Object);
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);

            var result = provider.GetCards("123", VisibilityLevel.Decrypted, true, true, "US", "US");

            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count(), 2);
        }


        #endregion

        #region GetStarbucksCardTransactions
        //[TestMethod]
        //public void GetStarbucksCardTransactions()
        //{
        //    var transactions = new Mock<IList<IStarbucksCardTransaction>>();
        //    var trans = new Mock<IStarbucksCardTransaction>();
        //    transactions.Object.Add(trans.Object);


        //    var cardDal = new Mock<ICardDal>();
        //    cardDal.Setup(cd => cd.(It.IsAny<string>(), It.IsAny<string>())).Returns(transactions.Object);


        //    var cardIssuerDal = new Mock<ICardIssuerDal>();
        //    var messageBrokerDal = new Mock<IMessageBroker>();
        //    var logCounterManager = new Mock<ILogCounterManager>();

        //    var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object);

        //    var result = provider.GetStarbucksCardTransactions("1", "2");

        //    Assert.IsNotNull(result);
        //}
        #endregion

        #region GetStatusesForUserCards
        [TestMethod]
        public void GetStatusesForUserCards()
        {
            var cards = new List<IAssociatedCard>();
            var card = new Mock<IAssociatedCard>();
            card.SetupProperty(p => p.RegisteredUserId, "1");
            card.SetupProperty(p => p.CardId, "2");
            card.SetupProperty(p => p.IsOwner, true);
            cards.Add(card.Object);


            var cardDal = new Mock<ICardDal>();
            cardDal.Setup(cd => cd.GetAssociatedCards(It.IsAny<string>())).Returns(cards);

            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);

            var result = provider.GetStatusesForUserCards("1", new List<string> { "2" });

            Assert.IsNotNull(result);
        }
        #endregion

        #region RefreshStarbucksCardBalance
        [TestMethod]
        public void RefreshStarbucksCardBalance()
        {
            var balance = new Mock<ICardBalance>();
            var card = new Mock<IAssociatedCard>();
            card.SetupProperty(c => c.RegisteredUserId, "1").SetupProperty(c => c.Balance, 100M).SetupProperty(c => c.BalanceDate, DateTime.Now).SetupProperty(c => c.BalanceCurrency, "USD")
                .SetupProperty(c => c.Currency, "USD");

            var cardDal = new Mock<ICardDal>();
            cardDal.Setup(cd => cd.GetCard(It.IsAny<string>())).Returns(card.Object);

            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var merchantInfo = new Mock<IMerchantInfo>();
            merchantInfo.SetupProperty(p => p.CurrencyCode, "USD");
            cardIssuerDal.Setup(p => p.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);

            var result = provider.GetCardBalance("1", "23", "US", "US");

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void RefreshStarbucksCardBalanceRealTime()
        {
            var cardDal = new Mock<ICardDal>();
            var card = CreateAssociatedCard(); // new Mock<IAssociatedCard>().SetupProperty(p => p.Balance, 100M).SetupProperty(p => p.RegisteredUserId, "1").SetupProperty(p => p.CardId, "23").SetupProperty(p => p.BalanceDate, DateTime.Now);
            cardDal.Setup(cd => cd.GetCard(It.IsAny<string>())).Returns(card);

            var cardIssuerDal = new Mock<ICardIssuerDal>();

            var merchantInfo = new Mock<IMerchantInfo>()
               .SetupProperty(mi => mi.MarketId, "US").SetupProperty(mi => mi.Mid, "123456789");
            cardIssuerDal.Setup(cid => cid.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var cardTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>()
                        .SetupProperty(p => p.ResponseCode, "00")
                        .SetupProperty(p => p.BeginningBalance, 100M);
            cardTransaction.SetupGet(p => p.TransactionSucceeded).Returns(true);
            cardIssuerDal.Setup(cid => cid.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>())).Returns(cardTransaction.Object);


            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);

            var result = provider.GetCardBalanceRealTime("1", "23", "US", "US");

            Assert.IsNotNull(result);
        }

        //[TestMethod]
        //public void RefreshStarbucksCardBalanceNullBalance()
        //{
        //    var card = new Mock<IAssociatedCard>();
        //    card.SetupProperty(c => c.RegisteredUserId, "1");
        //    var cardDal = new Mock<ICardDal>();
        //    cardDal.Setup(cd => cd.GetBalanceByCardId(It.IsAny<string>()));
        //    cardDal.Setup(cd => cd.GetCard(It.IsAny<string>(), It.IsAny<string>())).Returns(card.Object);

        //    var cardIssuerDal = new Mock<ICardIssuerDal>();
        //    var messageBrokerDal = new Mock<IMessageBroker>();
        //    var logCounterManager = new Mock<ILogCounterManager>();
        //    var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object);

        //    var result = provider.GetCardBalance("1", "23");

        //    Assert.IsNull(result);
        //}
        #endregion

        #region RefreshStarbucksCardBalanceByCarNumberPinNumber
        [TestMethod]
        public void RefreshStarbucksCardBalanceByCarNumberPinNumber()
        {

            var balance = new Mock<ICardBalance>();
            var card = CreateAssociatedCard();
            var cardDal = new Mock<ICardDal>();
            cardDal.Setup(cd => cd.GetBalanceByNumber(It.IsAny<string>())).Returns(balance.Object);
            cardDal.Setup(cd => cd.GetCardByNumber(It.IsAny<string>())).Returns(card);


            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);


            var result = provider.GetCardBalanceByCardNumberPinNumber("0123456789101112", "01235678", "US");

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void RefreshStarbucksCardBalanceByCardNumberPinNumberRealtime()
        {
            var cardDal = new Mock<ICardDal>();
            var card = new Mock<IAssociatedCard>().SetupProperty(p => p.Balance, 100M).SetupProperty(p => p.RegisteredUserId, "1").SetupProperty(p => p.CardId, "23").SetupProperty(p => p.BalanceDate, DateTime.Now);
            cardDal.Setup(cd => cd.GetCard(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).Returns(card.Object);

            var cardIssuerDal = new Mock<ICardIssuerDal>();

            var merchantInfo = new Mock<IMerchantInfo>()
               .SetupProperty(mi => mi.MarketId, "US").SetupProperty(mi => mi.Mid, "123456789");
            cardIssuerDal.Setup(cid => cid.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var cardTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>()
                        .SetupProperty(p => p.ResponseCode, "00")
                        .SetupProperty(p => p.BeginningBalance, 100M);
            cardTransaction.SetupGet(p => p.TransactionSucceeded).Returns(true);
            cardIssuerDal.Setup(cid => cid.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>())).Returns(cardTransaction.Object);


            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);

            var result = provider.GetCardBalanceByCardNumberPinNumberRealTime("012345678910112", "12345678", "US");

            Assert.IsNotNull(result);
        }

        //[TestMethod]
        //public void RefreshStarbucksCardBalanceByCarNumberPinNumberNullBalance()
        //{        
        //    var balance = new Mock<ICardBalance>();
        //    var card = new Mock<IAssociatedCard >();
        //    card.SetupProperty(c => c.RegisteredUserId, "1");           

        //    var cardDal = new Mock<ICardDal>();
        //    cardDal.Setup(cd => cd.GetBalanceByNumber( It.IsAny<string>()));
        //    cardDal.Setup(cd => cd.GetCardByNumber( It.IsAny<string>())).Returns(card.Object);


        //    var cardIssuerDal = new Mock<ICardIssuerDal>();
        //    var messageBrokerDal = new Mock<IMessageBroker>();
        //    var logCounterManager = new Mock<ILogCounterManager>();
        //    var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object);

        //    var result = provider.GetCardBalanceByCardNumberPinNumber( "0123456789101112", "01235678");
        //    Assert.IsNull(result);
        //}
        #endregion




        #region AutoReload
        [TestMethod]
        public void EnableAutoReload()
        {
            var cardDal = new Mock<ICardDal>();
            cardDal.Setup(cd => cd.UpdateAutoReloadStatus(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()));
            cardDal.Setup(cd => cd.IsAutoReloadAssociatedToPaymentMethod(It.IsAny<string>(), It.IsAny<string>()))
                   .Returns(true);

            var card = CreateAssociatedCard();
            var autoReload = new Mock<IAutoReloadProfile>();

            cardDal.Setup(cd => cd.GetCard(It.IsAny<string>()))
                   .Returns(card);
            var reloadProfile =
                new Mock<Starbucks.Card.Dal.Common.Models.IAutoReloadProfile>().SetupProperty(p => p.Amount, 100M)
                                              .SetupProperty(p => p.TriggerAmount, 10M)
                                              .SetupProperty(p => p.AutoReloadType, "Amount")
                                              .SetupProperty(p => p.PaymentMethodId, "123123");

            cardDal.Setup(cd => cd.GetAutoReload(It.IsAny<string>(), It.IsAny<string>())).Returns(reloadProfile.Object);

            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();

            var merchantInfo = new Mock<IMerchantInfo>();
            merchantInfo.SetupProperty(p => p.SubMarketId, "US");
            cardIssuerDal.Setup(p => p.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var accountProvider = new Mock<IAccountProvider>();
            var user = new Mock<IUser>();
            user.SetupProperty(p => p.EmailAddress, "test@test.com");
            accountProvider.Setup(p => p.GetUser(It.IsAny<string>())).Returns(user.Object);

            var paymentProvider = new Mock<IPaymentMethodProvider>();
            var paymentMethod = new Mock<IPaymentMethod>();
            paymentMethod.SetupProperty(p => p.PaymentTypeId, (int)PaymentType.Visa);

            paymentProvider.Setup(p => p.GetPaymentMethod(It.IsAny<string>(), It.IsAny<string>())).Returns(paymentMethod.Object);

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object,
                messageListener.Object, null, nsb.Object, paymentMethodProvider: paymentProvider.Object, accountProvider: accountProvider.Object);

            provider.EnableAutoReload("1", "2", "US");
        }

        [TestMethod]
        public void DisableAutoReload()
        {
            var cardDal = new Mock<ICardDal>();
            cardDal.Setup(cd => cd.UpdateAutoReloadStatus(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()));
            cardDal.Setup(cd => cd.IsAutoReloadAssociatedToPaymentMethod(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(true);



            var autoReload = new Mock<IAutoReloadProfile>();
            var card = CreateAssociatedCard();

            cardDal.Setup(cd => cd.GetCard(It.IsAny<string>()))
                   .Returns(card);

            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);


            provider.DisableAutoReload("1", "2", "US");
            nsb.Verify(c => c.Send(It.IsAny<DisableAutoReload>()), Times.Once());
        }

        [TestMethod]
        public void SetupAutoUpload()
        {
            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();

            var merchantInfo = new Mock<IMerchantInfo>();
            merchantInfo.SetupProperty(p => p.SubMarketId, "US");
            cardIssuerDal.Setup(p => p.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var accountProvider = new Mock<IAccountProvider>();
            var user = new Mock<IUser>();
            user.SetupProperty(p => p.EmailAddress, "test@test.com");
            accountProvider.Setup(p => p.GetUser(It.IsAny<string>())).Returns(user.Object);

            var paymentProvider = new Mock<IPaymentMethodProvider>();
            var paymentMethod = new Mock<IPaymentMethod>();
            paymentMethod.SetupProperty(p => p.PaymentTypeId, (int)PaymentType.PayPal);

            paymentProvider.Setup(p => p.GetPaymentMethod(It.IsAny<string>(), It.IsAny<string>())).Returns(paymentMethod.Object);

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object,
                messageListener.Object, null, nsb.Object, paymentMethodProvider: paymentProvider.Object, accountProvider: accountProvider.Object);

            var profile =
                new Mock<IAutoReloadProfile>().SetupProperty(p => p.Amount, 100M)
                                              .SetupProperty(p => p.TriggerAmount, 10M)
                                              .SetupProperty(p => p.AutoReloadType, "Amount")
                                              .SetupProperty(p => p.PaymentMethodId, "123123");

            var card = CreateAssociatedCard();
            cardDal.Setup(x => x.GetCard(It.IsAny<string>())).Returns(card);
            cardDal.Setup(cd => cd.SetupAutoReload(It.IsAny<Starbucks.Card.Dal.Common.Models.IAutoReloadProfile>()));

            provider.SetupAutoReload("1", "1", profile.Object, "US");
        }

        [TestMethod]
        public void SetupAutoUploadNoPaymentMethod()
        {
            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();

            var merchantInfo = new Mock<IMerchantInfo>();
            merchantInfo.SetupProperty(p => p.SubMarketId, "US");
            cardIssuerDal.Setup(p => p.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);

            var profile =
                new Mock<IAutoReloadProfile>().SetupProperty(p => p.Amount, 100M)
                                              .SetupProperty(p => p.TriggerAmount, 10M)
                                              .SetupProperty(p => p.AutoReloadType, "Amount")
                                              .SetupProperty(p => p.Status, 1);

            cardDal.Setup(cd => cd.SetupAutoReload(It.IsAny<Starbucks.Card.Dal.Common.Models.IAutoReloadProfile>()));

            CardProviderValidationException cardProviderValidationException = null;
            try
            {
                provider.SetupAutoReload("1", "1", profile.Object, "US");
            }
            catch (CardProviderValidationException ex)
            {
                cardProviderValidationException = ex;
            }

            Assert.IsNotNull(cardProviderValidationException);
            Assert.AreEqual(cardProviderValidationException.Code,
                            CardProviderValidationErrorResource.ValidationNoPaymentMethodCode);
        }

        [TestMethod]
        public void UpdateAutoReload()
        {
            var cardDal = new Mock<ICardDal>();
            var cardIssuer = new Mock<ICardIssuerDal>();

            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();

            var merchantInfo = new Mock<IMerchantInfo>();
            merchantInfo.SetupProperty(p => p.SubMarketId, "US");
            cardIssuer.Setup(p => p.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var accountProvider = new Mock<IAccountProvider>();
            var user = new Mock<IUser>();
            user.SetupProperty(p => p.EmailAddress, "test@test.com");
            accountProvider.Setup(p => p.GetUser(It.IsAny<string>())).Returns(user.Object);

            var paymentProvider = new Mock<IPaymentMethodProvider>();
            var paymentMethod = new Mock<IPaymentMethod>();
            paymentMethod.SetupProperty(p => p.PaymentTypeId, (int)PaymentType.PayPal);

            paymentProvider.Setup(p => p.GetPaymentMethod(It.IsAny<string>(), It.IsAny<string>())).Returns(paymentMethod.Object);

            var provider = new CardProvider(cardDal.Object, cardIssuer.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object,
                messageListener.Object, null, nsb.Object, paymentMethodProvider: paymentProvider.Object, accountProvider: accountProvider.Object);

            var profile =
                new Mock<Starbucks.Card.Dal.Common.Models.IAutoReloadProfile>().SetupProperty(p => p.Amount, 100M)
                                              .SetupProperty(p => p.TriggerAmount, 10M)
                                              .SetupProperty(p => p.AutoReloadType, "Amount")
                                              .SetupProperty(p => p.PaymentMethodId, "123123")
                                              .SetupProperty(p => p.UserId, "1");

            var otherProfile =
               new Mock<IAutoReloadProfile>().SetupProperty(p => p.Amount, 100M)
                                             .SetupProperty(p => p.TriggerAmount, 10M)
                                             .SetupProperty(p => p.AutoReloadType, "Amount")
                                             .SetupProperty(p => p.PaymentMethodId, "123123")
                                             .SetupProperty(p => p.UserId, "1");

            var card = CreateAssociatedCard();


            cardDal.Setup(cd => cd.GetAutoReload(It.IsAny<string>(), It.IsAny<string>())).Returns(profile.Object);
            cardDal.Setup(cd => cd.UpdateAutoReload(It.IsAny<Starbucks.Card.Dal.Common.Models.IAutoReloadProfile>()));
            cardDal.Setup(cd => cd.GetCard(It.IsAny<string>())).Returns(card);
            provider.UpdateAutoReload("1", "1", otherProfile.Object, "US");
        }

        #endregion

        #region Transfer

        [TestMethod]
        public void TransferRegisteredAmount()
        {
            CardProviderSettingsSection cardProviderSettingsSection = ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection;

            const decimal transferAmount = 100M;
            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var cardFrom = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "2",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A3",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("2")).Returns(cardFrom);

            var cardTo = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "3",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "846671FB9ED51FAD90FE9F1820B9F7A1",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("3")).Returns(cardTo);


            cardDal.Setup(cd => cd.UpsertCardTransactionHistory(It.IsAny<string>(), It.IsAny<Starbucks.Card.Provider.Common.Models.ICardTransaction>()));
            cardDal.Setup(
                cd =>
                cd.UpsertCardBalance(It.IsAny<string>(), It.IsAny<decimal>(), It.IsAny<DateTime>(), It.IsAny<string>()));

            var cardFromBalance =
                new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>().SetupProperty(p => p.EndingBalance, 120M)
                                        .SetupProperty(p => p.TransactionDate, DateTime.Now);
            cardIssuerDal.Setup(cid => cid.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>()))
                         .Returns(cardFromBalance.Object);

            cardIssuerDal.Setup(
                cid =>
                cid.UpsertPartialTransferTransactionDetails(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<DateTime>(),
                                                            It.IsAny<string>(), It.IsAny<DateTime?>())).Returns(1);



            var merchantInfo = new Mock<IMerchantInfo>().SetupProperty(p => p.Mid, "1231212");
            cardIssuerDal.Setup(cid => cid.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var reloadTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>().SetupProperty(p => p.ResponseCode, "00");
            reloadTransaction.SetupGet(p => p.TransactionSucceeded).Returns(true);
            cardIssuerDal.Setup(
                p => p.Reload(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<decimal>()))
                         .Returns(reloadTransaction.Object);

            var redeemTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>().SetupProperty(p => p.ResponseCode, "00");
            redeemTransaction.SetupGet(p => p.TransactionSucceeded).Returns(true);
            redeemTransaction.SetupGet(p => p.Amount).Returns(transferAmount);
            cardIssuerDal.Setup(
                p => p.Redeem(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<decimal>()))
                         .Returns(redeemTransaction.Object);

            Mock<ICardTransaction> cardTransaction = new Mock<ICardTransaction>();
            cardTransaction.SetupProperty(p => p.RequestCode, "0200");
            cardTransaction.SetupProperty(p => p.UtcDate, DateTime.UtcNow.AddDays(1));
            cardIssuerDal.Setup(
                p => p.GetHistoryCondensed(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new List<ICardTransaction> { cardTransaction.Object });
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);
            var result = provider.TransferRegistered("1", "2", "3", transferAmount, "US");

            Assert.IsNotNull(result);
        }

        // msharp - this test is woefully out of date, I'll resume fixing it later.
        [Ignore]
        [TestMethod]
        public void TransferRegisteredBalance()
        {
            CardProviderSettingsSection cardProviderSettingsSection = ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection;

            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var cardFrom = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "2",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A3",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };
            cardDal.Setup(cd => cd.GetCard("2")).Returns(cardFrom);
            cardDal.Setup(cd => cd.GetCardByNumber("7777064110727700")).Returns(cardFrom);

            var cardTo = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "3",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A3",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };
            cardDal.Setup(cd => cd.GetCard("3")).Returns(cardTo);


            cardDal.Setup(cd => cd.UpsertCardTransactionHistory(It.IsAny<string>(), It.IsAny<Starbucks.Card.Provider.Common.Models.ICardTransaction>()));
            cardDal.Setup(
                cd =>
                cd.UpsertCardBalance(It.IsAny<string>(), It.IsAny<decimal>(), It.IsAny<DateTime>(), It.IsAny<string>()));

            var cardFromBalance =
                new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>().SetupProperty(p => p.EndingBalance, 120M)
                                        .SetupProperty(p => p.TransactionDate, DateTime.Now);
            cardFromBalance.SetupGet(p => p.TransactionSucceeded).Returns(true);
            cardIssuerDal.Setup(cid => cid.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>()))
                         .Returns(cardFromBalance.Object);

            var merchantInfo = new Mock<IMerchantInfo>().SetupProperty(p => p.Mid, "1231212");
            cardIssuerDal.Setup(cid => cid.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var cardTransferTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>().SetupProperty(p => p.ResponseCode, "00");
            cardTransferTransaction.SetupGet(p => p.TransactionSucceeded).Returns(true);
            cardIssuerDal.Setup(
                p => p.TransferBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>()))
                         .Returns(cardTransferTransaction.Object);

            /*var cardTransaction = new CardTransaction(new Starbucks.Card.Provider.CardIssuerModels.CardTransaction {})
            cardTransaction.SetupProperty(p => p.TransactionSucceeded, true);
            cardIssuerDal.Setup(
                p => p.Reload(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<decimal>()))
                .Returns(cardTransaction);*/
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);
            var result = provider.TransferRegistered("1", "2", "3", null, "US");

            Assert.IsNotNull(result);
        }
        #endregion

        #region UnregisterCard

        [TestMethod]
        public void UnregisterCard()
        {
            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var card = new Mock<IAssociatedCard>().SetupProperty(p => p.CardId, "2").SetupProperty(p => p.RegisteredUserId, "1").SetupProperty(p => p.IsDefault, true);
            cardDal.Setup(cd => cd.GetCard(It.IsAny<string>())).Returns(card.Object);

            var cardBalance = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>();
            cardIssuerDal.Setup(cid => cid.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>())).Returns(cardBalance.Object);

            cardDal.Setup(cd => cd.UnregisterCard(It.IsAny<string>(), It.IsAny<string>()));
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);
            var result = provider.UnregisterCard("1", "2", true, "US");
            Assert.IsTrue(result);
            nsb.Verify(c => c.Send(It.IsAny<RemoveCard>()), Times.Once());

        }
        [TestMethod]
        public void UnregisterDigitalCard()
        {
            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var card = new Mock<IAssociatedCard>().SetupProperty(p => p.CardId, "2").SetupProperty(p => p.RegisteredUserId, "1").SetupProperty(p => p.IsDefault, true);
            card.SetupGet(p => p.IsDigitalCard).Returns(true);

            cardDal.Setup(cd => cd.GetCard(It.IsAny<string>())).Returns(card.Object);

            var cardBalance = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>();
            cardIssuerDal.Setup(cid => cid.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>())).Returns(cardBalance.Object);

            cardDal.Setup(cd => cd.UnregisterCard(It.IsAny<string>(), It.IsAny<string>()));
            var nsb = new Mock<IBus>();

            var merchantInfo = new Mock<IMerchantInfo>();
            merchantInfo.SetupProperty(p => p.SubMarketId, "US");
            cardIssuerDal.Setup(p => p.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);
            var result = provider.UnregisterCard("1", "2", true, "US");

            Assert.IsTrue(result);
            nsb.Verify(c => c.Send(It.IsAny<RemoveCard>()), Times.Once());
        }
        #endregion

        #region UpdateCards
        [TestMethod]
        public void UpdateCardNickname()
        {
            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);
            provider.UpdateCardNickname("1", "2", "new");
        }

        [TestMethod]
        public void UpdateDefaultCard()
        {
            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var card = new Mock<IAssociatedCard>().SetupProperty(p => p.RegisteredUserId, "1").SetupProperty(p => p.CardId, "1");
            cardDal.Setup(cd => cd.GetCard(It.IsAny<string>())).Returns(card.Object);
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);
            provider.UpdateDefaultCard("1", "1");
        }

        #endregion

        #region ClearPaymentMethodSurrogateAndPaypalAccountNumbers

        private Starbucks.PaymentMethod.Provider.Common.Models.IPaymentMethod CreatePaymentMethod(string paymentMethodId)
        {
            var mock = new Mock<Starbucks.PaymentMethod.Provider.Common.Models.IPaymentMethod>();
            mock.SetupGet(x => x.PaymentMethodId).Returns(paymentMethodId);
            return mock.Object;
        }

        [TestMethod]
        public void ClearPaymentMethodSurrogateAndPaypalAccountNumbers_Success()
        {
            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();

            const long paymentMethodId = 25;
            cardDal.Setup(cd => cd.ClearPaymentMethodSurrogateAndPaypalAccountNumbers(It.IsAny<string>(), It.IsAny<Starbucks.PaymentMethod.Provider.Common.Models.IPaymentMethod>())).Returns(paymentMethodId);

            string userId = "00000000-0000-0000-0000-000000000000";
            var paymentMethod = CreatePaymentMethod(paymentMethodId.ToString("G"));

            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object,
                settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);

            Assert.AreEqual(paymentMethodId.ToString(), provider.ClearPaymentMethodSurrogateAndPaypalAccountNumbers(userId, paymentMethod));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ClearPaymentMethodSurrogateAndPaypalAccountNumbers_FailWithNullPaymentMethodId()
        {
            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            //            var card = new Mock<IAssociatedCard>().SetupProperty(p => p.CardId, "2").SetupProperty(p => p.RegisteredUserId, "1").SetupProperty(p => p.IsDefault, true);

            string userId = "00000000-0000-0000-0000-000000000000";
            var paymentMethod = CreatePaymentMethod(null);

            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);

            provider.ClearPaymentMethodSurrogateAndPaypalAccountNumbers(userId, paymentMethod);
        }
        #endregion

        private AssociatedCard CreateAssociatedCard()
        {
            var card = new AssociatedCard(ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection);
            card.Currency = "USD";
            card.Actions = new List<string>();
            card.Balance = 100M;
            card.RegisteredUserId = "1";
            card.AutoReloadId = "13";
            card.Pin = "01235678";
            return card;
        }

        private static Mock<ISettingsProvider> CreateSettingProvider()
        {
            var settingProvider = new Mock<ISettingsProvider>();
            settingProvider.Setup(x => x.GetCurrencyCodeForMarketCode(It.IsAny<string>())).Returns("USD");
            return settingProvider;
        }
    }
}
