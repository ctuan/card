﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NServiceBus;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.Card.Provider;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.Card.Provider.Configuration;
using Starbucks.Card.Provider.Models;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.Common.Resources;
using Starbucks.FraudData.QueueService.Common.Commands;
using Starbucks.LogCounter.Provider.Common;
using Starbucks.MessageBroker.Common;
using Starbucks.OpenApi.Logging.Common;
using Starbucks.Settings.Provider.Common;

namespace Card.Provider.UnitTest
{
    [TestClass]
    public class RegisterCardTests : CardProviderTests
    {


        [TestMethod]
        public void AddCardAssociation()
        {
            var cardDal = new Mock<ICardDal>();
            cardDal.Setup(cd => cd.GetPromoCode(It.IsAny<string>())).Returns("1234");

            var card = new Mock<IAssociatedCard>();
            card.SetupProperty(p => p.Number, "0123456789101112")
                .SetupProperty(p => p.Pin, "01235678")
                .SetupProperty(p => p.CardId, "1234");
            cardDal.Setup(cd => cd.GetCardByNumber(It.IsAny<string>())).Returns(card.Object);


            var cardIssuerDal = new Mock<ICardIssuerDal>();

            var merchantInfo = new Mock<IMerchantInfo>()
                .SetupProperty(mi => mi.MarketId, "US").SetupProperty(mi => mi.Mid, "123456789");
            cardIssuerDal.Setup(cid => cid.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var cardTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>()
                .SetupProperty(p => p.ResponseCode, "00")
                .SetupProperty(p => p.CardNumber, "7777064110727700")
                .SetupProperty(ct => ct.CardClass, "125");
            cardIssuerDal.Setup(cid => cid.ActivateVirtual(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<decimal>()))
                         .Returns(cardTransaction.Object);

            var cardTransaction2 = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>()
                .SetupProperty(p => p.ResponseCode, "00")
                .SetupProperty(ct => ct.CardClass, "125")
                .SetupProperty(p => p.CardNumber, "7777064110727700");

            cardIssuerDal.Setup(cid => cid.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>()))
                         .Returns(cardTransaction2.Object);

            cardDal.Setup(cd => cd.UpsertCard(It.IsAny<IAssociatedCard>())).Returns("1234");

            var promoCode = new Mock<Starbucks.Card.Provider.Common.Models.IProgramPromoCode>()
                .SetupProperty(p => p.PromoCode, "27341")
                .SetupProperty(ct => ct.MarketingRegistrationSource, "starbucks card");

            cardDal.Setup(n => n.GetPromoCodeByCardNumber(It.IsAny<string>())).Returns(promoCode.Object);
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();
            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);
            var cardHeader = new Starbucks.Card.Provider.Models.StarbucksCardHeader
            {
                CardNumber = "0123456789101112",
                CardPin = "45922522",
                Register = false,
                RegistrationSource = new CardRegistrationSource
                {
                    Marketing = "test marketing",
                    Platform = "test platform"
                }
            };
            provider.AddCardAssociation("12334", "123", cardHeader, "US");

            // Since the method we are trying to test returns void we need to make sure that the method is invoked.
            cardDal.Verify(c => c.AddCardAssociation("12334", "123", cardHeader, false), Times.Never());
            nsb.Verify(c => c.Send(It.IsAny<AddCard>()), Times.Once());

        }

        #region ActivateAndRegisterStarbucksCards

        [TestMethod]
        public void ActivateAndRegisterStarbucksCardActivePromotion()
        {
            var cardDal = new Mock<ICardDal>();
            cardDal.Setup(cd => cd.GetPromoCodeByCardNumber(It.IsAny<string>()))
                   .Returns(new Mock<IProgramPromoCode>().SetupProperty(p => p.PromoCode, "27020").Object);
            cardDal.Setup(cd => cd.GetPromoCode(It.IsAny<string>())).Returns("27020");
            cardDal.Setup(cd => cd.IsPromoCodeActive(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            cardDal.Setup(cd => cd.IsCardNumberAlreadyRegistered(It.IsAny<string>())).Returns(true);

            var promoFeature =
                new Mock<IPromoFeature>().SetupProperty(p => p.Description, "Weee")
                                         .SetupProperty(p => p.Name, "Test")
                                         .SetupProperty(p => p.Value, "1");
            var promotionSettings =
                new Mock<IPromotionProperties>().SetupProperty(p => p.PromoCode, "27020")
                                                .SetupProperty(p => p.Submarket, "MOUS")
                                                .SetupProperty(p => p.PromoFeatures,
                                                               new List<IPromoFeature>() { promoFeature.Object });
            cardDal.Setup(cd => cd.GetPromoSettings(It.IsAny<string>())).Returns(promotionSettings.Object);

            var card = new Mock<IAssociatedCard>();
            card.SetupProperty(p => p.Number, "836074FF9AD61DAB99F7991A26BFF6A3")
                .SetupProperty(p => p.Pin, "86617AF19AD218AD")
                .SetupProperty(p => p.CardId, "1234");
            cardDal.Setup(cd => cd.GetCardByNumber(It.IsAny<string>())).Returns(card.Object);
            var cardIssuerDal = new Mock<ICardIssuerDal>();

            var merchantInfo = new Mock<IMerchantInfo>()
                .SetupProperty(mi => mi.MarketId, "US").SetupProperty(mi => mi.Mid, "123456789");
            cardIssuerDal.Setup(cid => cid.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var cardTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>()
                .SetupProperty(p => p.ResponseCode, "00")
                .SetupProperty(p => p.CardNumber, "7777064110727700")
                .SetupProperty(p => p.Pin, "26990217")
                .SetupProperty(ct => ct.CardClass, "125");
            cardIssuerDal.Setup(
                cid => cid.ActivateVirtual(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<decimal>()))
                         .Returns(cardTransaction.Object);

            var cardTransaction2 = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>()
                .SetupProperty(p => p.ResponseCode, "00")
                .SetupProperty(ct => ct.CardClass, "125")
                .SetupProperty(p => p.Pin, "26990217")
                .SetupProperty(p => p.CardNumber, "7777064110727700");

            cardIssuerDal.Setup(cid => cid.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>()))
                         .Returns(cardTransaction2.Object);

            cardDal.Setup(cd => cd.UpsertCard(It.IsAny<IAssociatedCard>())).Returns("1234");

            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();
            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object,
                                            messageBrokerDal.Object, settingProvider.Object, Settings,
                                            logRepository.Object, messageListener.Object, null, nsb.Object);

            var regSource = new Mock<ICardRegistrationSource>();
            regSource.SetupProperty(p => p.Marketing, "Web");

            var cardHeader = new Mock<Starbucks.Card.Provider.Common.Models.IStarbucksCardHeader>()
                .SetupProperty(p => p.CardNumber, "7777064110727700")
                .SetupProperty(p => p.CardPin, "26990217")
                .SetupProperty(p => p.RegistrationSource, regSource.Object)
                .SetupProperty(p => p.Nickname, "test card");


            var result = provider.ActivateAndRegisterCard("12334", "US", cardHeader.Object);

            Assert.IsNotNull(result);
            nsb.Verify(c => c.Send(It.IsAny<AddCard>()), Times.Once());

        }

        [TestMethod]
        public void ActivateAndRegisterStarbucksCardsNoPromoCode()
        {
            var card = new Mock<IStarbucksCard>();

            var cardDal = new Mock<ICardDal>();
            cardDal.Setup(cd => cd.GetPromoCode(It.IsAny<string>()));

            var cardIssuerDal = new Mock<ICardIssuerDal>();

            var merchantInfo = new Mock<IMerchantInfo>()
                .SetupProperty(mi => mi.MarketId, "US").SetupProperty(mi => mi.Mid, "123456789");
            cardIssuerDal.Setup(cid => cid.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var cardTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>()
                .SetupProperty(p => p.ResponseCode, "00")
                .SetupProperty(p => p.CardNumber, "7777064110727700")
                .SetupProperty(p => p.Pin, "12345678")
                .SetupProperty(ct => ct.CardClass, "125");
            cardIssuerDal.Setup(cid => cid.ActivateVirtual(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<decimal>()))
                         .Returns(cardTransaction.Object);

            var cardTransaction2 = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>()
                .SetupProperty(p => p.ResponseCode, "00")
                .SetupProperty(ct => ct.CardClass, "125")
                .SetupProperty(p => p.Pin, "12345678")
                .SetupProperty(p => p.CardNumber, "7777064110727700");

            cardIssuerDal.Setup(cid => cid.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>()))
                         .Returns(cardTransaction2.Object);

            cardDal.Setup(cd => cd.UpsertCard(It.IsAny<IAssociatedCard>()));

            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);
            var cardHeader = new Mock<Starbucks.Card.Provider.Common.Models.IStarbucksCardHeader>()
                .SetupProperty(p => p.Nickname, "test card");
            try
            {

                var result = provider.ActivateAndRegisterCard("12334", "US", cardHeader.Object);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
        }

        [TestMethod]
        public void ActivateAndRegisterStarbucksCardsWithPromoCode()
        {

            var cardDal = new Mock<ICardDal>();
            cardDal.Setup(cd => cd.GetPromoCodeByCardNumber(It.IsAny<string>()))
                  .Returns(new Mock<IProgramPromoCode>().SetupProperty(p => p.PromoCode, "27020").Object);
            cardDal.Setup(cd => cd.GetPromoCode(It.IsAny<string>())).Returns("27020");
            var card = new Mock<IAssociatedCard>();
            card.SetupProperty(p => p.Number, "836074FF9AD61DAB99F7991A26BFF6A3")
                .SetupProperty(p => p.Pin, "86617AF19AD218AD")
                .SetupProperty(p => p.CardId, "1234");
            cardDal.Setup(cd => cd.GetCardByNumber(It.IsAny<string>())).Returns(card.Object);
            var cardIssuerDal = new Mock<ICardIssuerDal>();

            var merchantInfo = new Mock<IMerchantInfo>()
                .SetupProperty(mi => mi.MarketId, "US").SetupProperty(mi => mi.Mid, "123456789");
            cardIssuerDal.Setup(cid => cid.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var cardTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>()
                .SetupProperty(p => p.ResponseCode, "00")
                .SetupProperty(p => p.CardNumber, "7777064110727700")
                .SetupProperty(p => p.Pin, "26990217")
                .SetupProperty(ct => ct.CardClass, "125");
            cardIssuerDal.Setup(cid => cid.ActivateVirtual(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<decimal>()))
                         .Returns(cardTransaction.Object);

            var cardTransaction2 = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>()
                .SetupProperty(p => p.ResponseCode, "00")
                .SetupProperty(ct => ct.CardClass, "125")
                .SetupProperty(p => p.Pin, "26990217")
                .SetupProperty(p => p.CardNumber, "7777064110727700");

            cardIssuerDal.Setup(cid => cid.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>()))
                         .Returns(cardTransaction2.Object);

            cardDal.Setup(cd => cd.UpsertCard(It.IsAny<IAssociatedCard>())).Returns("1234");

            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);

            var regSource = new Mock<ICardRegistrationSource>();
            regSource.SetupProperty(p => p.Marketing, "Web");

            var cardHeader = new Mock<Starbucks.Card.Provider.Common.Models.IStarbucksCardHeader>()
                .SetupProperty(p => p.CardNumber, "7777064110727700")
                .SetupProperty(p => p.CardPin, "26990217")
                .SetupProperty(p => p.RegistrationSource, regSource.Object)
                .SetupProperty(p => p.Nickname, "test card");

            var result = provider.ActivateAndRegisterCard("12334", "US", cardHeader.Object);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ActivateAndRegisterStarbucksCardWithValueLinkException()
        {

            var cardDal = new Mock<ICardDal>();
            cardDal.Setup(cd => cd.GetPromoCodeByCardNumber(It.IsAny<string>()))
                  .Returns(new Mock<IProgramPromoCode>().SetupProperty(p => p.PromoCode, "27020").Object);
            cardDal.Setup(cd => cd.GetPromoCode(It.IsAny<string>())).Returns("27020");
            var card = new Mock<IAssociatedCard>();
            card.SetupProperty(p => p.Number, "836074FF9AD61DAB99F7991A26BFF6A3")
                .SetupProperty(p => p.Pin, "86617AF19AD218AD")
                .SetupProperty(p => p.CardId, "1234");
            cardDal.Setup(cd => cd.GetCardByNumber(It.IsAny<string>())).Returns(card.Object);
            var cardIssuerDal = new Mock<ICardIssuerDal>();

            var merchantInfo = new Mock<IMerchantInfo>()
                .SetupProperty(mi => mi.MarketId, "US").SetupProperty(mi => mi.Mid, "123456789");
            cardIssuerDal.Setup(cid => cid.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var cardTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>()
                .SetupProperty(p => p.ResponseCode, "00")
                .SetupProperty(p => p.CardNumber, "7777064110727700")
                .SetupProperty(p => p.Pin, "26990217")
                .SetupProperty(ct => ct.CardClass, "125");
            cardIssuerDal.Setup(cid => cid.ActivateVirtual(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<decimal>()))
                         .Throws(new CardIssuerException(CardIssuerErrorResource.ValueLinkHostDownCode, CardIssuerErrorResource.ValueLinkHostDownMessage));

            var cardTransaction2 = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>()
                .SetupProperty(p => p.ResponseCode, "00")
                .SetupProperty(ct => ct.CardClass, "125")
                .SetupProperty(p => p.Pin, "26990217")
                .SetupProperty(p => p.CardNumber, "7777064110727700");

            cardIssuerDal.Setup(cid => cid.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>()))
                         .Returns(cardTransaction2.Object);

            cardDal.Setup(cd => cd.UpsertCard(It.IsAny<IAssociatedCard>())).Returns("1234");

            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);

            var regSource = new Mock<ICardRegistrationSource>();
            regSource.SetupProperty(p => p.Marketing, "Web");

            var cardHeader = new Mock<Starbucks.Card.Provider.Common.Models.IStarbucksCardHeader>()
                .SetupProperty(p => p.CardNumber, "7777064110727700")
                .SetupProperty(p => p.CardPin, "26990217")
                .SetupProperty(p => p.RegistrationSource, regSource.Object)
                .SetupProperty(p => p.Nickname, "test card");

            try
            {
                var result = provider.ActivateAndRegisterCard("12334", "US", cardHeader.Object);
                Assert.Fail("Failed to throw ValueLink Exception");
            }
            catch (CardIssuerException ex)
            {
                Assert.AreEqual(ex.Code, CardIssuerErrorResource.ValueLinkHostDownCode);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                Assert.Fail("Threw wrong ValueLink exception");
            }
        }

        #endregion
        #region RegisterStarbucksCards
        [TestMethod]
        public void RegisterStarbucksCard()
        {
            //Mock<ICardProvider> cardProvider;
            //Mock<IAccountProvider> accountProvider;
            //BuildUpMockController(out controller, out cardProvider, out accountProvider);
            var cardDal = new Mock<ICardDal>();
            cardDal.Setup(cd => cd.GetPromoCode(It.IsAny<string>())).Returns("1234");

            var card = new Mock<IAssociatedCard>();
            card.SetupProperty(p => p.Number, "0123456789101112")
                .SetupProperty(p => p.Pin, "01235678")
                .SetupProperty(p => p.CardId, "1234");
            cardDal.Setup(cd => cd.GetCardByNumber(It.IsAny<string>())).Returns(card.Object);


            var cardIssuerDal = new Mock<ICardIssuerDal>();

            var merchantInfo = new Mock<IMerchantInfo>()
                .SetupProperty(mi => mi.MarketId, "US").SetupProperty(mi => mi.Mid, "123456789");
            cardIssuerDal.Setup(cid => cid.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var cardTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>()
                .SetupProperty(p => p.ResponseCode, "00")
                .SetupProperty(p => p.CardNumber, "7777064110727700")
                .SetupProperty(ct => ct.CardClass, "125");
            cardIssuerDal.Setup(cid => cid.ActivateVirtual(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<decimal>()))
                         .Returns(cardTransaction.Object);

            var cardTransaction2 = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>()
                .SetupProperty(p => p.ResponseCode, "00")
                .SetupProperty(ct => ct.CardClass, "125")
                .SetupProperty(p => p.CardNumber, "7777064110727700");

            cardIssuerDal.Setup(cid => cid.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>()))
                         .Returns(cardTransaction2.Object);

            cardDal.Setup(cd => cd.UpsertCard(It.IsAny<IAssociatedCard>())).Returns("1234");

            var promoCode = new Mock<Starbucks.Card.Provider.Common.Models.IProgramPromoCode>()
                .SetupProperty(p => p.PromoCode, "27341")
                .SetupProperty(ct => ct.MarketingRegistrationSource, "starbucks card");

            cardDal.Setup(n => n.GetPromoCodeByCardNumber(It.IsAny<string>())).Returns(promoCode.Object);
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);
            var cardHeader = new Starbucks.Card.Provider.Models.StarbucksCardHeader
            {
                CardNumber = "0123456789101112",
                CardPin = "45922522"//"01235678"
            };

            var result = provider.RegisterStarbucksCard("12334", cardHeader, "US");

            Assert.IsNotNull(result);
        }
        #endregion

        #region RegisterMultipleStarbucksCards
        [TestMethod]
        public void RegisterMultipleStarbucksCards()
        {
            var starbucksCardAndPins = new Mock<List<IStarbucksCardHeader>>();

            var starbucksCardAndPin = new Mock<IStarbucksCardHeader>();
            starbucksCardAndPin.SetupProperty(p => p.CardNumber, "0123456789101112");
            starbucksCardAndPin.SetupProperty(p => p.CardPin, "01235678");

            starbucksCardAndPins.Object.Add(starbucksCardAndPin.Object);



            var cardDal = new Mock<ICardDal>();
            cardDal.Setup(cd => cd.GetPromoCode(It.IsAny<string>())).Returns("1234");

            var card = new Mock<IAssociatedCard>();
            card.SetupProperty(p => p.Number, "0123456789101112")
                .SetupProperty(p => p.Pin, "01235678")
                .SetupProperty(p => p.CardId, "1234");
            cardDal.Setup(cd => cd.GetCardByNumber(It.IsAny<string>())).Returns(card.Object);


            var cardIssuerDal = new Mock<ICardIssuerDal>();

            var merchantInfo = new Mock<IMerchantInfo>()
                .SetupProperty(mi => mi.MarketId, "US").SetupProperty(mi => mi.Mid, "123456789");
            cardIssuerDal.Setup(cid => cid.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var cardTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>()
                .SetupProperty(p => p.ResponseCode, "00")
                .SetupProperty(p => p.CardNumber, "7777064110727700")
                .SetupProperty(ct => ct.CardClass, "125");
            cardIssuerDal.Setup(cid => cid.ActivateVirtual(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<decimal>()))
                         .Returns(cardTransaction.Object);

            var cardTransaction2 = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>()
                .SetupProperty(p => p.ResponseCode, "00")
                .SetupProperty(ct => ct.CardClass, "125")
                .SetupProperty(p => p.CardNumber, "7777064110727700");

            cardIssuerDal.Setup(cid => cid.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>()))
                         .Returns(cardTransaction2.Object);

            cardDal.Setup(cd => cd.UpsertCard(It.IsAny<IAssociatedCard>())).Returns("1234");

            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);


            var result = provider.RegisterMultipleCards("12334", starbucksCardAndPins.Object, "US");

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void RegisterMultipleStarbucksCardsWithException()
        {
            var starbucksCardAndPins = new Mock<List<IStarbucksCardHeader>>();

            var starbucksCardAndPin = new Mock<IStarbucksCardHeader>();
            starbucksCardAndPin.SetupProperty(p => p.CardNumber, "0123456789101112");
            starbucksCardAndPin.SetupProperty(p => p.CardPin, "01235678");

            starbucksCardAndPins.Object.Add(starbucksCardAndPin.Object);



            var cardDal = new Mock<ICardDal>();
            cardDal.Setup(cd => cd.GetPromoCode(It.IsAny<string>())).Returns("1234");

            var card = new Mock<IAssociatedCard>();
            card.SetupProperty(p => p.Number, "0123456789101112")
                .SetupProperty(p => p.Pin, "01235678")
                .SetupProperty(p => p.CardId, "1234");
            cardDal.Setup(cd => cd.GetCardByNumber(It.IsAny<string>())).Returns(card.Object);


            var cardIssuerDal = new Mock<ICardIssuerDal>();

            var merchantInfo = new Mock<IMerchantInfo>()
                .SetupProperty(mi => mi.MarketId, "US").SetupProperty(mi => mi.Mid, "123456789");
            cardIssuerDal.Setup(cid => cid.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Throws<Exception>();

            var cardTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>()
                .SetupProperty(p => p.ResponseCode, "00")
                .SetupProperty(p => p.CardNumber, "7777064110727700")
                .SetupProperty(ct => ct.CardClass, "125");
            cardIssuerDal.Setup(cid => cid.ActivateVirtual(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<decimal>()))
                         .Returns(cardTransaction.Object);

            var cardTransaction2 = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>()
                .SetupProperty(p => p.ResponseCode, "00")
                .SetupProperty(ct => ct.CardClass, "125")
                .SetupProperty(p => p.CardNumber, "7777064110727700");

            cardIssuerDal.Setup(cid => cid.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>()))
                         .Returns(cardTransaction2.Object);

            cardDal.Setup(cd => cd.UpsertCard(It.IsAny<IAssociatedCard>())).Returns("1234");

            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = new Mock<ISettingsProvider>();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var nsb = new Mock<IBus>();

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, nsb.Object);


            var result = provider.RegisterMultipleCards("12334", starbucksCardAndPins.Object, "US");

            Assert.IsNotNull(result);
            Assert.IsFalse(result.First().Successful);
        }
        #endregion

    }
}
