﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.Card.Provider;
using Starbucks.Card.Provider.Common;
using Starbucks.Card.Provider.Common.ErrorResources;
using Starbucks.Card.Provider.Common.Exceptions;
using Starbucks.Card.Provider.Configuration;
using Starbucks.Card.Provider.Models;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.LogCounter.Provider.Common;
using Starbucks.MessageBroker.Common;
using Starbucks.OpenApi.Logging.Common;
using Starbucks.Settings.Provider.Common;
using ICardTransaction = Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction;
using Starbucks.FraudData.QueueService.Common.Commands;
using SdkModels = Starbucks.Api.Sdk.PaymentService.Models;

namespace Card.Provider.UnitTest
{
    public class CardProviderTests
    {
        private static CardProviderSettingsSection _settings;

        public static CardProviderSettingsSection Settings
        {
            get
            {
                return _settings ??
                       (_settings =
                           ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection);
            }
        }

        private const string CardReloadExceptionMessage = "Exception calling _cardIssuerDal.Reload";

        [TestMethod]
        public void WhenConstructorInvalid()
        {
            var cardProvider = new CardProvider(null, null, null, null, null, null, null, null, null, null);
        }

        [TestMethod]
        public void SendMessage()
        {
            AddCard addCard = new AddCard
            {
                Balance = 100,
                CardId = "123",
                CardMarket = "US",
                CardNickname = "Nickname",
                CardNumber = "77778888",
                CardType = "Test Card Type",
                IsDigital = false,
                IsOwner = false,
                IsPartner = false,
                IsPrimary = false,
                UserId = "123",
                Market = "US",
                Platform = "Mobile"
            };

            try
            {
                //BusProvider.SendMessage(addCard, SdkModels.TransactionTypes.AddCard, null);
                Assert.Fail("need work on this test");
            }
            catch (Exception ex)
            {
                Console.WriteLine("ex = " + ex.ToString());
            }
        }
        #region Transfer

        [TestMethod]
        public void TransferRegisteredAmount()
        {
            CardProviderSettingsSection cardProviderSettingsSection = ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection;

            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var bus = new Mock<NServiceBus.ISendOnlyBus>();
            var cardFrom = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "2",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A3",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("2")).Returns(cardFrom);

            var cardTo = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "3",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A5",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("3")).Returns(cardTo);


            cardDal.Setup(cd => cd.UpsertCardTransactionHistory(It.IsAny<string>(), It.IsAny<Starbucks.Card.Provider.Common.Models.ICardTransaction>()));
            cardDal.Setup(
                cd =>
                cd.UpsertCardBalance(It.IsAny<string>(), It.IsAny<decimal>(), It.IsAny<DateTime>(), It.IsAny<string>()));

            var cardFromBalance =
                new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>().SetupProperty(p => p.EndingBalance, 120M)
                                        .SetupProperty(p => p.TransactionDate, DateTime.Now);
            cardIssuerDal.Setup(cid => cid.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>()))
                         .Returns(cardFromBalance.Object);

            cardIssuerDal.Setup(
                cid =>
                cid.UpsertPartialTransferTransactionDetails(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<DateTime>(),
                                                            It.IsAny<string>(), It.IsAny<DateTime?>())).Returns(1);



            var merchantInfo = new Mock<IMerchantInfo>().SetupProperty(p => p.Mid, "1231212");
            cardIssuerDal.Setup(cid => cid.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var reloadTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>().SetupProperty(p => p.ResponseCode, "00");
            reloadTransaction.SetupGet(p => p.TransactionSucceeded).Returns(true);
            cardIssuerDal.Setup(
                p => p.Reload(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<decimal>()))
                         .Returns(reloadTransaction.Object);

            const decimal transferAmount = 100M;
            var redeemTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>().SetupProperty(p => p.ResponseCode, "00");
            redeemTransaction.SetupGet(p => p.TransactionSucceeded).Returns(true);
            redeemTransaction.SetupProperty(p => p.Amount, transferAmount);

            cardIssuerDal.Setup(
                p => p.Redeem(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<decimal>()))
                         .Returns(redeemTransaction.Object);

            Mock<ICardTransaction> cardTransaction = new Mock<ICardTransaction>();
            cardTransaction.SetupProperty(p => p.RequestCode, "0400");
            cardTransaction.SetupProperty(p => p.UtcDate, DateTime.UtcNow.AddDays(1));
            cardIssuerDal.Setup(
                p => p.GetHistoryCondensed(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new List<ICardTransaction> { cardTransaction.Object });


            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object,
                settingProvider.Object, Settings, logRepository.Object, messageListener.Object, null, bus.Object);
            var result = provider.TransferRegistered("1", "2", "3", transferAmount, "US");

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TransferRegisteredAmount_CardFromNull()
        {
            CardProviderSettingsSection cardProviderSettingsSection = ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection;

            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var bus = new Mock<NServiceBus.ISendOnlyBus>();

            AssociatedCard cardFrom = null;
            cardDal.Setup(cd => cd.GetCard("2")).Returns(cardFrom);

            var cardTo = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "3",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A5",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };
            cardDal.Setup(cd => cd.GetCard("3")).Returns(cardTo);

            const decimal transferAmount = 100M;
            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings,
                logRepository.Object, messageListener.Object, null, bus.Object);

            try
            {
                var result = provider.TransferRegistered("1", "2", "3", transferAmount, "US");

                Assert.Fail("Expected code not returned.");
            }
            catch (CardProviderException ex)
            {
                Assert.AreEqual(CardProviderErrorResource.CardNotFoundCode, ex.Code);
            }
        }

        [TestMethod]
        public void TransferRegisteredAmount_CardToNull()
        {
            CardProviderSettingsSection cardProviderSettingsSection = ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection;

            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var bus = new Mock<NServiceBus.ISendOnlyBus>();
            var cardFrom = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "2",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("2")).Returns(cardFrom);

            AssociatedCard cardTo = null;

            cardDal.Setup(cd => cd.GetCard("3")).Returns(cardTo);

            const decimal transferAmount = 100M;
            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings,
                logRepository.Object, messageListener.Object, null, bus.Object);

            try
            {
                var result = provider.TransferRegistered("1", "2", "3", transferAmount, "US");

                Assert.Fail("Expected code not returned.");
            }
            catch (CardProviderException ex)
            {
                Assert.AreEqual(CardProviderErrorResource.CardNotFoundCode, ex.Code);
            }
        }

        [TestMethod]
        public void TransferRegisteredAmount_DAL_CardFromNumberNullError()
        {
            CardProviderSettingsSection cardProviderSettingsSection = ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection;

            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var bus = new Mock<NServiceBus.ISendOnlyBus>();

            var cardFrom = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "2",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };
            cardDal.Setup(cd => cd.GetCard("2")).Returns(cardFrom);

            var cardTo = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "3",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A5",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };
            cardDal.Setup(cd => cd.GetCard("3")).Returns(cardTo);

            const decimal transferAmount = 100M;
            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings,
                logRepository.Object, messageListener.Object, null, bus.Object);

            try
            {
                var result = provider.TransferRegistered("1", "2", "3", transferAmount, "US");

                Assert.Fail("Expected code not returned.");
            }
            catch (CardProviderException ex)
            {
                Assert.AreEqual(CardProviderErrorResource.InvalidCardNumberCode, ex.Code);
            }
        }

        [TestMethod]
        public void TransferRegisteredAmount_DAL_CardFromToNullError()
        {
            CardProviderSettingsSection cardProviderSettingsSection = ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection;

            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var bus = new Mock<NServiceBus.ISendOnlyBus>();

            var cardFrom = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "2",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A3",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };
            cardDal.Setup(cd => cd.GetCard("2")).Returns(cardFrom);

            var cardTo = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "3",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };
            cardDal.Setup(cd => cd.GetCard("3")).Returns(cardTo);

            const decimal transferAmount = 100M;
            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings,
                logRepository.Object, messageListener.Object, null, bus.Object);

            try
            {
                var result = provider.TransferRegistered("1", "2", "3", transferAmount, "US");

                Assert.Fail("Expected code not returned.");
            }
            catch (CardProviderException ex)
            {
                Assert.AreEqual(CardProviderErrorResource.InvalidCardNumberCode, ex.Code);
            }
        }

        [TestMethod]
        public void TransferRegisteredAmount_CardFromAndToSameError()
        {
            CardProviderSettingsSection cardProviderSettingsSection = ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection;

            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var bus = new Mock<NServiceBus.ISendOnlyBus>();

            var cardFrom = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "2",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A5",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };
            cardDal.Setup(cd => cd.GetCard("2")).Returns(cardFrom);

            var cardTo = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "3",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A5",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };
            cardDal.Setup(cd => cd.GetCard("3")).Returns(cardTo);

            const decimal transferAmount = 100M;
            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings,
                logRepository.Object, messageListener.Object, null, bus.Object);

            try
            {
                var result = provider.TransferRegistered("1", "2", "3", transferAmount, "US");

                Assert.Fail("Expected code not returned.");
            }
            catch (CardProviderException ex)
            {
                Assert.AreEqual(CardProviderErrorResource.InvalidCardNumberCode, ex.Code);
            }
        }

        [TestMethod]
        public void TransferRegisteredAmount_CardAmountZeroError()
        {
            CardProviderSettingsSection cardProviderSettingsSection = ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection;

            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var bus = new Mock<NServiceBus.ISendOnlyBus>();

            var cardFrom = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "2",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A3",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };
            cardDal.Setup(cd => cd.GetCard("2")).Returns(cardFrom);

            var cardTo = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "3",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A5",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };
            cardDal.Setup(cd => cd.GetCard("3")).Returns(cardTo);

            const decimal transferAmount = 0M;
            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings,
                logRepository.Object, messageListener.Object, null, bus.Object);

            try
            {
                var result = provider.TransferRegistered("1", "2", "3", transferAmount, "US");

                Assert.Fail("Expected code not returned.");
            }
            catch (CardProviderValidationException ex)
            {
                Assert.AreEqual(ex.Code, CardProviderValidationErrorResource.CannotTransferABalanceAmountOfZeroCode);
            }
        }

        [TestMethod]
        public void TransferRegisteredAmount_CannotTransferFromCardClassError()
        {
            CardProviderSettingsSection cardProviderSettingsSection = ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection;

            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var bus = new Mock<NServiceBus.ISendOnlyBus>();
            var cardFrom = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "2",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "254",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A3",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("2")).Returns(cardFrom);

            var cardTo = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "3",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A5",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("3")).Returns(cardTo);

            const decimal transferAmount = 100M;
            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings,
                logRepository.Object, messageListener.Object, null, bus.Object);

            try
            {
                var result = provider.TransferRegistered("1", "2", "3", transferAmount, "US");

                Assert.Fail("Expected code not returned.");
            }
            catch (CardProviderException ex)
            {
                Assert.AreEqual(ex.Code, CardProviderErrorResource.CannotTransferFromCardClassCode);
            }
        }

        [TestMethod]
        public void TransferRegisteredAmount_CannotTransferToCardClassError()
        {
            CardProviderSettingsSection cardProviderSettingsSection = ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection;

            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var bus = new Mock<NServiceBus.ISendOnlyBus>();
            var cardFrom = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "2",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A3",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("2")).Returns(cardFrom);

            var cardTo = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "3",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "254",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A5",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("3")).Returns(cardTo);

            const decimal transferAmount = 100M;
            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings, logRepository.Object,
                messageListener.Object, null, bus.Object);

            try
            {
                var result = provider.TransferRegistered("1", "2", "3", transferAmount, "US");

                Assert.Fail("Expected code not returned.");
            }
            catch (CardProviderException ex)
            {
                Assert.AreEqual(ex.Code, CardProviderErrorResource.CannotTransferToCardClassCode);
            }
        }

        [TestMethod]
        public void TransferRegisteredAmount_InsufficientFundsForTransactionError()
        {
            CardProviderSettingsSection cardProviderSettingsSection = ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection;

            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var bus = new Mock<NServiceBus.ISendOnlyBus>();
            var cardFrom = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "2",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A3",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("2")).Returns(cardFrom);

            var cardTo = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "3",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A5",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("3")).Returns(cardTo);

            const decimal transferAmount = 100M;
            var cardFromBalance =
                new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>().SetupProperty(p => p.EndingBalance, 80M)
                                        .SetupProperty(p => p.TransactionDate, DateTime.Now);
            cardIssuerDal.Setup(cid => cid.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>()))
                         .Returns(cardFromBalance.Object);

            var merchantInfo = new Mock<IMerchantInfo>().SetupProperty(p => p.Mid, "1231212");
            cardIssuerDal.Setup(cid => cid.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            Mock<ICardTransaction> cardTransaction = new Mock<ICardTransaction>();
            cardTransaction.SetupProperty(p => p.RequestCode, "0400");
            cardTransaction.SetupProperty(p => p.UtcDate, DateTime.UtcNow.AddDays(1));
            cardIssuerDal.Setup(
                p => p.GetHistoryCondensed(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new List<ICardTransaction> { cardTransaction.Object });

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings
                , logRepository.Object, messageListener.Object, null, bus.Object);

            try
            {
                var result = provider.TransferRegistered("1", "2", "3", transferAmount, "US");

                Assert.Fail("Expected code not returned.");
            }
            catch (CardProviderException ex)
            {
                Assert.AreEqual(ex.Code, CardProviderErrorResource.InsufficientFundsForTransactionCode);
            }
        }

        [TestMethod]
        public void TransferRegisteredAmount_PossiblePosFraudError()
        {
            CardProviderSettingsSection cardProviderSettingsSection = ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection;

            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = new Mock<ICardIssuerDal>();
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var bus = new Mock<NServiceBus.ISendOnlyBus>();
            var cardFrom = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "2",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A3",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("2")).Returns(cardFrom);

            var cardTo = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "3",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A5",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("3")).Returns(cardTo);

            cardDal.Setup(cd => cd.UpsertCardTransactionHistory(It.IsAny<string>(), It.IsAny<Starbucks.Card.Provider.Common.Models.ICardTransaction>()));
            cardDal.Setup(
                cd =>
                cd.UpsertCardBalance(It.IsAny<string>(), It.IsAny<decimal>(), It.IsAny<DateTime>(), It.IsAny<string>()));

            var cardFromBalance =
                new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>().SetupProperty(p => p.EndingBalance, 120M)
                                        .SetupProperty(p => p.TransactionDate, DateTime.Now);
            cardIssuerDal.Setup(cid => cid.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>()))
                         .Returns(cardFromBalance.Object);

            cardIssuerDal.Setup(
                cid =>
                cid.UpsertPartialTransferTransactionDetails(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<DateTime>(),
                                                            It.IsAny<string>(), It.IsAny<DateTime?>())).Returns(1);

            var merchantInfo = new Mock<IMerchantInfo>().SetupProperty(p => p.Mid, "1231212");
            cardIssuerDal.Setup(cid => cid.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            var reloadTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>().SetupProperty(p => p.ResponseCode, "00");
            reloadTransaction.SetupGet(p => p.TransactionSucceeded).Returns(true);
            cardIssuerDal.Setup(
                p => p.Reload(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<decimal>()))
                         .Returns(reloadTransaction.Object);

            const decimal transferAmount = 100M;
            var redeemTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>().SetupProperty(p => p.ResponseCode, "00");
            redeemTransaction.SetupGet(p => p.TransactionSucceeded).Returns(true);
            redeemTransaction.SetupProperty(p => p.Amount, transferAmount);

            cardIssuerDal.Setup(
                p => p.Redeem(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<decimal>()))
                         .Returns(redeemTransaction.Object);

            var cardTransaction = new Mock<ICardTransaction>();
            cardTransaction.SetupProperty(p => p.RequestCode, "0300");
            cardTransaction.SetupProperty(p => p.UtcDate, DateTime.UtcNow.AddDays(1));
            cardIssuerDal.Setup(
                p => p.GetHistoryCondensed(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new List<ICardTransaction> { cardTransaction.Object });

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings,
                logRepository.Object, messageListener.Object, null, bus.Object);

            try
            {
                var result = provider.TransferRegistered("1", "2", "3", transferAmount, "US");

                Assert.Fail("Expected code not returned.");
            }
            catch (CardProviderException ex)
            {
                Assert.AreEqual(ex.Code, CardProviderErrorResource.CannotTransferAfterReloadCode);
            }
        }

        [TestMethod]
        public void TransferRegisteredAmount_RedeemResultAmountError()
        {
            const decimal transferAmount = 100M;
            var cardProviderSettingsSection = ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection;

            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = CreateCardIssuerDalMock(transferAmount + 1);
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var bus = new Mock<NServiceBus.ISendOnlyBus>();
            var cardFrom = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "2",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A3",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("2")).Returns(cardFrom);

            var cardTo = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "3",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A5",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("3")).Returns(cardTo);

            cardDal.Setup(cd => cd.UpsertCardTransactionHistory(It.IsAny<string>(), It.IsAny<Starbucks.Card.Provider.Common.Models.ICardTransaction>()));
            cardDal.Setup(
                cd =>
                cd.UpsertCardBalance(It.IsAny<string>(), It.IsAny<decimal>(), It.IsAny<DateTime>(), It.IsAny<string>()));

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings,
                logRepository.Object, messageListener.Object, null, bus.Object);

            try
            {
                var result = provider.TransferRegistered("1", "2", "3", transferAmount, "US");

                Assert.Fail("Expected code not returned.");
            }
            catch (CardProviderException ex)
            {
                Assert.AreEqual(ex.Code, CardProviderErrorResource.InvalidCardTransferCode);
            }
        }

        [TestMethod]
        public void TransferRegisteredAmount_ReloadResultTransactionFailedError()
        {
            const decimal transferAmount = 100M;

            var cardProviderSettingsSection = ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection;

            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = CreateCardIssuerDalMock(transferAmount, transactionSuccess: false);
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var bus = new Mock<NServiceBus.ISendOnlyBus>();
            var cardFrom = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "2",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A3",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("2")).Returns(cardFrom);

            var cardTo = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "3",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A5",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("3")).Returns(cardTo);

            cardDal.Setup(cd => cd.UpsertCardTransactionHistory(It.IsAny<string>(), It.IsAny<Starbucks.Card.Provider.Common.Models.ICardTransaction>()));
            cardDal.Setup(
                cd =>
                cd.UpsertCardBalance(It.IsAny<string>(), It.IsAny<decimal>(), It.IsAny<DateTime>(), It.IsAny<string>()));

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings
                , logRepository.Object, messageListener.Object, null, bus.Object);

            try
            {
                var result = provider.TransferRegistered("1", "2", "3", transferAmount, "US");
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
            }
        }

        [TestMethod]
        public void TransferRegisteredAmount_ReloadException()
        {
            const decimal transferAmount = 100M;

            var cardProviderSettingsSection = ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection;

            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = CreateCardIssuerDalMock(transferAmount, transactionSuccess: false, throwReloadException: true);
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var bus = new Mock<NServiceBus.ISendOnlyBus>();
            var cardFrom = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "2",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A3",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("2")).Returns(cardFrom);

            var cardTo = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "3",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A5",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("3")).Returns(cardTo);

            cardDal.Setup(cd => cd.UpsertCardTransactionHistory(It.IsAny<string>(), It.IsAny<Starbucks.Card.Provider.Common.Models.ICardTransaction>()));
            cardDal.Setup(
                cd =>
                cd.UpsertCardBalance(It.IsAny<string>(), It.IsAny<decimal>(), It.IsAny<DateTime>(), It.IsAny<string>()));

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings
                , logRepository.Object, messageListener.Object, null, bus.Object);

            try
            {
                var result = provider.TransferRegistered("1", "2", "3", transferAmount, "US");
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
                Assert.IsTrue(CardReloadExceptionMessage.Equals(ex.Message));
            }
        }

        [TestMethod]
        public void TransferRegisteredAmount_ReloadException_Then_Void_Redeem_Exception()
        {
            const decimal transferAmount = 100M;

            var cardProviderSettingsSection = ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection;

            var cardDal = new Mock<ICardDal>();
            var cardIssuerDal = CreateCardIssuerDalMock(transferAmount, transactionSuccess: false, throwReloadException: true,
                throwVoidRedeemException: true);
            var messageBrokerDal = new Mock<IMessageBroker>();
            var logCounterManager = new Mock<ILogCounterManager>();
            var settingProvider = CreateSettingProvider();
            var logRepository = new Mock<ILogRepository>();
            var messageListener = new Mock<IMessageListener>();
            var bus = new Mock<NServiceBus.ISendOnlyBus>();
            var cardFrom = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "2",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A3",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("2")).Returns(cardFrom);

            var cardTo = new AssociatedCard(cardProviderSettingsSection)
            {
                RegisteredUserId = "1",
                CardId = "3",
                Balance = 100M,
                BalanceDate = DateTime.Now,
                Class = "25",
                Currency = "USD",
                Number = "836074FF9AD61DAB99F7991A26BFF6A5",
                Pin = "86617AF19AD218AD",
                Actions = new List<string>()
            };

            cardDal.Setup(cd => cd.GetCard("3")).Returns(cardTo);

            cardDal.Setup(cd => cd.UpsertCardTransactionHistory(It.IsAny<string>(), It.IsAny<Starbucks.Card.Provider.Common.Models.ICardTransaction>()));
            cardDal.Setup(
                cd =>
                cd.UpsertCardBalance(It.IsAny<string>(), It.IsAny<decimal>(), It.IsAny<DateTime>(), It.IsAny<string>()));

            var provider = new CardProvider(cardDal.Object, cardIssuerDal.Object, logCounterManager.Object, messageBrokerDal.Object, settingProvider.Object, Settings
                , logRepository.Object, messageListener.Object, null, bus.Object);

            try
            {
                var result = provider.TransferRegistered("1", "2", "3", transferAmount, "US");
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
                Assert.IsTrue(CardReloadExceptionMessage.Equals(ex.Message));
            }
        }

        #endregion

        private static Mock<ICardIssuerDal> CreateCardIssuerDalMock(decimal transferAmount, bool transactionSuccess = true,
            bool throwReloadException = false, bool throwVoidRedeemException = false)
        {
            var cardIssuerDal = new Mock<ICardIssuerDal>();

            var cardFromBalance =
                new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>().SetupProperty(p => p.EndingBalance, 120M)
                                        .SetupProperty(p => p.TransactionDate, DateTime.Now);

            cardIssuerDal.Setup(cid => cid.GetBalance(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>()))
                         .Returns(cardFromBalance.Object);

            cardIssuerDal.Setup(
                cid =>
                cid.UpsertPartialTransferTransactionDetails(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<DateTime>(),
                                                            It.IsAny<string>(), It.IsAny<DateTime?>())).Returns(1);

            var merchantInfo = new Mock<IMerchantInfo>().SetupProperty(p => p.Mid, "1231212");
            cardIssuerDal.Setup(cid => cid.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);

            if (throwReloadException)
            {
                cardIssuerDal.Setup(
                    p => p.Reload(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<decimal>()))
                             .Throws(new Exception(CardReloadExceptionMessage));
            }
            else
            {
                var reloadTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>().SetupProperty(p => p.ResponseCode, "00");
                reloadTransaction.SetupGet(p => p.TransactionSucceeded).Returns(transactionSuccess);
                cardIssuerDal.Setup(
                    p => p.Reload(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<decimal>()))
                             .Returns(reloadTransaction.Object);
            }

            var redeemTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>().SetupProperty(p => p.ResponseCode, "00");
            redeemTransaction.SetupGet(p => p.TransactionSucceeded).Returns(true);
            redeemTransaction.SetupProperty(p => p.Amount, transferAmount);

            cardIssuerDal.Setup(
                p => p.Redeem(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<decimal>()))
                         .Returns(redeemTransaction.Object);

            if (throwVoidRedeemException)
            {
                cardIssuerDal.Setup(
                    p => p.VoidRequest(It.IsAny<ICardTransaction>(), It.IsAny<IMerchantInfo>()))
                             .Throws<Exception>();
            }
            else
            {
                var voidRedeemTransaction = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>().SetupProperty(p => p.ResponseCode, "00");
                voidRedeemTransaction.SetupGet(p => p.TransactionSucceeded).Returns(true);
                voidRedeemTransaction.SetupProperty(p => p.Amount, transferAmount);

                cardIssuerDal.Setup(
                    p => p.VoidRequest(It.IsAny<ICardTransaction>(), It.IsAny<IMerchantInfo>()))
                             .Returns(voidRedeemTransaction.Object);
            }

            var cardTransaction = new Mock<ICardTransaction>();
            cardTransaction.SetupProperty(p => p.RequestCode, "0400");
            cardTransaction.SetupProperty(p => p.UtcDate, DateTime.UtcNow.AddDays(1));
            cardIssuerDal.Setup(
                p => p.GetHistoryCondensed(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new List<ICardTransaction> { cardTransaction.Object });

            return cardIssuerDal;
        }

        private AssociatedCard CreateAssociatedCard()
        {
            var card = new AssociatedCard(ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection);
            card.Currency = "USD";
            card.Actions = new List<string>();
            card.Balance = 100M;
            card.RegisteredUserId = "1";
            card.AutoReloadId = "13";
            card.Pin = "01235678";
            return card;
        }

        private static Mock<ISettingsProvider> CreateSettingProvider()
        {
            var settingProvider = new Mock<ISettingsProvider>();
            settingProvider.Setup(x => x.GetCurrencyCodeForMarketCode(It.IsAny<string>())).Returns("USD");
            return settingProvider;
        }

        private static ICardProvider CreateCardProvider()
        {
            return new CardProvider(
                new Mock<ICardDal>().Object,
                new Mock<ICardIssuerDal>().Object,
                new Mock<ILogCounterManager>().Object,
                new Mock<IMessageBroker>().Object,
                new Mock<ISettingsProvider>().Object,
                new Mock<CardProviderSettingsSection>().Object,
                new Mock<ILogRepository>().Object,
                new Mock<IMessageListener>().Object,
                null,
                new Mock<NServiceBus.ISendOnlyBus>().Object);
        }
    }
}