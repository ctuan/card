﻿using System;
using Starbucks.Card.Provider.Common.Models;



namespace Starbucks.Card.Provider.DalModels
{
    public class CardTransaction : ICardTransaction 
    {
        private readonly CardIssuer.Dal.Common.Models.ICardTransaction _cardTransaction;

        public CardTransaction(CardIssuer.Dal.Common.Models.ICardTransaction cardTransaction)
        {
            _cardTransaction = cardTransaction;
        }

        public string CardId { get { return _cardTransaction.CardId; } set { _cardTransaction.CardId = value; } }
        public string TransactionId { get { return _cardTransaction.TransactionId; } set { _cardTransaction.TransactionId = value; } }
        public string AuthorizationCode { get { return _cardTransaction.AuthorizationCode; } set { _cardTransaction.AuthorizationCode = value; } }
        public string CardClass { get { return _cardTransaction.CardClass; } set { _cardTransaction.CardClass = value; } }
        public decimal Amount { get { return _cardTransaction.Amount; } set { _cardTransaction.Amount = value; } }
        public string Currency { get { return _cardTransaction.Currency; } set { _cardTransaction.Currency = value; } }
        public string Description { get { return _cardTransaction.Description; } set { _cardTransaction.Description = value; } }
        public DateTime TransactionDate { get { return _cardTransaction.TransactionDate; } set { _cardTransaction.TransactionDate = value; } }
        public decimal BeginningBalance { get { return _cardTransaction.BeginningBalance; } set { _cardTransaction.BeginningBalance = value; } }
        public decimal EndingBalance { get { return _cardTransaction.EndingBalance; } set { _cardTransaction.EndingBalance = value; } }
        public string RequestCode { get { return _cardTransaction.RequestCode; } set { _cardTransaction.RequestCode = value; } }
        public string ResponseCode { get { return _cardTransaction.ResponseCode; } set { _cardTransaction.ResponseCode = value; } }
        public bool TransactionSucceeded { get { return _cardTransaction.TransactionSucceeded; } }
        public ICardPromotion Promotion { get;  set ;  }
        public string Pin { get { return _cardTransaction.Pin; } set { _cardTransaction.Pin = value; } }
        public string CardNumber { get { return _cardTransaction.CardNumber; } set { _cardTransaction.CardNumber = value; } }
        public string BaseCurrency { get { return _cardTransaction.BaseCurrency; } set { _cardTransaction.BaseCurrency = value; } }
        public decimal BeginingBalanceInBaseCurrency { get { return _cardTransaction.BeginingBalanceInBaseCurrency; } set { _cardTransaction.BeginingBalanceInBaseCurrency = value; } }
        public decimal EndingBalanceInBaseCurrency { get { return _cardTransaction.EndingBalanceInBaseCurrency; } set { _cardTransaction.EndingBalanceInBaseCurrency = value; } }
        public decimal AmountInBaseCurrency { get { return _cardTransaction.AmountInBaseCurrency; } set { _cardTransaction.AmountInBaseCurrency = value; } }
    }
}
