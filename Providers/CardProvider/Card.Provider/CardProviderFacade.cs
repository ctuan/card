﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Card.Provider.Interfaces;

namespace Card.Provider
{
    public class CardProviderFacade :ICardProvider 
    {
        private readonly ICardProvider _cardProvider;

        public CardProviderFacade(ICardProvider cardProvider)
        {
            _cardProvider = cardProvider;
        }

        public IStarbucksCard GetStarbucksCardByNumberAndPin(string cardNumber, string pin)
        {
            return _cardProvider.GetStarbucksCardByNumberAndPin(cardNumber, pin);
        }

        public IEnumerable< IStarbucksCardImage> GetStarbucksCardImageUrlByCardNumber(string cardNumber)
        {
            return _cardProvider.GetStarbucksCardImageUrlByCardNumber(cardNumber);
        }

        public IEnumerable<IStarbucksCard> GetStarbucksCards(string userId, bool enableBalanceUpdatesInCompleteProfile)
        {
            return _cardProvider.GetStarbucksCards(userId, enableBalanceUpdatesInCompleteProfile);
        }

        public IEnumerable<IStarbucksCardStatus> GetStatusesForUserCards(string userId, IEnumerable<string> cardIds)
        {
            return _cardProvider.GetStatusesForUserCards(userId, cardIds);
        }

        public IStarbucksCard GetStarbucksCardById(string userId, string cardId)
        {
            return _cardProvider.GetStarbucksCardById(userId, cardId);
        }

        public IStarbucksCardBalance RefreshStarbucksCardBalance(string userId, string cardId)
        {
            return _cardProvider.RefreshStarbucksCardBalance(userId, cardId);
        }

        public IStarbucksCardBalance RefreshStarbucksCardBalanceByCardNumberPinNumber(string cardNumber, string pin)
        {
            return _cardProvider.RefreshStarbucksCardBalanceByCardNumberPinNumber(cardNumber, pin);
        }

        public IEnumerable<IStarbucksCardTransaction> GetStarbucksCardTransactions(string userId, string cardId)
        {
            return _cardProvider.GetStarbucksCardTransactions(userId, cardId);
        }

        public IStarbucksCard RegisterStarbucksCard(string userId, string cardNumber, string pin)
        {
            return _cardProvider.RegisterStarbucksCard(userId, cardNumber, pin);
        }

        public IStarbucksCard ActivateAndRegisterCard(string userId)
        {
            return _cardProvider.ActivateAndRegisterCard(userId);
        }

        public IEnumerable<ICardRegistrationStatus> RegisterMultipleStarbucksCards(string userId,
                                                                            IEnumerable<IStarbucksCardNumberAndPin>
                                                                                cards)
        {
            return _cardProvider.RegisterMultipleStarbucksCards(userId, cards);
        }

        public IStarbucksCardUnregisterResult UnregisterStarbucksCard(string userId, string cardId)
        {
            return _cardProvider.UnregisterStarbucksCard(userId, cardId);
        }

        public IStarbucksCardTransaction ReloadStarbucksCard(string userId, string cardId,
                                                      IReloadForStarbucksCard reloadForStarbucksCard)
        {
            return _cardProvider.ReloadStarbucksCard(userId, cardId, reloadForStarbucksCard);
        }

        public IStarbucksCardTransaction ReloadStarbucksCardByCardNumberPin(string cardNumber, string pin,
                                                                     IReloadForStarbucksCard reloadForStarbucksCard)
        {
            return _cardProvider.ReloadStarbucksCardByCardNumberPin(cardNumber, pin, reloadForStarbucksCard);
        }

        public IAutoReloadProfile SetupAutoReload(string userId, string cardId, IAutoReloadProfile autoReloadProfile)
        {
            return _cardProvider.SetupAutoReload(userId, cardId, autoReloadProfile);
        }

        public IAutoReloadProfile UpdateAutoReload(string userId, string cardId, IAutoReloadProfile autoReloadProfile)
        {
            return _cardProvider.UpdateAutoReload(userId, cardId, autoReloadProfile);
        }

        public void EnableAutoReload(string userId, string cardId)
        {
            _cardProvider.EnableAutoReload(userId, cardId);
        }

        public void DisableAutoReload(string userId, string cardId)
        {
            _cardProvider.DisableAutoReload(userId, cardId);
        }

      public  ITransferBalanceResult TransferStarbucksCardBalance(string userId, string fromCardId, string toCardId,
                                                            decimal? amount)
        {
            return _cardProvider.TransferStarbucksCardBalance(userId, fromCardId, toCardId, amount);
        }
    }
}
