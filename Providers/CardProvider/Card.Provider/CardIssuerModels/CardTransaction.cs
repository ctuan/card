﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.CardIssuer.Dal.Common.Models;

namespace Starbucks.Card.Provider.CardIssuerModels
{
    public class CardTransaction : Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction 
    {
        public string CardId { get; set; }
        public string TransactionId { get; set; }
        public string AuthorizationCode { get; set; }
        public string CardClass { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public string Description { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal BeginningBalance { get; set; }
        public decimal EndingBalance { get; set; }
        public string RequestCode { get; set; }
        public string ResponseCode { get; set; }
        public bool TransactionSucceeded { get; private set; }
        public ICardPromotion Promotion { get; set; }
        public string Pin { get; set; }
        public string CardNumber { get; set; }
        public string BaseCurrency { get; set; }
        public decimal BeginingBalanceInBaseCurrency { get; set; }
        public decimal EndingBalanceInBaseCurrency { get; set; }
        public decimal AmountInBaseCurrency { get; set; }
        public string MerchantId { get; set; }
        public string TerminalId { get; set; }
        public string StoreId { get; set; }
        public DateTime UtcDate { get; set; }        
        public decimal LockAmount  { get; set; }
    }
}
