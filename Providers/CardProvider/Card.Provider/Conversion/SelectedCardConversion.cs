﻿using Starbucks.Card.Dal.Common.Models;
using Starbucks.Card.Provider.Models;

namespace Starbucks.Card.Provider.Conversion
{
    internal static class SelectedCardConversion
    {
        internal static Starbucks.Card.Provider.Common.Models.ISelectedCard ToProvider(this ISelectedCard source)
        {
            if (source == null)
            {
                return null;
            }
            var target = new SelectedCard
                {
                    CardId = source.CardId,
                    ClientId = source.ClientId,
                    UserId = source.UserId
                };
            return target;
        }
    }
}
