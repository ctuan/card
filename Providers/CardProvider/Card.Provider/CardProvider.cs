﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using NServiceBus;
using Starbucks.Api.Sdk.PaymentService;
using Starbucks.Api.Sdk.PaymentService.Interfaces;
using Starbucks.Api.Sdk.PaymentService.Models;
using Starbucks.Card.Dal.Common;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.Card.Provider.Common;
using Starbucks.Card.Provider.Common.ErrorResources;
using Starbucks.Card.Provider.Common.Exceptions;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.Card.Provider.Configuration;
using Starbucks.Card.Provider.Models;
using Starbucks.Card.Provider.Validations;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.Common.Rule;
using Starbucks.CardIssuer.Dal.SvDot.Common;
using Starbucks.SecurePayPalPayment.Provider.Common;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;
using Starbucks.LogCounter.Provider.Common;
using Starbucks.PaymentMethod.Provider.Common.Models;
using Starbucks.PaymentMethod.Provider.Common;
using Starbucks.FraudData.QueueService.Common.Commands;
using Starbucks.MessageBroker.Common;
using Starbucks.OpenApi.Logging.Common;
using Starbucks.Platform.Security;
using Starbucks.Settings.Provider.Common;
using AutoReloadProfile = Starbucks.Card.Provider.Models.AutoReloadProfile;
using IAssociatedCard = Starbucks.Card.Provider.Common.Models.IAssociatedCard;
using IAutoReloadProfile = Starbucks.Card.Provider.Common.Models.IAutoReloadProfile;
using ICard = Starbucks.Card.Provider.Common.Models.ICard;
using ICardTransaction = Starbucks.Card.Provider.Common.Models.ICardTransaction;
using IRisk = Starbucks.Card.Provider.Common.Models.IRisk;
using RegistrationResult = Starbucks.Card.Provider.Common.Models.RegistrationResult;
using StarbucksAccountProvider = Account.Provider;
using IAccountAddress = Account.Provider.Common.Models.IAddress;
using SdkModels = Starbucks.Api.Sdk.PaymentService.Models;
using Starbucks.Platform.OrderManagement.Contracts.Email;
using Starbucks.MessageBroker.StageEmailListener;
using AssociatedCard = Starbucks.Card.Provider.Models.AssociatedCard;
using CardBalance = Starbucks.Card.Provider.Models.CardBalance;
using CardTransaction = Starbucks.Card.Provider.DalModels.CardTransaction;
using CardType = Starbucks.Card.Provider.Common.Models.CardType;
using VisibilityLevel = Starbucks.Card.Provider.Common.Enums.VisibilityLevel;


namespace Starbucks.Card.Provider
{
    public class CardProvider : ICardProvider
    {
        private readonly ICardDal _cardDal;
        private readonly ICardIssuerDal _cardIssuer;
        private readonly ILogCounterManager _logCounterManager;
        private readonly IMessageBroker _messageBroker;
        private readonly ISettingsProvider _settingsProvider;
        private readonly CardProviderSettingsSection _cardProviderSettingsSection;
        private readonly ILogRepository _logRepository;
        private readonly IMessageListener _loyaltyHostListener;//MB is throwing error Container being disposed on EndRequest
        private readonly IPaymentServiceApiClient _paymentServiceApiClient;
        private readonly IPaymentMethodProvider _paymentMethodProvider;
        private readonly ISecurePayPalPaymentClient _securePayPalPayment;
        private readonly StarbucksAccountProvider.Common.IAccountProvider _accountProvider;
        private readonly NServiceBus.ISendOnlyBus _bus;

        private const string DefaultIsLoggedInValue = "true";
        private const string LogTitle = "CardApi - CardProvider";
        private const string UnKnownPlatform = "unknown";

        public CardProvider(ICardDal cardDal, ICardIssuerDal cardIssuer, ILogCounterManager logCounterManager,
                            IMessageBroker messageBroker, ISettingsProvider settingsProvider, CardProviderSettingsSection cardProviderSettingsSection,
                            ILogRepository logRepository, IMessageListener loyaltyHostListener, IPaymentServiceApiClient paymentServiceApiClient,
                            ISendOnlyBus bus, ISecurePayPalPaymentClient securePayPalPayment = null, IPaymentMethodProvider paymentMethodProvider = null,
                            StarbucksAccountProvider.Common.IAccountProvider accountProvider = null)
        {
            _cardDal = cardDal;
            _cardIssuer = cardIssuer;
            _logCounterManager = logCounterManager;
            _messageBroker = messageBroker;
            _settingsProvider = settingsProvider;
            _cardProviderSettingsSection = cardProviderSettingsSection;
            _logRepository = logRepository;
            _loyaltyHostListener = loyaltyHostListener;
            _paymentServiceApiClient = paymentServiceApiClient;
            _paymentMethodProvider = paymentMethodProvider;
            _accountProvider = accountProvider;
            _securePayPalPayment = securePayPalPayment;
            _bus = bus;
        }

        #region Culture

        //TODO: Evaluate if we can just use US instead of MOUS etc.  Need to figure out someway to get MOUS more reliably if cannot use just US.
        private const string DefaultPlatformRegSourceCode = "iOS";
        private const string MarketingRegSourceCode = "Starbucks";

        private string CurrentSubMarket(string accountMarket = null, bool isDigitalCard = false)
        {
            if (isDigitalCard)
            {
                string marketLocale = _settingsProvider.GetLocaleByMarket(accountMarket).FirstOrDefault();
                return _settingsProvider.GetDigitalCardMarketByLocale(marketLocale);
            }

            return _settingsProvider.GetStandardizedSubMarketCode(accountMarket);
        }

        #endregion

        private System.Diagnostics.TraceEventType AddFraudDataLoggingProperties(Dictionary<string, string> properties, string key, object value, TraceEventType currentLevel, bool allowMissing = false)
        {
            if (value == null)
            {
                properties.Add(key, "<Null Value>");
                return allowMissing ? currentLevel : TraceEventType.Warning;
            }

            string strValue = value.ToString();

            if (string.IsNullOrWhiteSpace(strValue))
            {
                properties.Add(key, "<Empty Value>");
                return allowMissing ? currentLevel : TraceEventType.Warning;
            }

            properties.Add(key, strValue);
            return currentLevel;
        }

        private void ConstructFraudMessageBase(string userId,
                                                string market,
                                                string registrationSourcePlatform,
                                                Starbucks.Card.Provider.Common.Models.IRiskWithoutIovation risk,
                                                FraudMessageBase fraudMessageBase)
        {
            fraudMessageBase.UserId = userId;
            fraudMessageBase.CCAgentName = (risk != null) ? risk.CcAgentName : null;
            fraudMessageBase.IsUserLoggedIn = (risk != null && risk.IsLoggedIn.HasValue) ? risk.IsLoggedIn.Value : true;
            fraudMessageBase.TransactionDateTime = DateTime.Now;
            fraudMessageBase.Market = (risk != null && !string.IsNullOrWhiteSpace(risk.Market)) ? risk.Market : market;
            fraudMessageBase.Platform = (risk != null && !string.IsNullOrWhiteSpace(risk.Platform)) ?
                                                                                            risk.Platform :
                                                                                            !string.IsNullOrWhiteSpace(registrationSourcePlatform) ?
                                                                                                registrationSourcePlatform :
                                                                                                DefaultPlatformRegSourceCode;
        }

        private void FillInAutoReloadProfileForFraudMessage(IAssociatedCard card, FraudData.QueueService.Common.Commands.AddCard addCard)
        {
            if (card.AutoReloadProfile == null)
            {
                addCard.AutoReloadProfile = null;
                return;
            }

            Starbucks.FraudData.QueueService.Common.Commands.AutoReloadProfile autoReloadProfile =
                            new Starbucks.FraudData.QueueService.Common.Commands.AutoReloadProfile();

            autoReloadProfile.AutoReloadAmount = card.AutoReloadProfile.Amount;
            autoReloadProfile.AutoReloadId = card.AutoReloadProfile.AutoReloadId;
            autoReloadProfile.AutoReloadType = card.AutoReloadProfile.AutoReloadType;
            autoReloadProfile.Day = card.AutoReloadProfile.Day;
            autoReloadProfile.DisableUntilDate = card.AutoReloadProfile.DisableUntilDate;
            autoReloadProfile.PaymentMethodId = card.AutoReloadProfile.PaymentMethodId;
            autoReloadProfile.Status = card.AutoReloadProfile.Status.ToString();
            autoReloadProfile.StoppedDate = card.AutoReloadProfile.StoppedDate;
            autoReloadProfile.TriggerAmount = card.AutoReloadProfile.TriggerAmount;

            addCard.AutoReloadProfile = autoReloadProfile;
        }

        private void ReportFraudDataForDisableAutoReload(IAssociatedCard card, string userId, string market,
                                    string registrationSourcePlatform, Card.Provider.Common.Models.IRiskWithoutIovation risk)
        {
            if (card == null)
                return;

            DisableAutoReload disableAutoReload = new DisableAutoReload
            {
                // from DisableAutoReload
                CardId = card.CardId,
            };

            ConstructFraudMessageBase(userId, market, registrationSourcePlatform, risk, disableAutoReload);

            LogFraudDataReport(disableAutoReload, LogTitle);

            ReportFraudData(disableAutoReload, SdkModels.TransactionTypes.DisableAutoReloadSettings);
        }


        private void ReportFraudDataForAddCard(IAssociatedCard card, string userId, string market,
                                    string registrationSourcePlatform,
                                    string registrationSourceMarketing,
                                    Card.Provider.Common.Models.IRiskWithoutIovation risk)
        {
            if (card == null)
                return;

            FraudData.QueueService.Common.Commands.AddCard addCard = new FraudData.QueueService.Common.Commands.AddCard
            {
                // from AddCard
                Balance = card.Balance,
                CardId = card.CardId,
                CardMarket = card.SubMarketCode,
                CardNickname = card.Nickname,
                CardNumber = card.Number,
                CardType = card.Type.ToString(),
                CurrencyCode = card.Currency,
                IsDigital = card.IsDigitalCard,
                IsOwner = card.IsOwner,
                IsPartner = card.IsPartner,
                IsPrimary = card.IsDefault,
                MarketingRegSource = registrationSourceMarketing,
                PlatformRegSource = registrationSourcePlatform,
            };

            ConstructFraudMessageBase(userId, market, registrationSourcePlatform, risk, addCard);

            FillInAutoReloadProfileForFraudMessage(card, addCard);

            ReportFraudData(addCard, SdkModels.TransactionTypes.AddCard);
        }

        private void ReportFraudDataForRemoveCard(IAssociatedCard card, string userId, string market,
                                    string registrationSourcePlatform, Starbucks.Card.Provider.Common.Models.IRiskWithoutIovation risk)
        {
            if (card == null)
                return;

            FraudData.QueueService.Common.Commands.RemoveCard removeCard = new FraudData.QueueService.Common.Commands.RemoveCard
            {
                // from RemoveCard
                CardId = card.CardId,
            };

            ConstructFraudMessageBase(userId, market, registrationSourcePlatform, risk, removeCard);

            LogFraudDataReport(removeCard, LogTitle);

            ReportFraudData(removeCard, SdkModels.TransactionTypes.RemoveCard);
        }

        private void LogFraudDataReport(FraudData.QueueService.Common.Commands.AddCard command, string title)
        {
            try
            {
                Dictionary<string, string> properties = new Dictionary<string, string>();
                string message = "Report FraudData (Add Card)";
                properties.Add("hasAddCardCommandObject", (command != null).ToString());
                properties.Add("hasBusObject", (_bus != null).ToString());
                properties.Add("hasAutoReloadProfile", (command.AutoReloadProfile != null).ToString());
                properties.Add("currentTime", DateTime.Now.ToString());

                System.Diagnostics.TraceEventType level = (_bus != null && command != null) ? TraceEventType.Information : TraceEventType.Warning;
                if (command != null)
                {
                    level = AddFraudDataLoggingProperties(properties, "Request: TransactionDateTime", command.TransactionDateTime.ToString(), level);
                    level = AddFraudDataLoggingProperties(properties, "Request: UserId", command.UserId, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: CardId", command.CardId, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: CardMarket", command.CardMarket, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: CardNickname", command.CardNickname, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: CardNumber", command.CardNumber, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: CardType", command.CardType, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: CurrencyCode", command.CurrencyCode, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: Market", command.Market, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: Platform", command.Platform, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: IsUserLoggedIn", command.IsUserLoggedIn, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: CCAgentName", command.CCAgentName, level, allowMissing: true);
                    level = AddFraudDataLoggingProperties(properties, "Request: MarketingRegSource", command.MarketingRegSource, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: PlatformRegSource", command.PlatformRegSource, level);
                    if (command.Balance.HasValue)
                    {
                        level = AddFraudDataLoggingProperties(properties, "Request: Balance", command.Balance.Value, level, allowMissing: true);
                    }
                    if (command.IsDigital.HasValue)
                    {
                        level = AddFraudDataLoggingProperties(properties, "Request: IsDigital", command.IsDigital.Value, level);
                    }
                    if (command.IsOwner.HasValue)
                    {
                        level = AddFraudDataLoggingProperties(properties, "Request: IsOwner", command.IsOwner.Value, level);
                    }
                    if (command.IsPartner.HasValue)
                    {
                        level = AddFraudDataLoggingProperties(properties, "Request: IsPartner", command.IsPartner.Value, level);
                    }
                    if (command.IsPrimary.HasValue)
                    {
                        level = AddFraudDataLoggingProperties(properties, "Request: IsPrimary", command.IsPrimary.Value, level);
                    }
                }
                if (level == TraceEventType.Warning)
                {
                    message += ": missing value(s)";
                }
                _logRepository.Log("APILog", title, message, level, properties);
            }
            catch (System.Exception ex)
            {
                // logging error, we have to eat it.  
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }


        private void LogFraudDataReport(FraudData.QueueService.Common.Commands.RemoveCard command, string title)
        {
            try
            {
                Dictionary<string, string> properties = new Dictionary<string, string>();
                string message = "Report FraudData (Remove Card)";
                properties.Add("hasRemoveCommandObject", (command != null).ToString());
                properties.Add("hasBusObject", (_bus != null).ToString());
                properties.Add("currentTime", DateTime.Now.ToString());

                System.Diagnostics.TraceEventType level = (_bus != null && command != null) ? TraceEventType.Information : TraceEventType.Warning;
                if (command != null)
                {
                    level = AddFraudDataLoggingProperties(properties, "Request: TransactionDateTime", command.TransactionDateTime.ToString(), level);
                    level = AddFraudDataLoggingProperties(properties, "Request: UserId", command.UserId, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: CardId", command.CardId, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: Market", command.Market, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: Platform", command.Platform, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: IsUserLoggedIn", command.IsUserLoggedIn, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: CCAgentName", command.CCAgentName, level, allowMissing: true);
                }

                if (level == TraceEventType.Warning)
                {
                    message += ": missing value(s)";
                }
                _logRepository.Log("APILog", title, message, level, properties);
            }
            catch (System.Exception ex)
            {
                // logging error, we have to eat it.  
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        private void LogFraudDataReport(FraudData.QueueService.Common.Commands.DisableAutoReload command, string title)
        {
            try
            {
                Dictionary<string, string> properties = new Dictionary<string, string>();
                string message = "Report FraudData (Disable AutoReload)";
                properties.Add("hasDisableCommandObject", (command != null).ToString());
                properties.Add("hasBusObject", (_bus != null).ToString());
                properties.Add("currentTime", DateTime.Now.ToString());

                System.Diagnostics.TraceEventType level = (_bus != null && command != null) ? TraceEventType.Information : TraceEventType.Warning;
                if (command != null)
                {
                    level = AddFraudDataLoggingProperties(properties, "Request: TransactionDateTime", command.TransactionDateTime.ToString(), level);
                    level = AddFraudDataLoggingProperties(properties, "Request: UserId", command.UserId, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: CardId", command.CardId, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: Market", command.Market, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: Platform", command.Platform, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: IsUserLoggedIn", command.IsUserLoggedIn, level);
                    level = AddFraudDataLoggingProperties(properties, "Request: CCAgentName", command.CCAgentName, level, allowMissing: true);
                }

                if (level == TraceEventType.Warning)
                {
                    message += ": missing value(s)";
                }
                _logRepository.Log("APILog", title, message, level, properties);
            }
            catch (System.Exception ex)
            {
                // logging error, we have to eat it.  
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        private void ReportFraudData<T>(T message, SdkModels.TransactionTypes type) where T : FraudMessageBase
        {
            if (message == null)
            {
                string warnning = string.Format("sending {0} type message but message object is <NULL>", type);
                LogMessage(warnning, System.Diagnostics.TraceEventType.Warning);
                return;
            }

            if (_bus == null)
            {
                LogMessage("SendOnlyBus is NULL.  Check the NSB configuration", System.Diagnostics.TraceEventType.Error);
                return;
            }

            try
            {
                Starbucks.FraudData.QueueService.Common.FraudDataQueueServiceClientHelper.ReportFraudData(_bus, message);
            }
            catch (Exception ex)
            {
                string resultMessage =
                    string.Format("Exception occurred while sending {0} type message to NServiceBus: {1}. InnerException: {2} for User: {3}.", type, ex.Message
                                  , (ex.InnerException != null) ? ex.InnerException.Message : string.Empty, message.UserId);
                LogMessage(resultMessage, System.Diagnostics.TraceEventType.Error);

            }
        }

        private void LogMessage(string message, System.Diagnostics.TraceEventType type, string title = "CardApi - CardProvider")
        {
            try
            {
                if (_logRepository != null)
                {
                    _logRepository.Log("APILog", title, message, type);
                }
            }
            catch (Exception ex)
            {
                // logging error, have to eat the exception
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        #region GetCards

        public ICard GetCardByNumberAndPin(string cardNumber, string pin, VisibilityLevel visibilityLevel,
                                           string market = null)
        {
            Validator.ValidateCardAndPin(cardNumber, pin);
            var card = _cardDal.GetCardByNumber(cardNumber);
            IMerchantInfo merchantInfo;
            if (card != null)
            {
                merchantInfo = GetMerchantInfo(CurrentSubMarket(string.IsNullOrEmpty(market) ? card.SubMarketCode : market));
            }
            else
            {
                merchantInfo = GetMerchantInfo(CurrentSubMarket(market));
            }
            //if card is not found try to validate and save the card
            if (card == null)
            {
                IAssociatedCard tempCard = new AssociatedCard(_cardProviderSettingsSection);
                if (ValidateAndSaveCard(cardNumber, pin, CurrentSubMarket(market), ref tempCard, merchantInfo))
                {
                    card = _cardDal.GetCardByNumber(cardNumber);
                }
            }

            if (card == null)
                throw new CardProviderException(CardProviderErrorResource.CardNotFoundCode,
                                                CardProviderErrorResource.CardNotFoundMessage);

            if (Encryption.DecryptPin(card.Pin) != pin)
            {
                throw new CardProviderException(CardProviderErrorResource.InvalidPinCode,
                                                CardProviderErrorResource.InvalidPinMessage);
            }

            var retCard = (ICard)(card);
            ApplyVisibilityLevel(visibilityLevel, retCard);

            if (!card.Balance.HasValue || card.Currency != merchantInfo.CurrencyCode)
            {
                var balance = GetCardBalanceByCardNumberPinNumberRealTime(cardNumber, pin, market, false);
                if (balance != null)
                {
                    card.Balance = balance.Balance;
                    card.BalanceCurrency = balance.BalanceCurrencyCode;
                    card.BalanceDate = balance.BalanceDate;
                }
            }
            return Models.Card.FromICard(retCard, _cardProviderSettingsSection);
        }

        public IAssociatedCard GetCardById(string userId, string cardId, VisibilityLevel visibilityLevel,
                                           bool includeAutoReload, string market, string userMarket, bool includeAssociatedCards = false, string platform = UnKnownPlatform)
        {
            Validator.ValidateCardId(cardId);
            Validator.ValidateUserId(userId);

            IMerchantInfo userMerchantInfo;
            IMerchantInfo currentMerchantInfo;
            GetMerchantInfos(market, userMarket, platform, out userMerchantInfo, out currentMerchantInfo);

            var card = _cardDal.GetCard(userId, cardId, includeAssociatedCards);
            if (card == null) return null;

            if (!String.IsNullOrEmpty(card.AutoReloadId) && includeAutoReload)
                card.AutoReloadProfile = GetAutoReload(userId, card.AutoReloadId);

            if (!card.Balance.HasValue || card.BalanceCurrency != currentMerchantInfo.CurrencyCode)
            {
                var balance = GetCardBalanceByCardNumberPinNumberRealTime(Encryption.DecryptCardNumber(card.Number),
                                                                          Encryption.DecryptPin(card.Pin), card.SubMarketCode,
                                                                          userMerchantInfo.CurrencyCode ==
                                                                          currentMerchantInfo.CurrencyCode);
                card.Balance = balance.Balance;
                card.BalanceCurrency = balance.BalanceCurrencyCode;
                card.BalanceDate = balance.BalanceDate;
            }
            ApplyVisibilityLevel(visibilityLevel, card);

            var mkt = string.IsNullOrEmpty(market) ? userMarket : market;
            card.Actions = CardActions.GetValidActions(mkt, _settingsProvider.GetCurrencyCodeForMarketCode(mkt), card.Currency);
            return AssociatedCard.FromIAssociatedCard(card, _cardProviderSettingsSection);
        }
        /// <summary>
        /// Gets the card details by cardId.
        /// </summary>
        /// <param name="userId"> user id. </param>
        /// <param name="cardId"> unencrypted card id. </param>
        ///  <param name="cardNumber"> card number. </param>
        /// <param name="market"> user sub market. </param>
        /// <returns>An instance that has all card detail info.</returns>
        public IAssociatedCard GetCardById(string userId, string cardId, string cardNumber, string market, string userMarket)
        {
            // Check if cardId exists. If the card is newly inserted to card table, by the time we tried to get cardId it wasn't created. 
            if (string.IsNullOrEmpty(cardId) && (!string.IsNullOrEmpty(cardNumber)))
            {
                cardId = GetCardIdByCardNumber(cardNumber);
            }
            IMerchantInfo userMerchantInfo;
            IMerchantInfo currentMerchantInfo;
            GetMerchantInfos(market, userMarket, UnKnownPlatform, out userMerchantInfo, out currentMerchantInfo);

            var card = _cardDal.GetCard(Encryption.EncryptCardId(Convert.ToInt32(cardId)));

            if (!card.Balance.HasValue || card.BalanceCurrency != currentMerchantInfo.CurrencyCode)
            {
                var balance = GetCardBalanceByCardNumberPinNumberRealTime(Encryption.DecryptCardNumber(card.Number),
                                                                          Encryption.DecryptPin(card.Pin), market,
                                                                          userMerchantInfo.CurrencyCode ==
                                                                          currentMerchantInfo.CurrencyCode);
                card.Balance = balance.Balance;
                card.BalanceCurrency = balance.BalanceCurrencyCode;
                card.BalanceDate = balance.BalanceDate;
            }

            DecryptCard(card);
            card.Actions = CardActions.GetValidActions(userMarket, _settingsProvider.GetCurrencyCodeForMarketCode(userMarket), card.Currency);
            _cardDal.LoadCardAssociation(card, userId);
            return AssociatedCard.FromIAssociatedCard(card, _cardProviderSettingsSection);
        }

        private void GetMerchantInfos(string market, string userMarket, string platform, out IMerchantInfo userMerchantInfo,
                                      out IMerchantInfo currentMerchantInfo)
        {
            var marketSubMarket = CurrentSubMarket(market);
            var userSubMarket = CurrentSubMarket(userMarket);

            if (marketSubMarket != userSubMarket)
            {
                userMerchantInfo = GetMerchantInfo(userSubMarket, platform);
                currentMerchantInfo = GetMerchantInfo(marketSubMarket, platform);
            }
            else
            {
                userMerchantInfo = GetMerchantInfo(userSubMarket, platform);
                currentMerchantInfo = userMerchantInfo;
            }
        }

        public IEnumerable<IStarbucksCardImage> GetStarbucksCardImageUrlByCardNumber(string cardNumber)
        {
            Validator.ValidateCardNumber(cardNumber);
            var card = _cardDal.GetCardByNumber(cardNumber);
            return card == null ? null : Card.Provider.Models.Card.FromICard(card, _cardProviderSettingsSection).CardImages;
        }

        public IEnumerable<IAssociatedCard> GetCards(string userId, VisibilityLevel visibilityLevel,
                                                     bool includeAutoReload, bool includeRealTimeBalance, string market,
                                                     string userMarket)
        {
            Validator.ValidateUserId(userId);

            IMerchantInfo userMerchantInfo;
            IMerchantInfo currentMerchantInfo;
            GetMerchantInfos(market, userMarket, UnKnownPlatform, out userMerchantInfo, out currentMerchantInfo);

            var cards = _cardDal.GetAssociatedCards(userId);

            if (cards == null) return null;
            var associatedCards = cards as IAssociatedCard[] ?? cards.ToArray();

            //Check to see if there is a primary, if not set a primary
            if (!associatedCards.Any(p => p.IsDefault))
            {
                var nextDefault =
                    associatedCards.Where(
                        p => IsCardOwned(userId, p))
                                   .OrderByDescending(p => p.RegistrationDate)
                                   .FirstOrDefault();

                if (nextDefault != null && !string.IsNullOrEmpty(nextDefault.CardId))
                    UpdateDefaultCard(userId, nextDefault.CardId);
            }

            if (includeAutoReload)
            {
                Parallel.ForEach(associatedCards, card =>
                {
                    if (!string.IsNullOrEmpty(card.AutoReloadId))
                    {
                        card.AutoReloadProfile = GetAutoReload(userId, card.AutoReloadId);
                    }
                });
            }

            if (includeRealTimeBalance)
            {
                Parallel.ForEach(associatedCards, card =>
                {
                    if (card.Balance.HasValue && card.BalanceCurrency == currentMerchantInfo.CurrencyCode) return;
                    if (!card.IsOwner) return;
                    try
                    {
                        var balance =
                            GetCardBalanceByCardNumberPinNumberRealTime(Encryption.DecryptCardNumber(card.Number),
                                                                        Encryption.DecryptPin(card.Pin), market,
                                                                        userMerchantInfo.CurrencyCode ==
                                                                        currentMerchantInfo.CurrencyCode);
                        if (balance == null) return;
                        card.BalanceDate = balance.BalanceDate;
                        card.BalanceCurrency = balance.BalanceCurrencyCode;
                        card.Balance = balance.Balance;
                    }
                    catch
                    {
                        //eat this and return no balance info for the card
                    }

                });
            }

            //default is encrypted from db
            ApplyVisibilityLevel(visibilityLevel, associatedCards);
            foreach (var ac in associatedCards)
            {
                CleanNonOwnerCard(ac);
                var mkt = string.IsNullOrEmpty(market) ? userMarket : market;
                if (!string.IsNullOrEmpty(mkt))
                    ac.Actions = CardActions.GetValidActions(mkt, _settingsProvider.GetCurrencyCodeForMarketCode(mkt), ac.Currency);
            }
            return associatedCards.Select(p => AssociatedCard.FromIAssociatedCard(p, _cardProviderSettingsSection));

        }

        public IAssociatedCard GetPrimaryCard(string userId, VisibilityLevel visibilityLevel, bool includeAutoReload,
                                              string market, string userMarket)
        {
            IMerchantInfo userMerchantInfo;
            IMerchantInfo currentMerchantInfo;
            GetMerchantInfos(market, userMarket, UnKnownPlatform, out userMerchantInfo, out currentMerchantInfo);

            var allCards = GetCards(userId, visibilityLevel, includeAutoReload, false, market, userMarket);
            var card =
                allCards.FirstOrDefault(
                    p => p.IsDefault && userId.Equals(p.RegisteredUserId, StringComparison.InvariantCultureIgnoreCase));
            if (card == null) return null;
            if (!card.Balance.HasValue || card.BalanceCurrency != currentMerchantInfo.CurrencyCode)
                try
                {
                    var balance = GetCardBalanceByCardNumberPinNumberRealTime(Encryption.DecryptCardNumber(card.Number),
                                                                              Encryption.DecryptPin(card.Pin), market,
                                                                              userMerchantInfo.CurrencyCode ==
                                                                              currentMerchantInfo.CurrencyCode);
                    if (balance != null)
                    {
                        card.BalanceDate = balance.BalanceDate;
                        card.BalanceCurrency = balance.BalanceCurrencyCode;
                        card.Balance = balance.Balance;
                    }
                }
                catch
                {
                    //eat this and return no balance info for the card
                }
            return card;
        }

        private void VoidCardIssuerReload(Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction reloadResult, IMerchantInfo merchantInfo)
        {
            try
            {
                _cardIssuer.VoidRequest(reloadResult, merchantInfo);
            }
            catch (Exception ex)
            {
                _logRepository.Log("APILog", "CardApi - _cardIssuer VoidRequest Throws Exception",
                    string.Format("Reload Card ID:{0} card; Exception:{1}", reloadResult.CardId, ex.ToString()),
                    TraceEventType.Error);
            }
        }

        private void ApplyVisibilityLevel(VisibilityLevel visibilityLevel, IEnumerable<ICard> cards)
        {
            switch (visibilityLevel)
            {
                case VisibilityLevel.Decrypted:
                    cards.ToList().ForEach(DecryptCard);
                    break;
                case VisibilityLevel.Masked:
                    cards.ToList().ForEach(MaskCard);
                    break;
                case VisibilityLevel.None:
                    cards.ToList().ForEach(NoneCard);
                    break;
            }
        }

        private void ApplyVisibilityLevel(VisibilityLevel visibilityLevel, ICard card)
        {
            switch (visibilityLevel)
            {
                case VisibilityLevel.Decrypted:
                    DecryptCard(card);
                    break;
                case VisibilityLevel.Masked:
                    MaskCard(card);
                    break;
                case VisibilityLevel.None:
                    NoneCard(card);
                    break;
            }
        }

        private void DecryptCard(ICard card)
        {
            card.Number = Encryption.DecryptCardNumber(card.Number);
            card.Pin = Encryption.DecryptPin(card.Pin);
        }

        private void MaskCard(ICard card)
        {
            var decryptedCard = Encryption.DecryptCardNumber(card.Number);
            card.Number = new string('x', decryptedCard.Length - 4) + decryptedCard.Substring(decryptedCard.Length - 4);
            card.Pin = string.Empty;
        }

        private void NoneCard(ICard card)
        {
            card.Number = string.Empty;
            card.Pin = string.Empty;
        }

        private void CleanNonOwnerCard(IAssociatedCard card)
        {
            if (card.IsOwner) return;
            card.Balance = 0;
            card.BalanceCurrency = string.Empty;
            card.BalanceDate = null;
            card.AutoReloadProfile = null;
        }

        public IEnumerable<IStarbucksCardStatus> GetStatusesForUserCards(string userId, IEnumerable<string> cardIds)
        {
            Validator.ValidateUserId(userId);

            var cards = cardIds as string[] ?? cardIds.ToArray();
            try
            {
                var cardStatuses = new List<StarbucksCardStatus>();
                var registeredCards = _cardDal.GetAssociatedCards(userId).Where(p => p.IsOwner);
                cardStatuses.AddRange(
                    registeredCards.Select(registeredCard =>
                                           new StarbucksCardStatus { CardId = registeredCard.CardId, Valid = true }));

                cardStatuses.AddRange(
                    cards.Except(cardStatuses.Select(cs => cs.CardId)).Select(
                        c => new StarbucksCardStatus { CardId = c, Valid = false }));

                return cardStatuses;
            }
            catch (Exception ex)
            {
                throw new CardProviderException(CardProviderErrorResource.UnknownErrorCode,
                                                CardProviderErrorResource.UnknownErrorMessage, ex);
            }
        }

        public string GetCardIdByCardNumber(string cardNumber)
        {
            return _cardDal.GetCardIdByCardNumber(cardNumber);
        }

        #endregion

        #region Balance

        public IStarbucksCardBalance GetCardBalance(string userId, string cardId, string market, string userMarket)
        {
            Validator.ValidateUserIdCardId(userId, cardId);

            IMerchantInfo userMerchantInfo;
            IMerchantInfo currentMerchantInfo;
            GetMerchantInfos(market, userMarket, UnKnownPlatform, out userMerchantInfo, out currentMerchantInfo);

            var card = GetDecryptedCardForInternalUse(userId, cardId, market ?? userMarket);
            DecryptCard(card);
            if (card.Balance.HasValue && card.BalanceCurrency == currentMerchantInfo.CurrencyCode)
            {
                return new CardBalance
                {
                    Balance = card.Balance,
                    BalanceDate = card.BalanceDate,
                    BalanceCurrencyCode = card.BalanceCurrency,
                    CardId = card.CardId,
                    CardNumber = card.Number
                };
            }
            return GetCardBalanceRealTime(userId, cardId, market, userMarket);
        }

        public IStarbucksCardBalance GetCardBalanceByCardNumberPinNumber(string cardNumber, string pin, string market)
        {
            Validator.ValidateCardAndPin(cardNumber, pin);
            //Will throw if cardNumber and Pin are invalid
            var card = GetDecryptedCardForInternalUseByNumber(cardNumber, pin);

            var bal = _cardDal.GetBalanceByNumber(card.Number);
            return bal == null
                       ? GetCardBalanceByCardNumberPinNumberRealTime(cardNumber, pin, market, false)
                       : new CardBalance
                       {
                           Balance = bal.Balance,
                           BalanceDate = bal.BalanceDate,
                           BalanceCurrencyCode = bal.Currency,
                           CardId = bal.CardId,
                           CardNumber = cardNumber
                       };
        }



        public IStarbucksCardBalance GetCardBalanceRealTime(string userId, string cardId, string market,
                                                            string userMarket)
        {
            var card = GetDecryptedCardForInternalUse(userId, cardId, market ?? userMarket);
            market = string.IsNullOrEmpty(market) ? userMarket : market;

            IMerchantInfo userMerchantInfo;
            IMerchantInfo currentMerchantInfo;
            GetMerchantInfos(market, userMarket, UnKnownPlatform, out userMerchantInfo, out currentMerchantInfo);

            return card == null
                       ? null
                       : GetCardBalanceByCardNumberPinNumberRealTime(card.Number, string.Empty, market,
                                                                     userMerchantInfo.CurrencyCode ==
                                                                     currentMerchantInfo.CurrencyCode);
        }

        public IStarbucksCardBalance GetCardBalanceByCardNumberPinNumberRealTime(string cardNumber, string pin,
                                                                                 string market)
        {
            return GetCardBalanceByCardNumberPinNumberRealTime(cardNumber, pin, market, false);
        }


        private IStarbucksCardBalance GetCardBalanceByCardNumberPinNumberRealTime(string cardNumber, string pin,
                                                                                  string market, bool updateCardBalance)
        {
            var mi = GetMerchantInfo(CurrentSubMarket(market));

            var valueLinkBalance = _cardIssuer.GetBalance(mi, cardNumber, pin);

            IStarbucksCardBalance starbucksBalance = null;
            if (valueLinkBalance == null)
                return null;
            if (!valueLinkBalance.TransactionSucceeded)
                throw HandleCardIssuerResponse(valueLinkBalance.ResponseCode);

            starbucksBalance = ConvertTransactionToBalance(cardNumber, valueLinkBalance, valueLinkBalance.CardId);
            if (updateCardBalance)
            {
                UpsertCardBalanceByDecryptedNumber(cardNumber, valueLinkBalance);
            }
            return starbucksBalance;
        }

        #endregion

        #region AutoReload

        public IAutoReloadProfile SetupAutoReload(string userId, string cardId,
                                                IAutoReloadProfile autoReloadProfile,
                                                string market, string email = null, IRisk risk = null)
        {
            var merchantInfo = GetMerchantInfo(market);

            if (autoReloadProfile.Status <= 0)
                autoReloadProfile.Status = (int)AutoReloadStatus.Active;

            if (autoReloadProfile.Status == (int)AutoReloadStatus.Active)
            {
                Validator.ValidateCreateAutoReload(autoReloadProfile);
                Validator.ValidateUserIdCardId(userId, cardId);
            }

            if (autoReloadProfile.Status != (int)AutoReloadStatus.Active)
            {
                if (string.IsNullOrEmpty(autoReloadProfile.AutoReloadType))
                    autoReloadProfile.AutoReloadType = "Amount";
            }

            var card = GetDecryptedCardForInternalUse(userId, cardId, market);
            if (!card.Actions.Contains(CardActions.AutoReload))
                throw new CardProviderException(CardProviderErrorResource.CannotAutoReloadMarketCode,
                                                CardProviderErrorResource.CannotAutoReloadMarketMessage);

            autoReloadProfile.UserId = userId;
            autoReloadProfile.CardId = cardId;

            if (string.IsNullOrEmpty(autoReloadProfile.AutoReloadId))
            {
                var arId = _cardDal.GetAutoReloadId(userId, cardId);
                autoReloadProfile.AutoReloadId = string.IsNullOrEmpty(arId)
                                                     ? Guid.NewGuid().ToString()
                                                     : arId;
            }

            FraudCheckOrZeroAuthForAutoReload(userId, cardId, autoReloadProfile.PaymentMethodId,
                                                            merchantInfo, autoReloadProfile.Amount, autoReloadProfile, email, market, risk);

            var autoReloadId = _cardDal.SetupAutoReload(new DalModels.AutoReloadProfile(autoReloadProfile));

            if (!string.IsNullOrEmpty(autoReloadProfile.PaymentMethodId))
                _cardDal.UpsertAutoReloadBilling(autoReloadProfile.UserId, autoReloadId,
                                                 autoReloadProfile.PaymentMethodId);

            return new AutoReloadProfile(_cardDal.GetAutoReload(autoReloadProfile.UserId, autoReloadId));
        }

        public IAutoReloadProfile UpdateAutoReload(string userId, string cardId,
                                                   IAutoReloadProfile autoReloadProfile,
                                                   string market, string email = null, IRisk risk = null)
        {
            var merchantInfo = GetMerchantInfo(market);

            var card = GetDecryptedCardForInternalUse(userId, cardId, market);
            if (card == null)
            {
                throw new CardProviderException(CardProviderErrorResource.CardNotFoundCode,
                                                CardProviderErrorResource.CardNotFoundMessage);
            }
            if (!IsCardOwned(userId, card))
            {
                throw new CardProviderException(CardProviderErrorResource.CardNotRegisteredToUserCode,
                                                CardProviderErrorResource.CardNotRegisteredToUserMessage);
            }

            if (autoReloadProfile.Status == (int)AutoReloadStatus.Disabled)
            {
                ReportFraudDataForDisableAutoReload(card, userId, market, null, risk);
            }
            else
            {
                FraudCheckOrZeroAuthForAutoReload(userId, cardId, autoReloadProfile.PaymentMethodId,
                                                merchantInfo, autoReloadProfile.Amount, autoReloadProfile, email, market, risk);
            }

            if (autoReloadProfile.Status <= 0)
                autoReloadProfile.Status = (int)AutoReloadStatus.Active;

            if (autoReloadProfile.Status == (int)AutoReloadStatus.Active)
            {
                Validator.ValidateCreateAutoReload(autoReloadProfile);
                Validator.ValidateUserIdCardId(userId, cardId);
            }

            if (autoReloadProfile.Status != (int)AutoReloadStatus.Active)
            {
                if (string.IsNullOrEmpty(autoReloadProfile.AutoReloadType))
                    autoReloadProfile.AutoReloadType = "Amount";
            }

            if (!card.Actions.Contains(CardActions.AutoReload))
                throw new CardProviderException(CardProviderErrorResource.CannotAutoReloadMarketCode,
                                                CardProviderErrorResource.CannotAutoReloadMarketMessage);

            if (string.IsNullOrEmpty(card.AutoReloadId))
            //try to find using other proc
            {
                card.AutoReloadId = _cardDal.GetAutoReloadId(userId, cardId);
            }

            if (string.IsNullOrEmpty(card.AutoReloadId))
            {
                throw new CardProviderException(CardProviderErrorResource.AutoReloadNotFoundCode,
                                                CardProviderErrorResource.AutoReloadNotFoundMessage);
            }

            autoReloadProfile.CardId = cardId;
            autoReloadProfile.UserId = userId;
            autoReloadProfile.AutoReloadId = card.AutoReloadId;

            _cardDal.UpdateAutoReload(new DalModels.AutoReloadProfile(autoReloadProfile));
            var message = string.Format("Amount based auto-reload profile saved. Amount = {0}",
                                        autoReloadProfile.Amount.ToString(CultureInfo.InvariantCulture));
            _cardDal.LogAutoReloadStatus(autoReloadProfile.UserId, autoReloadProfile.CardId,
                                         autoReloadProfile.AutoReloadId,
                                         GetAutoreloadStatus(autoReloadProfile.Status), message);

            if (!string.IsNullOrEmpty(autoReloadProfile.PaymentMethodId))
            {
                try
                {
                    _cardDal.UpsertAutoReloadBilling(autoReloadProfile.UserId, autoReloadProfile.AutoReloadId,
                                                     autoReloadProfile.PaymentMethodId);
                }
                catch
                {
                    autoReloadProfile.Status = (int)AutoReloadStatus.Temporary;
                    _cardDal.UpdateAutoReload(new DalModels.AutoReloadProfile(autoReloadProfile));
                    var billingMessage = string.Format("Auto-Reload status set to Temporary as billing Info could not be associated");
                    _cardDal.LogAutoReloadStatus(autoReloadProfile.UserId, autoReloadProfile.CardId,
                                                 autoReloadProfile.AutoReloadId, GetAutoreloadStatus(autoReloadProfile.Status), billingMessage);

                }
            }

            return
                new AutoReloadProfile(_cardDal.GetAutoReload(autoReloadProfile.UserId, autoReloadProfile.AutoReloadId));
        }

        public void EnableAutoReload(string userId, string cardId, string market, IRisk risk = null)
        {
            Validator.ValidateUserIdCardId(userId, cardId);
            var card = GetDecryptedCardForInternalUse(userId, cardId, market);

            if (!String.IsNullOrEmpty(card.AutoReloadId))
                card.AutoReloadProfile = GetAutoReload(userId, card.AutoReloadId);

            if (!card.Actions.Contains(CardActions.AutoReload))
                throw new CardProviderException(CardProviderErrorResource.CannotAutoReloadMarketCode,
                                                CardProviderErrorResource.CannotAutoReloadMarketMessage);

            if (string.IsNullOrEmpty(card.AutoReloadId) || card.AutoReloadProfile == null)
            {
                throw new CardProviderException(CardProviderErrorResource.AutoReloadNotFoundCode,
                                                CardProviderErrorResource.AutoReloadNotFoundMessage);
            }

            if (!_cardDal.IsAutoReloadAssociatedToPaymentMethod(userId, card.AutoReloadId))
            {
                throw new CardProviderException(CardProviderErrorResource.AutoReloadNotAssociatedToPaymentMethodCode,
                                                CardProviderErrorResource.AutoReloadNotAssociatedToPaymentMethodMessage);
            }

            // fraud check
            var merchantInfo = GetMerchantInfo(market);
            FraudCheckOrZeroAuthForAutoReload(userId, cardId, card.AutoReloadProfile.PaymentMethodId,
                                                            merchantInfo, card.AutoReloadProfile.Amount, card.AutoReloadProfile, null, market, risk);

            _cardDal.UpdateAutoReloadStatus(userId, card.AutoReloadId, (int)AutoReloadStatus.Active);

            string message = string.Format("Auto-Reload Enabled for user.");
            _cardDal.LogAutoReloadStatus(userId, string.Empty, card.AutoReloadId, AutoReloadStatus.Active.ToString(),
                                         message);

            //    IDictionary<string, string> parametersToLog = new Dictionary<string, string>();
            //    parametersToLog.Add("AutoReloadId", autoReloadId);
            //    parametersToLog.Add("AutoReloadStatus", AutoReloadStatus.Active.ToString());

            //    LogHelper.Log(userId, string.Empty, string.Empty, string.Empty, 0, string.Empty,
            //            LogMessages.Enable_AutoReload,
            //            parametersToLog, string.Empty, CardLogMessageCategory.Reload);
            //}
            //else
            //    throw new FaultException("Cannot enable AutoReload because there is no associated payment method.");
        }

        //Helper to get a card for purpose of validating ownership (enable/disable reload, unregister etc)
        private IAssociatedCard GetDecryptedCardForInternalUse(string userId, string cardId, string market)
        {
            var card = _cardDal.GetCard(cardId);
            if (card == null)
            {
                throw new CardProviderException(CardProviderErrorResource.CardNotFoundCode,
                                                CardProviderErrorResource.CardNotFoundMessage);
            }

            if (!userId.Equals(card.RegisteredUserId, StringComparison.InvariantCultureIgnoreCase))
            {
                throw new CardProviderException(CardProviderErrorResource.CardNotRegisteredToUserCode,
                                                CardProviderErrorResource.CardNotRegisteredToUserMessage);
            }
            DecryptCard(card);
            if (!String.IsNullOrEmpty(market))
                card.Actions = CardActions.GetValidActions(market,
                                                           _settingsProvider.GetCurrencyCodeForMarketCode(market),
                                                           card.Currency);
            return card;
        }

        private IAssociatedCard GetDecryptedCardForInternalUseByNumber(string cardNumber, string pin)
        {
            var card = _cardDal.GetCardByNumber(cardNumber);
            if (card == null)
                throw new CardProviderException(CardProviderErrorResource.CardNotFoundCode,
                                                CardProviderErrorResource.CardNotFoundMessage);
            DecryptCard(card);
            if (card.Pin != pin)
                throw new CardProviderException(CardProviderErrorResource.CardNotFoundCode,
                CardProviderErrorResource.CardNotFoundMessage);


            return card;
        }

        private IAssociatedCard GetAssociatedCard(string cardNumber, string cardPin, bool isPinRequired)
        {
            if (isPinRequired)
            {
                Validator.ValidateCardAndPin(cardNumber, cardPin);
            }
            else
            {
                Validator.ValidateCardNumber(cardNumber);
            }
            var card = _cardDal.GetCardByNumber(cardNumber);
            return card == null ? null : Card.Provider.Models.AssociatedCard.FromIAssociatedCard(card, _cardProviderSettingsSection);
        }

        private static bool IsCardOwned(string userId, IAssociatedCard card)
        {
            return card.RegisteredUserId != null &&
                   card.RegisteredUserId.Equals(userId, StringComparison.InvariantCultureIgnoreCase);
        }

        public void DisableAutoReload(string userId, string cardId, string market, IRisk risk = null)
        {
            Validator.ValidateUserIdCardId(userId, cardId);

            var card = GetDecryptedCardForInternalUse(userId, cardId, market);
            if (!card.Actions.Contains(CardActions.AutoReload))
                throw new CardProviderException(CardProviderErrorResource.CannotAutoReloadMarketCode,
                                                CardProviderErrorResource.CannotAutoReloadMarketMessage);

            if (string.IsNullOrEmpty(card.AutoReloadId))
            {
                throw new CardProviderException(CardProviderErrorResource.AutoReloadNotFoundCode,
                                                CardProviderErrorResource.AutoReloadNotFoundMessage);
            }

            var disableUntilDate = DateTime.Now.AddYears(100);
            _cardDal.DisableAutoReload(userId, card.AutoReloadId, disableUntilDate);

            string message = string.Format("Auto-Reload disabled for user until {0}.",
                                           disableUntilDate.ToShortDateString());
            _cardDal.LogAutoReloadStatus(userId, string.Empty, card.AutoReloadId, AutoReloadStatus.Disabled.ToString(),
                                         message);

            //IDictionary<string, string> parametersToLog = new Dictionary<string, string>();
            //parametersToLog.Add("AutoReloadId", autoReloadId);

            //LogHelper.Log(userId, string.Empty, string.Empty, string.Empty, 0, string.Empty,
            //        LogMessages.Disable_Autoreload,
            //        parametersToLog, string.Empty, CardLogMessageCategory.Reload);

            ReportFraudDataForDisableAutoReload(card, userId, market, null, risk);
        }

        #endregion

        #region UsedInOtherProviders

        public int UpsertCardBalanceByDecryptedNumber(string cardNumber, decimal balance, DateTime balanceDate,
                                                      string currency)
        {
            return _cardDal.UpsertCardBalanceByDecryptedNumber(cardNumber, balance, balanceDate, currency);
        }

        public int UpsertCardBalance(string cardId, decimal balance, DateTime balanceDate, string currency)
        {
            return _cardDal.UpsertCardBalance(cardId, balance, balanceDate, currency);
        }

        public int UpsertCardTransactionHistory(ICardTransaction cardTransaction)
        {
            return _cardDal.UpsertCardTransactionHistory(cardTransaction);
        }

        public int UpsertCardTransactionHistory(string cardNumber, ICardTransaction cardTransaction)
        {
            return _cardDal.UpsertCardTransactionHistory(cardNumber, cardTransaction);
        }

        #endregion

        #region UpdateCard

        public void UpdateCardNickname(string userId, string cardId, string nickname)
        {
            Validator.ValidateUserIdCardId(userId, cardId);
            Validator.ValidateNickname(nickname);
            _cardDal.UpdateCardNickName(userId, cardId, nickname);
        }

        public void UpdateDefaultCard(string userId, string cardId)
        {
            Validator.ValidateUserIdCardId(userId, cardId);
            var card = GetDecryptedCardForInternalUse(userId, cardId, string.Empty);
            if (card == null)
                throw new CardProviderException(CardProviderErrorResource.CardNotFoundCode,
                                                CardProviderErrorResource.CardNotFoundMessage);
            _cardDal.UpdateCardAssociation(userId, cardId, null, true, null);
        }

        #endregion

        #region UnregisterCard

        public bool UnregisterCard(string userId, string cardId, bool keepInLibrary, string userMarket, IRisk risk = null)
        {
            Validator.ValidateUserIdCardId(userId, cardId);

            var card = GetDecryptedCardForInternalUse(userId, cardId, string.Empty);
            DecryptCard(card);

            if (card.IsDigitalCard)
            {
                var balance = _cardIssuer.GetBalance(GetMerchantInfo(card.SubMarketCode), card.Number, card.Pin);

                if (balance.BeginningBalance > 0)
                {
                    throw new CardProviderException(CardProviderErrorResource.UnregisterNonZeroBalanceCode,
                                                    CardProviderErrorResource.UnregisterNonZeroBalanceMessage);
                }
            }

            if (keepInLibrary)
            {
                _cardDal.UnregisterCard(userId, cardId);
            }
            else
            {
                _cardDal.DeleteCardAssociation(userId, cardId);
            }

            ReportFraudDataForRemoveCard(card, userId, userMarket, null, risk);

            if (card.IsDefault)
            {
                SetDefaultNextRegisteredCard(userId, cardId);
            }

            //Remove from loyalty host
            //
            var data = new List<KeyValuePair<string, object>>
                {
                    new KeyValuePair<string, object>("action", "unregister"),
                    new KeyValuePair<string, object>("userid", userId),
                    new KeyValuePair<string, object>("cardid", cardId),
                };
            try
            {
                _logRepository.Log("APILog", "CardApi - Unregister",
                                  string.Format("Unregister Card UserID:{0} CardId:{1}", userId, cardId),
                                  TraceEventType.Information);

                _loyaltyHostListener.Handle(new Starbucks.MessageBroker.Common.Models.LoyaltyHostServiceMessage() { Data = data });
            }
            catch (Exception ex)
            {
                _logRepository.Log("APILog", "CardApi - UnregisterException",
                                 string.Format("Register Card UserID:{0} CardId:{1} Exception:{2}", userId, cardId, ex),
                                 TraceEventType.Error);
            }

            return true;
        }

        private void SetDefaultNextRegisteredCard(string userId, string cardId)
        {
            var cards = _cardDal.GetAssociatedCards(userId);
            var nextDefault =
                cards.Where(
                    p => p.CardId != cardId && IsCardOwned(userId, p))
                     .OrderByDescending(p => p.RegistrationDate)
                     .FirstOrDefault();

            if (nextDefault != null && !string.IsNullOrEmpty(nextDefault.CardId))
                UpdateDefaultCard(userId, nextDefault.CardId);
        }

        #endregion

        #region Transfer

        private IEnumerable<IStarbucksCardBalance> TransferRegisteredCardBalanceToAssociatedCard(string userId,
                                                                                                 string sourceCardId,
                                                                                                 string destinationCardId,
                                                                                                 string userMarket,
                                                                                                 decimal? amount,
                                                                                                 string email = null,
                                                                                                 IRisk risk = null)
        {
            Validator.ValidateTransferBalance(userId, sourceCardId, destinationCardId);

            var rules = new Rules();

            var cardFrom = GetDecryptedCardForInternalUse(userId, sourceCardId, userMarket);
            if (cardFrom == null)
                throw new CardProviderException(CardProviderErrorResource.CardNotFoundCode,
                                                CardProviderErrorResource.CardNotFoundMessage);
            if (!rules.CanTransferFrom(cardFrom.Class))
                throw new CardProviderException(CardProviderErrorResource.CannotTransferFromCardClassCode,
                                                CardProviderErrorResource.CannotTransferFromCardClassMessage);

            if (!cardFrom.Actions.Contains(CardActions.Transfer))
                throw new CardProviderException(CardProviderErrorResource.CannotTransferMarketCode,
                                                CardProviderErrorResource.CannotTransferMarketMessage);

            var cardTo = GetDecryptedCardForInternalUse(userId, destinationCardId, userMarket);
            if (cardTo == null)
                throw new CardProviderException(CardProviderErrorResource.CardNotFoundCode,
                                                CardProviderErrorResource.CardNotFoundMessage);
            if (!rules.CanTransferTo(cardTo.Class))
                throw new CardProviderException(CardProviderErrorResource.CannotTransferToCardClassCode,
                                                CardProviderErrorResource.CannotTransferToCardClassMessage);

            if (!cardTo.Actions.Contains(CardActions.Transfer))
                throw new CardProviderException(CardProviderErrorResource.CannotTransferMarketCode,
                                                CardProviderErrorResource.CannotTransferMarketMessage);

            var result = TransferCardBalance(userId, cardFrom, cardTo, CurrentSubMarket(userMarket), email, risk);

            return result;
        }


        public IEnumerable<IStarbucksCardBalance> TransferRegistered(string userId, string sourceCardId,
                                                                     string targetCardId, decimal? amount,
                                                                     string userMarket, string email = null, IRisk risk = null)
        {
            if (string.IsNullOrEmpty(sourceCardId))
                throw new CardProviderValidationException(
                    CardProviderValidationErrorResource.ValidationNoSourceCardIdCode,
                    CardProviderValidationErrorResource.ValidationNoSourceCardIdMessage);
            if (string.IsNullOrEmpty(targetCardId))
                throw new CardProviderValidationException(
                    CardProviderValidationErrorResource.ValidationNoDestinationCardIdCode,
                    CardProviderValidationErrorResource.ValidationNoDestinationCardIdMessage);

            if (amount.HasValue && amount.Value <= 0)
                throw new CardProviderValidationException(
                    CardProviderValidationErrorResource.CannotTransferABalanceAmountOfZeroCode,
                    CardProviderValidationErrorResource.CannotTransferABalanceAmountOfZeroMessage);


            return amount.HasValue && amount.Value > 0
                       ? TransferRegisteredCardAmountToAssociatedCard(userId, sourceCardId, targetCardId, amount,
                                                                      userMarket, email, risk)
                       : TransferRegisteredCardBalanceToAssociatedCard(userId, sourceCardId, targetCardId, userMarket, amount, email, risk);
        }

        public void UpsertPendingTip(IPendingTip tip)
        {
            throw new NotImplementedException();
        }

        private decimal GetMaxBalanceByMarket(string market)
        {
            return _settingsProvider.GetMaxBalanceByMarketCode(market);
        }

        private IEnumerable<IStarbucksCardBalance> TransferCardBalance(string userId,
                                                                       IAssociatedCard cardFrom,
                                                                       IAssociatedCard cardTo,
                                                                       string submarket, string email = null,
                                                                       IRisk risk = null)
        {
            string cardNumberFrom = cardFrom.Number;
            string cardPinFrom = cardFrom.Pin;
            string carIdFrom = cardFrom.CardId;
            string cardNumberTo = cardTo.Number;
            string cardPinTo = cardTo.Pin;

            //1. get MID and AltMID based on submarket
            var merchantInfo = GetMerchantInfo(submarket);

            if (IsPossiblePosFraud(merchantInfo, cardNumberFrom, submarket))
            {
                _logCounterManager.Increment("Card_TransferStarbucksCardBalance_Error");
                throw new CardProviderException(CardProviderErrorResource.CannotTransferAfterReloadCode, CardProviderErrorResource.CannotTransferAfterReloadMessage);
            }

            //get the balance of the from card in the correct currency(ie, transferring from US to CAN) for balance and history update later
            //var fromBalance = _cardIssuer.GetBalance(merchantInfo, cardNumberFrom, null);
            //if (!fromBalance.TransactionSucceeded)
            //{
            //    var exception = HandleCardIssuerResponse(fromBalance.ResponseCode);
            //    if (exception != null)
            //        throw exception;
            //}
            var fromBalance = GetCardBalanceByCardNumberPinNumber(cardNumberFrom, cardPinFrom, submarket);
            var toBalance = GetCardBalanceByCardNumberPinNumber(cardNumberTo, cardPinTo, submarket);
            var maxBalance = GetMaxBalanceByMarket(submarket);

            //kick out to do a transfer amount
            if (fromBalance.Balance + toBalance.Balance > maxBalance)
            {
                var transferAmount = maxBalance - toBalance.Balance;
                return TransferAmount(userId, cardFrom, cardTo, transferAmount.GetValueOrDefault(), submarket, email, risk);
            }

            FraudCheckForTransfer(userId, cardFrom, cardTo, fromBalance.Balance.GetValueOrDefault(), merchantInfo, email, submarket, risk);

            Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction result = null;
            try
            {
                result = _cardIssuer.TransferBalance(merchantInfo, cardNumberFrom, cardNumberTo);
            }
            catch (Exception ex)
            {
                _logRepository.Log("APILog", "CardApi - _cardIssuer TransferBalance Throws Exception",
                    string.Format("Transfer failed in card issuer transferBalance() for User {0} from card Id:{1} to card Id:{2}. Exception Occurred:{3}", userId, cardNumberFrom, cardNumberTo, ex.ToString()),
                    TraceEventType.Error);

                throw;
            }

            //Handle VL response.
            if (!result.TransactionSucceeded)
            {
                var exception = HandleCardIssuerResponse(result.ResponseCode);
                if (exception != null)
                    throw exception;
            }

            var fromTransaction = new CardIssuerModels.CardTransaction();
            fromTransaction.CardId = carIdFrom;
            //Update transaction history for the first card with the result.
            fromTransaction.Amount = fromBalance.Balance.GetValueOrDefault() * -1;
            fromTransaction.Description = string.Format("Transfer balance to card {0}",
                                                    cardNumberTo.Substring(cardNumberTo.Length - 4));
            fromTransaction.EndingBalance = 0M;
            fromTransaction.Currency = fromBalance.BalanceCurrencyCode;
            fromTransaction.AuthorizationCode = result.AuthorizationCode;
            fromTransaction.TransactionId = result.TransactionId;
            fromTransaction.TransactionDate = result.TransactionDate;
            fromTransaction.RequestCode = result.RequestCode;
            fromTransaction.LockAmount = result.LockAmount;
            UpsertCardTransactionHistory(cardNumberFrom, new CardTransaction(fromTransaction));

            //Update the balance on the first card to 0, since the amount was transferred.
            UpsertCardBalanceByDecryptedNumber(cardNumberFrom, 0M, result.TransactionDate, fromBalance.BalanceCurrencyCode);

            //Update transaction history for the second card.
            result.Amount = result.EndingBalance - result.BeginningBalance;
            result.Description = string.Format("Transfer balance from card {0}",
                                               cardNumberFrom.Substring(cardNumberFrom.Length - 4));
            UpsertCardTransactionHistory(cardNumberTo, new CardTransaction(result));
            //Update the balance on the second card.
            UpsertCardBalanceByDecryptedNumber(cardNumberTo, result);

            var balanceFrom = new CardBalance
            {
                CardNumber = cardNumberFrom,
                Balance = 0M,
                BalanceCurrencyCode = fromBalance.BalanceCurrencyCode,
                BalanceDate = result.TransactionDate,
            };
            var balanceTo = new CardBalance
            {
                CardNumber = cardNumberTo,
                Balance = result.EndingBalance,
                BalanceCurrencyCode = result.Currency,
                BalanceDate = result.TransactionDate,
            };

            SendTransferConfirmationEmail(submarket, fromBalance.Balance.GetValueOrDefault(), cardNumberFrom, cardNumberTo);
            return new List<IStarbucksCardBalance> { balanceFrom, balanceTo };
        }

        private IEnumerable<IStarbucksCardBalance> TransferRegisteredCardAmountToAssociatedCard(string userId,
                                                                                                string sourceCardId,
                                                                                                string destinationCardId,
                                                                                                decimal? amount,
                                                                                                string market,
                                                                                                string email = null,
                                                                                                IRisk risk = null)
        {

            Validator.ValidateTransferBalance(userId, sourceCardId, destinationCardId);

            var rules = new Rules();

            var cardFrom = GetDecryptedCardForInternalUse(userId, sourceCardId, market);

            if (cardFrom == null)
                throw new CardProviderException(CardProviderErrorResource.CardNotFoundCode,
                                                CardProviderErrorResource.CardNotFoundMessage);
            if (!rules.CanTransferFrom(cardFrom.Class))
                throw new CardProviderException(CardProviderErrorResource.CannotTransferFromCardClassCode,
                                                CardProviderErrorResource.CannotTransferFromCardClassMessage);

            if (!cardFrom.Actions.Contains(CardActions.Transfer))
                throw new CardProviderException(CardProviderErrorResource.CannotTransferMarketCode,
                                                CardProviderErrorResource.CannotTransferMarketMessage);

            var cardTo = GetDecryptedCardForInternalUse(userId, destinationCardId, market);
            if (cardTo == null)
                throw new CardProviderException(CardProviderErrorResource.CardNotFoundCode,
                                                CardProviderErrorResource.CardNotFoundMessage);
            if (!rules.CanTransferTo(cardTo.Class))
                throw new CardProviderException(CardProviderErrorResource.CannotTransferToCardClassCode,
                                                CardProviderErrorResource.CannotTransferToCardClassMessage);

            cardTo.Actions = CardActions.GetValidActions(market, _settingsProvider.GetCurrencyCodeForMarketCode(market), cardTo.Currency);
            if (!cardTo.Actions.Contains(CardActions.Transfer))
                throw new CardProviderException(CardProviderErrorResource.CannotTransferMarketCode,
                                                CardProviderErrorResource.CannotTransferMarketMessage);

            return TransferAmount(userId, cardFrom, cardTo, amount.GetValueOrDefault(), market, email, risk);
        }

        private static Api.Sdk.PaymentService.Models.TransferBalanceDetails GetTransferBalanceDetails(
            string userId,
            IAssociatedCard cardFrom,
            IAssociatedCard cardTo)
        {
            if (cardFrom == null)
            {
                throw new CardProviderValidationException(
                    CardProviderValidationErrorResource.ValidationNoSourceCardIdCode,
                    CardProviderValidationErrorResource.ValidationNoSourceCardIdMessage);
            }

            if (cardTo == null)
            {
                throw new CardProviderValidationException(
                    CardProviderValidationErrorResource.ValidationNoDestinationCardIdCode,
                    CardProviderValidationErrorResource.ValidationNoDestinationCardIdMessage);
            }

            return new Api.Sdk.PaymentService.Models.TransferBalanceDetails()
            {
                SourceCardBalance = cardFrom.Balance,
                SourceCardId = cardFrom.CardId,
                SourceCardNumber = cardFrom.Number,
                TargetCardBalance = cardTo.Balance,
                TargetCardId = cardTo.CardId,
                TargetCardNumber = cardTo.Number,
                TargetCardIsOwner = IsCardOwned(userId, cardTo)
            };
        }

        private static Api.Sdk.PaymentService.Models.StarbucksCardDetails
            GetPaymentServiceApiSdkCard(string userId, IAutoReloadProfile autoReloadProfile, IAssociatedCard card, string CurrencyCode, string platform)
        {
            if (card == null)
            {
                return null;
            }

            Api.Sdk.PaymentService.Models.StarbucksCardDetails starbucksCardDetails = new Api.Sdk.PaymentService.Models.StarbucksCardDetails()
            {
                CardId = card.CardId,
                CardNumber = card.Number,
                CurrencyCode = card.BalanceCurrency,
                CardNickname = card.Nickname,
                Balance = card.Balance,
                CardType = card.Type.ToString(),
                CardMarket = card.SubMarketCode,
                MarketingRegSource = card.MarketingRegSourceCode,
                PlatformRegSource = card.PlatformRegSourceCode,
                IsPartner = card.IsPartner,
                IsDigital = card.IsDigitalCard,
                IsOwner = IsCardOwned(userId, card)
            };

            if (autoReloadProfile != null)
            {
                Starbucks.Api.Sdk.PaymentService.Models.AutoReloadProfile reloadProfile = new Starbucks.Api.Sdk.PaymentService.Models.AutoReloadProfile();
                reloadProfile.AutoReloadAmount = autoReloadProfile.Amount;
                reloadProfile.AutoReloadId = autoReloadProfile.AutoReloadId;
                reloadProfile.AutoReloadType = autoReloadProfile.AutoReloadType;
                reloadProfile.Day = autoReloadProfile.Day;
                reloadProfile.DisableUntilDate = autoReloadProfile.DisableUntilDate;
                reloadProfile.PaymentMethodId = autoReloadProfile.PaymentMethodId;
                reloadProfile.Status = autoReloadProfile.Status.ToString();
                reloadProfile.StoppedDate = autoReloadProfile.StoppedDate;
                reloadProfile.TriggerAmount = autoReloadProfile.TriggerAmount;

                starbucksCardDetails.AutoReloadProfile = reloadProfile;
            }

            return starbucksCardDetails;
        }

        public DeviceReputationData ToPaymentServiceApiSdkReputation(Starbucks.Card.Provider.Common.Models.IReputation iovationFields)
        {
            if (iovationFields == null)
            {
                return null;
            }

            return new DeviceReputationData()
            {
                DeviceFingerprint = iovationFields.DeviceFingerprint,
                IpAddress = iovationFields.IpAddress
            };
        }

        public static List<OrderItem> CreateOrderForFraudCheck(string category, decimal? amount)
        {
            decimal amountValue = amount.HasValue ? amount.Value : 0;
            var orderItems = new List<OrderItem>()
                {
                    new OrderItem()
                        {
                            Sku = null,
                            Category = category,
                            Name = "sbuxreld",
                            Quantity = 1,
                            Amount = amountValue,
                        },
                };
            return orderItems;
        }

        private IPaymentMethod GetPaymentMethod(string userId, string paymentMethodId)
        {
            if (string.IsNullOrEmpty(paymentMethodId) || _paymentMethodProvider == null || _accountProvider == null)
                return null;

            IPaymentMethod paymentMethod = String.IsNullOrEmpty(userId) ? _paymentMethodProvider.GetPaymentMethod(paymentMethodId) : _paymentMethodProvider.GetPaymentMethod(userId, paymentMethodId);
            if (paymentMethod == null)
            {
                return null;
            }

            return paymentMethod;
        }

        private IAccountAddress GetAddressFromAccountProvider(string userId, string billingAddressId)
        {
            IAccountAddress billingAddress = _accountProvider.GetAddress(userId, billingAddressId);
            return billingAddress;
        }

        private Starbucks.Api.Sdk.PaymentService.Models.Address GetAddress(string userId, IPaymentMethod paymentMethod)
        {
            if (paymentMethod == null)
            {
                return null;
            }

            var billingAddress = GetAddressFromAccountProvider(userId, paymentMethod.BillingAddressId);
            if (billingAddress == null)
            {
                _logRepository.Log("APILog", "CardApi - AutoReloadSettings_ZeoAuth_GetAddress",
                    string.Format("Cannot get address for user Id: {0}, address Id:{1}", userId, paymentMethod.BillingAddressId),
                    TraceEventType.Error);

                return null;
            }

            return new Starbucks.Api.Sdk.PaymentService.Models.Address()
            {
                AddressLine1 = billingAddress.Line1,
                AddressLine2 = billingAddress.Line2,
                City = billingAddress.City,
                Country = billingAddress.Country,
                CountrySubdivision = billingAddress.CountrySubdivision,
                FirstName = billingAddress.FirstName,
                LastName = billingAddress.LastName,
                PhoneNumber = billingAddress.PhoneNumber,
                PostalCode = billingAddress.PostalCode
            };
        }

        private BillingInfo CreateFraudCheckBillingInfoForTransfer(string email)
        {
            BillingInfo fraudCheckBillingInfo = new BillingInfo()
            {
                Email = email,
                PaymentMethod = null
            };

            return fraudCheckBillingInfo;
        }

        private SdkModels.PayPalDetails GetPayPalDetails(BillingInfo billingInfo, IPaymentMethod paymentMethod, IMerchantInfo merchantInfo)
        {
            if (paymentMethod == null)
                return null;

            SdkModels.PayPalDetails details = new SdkModels.PayPalDetails();

            var billingAgreementRequest = new BillingAgreementUpdateRequest()
            {
                Currency = merchantInfo.CurrencyCode,
                ReferenceId = Encryption.DecryptCardNumber(paymentMethod.AccountNumber)
            };

            IBillingAgreementUdpateResponse billingAgreementInfoResponse;
            try
            {
                billingAgreementInfoResponse = _securePayPalPayment.UpdateBillingAgreement(billingAgreementRequest);
            }
            catch (Exception ex)
            {
                _logRepository.Log("APILog", "CardApi - UpdateBillingAgreement",
                    string.Format("Currency: {0}, ReferenceId: {1}, Exception: {2}", merchantInfo.CurrencyCode, paymentMethod.AccountNumber, ex.ToString()), TraceEventType.Warning);

                return null;
            }

            if (billingAgreementInfoResponse != null)
            {
                details.BillAgreementId = billingAgreementInfoResponse.BillingAgreementId;
                details.PayPalPayerId = (billingAgreementInfoResponse.PayerInformation != null) ? billingAgreementInfoResponse.PayerInformation.PayerId : string.Empty;
                details.PayPayEmail = (billingAgreementInfoResponse.PayerInformation != null) ? billingAgreementInfoResponse.PayerInformation.Payer : string.Empty;
                details.PayPayFullName = (billingAgreementInfoResponse.PayerInformation != null) ? billingAgreementInfoResponse.PayerInformation.FirstName + " " + billingAgreementInfoResponse.PayerInformation.LastName : string.Empty;
                details.PendingReason = string.Empty;
                if (billingAgreementInfoResponse.BillingAddress != null)
                {
                    var addressInfo = new Starbucks.Api.Sdk.PaymentService.Models.Address()
                    {
                        Country = billingAgreementInfoResponse.BillingAddress.Country,
                        CountrySubdivision = billingAgreementInfoResponse.BillingAddress.StateOrProvince,
                        City = billingAgreementInfoResponse.BillingAddress.City,
                        AddressLine1 = billingAgreementInfoResponse.BillingAddress.Street1,
                        AddressLine2 = billingAgreementInfoResponse.BillingAddress.Street2,
                        PostalCode = billingAgreementInfoResponse.BillingAddress.PostalCode
                    };
                    billingInfo.AddressInfo = new AddressInfo() { AddressDetails = addressInfo };
                }
            }

            billingInfo.PaymentMethod.PayPalDetails = details;

            return details;
        }

        private BillingInfo CreateFraudCheckPaypalBillingInfoForAutoReloadSettings(string userId, IPaymentMethod paymentMethod, string email, IMerchantInfo merchantInfo)
        {
            BillingInfo billingInfo = new BillingInfo();

            SdkModels.PaymentMethod sdkPaymentMethod = new SdkModels.PaymentMethod();
            sdkPaymentMethod.PaymentId = paymentMethod.PaymentMethodId;

            billingInfo.Email = email;
            billingInfo.PaymentMethod = sdkPaymentMethod;

            sdkPaymentMethod.PayPalDetails = GetPayPalDetails(billingInfo, paymentMethod, merchantInfo);

            return billingInfo;
        }

        private BillingInfo CreateFraudCheckBillingInfoForAutoReloadSettings(string userId, IPaymentMethod paymentmethod, string email, IMerchantInfo merchantInfo)
        {
            Starbucks.Api.Sdk.PaymentService.Models.Address address = GetAddress(userId, paymentmethod);
            if (address == null)
            {
                return null;
            }

            BillingInfo fraudCheckBillingInfo = new BillingInfo()
            {
                Email = email,
                AddressInfo = new AddressInfo()
                {
                    AddressDetails = address,
                    AddressId = paymentmethod.BillingAddressId
                },
            };

            if (paymentmethod != null)
            {
                fraudCheckBillingInfo.PaymentMethod = new Starbucks.Api.Sdk.PaymentService.Models.PaymentMethod();
                fraudCheckBillingInfo.PaymentMethod.CreditCardDetails = new CreditCardDetails();
                fraudCheckBillingInfo.PaymentMethod.CreditCardDetails.SurrogateNumber = paymentmethod.SurrogateAccountNumber;
                fraudCheckBillingInfo.PaymentMethod.CreditCardDetails.Cvn = paymentmethod.Cvn;
                fraudCheckBillingInfo.PaymentMethod.CreditCardDetails.ExpireMonth = paymentmethod.ExpirationMonth;
                fraudCheckBillingInfo.PaymentMethod.CreditCardDetails.ExpireYear = paymentmethod.ExpirationYear;
                fraudCheckBillingInfo.PaymentMethod.CreditCardDetails.FullName = paymentmethod.FullName;
                fraudCheckBillingInfo.PaymentMethod.CreditCardDetails.Type = (CreditCardTypes)paymentmethod.PaymentTypeId;
                fraudCheckBillingInfo.PaymentMethod.PaymentId = paymentmethod.PaymentMethodId;
            }

            return fraudCheckBillingInfo;
        }

        private void FraudCheckOrZeroAuthForAutoReload(string userId, string cardId, string paymentMethodId, IMerchantInfo merchantInfo, decimal amount,
            IAutoReloadProfile autoReloadProfile, string emailAddress, string submarket, IRisk risk)
        {
            IPaymentMethod paymentMethod = GetPaymentMethod(userId, paymentMethodId);
            if (paymentMethod == null || paymentMethod.IsTemporary)
            {
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationNoPaymentMethodCode,
                                                CardProviderValidationErrorResource.ValidationNoPaymentMethodMessage);
            }

            if (string.IsNullOrEmpty(emailAddress))
            {
                var user = _accountProvider.GetUser(userId);
                emailAddress = user.EmailAddress;
                if (string.IsNullOrEmpty(emailAddress))
                {
                    _logRepository.Log("APILog", "CardApi - FraudCheck_ZeroAuth",
                        string.Format("No Email Found UserID: {0}", userId), TraceEventType.Warning);
                    return;
                }
            }

            bool CheckResultSucceeded;
            if (paymentMethod.PaymentTypeId == (int)PaymentType.PayPal) // paypal, then perform fraud check
            {
                CheckResultSucceeded = FraudCheckForAutoReloadSettings(userId, cardId, paymentMethod, merchantInfo, amount,
                    autoReloadProfile, emailAddress, submarket, risk);
            }
            else
            {
                CheckResultSucceeded = ZeroCheckForAutoReloadSettings(userId, cardId, paymentMethod, merchantInfo, amount,
                    autoReloadProfile, emailAddress, submarket, risk);
            }

            if (!CheckResultSucceeded)
            {
                _logCounterManager.Increment("Card_TransferStarbucksCardBalance_Request_Denied");

                _logRepository.Log("APILog", "CardApi - Request Denied",
                    string.Format("Request Denied for PaymentTypeId {0}, user Id: {1}, card Id:{2}",
                                                                            paymentMethod.PaymentTypeId,
                                                                            userId,
                                                                            cardId),
                                                                            TraceEventType.Error);

                throw new CardProviderException(CardProviderErrorResource.RequestDeniedCode,
                                                CardProviderErrorResource.RequestDeniedMessage);
            }
        }

        private bool ZeroCheckForAutoReloadSettings(string userId, string cardId, IPaymentMethod paymentMethod, IMerchantInfo merchantInfo, decimal amount,
            IAutoReloadProfile autoReloadProfile, string emailAddress, string submarket, IRisk risk)
        {
            var enableZeroAuth = true;
            bool.TryParse(ConfigurationManager.AppSettings["EnableZeroAuth"], out enableZeroAuth);
            if (!enableZeroAuth)
            {
                return true;
            }

            if (risk == null)
            {
                return true;
            }

            Validator.ValidateCardId(cardId);
            Validator.ValidateUserId(userId);

            var card = _cardDal.GetCard(userId, cardId, true);
            if (card == null)
            {
                _logRepository.Log("APILog", "CardApi - AutoReloadSettings_ZeoAuth_GetCard",
                    string.Format("Cannot get card for user Id: {0}, Card Id:{1}", userId, cardId),
                    TraceEventType.Error);
            }

            BillingInfo billingInfo = CreateFraudCheckBillingInfoForAutoReloadSettings(userId, paymentMethod, emailAddress, merchantInfo);
            if (billingInfo == null)
            {
                // already logged when getting address from account provider
                return false; // no need to call PS API since no billing info and zero auth will fail. 
            }

            ZeroRequest zeroAuthRequest = null;
            try
            {
                string currencyCode = merchantInfo == null ? null : merchantInfo.CurrencyCode;
                zeroAuthRequest = new ZeroRequest()
                {
                    CurrencyCode = currencyCode,
                    TransactionType = TransactionTypes.ChangeAutoReloadSettings,
                    Platform = risk.Platform,
                    Market = !string.IsNullOrWhiteSpace(risk.Market) ? risk.Market : submarket,
                    BillingInfo = billingInfo,

                    Risk = new ZeroAuthRiskInfo()
                    {
                        UserId = userId,
                        OrderMessage = null,
                        CCAgentName = risk.CcAgentName,
                        // Set our default logged in state if none was specified.
                        SignedIn = (risk.IsLoggedIn == null) ? DefaultIsLoggedInValue : risk.IsLoggedIn.ToString(),

                        StarbucksCardDetails = GetPaymentServiceApiSdkCard(userId, autoReloadProfile, card, currencyCode, risk.Platform),
                        DeviceReputation = ToPaymentServiceApiSdkReputation(risk.IovationFields)
                    }
                };

                IZeroAuthResult zeroAuthResult = null;
                try
                {
                    zeroAuthResult = _paymentServiceApiClient.ZeroAuth(zeroAuthRequest);
                    LogPSApiCallDetails.LogZeroAuthResult(zeroAuthRequest, zeroAuthResult, _logRepository);
                }
                catch (Exception ex)
                {
                    // not deny, still proceed
                    LogPSApiCallDetails.LogZeroAuthException(zeroAuthRequest, ex, _logRepository);
                    _logCounterManager.Increment("CardProvider_AutoReload_FraudCheck_failed_with_exception");
                }

                if (zeroAuthResult != null && zeroAuthResult.DecisionType == DecisionType.Deny)
                {
                    // only deby the request when PS API denies the request
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logCounterManager.Increment("Card_AutoReloadSettings_Error");
                _logRepository.Log("APILog", "CardApi - AutoReloadSettings ZeoAuth Exception",
                    string.Format("AutoReloadSettings ZeoAuth for user Id: {0}, card Id:{1} Exception:{2}", userId, card.CardId, ex.ToString()),
                    TraceEventType.Error);
            }

            return true;
        }

        private bool FraudCheckForAutoReloadSettings(string userId, string cardId, IPaymentMethod paymentMethod, IMerchantInfo merchantInfo, decimal amount,
            IAutoReloadProfile autoReloadProfile, string emailAddress, string submarket, IRisk risk)
        {
            var enableFraudCheck = true;
            bool.TryParse(ConfigurationManager.AppSettings["EnableFraudCheck"], out enableFraudCheck);
            if (!enableFraudCheck)
            {
                return true;
            }

            if (risk == null)
            {
                return true;
            }

            Validator.ValidateCardId(cardId);
            Validator.ValidateUserId(userId);

            var card = _cardDal.GetCard(userId, cardId, true);
            if (card == null)  // if cannot get card detail, still proceed
            {
                _logRepository.Log("APILog", "CardApi - AutoReloadSettings_FraudCheck_GetCard",
                    string.Format("Cannot get card for user Id: {0}, Card Id:{1}", userId, cardId),
                    TraceEventType.Error);
            }

            FraudCheckRequest fraudCheckRequest = null;
            try
            {
                fraudCheckRequest = new FraudCheckRequest()
                {
                    TransactionId = Guid.NewGuid().ToString(),
                    TransactionType = TransactionTypes.ChangeAutoReloadSettings,
                    Platform = risk.Platform,
                    Market = !string.IsNullOrWhiteSpace(risk.Market) ? risk.Market : submarket,
                    CurrencyCode = merchantInfo.CurrencyCode,
                    BillingInfo = CreateFraudCheckPaypalBillingInfoForAutoReloadSettings(userId, paymentMethod, emailAddress, merchantInfo),
                    Basket = CreateOrderForFraudCheck("SVC", amount),
                    Shipping = null,

                    Risk = new FraudCheckRiskInfo()
                    {
                        UserId = userId,
                        OrderMessage = null,
                        CCAgentName = risk.CcAgentName,
                        // Set our default logged in state if none was specified.
                        SignedIn = (risk.IsLoggedIn == null) ? DefaultIsLoggedInValue : risk.IsLoggedIn.ToString(),

                        StarbucksCardDetails = GetPaymentServiceApiSdkCard(userId, autoReloadProfile, card, merchantInfo.CurrencyCode, risk.Platform),
                        DeviceReputation = ToPaymentServiceApiSdkReputation(risk.IovationFields)
                    }
                };

                IFraudCheckResult fraudCheckResult = null;
                try
                {
                    fraudCheckResult = _paymentServiceApiClient.FraudCheck(fraudCheckRequest);
                    LogPSApiCallDetails.LogFraudCheckResult(fraudCheckRequest, fraudCheckResult, _logRepository, "CardTransactionPaypalFraudCheckResult", "AutoReloadSettingPaypalFraudCheck");
                }
                catch (Exception ex)
                {
                    // not deny, still proceed
                    LogPSApiCallDetails.LogFraudCheckException(fraudCheckRequest, ex, _logRepository, "CardTransactionPaypalFraudCheckException", "AutoReloadSettingPaypalFraudCheck");
                    _logCounterManager.Increment("CardProvider_AutoReload_FraudCheck_failed_with_exception");
                }

                if (fraudCheckResult != null && fraudCheckResult.DecisionType == DecisionType.Deny)
                {
                    // only deny the request when PS API denies the request
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logCounterManager.Increment("Card_AutoReloadSettings_Paypal_Error");
                _logRepository.Log("APILog", "CardApi - AutoReloadSettings FraudCheck Exception",
                    string.Format("AutoReloadSettings FraudCheck for user Id: {0}, card Id:{1} Exception:{2}", userId, card.CardId, ex.ToString()),
                    TraceEventType.Error);

                // got expcetion from calling fraudcheck , but not explicit deny so we keep moving
            }

            return true;
        }

        private void FraudCheckForTransfer(string userId, IAssociatedCard cardFrom, IAssociatedCard cardTo, decimal amount, IMerchantInfo merchantInfo,
            string emailAddress, string submarket, IRisk risk)
        {
            if (risk == null)
            {
                return;
            }

            IFraudCheckResult fraudCheckResult = null;
            FraudCheckRequest fraudCheckRequest = null;
            try
            {
                fraudCheckRequest = new FraudCheckRequest()
                {
                    TransactionId = Guid.NewGuid().ToString(),
                    TransactionType = TransactionTypes.TransferBalance,
                    Platform = risk.Platform,
                    Market = !string.IsNullOrWhiteSpace(risk.Market) ? risk.Market : submarket,
                    CurrencyCode = merchantInfo.CurrencyCode,
                    BillingInfo = null,
                    Basket = CreateOrderForFraudCheck("SVCTransferBalance", amount),
                    Shipping = null,
                    Risk = new FraudCheckRiskInfo()
                    {
                        UserId = userId,
                        //Email = emailAddress,
                        OrderMessage = null,
                        CCAgentName = risk.CcAgentName,
                        // Set our default logged in state if none was specified.
                        SignedIn = (risk.IsLoggedIn == null) ? DefaultIsLoggedInValue : risk.IsLoggedIn.ToString(),

                        TransferBalanceDetails = GetTransferBalanceDetails(userId, cardFrom, cardTo),
                        DeviceReputation = ToPaymentServiceApiSdkReputation(risk.IovationFields)
                    }
                };

                fraudCheckResult = _paymentServiceApiClient.FraudCheck(fraudCheckRequest);
                LogPSApiCallDetails.LogFraudCheckResult(fraudCheckRequest, fraudCheckResult, _logRepository, "CardTransactionTransferFraudCheckResult", "CardTransferFraudCheck");
            }
            catch (Exception ex)
            {
                LogPSApiCallDetails.LogFraudCheckException(fraudCheckRequest, ex, _logRepository, "CardTransactionTransferFraudCheckException", "CardTransferFraudCheck"); ;
                _logCounterManager.Increment("Card_TransferStarbucksCardBalance_Error");
                _logRepository.Log("APILog", "CardApi - FraudCheck Exception",
                    string.Format("Transfer for user {0} from card Id:{1} to card id:{2} exception occurred:{3}", userId, cardFrom.CardId, cardTo.CardId, ex.ToString()),
                    TraceEventType.Error);
            }

            if (fraudCheckResult != null &&
                (fraudCheckResult.DecisionType != DecisionType.Accept && fraudCheckResult.DecisionType != DecisionType.Review))
            {
                _logRepository.Log("APILog", "CardApi - FraudCheck Denied",
                    string.Format("Transfer for user {0} from card Id:{1} to card id:{2} denied.", userId, cardFrom.CardId, cardTo.CardId),
                    TraceEventType.Warning);

                _logCounterManager.Increment("Card_TransferStarbucksCardBalance_RequestDenied");

                throw new CardProviderException(CardProviderErrorResource.RequestDeniedCode,
                                                CardProviderErrorResource.RequestDeniedMessage);
            }
        }

        private IEnumerable<IStarbucksCardBalance> TransferAmount(string userId,
                                                                  IAssociatedCard cardFrom,
                                                                  IAssociatedCard cardTo,
                                                                  decimal amount, string submarket,
                                                                  string email = null,
                                                                  IRisk risk = null)
        {
            string cardNumberFrom = cardFrom.Number;
            string cardNumberTo = cardTo.Number;

            // Validate source card.
            if (string.IsNullOrEmpty(cardNumberFrom))
            {
                throw new CardProviderException(CardProviderErrorResource.InvalidCardNumberCode,
                                                CardProviderErrorResource.InvalidCardNumberMessage);
            }

            // Validate target card.
            if (string.IsNullOrEmpty(cardNumberTo))
            {
                throw new CardProviderException(CardProviderErrorResource.InvalidCardNumberCode,
                                                CardProviderErrorResource.InvalidCardNumberMessage);
            }

            // Validate target card is same as source card.
            // Trying to account for a race condition and solving for a reported fraud case..
            if (cardNumberFrom.Equals(cardNumberTo, StringComparison.OrdinalIgnoreCase))
            {
                throw new CardProviderException(CardProviderErrorResource.InvalidCardNumberCode,
                                                CardProviderErrorResource.InvalidCardNumberMessage);
            }

            // Validate amount.
            if (amount <= 0)
            {
                throw new CardProviderException(CardProviderErrorResource.InvalidAmountCode,
                                                CardProviderErrorResource.InvalidAmountMessage);
            }

            string pin = cardFrom.Pin;

            //1. get MID and AltMID based on submarket
            var merchantInfo = GetMerchantInfo(submarket);

            if (IsPossiblePosFraud(merchantInfo, cardNumberFrom, submarket))
            {
                _logCounterManager.Increment("Card_TransferStarbucksCardBalance_Error");
                throw new CardProviderException(CardProviderErrorResource.CannotTransferAfterReloadCode, CardProviderErrorResource.CannotTransferAfterReloadMessage);
            }

            //2. Check the balance on the "from" card to make sure we have an adequate balance to conduct the transfer.          
            var transBal = _cardIssuer.GetBalance(merchantInfo, cardNumberFrom, pin);

            if (transBal.EndingBalance < amount || amount == 0)
            {
                //Throw a fault.
                throw new CardProviderException(CardProviderErrorResource.InsufficientFundsForTransactionCode,
                                                CardProviderErrorResource.InsufficientFundsForTransactionMessage);
            }

            // 3 perform fraud check. 
            FraudCheckForTransfer(userId, cardFrom, cardTo, amount, merchantInfo, email, submarket, risk);

            // 4. call redeem
            Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction redeemResult = null;
            try
            {
                redeemResult = _cardIssuer.Redeem(merchantInfo, cardNumberFrom, pin, amount);
            }
            catch (Exception ex)
            {
                _logRepository.Log("APILog", "CardApi - _cardIssuer Redeem Throws Exception",
                    string.Format("Transfer failed in card issuer redeem for user {0} from card Id:{1} to card id:{2}. Exception occurred:{3}", userId, cardNumberFrom, cardNumberTo, ex.ToString()),
                    TraceEventType.Error);
                throw;
            }

            // This should not happen for normal users. Trying to account for a race condition and solving for a reported fraud case.
            if (redeemResult.ResponseCode == "00" && amount != redeemResult.Amount)
            {
                // Handle the amount mismatch error.
                _logCounterManager.Increment("Card_InvalidCardTransfer_Error");

                //Void the redeem request since the money alreay taken out and failed to reload.
                VoidValueLinkRedeemRequest(redeemResult, merchantInfo);

                throw new CardProviderException(CardProviderErrorResource.InvalidCardTransferCode,
                                                CardProviderErrorResource.InvalidCardTransferMessage);
            }

            //If there was any sort of redemption error, throw error response.
            if (redeemResult.ResponseCode != "00")
            {
                // Handle the VL transfer amount denied error.
                _logRepository.Log("APILog", "CardApi - redeemResult ResponseCode NOT\"00\"",
                    string.Format("Transfer failed in redeem for user {0} from card Id:{1} to card id:{2} with response code:{3}", userId, cardNumberFrom, cardNumberTo, redeemResult.ResponseCode),
                    TraceEventType.Error);

                throw HandleCardIssuerResponse(redeemResult.ResponseCode);
            }

            // 5. call reload
            Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction reloadResult = null;
            try
            {
                reloadResult = _cardIssuer.Reload(merchantInfo, cardNumberTo, amount);
            }
            catch (Exception ex)
            {
                //Void the redeem request since the money alreay taken out and failed to reload.
                VoidValueLinkRedeemRequest(redeemResult, merchantInfo);

                _logRepository.Log("APILog", "CardApi - Transfer",
                    @"_cardIssuer Reload Throws Exception user {userId} from card Id:{cardNumberFrom} to card id:{cardNumberTo}. Exception occurred:{ex}",
                    TraceEventType.Error);

                throw;
            }

            //Handle the VL error.
            if (!reloadResult.TransactionSucceeded || reloadResult.ResponseCode != "00")
            {
                //Void the redeem request since the money alreay taken out and failed to reload.
                VoidValueLinkRedeemRequest(redeemResult, merchantInfo);

                throw HandleCardIssuerResponse(reloadResult.ResponseCode);
            }


            //Open the transaction database to look up the currency.

            // Insert the CardAmountTransactionDetails with ToCard reload transactionid
            long returnCardAmountTransactionId = _cardIssuer.UpsertPartialTransferTransactionDetails(0,
                      reloadResult.TransactionId, reloadResult.TransactionDate, null, null);

            // Update the CardAmountTransactionDetails with FromCard redeem transactionid
            if (returnCardAmountTransactionId > 0)
            {
                _cardIssuer.UpsertPartialTransferTransactionDetails(returnCardAmountTransactionId,
                                                                    reloadResult.TransactionId,
                                                                    reloadResult.TransactionDate,
                                                                    redeemResult.TransactionId,
                                                                    redeemResult.TransactionDate);
            }

            //Update the transaction history and balances for both cards. 

            //Update transaction history for the first card with the result.
            redeemResult.Amount = redeemResult.Amount * -1;
            redeemResult.Description = string.Format("Transfer balance to card {0}",
                                                     cardNumberTo.Substring(cardNumberTo.Length - 4));
            UpsertCardTransactionHistory(cardNumberFrom, new CardTransaction(redeemResult));

            //Update the balance on the first card
            UpsertCardBalanceByDecryptedNumber(cardNumberFrom, redeemResult);


            //Update transaction history for the second card.
            reloadResult.Description = string.Format("Transfer balance from card {0}",
                                                     cardNumberFrom.Substring(cardNumberFrom.Length - 4));
            UpsertCardTransactionHistory(cardNumberTo, new CardTransaction(reloadResult));

            //Update the balance on the second card.
            UpsertCardBalanceByDecryptedNumber(cardNumberTo, reloadResult);

            var balanceFrom = ConvertTransactionToBalance(cardNumberFrom, redeemResult);
            var balanceTo = ConvertTransactionToBalance(cardNumberTo, reloadResult);

            SendTransferConfirmationEmail(submarket, amount, cardNumberFrom, cardNumberTo);

            return new List<IStarbucksCardBalance> { balanceFrom, balanceTo };
        }


        private void VoidValueLinkRedeemRequest(Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction redeemResult, IMerchantInfo merchantInfo)
        {
            // Void the redeem request since the money alreay taken out and failed to reload.
            Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction voidRedeemResult = null;
            try
            {
                voidRedeemResult = _cardIssuer.VoidRequest(redeemResult, merchantInfo);
            }
            catch (Exception ex)
            {
                _logRepository.Log("APILog", "CardApi - TransferCardBalance",
                    string.Format("VoidValueLinkRedeemRequest, _cardIssuer VoidRequest Exception: {0}. redeemResult.TransactionId {1} from card Id:{2} redeemResult.ResponseCode:{3}.",
                    ex, redeemResult.TransactionId, redeemResult.CardId, redeemResult.ResponseCode),
                    TraceEventType.Error);
                return;
            }
            if (voidRedeemResult.ResponseCode != "00")
            {
                _logRepository.Log("APILog", "CardApi - TransferCardBalance",
                    string.Format("VoidValueLinkRedeemRequest _cardIssuer.VoidRequest Error. redeemResult.TransactionId {0} from card Id:{1} redeemResult.ResponseCode:{2}.",
                    redeemResult.TransactionId, redeemResult.CardId, redeemResult.ResponseCode),
                    TraceEventType.Error);
            }
        }

        private bool IsPossiblePosFraud(IMerchantInfo merchantInfo, string cardNumberFrom, string market)
        {
            var interval = GetReloadLockoutTransferInterval(market);
            if (!interval.HasValue) return false;
            var condensedHistory = _cardIssuer.GetHistoryCondensed(merchantInfo, cardNumberFrom);
            var now = DateTime.UtcNow;
            return
                (condensedHistory.Exists(
                    p => (p.RequestCode == "0300" || p.RequestCode == "0100") && (p.UtcDate.AddMinutes(interval.Value) > now)));

        }

        private int? GetReloadLockoutTransferInterval(string market)
        {
            var lockouts = _cardProviderSettingsSection.ReloadLockouts;
            if (lockouts == null) return null;
            var lockout = lockouts.SingleOrDefault(p => p.Market.Equals(market));
            return lockout == null ? (int?)null : lockout.LockOutTime;
        }

        private void SendTransferConfirmationEmail(string submarket, decimal amount, string cardNumberFrom, string cardNumberTo)
        {
            try
            {
                const string GenericCardNickName = "Card";
                var cardFrom = _cardDal.GetCardByNumber(cardNumberFrom);

                if (!string.IsNullOrEmpty(cardFrom.RegisteredUserId))
                {
                    var user = _cardDal.GetUserAccount(cardFrom.RegisteredUserId);
                    var cardTo = _cardDal.GetCardByNumber(cardNumberTo);
                    string locale = _settingsProvider.GetLocaleByMarket(submarket).FirstOrDefault();
                    string siteCoreCountry = _settingsProvider.GetSiteCoreSiteFromMarket(submarket);
                    var replacements = new Dictionary<string, string>();

                    replacements.Add("AMOUNT", amount.ToString());
                    replacements.Add("CURRENCY", cardFrom.BalanceCurrency);

                    if (string.IsNullOrEmpty(cardFrom.Nickname))
                    {
                        cardFrom.Nickname = GenericCardNickName;
                    }

                    replacements.Add("FROMCARDNICKNAME", cardFrom.Nickname);

                    // The nick name already contains the card number. So no need to append it.
                    if (cardFrom.Nickname.Contains("My Card ("))
                    {
                        replacements.Add("FROMCARDNUMBER", string.Empty);
                    }
                    else
                    {
                        var cardNumber = Encryption.DecryptCardNumber(cardFrom.Number);
                        replacements.Add("FROMCARDNUMBER", cardNumber.Substring(12, 4));
                    }

                    if (string.IsNullOrEmpty(cardTo.Nickname))
                    {
                        cardTo.Nickname = GenericCardNickName;
                    }
                    replacements.Add("TOCARDNICKNAME", cardTo.Nickname);
                    if (cardTo.Nickname.Contains("My Card ("))
                    {
                        replacements.Add("TOCARDNUMBER", string.Empty);
                    }
                    else
                    {
                        var cardNumber = Encryption.DecryptCardNumber(cardTo.Number);
                        replacements.Add("TOCARDNUMBER", cardNumber.Substring(12, 4));
                    }

                    var item = new StageEmailArgs
                    {
                        MessageId = Guid.NewGuid().ToString(),
                        EmailTemplateName = QueuedEmailType.TransferConfirmation.ToString(),
                        RecipientList = new List<string> { user.EmailAddress },
                        ReplacementValues = replacements,
                        Site = siteCoreCountry,
                        Language = locale,
                        SendOn = null, //![Obsolete]?
                        SendDelayAsHours = 0 //![Obsolete]?
                    };
                    _messageBroker.SubmitMessage(item);
                }
            }
            catch (Exception ex)
            {
                _logRepository.Write(string.Format("Could not send email to queue {0}.\n{1}", ex.Message, ex.StackTrace));
            }
        }

        private static CardBalance ConvertTransactionToBalance(string cardNumber,
                                                               CardIssuer.Dal.Common.Models.ICardTransaction
                                                                   cardTransaction, string cardId = null)
        {
            var balance = new CardBalance
            {
                CardNumber = cardNumber,
                Balance = cardTransaction.EndingBalance,
                BalanceCurrencyCode = cardTransaction.Currency,
                BalanceDate = DateTime.SpecifyKind(cardTransaction.UtcDate, DateTimeKind.Utc),
                CardId = cardId
            };
            return balance;
        }

        private void UpsertCardBalanceByDecryptedNumber(string cardNumber,
                                                        CardIssuer.Dal.Common.Models.ICardTransaction balance)
        {
            _cardDal.UpsertCardBalanceByDecryptedNumber(cardNumber, balance.EndingBalance,
                                                        balance.UtcDate,
                                                        balance.Currency);
        }

        #endregion

        #region Registration

        private RegistrationResult RegisterCard(string userId, IStarbucksCardHeader cardHeader,
                                               string submarket, string code,
                                               uint numberOfStars, string merchantId, bool mailNewCard,
                                               out string cardId)
        {
            const RegistrationResult result = RegistrationResult.SuccessfulRegistration;

            _logCounterManager.Increment("card_register");

            _logCounterManager.Increment(string.Format("card_register_{0}",
                                                       string.IsNullOrEmpty(submarket) ? string.Empty : submarket));

            IAssociatedCard card;
            cardId = string.Empty;

            InitialCardValidation(userId, cardHeader, out cardId, out card, true);

            //Changed to do always call validate and save in case the market has changed...
            ValidateAndSaveCard(cardHeader.CardNumber, cardHeader.CardPin, submarket, ref card);

            var cards = GetCards(userId, VisibilityLevel.Decrypted, false, false, submarket, submarket);

            var makeDefault = cards.Count(p => p.IsOwner && p.IsDefault) == 0;

            cardHeader.Primary = makeDefault;

            var promo = GetPromoCodeByCardNumber(cardHeader.CardNumber);

            string promoCode = promo.PromoCode;


            //If the client didn't pass MarketingRegSource infer from the card type
            if (string.IsNullOrEmpty(cardHeader.RegistrationSource.Marketing))
            {
                cardHeader.RegistrationSource.Marketing = promo.MarketingRegistrationSource;
                ValidatePlatformRegistrationSource(cardHeader.RegistrationSource);
            }
            else
            {
                ValidateMarketingRegistrationSource(cardHeader.RegistrationSource);
                ValidatePlatformRegistrationSource(cardHeader.RegistrationSource);
            }

            //Register the card. 
            AssociateCard(userId, cardHeader, card);

            if (card.Type != CardType.Duetto)
            {
                //Validate if the card is first time registered.
                bool isPromoCardFirstTimeRegistered = IsCardNumberAlreadyRegistered(card.Number);
                //string promoCode = GetPromoCodeByCardNumber(card.Number);

                UpdateRegistrationSourceTeavana(userId, promoCode, isPromoCardFirstTimeRegistered);

                //update registration source in the customProperty table
                UpdateRegistrationSource(userId, isPromoCardFirstTimeRegistered, cardHeader);

                if (_cardDal.IsPromoCodeActive(promoCode, submarket) && isPromoCardFirstTimeRegistered.Equals(true))
                {
                    var promoConfigSettings = _cardDal.GetPromoSettings(promoCode);
                    promoConfigSettings.CardId = card.CardId;
                    promoConfigSettings.UserId = card.RegisteredUserId;

                    SaveCardToLoyaltyHostWithPromotion(promoConfigSettings);
                }
                else
                {
                    DoSaveCardToLoyalty(userId, card.CardId, code, 0, merchantId, mailNewCard);
                }

            }

            cardId = card.CardId;

            //Not sure if this is really here
            //SubscribeCheetahMail(userId);
            string registrationSourcePlatform = (cardHeader.RegistrationSource != null) ? cardHeader.RegistrationSource.Platform : null;
            string registrationSourceMarketing = (cardHeader.RegistrationSource != null) ? cardHeader.RegistrationSource.Marketing : null;

            // Since we just registered this card its Nickname is in the header, not the card object. Put it into the card so we report it.
            card.Nickname = cardHeader.Nickname;

            ReportFraudDataForAddCard(card, userId, submarket, registrationSourcePlatform, registrationSourceMarketing, cardHeader.Risk);

            return result;
        }

        private void InitialCardValidation(string newOwnerId, IStarbucksCardHeader cardHeader, out string cardId, out IAssociatedCard card, bool isPinRequired)
        {
            card = GetAssociatedCard(cardHeader.CardNumber, cardHeader.CardPin, isPinRequired);

            cardId = string.Empty;
            if (card != null)
            {
                cardId = card.CardId;

                ////Check if card is already registered. Also check if it is not associate (for associate a card isPinRequired = false)           
                if (!string.IsNullOrEmpty(card.RegisteredUserId) && isPinRequired)
                {
                    _logRepository.Log("APILog", "CardApi - Register Card",
                                       string.Format("Card has been registered to UserID:{0} CardId:{1}, not able to Registered to new UserID {2}", card.RegisteredUserId ?? "<NULL>", cardId ?? "<NULL>", newOwnerId ?? "<NULL>"),
                                       TraceEventType.Warning);

                    throw new CardProviderException(
                        CardProviderErrorResource.CardIsAlreadyRegisteredCode,
                        CardProviderErrorResource.CardIsAlreadyRegisteredMessage);

                }
            }
            else
            {
                card = new AssociatedCard(_cardProviderSettingsSection)
                {
                    PinValidated = false,
                    Number = cardHeader.CardNumber,
                    Pin = cardHeader.CardPin
                };
            }

            //If the card pin exists, and it has been validated, check the pin passed in against the card PIN.
            if (!string.IsNullOrEmpty(card.Pin) && card.PinValidated && isPinRequired)
            {
                if (cardHeader.CardPin != Encryption.DecryptPin(card.Pin))
                    //If PINs don't match return an Invalid PIN result.
                    //return RegistrationResult.InvalidPin;
                    throw new CardProviderException(
                        CardProviderErrorResource.InvalidPinCode,
                        CardProviderErrorResource.InvalidPinMessage);
            }
        }

        private void SubscribeCheetahMail(string userId)
        {
            var emailPreference = _cardDal.GetEmailPreference(userId);
            if (emailPreference)
            {
                var data = new List<KeyValuePair<string, object>>
                    {
                        new KeyValuePair<string, object>("action", "CheetahMailSubscribe"),
                        new KeyValuePair<string, object>("userid", userId)
                    };
                _messageBroker.Publish(new Starbucks.MessageBroker.Common.Models.LoyaltyHostServiceMessage() { Data = data });
            }
        }

        private void DoSaveCardToLoyalty(string userId, string cardId, string code, uint numberOfStars,
                                                     string merchantId, bool mailNewCard)
        {
            // not code redemption code page, use the original approach.
            if (string.IsNullOrEmpty(code))
            {
                SaveCardToLoyaltyHost(userId, cardId);
            }
            else
            {
                CreateAccountAddStars(userId, cardId, code, numberOfStars, merchantId, mailNewCard);
            }
        }

        private void CreateAccountAddStars(string userId, string cardId, string code, uint numberOfStars, string merchantId,
                                           bool mailNewCard)
        {
            var data = new List<KeyValuePair<string, object>>
                {
                    new KeyValuePair<string, object>("action", "createaccountandaddstars"),
                    new KeyValuePair<string, object>("userid", userId),
                    new KeyValuePair<string, object>("cardid", cardId),
                    new KeyValuePair<string, object>("code", code),
                    new KeyValuePair<string, object>("numberofstars", numberOfStars),
                    new KeyValuePair<string, object>("merchantid", merchantId),
                    new KeyValuePair<string, object>("mailnewcard", mailNewCard)
                };
            try
            {
                _logRepository.Log("APILog", "CardApi - CreateAccountAndAddStars",
                                   string.Format("Register Card UserID:{0} CardId:{1}", userId, cardId),
                                   TraceEventType.Information);

                _loyaltyHostListener.Handle(new Starbucks.MessageBroker.Common.Models.LoyaltyHostServiceMessage() { Data = data });
            }
            catch (Exception ex)
            {
                _logRepository.Log("APILog", "CardApi - CreateAccountAndAddStarsException",
                                   string.Format("Register Card UserID:{0} CardId:{1} Exception:{2}", userId, cardId, ex),
                                   TraceEventType.Error);
            }
        }

        private void SaveCardToLoyaltyHost(string userId, string cardId)
        {
            var data = new List<KeyValuePair<string, object>>
                {
                    new KeyValuePair<string, object>("action", "savecardtoloyaltyhost"),
                    new KeyValuePair<string, object>("userid", userId),
                    new KeyValuePair<string, object>("cardid", cardId),
                };
            try
            {
                _logRepository.Log("APILog", "CardApi - SaveCardToLoyaltyHost",
                                   string.Format("Register Card UserID:{0} CardId:{1}", userId, cardId),
                                   TraceEventType.Information);

                _loyaltyHostListener.Handle(new Starbucks.MessageBroker.Common.Models.LoyaltyHostServiceMessage() { Data = data });
            }
            catch (Exception ex)
            {
                _logRepository.Log("APILog", "CardApi - SaveCardToLoyaltyHostException",
                                   string.Format("Register Card UserID:{0} CardId:{1} Exception:{2}", userId, cardId, ex),
                                   TraceEventType.Error);
            }
        }


        private void SaveCardToLoyaltyHostWithPromotion(IPromotionProperties promotionProperties)
        {
            var data = new List<KeyValuePair<string, object>>
                {
                    new KeyValuePair<string, object>("action", "savecardtoloyaltyhostwithpromotion"),
                    new KeyValuePair<string, object>("userid", promotionProperties.UserId),
                    new KeyValuePair<string, object>("cardid", promotionProperties.CardId),
                    new KeyValuePair<string, object>("submarket", promotionProperties.Submarket),
                    new KeyValuePair<string, object>("promourl", promotionProperties.PromoUrl),
                    new KeyValuePair<string, object>("promocode", promotionProperties.PromoCode),
                    new KeyValuePair<string, object>("pinrequired", promotionProperties.PinRequired),
                    new KeyValuePair<string, object>("cardrange", promotionProperties.CardRange),
                    new KeyValuePair<string, object>("enddate", promotionProperties.EndDate),
                    new KeyValuePair<string, object>("countrycodes", promotionProperties.CountryCodes),
                    new KeyValuePair<string, object>("promofeatures",
                                                     promotionProperties.PromoFeatures.Select(
                                                         promoFeature => new List<KeyValuePair<string, object>>
                                                             {
                                                                 new KeyValuePair<string, object>("name",
                                                                                                  promoFeature.Name),
                                                                 new KeyValuePair<string, object>("value",
                                                                                                  promoFeature.Value),
                                                                 new KeyValuePair<string, object>("description",
                                                                                                  promoFeature
                                                                                                      .Description)
                                                             }).ToList())
                };

            try
            {
                _logRepository.Log("APILog", "CardApi - SaveCardToLoyaltyHostWithPromotion",
                                  string.Format("Register Card UserID:{0} CardId:{1}", promotionProperties.UserId, promotionProperties.CardId),
                                  TraceEventType.Information);

                _loyaltyHostListener.Handle(new Starbucks.MessageBroker.Common.Models.LoyaltyHostServiceMessage() { Data = data });
            }
            catch (Exception ex)
            {
                _logRepository.Log("APILog", "CardApi - SaveCardToLoyaltyHostWithPromotionException",
                                  string.Format("Register Card UserID:{0} CardId:{1} Exception:{2}", promotionProperties.UserId, promotionProperties.CardId, ex),
                                  TraceEventType.Error);
            }
        }


        private void UpdateRegistrationSourceTeavana(string userId, string promoCode,
                                                     bool isPromoCardFirstTimeRegistered)
        {
            //Condition to check for the Teavana card is being registered.
            if ((string.IsNullOrEmpty(promoCode)) || !isPromoCardFirstTimeRegistered.Equals(true)) return;

            var teavanaPromoCode = _cardProviderSettingsSection.TeavanaPromoCode;
            if (!string.IsNullOrEmpty(teavanaPromoCode))
            {
                //Logger.Write(string.Format(
                //    "Teavana PromoCode:{0} is active for this UserId:{1} and CardId:{2}", promoCode, userId,
                //    card.CardId));
                var strTeavanaRegSourceKey =
                    ConfigurationManager.AppSettings["TeavanaRegSourceKey"].ToString(
                        CultureInfo.InvariantCulture);
                if (string.Equals(promoCode.Trim(), teavanaPromoCode.Trim()))
                {
                    var registeredCards = _cardDal.GetAssociatedCards(userId);
                    var registeredCardCount = registeredCards.Where(x => x.RegisteredUserId.Equals(userId, StringComparison.CurrentCultureIgnoreCase));
                    //Check the condition whether the user has only the Current Card (Teavana Card) as registered card. 
                    if (registeredCardCount.Count() == 1)
                    {
                        UpdateUserRegistrationSource(userId, strTeavanaRegSourceKey);
                    }
                }
            }
        }

        private void UpdateRegistrationSource(string userId, bool isPromoCardFirstTimeRegistered, IStarbucksCardHeader cardHeader)
        {
            if (!isPromoCardFirstTimeRegistered.Equals(true)) return;

            var registeredCards = _cardDal.GetAssociatedCards(userId);
            //Check if the user has only the current Card as registered card. If true update the registration source

            var registeredCardCount = registeredCards.Where(x => !string.IsNullOrEmpty(x.RegisteredUserId) && x.RegisteredUserId.Equals(userId, StringComparison.CurrentCultureIgnoreCase));


            if (registeredCardCount.Count() == 1)
            {
                UpdateUserRegistrationSource(userId, cardHeader.RegistrationSource);
            }

        }

        /// <summary>
        /// Validates marketing and platform registration source if invalid value provided set to default
        /// </summary>
        /// <param name="cardRegistrationSource">An instance with valid or default registration source values</param>
        private void ValidateMarketingRegistrationSource(ICardRegistrationSource cardRegistrationSource)
        {
            if (string.IsNullOrEmpty(cardRegistrationSource.Marketing) || !_cardDal.IsMarketingRegSourceCodeValid(cardRegistrationSource.Marketing))
            {
                cardRegistrationSource.Marketing = MarketingRegSourceCode;
            }
        }

        private void ValidatePlatformRegistrationSource(ICardRegistrationSource cardRegistrationSource)
        {
            if (string.IsNullOrEmpty(cardRegistrationSource.Platform) || !_cardDal.IsPlatformRegSourceCodeValid(cardRegistrationSource.Platform))
            {
                cardRegistrationSource.Platform = DefaultPlatformRegSourceCode;
            }
        }


        private void AssociateCard(string userId, IStarbucksCardHeader cardHeader, IAssociatedCard card)
        {
            AddCardAssociation(userId, card.CardId, cardHeader, true);

            // Now that the card is registered to the user set its isOwner and isDefault flags appropriately.
            card.IsOwner = true;
            card.IsDefault = cardHeader.Primary;
        }

        private string GetCode(Exception ex)
        {
            try
            {
                dynamic exc = ex;
                return exc.Code;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public IEnumerable<ICardRegistrationStatus> RegisterMultipleCards(string userId,
                                                                          IEnumerable<IStarbucksCardHeader>
                                                                              cardHeaders, string userMarket)
        {
            var cardRegistrationStatuses = new List<ICardRegistrationStatus>();
            foreach (var cardHeader in cardHeaders)
            {
                try
                {
                    var id = RegisterStarbucksCard(userId, cardHeader,
                                                   userMarket);
                    var cardRegistrationStatus = new CardRegistrationStatus
                    {
                        CardId = id,
                        CardNumber = cardHeader.CardNumber,
                        Successful = !string.IsNullOrEmpty(id)
                    };
                    cardRegistrationStatuses.Add(cardRegistrationStatus);
                }
                catch (Exception ex)
                {
                    var cardRegistrationStatus = new CardRegistrationStatus
                    {
                        CardNumber = cardHeader.CardNumber,
                        Message = ex.Message,
                        Successful = false,
                        Code = GetCode(ex),
                        ExceptionType = ex.GetType()
                    };
                    cardRegistrationStatuses.Add(cardRegistrationStatus);

                }
            }
            return cardRegistrationStatuses;
        }



        /// <summary>
        /// Checks that the card number and PIN are valid by doing a balance check against ValueLink, and saves the card.
        /// </summary>
        /// <param name="cardNumber">The card number to validate</param>
        /// <param name="pin">The PIN to validate</param>
        /// <param name="submarket">The card submarket</param>
        /// <param name="savedCard"></param>
        /// <returns>Returns <c>true</c> if card was validated and saved, otherwise <value>false</value></returns>
        /// <exception cref="ValueLinkException">Throws a <see>
        ///                                                   <cref>ValuLinkException</cref>
        ///                                               </see>
        ///     if the VL return code is non-zero.</exception>
        public bool ValidateAndSaveCard(string cardNumber, string pin, string submarket, ref IAssociatedCard savedCard, IMerchantInfo merchantInfo = null)
        {
            bool validated = false;

            //MAKE VL CALL WITH CARD NUMBER AND PIN.  MAKE SURE WE GET VALID VALUES.         
            //1. Get submarket and currency data.
            if (merchantInfo == null)
            {
                merchantInfo = GetMerchantInfo(submarket);
            }

            var result = _cardIssuer.GetBalance(merchantInfo, cardNumber, pin);

            if (result != null)
            {
                var rules = new Rules();
                if (!rules.CanRegister(result.CardClass))
                {
                    _logRepository.Log("APILog", "CardApi - ValidateAndSaveCard",
                                      string.Format("ValidateAndSaveCard: Message:{0}, cardNumber:{1} result.CardClass:{2}",
                                      CardProviderErrorResource.CannotRegisterInvalidCardClassMessage, cardNumber, result.CardClass),
                                      TraceEventType.Error);
                    throw new CardProviderException(CardProviderErrorResource.CannotRegisterInvalidCardClassCode,
                                                    CardProviderErrorResource.CannotRegisterInvalidCardClassMessage);
                }
            }
            int returnCode;

            //Convert the response code to an int.
            if (int.TryParse(result.ResponseCode, out returnCode))
            {
                //If it is 0, no error, then save the card.
                if (returnCode == 0)
                {
                    if (string.IsNullOrEmpty(savedCard.CardId))
                    {
                        savedCard.Active = true;
                        savedCard.Number = cardNumber;
                        savedCard.SubMarketCode = merchantInfo.SubMarketId;
                    }

                    if (pin != null)
                    {
                        //Save the PIN, and set pinvalidated to true.
                        savedCard.Pin = pin;
                        savedCard.PinValidated = true;
                    }

                    //Get the class and currency from the ValueLink call.
                    savedCard.Class = result.CardClass;

                    savedCard.Currency = !string.IsNullOrEmpty(result.BaseCurrency)
                                             ? result.BaseCurrency
                                             : result.Currency;


                    //Try to set the card type.
                    //Check if it's a duetto card.  Visa's start with 4.
                    if (cardNumber.Substring(0, 1) == "4")
                        savedCard.Type = CardType.Duetto;
                    else if (savedCard.Class == GoldCardClass) //GOLD CARD CLASS.
                        savedCard.Type = CardType.Gold;
                    else
                        savedCard.Type = CardType.Standard;

                    //Save the card, and get the card ID.
                    savedCard.CardId = UpsertCard(savedCard);

                    savedCard.IsDigitalCard = IsDigitalCard(savedCard.BatchCode);

                    validated = true;
                }
                else
                {

                    if (returnCode == 3)
                        throw new CardProviderException(CardProviderErrorResource.CannotRegisterInvalidCardNumberCode,
                                                        CardProviderErrorResource.CannotRegisterInvalidCardNumberMessage);
                    //some non-zero code 
                    throw HandleCardIssuerResponse(returnCode.ToString(CultureInfo.InvariantCulture));
                }
            }
            else
            {
                //Handle VL errors.
                throw HandleCardIssuerResponse(result.ResponseCode);
            }

            return validated;
        }

        private string GoldCardClass
        {
            get
            {
                return string.IsNullOrEmpty(_cardProviderSettingsSection.GoldCardClass)
                     ? "205"
                     : _cardProviderSettingsSection.GoldCardClass;
            }
        }

        private static readonly NameValueCollection DigitalCardPromoCodes =
            (NameValueCollection)ConfigurationManager.GetSection("digitalCardPromoCodes");

        public static bool IsDigitalCard(string promoCode)
        {
            // They keys should always be a number and not have casing issues, see if the key exists in the collection 
            // If so this should be a digital card

            if (DigitalCardPromoCodes == null)
            {
                throw new Exception("Digital Promo code configuration section is missing in the configuration.");
            }

            bool returnValue = false;
            if (promoCode != null)
            {
                returnValue = DigitalCardPromoCodes.Cast<string>().Contains(promoCode);
            }
            return returnValue;
        }


        public string RegisterStarbucksCard(string userId, IStarbucksCardHeader cardHeader, string userMarket)
        {
            string cardId;
            var cardSubMarket = string.Empty;
            IMerchantInfo merchantInfo = null;

            if (!string.IsNullOrEmpty(cardHeader.Submarket))
            {
                cardSubMarket = cardHeader.Submarket;
            }
            else
            {
                cardSubMarket = userMarket;
            }
            var submarket = CurrentSubMarket(cardSubMarket);

            merchantInfo = GetMerchantInfo(cardSubMarket);

            if (cardHeader.RegistrationSource == null)
            {
                cardHeader.RegistrationSource = new CardRegistrationSource();
            }

            RegisterCard(userId, cardHeader, submarket, string.Empty, 0,
                         merchantInfo.Mid, false, out cardId);

            return cardId;
        }

        public string ActivateAndRegisterCard(string userId, string userMarket, IStarbucksCardHeader cardHeader)
        {
            var cardSubMarket = string.Empty;
            IMerchantInfo merchantInfo = null;

            if (!string.IsNullOrEmpty(cardHeader.Submarket))
            {
                cardSubMarket = cardHeader.Submarket;
                merchantInfo = GetMerchantInfo(cardSubMarket);
                if (merchantInfo == null)
                {
                    throw new CardProviderException(CardProviderErrorResource.InvalidSubMarketCode, CardProviderErrorResource.InvalidSubMarketMessage);
                }
            }
            else
            {
                cardSubMarket = userMarket;
            }

            var submarket = CurrentSubMarket(cardSubMarket);
            if (merchantInfo == null)
            {
                merchantInfo = GetMerchantInfo(cardSubMarket);
            }

            string response = string.Empty;
            string promoCode = GetPromoCode(CurrentSubMarket(cardSubMarket, true));

            if (string.IsNullOrEmpty(promoCode))
            {
                throw new CardProviderException(CardProviderErrorResource.UnableToFindPromoCode, CardProviderErrorResource.UnableToFindPromoCodeMessage);
            }

            const decimal amount = 0; // activate Zero Value amount

            CardIssuer.Dal.Common.Models.ICardTransaction newCardTransaction;

            try
            {
                newCardTransaction = _cardIssuer.ActivateVirtual(merchantInfo, promoCode, amount);
            }
            catch (Exception ex)
            {
                _logRepository.Log("APILog", "CardApi - CardProvider:ActivateAndRegisterCard",
                                  string.Format("CardProvider::ActivateAndRegisterCard: UserId:{0}, CardNumber:{1}, exception:{2}",
                                  userId ?? "<NULL>", cardHeader.CardNumber, ex.ToString()),
                                  TraceEventType.Error);
                throw;
            }

            if (newCardTransaction == null) return string.Empty;

            IAssociatedCard savedCard = new AssociatedCard(_cardProviderSettingsSection) { BatchCode = promoCode };
            ValidateAndSaveCard(newCardTransaction.CardNumber, newCardTransaction.Pin, submarket, ref savedCard, merchantInfo);

            //invoke the register call with cardnumber and pin
            if ((savedCard != null) && (!string.IsNullOrEmpty(savedCard.Number)) &&
                (!string.IsNullOrEmpty(savedCard.Pin)))
            {
                //var cardHeader = new StarbucksCardHeader();
                cardHeader.CardNumber = newCardTransaction.CardNumber;
                cardHeader.CardPin = newCardTransaction.Pin;
                var regResponse = RegisterCard(userId, cardHeader, submarket, promoCode, 0, merchantInfo.Mid, false,
                                               out response);
                if (regResponse != RegistrationResult.SuccessfulRegistration)
                    response = string.Empty;

            }

            // no need to report fraud here since inside RegisterCard already does so
            return response;
        }

        #endregion

        #region PrivateHelpers

        private IMerchantInfo GetMerchantInfo(string currentSubMarket, string platform = UnKnownPlatform)
        {
            var merchantInfo = _cardIssuer.GetMerchantInfo(currentSubMarket, platform);
            if (merchantInfo == null)
            {
                _logRepository.Log("APILog", "CardApi - CardProvider_GetMerchantInfo__cardIssuer.GetMerchantInfo_ReturnedNull",
                      string.Format("currentSubMarket: {0}", currentSubMarket), TraceEventType.Error);

                throw new CardProviderException(CardProviderErrorResource.InvalidSubMarketCode, CardProviderErrorResource.InvalidSubMarketMessage);
            }
            return merchantInfo;
        }

        private string UpsertCard(ICard card)
        {
            return card != null ? _cardDal.UpsertCard(card) : string.Empty;
        }

        private void AddCardAssociation(string userId, string cardId, IStarbucksCardHeader cardHeader, bool registered)
        {
            Validator.ValidateUserIdCardId(userId, cardId);
            _cardDal.AddCardAssociation(userId, cardId, cardHeader, registered);
        }

        /// <summary>
        /// Associates a card to a user
        /// </summary>
        /// <param name="userId">user id </param>
        /// <param name="cardId"> card id </param>
        /// <param name="cardHeader">card head. </param>
        /// <returns>card id of the associated card. </returns>
        public void AddCardAssociation(string userId, string cardId, IStarbucksCardHeader cardHeader, string userMarket)
        {
            IAssociatedCard card = null;
            if (string.IsNullOrEmpty(cardId))
            {
                card = new AssociatedCard(_cardProviderSettingsSection);
                card.Number = cardHeader.CardNumber;
            }

            //
            string cardSubMarket;
            IMerchantInfo merchantInfo = null;
            if (!string.IsNullOrEmpty(cardHeader.Submarket))
            {
                cardSubMarket = cardHeader.Submarket;
                merchantInfo = GetMerchantInfo(cardSubMarket);
            }
            else
            {
                cardSubMarket = userMarket;
            }
            //

            InitialCardValidation(userId, cardHeader, out cardId, out card, false);

            ValidateAndSaveCard(cardHeader.CardNumber, cardHeader.CardPin, cardSubMarket, ref card, merchantInfo);
            cardId = card.CardId;

            var promo = GetPromoCodeByCardNumber(cardHeader.CardNumber);

            //If the client didn't pass MarketingRegSource infer from the card type
            if (string.IsNullOrEmpty(cardHeader.RegistrationSource.Marketing))
            {
                cardHeader.RegistrationSource.Marketing = promo.MarketingRegistrationSource;
                ValidatePlatformRegistrationSource(cardHeader.RegistrationSource);
            }
            else
            {
                ValidateMarketingRegistrationSource(cardHeader.RegistrationSource);
                ValidatePlatformRegistrationSource(cardHeader.RegistrationSource);
            }

            _cardDal.AddCardAssociation(userId, cardId, cardHeader, cardHeader.Register);

            string registrationSourcePlatform = (cardHeader.RegistrationSource != null) ? cardHeader.RegistrationSource.Platform : null;
            string registrationSourceMarketing = (cardHeader.RegistrationSource != null) ? cardHeader.RegistrationSource.Marketing : null;

            ReportFraudDataForAddCard(card, userId, userMarket, registrationSourcePlatform, registrationSourceMarketing, cardHeader.Risk);
        }

        private bool IsCardNumberAlreadyRegistered(string cardNumber)
        {
            return _cardDal.IsCardNumberAlreadyRegistered(cardNumber);
        }

        private IProgramPromoCode GetPromoCodeByCardNumber(string cardNumber)
        {
            return _cardDal.GetPromoCodeByCardNumber(cardNumber);
        }

        private void UpdateUserRegistrationSource(string userId, ICardRegistrationSource cardRegSource)
        {
            _cardDal.UpdateUserRegistrationSource(userId, cardRegSource);
        }
        private void UpdateUserRegistrationSource(string userId, string registrationSource)
        {
            _cardDal.UpdateUserRegistrationSource(userId, registrationSource);
        }
        /*
                private bool IsPromoCodeActive(string promoCode, string subMarket)
                {
                    return _cardDal.IsPromoCodeActive(promoCode, subMarket);
                }
        */

        private string GetPromoCode(string subMarket)
        {
            return _cardDal.GetPromoCode(subMarket);
        }

        private IAutoReloadProfile GetAutoReload(string userId, string autoReloadId)
        {
            var result = _cardDal.GetAutoReload(userId, autoReloadId);
            return result == null ? null : new AutoReloadProfile(result);
        }

        #endregion

        #region Tipping

        public void ProcessTip(string userId, long historyId)
        {
            // _cardIssuer.Tip()
        }

        #endregion

        #region Fraud

        public IFraudCheckUserData GetUserDataForFraudCheck(string userId)
        {
            Dal.Common.Models.IFraudCheckUserData userDataForFraudCheck = _cardDal.GetUserDataForFraudCheck(userId);
            return FraudCheckUserData.FromDalIFraudCheckUserData(userDataForFraudCheck);
        }

        public IFraudCheckCardData GetCardDataForFraudCheck(string userId, string cardId)
        {
            Dal.Common.Models.IFraudCheckCardData cardDataForFraudCheck = _cardDal.GetCardDataForFraudCheck(userId, cardId);
            return FraudCheckCardData.FromDalIFraudCheckCardData(cardDataForFraudCheck);
        }
        #endregion

        private static Exception HandleCardIssuerResponse(string responseCode)
        {
            return CardIssuerExceptionHandler.HandleCardIssuerResponse(responseCode);
        }

        private string GetAutoreloadStatus(int statusId)
        {
            return ((AutoReloadStatus)statusId).ToString();
        }
        public string ClearPaymentMethodSurrogateAndPaypalAccountNumbers(string userId, IPaymentMethod paymentMethod)
        {
            // Validate parms needed to succeed.
            if (paymentMethod == null || String.IsNullOrWhiteSpace(paymentMethod.PaymentMethodId) || String.IsNullOrWhiteSpace(userId))
            {
                _logRepository.Log("APILog", "CardApi - CardProvider",
                                  string.Format("ClearPaymentMethodSurrogateAndPaypalAccountNumbers: paymentMethodType:{0}, paymentMethod.PaymentMethodId:{1}, paymentMethod.UserId:{2}",
                                  paymentMethod == null ? "<NULL>" : paymentMethod.PaymentType,
                                  paymentMethod == null ? "<NULL>" : paymentMethod.PaymentMethodId,
                                  userId == null ? "<NULL>" : userId),
                                  TraceEventType.Error);
                if (paymentMethod == null)
                    throw new ArgumentNullException("paymentMethod");
                if (paymentMethod.PaymentMethodId == null)
                    throw new ArgumentException("paymentMethod.PaymentMethodId");
                if (userId == null)
                    throw new ArgumentException("paymentMethod.UserId");
            }

            // Clear the SurrogateAccountNumber (CCs) and the AccountNumber (Paypal) so hackers can't reuse them.

            paymentMethod.AccountNumber = "";
            paymentMethod.SurrogateAccountNumber = "";     // Can't use null because the sproc filters this out.

            try
            {
                var paymentMethodId = _cardDal.ClearPaymentMethodSurrogateAndPaypalAccountNumbers(userId, paymentMethod);
                return paymentMethodId.ToString();
            }
            catch (Exception ex)
            {
                _logRepository.Log("APILog", "CardApi - CardProvider",
                                  string.Format("ClearPaymentMethodSurrogateAndPaypalAccountNumbers: paymentMethodType:{0}, paymentMethod.PaymentMethodId:{1}, paymentMethod.UserId:{2}, exception:{3}",
                                  paymentMethod == null ? "<NULL>" : paymentMethod.PaymentType,
                                  paymentMethod == null ? "<NULL>" : paymentMethod.PaymentMethodId,
                                  userId == null ? "<NULL>" : userId,
                                  ex.ToString()),
                                  TraceEventType.Error);
            }

            return paymentMethod.PaymentMethodId;
        }
    }
}
