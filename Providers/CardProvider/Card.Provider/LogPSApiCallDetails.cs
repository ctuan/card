﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.Platform.Bom;
using Starbucks.Api.Sdk.PaymentService.Common;
using Starbucks.Api.Sdk.PaymentService.Interfaces;
using Starbucks.Api.Sdk.WebRequestWrapper;
using SdkModels = Starbucks.Api.Sdk.PaymentService.Models;
using Starbucks.OpenApi.Logging.Common;

namespace Starbucks.Card.Provider
{
    class LogPSApiCallDetails
    {
        internal const string NullDisplay = "<NULL>";

        internal static void LogZeroAuthResult(SdkModels.ZeroRequest sdkRequest, IZeroAuthResult result, ILogRepository logRepository)
        {
            try
            {
                Dictionary<string, string> properties = new Dictionary<string, string>();
                BuildRequestProperties(properties, sdkRequest);
                BuildResponseProperties(properties, result);

                System.Diagnostics.TraceEventType level = GetLoggingLevel(result);
                PerformLogging("CardApi - CardProviderZeroAuthResult", "CardProviderZeroAuth", level, properties, logRepository);
            }
            catch (Exception ex)  // logging exception, we have to eat it
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        internal static void LogZeroAuthException(SdkModels.ZeroRequest sdkRequest, Exception ex, ILogRepository logRepository)
        {
            try
            {
                Dictionary<string, string> properties = new Dictionary<string, string>();
                BuildRequestProperties(properties, sdkRequest);
                BuildExceptionProperties(properties, ex);

                System.Diagnostics.TraceEventType level = System.Diagnostics.TraceEventType.Warning;
                PerformLogging("CardApi - CardProviderZeroAuthException", "CardProviderZeroAuth", level, properties, logRepository);
            }
            catch (Exception err)  // logging exception, we have to eat it
            {
                System.Diagnostics.Debug.WriteLine(err.ToString());
            }

        }

        internal static void LogFraudCheckResult(SdkModels.FraudCheckRequest sdkRequest, IFraudCheckResult result, ILogRepository logRepository, string title, string message)
        {
            try
            {
                Dictionary<string, string> properties = new Dictionary<string, string>();
                BuildRequestProperties(properties, sdkRequest);
                BuildResponseProperties(properties, result);

                System.Diagnostics.TraceEventType level = GetLoggingLevel(result);
                PerformLogging(title, message,  level, properties, logRepository);
            }
            catch (Exception ex)  // logging exception, we have to eat it
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        internal static void LogFraudCheckException(SdkModels.FraudCheckRequest sdkRequest, Exception ex, ILogRepository logRepository, string title, string message)
        {
            try
            {
                Dictionary<string, string> properties = new Dictionary<string, string>();
                BuildRequestProperties(properties, sdkRequest);
                BuildExceptionProperties(properties, ex);

                System.Diagnostics.TraceEventType level = System.Diagnostics.TraceEventType.Warning;
                PerformLogging(title, message, level, properties, logRepository);
            }
            catch (Exception err)  // logging exception, we have to eat it
            {
                System.Diagnostics.Debug.WriteLine(err.ToString());
            }
        }

        internal static void BuildExceptionProperties(Dictionary<string, string> properties, Exception ex)
        {
            properties.Add("Result: hasExceptionObject", (ex != null).ToString());
            if (ex != null)
            {
                properties.Add("Result: Exception", ex.ToString());
                properties.Add("Result: HResult", ex.HResult.ToString());
                SecurePaymentException secEx = ex as SecurePaymentException;
                if (secEx != null)
                {
                    properties.Add("SecurePaymentException Result: ApiErrorMessage", secEx.ApiErrorMessage.ToDisplay());
                    properties.Add("SecurePaymentException Result: ApiErrorCode", secEx.ApiErrorCode.ToDisplay());

                    ApiException apiEx = secEx.InnerException as ApiException;
                    if (apiEx != null)
                    {
                        // TODO, ctuan. review
                        //properties.Add("Inner ApiException Result: HttpStatus", apiEx.HttpStatus.ToString());
                        properties.Add("Inner ApiException Result: StatusCode", apiEx.StatusCode == null ? null : apiEx.StatusCode.ToString());
                        properties.Add("Inner ApiException Result: ApiErrorMessage", apiEx.ApiErrorMessage.ToDisplay());
                        properties.Add("Inner ApiException Result: ApiErrorCode", apiEx.ApiErrorCode.ToDisplay());
                    }
                }
            }
        }

        internal static void BuildRequestProperties(Dictionary<string, string> properties, SdkModels.ZeroRequest sdkRequest)
        {
            properties.Add("request: hasZeroRequestObject", (sdkRequest != null).ToString());
            if (sdkRequest != null)
            {
                properties.Add("request: TransactionType", sdkRequest.TransactionType.ToString());
                properties.Add("request: TransactionDateTime", sdkRequest.TransactionDateTime.ToString());
                properties.Add("request: CurrentTime", DateTime.Now.ToString());
                properties.Add("request: Market", sdkRequest.Market.ToDisplay());
                properties.Add("request: Platform", sdkRequest.Platform.ToDisplay());
                properties.Add("request: CurrencyCode", sdkRequest.CurrencyCode.ToDisplay());

                properties.Add("request: hasRiskObject", (sdkRequest.Risk != null).ToString());
                if (sdkRequest.Risk != null)
                {
                    properties.Add("request: UserId", sdkRequest.Risk.UserId.ToDisplay());
                    properties.Add("request: CCAgentName", sdkRequest.Risk.CCAgentName.ToDisplay());
                    properties.Add("request: SignedIn", sdkRequest.Risk.SignedIn.ToDisplay());

                    properties.Add("request: hasDeviceReputation", (sdkRequest.Risk.DeviceReputation != null).ToString());
                    if (sdkRequest.Risk.DeviceReputation != null)
                    {
                        properties.Add("request: ipAddress", sdkRequest.Risk.DeviceReputation.IpAddress.ToDisplay());
                        properties.Add("request: DeviceFingerprint", sdkRequest.Risk.DeviceReputation.DeviceFingerprint.ToDisplay());
                    }
                }

                BuildRequestProperties(properties, sdkRequest.BillingInfo);
            }
        }

        internal static void BuildRequestProperties(Dictionary<string, string> properties, SdkModels.FraudCheckRequest sdkRequest)
        {
            properties.Add("request: hasFraudCheckRequestObject", (sdkRequest != null).ToString());
            if (sdkRequest != null)
            {
                properties.Add("request: TransactionType", sdkRequest.TransactionType.ToString());
                properties.Add("request: TransactionDateTime", sdkRequest.TransactionDateTime.ToString());
                properties.Add("request: CurrentTime", DateTime.Now.ToString());
                properties.Add("request: Market", sdkRequest.Market.ToDisplay());
                properties.Add("request: Platform", sdkRequest.Platform.ToDisplay());
                properties.Add("request: CurrencyCode", sdkRequest.CurrencyCode.ToDisplay());

                properties.Add("request: hasRiskObject", (sdkRequest.Risk != null).ToString());
                if (sdkRequest.Risk != null)
                {
                    properties.Add("request: UserId", sdkRequest.Risk.UserId.ToDisplay());
                    properties.Add("request: CCAgentName", sdkRequest.Risk.CCAgentName.ToDisplay());
                    properties.Add("request: SignedIn", sdkRequest.Risk.SignedIn.ToDisplay());

                    properties.Add("request: hasDeviceReputation", (sdkRequest.Risk.DeviceReputation != null).ToString());
                    if (sdkRequest.Risk.DeviceReputation != null)
                    {
                        properties.Add("request: ipAddress", sdkRequest.Risk.DeviceReputation.IpAddress.ToDisplay());
                        properties.Add("request: DeviceFingerprint", sdkRequest.Risk.DeviceReputation.DeviceFingerprint.ToDisplay());
                    }
                }
                BuildRequestProperties(properties, sdkRequest.BillingInfo);
            }
        }

        internal static void BuildRequestProperties(Dictionary<string, string> properties, List<SdkModels.OrderItem> basket)
        {
            properties.Add("request: hasBasketObject", (basket != null).ToString());
            if (basket != null)
            {
                foreach (SdkModels.OrderItem orderitem in basket)
                {
                    properties.Add("request: orderitem Amount", orderitem.Amount.ToString());
                    properties.Add("request: orderitem Category", orderitem.Category.ToDisplay());
                    properties.Add("request: orderitem Name", orderitem.Name.ToDisplay());
                    properties.Add("request: orderitem Quantity", orderitem.Quantity.ToString());
                    properties.Add("request: orderitem Sku", orderitem.Sku.ToDisplay());
                }
            }
        }

        internal static void BuildRequestProperties(Dictionary<string, string> properties, SdkModels.BillingInfo billing)
        {
            properties.Add("request: hasBillingObject", (billing != null).ToString());
            if (billing != null)
            {
                properties.Add("request: hasBillingAddressInfoObject", (billing.AddressInfo != null).ToString());
                if (billing.AddressInfo != null)
                {
                    properties.Add("request: billingAddressId", billing.AddressInfo.AddressId.ToDisplay());
                    properties.Add("request: billingAddressDetails", (billing.AddressInfo.AddressDetails != null).ToString());
                    if (billing.AddressInfo.AddressDetails != null)
                    {
                        properties.Add("request: billingAddress FirstName", billing.AddressInfo.AddressDetails.FirstName.ToDisplay());
                        properties.Add("request: billingAddress LastName", billing.AddressInfo.AddressDetails.LastName.ToDisplay());
                        properties.Add("request: billingAddress AddressLine1", billing.AddressInfo.AddressDetails.AddressLine1.ToDisplay());
                        properties.Add("request: billingAddress AddressLine2", billing.AddressInfo.AddressDetails.AddressLine2.ToDisplay());
                        properties.Add("request: billingAddress City", billing.AddressInfo.AddressDetails.City.ToDisplay());
                        properties.Add("request: billingAddress CountrySubdivision", billing.AddressInfo.AddressDetails.CountrySubdivision.ToDisplay());
                        properties.Add("request: billingAddress Country", billing.AddressInfo.AddressDetails.Country.ToDisplay());
                        properties.Add("request: billingAddress PostalCode", billing.AddressInfo.AddressDetails.PostalCode.ToDisplay());
                        properties.Add("request: billingAddress PhoneNumber", billing.AddressInfo.AddressDetails.PhoneNumber.ToDisplay());
                    }
                }

                properties.Add("request: hasBillingPaymentMethodObject", (billing.PaymentMethod != null).ToString());

                BuildCreditCardProperties(properties, billing.PaymentMethod.CreditCardDetails);
                BuildPayPalProperties(properties, billing.PaymentMethod.PayPalDetails);
                BuildTokenProperties(properties, billing.PaymentMethod.TokenDetails);
            }
        }

        internal static void BuildTokenProperties(Dictionary<string, string> properties, SdkModels.TokenDetails token)
        {
            properties.Add("request: hasBilling Token Object", (token != null).ToString());

            if (token != null)
            {
                properties.Add("request: TokenType ", token.PaymentTokenType.ToDisplay());
                properties.Add("request: has Token string", (!string.IsNullOrEmpty(token.PaymentToken)).ToString());
            }
        }

        internal static void BuildCreditCardProperties(Dictionary<string, string> properties, SdkModels.CreditCardDetails cc)
        {
            properties.Add("request: hasBilling CC Object", (cc != null).ToString());

            if (cc != null)
            {
                BuildCreditCardBaseProperties(properties, cc,  "CC");
                properties.Add("request: CC hasCvn", (!string.IsNullOrEmpty(cc.Cvn)).ToString());
                properties.Add("request: CC hasSurrogateNumber", (!string.IsNullOrEmpty(cc.SurrogateNumber)).ToString());
            }
        }

        internal static void BuildCreditCardBaseProperties(Dictionary<string, string> properties, SdkModels.CreditCardDetailsBase cc, string type)
        {
            if (cc != null)
            {
                properties.Add(string.Format("request: {0} Type", type), cc.Type.ToString());
                properties.Add(string.Format("request: {0} hasExpireYear", type), (cc.ExpireYear != null).ToString());
                properties.Add(string.Format("request: {0} hasExpireMonth", type), (cc.ExpireMonth != null).ToString());
                properties.Add(string.Format("request: {0} hasFullName", type), (!string.IsNullOrEmpty(cc.FullName)).ToString());
            }
        }

        internal static void BuildPayPalProperties(Dictionary<string, string> properties, SdkModels.PayPalDetails pp)
        {
            properties.Add("request: hasBilling Paypal Object", (pp != null).ToString());
            if (pp != null)
            {
                properties.Add("request: has Paypal BillAgreementId", (!string.IsNullOrEmpty(pp.BillAgreementId)).ToString());
                properties.Add("request: Paypal PayPalPayerId", pp.PayPalPayerId.ToDisplay());
                properties.Add("request: Paypal PayPayEmail", pp.PayPayEmail.ToDisplay());
                properties.Add("request: Paypal PayPayFullName", pp.PayPayFullName.ToDisplay());
                properties.Add("request: Paypal PendingReason", pp.PendingReason.ToDisplay());
            }
        }

        internal static void BuildResponseProperties(Dictionary<string, string> properties, IZeroAuthResult result)
        {
            BuildResponseProperties(properties, result as IFraudCheckResult);
            properties.Add("Result: hasCreditCardAuthInfo", (result != null && result.CreditCardAuthorizationDetails != null).ToString());
            if (result != null && result.CreditCardAuthorizationDetails != null)
            {
                var cc = result.CreditCardAuthorizationDetails;
                properties.Add("Result: Native AuthorizationDateTime", cc.AuthorizationDateTime.ToString());
                properties.Add("Result: Native AuthorizedAmount", cc.AuthorizedAmount.ToString());
                properties.Add("Result: Native MerchantReferenceCode", cc.MerchantReferenceCode.ToDisplay());
                properties.Add("Result: Native AVCode", cc.AVCode.ToDisplay());
                properties.Add("Result: Native CVCode", cc.CVCode.ToDisplay());
                properties.Add("Result: Native RequestId", cc.RequestId.ToDisplay());
                properties.Add("Result: Native ReasonCode", cc.ReasonCode != null ? cc.ReasonCode.Value.ToString() : NullDisplay);
                properties.Add("Result: Native Decision", cc.Decision.ToDisplay());
            }
        }

        internal static void BuildResponseProperties(Dictionary<string, string> properties, IFraudCheckResult result)
        {
            properties.Add("Result: hasResultObject", (result != null).ToString());
            if (result != null)
            {
                properties.Add("Result: DecisionType", result.DecisionType.ToString());
                properties.Add("Result: DecisionReasonCode", result.DecisionReason == null ? NullDisplay : result.DecisionReason.Code.ToString());
                properties.Add("Result: DecisionReasonMessage", result.DecisionReason == null ? NullDisplay : result.DecisionReason.Description.ToDisplay());
            }
        }

        internal static System.Diagnostics.TraceEventType GetLoggingLevel(IFraudCheckResult result)
        {
            if (result == null)
            {
                return System.Diagnostics.TraceEventType.Warning;
            }

            if (result.DecisionType != SdkModels.DecisionType.Accept)
            {
                return System.Diagnostics.TraceEventType.Warning;
            }

            return System.Diagnostics.TraceEventType.Information;
        }

        internal static void PerformLogging(string title, string message, System.Diagnostics.TraceEventType level, Dictionary<string, string> properties, ILogRepository logRepository)
        {
            logRepository.Log("APILog", title, message, level, properties);
        }

    }

    public static class Extionsions
    {
        public static void EnsureObject(this object input, string message)
        {
            if (input == null)
            {
                throw new ArgumentNullException(message);
            }
        }

        public static string ToDisplay(this string value)
        {
            if (value == null)
            {
                return LogPSApiCallDetails.NullDisplay;
            }

            return value;
        }
    }

}
