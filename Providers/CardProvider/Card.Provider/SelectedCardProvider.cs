﻿using System;
using System.Collections.Generic;
using System.Linq;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.Card.Provider.Common;
using Starbucks.Card.Provider.Common.Helpers;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.Card.Provider.Models;
using Starbucks.Card.Provider.Validations;

namespace Starbucks.Card.Provider
{
    public class SelectedCardProvider : ISelectedCardProvider
    {
        private readonly ISelectedCardDal _selectedCardDal;

        public SelectedCardProvider(ISelectedCardDal selectedCardDal)
        {
            if (selectedCardDal == null)
            {
                throw new ArgumentNullException("selectedCardDal");
            }
            _selectedCardDal = selectedCardDal;
        }

        public IEnumerable<string> GetSelectedCards(string userId, string clientId)
        {
            Validator.ValidateUserId(userId);
            return _selectedCardDal.GetSelectedCards(userId, clientId);
        }

        public IEnumerable<ISelectedCard> GetSelectedCards(string userId)
        {
            Validator.ValidateUserId(userId);
            IEnumerable<Dal.Common.Models.ISelectedCard> selectedCards = _selectedCardDal.GetSelectedCards(userId);

            return selectedCards.Select(c => c.Map<Dal.Common.Models.ISelectedCard, SelectedCard>());
        }

        public void AssociateSelectedCardsWithClient(string userId, string clientId, IEnumerable<string> cardIds)
        {
            var cards = new List<string>();

            ValidateSelectedCards(userId, cardIds, cards);

            _selectedCardDal.AssociateSelectedCardsWithClient(userId, clientId, cards.ToArray());
        }

        public void RemoveSelectedCards(string userId, string clientId, IEnumerable<string> cardIds)
        {
            var cards = new List<string>();

            ValidateSelectedCards(userId, cardIds, cards);
            _selectedCardDal.RemoveSelectedCards(userId, clientId, cards.ToArray());
        }

        public bool IsCardLinked(string userId, string cardId)
        {
            Validator.ValidateUserId(userId);
            Validator.ValidateCardId(cardId);
            return _selectedCardDal.IsCardLinked(userId, cardId);
        }

        private static void ValidateSelectedCards(string userId, IEnumerable<string> cardIds, List<string> cards)
        {
            foreach (string cardId in cardIds.Distinct())
            {
                Validator.ValidateUserId(userId);
                Validator.ValidateCardId(cardId);
                cards.Add(cardId);
            }
        }
    }
}