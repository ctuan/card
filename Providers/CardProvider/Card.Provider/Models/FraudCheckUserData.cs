﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Provider.Models
{
    public class FraudCheckUserData : IFraudCheckUserData
    {
        public string UserId { get; set; }
        public int AgeOfAccountSinceCreation { get; set; }
        public bool IsPartner { get; set; }
        public int BirthDay { get; set; }
        public int BirthMonth { get; set; }
        public bool eMailSignUp { get; set; }
        public bool MailSignUp { get; set; }
        public bool TextMessageSignUp { get; set; }
        public bool TwitterSignUp { get; set; }
        public bool FacebookSignUp { get; set; }

        public static FraudCheckUserData FromDalIFraudCheckUserData(Dal.Common.Models.IFraudCheckUserData userDataForFraudCheck)
        {
            if (userDataForFraudCheck == null)
            {
                return null;
            }

            return new FraudCheckUserData
            {
                UserId = userDataForFraudCheck.UserId,
                AgeOfAccountSinceCreation = userDataForFraudCheck.AgeOfAccountSinceCreation,
                IsPartner = userDataForFraudCheck.IsPartner,
                BirthDay = userDataForFraudCheck.BirthDay,
                BirthMonth = userDataForFraudCheck.BirthMonth,
                eMailSignUp = userDataForFraudCheck.eMailSignUp,
                MailSignUp = userDataForFraudCheck.MailSignUp,
                TextMessageSignUp = userDataForFraudCheck.TextMessageSignUp,
                TwitterSignUp = userDataForFraudCheck.TwitterSignUp,
                FacebookSignUp = userDataForFraudCheck.FacebookSignUp,
            };
        }
    }
}
