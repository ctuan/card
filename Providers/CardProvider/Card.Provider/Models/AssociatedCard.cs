﻿using System;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.Card.Provider.Configuration;

namespace Starbucks.Card.Provider.Models
{
    public class AssociatedCard : Card, IAssociatedCard
    {        
        public AssociatedCard(CardProviderSettingsSection cardProviderSettingsSection): base(cardProviderSettingsSection){}

        public string AutoReloadId { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public string RegistrationSource { get; set; }
        public string RegisteredUserId { get; set; }
        public string Nickname { get; set; }
        public bool IsDefault { get; set; }
        public bool IsOwner { get; set; }
        public string PlatformRegSourceCode { get; set; }
        public string MarketingRegSourceCode { get; set; }
        public IAutoReloadProfile AutoReloadProfile { get; set; }

        public static AssociatedCard FromIAssociatedCard(IAssociatedCard card, CardProviderSettingsSection cardProviderSettingsSection)
        {
            if (card == null) return null;

            return new AssociatedCard(cardProviderSettingsSection)
                {
                    Active = card.Active,
                    Balance = card.Balance,
                    BalanceCurrency = card.BalanceCurrency,
                    BalanceDate = card.BalanceDate,
                    BatchCode = card.BatchCode,
                    CardId = card.CardId,
                    CardRangeId = card.CardRangeId,
                    Class = card.Class,
                    Currency = card.Currency,
                    ExpirationDate = card.ExpirationDate,
                    IsDigitalCard = card.IsDigitalCard,
                    IsPartner = card.IsPartner ,
                    Name = card.Name,
                    Number = card.Number,
                    Pin = card.Pin,
                    PinValidated = card.PinValidated,
                    SubMarketCode = card.SubMarketCode,
                    Type = card.Type   ,
                    AutoReloadId = card.AutoReloadId ,
                    AutoReloadProfile = card.AutoReloadProfile ,
                    IsDefault = card.IsDefault ,
                    IsOwner = card.IsOwner ,
                    Nickname = card.Nickname ,
                    RegisteredUserId = card.RegisteredUserId ,
                    RegistrationDate = card.RegistrationDate ,
                    RegistrationSource = card.RegistrationSource ,
                    PlatformRegSourceCode = card.PlatformRegSourceCode,
                    MarketingRegSourceCode = card.MarketingRegSourceCode,
                    Actions = card.Actions 
                };
        }
    }
}
