﻿using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Provider.Models
{
    public class RiskWithoutIovation : IRiskWithoutIovation
    {
        public string Platform { get; set; }
        public string Market { get; set; }
        public bool? IsLoggedIn { get; set; }
        public string CcAgentName { get; set; }
    }
}
