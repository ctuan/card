﻿using System;
using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Provider.Models
{
    public class CardBalance : IStarbucksCardBalance
    {
        private decimal? _balance;        
        public decimal? Balance
        {
            get { return _balance.HasValue ? decimal.Round(_balance.Value, 2, MidpointRounding.AwayFromZero) : _balance; }
            set { _balance = value; }
        }
        public string CardId { get; set; }
        public DateTime? BalanceDate { get; set; }
        public string BalanceCurrencyCode { get; set; }
        public string CardNumber { get; set; }
    }
}
