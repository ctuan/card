﻿using Starbucks.Card.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.Card.Provider.Models
{

    public class Reputation : IReputation
    {
        public string IpAddress { get; set; }
        public string DeviceFingerprint { get; set; }
    }

    public class Risk : IRisk
    {
        public string Platform { get; set; }
        public string Market { get; set; }
        public bool? IsLoggedIn { get; set; }
        public string CcAgentName { get; set; }
        public IReputation IovationFields { get; set; }
    }
}
