﻿using System;
using System.Collections.Generic;
using Starbucks.Card.Provider.Common.Enums;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.Card.Provider.Configuration;

namespace Starbucks.Card.Provider.Models
{
    public class Card : ICard 
    {
        private readonly CardProviderSettingsSection _cardProviderSettingsSection;
        private readonly Lazy<IEnumerable<IStarbucksCardImage>>  _images;


        public Card(CardProviderSettingsSection cardProviderSettingsSection)
        {
            _cardProviderSettingsSection = cardProviderSettingsSection;
            _images = new Lazy<IEnumerable<IStarbucksCardImage>>(() => StarbucksCardImages(Name));
        }

        public bool Active { get; set; }
        public decimal? Balance { get; set; }
        public DateTime? BalanceDate { get; set; }
        public string BalanceCurrency { get; set; }
        public string CardId { get; set; }
        public CardType Type { get; set; }
        public string Class { get; set; }
        public string Currency { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string Pin { get; set; }
        public bool PinValidated { get; set; }
        public string BatchCode { get; set; }
        public string SubMarketCode { get; set; }
        public int? CardRangeId { get; set; }
        public bool IsPartner { get; set; }
        public IEnumerable<IStarbucksCardImage> CardImages { get { return _images.Value  ; } }
        public bool IsDigitalCard { get; set; }
        public IEnumerable<string> Actions { get; set; }

        private IEnumerable<IStarbucksCardImage> StarbucksCardImages(string cardName)
        {
            var list = new List<IStarbucksCardImage>
                {
                    new StarbucksCardImage
                        {
                            Type = CardImageType.ImageIcon,
                            Uri =
                                string.Format(_cardProviderSettingsSection.StarbucksCardWebImagePaths.Icon,
                                              cardName)
                        },
                    new StarbucksCardImage
                        {
                            Type = CardImageType.ImageSmall,
                            Uri =
                                string.Format(_cardProviderSettingsSection.StarbucksCardWebImagePaths.Small,
                                              cardName)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.ImageMedium,
                            Uri =
                                string.Format(_cardProviderSettingsSection.StarbucksCardWebImagePaths.Medium,
                                              cardName)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.ImageLarge,
                            Uri =
                                string.Format(_cardProviderSettingsSection.StarbucksCardWebImagePaths.Large,
                                              cardName)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.ImageStrip,
                            Uri =
                                string.Format(_cardProviderSettingsSection.StarbucksCardWebImagePaths.ImageStrip,
                                              cardName)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.iosThumb,
                            Uri =
                                string.Format(_cardProviderSettingsSection.StarbucksCardMobileImagePaths.Icon,
                                              cardName)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.iosThumbHighRes,
                            Uri =
                                string.Format(_cardProviderSettingsSection.StarbucksCardMobileImagePaths.Small,
                                              cardName)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.iosLarge,
                            Uri =
                                string.Format(_cardProviderSettingsSection.StarbucksCardMobileImagePaths.Medium,
                                              cardName)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.iosLargeHighRes,
                            Uri =
                                string.Format(_cardProviderSettingsSection.StarbucksCardMobileImagePaths.Large,
                                              cardName)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.iosImageStripMedium,
                            Uri =
                                string.Format(_cardProviderSettingsSection.StarbucksCardMobileImagePaths.ImageStripMedium,
                                              cardName)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.iosImageStripLarge,
                            Uri =
                                string.Format(_cardProviderSettingsSection.StarbucksCardMobileImagePaths.ImageStripLarge,
                                              cardName)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.androidFullMdpi,
                            Uri =
                                string.Format(_cardProviderSettingsSection.StarbucksCardAndroidImagePaths.Icon,
                                              cardName)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.androidFullHdpi,
                            Uri =
                                string.Format(_cardProviderSettingsSection.StarbucksCardAndroidImagePaths.Small,
                                              cardName)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.androidFullXhdpi,
                            Uri =
                                string.Format(_cardProviderSettingsSection.StarbucksCardAndroidImagePaths.Medium,
                                              cardName)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.androidFullXxhdpi,
                            Uri =
                                string.Format(_cardProviderSettingsSection.StarbucksCardAndroidImagePaths.Large,
                                              cardName)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.androidThumbMdpi,
                            Uri =
                                string.Format(_cardProviderSettingsSection.StarbucksCardAndroidThumbImagePaths.Icon,
                                              cardName)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.androidThumbHdpi,
                            Uri =
                                string.Format(_cardProviderSettingsSection.StarbucksCardAndroidThumbImagePaths.Small,
                                              cardName)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.androidThumbXhdpi,
                            Uri =
                                string.Format(_cardProviderSettingsSection.StarbucksCardAndroidThumbImagePaths.Medium,
                                              cardName)
                        }
                    ,
                    new StarbucksCardImage
                        {
                            Type = CardImageType.androidThumbXxhdpi,
                            Uri =
                                string.Format(_cardProviderSettingsSection.StarbucksCardAndroidThumbImagePaths.Large,
                                              cardName)
                        }
                };

            return list;
        }

        public static Card FromICard(ICard card, CardProviderSettingsSection cardProviderSettingsSection)
        {
            return new Card(cardProviderSettingsSection)
                {
                    Active = card.Active ,
                    Balance = card.Balance ,
                    BalanceCurrency = card.BalanceCurrency ,
                    BalanceDate = card.BalanceDate ,
                    BatchCode = card.BatchCode ,
                    CardId = card.CardId ,
                    CardRangeId = card.CardRangeId ,
                    Class = card.Class ,
                    Currency = card.Currency ,
                    ExpirationDate = card.ExpirationDate ,
                    IsDigitalCard = card.IsDigitalCard ,
                    IsPartner = card.IsPartner  ,
                    Name = card.Name,
                    Number = card.Number ,
                    Pin = card.Pin ,
                    PinValidated = card.PinValidated ,
                    SubMarketCode = card.SubMarketCode ,
                    Type = card.Type                  
                };
        }
    }
}
