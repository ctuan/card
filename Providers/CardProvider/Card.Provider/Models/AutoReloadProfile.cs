﻿using System;
using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Provider.Models
{
    public class AutoReloadProfile : IAutoReloadProfile
    {
        private readonly Dal.Common.Models.IAutoReloadProfile _autoReloadProfile;

        public AutoReloadProfile(Dal.Common.Models.IAutoReloadProfile autoReloadProfile)
        {
            _autoReloadProfile = autoReloadProfile;
        }

        public string UserId { get { return _autoReloadProfile.UserId; } set { _autoReloadProfile.UserId = value; } }
        public string CardId { get { return _autoReloadProfile.CardId; } set { _autoReloadProfile.CardId= value; } }
        public string AutoReloadType { get { return _autoReloadProfile.AutoReloadType; } set { _autoReloadProfile.AutoReloadType = value; } }
        public int? Day { get { return _autoReloadProfile.Day; } set { _autoReloadProfile.Day = value; } }
        public decimal? TriggerAmount { get { return _autoReloadProfile.TriggerAmount; } set { _autoReloadProfile.TriggerAmount = value; } }
        public decimal Amount { get { return _autoReloadProfile.Amount; } set { _autoReloadProfile.Amount = value; } }
        public string PaymentType { get { return _autoReloadProfile.PaymentType; } set { _autoReloadProfile.PaymentType = value; } }
        public string PaymentMethodId { get { return _autoReloadProfile.PaymentMethodId; } set { _autoReloadProfile.PaymentMethodId = value; } }
        public string AutoReloadId { get { return _autoReloadProfile.AutoReloadId; } set { _autoReloadProfile.AutoReloadId = value; } }
        public int Status { get { return _autoReloadProfile.Status; } set { _autoReloadProfile.Status = value; } }
        public DateTime? DisableUntilDate { get { return _autoReloadProfile.DisableUntilDate; } set { _autoReloadProfile.DisableUntilDate = value; } }
        public DateTime? StoppedDate { get { return _autoReloadProfile.StoppedDate; } set { _autoReloadProfile.StoppedDate = value; } }
        public string BillingAgreementId { get { return _autoReloadProfile.BillingAgreementId; } set { _autoReloadProfile.BillingAgreementId = value; } }
    }
}
