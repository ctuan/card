﻿using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Provider.Models
{
    public class StarbucksCardHeader : IStarbucksCardHeader
    {
        public string CardNumber { get; set; }
        public string CardPin { get; set; }
        public string Nickname { get; set; }         
        public string Submarket { get; set; }
        public bool Primary { get; set; }
        public bool Register { get; set; }
        public ICardRegistrationSource RegistrationSource { get; set; }
        public IRiskWithoutIovation Risk { get; set; }       
    }
}
