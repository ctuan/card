﻿using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Provider.Models
{
    public class BaseAutoReloadProfile : IBaseAutoReloadProfile
    {
        public string UserId { get; set; }
        public string CardId { get; set; }
        public string AutoReloadType { get; set; }
        public int? Day { get; set; }
        public decimal? TriggerAmount { get; set; }
        public decimal Amount { get; set; }
        public string PaymentType { get; set; }
        public string PaymentMethodId { get; set; }
    }
}
