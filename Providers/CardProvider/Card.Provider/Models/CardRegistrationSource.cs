﻿using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Provider.Models
{
    public class CardRegistrationSource : ICardRegistrationSource
    {
        public string Platform { get; set; }
        public string Marketing { get; set; }
    }
}
