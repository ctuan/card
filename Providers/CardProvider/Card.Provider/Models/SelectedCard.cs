﻿using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Provider.Models
{
    public class SelectedCard : ISelectedCard
    {
        public string UserId { get; set; }

        public string ClientId { get; set; }

        public string CardId { get; set; }
    }
}