﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Provider.Models
{
    public class FraudCheckCardData : IFraudCheckCardData
    {
        public string Nickname { get; set; }
        public bool IsDefaultSvcCard { get; set; }
        public int TotalCardsRegistered { get; set; }

        public static FraudCheckCardData FromDalIFraudCheckCardData(Dal.Common.Models.IFraudCheckCardData cardDataForFraudCheck)
        {
            if (cardDataForFraudCheck == null)
            {
                return null;
            }

            return new FraudCheckCardData
            {
                Nickname = cardDataForFraudCheck.Nickname,
                IsDefaultSvcCard = cardDataForFraudCheck.IsDefaultSvcCard,
                TotalCardsRegistered = cardDataForFraudCheck.TotalCardsRegistered,
            };
        }
    }
}
