﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.Card.Provider.Common.Enums;
using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Provider.Models
{
    public class StarbucksCardImage : IStarbucksCardImage
    {
        public CardImageType Type { get; set; }
        public string Uri { get; set; }
    }
}
