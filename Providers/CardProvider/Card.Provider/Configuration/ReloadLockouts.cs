﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Starbucks.Card.Provider.Configuration
{
    [ConfigurationCollection(typeof(ReloadLockout), AddItemName = "reloadLockout")]
    public class ReloadLockouts : ConfigurationElementCollection, IEnumerable<ReloadLockout>
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ReloadLockout();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }

            return ((ReloadLockout) element).Market;
        }

        public ReloadLockout this[int index]
        {
            get
            {
                return BaseGet(index) as ReloadLockout;
            }
        }       

        IEnumerator<ReloadLockout> IEnumerable<ReloadLockout>.GetEnumerator()
        {
            return (from i in Enumerable.Range(0, this.Count)
                    select this[i])
                .GetEnumerator();;
        }
    }
}