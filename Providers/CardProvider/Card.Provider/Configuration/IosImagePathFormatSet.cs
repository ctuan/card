﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.Card.Provider.Configuration
{
    public class IosImagePathFormatSet : ImagePathFormatSet
    {
        [ConfigurationProperty("imageStripMedium", IsRequired = false)]
        public string ImageStripMedium
        {
            get
            {
                return (string)this["imageStripMedium"];
            }
            set
            {
                this["imageStripMedium"] = value;
            }
        }

        [ConfigurationProperty("imageStripLarge", IsRequired = false)]
        public string ImageStripLarge
        {
            get
            {
                return (string)this["imageStripLarge"];
            }
            set
            {
                this["imageStripLarge"] = value;
            }
        }
    }
}
