﻿using System.Configuration;

namespace Starbucks.Card.Provider.Configuration
{
    public class ImagePathFormatSet : ConfigurationElement
    {
        [ConfigurationProperty("icon", IsRequired = false)]
        public string Icon
        {
            get
            {
                return (string)this["icon"];
            }
            set
            {
                this["icon"] = value;
            }
        }

        [ConfigurationProperty("small", IsRequired = false)]
        public string Small
        {
            get
            {
                return (string)this["small"];
            }
            set
            {
                this["small"] = value;
            }
        }

        [ConfigurationProperty("medium", IsRequired = false)]
        public string Medium
        {
            get
            {
                return (string)this["medium"];
            }
            set
            {
                this["medium"] = value;
            }
        }

        [ConfigurationProperty("large", IsRequired = false)]
        public string Large
        {
            get
            {
                return (string)this["large"];
            }
            set
            {
                this["large"] = value;
            }
        }
    }
}