﻿using System.Configuration;

namespace Starbucks.Card.Provider.Configuration
{
    public class WebImagePathFormatSet : ImagePathFormatSet
    {
        [ConfigurationProperty("imageStrip", IsRequired = false)]
        public string ImageStrip
        {
            get
            {
                return (string)this["imageStrip"];
            }
            set
            {
                this["imageStrip"] = value;
            }
        }
    }
}