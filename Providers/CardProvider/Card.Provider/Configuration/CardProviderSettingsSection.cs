﻿using System.Configuration;

namespace Starbucks.Card.Provider.Configuration
{
    public class CardProviderSettingsSection : ConfigurationSection
    {       
        [ConfigurationProperty("teavanaRegSourceKey")]
        public string TeavanaRegSourceKey
        {
            get { return (string) this["teavanaRegSourceKey"]; }
            set { this["teavanaRegSourceKey"] = value; }
        }

        [ConfigurationProperty("teavanaPromoCode")]
        public string TeavanaPromoCode
        {
            get { return (string)this["teavanaPromoCode"]; }
            set { this["teavanaPromoCode"] = value; }
        }

        [ConfigurationProperty("goldCardClass")]
        public string GoldCardClass
        {
            get { return (string)this["goldCardClass"]; }
            set { this["goldCardClass"] = value; }
        }

         
        [ConfigurationProperty("starbucksCardWebImagePaths")]
        public WebImagePathFormatSet StarbucksCardWebImagePaths
        {
            get
            {
                return (WebImagePathFormatSet)this["starbucksCardWebImagePaths"];
            }
            set
            {
                this["starbucksCardWebImagePaths"] = value;
            }
        }

        [ConfigurationProperty("starbucksCardMobileImagePaths")]
        public IosImagePathFormatSet StarbucksCardMobileImagePaths
        {
            get
            {
                return (IosImagePathFormatSet)this["starbucksCardMobileImagePaths"];
            }
            set
            {
                this["starbucksCardMobileImagePaths"] = value;
            }
        }

        [ConfigurationProperty("starbucksCardAndroidImagePaths")]
        public ImagePathFormatSet StarbucksCardAndroidImagePaths
        {
            get
            {
                return (ImagePathFormatSet)this["starbucksCardAndroidImagePaths"];
            }
            set
            {
                this["starbucksCardAndroidImagePaths"] = value;
            }
        }

        [ConfigurationProperty("starbucksCardAndroidThumbImagePaths")]
        public ImagePathFormatSet StarbucksCardAndroidThumbImagePaths
        {
            get
            {
                return (ImagePathFormatSet)this["starbucksCardAndroidThumbImagePaths"];
            }
            set
            {
                this["starbucksCardAndroidThumbImagePaths"] = value;
            }
        }        

        [ConfigurationProperty("reloadLockouts")]
        public ReloadLockouts ReloadLockouts 
        {
            get { return base["reloadLockouts"] as ReloadLockouts; }
        }
    }
}
