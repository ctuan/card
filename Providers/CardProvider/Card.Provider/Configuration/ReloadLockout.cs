﻿using System.Configuration;

namespace Starbucks.Card.Provider.Configuration
{
    public class ReloadLockout : ConfigurationElement 
    {
        [ConfigurationProperty("interval")]
        public int LockOutTime
        {
            get { return (int)this["interval"]; }
            set { this["interval"] = value; }
        }

        [ConfigurationProperty("market")]
        public string Market
        {
            get { return (string)this["market"]; }
            set { this["market"] = value; }
        }       
    }
}