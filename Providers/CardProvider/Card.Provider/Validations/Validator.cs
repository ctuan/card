﻿using Starbucks.Card.Provider.Common.ErrorResources;
using Starbucks.Card.Provider.Common.Exceptions;
using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.Card.Provider.Validations
{
    internal class Validator
    {
        public static void ValidateCardNumber(string cardNumber)
        {
            if (string.IsNullOrEmpty(cardNumber))
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationCardNumberMissingCode, CardProviderValidationErrorResource.ValidationCardNumberMissingMessage);

            if (cardNumber.Length != 16)
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationCardNumberMissingCode, CardProviderValidationErrorResource.ValidationCardNumberMissingMessage);
        }

        public static void ValidateCardPin(string cardPin)
        {
            if (string.IsNullOrEmpty(cardPin))
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationCardPinMissingCode, CardProviderValidationErrorResource.ValidationCardPinMissingMessage);

            if (cardPin.Length != 8)
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationCardPinMissingCode, CardProviderValidationErrorResource.ValidationCardPinMissingMessage);
        }

        public static void ValidateCardAndPin(string cardNumber, string pinNumber)
        {
            ValidateCardNumber(cardNumber);
            ValidateCardPin(pinNumber);
        }

        public static void ValidateUserId(string userId)
        {
            if (string.IsNullOrEmpty( userId))
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationCardNoUserCode, CardProviderValidationErrorResource.ValidationCardNoUserMessage);
        }

        public static void ValidateCardId(string cardId)
        {
            if (string.IsNullOrEmpty(cardId ))
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationCardCardIdMissingCode, CardProviderValidationErrorResource.ValidationCardCardIdMissingMessage);
        }

        public static void ValidateReload(IReloadForStarbucksCard reloadForStarbucksCard)
        {
            if (string.IsNullOrEmpty(reloadForStarbucksCard.PaymentMethodId))
            {
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationNoPaymentMethodCode, CardProviderValidationErrorResource.ValidationNoPaymentMethodMessage);
            }

            if (reloadForStarbucksCard.Amount <= 0)
            {
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationNoReloadAmountCode, CardProviderValidationErrorResource.ValidationNoReloadAmountMessage);
            }
        }        

        public static void ValidateTransferBalance(string userId, string sourceCardId, string destinationCardId)
        {
            ValidateUserId(userId);

            if (string.IsNullOrEmpty(sourceCardId))
            {
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationNoSourceCardIdCode, CardProviderValidationErrorResource.ValidationNoSourceCardIdMessage);
            }


            if (string.IsNullOrEmpty(destinationCardId))
            {
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationNoDestinationCardIdCode, CardProviderValidationErrorResource.ValidationNoDestinationCardIdMessage);
            }
        }

        public static void ValidateCreateAutoReload(IAutoReloadProfile autoReloadProfile)
        {
            const string amount = "Amount";
            const string date = "Date";

            const int minDay = 1;
            const int maxDay = 31;

            const decimal minReloadAmount = 10;
            const decimal maxReloadAmount = 100;


            if (autoReloadProfile == null)
            {
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationNoRequestCode, CardProviderValidationErrorResource.ValidationNoRequestMessage);
            }

            if (string.IsNullOrEmpty(autoReloadProfile.PaymentMethodId))
            {
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationNoPaymentMethodCode, CardProviderValidationErrorResource.ValidationNoPaymentMethodMessage);
            }

            if (string.IsNullOrEmpty(autoReloadProfile.AutoReloadType))
            {
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationNoAutoReloadTypeCode, CardProviderValidationErrorResource.ValidationNoAutoReloadTypeMessage);
            }
            

            if (autoReloadProfile.AutoReloadType != amount && autoReloadProfile.AutoReloadType != date)
            {
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationNoAutoReloadTypeCode, CardProviderValidationErrorResource.ValidationNoAutoReloadTypeMessage);
            }

            if (!(autoReloadProfile.Amount >= minReloadAmount && autoReloadProfile.Amount <= maxReloadAmount))
            {
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationNoAutoReloadAmountCode, CardProviderValidationErrorResource.ValidationNoAutoReloadAmountMessage);
            }

            if (autoReloadProfile.AutoReloadType == date && (!autoReloadProfile.Day.HasValue || !(autoReloadProfile.Day >= minDay && autoReloadProfile.Day <= maxDay)))
            {
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationNoAutoReloadDayCode, CardProviderValidationErrorResource.ValidationNoAutoReloadDayMessage);
            }

            if (autoReloadProfile.AutoReloadType == amount && !autoReloadProfile.TriggerAmount.HasValue)
            {
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationNoAutoReloadTriggerAmountCode, CardProviderValidationErrorResource.ValidationNoAutoReloadTriggerAmountMessage);
            }
        }

        public static void ValidateUserIdCardId(string userId, string cardId)
        {
            ValidateCardId(cardId);
            ValidateUserId(userId);
        }

        public static void ValidateRegistrationCardAndPin(string cardNumber, string pinNumber)
        {
            if (string.IsNullOrEmpty(cardNumber))
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationRegisterMultipleNoCardNumberCode, CardProviderValidationErrorResource.ValidationRegisterMultipleNoCardNumberMessage);

            if (cardNumber.Length != 16)
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationRegisterMultipleNoCardNumberCode, CardProviderValidationErrorResource.ValidationRegisterMultipleNoCardNumberMessage);

            if (string.IsNullOrEmpty(pinNumber))
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationRegisterMultipleNoPinCode, CardProviderValidationErrorResource.ValidationRegisterMultipleNoPinMessage);

            if (pinNumber.Length != 8)
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationRegisterMultipleNoPinCode, CardProviderValidationErrorResource.ValidationRegisterMultipleNoPinMessage);
        }

        public static void ValidateNickname(string nickname)
        {
            if (string.IsNullOrEmpty(nickname))
                throw new CardProviderValidationException(CardProviderValidationErrorResource.ValidationNoNicknameCode, CardProviderValidationErrorResource.ValidationNoNicknameMessage);
        }
    }
}
