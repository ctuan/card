﻿using System;
using System.Collections.Generic;
using Starbucks.CardIssuer.Dal.Common.Models;

namespace Starbucks.CardIssuer.Dal.Common
{
    public interface ICardIssuerDal
    {
        /// Activates a card.
        ICardTransaction Activate(IMerchantInfo merchantInfo, string cardNumber, string pin, decimal amount);

        ICardTransaction ActivateVirtual(IMerchantInfo merchantInfo, string promoCode, decimal amount);

        /// Removes remaining balance on card.  For use in lost/stolen scenarios.
        ICardTransaction CashOut(IMerchantInfo merchantInfo, string cardNumber, string pin, decimal amount);

        /// Disables the card's PIN so it can no longer be used for online transactions.
        ICardTransaction DisablePin(IMerchantInfo merchantInfo, string cardNumber, string pin);

        /// Sends a call to CallInteractive to retrieve Balance in base currency of card
        ICardTransaction GetBalance(IMerchantInfo merchantInfo, string cardNumber, string pin);

        List<ICardTransaction> GetHistory(IMerchantInfo merchantInfo, string cardNumber, string pin);
        List<ICardTransaction> GetHistoryCondensed(IMerchantInfo merchantInfo, string cardNumber, string pin = null);

        List<ICardTransaction> GetHistoryPinless(IMerchantInfo merchantInfo, string cardNumber);

        //Move to other provider?
        IMerchantInfo GetMerchantInfo(string subMarket, string platform = "unknown");
        IMerchantInfo GetMerchantInfoByMerchantId(string merchantId, string platform = "unknown");
        IMerchantInfo GetMerchantInfoByCurrency(string currency);
        IOrderAddressInfo GetOrderAddressInfo(string orderId);

        /// Redeem an amount from the card.
        ICardTransaction Redeem(IMerchantInfo merchantInfo, string cardNumber, string pin, decimal amount);

        /// Reload the card with a given amount.
        ICardTransaction Reload(IMerchantInfo merchantInfo, string cardNumber, decimal amount);

        ICardTransaction Reload(IMerchantInfo merchantInfo, string cardNumber, string pin, decimal amount);
        ICardTransaction Reload(IMerchantInfo merchantInfo, string cardNumber, string pin, decimal amount, bool isAutoreload);

        /// Sends a call to CallInteractive to save the Merchant Working Key (MWK) - used for PIN encryption -
        ICardTransaction SendMwk(IMerchantInfo merchantInfo);

        /// <summary>Sends a request to ValueLink to charge a card with the amount of a tip.</summary>
        ICardTransaction Tip(IMerchantInfo merchantInfo, string cardNumber, decimal amount, DateTime localTransactionTime, long localTransactionId);

        /// Transfers full balance of one card to another card.  Transaction is PIN-less.
        ICardTransaction TransferBalance(IMerchantInfo merchantInfo, string cardNumberFrom, string cardNumberTo);

        // sends a call to lock the current balance (used by bulk data closure as <freeze>)
        // value link parameter pattern is the same as Balance (2400)
        ICardTransaction BalanceLock(IMerchantInfo merchantInfo, string cardNumber, string pin);

        // sends a call to unlock the balance (used by bulk data closure as <unfreeze>)
        // value link parameter pattern is the same as Redeem (2202)
        ICardTransaction RedemptionUnlock(IMerchantInfo merchantInfo, string cardNumber, string pin, decimal amount);

        long UpsertPartialTransferTransactionDetails(long cardAmountTransactionId, string toCardTransactionId,
                                                    DateTime toCardTransactionDate
                                                    , string fromCardTransactionId,
                                                    DateTime? fromCardTransactionDate);

        /// Sends a void request to ValueLink.
        ICardTransaction VoidRequest(ICardTransaction voidTransaction, IMerchantInfo merchInfo);

        ICardTransaction VoidRequest(Enums.CallItCommand command, IMerchantInfo merchInfo, long transactionId);
    }
}