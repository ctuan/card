﻿namespace Starbucks.CardIssuer.Dal.Common.Models
{
    public interface IOrderAddressInfo
    {
        /// <summary>
        /// Indicator that type if the address.
        /// </summary>
        int AddressType { get; set; }

        /// <summary>
        /// Indicator the first name.
        /// </summary>
        string FirstName { get; set; }

        /// <summary>
        /// Indicator the last name.
        /// </summary>
        string LastName { get; set; }

        /// <summary>
        /// Indicator address line 1
        /// </summary>
        string AddressLine1 { get; set; }

        /// <summary>
        /// Indicator address line 2
        /// </summary>
        string AddressLine2 { get; set; }

        /// <summary>
        /// Indicator city
        /// </summary>
        string City { get; set; }

        /// <summary>
        /// Indicator Region
        /// </summary>
        string Region { get; set; }

        /// <summary>
        /// Indicator Country
        /// </summary>
        string Country { get; set; }

        /// <summary>
        /// Phone Number
        /// </summary>
        string PhoneNumber { get; set; }

        /// <summary>
        /// Postal Code
        /// </summary>
        string PostalCode { get; set; }
    }
}
