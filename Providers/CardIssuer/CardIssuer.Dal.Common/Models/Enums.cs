﻿namespace Starbucks.CardIssuer.Dal.Common.Models
{
    public class Enums
    {
        public enum CallItCommand
        {
            Balance = 2400,
			BalanceLock = 2401,  //used by bulk data closure (freeze).   valuelink parameters are the same as Balance (2400)

			History = 2410,
            Reload = 2300,

            AutomaticReload = 2301,
			RedemptionUnlock = 2201,  //used by bulk data closure (unfreeze).  valuelink parameters are the same as Redeem (2202)

            //Only AR uses this for calling ValueLink, but it gets returned in the transaction history.
            Redeem = 2202,
            HistoryCondensed = 2413,
            BalanceMerge = 2420,
            Cashout = 2600,
            Disable = 2150,
            ActivatePhysical = 2104,
            Rollback = 704,
            Activation = 100, //These 3-digit codes are for POS transactions in History.
            InStorePurchase = 202,
            InStoreReload = 300,
            InStoreReload2 = 301, //Reload without card -- don't know if this is used
            LoyaltyRebate = 302,
            LoyaltyReward = 303,
            LoyaltyCustomerReturn = 304,
            InStoreTransfer = 420,
            Encrypt = 2010,
            Activate = 2102,
            VoidOfRedemption = 2800,
            VoidOfReload = 2801,
            VoidOfActivation = 2802,
            VoidOfBalanceMerge = 2806
        }
    }
}
