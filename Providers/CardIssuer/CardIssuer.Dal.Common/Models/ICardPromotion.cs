﻿namespace Starbucks.CardIssuer.Dal.Common.Models
{
    public interface ICardPromotion
    {
        string Code { get; set; }
        decimal Amount { get; set; }
        string Message { get; set; }
    }
}
