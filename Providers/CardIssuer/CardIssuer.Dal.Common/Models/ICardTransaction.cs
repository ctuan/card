﻿using System;

namespace Starbucks.CardIssuer.Dal.Common.Models
{
    public interface ICardTransaction
    {
        string CardId { get; set; }
        string TransactionId { get; set; }
        string AuthorizationCode { get; set; }
        string CardClass { get; set; }
        decimal Amount { get; set; }
        string Currency { get; set; }
        string Description { get; set; }
        DateTime TransactionDate { get; set; }
        decimal BeginningBalance { get; set; }
        decimal EndingBalance { get; set; }
        string RequestCode { get; set; }
        string ResponseCode { get; set; }
        bool TransactionSucceeded { get; }
        ICardPromotion Promotion { get; set; }
        string Pin { get; set; }
        string CardNumber { get; set; }
        string BaseCurrency { get; set; }
        decimal BeginingBalanceInBaseCurrency { get; set; }
        decimal EndingBalanceInBaseCurrency { get; set; }
        decimal AmountInBaseCurrency { get; set; }
        string MerchantId { get; set; }
        string TerminalId { get; set; }
        string StoreId { get; set; }
        DateTime UtcDate { get; set; }
		decimal LockAmount { get; set; }
	}
}
