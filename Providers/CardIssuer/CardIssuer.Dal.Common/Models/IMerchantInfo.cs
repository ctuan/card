﻿namespace Starbucks.CardIssuer.Dal.Common.Models
{
    public interface IMerchantInfo
    {       
            /// <summary>
            /// Indicator that Encryption ID and keys are included.
            /// </summary>
            bool HasEncryption { get; set; }

            /// <summary>
            /// A 4-digit unique ID for the encryption key, 
            /// used when making ValueLink calls so they can identify
            /// the key used for PIN encyryption.
            /// </summary>
            string EncryptionId { get; set; }

            /// <summary>
            /// The ValueLink encryption key
            /// </summary>
            string Mwk { get; set; }

            /// <summary>
            /// The extended encryption key
            /// </summary>
            string Emwk { get; set; }

            /// <summary>
            /// The ISO 4217 alpha currency code
            /// </summary>
            string CurrencyCode { get; set; }

            /// <summary>
            /// The ISO 4217 numeric currency code
            /// </summary>
            string CurrencyNumber { get; set; }

            /// <summary>
            /// The Merchant ID for ValueLink transactions
            /// </summary>
            string Mid { get; set; }

            /// <summary>
            /// An alternate merchant ID.  This is usually "1" for our purposes.
            /// </summary>
            string AlternateMid { get; set; }

            /// <summary>
            /// Market ID, such as North America - "NoAm"
            /// </summary>
            string MarketId { get; set; }

            /// <summary>
            /// The submarket ID, such as "US"
            /// </summary>
            string SubMarketId { get; set; }

            /// <summary>
            /// A ValueLink customer-defined value for the terminal used in 
            /// a transaction.  Usually "0001" for web transactions.
            /// </summary>
            string TerminalId { get; set; }

            /// <summary>
            /// Merchant key, sometimes referred to as merchant id. This is the string not the numerical value (e.g. starbucks1)
            /// </summary>
            string MerchantKey { get; set; }

            /// <summary>
            /// Key used as password for connecting to CyberSource Soap service
            /// </summary>
            string TransactionSecurityKey { get; set; }

            /// <summary>
            /// The fraud threshold for the transaction.
            /// </summary>
            string FraudThreshold { get; set; }
        
    }
}
