﻿using System;

namespace Starbucks.CardIssuer.Dal.Common
{
    public class CardIssuerException : Exception 
    {
        public string Code { get; private set; }

        public CardIssuerException(string code, string message) : this  (code, message, null)
        {            
        }

        public CardIssuerException(string code, string message, Exception innerException)
            : base(message, innerException )
        {
            Code = code;
        }

    }
}
