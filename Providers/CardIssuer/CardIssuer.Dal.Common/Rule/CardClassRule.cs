﻿namespace Starbucks.CardIssuer.Dal.Common.Rule
{
    public class CardClassRule
    {
        public string Class { get; set; }
        public CardRule Rule { get; set; }
    }
}