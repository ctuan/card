﻿using System.Collections.Generic;
using System.Linq;

namespace Starbucks.CardIssuer.Dal.Common.Rule
{
    public class Rules
    {
        private static readonly Dictionary<string, IList<CardClassRule>> CardClassRules = new Dictionary<string, IList<CardClassRule>>(); 
        static Rules()
        {
            foreach (CardClassRuleConfigurationElement  element in CardClassRuleConfigurationSettings.Settings.CardClassRuleConfigurationElements)
            {
                var cardClassRule = new CardClassRule()
                    {
                        Class = element.Class,
                        Rule = new CardRule() {RuleName = element.RuleName, CanExecute = element.CanExecute }
                    };               

                if (CardClassRules.ContainsKey(cardClassRule.Class))
                {
                    CardClassRules[cardClassRule.Class].Add(cardClassRule);
                }
                else
                {
                    CardClassRules[cardClassRule.Class] = new List<CardClassRule>() {cardClassRule};
                }
            }
        }

        //private const string ServiceRecoveryCard = "254";
        private const string CanReloadString = "CanReload";
        private const string CanTransferToString = "CanTransferTo";
        private const string CanTransferFromString = "CanTransferFrom";
        private const string CanRegisterString = "CanRegister";

        public bool CanReload(string classId)
        {
            if (!CardClassRules.ContainsKey(classId)) return true;

            var cardClassRule =
                CardClassRules[classId].SingleOrDefault(ccr => ccr.Rule != null && ccr.Rule.RuleName == CanReloadString);

            return cardClassRule == null || cardClassRule.Rule.CanExecute;
        }

        public bool CanTransferTo(string classId)
        {
            if (!CardClassRules.ContainsKey(classId)) return true;

            var cardClassRule =
                CardClassRules[classId].SingleOrDefault(ccr => ccr.Rule != null && ccr.Rule.RuleName == CanTransferToString);

            return cardClassRule == null || cardClassRule.Rule.CanExecute;
        }

        public bool CanTransferFrom(string classId)
        {
            if (!CardClassRules.ContainsKey(classId)) return true;

            var cardClassRule =
                CardClassRules[classId].SingleOrDefault(ccr => ccr.Rule != null && ccr.Rule.RuleName == CanTransferFromString);

            return cardClassRule == null || cardClassRule.Rule.CanExecute;
        }

        public bool CanRegister(string classId)
        {
            if (!CardClassRules.ContainsKey(classId)) return true;

            var cardClassRule =
                CardClassRules[classId].SingleOrDefault(ccr => ccr.Rule != null && ccr.Rule.RuleName == CanRegisterString);

            return cardClassRule == null || cardClassRule.Rule.CanExecute;
        }

    }
}
