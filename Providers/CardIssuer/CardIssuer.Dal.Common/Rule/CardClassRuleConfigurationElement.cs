﻿using System.Configuration;

namespace Starbucks.CardIssuer.Dal.Common.Rule
{
    public class CardClassRuleConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("class", IsRequired = true)]
        public string Class
        {
            get { return (string)this["class"]; }
            set { this["class"] = value; }
        }

        [ConfigurationProperty("ruleName", IsRequired = true)]
        public string RuleName
        {
            get { return (string)this["ruleName"]; }
            set { this["ruleName"] = value; }
        }

        [ConfigurationProperty("canExecute", IsRequired = true)]
        public bool CanExecute
        {
            get { return (bool)this["canExecute"]; }
            set { this["canExecute"] = value; }
        }
    }
}