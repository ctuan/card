﻿using System.Configuration;

namespace Starbucks.CardIssuer.Dal.Common.Rule
{
    public class CardClassRuleConfigurationSettings : ConfigurationSection
    {
        private static CardClassRuleConfigurationSettings _settings;
        public static CardClassRuleConfigurationSettings Settings
        {
            get
            {
                return _settings ??
                       (_settings =
                        ConfigurationManager.GetSection("cardClassRuleConfigurationSettings") as CardClassRuleConfigurationSettings);
            }
        }

        [ConfigurationProperty("rules", IsRequired = true)]
        public CardClassRuleConfigurationElements CardClassRuleConfigurationElements
        {
            get
            {
                return base["rules"] as CardClassRuleConfigurationElements;
            }
        }
    }
}