﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Starbucks.CardIssuer.Dal.Common.Rule
{
    [ConfigurationCollection( typeof( CardClassRuleConfigurationElement ), AddItemName = "rule" )]
    public class CardClassRuleConfigurationElements : ConfigurationElementCollection, IEnumerable<CardClassRuleConfigurationElement>
    {

        protected override ConfigurationElement CreateNewElement()
        {
            return new CardClassRuleConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            var lConfigElement = element as CardClassRuleConfigurationElement;
            if (lConfigElement != null)
                return lConfigElement.Class + lConfigElement.RuleName ;
            else
                return null;
        }

        public CardClassRuleConfigurationElement this[int index]
        {
            get
            {
                return BaseGet(index) as CardClassRuleConfigurationElement;
            }
        }       

        IEnumerator<CardClassRuleConfigurationElement> IEnumerable<CardClassRuleConfigurationElement>.GetEnumerator()
        {
            return (from i in Enumerable.Range(0, this.Count)
                    select this[i])
                .GetEnumerator();
        }
    }
}