﻿namespace Starbucks.CardIssuer.Dal.Common.Rule
{
    public class CardRule
    {
        public string RuleName { get; set; }
        public bool CanExecute { get; set; }
    }
}