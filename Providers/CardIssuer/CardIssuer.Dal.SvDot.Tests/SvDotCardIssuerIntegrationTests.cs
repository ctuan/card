﻿using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.SvDot.Configuration;
using Starbucks.CardIssuer.Dal.SvDot.Db;
using Starbucks.CardIssuer.Dal.SvDot.Requestor;
using Starbucks.CardIssuer.Dal.SvDot.TransportModels;
using Starbucks.LogCounter.Provider;
using Starbucks.MessageBroker.CardNotificationListener;
using Starbucks.MessageBroker.Common;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Starbucks.CardIssuer.Dal.SvDot.Tests
{
    [TestClass]
    [Ignore]
    public class SvDotCardIssuerIntegrationTests
    {
        private const string UnitedStatesTippingMarketId = "99030169997";
        private const string USMid = "99910439997";

        [TestMethod]
        [TestCategory("Integration")]
        public void ReloadPos()
        {
            const string cardNumber = "7777005326776374";
            const string storeId = "00303";
            const string terminalId = "0001";

            var amount = 1.11M;

            var listeners = new List<Lazy<IMessageListener, ListenerMetaData>>
                {
                    new Lazy<IMessageListener, ListenerMetaData>(
                        () => new CardNotificationListener("cardNotificationListenerEndPoint"), new ListenerMetaData())
                };

            var cardIssuer = new SvDotCardIssuer(new ServiceRequestor(
                                                                                                                                                                 new CardTransactionDal("CardTransactions", "Commerce"),
                                                                                                                                                                 new SvDotTransportXml(ConfigurationManager.GetSection("svDotConfiguration") as SvDotConfigurationSettings),
                                                                                                                                                                 ConfigurationManager.GetSection("svDotConfiguration") as SvDotConfigurationSettings, LogCounterManager.Instance()),
                                                 new CardTransactionDal("CardTransactions", "Commerce"),
                                                 new MessageBroker.MessageBroker(listeners), ConfigurationManager.GetSection("svDotConfiguration") as SvDotConfigurationSettings, new Starbucks.Settings.Provider.SettingsProvider(), new CardTransactionDal("CardTransactions", "Commerce"), LogCounterManager.Instance());
            IMerchantInfo merchantInfo = cardIssuer.GetMerchantInfo("us", "web");
            merchantInfo.AlternateMid = storeId;
            merchantInfo.Mid = UnitedStatesTippingMarketId;
            merchantInfo.TerminalId = terminalId;

            ICardTransaction transaction = cardIssuer.ReloadInStore(merchantInfo, cardNumber, amount);

            // Sleep for a second so that the thread that writes the notification to the msmq isn't killed before the test ends.
            System.Threading.Thread.Sleep(1000);

            Assert.AreEqual(cardNumber, transaction.CardNumber);
            Assert.AreEqual(amount, Math.Abs(transaction.Amount));
            Assert.AreEqual(merchantInfo.AlternateMid, transaction.StoreId);
            Assert.AreEqual(merchantInfo.Mid, transaction.MerchantId);
            Assert.AreEqual(merchantInfo.TerminalId, transaction.TerminalId);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void GetNewHistory()
        {
            const string cardNumber = "7777005326776374";
            const string storeId = "00303";
            const string terminalId = "0001";

            var listeners = new List<Lazy<IMessageListener, ListenerMetaData>>
                {
                    new Lazy<IMessageListener, ListenerMetaData>(
                        () => new CardNotificationListener("cardNotificationListenerEndPoint"), new ListenerMetaData())
                };

            var cardIssuer = new SvDotCardIssuer(new ServiceRequestor(
                                                                                                                                                                 new CardTransactionDal("CardTransactions", "Commerce"),
                                                                                                                                                                 new SvDotTransportXml(ConfigurationManager.GetSection("svDotConfiguration") as SvDotConfigurationSettings),
                                                                                                                                                                 ConfigurationManager.GetSection("svDotConfiguration") as SvDotConfigurationSettings, LogCounterManager.Instance()),
                                                 new CardTransactionDal("CardTransactions", "Commerce"),
                                                 new MessageBroker.MessageBroker(listeners), ConfigurationManager.GetSection("svDotConfiguration") as SvDotConfigurationSettings, new Starbucks.Settings.Provider.SettingsProvider(), new CardTransactionDal("CardTransactions", "Commerce"), LogCounterManager.Instance());
            IMerchantInfo merchantInfo = cardIssuer.GetMerchantInfo("US", "web");
            merchantInfo.AlternateMid = storeId;
            merchantInfo.Mid = USMid;
            merchantInfo.TerminalId = terminalId;

            List<ICardTransaction> transaction = cardIssuer.GetHistoryCondensed(merchantInfo, cardNumber);

            // Sleep for a second so that the thread that writes the notification to the msmq isn't killed before the test ends.
            System.Threading.Thread.Sleep(1000);

        }



        [TestMethod]
        [TestCategory("Integration")]
        public void Tip()
        {
            // All of these values should match the values of the redemption transaction except for the Amount.
            const string cardNumber = "7777005326776374";
            const string storeId = "00303";
            const string terminalId = "0001"; //This is a 4-digit number. In the xml it is the last 4 digits of the TerminalId.  The first 11 digits are the MerchantId.
            var testData = new[]
                {
                    new
                        {
                            CardNumber = cardNumber,
                            StoreId = storeId,
                            TerminalId = terminalId,
                            LocalTransactionTime = new DateTime(2013, 07, 26, 06, 57, 21),
                            LocalTransactionId = 0001076,
                            Amount = 1.11m,
                        },
                    //new
                    //    {
                    //        CardNumber = cardNumber,
                    //        StoreId = storeId,
                    //        TerminalId = terminalId,
                    //        LocalTransactionTime = new DateTime(2013, 07, 16, 10, 10, 10),
                    //        LocalTransactionId = 0001014,
                    //        Amount = 2.22m,
                    //    },
                    //new 
                    //    {
                    //        CardNumber = cardNumber,
                    //        StoreId = storeId,
                    //        TerminalId = terminalId,
                    //        LocalTransactionTime = new DateTime(2013, 07, 16, 10, 10, 31),
                    //        LocalTransactionId = 0001015,
                    //        Amount = 3.33m,
                    //    },
                    //new 
                    //    {
                    //        CardNumber = cardNumber,
                    //        StoreId = storeId,
                    //        TerminalId = terminalId,
                    //        LocalTransactionTime = new DateTime(2013, 07, 16, 10, 12, 17),
                    //        LocalTransactionId = 0001017,
                    //        Amount = 4.44m,
                    //    },
                    //new 
                    //    {
                    //        CardNumber = cardNumber,
                    //        StoreId = storeId,
                    //        TerminalId = terminalId,
                    //        LocalTransactionTime = new DateTime(2013, 07, 16, 10, 13, 18),
                    //        LocalTransactionId = 0001018,
                    //        Amount = 5.55m,
                    //    },
                };

            foreach (var data in testData)
            {
                SendTestTip(data.CardNumber, data.StoreId, data.TerminalId, data.Amount, data.LocalTransactionTime, data.LocalTransactionId);
            }
        }

        private static void SendTestTip(string cardNumber, string storeId, string terminalId, decimal amount, DateTime localTransactionTime, int localTransactionId)
        {
            var listeners = new List<Lazy<IMessageListener, ListenerMetaData>>
                {
                    new Lazy<IMessageListener, ListenerMetaData>(
                        () => new CardNotificationListener("cardNotificationListenerEndPoint"), new ListenerMetaData())
                };

            var cardIssuer = new SvDotCardIssuer(new ServiceRequestor(
                                                                                                                                                                  new CardTransactionDal("CardTransactions", "Commerce"),
                                                                                                                                                                  new SvDotTransportXml(ConfigurationManager.GetSection("svDotConfiguration") as SvDotConfigurationSettings),
                                                                                                                                                                  ConfigurationManager.GetSection("svDotConfiguration") as SvDotConfigurationSettings, LogCounterManager.Instance()),
                                                  new CardTransactionDal("CardTransactions", "Commerce"),
                                                  new MessageBroker.MessageBroker(listeners), ConfigurationManager.GetSection("svDotConfiguration") as SvDotConfigurationSettings, new Starbucks.Settings.Provider.SettingsProvider(), new CardTransactionDal("CardTransactions", "Commerce"), LogCounterManager.Instance());
            IMerchantInfo merchantInfo = cardIssuer.GetMerchantInfo("us", "web");
            merchantInfo.AlternateMid = storeId;
            merchantInfo.Mid = UnitedStatesTippingMarketId;
            merchantInfo.TerminalId = terminalId;

            ICardTransaction transaction = cardIssuer.Tip(merchantInfo, cardNumber, amount, localTransactionTime,
                                                          localTransactionId);

            // Sleep for a second so that the thread that writes the notification to the msmq isn't killed before the test ends.
            System.Threading.Thread.Sleep(1000);

            Assert.AreEqual(cardNumber, transaction.CardNumber);
            Assert.AreEqual(amount, Math.Abs(transaction.Amount));
            Assert.AreEqual(localTransactionTime, transaction.TransactionDate);
            Assert.AreEqual(localTransactionId.ToString(CultureInfo.InvariantCulture), transaction.TransactionId);
            Assert.AreEqual(merchantInfo.AlternateMid, transaction.StoreId);
            Assert.AreEqual(merchantInfo.Mid, transaction.MerchantId);
            Assert.AreEqual(merchantInfo.TerminalId, transaction.TerminalId);
        }
    }
}
