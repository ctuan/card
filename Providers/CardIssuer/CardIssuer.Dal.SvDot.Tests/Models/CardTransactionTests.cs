﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.CardIssuer.Dal.SvDot.Models;

namespace Starbucks.CardIssuer.Dal.SvDot.Tests.Models
{
    [TestClass]
    public class CardTransactionTests
    {
        private readonly RandomDataGenerator dataGenerator = new RandomDataGenerator();

        [TestMethod]
        public void MerchantId_WhenMerchantIdAndTerminalIdIsSet_MerchantIdReturnsExpectedValue()
        {
            string merchantId = dataGenerator.GenerateMerchantId();
            string terminalId = dataGenerator.GenerateTerminalId();
            var cardTransaction = new CardTransaction
                {
                    MerchantIdAndTerminalId = merchantId + terminalId,
                };

            string expected = merchantId;
            string actual = cardTransaction.MerchantId;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TerminalId_WhenMerchantIdAndTerminalIdIsSet_TerminalIdReturnsExpectedValue()
        {
            string merchantId = dataGenerator.GenerateMerchantId();
            string terminalId = dataGenerator.GenerateTerminalId();
            var cardTransaction = new CardTransaction
            {
                MerchantIdAndTerminalId = merchantId + terminalId,
            };

            string expected = terminalId;
            string actual = cardTransaction.TerminalId;
            Assert.AreEqual(expected, actual);
        }
    }
}
