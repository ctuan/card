﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.SvDot.Common;
using Starbucks.CardIssuer.Dal.SvDot.Db;

namespace Starbucks.CardIssuer.Dal.SvDot.Tests
{
    [TestClass]
    public class CachedCardTransactionTest
    {

        protected CachedCardTransactionDal CachedCardTransactionDal = null;
        protected Mock<ICardTransactionDal> DbCardTransactionDalDal = null;
        protected void Given_Cached_Card_Transaction_Dal()
        {
            DbCardTransactionDalDal = new Mock<ICardTransactionDal>();
            CachedCardTransactionDal = new CachedCardTransactionDal(DbCardTransactionDalDal.Object);
        }

        protected void Given_Real_Cached_Card_Transaction_Dal()
        {
            var cardTransactionDalDal = new CardTransactionDal("CardTransactions", "Commerce");
            CachedCardTransactionDal = new CachedCardTransactionDal(cardTransactionDalDal);
        }

        protected string When_GetAltMerchNo(string platform)
        {
            return CachedCardTransactionDal.GetAltMerchNo(platform);
        }

        protected string When_GetIsoCurrencyCode(string currency)
        {
            return CachedCardTransactionDal.GetIsoAlphaCurrency(currency);
        }

        protected IMerchantInfo When_GetMerchantInfo(string subMarket, string platform)
        {
            return CachedCardTransactionDal.GetMerchantInfo(subMarket, platform);
        }

        protected IMerchantInfo When_GetMerchantInfoByMerchantId(string subMarket)
        {
            return CachedCardTransactionDal.GetMerchantInfoByMerchantId(subMarket, "web");
        }

        protected long When_GetTerminalTransactionNumber(string cardNumber, string transferFromCardNumber, string pin, decimal amount, IMerchantInfo merchantInfo, int requestCode, string merchTime)
        {
            return CachedCardTransactionDal.GetTerminalTransactionNumber(cardNumber, transferFromCardNumber, pin, amount, merchantInfo, requestCode, merchTime);
        }

        protected IServiceTransaction When_GetTransaction(long transactionId)
        {
            return CachedCardTransactionDal.GetTransaction(transactionId);
        }


        protected void When_SaveReturnValues(long transactionId, string authCode, decimal amount, decimal balance, string responseCode,
                                                             int status, decimal exchangeRate, int? baseCurrency, decimal basePreviousBalance,
                                                             decimal baseNewBalance, int localCurrency, string cardNumber, string voidRollbackCode,
                                                             string voidRollbackResponse)
        {
            CachedCardTransactionDal.SaveReturnValues(transactionId, authCode, amount, balance, responseCode,
                                                             status, exchangeRate, baseCurrency, basePreviousBalance,
                                                             baseNewBalance, localCurrency, cardNumber, voidRollbackCode,
                                                             voidRollbackResponse);
        }

        protected void When_SetVirtualActivationValues(long transactionId, string cardNumber, string pin)
        {
            CachedCardTransactionDal.SetVirtualActivationValues(transactionId, cardNumber, pin);
        }

        protected void When_UpsertPartialTransferTransactionDetails(long transactionId, string toCardTransactionId, DateTime toCardTransactionDate, string fromCardTransactionId, DateTime? fromCardTransactionDate)
        {
            CachedCardTransactionDal.UpsertPartialTransferTransactionDetails(transactionId, toCardTransactionId, toCardTransactionDate, fromCardTransactionId, fromCardTransactionDate);
        }

        [TestMethod]
        [TestCategory("CachedCardTransactionDal")]
        public void Given_Cached_Dal_When_Get_Alt_Merch_With_Valid_Values_Then_Ok()
        {
            const string platform = "web";
            const string expectedResult = "01";
            Given_Cached_Card_Transaction_Dal();
            DbCardTransactionDalDal.Setup(p => p.GetAltMerchNo(platform)).Returns(expectedResult);
            var result = When_GetAltMerchNo(platform);

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        [TestCategory("CachedCardTransactionDal")]
        public void Given_Cached_Dal_When_Get_Iso_Currency_With_Valid_Values_Then_Ok()
        {
            const string currency = "USD";
            const string expectedResult = "960";
            Given_Cached_Card_Transaction_Dal();
            DbCardTransactionDalDal.Setup(p => p.GetIsoAlphaCurrency(currency)).Returns(expectedResult);

            var result = When_GetIsoCurrencyCode(currency);

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        [TestCategory("CachedCardTransactionDal")]
        public void Given_Cached_Dal_When_Get_Merchant_Info_With_Web_Platform_Then_Ok()
        {
            const string subMarket = "US";
            const string platform = "web";

            var expectedMerchantInfo = new Mock<IMerchantInfo>();
            expectedMerchantInfo.SetupProperty(p => p.AlternateMid, "01");

            Given_Cached_Card_Transaction_Dal();
            DbCardTransactionDalDal.Setup(p => p.GetMerchantInfo(subMarket, platform)).Returns(expectedMerchantInfo.Object);

            var result = When_GetMerchantInfo(subMarket, platform);

            Assert.AreEqual(expectedMerchantInfo.Object, result);
            Assert.AreEqual(result.AlternateMid, "01");
        }

        [TestMethod]
        [TestCategory("CachedCardTransactionDal")]
        public void Given_Cached_Dal_When_Get_Merchant_Info_With_Ios_Platform_Then_Ok()
        {
            const string subMarket = "US";
            const string platform = "ios";

            var expectedMerchantInfo = new Mock<IMerchantInfo>();
            expectedMerchantInfo.SetupProperty(p => p.AlternateMid, "10");

            Given_Cached_Card_Transaction_Dal();
            DbCardTransactionDalDal.Setup(p => p.GetMerchantInfo(subMarket, platform)).Returns(expectedMerchantInfo.Object);

            var result = When_GetMerchantInfo(subMarket, platform);

            Assert.AreEqual(expectedMerchantInfo.Object, result);
            Assert.AreEqual(result.AlternateMid, "10");
        }


        [TestMethod]
        [TestCategory("CachedCardTransactionDal")]
        public void Given_Cached_Dal_When_Get_Merchant_Info_By_Id_With_Valid_Values_Then_Ok()
        {
            const string subMarket = "US";

            var expectedMerchantInfo = new Mock<IMerchantInfo>();
            expectedMerchantInfo.SetupProperty(p => p.AlternateMid, "01");

            Given_Cached_Card_Transaction_Dal();
            DbCardTransactionDalDal.Setup(p => p.GetMerchantInfoByMerchantId(subMarket, "web")).Returns(expectedMerchantInfo.Object);

            var result = When_GetMerchantInfoByMerchantId(subMarket);

            Assert.AreEqual(expectedMerchantInfo.Object, result);
        }

        [TestMethod]
        [TestCategory("CachedCardTransactionDal")]
        public void Given_Cached_Dal_When_Get_Terminal_Transaction_Number_With_Valid_Values_Then_Ok()
        {
            var cardNumber = "0123456789101112";
            var transferFromCardNumber = "77777777777777777";
            var pin = "12345678";
            var amount = 5.00M;
            var requestCode = 1;
            var merchTime = "12:23";
            var expectedResult = 1L;
            var merchantInfo = new Mock<IMerchantInfo>();

            Given_Cached_Card_Transaction_Dal();
            DbCardTransactionDalDal.Setup(p => p.GetTerminalTransactionNumber(cardNumber, transferFromCardNumber, pin, amount, merchantInfo.Object, requestCode, merchTime)).Returns(expectedResult);

            var result = When_GetTerminalTransactionNumber(cardNumber, transferFromCardNumber, pin, amount, merchantInfo.Object, requestCode, merchTime);

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        [TestCategory("CachedCardTransactionDal")]
        public void Given_Cached_Dal_When_Get_Transaction_With_Valid_Values_Then_Ok()
        {
            const long transactionId = 10L;

            var expectedTransaction = new Mock<IServiceTransaction>();
            expectedTransaction.SetupProperty(p => p.AltMerchNo, "01");

            Given_Cached_Card_Transaction_Dal();
            DbCardTransactionDalDal.Setup(p => p.GetTransaction(transactionId)).Returns(expectedTransaction.Object);

            var result = When_GetTransaction(transactionId);

            Assert.AreEqual(expectedTransaction.Object, result);
        }

        [TestMethod]
        [TestCategory("CachedCardTransactionDal")]
        public void Given_Cached_Dal_When_Save_Return_Values_With_Valid_Values_Then_Ok()
        {
            var cardNumber = "0123456789101112";
            var amount = 5.00M;
            var transactionId = 10L;
            var authCode = "00";
            var balance = 10M;
            var requestCode = "2400";
            var exchangeRate = .98M;
            var baseCurrency = 168;
            var basePreviousBalance = 15M;
            var baseNewBalance = 10M;
            var status = 00;
            var localCurrency = 868;
            var voidRollbackCode = string.Empty;
            var voidRollbackResponse = string.Empty;


            Given_Cached_Card_Transaction_Dal();
            DbCardTransactionDalDal.Setup(
                p => p.SaveReturnValues(transactionId, authCode, amount, balance, requestCode, status,
                                        exchangeRate, baseCurrency, basePreviousBalance, baseNewBalance,
                                        localCurrency, cardNumber, voidRollbackCode, voidRollbackResponse));

            When_SaveReturnValues(transactionId, authCode, amount, balance, requestCode, status,
                                               exchangeRate, baseCurrency, basePreviousBalance, baseNewBalance,
                                               localCurrency, cardNumber, voidRollbackCode, voidRollbackResponse);

            DbCardTransactionDalDal.Verify(p => p.SaveReturnValues(transactionId, authCode, amount, balance, requestCode, status,
                                               exchangeRate, baseCurrency, basePreviousBalance, baseNewBalance,
                                               localCurrency, cardNumber, voidRollbackCode, voidRollbackResponse));
        }

        [TestMethod]
        [TestCategory("CachedCardTransactionDal")]
        public void Given_Cached_Dal_When_Set_Virtual_Activation_Values_With_Valid_Values_Then_Ok()
        {
            var cardNumber = "0123456789101112";
            var transactionId = 10L;
            var pin = "12345678";

            Given_Cached_Card_Transaction_Dal();
            DbCardTransactionDalDal.Setup(
                p => p.SetVirtualActivationValues(transactionId, cardNumber, pin));

            When_SetVirtualActivationValues(transactionId, cardNumber, pin);

            DbCardTransactionDalDal.Verify(p => p.SetVirtualActivationValues(transactionId, cardNumber, pin));
        }


        [TestMethod]
        [TestCategory("CachedCardTransactionDal")]
        public void Given_Cached_Dal_When_Update_Partial_Transfer_With_Valid_Values_Then_Ok()
        {
            var toTransactionId = "1";
            var fromTransactionId = "2";
            var toTransactionDate = DateTime.Now;
            var fromTransactionDate = DateTime.Now;


            var transactionId = 10L;


            Given_Cached_Card_Transaction_Dal();
            DbCardTransactionDalDal.Setup(
                p => p.UpsertPartialTransferTransactionDetails(transactionId, toTransactionId, toTransactionDate, fromTransactionId, fromTransactionDate));

            When_UpsertPartialTransferTransactionDetails(transactionId, toTransactionId, toTransactionDate, fromTransactionId, fromTransactionDate);

            DbCardTransactionDalDal.Verify(p => p.UpsertPartialTransferTransactionDetails(transactionId, toTransactionId, toTransactionDate, fromTransactionId, fromTransactionDate));
        }

        #region Integration Tests

        [TestMethod]
        [TestCategory("CachedCardTransactionDal")]
        public void Given_Real_Cached_Dal_When_Get_Merchant_Info_With_Web_Platform_Then_Ok()
        {
            const string subMarket = "US";
            const string platform = "web";

            Given_Real_Cached_Card_Transaction_Dal();

            var result = When_GetMerchantInfo(subMarket, platform);

            Assert.AreEqual(result.AlternateMid, "01");
        }

        [TestMethod]
        [TestCategory("CachedCardTransactionDal")]
        public void Given_Real_Cached_Dal_When_Get_Merchant_Info_With_Ios_Platform_Then_Ok()
        {
            const string subMarket = "US";
            const string platform = "ios";

            Given_Real_Cached_Card_Transaction_Dal();

            var result = When_GetMerchantInfo(subMarket, platform);

            Assert.AreEqual(result.AlternateMid, "10");
        }

        #endregion
    }
}
