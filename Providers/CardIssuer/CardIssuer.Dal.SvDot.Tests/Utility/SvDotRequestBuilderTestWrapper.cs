﻿using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.SvDot.Common;
using Starbucks.CardIssuer.Dal.SvDot.Db;
using Starbucks.CardIssuer.Dal.SvDot.Utility;
using System;

namespace Starbucks.CardIssuer.Dal.SvDot.Tests.Utility
{
    internal class SvDotRequestBuilderTestWrapper : SvDotRequestBuilder
    {
        public string LocalTransactionDate
        {
            get
            {
                if (Fields != null)
                {
                    string localTransactionDate;
                    return Fields.TryGetValue(SvDotRequestField.LocalTransactionDate.GetField(), out localTransactionDate) ? localTransactionDate : null;
                }

                return null;
            }
        }

        public new string LocalTransactionTime
        {
            get
            {
                if (Fields != null)
                {
                    string localTransactionTime;
                    return Fields.TryGetValue(SvDotRequestField.LocalTransactionTime.GetField(), out localTransactionTime) ? localTransactionTime : null;
                }

                return null;
            }
        }

        public SvDotRequestBuilderTestWrapper(IMerchantInfo merchantInfo, Enums.CallItCommand requestCode,
                                              ICardTransactionDal cardDal, DateTime localTransactionTime, int localTransactionId)
            : base(merchantInfo, requestCode, cardDal, localTransactionTime, localTransactionId)
        {
        }
    }
}