﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.SvDot.Db;
using Starbucks.CardIssuer.Dal.SvDot.Utility;
using System;

namespace Starbucks.CardIssuer.Dal.SvDot.Tests.Utility
{
    [TestClass]
    public class SvDotRequestBuilderTests
    {
        private readonly RandomDataGenerator dataGenerator = new RandomDataGenerator();

        [TestMethod]
        public void Constructor_ValidParameters_SetsTransactionId()
        {
            DateTime localTransactionTime = dataGenerator.GenerateDateTime();
            int localTransactionId = dataGenerator.GenerateInt32();

            var merchantInfoStub = new Mock<IMerchantInfo>();
            var cardDalStub = new Mock<ICardTransactionDal>();

            var requestBuilder = new SvDotRequestBuilder(merchantInfoStub.Object, Enums.CallItCommand.Redeem, cardDalStub.Object, localTransactionTime, localTransactionId);
            int expectedLocalTransactionId = localTransactionId;
            long actualLocalTransactionId = requestBuilder.TransactionId;

            Assert.AreEqual(expectedLocalTransactionId, actualLocalTransactionId);
        }

        [TestMethod]
        public void Constructor_ValidParameters_SetsLocalTransactionDate()
        {
            DateTime localTransactionTime = dataGenerator.GenerateDateTime();
            int localTransactionId = dataGenerator.GenerateInt32();

            var merchantInfoStub = new Mock<IMerchantInfo>();
            var cardDalStub = new Mock<ICardTransactionDal>();

            var requestBuilder = new SvDotRequestBuilderTestWrapper(merchantInfoStub.Object, Enums.CallItCommand.Redeem, cardDalStub.Object, localTransactionTime, localTransactionId);
            string expectedLocalTransactionDate = localTransactionTime.ToString("MMddyyyy");
            string actualLocalTransactionDate = requestBuilder.LocalTransactionDate;

            Assert.AreEqual(expectedLocalTransactionDate, actualLocalTransactionDate);
        }

        [TestMethod]
        public void Constructor_ValidParameters_SetsLocalTransactionTime()
        {
            DateTime localTransactionTime = dataGenerator.GenerateDateTime();
            int localTransactionId = dataGenerator.GenerateInt32();

            var merchantInfoStub = new Mock<IMerchantInfo>();
            var cardDalStub = new Mock<ICardTransactionDal>();

            var requestBuilder = new SvDotRequestBuilderTestWrapper(merchantInfoStub.Object, Enums.CallItCommand.Redeem, cardDalStub.Object, localTransactionTime, localTransactionId);
            string expectedLocalTransactionTime = localTransactionTime.ToString("HHmmss");
            string actualLocalTransactionTime = requestBuilder.LocalTransactionTime;

            Assert.AreEqual(expectedLocalTransactionTime, actualLocalTransactionTime);
        }

        [TestMethod]
        public void Constructor_ValidParameters_SetsMerchantId()
        {
            string expectedMerchantId = dataGenerator.GenerateMerchantId();
            string terminalId = dataGenerator.GenerateInt32(1, 9).ToString("0000");
            DateTime localTransactionTime = dataGenerator.GenerateDateTime();
            int localTransactionId = dataGenerator.GenerateInt32();

            var merchantInfoStub = new Mock<IMerchantInfo>();
            merchantInfoStub.SetupAllProperties();
            merchantInfoStub.Object.Mid = expectedMerchantId;
            merchantInfoStub.Object.TerminalId = terminalId;
            var cardDalStub = new Mock<ICardTransactionDal>();

            var requestBuilder = new SvDotRequestBuilderTestWrapper(merchantInfoStub.Object, Enums.CallItCommand.Redeem, cardDalStub.Object, localTransactionTime, localTransactionId);
            string actualMerchantId = requestBuilder.MerchantId;

            Assert.AreEqual(expectedMerchantId, actualMerchantId);
        }

        [TestMethod]
        public void Constructor_ValidParameters_SetsTerminalId()
        {
            string merchantId = dataGenerator.GenerateMerchantId();
            string expectedTerminalId = dataGenerator.GenerateInt32(1, 9).ToString("0000");
            DateTime localTransactionTime = dataGenerator.GenerateDateTime();
            int localTransactionId = dataGenerator.GenerateInt32();

            var merchantInfoStub = new Mock<IMerchantInfo>();
            merchantInfoStub.SetupAllProperties();
            merchantInfoStub.Object.Mid = merchantId;
            merchantInfoStub.Object.TerminalId = expectedTerminalId;
            var cardDalStub = new Mock<ICardTransactionDal>();

            var requestBuilder = new SvDotRequestBuilderTestWrapper(merchantInfoStub.Object, Enums.CallItCommand.Redeem, cardDalStub.Object, localTransactionTime, localTransactionId);
            string actualTerminalId = requestBuilder.MerchantInfo.TerminalId;

            Assert.AreEqual(expectedTerminalId, actualTerminalId);
        }
    }
}
