﻿using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.SvDot.Common;
using Starbucks.CardIssuer.Dal.SvDot.Configuration;
using Starbucks.CardIssuer.Dal.SvDot.Db;
using Starbucks.CardIssuer.Dal.SvDot.Requestor;
using Starbucks.LogCounter.Provider.Common;

namespace Starbucks.CardIssuer.Dal.SvDot.Tests
{
    [TestClass]
    public class ServiceRequestorTest
    {
        protected  Mock<ICardTransactionDal> CardTransactionDal;
        protected Mock<ISvDotTransport> SvDotTransport;
        protected Mock<ILogCounterManager> LogCounterManager;
        protected SvDotConfigurationSettings SvDotConfigurationSettings;
        protected ServiceRequestor ServiceRequestor = null;
        protected Mock<ISvDotRequestBuilder> RequestBuilder = null;
        
        protected void Given_Service_Requestor()
        {
            SvDotConfigurationSettings =
                ConfigurationManager.GetSection("svDotConfiguration") as SvDotConfigurationSettings;
            CardTransactionDal = new Mock<ICardTransactionDal>();
            SvDotTransport = new Mock<ISvDotTransport>();
            SvDotTransport.SetupGet(c => c.Response).Returns("0");
            LogCounterManager = new Mock<ILogCounterManager>();

            ServiceRequestor = new ServiceRequestor(CardTransactionDal.Object, SvDotTransport.Object, SvDotConfigurationSettings, LogCounterManager.Object);
        }

        protected void Given_Request_Builder()
        {
            const string payload = "SV.99910439997\u001c402102\u001c1307262016\u001c12140931\u001c42999104399970001\u001c4410\u001cF227020\u001cF37356\u001cEA30\u001c04000\u001cC0840\u001c152149896231";
            RequestBuilder = new Mock<ISvDotRequestBuilder>();
            RequestBuilder.SetupGet(x => x.CardNumber).Returns("1");
            RequestBuilder.SetupGet(x => x.RequestCode).Returns(Enums.CallItCommand.Activate);
            RequestBuilder.Setup(x => x.GetRequest()).Returns(payload);
            var merchangeInfo = new Mock<IMerchantInfo>();
            RequestBuilder.SetupGet(x => x.MerchantInfo).Returns(merchangeInfo.Object);
        }

        protected void Given_SvDot_Request_Builder()
        {
            RequestBuilder = new Mock<ISvDotRequestBuilder>();
            //var a = new SvDotRequestBuilder(IMerchantInfo, Enums.CallItCommand.History, CardTransactionDal, DateTime,
                                            //long);
        }
       
        protected void When_Get_History(IMerchantInfo merchantInfo, string cardNumber, string pin )
        {
            ServiceRequestor.GetHistory(merchantInfo, cardNumber, pin, Enums.CallItCommand.History);
        }

        
        protected void When_Send_Request(ISvDotRequestBuilder request, int rollbackAttempts)
        {
            ServiceRequestor.SendRequest(request, rollbackAttempts );
        }

        [TestMethod]
        [TestCategory("ServiceRequestor")]
        public void Given_When_Get_History()
        {
            Mock<IMerchantInfo> merchantInfo = new Mock<IMerchantInfo>( );
            //merchantInfo.SetupProperty(p => p.Mwk, "123");
            string cardNumber = "1234567891011121";
            string pin = string.Empty;
            Given_Service_Requestor();
            var result =  ServiceRequestor.GetHistory(merchantInfo.Object, cardNumber, pin, Enums.CallItCommand.History);
        }

        [TestMethod]
        [TestCategory("ServiceRequestor")]
        public void Given_When_Send_Request()
        {            
            const int rollbackAttempts = 3;
            Given_Request_Builder();

            Given_Service_Requestor();
            var result = ServiceRequestor.SendRequest(RequestBuilder.Object, rollbackAttempts);
        }
    }
}
