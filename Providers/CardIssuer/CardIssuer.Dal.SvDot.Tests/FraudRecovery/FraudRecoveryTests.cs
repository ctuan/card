﻿using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.SvDot.Configuration;
using Starbucks.CardIssuer.Dal.SvDot.Db;
using Starbucks.CardIssuer.Dal.SvDot.Requestor;
using Starbucks.CardIssuer.Dal.SvDot.TransportModels;
using Starbucks.LogCounter.Provider;
using Starbucks.MessageBroker.CardNotificationListener;
using Starbucks.MessageBroker.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using Starbucks.MessageBroker.Common.Models;


namespace Starbucks.CardIssuer.Dal.SvDot.Tests.FraudRecovery
{
	
	[TestClass]
	public class FraudRecoveryTests
	{
		protected readonly static string[] TestSvCardNumbers = { "7777083002943493", "7777083002953493", "7777083002964149" };
		protected readonly static string[] TestSvCardPins = { "33151451", "25567528", "56387208" };

		protected const string UnitedStatesTippingMarketId = "99030169997";
		//protected const string USMid = "99910439997";
		//protected const string USMid = "9741270007";
		protected const decimal InitAmount = 99.99m;

		protected FraudRecoveryTestAgent GetTestAgent(int idx = 0, bool pinlss = true, string market= "GSUS")
		{
			string cardNumber = TestSvCardNumbers[idx];
			string pin = pinlss ? null : TestSvCardPins[idx];
			var agent = new FraudRecoveryTestAgent(cardNumber, pin, InitAmount, market);

			return agent;
		}

		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
        [Ignore]
		public void FraudRecovery_GetBalance_Test()
		{
			using (var agent = GetTestAgent())
			{
				GetBalanceAndVerify(agent, InitAmount);		
			}
		}

		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
        [Ignore]
        public void FraudRecovery_Reload_Test()
		{
			decimal amount = 10m;
			using (var agent = GetTestAgent())
			{
				ReloadAndVerify(agent, amount);
			}
		}

		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
        [Ignore]
        public void FraudRecovery_Redeem_Test()
		{
			decimal amount = 10m;
			using (var agent = GetTestAgent())
			{
				RedeemAndVerify(agent, amount);
			}
		}

		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
        [Ignore]
        public void FraudRecovery_BalanceLock_Test()
		{
			using (var agent = GetTestAgent())
			{
				BalanceLockAndVerify(agent);
			}
		}


		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
        [Ignore]
        public void FraudRecovery_RedeemUnlock_Test()
		{
			using (var agent = GetTestAgent())
			{
				ICardTransaction transaction = agent.Freeze();
				Assert.IsTrue(transaction != null && transaction.TransactionSucceeded);

				RedeemUnlockAndVerify(agent);
			}
		}


		#region Helper Functions


		#endregion

		#region Verification functions

		protected ICardTransaction GetBalanceAndVerify(FraudRecoveryTestAgent agent, decimal expectedAmount)
		{
			ICardTransaction transaction = agent.GetBalance();

			Assert.IsTrue(transaction!= null && transaction.TransactionSucceeded);
			Assert.IsTrue(transaction.EndingBalance == expectedAmount);
			return transaction;
		}

		protected ICardTransaction ReloadAndVerify(FraudRecoveryTestAgent agent, decimal amount)
		{
			ICardTransaction transaction = agent.Reload( amount);

			Assert.IsTrue(transaction != null && transaction.TransactionSucceeded);
			Assert.IsTrue(transaction.EndingBalance == (InitAmount + amount));
			return transaction;
		}

		protected ICardTransaction RedeemAndVerify(FraudRecoveryTestAgent agent, decimal amount)
		{
			ICardTransaction transaction = agent.Redeem(amount);

			Assert.IsTrue(transaction != null && transaction.TransactionSucceeded);
			Assert.IsTrue(transaction.EndingBalance == (InitAmount - amount));
			return transaction;
		}

		protected ICardTransaction BalanceLockAndVerify(FraudRecoveryTestAgent agent)
		{
			ICardTransaction transaction = agent.Freeze();

			Assert.IsTrue(transaction != null && transaction.TransactionSucceeded);

			GetBalanceAndVerify(agent, InitAmount);

			ICardTransaction temp = agent.Redeem(5);
			Assert.IsTrue(temp != null && !temp.TransactionSucceeded);

			temp = agent.Reload(5);
			Assert.IsTrue(temp != null && temp.TransactionSucceeded);
			decimal balance = temp.EndingBalance;
			
			temp = agent.Freeze();
			Assert.IsTrue(temp != null && !temp.TransactionSucceeded);

			temp = agent.Unfreeze(0);
			Assert.IsTrue(temp != null && !temp.TransactionSucceeded);

			temp = agent.Unfreeze(balance);
			Assert.IsTrue(temp != null && !temp.TransactionSucceeded);

			RedeemUnlockAndVerify(agent);

			return transaction;
		}

		protected ICardTransaction RedeemUnlockAndVerify(FraudRecoveryTestAgent agent)
		{
			ICardTransaction transaction = agent.GetBalance();
			Assert.IsTrue(transaction != null && transaction.TransactionSucceeded);
			decimal lockAmount = transaction.LockAmount;
			decimal balance = transaction.EndingBalance;

		    transaction = agent.Unfreeze(0);
			Assert.IsTrue(transaction != null && !transaction.TransactionSucceeded);

			transaction = agent.Unfreeze(lockAmount + 10);
			Assert.IsTrue(transaction != null && !transaction.TransactionSucceeded);

			balance = balance - 0.01m;
			transaction = agent.Unfreeze(0.01m);
			Assert.IsTrue(transaction != null && transaction.TransactionSucceeded);
			Assert.IsTrue(transaction.EndingBalance == balance);

			transaction = agent.Unfreeze(lockAmount-0.01m);
			Assert.IsTrue(transaction != null && !transaction.TransactionSucceeded);

			transaction = agent.Redeem(balance);
			Assert.IsTrue(transaction != null && transaction.TransactionSucceeded);
			Assert.IsTrue(transaction.EndingBalance == 0);

			return transaction;
		}

		#endregion
	}
}
