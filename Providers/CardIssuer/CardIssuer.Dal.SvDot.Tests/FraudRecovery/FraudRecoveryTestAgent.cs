﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.SvDot.Configuration;
using Starbucks.CardIssuer.Dal.SvDot.Db;
using Starbucks.CardIssuer.Dal.SvDot.Requestor;
using Starbucks.CardIssuer.Dal.SvDot.TransportModels;
using Starbucks.LogCounter.Provider;
using Starbucks.MessageBroker.Common;
using Starbucks.MessageBroker.Common.Models;

namespace Starbucks.CardIssuer.Dal.SvDot.Tests.FraudRecovery
{
	public class FraudRecoveryTestAgent : IDisposable
	{
		private Mock<IMessageBroker> _mockBrocker = null;
		private ICardIssuerDal _cardIssuer = null;
		private IMerchantInfo _merchantInfo = null;
		private int _numberCallToBroker = 0;

		private string _testCardNumber = string.Empty;
		private string _testPin = null;

		public FraudRecoveryTestAgent(string cardNumber, string pin, decimal initAmount = 0, string market = "GSUS")
		{
			_testCardNumber = cardNumber;
			Assert.IsTrue(!string.IsNullOrWhiteSpace(cardNumber));

			_testPin = pin;

			InitTest(initAmount, market);
		}

		public void Dispose()
		{
			CleanupTest();
		}

		#region CardIssuer Wrapper

		public ICardTransaction GetBalance()
		{
			ICardTransaction transaction = null;
			try
			{
				transaction = _cardIssuer.GetBalance(_merchantInfo, _testCardNumber, _testPin);
				return VerifyTransaction(transaction);
			}
			finally
			{
				VerifyBroker(transaction);
			}
		}

		public ICardTransaction Reload(decimal amount)
		{
			ICardTransaction transaction = null;
			try
			{
				transaction = _cardIssuer.Reload(_merchantInfo, _testCardNumber, _testPin, amount);
				return VerifyTransaction(transaction);
			}
			finally
			{
				VerifyBroker(transaction);
			}
		}


		public ICardTransaction Redeem(decimal amount)
		{
			ICardTransaction transaction = null;
			try
			{
				transaction = _cardIssuer.Redeem(_merchantInfo, _testCardNumber, _testPin, amount);
				return VerifyTransaction(transaction);
			}
			finally
			{
				VerifyBroker(transaction);
			}
		}

		public ICardTransaction Freeze()
		{
			ICardTransaction transaction = null;
			try
			{
				transaction = _cardIssuer.BalanceLock(_merchantInfo, _testCardNumber, _testPin);
				return VerifyTransaction(transaction);
			}
			finally
			{
				VerifyBroker(transaction);
			}
		}

		public ICardTransaction Unfreeze(decimal amount)
		{
			ICardTransaction transaction = null;
			try
			{
				transaction = _cardIssuer.RedemptionUnlock(_merchantInfo, _testCardNumber, _testPin, amount);
				return transaction;
			}
			finally
			{
				VerifyBroker(transaction);
			}
		}

		#endregion

		#region Helper functions 


		protected void InitTest(decimal initAmount, string market)
		{
			_mockBrocker = new Mock<IMessageBroker>();
			IMessageBroker broker = _mockBrocker.Object;

			_cardIssuer = new SvDotCardIssuer
				(
				new ServiceRequestor
					(
                    new CardTransactionDal("CardTransactions", "Commerce"),
					new SvDotTransportXml(
						ConfigurationManager.GetSection("svDotConfiguration") as SvDotConfigurationSettings),
					ConfigurationManager.GetSection("svDotConfiguration") as SvDotConfigurationSettings,
					LogCounterManager.Instance()
					),
                new CardTransactionDal("CardTransactions", "Commerce"),
				_mockBrocker.Object,
				ConfigurationManager.GetSection("svDotConfiguration") as SvDotConfigurationSettings,
				new Starbucks.Settings.Provider.SettingsProvider(),
                new CardTransactionDal("CardTransactions", "Commerce"),
				LogCounterManager.Instance()
				);

			Assert.IsTrue(_cardIssuer != null);

			_merchantInfo = _cardIssuer.GetMerchantInfo(market);
			Assert.IsTrue(_merchantInfo != null);


			CleanupTest();
			var transaction = Reload(initAmount);
			Assert.IsTrue(transaction.TransactionSucceeded && transaction.EndingBalance == initAmount);
		}

		protected void CleanupTest()
		{
			var transaction = GetBalance();
			Assert.IsTrue(transaction.TransactionSucceeded);
			decimal balance = transaction.EndingBalance;

			decimal unfreezeAmount = transaction.LockAmount;
			if (unfreezeAmount > 0)
			{
				transaction = Unfreeze(unfreezeAmount);
			}

			if (transaction != null && transaction.TransactionSucceeded)
			{
				balance = transaction.EndingBalance;
			}

			if (balance > 0)
			{
				transaction = Redeem(balance);
				Assert.IsTrue(transaction.TransactionSucceeded && transaction.EndingBalance == 0);
			}
		}

		protected void VerifyBroker(ICardTransaction transaction)
		{
			_numberCallToBroker = transaction != null && transaction.TransactionSucceeded ? _numberCallToBroker + 1 : _numberCallToBroker;

			_mockBrocker.Verify(c => c.Publish(It.IsAny<SvcCardNotification>()), Times.Exactly(_numberCallToBroker));			
		}

		protected ICardTransaction VerifyTransaction(ICardTransaction transaction)
		{
			Assert.IsTrue(transaction != null);
			Assert.IsTrue(_testCardNumber == transaction.CardNumber);
			Assert.AreEqual(_merchantInfo.AlternateMid, transaction.StoreId);
			Assert.AreEqual(_merchantInfo.Mid, transaction.MerchantId);
			Assert.AreEqual(_merchantInfo.TerminalId, transaction.TerminalId);

			return transaction;
		}

		#endregion
	}
}
