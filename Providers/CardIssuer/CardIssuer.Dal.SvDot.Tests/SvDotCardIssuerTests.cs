﻿using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.SvDot.Common;
using Starbucks.CardIssuer.Dal.SvDot.Configuration;
using Starbucks.CardIssuer.Dal.SvDot.Db;
using Starbucks.MessageBroker.Common;
using System;
using Starbucks.Settings.Provider.Common;

namespace Starbucks.CardIssuer.Dal.SvDot.Tests
{
    [TestClass]

    public class SvDotCardIssuerTests
    {
        private readonly RandomDataGenerator dataGenerator = new RandomDataGenerator();

        [TestMethod]
        [TestCategory("CardIssuerUnitTest")]
        public void Tip_ValidParameters_CallsRequestorSendRequest()
        {
            var mockRequestor = CreateMockRequestor();
            var cardIssuer = CreateCardIssuer(mockRequestor);

            var merchantInfoStub = new Mock<IMerchantInfo>();
            const string cardNumber = "1234567890123456";
            decimal amount = dataGenerator.GenerateDecimal();
            DateTime localTransactionTime = dataGenerator.GenerateDateTime();
            int localTransactionId = dataGenerator.GenerateInt32();

            cardIssuer.Tip(merchantInfoStub.Object, cardNumber, amount, localTransactionTime, localTransactionId);

            mockRequestor.Verify(mock => mock.SendRequest(It.IsAny<ISvDotRequestBuilder>(), It.IsAny<int>()));
        }

        [TestMethod]
        [TestCategory("CardIssuerUnitTest")]
        public void Tip_ValidParameters_CallsRequestorSendRequestWithExpectedCardNumber()
        {
            var mockRequestor = CreateMockRequestor();
            var cardIssuer = CreateCardIssuer(mockRequestor);

            var merchantInfoStub = new Mock<IMerchantInfo>();
            const string cardNumber = "1234567890123456";
            decimal amount = dataGenerator.GenerateDecimal();
            DateTime localTransactionTime = dataGenerator.GenerateDateTime();
            int localTransactionId = dataGenerator.GenerateInt32();

            cardIssuer.Tip(merchantInfoStub.Object, cardNumber, amount, localTransactionTime, localTransactionId);

            mockRequestor.Verify(mock => mock.SendRequest(It.Is<ISvDotRequestBuilder>(builder => builder.CardNumber == cardNumber), It.IsAny<int>()));
        }

        [TestMethod]
        [TestCategory("CardIssuerUnitTest")]
        public void Tip_ValidParameters_CallsRequestorSendRequestWithExpectedAmount()
        {
            var mockRequestor = CreateMockRequestor();
            var cardIssuer = CreateCardIssuer(mockRequestor);

            var merchantInfoStub = new Mock<IMerchantInfo>();
            const string cardNumber = "1234567890123456";
            decimal amount = dataGenerator.GenerateDecimal();
            DateTime localTransactionTime = dataGenerator.GenerateDateTime();
            int localTransactionId = dataGenerator.GenerateInt32();

            cardIssuer.Tip(merchantInfoStub.Object, cardNumber, amount, localTransactionTime, localTransactionId);

            mockRequestor.Verify(mock => mock.SendRequest(It.Is<ISvDotRequestBuilder>(builder => builder.Amount == amount), It.IsAny<int>()));
        }

        [TestMethod]
        [TestCategory("CardIssuerUnitTest")]
        public void Tip_ValidParameters_CallsRequestorSendRequestWithExpectedTransactionId()
        {
            var mockRequestor = CreateMockRequestor();
            var cardIssuer = CreateCardIssuer(mockRequestor);

            var merchantInfoStub = new Mock<IMerchantInfo>();
            const string cardNumber = "1234567890123456";
            decimal amount = dataGenerator.GenerateDecimal();
            DateTime localTransactionTime = dataGenerator.GenerateDateTime();
            int localTransactionId = dataGenerator.GenerateInt32();

            cardIssuer.Tip(merchantInfoStub.Object, cardNumber, amount, localTransactionTime, localTransactionId);

            mockRequestor.Verify(mock => mock.SendRequest(It.Is<ISvDotRequestBuilder>(builder => builder.TransactionId == localTransactionId), It.IsAny<int>()));
        }

        [TestMethod]
        [TestCategory("CardIssuerUnitTest")]
        public void Tip_ValidParameters_CallsRequestorSendRequestWithExpectedMerchantId()
        {
            var mockRequestor = CreateMockRequestor();
            var cardIssuer = CreateCardIssuer(mockRequestor);

            var merchantInfoStub = new Mock<IMerchantInfo>();
            merchantInfoStub.SetupAllProperties();
            merchantInfoStub.Object.Mid = dataGenerator.GenerateMerchantId();
            merchantInfoStub.Object.TerminalId = dataGenerator.GenerateTerminalId();
            const string cardNumber = "1234567890123456";
            decimal amount = dataGenerator.GenerateDecimal();
            DateTime localTransactionTime = dataGenerator.GenerateDateTime();
            int localTransactionId = dataGenerator.GenerateInt32();

            cardIssuer.Tip(merchantInfoStub.Object, cardNumber, amount, localTransactionTime, localTransactionId);

            mockRequestor.Verify(mock => mock.SendRequest(It.Is<ISvDotRequestBuilder>(builder => builder.TransactionId == localTransactionId), It.IsAny<int>()));
        }

        [TestMethod]
        [TestCategory("CardIssuerUnitTest")]
        public void Activate_ValidParameters_CallsRequestorSendRequestWithExpectedMerchantId()
        {
            var mockRequestor = CreateMockRequestor();
            var cardIssuer = CreateCardIssuer(mockRequestor);

            var merchantInfoStub = new Mock<IMerchantInfo>();
            merchantInfoStub.SetupAllProperties();
            merchantInfoStub.Object.Mid = dataGenerator.GenerateMerchantId();
            merchantInfoStub.Object.TerminalId = dataGenerator.GenerateTerminalId();
            const string cardNumber = "1234567890123456";
            const string pin = "12345678";
            decimal amount = dataGenerator.GenerateDecimal();

            int localTransactionId = dataGenerator.GenerateInt32();

            cardIssuer.Activate(merchantInfoStub.Object, cardNumber, pin, amount);

            mockRequestor.Verify(
                mock =>
                mock.SendRequest(It.Is<ISvDotRequestBuilder>(b => b.CardNumber == cardNumber), It.IsAny<int>()));
        }

        [TestMethod]
        [TestCategory("CardIssuerUnitTest")]
        public void ActivateVirtual_ValidParameters_CallsRequestorSendRequestWithExpectedMerchantId()
        {
            var mockRequestor = CreateMockRequestor();
            var cardIssuer = CreateCardIssuer(mockRequestor);

            var merchantInfoStub = new Mock<IMerchantInfo>();
            merchantInfoStub.SetupAllProperties();
            merchantInfoStub.Object.Mid = dataGenerator.GenerateMerchantId();
            merchantInfoStub.Object.TerminalId = dataGenerator.GenerateTerminalId();
            const string promoCode = "234";
            decimal amount = dataGenerator.GenerateDecimal();

            int localTransactionId = dataGenerator.GenerateInt32();

            cardIssuer.ActivateVirtual(merchantInfoStub.Object, promoCode, amount);

            mockRequestor.Verify(
                 mock =>
                 mock.SendRequest(It.Is<ISvDotRequestBuilder>(b => b.Amount == amount), It.IsAny<int>()));
        }
       
        [TestMethod]
        [TestCategory("CardIssuerUnitTest")]
        public void CashOut_ValidParameters_CallsRequestorSendRequestWithExpectedMerchantId()
        {
            var mockRequestor = CreateMockRequestor();
            var cardIssuer = CreateCardIssuer(mockRequestor);

            var merchantInfoStub = new Mock<IMerchantInfo>();
            merchantInfoStub.SetupAllProperties();
            merchantInfoStub.Object.Mid = dataGenerator.GenerateMerchantId();
            merchantInfoStub.Object.TerminalId = dataGenerator.GenerateTerminalId();
            const string cardNumber = "1234567890123456";
            const string pin = "12345678";
            decimal amount = dataGenerator.GenerateDecimal();

            int localTransactionId = dataGenerator.GenerateInt32();

            cardIssuer.CashOut(merchantInfoStub.Object, cardNumber, pin, amount);

            mockRequestor.Verify(
                mock =>
                mock.SendRequest(It.Is<ISvDotRequestBuilder>(builder => builder.CardNumber == cardNumber),
                                 It.IsAny<int>()));
        }

        [TestMethod]
        [TestCategory("CardIssuerUnitTest")]
        public void DisablePin_ValidParameters_CallsRequestorSendRequestWithExpectedMerchantId()
        {
            var mockRequestor = CreateMockRequestor();
            var cardIssuer = CreateCardIssuer(mockRequestor);

            var merchantInfoStub = new Mock<IMerchantInfo>();
            merchantInfoStub.SetupAllProperties();
            merchantInfoStub.Object.Mid = dataGenerator.GenerateMerchantId();
            merchantInfoStub.Object.TerminalId = dataGenerator.GenerateTerminalId();
            const string cardNumber = "1234567890123456";
            const string pin = "12345678";

            int localTransactionId = dataGenerator.GenerateInt32();

            cardIssuer.DisablePin(merchantInfoStub.Object, cardNumber, pin);

            mockRequestor.Verify(
                mock =>
                mock.SendRequest(It.Is<ISvDotRequestBuilder>(b => b.CardNumber == cardNumber), It.IsAny<int>()));
        }

        [TestMethod]
        [TestCategory("CardIssuerUnitTest")]
        public void GetBalance_ValidParameters_CallsRequestorSendRequestWithExpectedMerchantId()
        {
            var mockRequestor = CreateMockRequestor();
            var cardIssuer = CreateCardIssuer(mockRequestor);

            var merchantInfoStub = new Mock<IMerchantInfo>();
            merchantInfoStub.SetupAllProperties();
            merchantInfoStub.Object.Mid = dataGenerator.GenerateMerchantId();
            merchantInfoStub.Object.TerminalId = dataGenerator.GenerateTerminalId();
            const string cardNumber = "1234567890123456";
            const string pin = "12345678";

            int localTransactionId = dataGenerator.GenerateInt32();

            cardIssuer.GetBalance(merchantInfoStub.Object, cardNumber, pin);

            mockRequestor.Verify(
                 mock =>
                 mock.SendRequest(It.Is<ISvDotRequestBuilder>(b => b.CardNumber == cardNumber), It.IsAny<int>()));
        }

        [TestMethod]
        [TestCategory("CardIssuerUnitTest")]
        public void Redeem_ValidParameters_CallsRequestorSendRequestWithExpectedMerchantId()
        {
            var mockRequestor = CreateMockRequestor();
            var cardIssuer = CreateCardIssuer(mockRequestor);

            var merchantInfoStub = new Mock<IMerchantInfo>();
            merchantInfoStub.SetupAllProperties();
            merchantInfoStub.Object.Mid = dataGenerator.GenerateMerchantId();
            merchantInfoStub.Object.TerminalId = dataGenerator.GenerateTerminalId();
            const string cardNumber = "1234567890123456";
            const string pin = "12345678";
            decimal amount = dataGenerator.GenerateDecimal();

            cardIssuer.Redeem(merchantInfoStub.Object, cardNumber, pin, amount);

            mockRequestor.Verify(
                 mock =>
                 mock.SendRequest(It.Is<ISvDotRequestBuilder>(b => b.CardNumber == cardNumber), It.IsAny<int>()));
        }

        [TestMethod]
        [TestCategory("CardIssuerUnitTest")]
        public void Reload_ValidParameters_CallsRequestorSendRequestWithExpectedMerchantId()
        {
            var mockRequestor = CreateMockRequestor();
            var cardIssuer = CreateCardIssuer(mockRequestor);

            var merchantInfoStub = new Mock<IMerchantInfo>();
            merchantInfoStub.SetupAllProperties();
            merchantInfoStub.Object.Mid = dataGenerator.GenerateMerchantId();
            merchantInfoStub.Object.TerminalId = dataGenerator.GenerateTerminalId();
            const string cardNumber = "1234567890123456";
            const string pin = "12345678";
            decimal amount = dataGenerator.GenerateDecimal();

            cardIssuer.Reload(merchantInfoStub.Object, cardNumber, pin, amount);

            mockRequestor.Verify(
                 mock =>
                 mock.SendRequest(It.Is<ISvDotRequestBuilder>(b => b.CardNumber == cardNumber), It.IsAny<int>()));
        }

        [TestMethod]
        [TestCategory("CardIssuerUnitTest")]
        public void TransferBalance_ValidParameters_CallsRequestorSendRequestWithExpectedMerchantId()
        {
            var mockRequestor = CreateMockRequestor();
            var cardIssuer = CreateCardIssuer(mockRequestor);

            var merchantInfoStub = new Mock<IMerchantInfo>();
            merchantInfoStub.SetupAllProperties();
            merchantInfoStub.Object.Mid = dataGenerator.GenerateMerchantId();
            merchantInfoStub.Object.TerminalId = dataGenerator.GenerateTerminalId();
            const string cardNumberFrom = "1234567890123456";
            const string cardNumberTo = "1234567890123451";

            cardIssuer.TransferBalance(merchantInfoStub.Object, cardNumberFrom, cardNumberTo);

            mockRequestor.Verify(
                 mock =>
                 mock.SendRequest(It.Is<ISvDotRequestBuilder>(b => b.CardNumber == cardNumberFrom), It.IsAny<int>()));
        }

        [TestMethod]
        [TestCategory("CardIssuerUnitTest")]
        public void GetHistory_ValidParameters_CallsRequestorSendRequestWithExpectedMerchantId()
        {
            var mockRequestor = CreateMockRequestorGetHistory();
            var cardIssuer = CreateCardIssuer(mockRequestor);

            var merchantInfoStub = new Mock<IMerchantInfo>();
            merchantInfoStub.SetupAllProperties();
            merchantInfoStub.Object.Mid = dataGenerator.GenerateMerchantId();
            merchantInfoStub.Object.TerminalId = dataGenerator.GenerateTerminalId();
            const string cardNumber = "1234567890123456";
            const string pin = "12345678";

            cardIssuer.GetHistory(merchantInfoStub.Object, cardNumber, pin);

            mockRequestor.Verify(
                 mock =>
                 mock.GetHistory(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>(), It.Is<Enums.CallItCommand>(p=>p==Enums.CallItCommand.History )));
        }

        [TestMethod]
        [TestCategory("CardIssuerUnitTest")]
        public void GetHistoryPinless_ValidParameters_CallsRequestorSendRequestWithExpectedMerchantId()
        {
            var mockRequestor = CreateMockRequestorGetHistory();
            var cardIssuer = CreateCardIssuer(mockRequestor);

            var merchantInfoStub = new Mock<IMerchantInfo>();
            merchantInfoStub.SetupAllProperties();
            merchantInfoStub.Object.Mid = dataGenerator.GenerateMerchantId();
            merchantInfoStub.Object.TerminalId = dataGenerator.GenerateTerminalId();
            const string cardNumber = "1234567890123456";


            cardIssuer.GetHistoryPinless(merchantInfoStub.Object, cardNumber);

            mockRequestor.Verify(
                 mock =>
                 mock.GetHistory(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>(), It.Is<Enums.CallItCommand>(p => p == Enums.CallItCommand.History)));
        }

        [TestMethod]
        [TestCategory("CardIssuerUnitTest")]
        public void GetHistoryRecent_ValidParameters_CallsRequestorSendRequestWithExpectedMerchantId()
        {
            var mockRequestor = CreateMockRequestorGetHistory();
            var cardIssuer = CreateCardIssuer(mockRequestor);

            var merchantInfoStub = new Mock<IMerchantInfo>();
            merchantInfoStub.SetupAllProperties();
            merchantInfoStub.Object.Mid = dataGenerator.GenerateMerchantId();
            merchantInfoStub.Object.TerminalId = dataGenerator.GenerateTerminalId();
            const string cardNumber = "1234567890123456";


            cardIssuer.GetHistoryCondensed( merchantInfoStub.Object, cardNumber);

            mockRequestor.Verify(
                 mock =>
                 mock.GetHistory(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>(), It.Is<Enums.CallItCommand>(p => p == Enums.CallItCommand.HistoryCondensed)));
        }

        //[TestMethod]
        //[TestCategory("CardIssuerUnitTest")]
        //public void VoidRequest_ValidParameters_CallsRequestorSendRequestWithExpectedMerchantId()
        //{
        //    var mockRequestor = CreateMockRequestor();
        //    var cardIssuer = CreateCardIssuer(mockRequestor);

        //    var merchantInfoStub = new Mock<IMerchantInfo>();
        //    merchantInfoStub.SetupAllProperties();
        //    merchantInfoStub.Object.Mid = dataGenerator.GenerateMerchantId();
        //    merchantInfoStub.Object.TerminalId = dataGenerator.GenerateTerminalId();
        //    const string cardNumber = "1234567890123456";
        //    const string pin = "12345678";
        //    decimal amount = dataGenerator.GenerateDecimal();
        //    string localTransactionId = dataGenerator.GenerateInt32().ToString(CultureInfo.InvariantCulture);

        //    var cardTransactionStub = new Mock<ICardTransaction>();
        //    cardTransactionStub.SetupProperty(p => p.TransactionId, localTransactionId);
        //    cardTransactionStub.SetupProperty(p => p.RequestCode, "2300");

        //    var svDotRequestBuilder = new Mock<ISvDotRequestBuilder>(MockBehavior.Loose);
            
        //    cardIssuer.VoidRequest(cardTransactionStub.Object, merchantInfoStub.Object);

        //    mockRequestor.Verify(
        //        mock =>
        //        mock.SendRequest(It.Is<ISvDotRequestBuilder>(b=>true ), It.IsAny<int>()));
        //}

        private static SvDotCardIssuer CreateCardIssuer(Mock<IRequestor> mockRequestor)
        {
            var cardDalStub = new Mock<ICardTransactionDal>();
            var messageBroker = new Mock<IMessageBroker>();
            var settings = new Mock<ISettingsProvider>();
            var transactionDal = new Mock<ICardTransactionDal>();
            var logCounter = new Mock<LogCounter.Provider.Common.ILogCounterManager>();
            var cardIssuer = new SvDotCardIssuer(mockRequestor.Object, cardDalStub.Object,
                                                 messageBroker.Object, ConfigurationManager.GetSection("svDotConfiguration") as SvDotConfigurationSettings, settings.Object, transactionDal.Object, logCounter.Object);
            return cardIssuer;
        }

        private static Mock<IRequestor> CreateMockRequestor()
        {
            var cardTransactionStub = new Mock<ICardTransaction>();
            var mockRequestor = new Mock<IRequestor>();
            mockRequestor.Setup(mock => mock.SendRequest(It.IsAny<ISvDotRequestBuilder>(), It.IsAny<int>()))
                         .Returns(cardTransactionStub.Object);
            return mockRequestor;
        }

        private static Mock<IRequestor> CreateMockRequestorGetHistory()
        {
            var cardTransactionStub = new Mock<List<ICardTransaction>>();
            var mockRequestor = new Mock<IRequestor>();
            mockRequestor.Setup(mock => mock.GetHistory(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Enums.CallItCommand >()))
                         .Returns(cardTransactionStub.Object);
            return mockRequestor;
        }
    }
}
