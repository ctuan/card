﻿using System.Globalization;

namespace Starbucks.CardIssuer.Dal.SvDot.Tests
{
    internal class RandomDataGenerator : Starbucks.Common.Tests.Utilities.RandomDataGenerator
    {
        public string GenerateMerchantId()
        {
            return GenerateInt64(10000000000, 99999999999).ToString(CultureInfo.InvariantCulture);
        }

        public string GenerateTerminalId()
        {
            return GenerateInt32(1, 9).ToString("0000", CultureInfo.InvariantCulture);
        }
    }
}
