﻿using System;
using System.Runtime.Serialization;

namespace Starbucks.LogCounter.Provider
{
    [DataContract(Namespace = "http://schemas.datacontract.org/2004/07/Starbucks.Platform.OrderManagement.Contracts.Counters")]
    public class LogCounterData
    {
        [DataMember]
        public string CounterName { get; set; }
        [DataMember]
        public decimal CounterValue { get; set; }
        [DataMember]
        public DateTime TimeStamp { get; set; }
        [DataMember]
        public int TimesProcessed { get; set; }
        [DataMember(IsRequired = false)]
        public string MachineName { get; set; }
        [DataMember(IsRequired = false)]
        public DateTime DateCreated { get; set; }
    }
}
