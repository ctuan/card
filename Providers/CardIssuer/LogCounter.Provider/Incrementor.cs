﻿namespace Starbucks.LogCounter.Provider
{
    internal class Incrementor
    {
        public string CounterName { get; set; }
        public decimal Increment { get; set; }
    }
}
