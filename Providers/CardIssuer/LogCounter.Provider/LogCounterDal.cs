﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Messaging;
using System.ServiceModel;
using System.Transactions;

namespace Starbucks.LogCounter.Provider
{
    public class LogCounterDal
    {
        // Todo: This should not be static. Merge this implementation with that in Platform.

        public static List<LogCounterData> GetCounter(string name, DateTime? sinceTime)
        {
            var returnList = new List<LogCounterData>();
            try
            {
                var connectionString = ConfigurationManager.ConnectionStrings["Logging"].ConnectionString;
                using (var connection = new SqlConnection(connectionString))
                using (var command = new SqlCommand("GetLogCounter", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    connection.Open();

                    command.Parameters.AddWithValue("@CounterName", name);
                    command.Parameters.AddWithValue("@SinceTime", sinceTime);
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            returnList.Add(new LogCounterData
                                {
                                    CounterValue = (decimal)reader["CounterValue"],
                                    CounterName = name,
                                    TimeStamp = (DateTime)reader["TimeStamp"]
                                });
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            return returnList;
        }

        public static bool SaveCounterWCF(LogCounterData data)
        {

            try
            {
                NetMsmqBinding binding = new NetMsmqBinding();
                binding.ExactlyOnce = false;
                binding.Durable = true;
                binding.DeadLetterQueue = DeadLetterQueue.System;
                binding.ReceiveErrorHandling = ReceiveErrorHandling.Move;
                binding.MaxRetryCycles = 32;
                binding.RetryCycleDelay = TimeSpan.FromMinutes(5);
                binding.ReceiveRetryCount = 1;
                binding.Security.Mode = NetMsmqSecurityMode.None;
                var client = ChannelManagerSingleton.Instance.GetChannel<ICounterServiceContract>(binding, new EndpointAddress(ConfigurationManager.AppSettings["LogCounterQueuePathWCF"]));
                data.DateCreated = DateTime.Now;
                client.LogCounter(data);
                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Print("Error writing to counter WCF:" + ex.Message + ex.StackTrace);
                return false;
            }
        }

        public static bool SaveCounter(LogCounterData data)
        {
            bool returnValue = false;
            try
            {
                string queuePath = ConfigurationManager.AppSettings["LogCounterQueuePath"];
                if (!string.IsNullOrEmpty(queuePath))
                {
                    using (var queue = new MessageQueue(queuePath))
                    {
                        if (queue.Transactional)
                        {
                            using (var transaction = new TransactionScope(TransactionScopeOption.RequiresNew))
                            {
                                queue.Send(data, data.CounterName, MessageQueueTransactionType.Automatic);
                                transaction.Complete();
                            }
                        }
                        else
                        {
                            queue.Send(data, data.CounterName);
                        }
                        returnValue = true;
                    }
                }
                else
                {
                    System.Diagnostics.Debug.Print("Please create a AppSettings entry for LogConterQueuePath. Example: (paems398.sbweb.prod\\LogCounter).");
                }
            }
            catch (Exception ex)
            {
                returnValue = false;
                System.Diagnostics.Debug.Print("Error writing to counter:" + ex.Message + ex.StackTrace);
            }
            return returnValue;
        }

        public static Dictionary<string, List<LogCounterData>> GetTopCounters(DateTime? sinceTime)
        {
            var returnDictionary = new Dictionary<string, List<LogCounterData>>();
            try
            {
                var connectionString = ConfigurationManager.ConnectionStrings["Logging"].ConnectionString;
                using (var connection = new SqlConnection(connectionString))
                using (var command = new SqlCommand("GetTopLogCounters", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    connection.Open();

                    command.Parameters.AddWithValue("@SinceTime", sinceTime);

                    List<LogCounterData> returnList = null;
                    using (var reader = command.ExecuteReader())
                    {
                        string lastCounterName = string.Empty;
                        while (reader.Read())
                        {
                            if (returnList == null ||
                                !string.Equals(reader["CounterName"].ToString(), lastCounterName, StringComparison.CurrentCultureIgnoreCase))
                            {
                                returnList = new List<LogCounterData>();
                                returnDictionary.Add((string)reader["countername"], returnList);
                                lastCounterName = (string)reader["countername"];
                            }
                            returnList.Add(new LogCounterData
                                {
                                    CounterValue = (decimal)reader["CounterValue"],
                                    CounterName = (string)reader["countername"],
                                    TimeStamp = (DateTime)reader["TimeStamp"]
                                });
                        }
                    }
                }
            }
            catch (Exception)
            {
                //fail silently
            }
            return returnDictionary;
        }
    }
}
