﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using Starbucks.LogCounter.Provider.Common;

namespace Starbucks.LogCounter.Provider
{   
    public class LogCounterManager : ILogCounterManager
    {
        private static readonly LogCounterManager _instance = new LogCounterManager();
        private readonly Dictionary<string, LogCounter> _counters = new Dictionary<string, LogCounter>();
        private readonly object _synconizationObject = new object();

        private LogCounterManager()
        { }

        public static LogCounterManager Instance()
        {
            return _instance;
        }

        public void Increment(string name)
        {
            try
            {

                if (!string.Equals(ConfigurationManager.AppSettings["DisableLogCounter"], "true"))
                {
                    var incrementor = new Incrementor { CounterName = name, Increment = 1 };
                    ThreadPool.QueueUserWorkItem(IncrementCounter, incrementor);
                }
            }
            catch (Exception)
            {
                //Never bubble exception
            }
        }

        public void Increment(string name, decimal value)
        {
            if (!string.Equals(ConfigurationManager.AppSettings["DisableLogCounter"], "true"))
            {
                var incrementor = new Incrementor { CounterName = name, Increment = value };
                ThreadPool.QueueUserWorkItem(IncrementCounter, incrementor);
            }
        }


        private void IncrementCounter(object state)
        {
            try
            {
                var incrementor = state as Incrementor;
                if (incrementor != null)
                {
                    lock (_synconizationObject)
                    {
                        if (_counters.ContainsKey(incrementor.CounterName))
                        {
                            var counter = _counters[incrementor.CounterName];
                            if (counter != null)
                            {
                                if (incrementor.Increment > 0)
                                {
                                    counter.Increment();
                                }
                                else
                                {
                                    counter.Increment(incrementor.Increment);
                                }
                            }
                        }
                        else
                        {
                            var counter =
                                new Starbucks.LogCounter.Provider.LogCounter(new LogCounterData
                                    {
                                    CounterName = incrementor.CounterName,
                                    CounterValue = incrementor.Increment
                                });
                            counter.Commit += Commited;
                            _counters.Add(incrementor.CounterName, counter);
                        }
                    }
                }
            }
            catch (Exception)
            {
                //Make sure we don't kill the process if there is an exception
            }
        }

        void Commited(object sender, EventArgs e)
        {
            var counter = sender as Starbucks.LogCounter.Provider.LogCounter;
            try
            {
                if (counter != null && counter.Data != null)
                {
                    if (counter.Data.CounterValue > 0)
                    {
                        counter.Data.MachineName = Environment.MachineName;
                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LogCounterQueuePathWCF"]))
                        {
                            if (LogCounterDal.SaveCounterWCF(counter.Data))
                            {
                                if (_counters.ContainsKey(counter.Data.CounterName))
                                {
                                    _counters[counter.Data.CounterName].Clear();
                                }
                            }
                            else
                            {
                                if (_counters.ContainsKey(counter.Data.CounterName))
                                {
                                    _counters[counter.Data.CounterName].Reset();
                                }
                            }
                        }
                        else if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LogCounterQueuePath"]))
                        {
                            if (LogCounterDal.SaveCounter(counter.Data))
                            {
                                if (_counters.ContainsKey(counter.Data.CounterName))
                                {
                                    _counters[counter.Data.CounterName].Clear();
                                }
                            }
                            else
                            {
                                if (_counters.ContainsKey(counter.Data.CounterName))
                                {
                                    _counters[counter.Data.CounterName].Reset();
                                }
                            }
                        }

                    }
                    else
                    {
                        if (_counters.ContainsKey(counter.Data.CounterName))
                        {
                            _counters[counter.Data.CounterName].Clear();
                        }
                    }
                }
            }
            catch (Exception)
            {
                try
                {
                    if (_counters.ContainsKey(counter.Data.CounterName))
                    {
                        _counters[counter.Data.CounterName].Reset();
                    }
                }
                catch (Exception)
                {
                    //swallow so we don't kill process.
                }
            }
        }
    }
}
