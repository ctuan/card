﻿using System.ServiceModel;

namespace Starbucks.LogCounter.Provider
{

    [ServiceContract]
    public interface ICounterServiceContract
    {
        [OperationContract(Name = "LogCounter", IsOneWay = true)]
        void LogCounter(LogCounterData data);
    }
}
