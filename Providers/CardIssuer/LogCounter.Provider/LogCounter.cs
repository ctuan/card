﻿using System;
using System.Threading;

namespace Starbucks.LogCounter.Provider
{
    internal class LogCounter : IDisposable
    {
        public LogCounterData Data { get; private set; }

        private readonly object _syncronizationObject = new object();
        private Timer _logTimer = null;
        private const int FlushTimeout = 60000;

        public LogCounter(LogCounterData data)
        {
            Data = data;
            _logTimer = new Timer(DoCommit, this, FlushTimeout, FlushTimeout);
        }

        public void Increment(decimal value)
        {

            lock (_syncronizationObject)
            {
                Data.CounterValue += value;
            }
        }

        public void Increment()
        {
            lock (_syncronizationObject)
            {
                Data.CounterValue++;
            }
        }

        public void Clear()
        {
            Data.CounterValue = 0;
            _logTimer.Change(FlushTimeout, FlushTimeout);
        }

        public void Reset()
        {
            _logTimer.Change(FlushTimeout, FlushTimeout);
        }


        internal event EventHandler Commit;

        public void DoCommit(object state)
        {
            try
            {
                _logTimer.Change(Timeout.Infinite, Timeout.Infinite);
                if (Commit != null)
                {
                    lock (_syncronizationObject)
                    {
                        Commit(state, new EventArgs());
                    }
                }
            }
            catch (Exception)
            {
                //do not kill the process if fails
            }
        }

        private bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_logTimer != null)
                        _logTimer.Dispose();

                }

                _logTimer = null;
                _disposed = true;
            }

        }
    }
}
