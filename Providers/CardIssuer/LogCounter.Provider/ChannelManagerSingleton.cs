﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.Threading;

namespace Starbucks.LogCounter.Provider
{
    /// <summary>
    /// Default implementation of the <see cref="IChannelManager"/>.
    /// Contains methods that enable the creation of channels that implement
    /// specific service interfaces. This class is designed to be used with
    /// Unity in order for transparent factory production of service channels.
    /// This class can be used to provide Unity with a delegate
    /// for creating channels implementing the specified service interface.
    /// </summary>
    public class ChannelManagerSingleton : IDisposable 
    {
        #region Singleton implementation

        ChannelManagerSingleton()
        {
        }

        public static ChannelManagerSingleton Instance
        {
            get
            {
                return Nested.instance;
            }
        }

        class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }

            internal static readonly ChannelManagerSingleton instance = new ChannelManagerSingleton();
        }

        #endregion

        readonly Dictionary<Type, object> _channels = new Dictionary<Type, object>();

        /* LockRecursionPolicy.SupportsRecursion may not be required. It has been noticed 
         * that recursion is occuring occasionally, and SupportsRecursion has been assigned 
         * to prevent this problem. In short, it's a hack for now. */
        ReaderWriterLockSlim _channelsLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
        //readonly string channelIdentifier = Assembly.GetExecutingAssembly().GetName().Name;

        /// <summary>
        /// Creates or retrieves a cached service channel.
        /// <example>
        /// var staticFactoryConfiguration = unityContainer.Configure<IStaticFactoryConfiguration>();
        ///	Debug.Assert(staticFactoryConfiguration != null);
        ///	if (staticFactoryConfiguration != null)
        ///	{
        ///		staticFactoryConfiguration.RegisterFactory<IYourService>(
        ///			ChannelManagerSingleton.Instance.GetChannel<IYourService>);
        /// 
        ///		/* Attempt to retrieve and use the service. */
        ///		var yourService = UnitySingleton.Container.Resolve<IYourService>();
        ///		yourService.DoSomething();
        /// }
        /// </example>
        /// </summary>
        /// <param name="unityContainer">The unity container.</param>
        /// <returns>A channel of the specified type.</returns>
        /// <exception cref="CommunicationException">Occurs if the service implements <see cref="IServiceContract"/>, 
        /// and the call to InitiateConnection results in a <code>CommunicationException</code></exception>
        /// <exception cref="SecurityNegotiationException">Occurs if the service implements <see cref="IServiceContract"/>, 
        /// and the call to InitiateConnection results in a <code>SecurityNegotiationException</code></exception>
        public TChannel GetChannel<TChannel>(NetMsmqBinding binding, EndpointAddress address)
        {
            Type serviceType = typeof(TChannel);
            object service;

            _channelsLock.EnterUpgradeableReadLock();
            try
            {
                if (!_channels.TryGetValue(serviceType, out service))
                {
#if LOGINFO
                    Logger.Write(String.Format("Creating channel for '{0}'", typeof(TChannel)));
#endif

                    /* Value not in cache, therefore we create it. */
                    _channelsLock.EnterWriteLock();
                    try
                    {
                        /* We don't cache the factory as it contains a list of channels 
                         * that aren't removed if a fault occurs. */
                        var channelFactory = new ChannelFactory<TChannel>(binding);

                        service = channelFactory.CreateChannel(address);
                        var communicationObject = (ICommunicationObject)service;
                        communicationObject.Faulted += OnChannelFaulted;
                        _channels.Add(serviceType, service);
                        communicationObject.Open();

                    }
                    finally
                    {
                        _channelsLock.ExitWriteLock();
                    }
                }
            }
            finally
            {
                _channelsLock.ExitUpgradeableReadLock();
            }

            return (TChannel)service;
        }
        /// <summary>
        /// Called when a channel fault occurs. The channel is removed from the channels cache
        /// so that the next request to retrieve the channel will see it recreated.
        /// </summary>
        /// <param name="sender">The <see cref="ICommunicationObject"/> sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        void OnChannelFaulted(object sender, EventArgs e)
        {
            var communicationObject = (ICommunicationObject)sender;

#if LOGINFO
            Logger.Write("Channel faulted. {0}", communicationObject.ToString());
#endif
            try
            {
                communicationObject.Abort();
            }
            catch (Exception ex)
            {               
#if LOGINFO
                Logger.Write(string.Format("Unable to abort channel. {0}", ex.Message));
#endif
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }

            _channelsLock.EnterWriteLock();
            try
            {
                communicationObject.Faulted -= OnChannelFaulted;

                var keys = from pair in _channels
                           where pair.Value == communicationObject
                           select pair.Key;

                /* Remove all items matching the channel. 
                 * This is somewhat defensive as there should only be one instance 
                 * of the channel in the channel dictionary. */
                foreach (var key in keys.ToList())
                {
                    _channels.Remove(key);
                }
            }
            finally
            {
                _channelsLock.ExitWriteLock();
            }
        }

        private bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_channelsLock  != null)
                        _channelsLock.Dispose();

                }

                _channelsLock = null;
                _disposed = true;
            }

        }
    }
}
