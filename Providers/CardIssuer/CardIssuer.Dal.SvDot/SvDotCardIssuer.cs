﻿using System.Globalization;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.Common.Resources;
using Starbucks.CardIssuer.Dal.SvDot.Common;
using Starbucks.CardIssuer.Dal.SvDot.Configuration;
using Starbucks.CardIssuer.Dal.SvDot.Db;
using Starbucks.CardIssuer.Dal.SvDot.Models;
using Starbucks.CardIssuer.Dal.SvDot.TransportModels;
using Starbucks.CardIssuer.Dal.SvDot.Utility;
using Starbucks.LogCounter.Provider;
using Starbucks.LogCounter.Provider.Common;
using Starbucks.MessageBroker.Common;
using Starbucks.MessageBroker.Common.Models;
using Starbucks.Platform.Security;
using System;
using System.Collections.Generic;
using Starbucks.Settings.Provider.Common;

namespace Starbucks.CardIssuer.Dal.SvDot
{
    public class SvDotCardIssuer : ICardIssuerDal
    {
        private readonly IRequestor _requestor;
        private readonly ICardTransactionDal _cardDal;
        private readonly IMessageBroker _messageBroker;
        private readonly ISettingsProvider _settingsProvider;
        private readonly ICardTransactionDal _cardTransactionDal;
        private readonly ILogCounterManager _logCounterManager;

        private static int _rollbackAttempts;

        private const string UnKnownPlatform = "unknown";

        public SvDotCardIssuer(IRequestor requestor, ICardTransactionDal cardDal, IMessageBroker messageBroker, SvDotConfigurationSettings svDotConfigurationSettings
                               , ISettingsProvider settingsProvider, ICardTransactionDal cardTransactionDal, ILogCounterManager logCounterManager)
        {
            if (requestor == null) throw new ArgumentNullException("requestor");
            if (cardDal == null) throw new ArgumentNullException("cardDal");
            if (messageBroker == null) throw new ArgumentNullException("messageBroker");
            if (settingsProvider == null) throw new ArgumentNullException("settingsProvider");
            if (cardTransactionDal == null) throw new ArgumentNullException("cardTransactionDal");
            if (logCounterManager == null) throw new ArgumentNullException("logCounterManager");
            _requestor = requestor;
            _cardDal = cardDal;
            _messageBroker = messageBroker;
            _settingsProvider = settingsProvider;
            _cardTransactionDal = cardTransactionDal;
            _logCounterManager = logCounterManager;
            _rollbackAttempts = svDotConfigurationSettings.RollbackAttempts ?? 1;
        }

        public ICardTransaction SendMwk(IMerchantInfo merchantInfo)
        {
            var request = new SvDotRequestBuilder(merchantInfo, Enums.CallItCommand.Encrypt, _cardDal);
            //Add the merchant key encryption ID and key.
            request.AddField(SvDotFieldCodes.MERCHANT_KEY_ID, merchantInfo.EncryptionId);
            request.AddField(SvDotRequestField.WorkingKey.GetField(), merchantInfo.Emwk);
            request.WithCurrency = false;
            var trans = SendRequest(request);
            return trans;
        }

        public ICardTransaction Activate(IMerchantInfo merchantInfo, string cardNumber, string pin, decimal amount)
        {
            return this.SendSimpleTransaction(Enums.CallItCommand.ActivatePhysical, merchantInfo, cardNumber, amount, pin);
        }

        public ICardTransaction CashOut(IMerchantInfo merchantInfo, string cardNumber, string pin, decimal amount)
        {
            return this.SendSimpleTransaction(Enums.CallItCommand.Cashout, merchantInfo, cardNumber, amount, pin);
        }

        public ICardTransaction DisablePin(IMerchantInfo merchantInfo, string cardNumber, string pin)
        {
            return this.SendSimpleTransaction(Enums.CallItCommand.Disable, merchantInfo, cardNumber, pin: pin, withCurrency: false);
        }

        public ICardTransaction GetBalance(IMerchantInfo merchantInfo, string cardNumber, string pin)
        {
            var trans = this.SendSimpleTransaction(Enums.CallItCommand.Balance, merchantInfo, cardNumber, pin: pin);

            // Hack to always return base currency for Australian and Canadian cards.
            // This is necessary because Australia and Canada don't have their own
            // merchant ID's yet, and use the US merchant ID. The legacy behavior is
            // to display the base currency, and we need to continue to support it
            // until these markets get their own merchant ID's.
            // TODO: CAN WE PULL "CAD" OUT FROM THE LOGIC BELOW NOW THAT CANADA SITE HAS LAUNCHED?)
            // Removed "CAD" for TFS# 48026, same is removed from ValueLinkProxy.cs
            if ((merchantInfo.SubMarketId == "US") && ((trans.BaseCurrency == "AUD") || (trans.BaseCurrency == "THB")))
            {
                trans.Currency = trans.BaseCurrency;
                trans.Amount = trans.AmountInBaseCurrency;
                trans.BeginningBalance = trans.BeginingBalanceInBaseCurrency;
                trans.EndingBalance = trans.EndingBalanceInBaseCurrency;
            }
            _logCounterManager.Increment("card_balance_check");
            _logCounterManager.Increment(string.Format("card_balance_check_{0}", string.IsNullOrEmpty(merchantInfo.SubMarketId) ? string.Empty : merchantInfo.SubMarketId));
            return trans;
        }

        public ICardTransaction BalanceLock(IMerchantInfo merchantInfo, string cardNumber, string pin)
        {
            _logCounterManager.Increment("card_balance_lock");
            _logCounterManager.Increment(string.Format("card_balance_lock_{0}", string.IsNullOrEmpty(merchantInfo.SubMarketId) ? string.Empty : merchantInfo.SubMarketId));

            var trans = this.SendSimpleTransaction(Enums.CallItCommand.BalanceLock, merchantInfo, cardNumber, pin: pin);
            return trans;
        }

        public List<ICardTransaction> GetHistoryPinless(IMerchantInfo merchantInfo, string cardNumber)
        {
            return GetHistory(merchantInfo, cardNumber, null);
        }

        public List<ICardTransaction> GetHistory(IMerchantInfo merchantInfo, string cardNumber, string pin = null)
        {
            return _requestor.GetHistory(merchantInfo, cardNumber, pin, Enums.CallItCommand.History);
        }

        public List<ICardTransaction> GetHistoryCondensed(IMerchantInfo merchantInfo, string cardNumber, string pin = null)
        {
            return _requestor.GetHistory(merchantInfo, cardNumber, pin, Enums.CallItCommand.HistoryCondensed);
        }

        public ICardTransaction Reload(IMerchantInfo merchantInfo, string cardNumber, decimal amount)
        {
            _logCounterManager.Increment("card_reload");
            _logCounterManager.Increment(string.Format("svdot_card_reload_{0}", string.IsNullOrEmpty(merchantInfo.SubMarketId) ? string.Empty : merchantInfo.SubMarketId));

            return this.SendSimpleTransaction(Enums.CallItCommand.Reload, merchantInfo, cardNumber, amount);
        }

        public ICardTransaction Reload(IMerchantInfo merchantInfo, string cardNumber, string pin, decimal amount)
        {
            return Reload(merchantInfo, cardNumber, pin, amount, false);
        }

        public ICardTransaction Reload(IMerchantInfo merchantInfo, string cardNumber, string pin, decimal amount, bool isAutoreload)
        {
            //Set the reload command to Autoreload or standard Reload.
            _logCounterManager.Increment("card_reload");
            _logCounterManager.Increment(string.Format("svdot_card_reload_{0}", string.IsNullOrEmpty(merchantInfo.SubMarketId) ? string.Empty : merchantInfo.SubMarketId));
            Enums.CallItCommand reloadCommand = isAutoreload ? Enums.CallItCommand.AutomaticReload : Enums.CallItCommand.Reload;
            return this.SendSimpleTransaction(reloadCommand, merchantInfo, cardNumber, amount, pin);
        }

        public ICardTransaction ReloadInStore(IMerchantInfo merchantInfo, string cardNumber, decimal amount)
        {
            //Set the reload command to Autoreload or standard Reload.

            return this.SendSimpleTransaction(Enums.CallItCommand.InStoreReload, merchantInfo, cardNumber, amount);
        }

        public ICardTransaction VoidRequest(ICardTransaction voidTransaction, IMerchantInfo merchInfo)
        {
            //Get the request code, and parse it to the command enum.
            int requestCode;
            long transactionId;
            var command = Enums.CallItCommand.Balance; //The command.  Set it to Balance so it has an initial value.

            //Try to parse the request code to an int.
            if (int.TryParse(voidTransaction.RequestCode, out requestCode))
            {
                try
                {
                    command = (Enums.CallItCommand)requestCode;
                }
                catch (InvalidCastException ex)
                {
                    _logCounterManager.Increment("valuelink_error");
                    _logCounterManager.Increment(string.Format("svdot_void_error_{0}", string.IsNullOrEmpty(merchInfo.SubMarketId) ? string.Empty : merchInfo.SubMarketId));
                    throw new CardIssuerException(CardIssuerErrorResource.InvalidRequestCodeCode, CardIssuerErrorResource.InvalidRequestCodeMessage, ex);
                }
            }
            else
            {
                _logCounterManager.Increment("valuelink_error");
                _logCounterManager.Increment(string.Format("svdot_void_error_{0}", string.IsNullOrEmpty(merchInfo.SubMarketId) ? string.Empty : merchInfo.SubMarketId));
                throw new CardIssuerException(CardIssuerErrorResource.InvalidRequestCodeCode, CardIssuerErrorResource.InvalidRequestCodeMessage);
            }
            //Try to parse the transaction id.
            if (long.TryParse(voidTransaction.TransactionId, out transactionId))
            {
                return VoidRequest(command, merchInfo, transactionId);
            }

            _logCounterManager.Increment("valuelink_error");
            _logCounterManager.Increment(string.Format("svdot_void_error_{0}", string.IsNullOrEmpty(merchInfo.SubMarketId) ? string.Empty : merchInfo.SubMarketId));

            throw new CardIssuerException(CardIssuerErrorResource.InvalidTransactionIdCode,
                                          CardIssuerErrorResource.CardIsAlreadyRegisteredMessage);
        }

        public ICardTransaction VoidRequest(Enums.CallItCommand command, IMerchantInfo merchInfo, long transactionId)
        {
            //SvDot transaction does not need command parameter.  We'll get the command from the transaction record.
            var request = new SvDotRequestBuilder(merchInfo, command, _cardDal).GetTransactionRequest(merchInfo,
                                                                                                      transactionId);

            bool canVoid = true;
            //Set the amount, but only for operations that can be voided.
            switch (request.RequestCode)
            {
                case Enums.CallItCommand.Activate:
                case Enums.CallItCommand.ActivatePhysical:
                    request.RequestCode = Enums.CallItCommand.VoidOfActivation;
                    break;
                case Enums.CallItCommand.Redeem:
                case Enums.CallItCommand.Cashout:
                    request.RequestCode = Enums.CallItCommand.VoidOfRedemption;
                    break;
                case Enums.CallItCommand.Reload:
                case Enums.CallItCommand.AutomaticReload:
                    request.RequestCode = Enums.CallItCommand.VoidOfReload;
                    break;
                case Enums.CallItCommand.BalanceMerge:
                    request.RequestCode = Enums.CallItCommand.VoidOfBalanceMerge;
                    request.WithCurrency = false;
                    break;
                default:
                    canVoid = false;
                    break;
            }

            if (!canVoid)
            {
                throw new CardIssuerException(CardIssuerErrorResource.CannotVoidTransactionCode,
                                              CardIssuerErrorResource.CannotVoidTransactionMessage);
                // throw new ValueLinkException(string.Format("Cannot void transaction {0} with request code {1}", transactionId, (int)request.RequestCode));
            }
            _logCounterManager.Increment("card_void");
            _logCounterManager.Increment(string.Format("svdot_card_void_request_{0}", request.RequestCode));
            var trans = SendRequest(request);
            return trans;
        }

        public ICardTransaction Tip(IMerchantInfo merchantInfo, string cardNumber, decimal amount, DateTime localTransactionTime, long localTransactionId)
        {
            _logCounterManager.Increment("card_tip");
            _logCounterManager.Increment(string.Format("svdot_card_tip_{0}", merchantInfo.SubMarketId));

            return this.SendTipTransaction(merchantInfo, cardNumber, amount, localTransactionTime, localTransactionId);
        }

        public ICardTransaction Redeem(IMerchantInfo merchantInfo, string cardNumber, string pin, decimal amount)
        {
            _logCounterManager.Increment("card_redeem");
            _logCounterManager.Increment(string.Format("svdot_card_redeem_{0}", merchantInfo.SubMarketId));

            return this.SendSimpleTransaction(Enums.CallItCommand.Redeem, merchantInfo, cardNumber, amount, pin);
        }

        public ICardTransaction RedemptionUnlock(IMerchantInfo merchantInfo, string cardNumber, string pin, decimal amount)
        {
            _logCounterManager.Increment("card_redemption_unlock");
            _logCounterManager.Increment(string.Format("svdot_card_redemption_unlock_{0}", merchantInfo.SubMarketId));

            return this.SendSimpleTransaction(Enums.CallItCommand.RedemptionUnlock, merchantInfo, cardNumber, pin: pin, amount: amount);
        }

        public ICardTransaction TransferBalance(IMerchantInfo merchantInfo, string cardNumberFrom, string cardNumberTo)
        {
            var request = new SvDotRequestBuilder(merchantInfo, Enums.CallItCommand.BalanceMerge, _cardDal);
            request.CardNumber = cardNumberFrom;
            //Add a field for the Target Card number.
            request.AddField(SvDotFieldCodes.TARGET_CARD_NUMBER, cardNumberTo);
            //Set currency flag to false, since we will transfer entire balance.
            request.WithCurrency = false;
            var trans = SendRequest(request);
            _logCounterManager.Increment("card_transfer_balance");
            _logCounterManager.Increment(string.Format("svdot_card_transfer_balance_{0}", merchantInfo.SubMarketId));
            return trans;
        }

        public ICardTransaction ActivateVirtual(IMerchantInfo merchantInfo, string promoCode, decimal amount)
        {
            var request = new SvDotRequestBuilder(merchantInfo, Enums.CallItCommand.Activate, _cardDal);
            request.Amount = amount;
            //Add the promo code field for the card promotion from which to issue the card.
            request.AddField(SvDotFieldCodes.PROMOTION_CODE, promoCode);
            //Add the merchant key encryption ID to encrypt the PIN value that is returned.
            request.AddField(SvDotFieldCodes.MERCHANT_KEY_ID, merchantInfo.EncryptionId);
            //Add the pin indicator so the PIN gets returned.
            request.AddField(SvDotFieldCodes.SOURCE_CODE, SvDotFieldCodes.PIN_TRANSACTION);

            var trans = SendRequest(request);

            int returnCode = -1;
            if (int.TryParse(trans.ResponseCode, out returnCode) && returnCode == 0)
            {
                trans.Pin = ValueLinkEncryption.DecryptPIN(merchantInfo.Mwk, trans.Pin);
                //var cardTransactionData = new CardTransactionsDataUtility();
                long tranId;
                long.TryParse(trans.TransactionId, out tranId);
                _cardDal.SetVirtualActivationValues(tranId, Encryption.EncryptCardNumber(trans.CardNumber), Encryption.EncryptPin(trans.Pin));
            }

            return trans;
        }

        public IMerchantInfo GetMerchantInfo(string subMarket, string platform = UnKnownPlatform)
        {
            var sanitizedSubmarket = _settingsProvider.GetStandardizedSubMarketCode(subMarket);
            return _cardTransactionDal.GetMerchantInfo(sanitizedSubmarket, platform);
        }

        public IMerchantInfo GetMerchantInfoByMerchantId(string merchantId, string platform = UnKnownPlatform)
        {
            return _cardTransactionDal.GetMerchantInfoByMerchantId(merchantId, platform);
        }

        public IMerchantInfo GetMerchantInfoByCurrency(string currency)
        {
            return _cardTransactionDal.GetMerchantInfoByCurrency(currency);
        }

        public IOrderAddressInfo GetOrderAddressInfo(string orderId)
        {
            return _cardTransactionDal.GetOrderGroupAddress(orderId);
        }

        public long UpsertPartialTransferTransactionDetails(long cardAmountTransactionId, string toCardTransactionId,
                                                           DateTime toCardTransactionDate
                                                           , string fromCardTransactionId,
                                                           DateTime? fromCardTransactionDate)
        {
            return _cardTransactionDal.UpsertPartialTransferTransactionDetails(cardAmountTransactionId, toCardTransactionId,
                                                           toCardTransactionDate
                                                           , fromCardTransactionId,
                                                            fromCardTransactionDate);
        }

        /// <summary>
        /// Sends a request to charge a tip amount to a card.
        /// </summary>
        /// <param name="merchantInfo">Merchant Information, including MID, AltMid, and encryption keys</param>
        /// <param name="cardNumber">The 16-digit SVC number</param>
        /// <param name="amount">The amount of the transaction for those that require amount</param>
        /// <param name="localTransactionTime">The local time of the original redemption transaction.</param>
        /// <param name="localTransactionId">The local ID of the original redemption transaction.</param>
        /// <returns></returns>
        private ICardTransaction SendTipTransaction(IMerchantInfo merchantInfo, string cardNumber, decimal amount, DateTime localTransactionTime, long localTransactionId)
        {
            var request = new SvDotRequestBuilder(merchantInfo, Enums.CallItCommand.Redeem, _cardDal, localTransactionTime, localTransactionId)
            {
                Amount = amount,
                CardNumber = cardNumber
            };

            return SendRequest(request);
        }


        /// <summary>
        /// Sends a simple monetary transaction request, such as Balance Check, Reload, Redeem, Activate, Cashout
        /// that does not require additional parameter fields.
        /// </summary>
        /// <param name="command">The request type to execute</param>
        /// <param name="merchantInfo">Merchant Information, including MID, AltMid, and encryption keys</param>
        /// <param name="cardNumber">The 16-digit SVC number</param>
        /// <param name="amount"><remarks>optional</remarks> The amount of the transaction for those that require amount</param>
        /// <param name="pin"><remarks>optional</remarks> The PIN on the card for those transactions that require PIN</param>
        /// <param name="withCurrency">Indicator that currency should be sent with request</param>
        /// <returns>A CardTransaction containing the data from the response.</returns>
        private ICardTransaction SendSimpleTransaction(Enums.CallItCommand command, IMerchantInfo merchantInfo, string cardNumber, decimal amount = 0M, string pin = null, bool withCurrency = true)
        {
            var request = new SvDotRequestBuilder(merchantInfo, command, _cardDal);
            request.CardNumber = cardNumber;

            if (!string.IsNullOrEmpty(pin))
            {
                Helpers.CheckPin(pin);
                request.Pin = pin;
            }

            request.WithCurrency = withCurrency;
            request.Amount = amount;

            var trans = SendRequest(request);
            return trans;
        }

        /// <summary>
        /// Send the request. Get the response.
        /// </summary>
        /// <param name="request">The <see cref="SvDotRequestBuilder"/> containing the request</param>
        /// <returns>The transaction response parsed into a <see cref="CardTransaction"/></returns>
        private ICardTransaction SendRequest(ISvDotRequestBuilder request)
        {
            var trans = _requestor.SendRequest(request, _rollbackAttempts);
            SendNotification(trans, request);
            return trans;
        }

        private void SendNotification(ICardTransaction transaction, ISvDotRequestBuilder request)
        {
            //Kick out if I failed
            if (!transaction.TransactionSucceeded) return;
            var cardNumber2 = string.Empty;
            if (request.RequestCode == Enums.CallItCommand.BalanceMerge)
            {
                cardNumber2 = transaction.CardNumber;
                transaction.CardNumber = request.CardNumber;
            }
            //SvDotFieldCodes.TARGET_CARD_NUMBER
            var data = new SvcCardNotification
            {
                BaseCurrency = transaction.BaseCurrency,
                CardNumber = transaction.CardNumber,
                CardNumber2 = cardNumber2,
                LocalTransactionId = transaction.TransactionId,
                LocalTransactionTime = transaction.TransactionDate,
                LocalCurrency = transaction.Currency,
                MerchantId = transaction.MerchantId,
                NewBalance = transaction.EndingBalance,
                PreviousBalance = transaction.BeginningBalance,
                StoreId = transaction.StoreId,
                Source = "API",
                SourceActionCode = transaction.RequestCode,
                SourceActionResponse = transaction.ResponseCode,
                TerminalId = transaction.TerminalId,
                TransactionAmount = transaction.Amount,
                TransactionUtcTime = transaction.UtcDate,
            };

            _messageBroker.Publish(data);
        }
    }
}
