﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.SvDot.Common;
using Starbucks.CardIssuer.Dal.SvDot.Models;
using Starbucks.CardIssuer.Dal.SvDot.Utility;

namespace Starbucks.CardIssuer.Dal.SvDot.Requestor
{
    public class MockServiceRequestor : IRequestor
    {
        private readonly int _historyDelay;
        private readonly int _sendRequestDelay;
        private static readonly Random Random = new Random();
            
        public MockServiceRequestor():this (0, 0)
        {
            
        }
        public MockServiceRequestor(int historyDelay, int sendRequestDelay)
        {
            _historyDelay = historyDelay;
            _sendRequestDelay = sendRequestDelay;
        }

        public ICardTransaction SendRequest(ISvDotRequestBuilder request, int rollbackAttempts)
        {
            Thread.Sleep(_sendRequestDelay);
            return CreateMockedTransaction();
        }      

        public List<ICardTransaction> GetHistory(IMerchantInfo merchantInfo, string cardNumber, string PIN, Enums.CallItCommand callItCommand)
        {
            
            var retVal = new List<ICardTransaction>();

            int cnt = new Random().Next(3, 20);

            for (int i = 0; i < cnt; i++)
            {
                retVal.Add(CreateMockedTransaction());
            }

            Thread.Sleep(_historyDelay);
            return retVal;
        }

        private CardTransaction CreateMockedTransaction()
        {
            var mockery = new CardTransaction();
            mockery.TransactionId = "11111111";
            mockery.TransactionDate = DateTime.Now.AddMinutes(-(Random.Next(5,3600)));
            mockery.UtcDate = DateTime.UtcNow;
            mockery.Description = "Mocked transaction simulated for testing purposes.";

            //generate random balance;
            var val = (int)Random.Next(5,100);
            mockery.Currency = "PLN"; // The Polish Zloty
            mockery.Amount = val;
            mockery.AmountInBaseCurrency = val;
            mockery.BeginningBalance = val;
            mockery.BeginingBalanceInBaseCurrency = val;
            mockery.EndingBalance = val;
            mockery.EndingBalanceInBaseCurrency = val;
            // Set response codes accordingly
            mockery.AuthorizationCode = "382318"; // Per test team request
            mockery.ResponseCode = "00";
            mockery.RequestCode = "2400";
            mockery.CardClass = "205";

            return mockery;
        }

        
    }
}
