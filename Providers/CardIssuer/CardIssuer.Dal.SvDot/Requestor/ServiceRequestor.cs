﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.Common.Resources;
using Starbucks.CardIssuer.Dal.SvDot.Common;
using Starbucks.CardIssuer.Dal.SvDot.Configuration;
using Starbucks.CardIssuer.Dal.SvDot.Db;
using Starbucks.CardIssuer.Dal.SvDot.Utility;
using Starbucks.LogCounter.Provider;
using Starbucks.LogCounter.Provider.Common;

namespace Starbucks.CardIssuer.Dal.SvDot.Requestor
{
    public class ServiceRequestor : IRequestor
    {
        private readonly ICardTransactionDal _cardDal;
        private readonly ISvDotTransport _transport;
        private readonly SvDotConfigurationSettings _svDotConfigurationSettings;
        private readonly ILogCounterManager _logCounterManager;

        public ServiceRequestor(ICardTransactionDal cardDal, ISvDotTransport transport, SvDotConfigurationSettings svDotConfigurationSettings, ILogCounterManager logCounterManager )//SVDotTransport needs to be new per call right now.
        {
            if (cardDal == null) throw new ArgumentNullException("cardDal");
            if (transport == null) throw new ArgumentNullException("transport");
            if (svDotConfigurationSettings == null) throw new ArgumentNullException("svDotConfigurationSettings");
            if (logCounterManager == null) throw new ArgumentNullException("logCounterManager");
            _cardDal = cardDal;
            _transport = transport;
            _svDotConfigurationSettings = svDotConfigurationSettings;
            _logCounterManager = logCounterManager;
        }

        public ICardTransaction SendRequest(ISvDotRequestBuilder request, int rollbackAttempts)
        {
            //Save the response, and get the card transaction.
            int result;

            bool retry;
            var responseBuilder = GetResponse(request, out result, out retry, _cardDal);

            //Check transport result.  If it is non-zero, attempt a rollback, since we don't know if the request made it through.
            if (result > 0)
            {
                //If the transaction was not retried, then roll it back. ROLLBACK ONLY CERTAIN TRANSACTION TYPES, the ones that process money.
                if (!retry
                    && request.RequestCode != Enums.CallItCommand.Balance
                    && request.RequestCode != Enums.CallItCommand.History
                    && request.RequestCode != Enums.CallItCommand.Disable)
                {
                    Rollback(request, rollbackAttempts);
                }

                //Create an exception.
                _logCounterManager.Increment("valuelink_communication_error");
                _logCounterManager.Increment("svdot_valuelink_communication_error");
                var vex = new CardIssuerException(CardIssuerErrorResource.ValueLinkHostDownCode, CardIssuerErrorResource.ValueLinkHostDownMessage);
                throw vex;
            }

            var trans = responseBuilder.SaveResponse();
            return trans;
        }

        private void Rollback(ISvDotRequestBuilder request, int rollbackAttempts)
        {
            //Add a field for the original request code.
            request.AddField(SvDotFieldCodes.ORIGINAL_TRANSACTION_REQUEST, string.Format("{0:0###}", (int)request.RequestCode));
            //Change the request code to rollback.
            request.RequestCode = Enums.CallItCommand.Rollback;

            for (int i = 0; i < rollbackAttempts; i++)
            {
                int result;
                bool retry;
                //Send the rollback request, get the response.
                var responseBuilder = GetResponse(request, out result, out retry, _cardDal);
                if (result == 0)
                {
                    //Successfully sent roll back, or original transaction never went through, or transaction was already rolled back.  
                    //Save the rollback transaction.
                    responseBuilder.SaveResponse();
                    break;
                }
            }
        }

        private SvDotResponseBuilder GetResponse(ISvDotRequestBuilder request, out int transportResult, out bool retry, ICardTransactionDal cardTransactionDal)
        {
            //Check for a null or empty card number.
            if (string.IsNullOrEmpty(request.CardNumber) && (request.RequestCode != Enums.CallItCommand.Activate) && (request.RequestCode != Enums.CallItCommand.Encrypt))
            {
                //Throw an exception if the card number is null/empty, and not a virtual activation.
                _logCounterManager.Increment("valuelink_cardnumber_error");
                _logCounterManager.Increment("svdot_valuelink_cardnumber_error");
                var vex = new CardIssuerException(CardIssuerErrorResource.InvalidCardNumberCode, CardIssuerErrorResource.InvalidCardNumberMessage);
                throw vex;
            }

            string payload = request.GetRequest();
            //Use the transaction ID with a timestamp as the payload ID.
            string id = GetPayloadId(request.MerchantInfo.SubMarketId, request.TransactionId);

            //For now always return false.
            retry = false;
            //var transport = fac.CreateTransport(payload, id);
            _transport.Payload = payload;
            _transport.PayloadTransactionId = id;
            try
            {
                transportResult = _transport.Send();
            }
            catch (Exception ex)
            {
                //Add the exception as an inner exception to the vl exception and throw.
                _logCounterManager.Increment("valuelink_communication_error");
                _logCounterManager.Increment("svdot_valuelink_communication_error_response");
                var vex = new CardIssuerException(CardIssuerErrorResource.ValueLinkHostDownCode,
                                                  CardIssuerErrorResource.ValueLinkHostDownMessage,
                                                  ex);
                throw vex;
            }

            SvDotResponseBuilder responseBuilder = null;
            if (_transport.Response != null)
            {
                responseBuilder = new SvDotResponseBuilder(_transport.Response, cardTransactionDal, request.LocalTransactionTime);
            }

            return responseBuilder;
        }
        /// <summary>
        /// Creates an ID to identify the payload being sent through Secure Transport
        /// 14-characters, composed of submarket code + transaction ID padded with leading zeros
        /// </summary>
        /// <param name="subMarket">The submarket of the transaction</param>
        /// <param name="transactionId">The unique transaction ID</param>
        /// <returns>An ID composed of the submarket and transaction ID
        /// <example>CA000000012345, MOUS0000001234</example>
        /// </returns>
        private string GetPayloadId(string subMarket, long transactionId)
        {
            //Start the ID with the submarket code.
            var idBuilder = new StringBuilder(subMarket);

            int length = idBuilder.Length;
            //Check the length of the stringbuilder
            int pad = (_svDotConfigurationSettings.PayloadIdLength > length) ? _svDotConfigurationSettings.PayloadIdLength - length : length;

            //Append the transaction ID, padded with zeros.
            idBuilder.Append(transactionId.ToString().PadLeft(pad, '0'));

            return idBuilder.ToString();
        }

        public List<ICardTransaction> GetHistory(IMerchantInfo merchantInfo, string cardNumber, string pin, Enums.CallItCommand callItCommand )
        {
            _logCounterManager.Increment("card_get_history");
            _logCounterManager.Increment(string.Format("card_get_history_{0}", string.IsNullOrEmpty(merchantInfo.SubMarketId) ? string.Empty : merchantInfo.SubMarketId));

            var request = new SvDotRequestBuilder(merchantInfo, callItCommand, _cardDal);
            request.CardNumber = cardNumber;
            request.Pin = pin;
            request.WithCurrency = false; //currency not required.
            request.AddField(SvDotFieldCodes.HISTORY_FORMAT, SvDotFieldCodes.HISTORY_FORMAT_VALUE);
            request.AddField(SvDotFieldCodes.FIRST_TRANSACTION_NUMBER, _svDotConfigurationSettings.TransactionStartValue.ToString(CultureInfo.InvariantCulture));
            request.AddField(SvDotFieldCodes.TRANSACTION_COUNT_REQUEST, _svDotConfigurationSettings.TransactionCount.ToString(CultureInfo.InvariantCulture));

            //Get the response.
            int result;
            //required out parameter.
            bool retry;

            var responseBuilder = GetResponse(request, out result, out retry, _cardDal);

            //Check for transport errors.
            if (result > 0)
            {
                //Create an exception.
                _logCounterManager.Increment("valuelink_communication_error");
                _logCounterManager.Increment(string.Format("card_history_valuelink_comm_error_{0}", string.IsNullOrEmpty(merchantInfo.SubMarketId) ? string.Empty : merchantInfo.SubMarketId));
                var vex = new CardIssuerException(CardIssuerErrorResource.ValueLinkHostDownCode, CardIssuerErrorResource.ValueLinkHostDownMessage);
                throw vex;
            }

            responseBuilder.SaveResponse();

            var history = responseBuilder.GetTransactionHistory(callItCommand );

            return history;
        }



    }
}
