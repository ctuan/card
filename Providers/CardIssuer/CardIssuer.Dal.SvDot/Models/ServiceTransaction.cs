﻿using Starbucks.CardIssuer.Dal.SvDot.Common;

namespace Starbucks.CardIssuer.Dal.SvDot.Models
{    
    public class ServiceTransaction : IServiceTransaction
    {
        public string RequestCode { get; set; }
        public string MerchantId { get; set; }
        public string TerminalId { get; set; }
        public string AltMerchNo { get; set; }
        public string CardNumber { get; set; }
        public string Pin { get; set; }
        public decimal Amount { get; set; }
        public string SourceCardNumber { get; set; }
    }
}
