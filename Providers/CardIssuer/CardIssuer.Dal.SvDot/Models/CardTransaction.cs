﻿using System;
using Starbucks.CardIssuer.Dal.Common.Models;

namespace Starbucks.CardIssuer.Dal.SvDot.Models
{
    public class CardTransaction : ICardTransaction
    {
        private string merchantIdAndTerminalId;
        private string merchantId;
        private string terminalId;
        public string CardId { get; set; }
        public string TransactionId { get; set; }
        public string AuthorizationCode { get; set; }
        public string CardClass { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public string Description { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal BeginningBalance { get; set; }
        public decimal EndingBalance { get; set; }
        public string RequestCode { get; set; }
        public string ResponseCode { get; set; }

        public bool TransactionSucceeded
        {
            get
            {
                int returnCode = -1;
                if (int.TryParse(ResponseCode, out returnCode))
                {
                    //if the returnCode is a 0, then we have successfully processed, so now reload
                    if (returnCode == 0)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
        }

        public ICardPromotion Promotion { get; set; }
        public string Pin { get; set; }
        public string CardNumber { get; set; }
        public string BaseCurrency { get; set; }
        public decimal BeginingBalanceInBaseCurrency { get; set; }
        public decimal EndingBalanceInBaseCurrency { get; set; }
        public decimal AmountInBaseCurrency { get; set; }

        public string MerchantIdAndTerminalId
        {
            get { return merchantIdAndTerminalId; }
            set
            {
                merchantIdAndTerminalId = value;
                merchantId = merchantIdAndTerminalId == null ? null : merchantIdAndTerminalId.Substring(0, 11);
                terminalId = merchantIdAndTerminalId == null ? null : merchantIdAndTerminalId.Substring(11, 4);
            }
        }
        public string MerchantId
        {
            get { return merchantId; }
            set
            {
                merchantId = value;
                merchantIdAndTerminalId = merchantId + terminalId;
            }
        }

        public string TerminalId
        {
            get { return terminalId; }
            set
            {
                terminalId = value;
                merchantIdAndTerminalId = merchantId + terminalId;
            }
        }

        public string StoreId { get; set; }
        public DateTime UtcDate { get; set; }

		public decimal LockAmount { get; set; }
    }
}
