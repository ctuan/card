﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.CardIssuer.Dal.SvDot.Models
{
    public interface ICardLog
    {
        string UserId { get; set; }
        string CardNumber { get; set; }
        string Message { get; set; }
        string Submarket { get; set; }
        IDictionary<string, string> ParameterCollection { get; set; }
        string CardId { get; set; }
        string CardName { get; set; }
        decimal Amount { get; set; }
        string TransactionId { get; set; }
    }
}
