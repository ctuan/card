﻿using Starbucks.CardIssuer.Dal.Common.Models;

namespace Starbucks.CardIssuer.Dal.SvDot.Models
{
    class MerchantInfo : IMerchantInfo
    {
        public bool HasEncryption { get; set; }
        public string EncryptionId { get; set; }
        public string Mwk { get; set; }
        public string Emwk { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyNumber { get; set; }
        public string Mid { get; set; }
        public string AlternateMid { get; set; }
        public string MarketId { get; set; }
        public string SubMarketId { get; set; }
        public string TerminalId { get; set; }
        public string MerchantKey { get; set; }
        public string TransactionSecurityKey { get; set; }
        public string FraudThreshold { get; set; }
    }
}
