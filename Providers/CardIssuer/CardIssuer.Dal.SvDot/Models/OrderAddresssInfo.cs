﻿using Starbucks.CardIssuer.Dal.Common.Models;

namespace Starbucks.CardIssuer.Dal.SvDot.Models
{
    class OrderAddressInfo : IOrderAddressInfo
    {
        public int AddressType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }
        public string PostalCode { get; set; }
    }
}
