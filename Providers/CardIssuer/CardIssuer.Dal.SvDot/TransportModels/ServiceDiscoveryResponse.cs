﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Starbucks.CardIssuer.Dal.SvDot.TransportModels
{
    [XmlRoot("Response")]
    public class ServiceDiscoveryResponse
    {
        [XmlElement("RespClientID")]
        public ResponseClient Client { get; set; }

        [XmlElement("Status")]
        public ResponseStatus Status { get; set; }

        [XmlElement("ServiceDiscoveryResponse")]
        public ServiceDiscovery Discovery { get; set; }
    }

    [XmlRoot("ServiceDiscoveryResponse")]
    public class ServiceDiscovery
    {
        [XmlElement("ServiceProvider")]
        public List<ServiceProvider> ServiceProviders { get; set; }
    }

    [XmlRoot("ServiceProvider")]
    public class ServiceProvider
    {
        [XmlElement("URL")]
        public string Url { get; set; }

        [XmlElement("MaxTransactionInPackage")]
        public string MaxTransactions { get; set; }
    }

    public class ResponseClient
    {
        [XmlElement("DID")]
        public string TransportId { get; set; }
        [XmlElement("ClientRef")]
        public string TransactionId { get; set; }

    }

    public class ResponseStatus
    {
        [XmlAttribute("StatusCode")]
        public string StatusCode { get; set; }

    }
}
