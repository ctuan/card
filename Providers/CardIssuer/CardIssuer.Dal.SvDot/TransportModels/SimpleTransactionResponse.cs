﻿using System.Xml.Serialization;

namespace Starbucks.CardIssuer.Dal.SvDot.TransportModels
{
    [XmlRoot("Response")]
    public class SimpleTransactionResponse
    {
        [XmlElement("RespClientID")]
        public ResponseClient Client { get; set; }
        [XmlElement("Status")]
        public ResponseStatus Status { get; set; }
        [XmlElement("TransactionResponse")]
        public TransactionResponse TransactionResponse { get; set; }
    }

    public class TransactionResponse
    {
        [XmlElement("ReturnCode")]
        public string ReturnCode { get; set; }
        [XmlElement("Payload")]
        public string Payload { get; set; }
    }
}
