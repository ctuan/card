﻿using System.Xml.Serialization;

namespace Starbucks.CardIssuer.Dal.SvDot.TransportModels
{

    [XmlRoot("Response")]
    public class PingResponse
    {
        [XmlElement("RespClientID")]
        public ResponseClient Client { get; set; }
        [XmlElement("Status")]
        public ResponseStatus Status { get; set; }
        [XmlElement("PingResponse")]
        public PingResult Ping { get; set; }
    }   

   
}
