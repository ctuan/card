﻿using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Resources;
using Starbucks.CardIssuer.Dal.Common.Rule;
using Starbucks.CardIssuer.Dal.SvDot.Common;

namespace Starbucks.CardIssuer.Dal.SvDot.TransportModels
{
    /// <summary>
    /// Helper methods.
    /// </summary>
    internal class Helpers
    {
        private Helpers() { }

        /// <summary>
        /// Check for valid pin format
        /// </summary>
        /// <param name="Pin"></param>
        public static void CheckPin(string Pin)
        {
            if ((Pin.Length != Constants.PinLength) || !Pin.IsInt())
                throw new CardIssuerException(CardIssuerErrorResource.InvalidPinCode, CardIssuerErrorResource.InvalidPinMessage);
        }

        public static void Log(string eventName, string message, bool isError)
        {
            //var logEntry = new LogEntry();
            //logEntry.ActivityId = Guid.NewGuid();
            //logEntry.Title = "SvDot Transport";
            //logEntry.Severity = isError ? System.Diagnostics.TraceEventType.Error : System.Diagnostics.TraceEventType.Information;
            //logEntry.Message = string.Format("{0} : {1}", eventName, message);
            //logEntry.Categories.Add("SvDot"); //TODO: PUT CONFIG CATEGORY INTO CONFIG.
            //Logger.Write(logEntry);
        }

        /// <summary>
        /// Parses a string representation of a decimal with an implied decimal place, and converts it to an actual decimal.
        /// </summary>
        /// <param name="input">String with an implied decimal, such as "1000" for 10.00</param>
        /// <returns>The decimal equivalent or 0 if the string cannot be parsed.</returns>
        public static decimal ParseImpliedDecimal(string input)
        {
            decimal decimalVal;
            decimal.TryParse(input, out decimalVal);
            decimalVal /= 100M;
            return decimalVal;
        }
    }

    public static class Constants
    {
        //Length of card pin
        public const int PinLength = 8;
        //Length of card number
        public const int CardNumberLength = 16;
    }
}
