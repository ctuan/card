﻿using System.Xml.Serialization;

namespace Starbucks.CardIssuer.Dal.SvDot.TransportModels
{

    [XmlRoot("Request")]
    public class PingRequest
    {
        [XmlElement("ReqClientID")]
        public RequestClient Client { get; set; }
        [XmlElement("Ping")]
        public ServicePing Ping { get; set; }
    }

    
}
