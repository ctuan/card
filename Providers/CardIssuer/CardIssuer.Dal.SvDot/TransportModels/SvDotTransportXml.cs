﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Starbucks.CardIssuer.Dal.SvDot.Common;
using Starbucks.CardIssuer.Dal.SvDot.Configuration;

namespace Starbucks.CardIssuer.Dal.SvDot.TransportModels
{
    public class SvDotTransportXml : ISvDotTransport
    {
        private readonly SvDotConfigurationSettings _svDotConfigurationSettings;

        public SvDotTransportXml(SvDotConfigurationSettings svDotConfigurationSettings)
        {
            _svDotConfigurationSettings = svDotConfigurationSettings;
        }  

        private byte[] _responseBytes;
        private RequestClient _transportRequestClient;
        private static readonly object syncObject = new object();

        //Need to replace any non-printable ASCII characters, such as the field separator, so we can pass it in the XML.
        //Escaped hex value of the field separator in the payload.
        private const string FIELD_SEPARATOR = "\x1C";
        //String representation of field separator encoded for XML.
        private const string FIELD_SEPARATOR_XML = "|1C";

        //Object to lock to prevent deadlocking.
        private static object syncRoot = new object();

        private SvDotProvider _activeProvider;

        #region ISvDotTransport Properties        
        public string Payload { get; set; }
        public string PayloadTransactionId { get; set; }
        public byte[] ResponseBytes
        {
            get
            {
                if (_responseBytes == null && Response != null)
                {
                    _responseBytes = Encoding.UTF8.GetBytes(Response);
                }
                return _responseBytes;
            }
        }
        public string Response { get; private set; }

        /// <summary>
        /// Indicates caller should retry Send on error.
        /// <remarks>
        ///     Will not be using this for now. Retry will always be false.
        /// </remarks>
        /// </summary>
        public bool Retry { get; private set; }

        #endregion
        private string MerchantId { get { return _svDotConfigurationSettings.MerchantId; } }
        private string TerminalId { get { return _svDotConfigurationSettings.TerminalId; } }
        private string DatawireId { get { return _svDotConfigurationSettings.DatawireId; } }
        private string ApplicationId { get { return _svDotConfigurationSettings.ApplicationId; } }
        private string ServiceId { get { return _svDotConfigurationSettings.ServiceId; } }
        private string PrimaryServiceDiscoveryUrl { get { return _svDotConfigurationSettings.PrimaryServiceDiscoveryUrl; } }
        private string SecondaryServiceDiscoveryUrl { get { return _svDotConfigurationSettings.SecondaryServiceDiscoveryUrl; } }
        /// <summary>
        /// Timeout for the service discovery call to retrieve available service provider endpoints
        /// </summary>
        private int DiscoveryTimeout { get { return _svDotConfigurationSettings.DiscoveryTimeout ?? 5; } }

        /// <summary>
        /// Timeout for the ping calls to the service providers
        /// </summary>
        private int PingTimeout { get { return _svDotConfigurationSettings.PingTimeout ?? 2; } }

        /// <summary>
        /// Timeout for the SvDot transactions to ValueLink through the Secure Transport
        /// </summary>
        private int TransactionTimeout { get { return _svDotConfigurationSettings.TransactionTimeout ?? 10; } }

        /// <summary>
        /// Time in minutes before active service provider expires.
        /// If 0, does not expire.
        /// </summary>
        private int ServiceProviderExpiry { get { return _svDotConfigurationSettings.ServiceProviderExpiry; } }

        protected RequestClient TransportRequestClient
        {
            get
            {
                if (_transportRequestClient == null)
                {
                    _transportRequestClient = new RequestClient
                    {
                        ApplicationId = ApplicationId,
                        MerchantTerminalId = string.Format("{0}|{1}", MerchantId, TerminalId),
                        TransactionId = PayloadTransactionId,
                        TransportId = DatawireId
                    };
                }
                return _transportRequestClient;
            }
        }

        /// <summary>
        /// Sends the Payload in the transport instance, and populates the Response.
        /// </summary>
        /// <returns>A 0 if OK, otherwise an error number.</returns>
        public int Send()
        {

            var returnStatus = ReturnStatus.OK;

            //Lock while we check the service discovery providers.
            lock (syncObject)
            {
                bool doDiscovery = false;
                //Do service discovery if there are no service providers set, or current active provider has expired.
                if (ServiceDiscoverySingleton.SvDotProviders == null)
                {
                    doDiscovery = true;
                }
                else
                {
                    _activeProvider = ServiceDiscoverySingleton.SvDotProviders.SingleOrDefault(p => p.Active);
                    //If provider has expired, do another discovery.
                    if (_activeProvider != null && _activeProvider.Expiry.HasValue && _activeProvider.Expiry.Value < DateTime.Now)
                    {
                        //Fail over the service providers so we can do a rediscovery.
                        FailOverProvider(_activeProvider.ServiceProviderUrl, true);
                        doDiscovery = true;
                    }
                }

                if (doDiscovery)
                {
                    returnStatus = DoServiceDiscovery();
                    if (returnStatus != ReturnStatus.OK)
                    {
                        return (int)returnStatus;
                    }
                }
            }

            if (_activeProvider != null)
            {
                returnStatus = SendPayload(_activeProvider.ServiceProviderUrl);
                if (returnStatus != ReturnStatus.OK)
                {
                    //An error in the send, try to failover.
                    FailOverProvider(_activeProvider.ServiceProviderUrl, false);
                }
            }
            else
            {
                returnStatus = SetTransportStatus(false, "No Active Service Provider Found.", ReturnStatus.OtherError);
            }

            return (int)returnStatus;
        }

        /// <summary>
        /// Calls Secure Transport to retrieve a set of service provider endpoints.
        /// </summary>
        /// <returns>The <see cref="ReturnStatus"/> the call.  Will return <see cref="ReturnStatus.OK"/> if successful.</returns>
        private ReturnStatus DoServiceDiscovery()
        {

            ReturnStatus returnStatus = ReturnStatus.OK;
            ServiceDiscoveryResponse discoveryResponse = null;

            //
            string serviceDiscoveryUrl = PrimaryServiceDiscoveryUrl;
            for (int i = 0; i < 2; i++)
            {
                Helpers.Log("Service Discovery Start", string.Format("Service Discovery Url: {0}", serviceDiscoveryUrl), false);

                try
                {
                    using (
                        var client = CreateHttpClient(string.Format(@"{0}/{1}", serviceDiscoveryUrl, ServiceId),
                                                      DiscoveryTimeout))
                    {
                        var response = client.GetAsync(string.Format(@"{0}/{1}", serviceDiscoveryUrl, ServiceId)).Result;
                        returnStatus = CheckHttpStatus(response.StatusCode);
                        if (returnStatus == ReturnStatus.OK)
                        {
                            discoveryResponse =
                                DeserializeXml<ServiceDiscoveryResponse>(response.Content.ReadAsStringAsync().Result);

                            //Check for any secure transport errors.
                            if (discoveryResponse != null)
                                returnStatus = ParseSecureTransportStatus(discoveryResponse.Status.StatusCode);

                            if (returnStatus == ReturnStatus.OK)
                            {
                                returnStatus = SetTransportStatus(false, "Discovery Complete. Starting Pings.",
                                                                  ReturnStatus.OK, "Service Discovery");
                                break;
                            }
                        }
                        else
                        {
                            SetHttpError(response.StatusCode,
                                         response.Content != null
                                             ? response.Content.ReadAsStringAsync().Result
                                             : string.Empty, returnStatus, "Service Discovery");
                        }
                    }
                }
                catch (AggregateException aggregateException)
                {
                    returnStatus = SetTransportStatus(false, aggregateException.Flatten().Message, ReturnStatus.NetworkError, "Service Discovery Error");
                }
                catch (HttpRequestException  hpx)
                {
                    //HttpClient error.
                    returnStatus = SetTransportStatus(false, hpx.InnerException.Message, ReturnStatus.NetworkError, "Service Discovery Error");
                }
                catch (Exception ex)
                {
                    returnStatus = SetTransportStatus(false, ex.Message, ReturnStatus.OtherError, "Service Discovery Error");
                }

                if (returnStatus != ReturnStatus.OK)
                {
                    //Try the secondary URL.
                    serviceDiscoveryUrl = SecondaryServiceDiscoveryUrl;
                }
            }

            //Now ping each service provider to see which one is fastest.
            if (discoveryResponse != null)
            {
                var providers = new List<SvDotProvider>();

                DateTime? expiry = null;

                if (ServiceProviderExpiry > 0)
                {
                    expiry = DateTime.Now.AddMinutes(ServiceProviderExpiry);
                }

                ReturnStatus pingStatus = ReturnStatus.OK;
                //Loop through the ServiceProviders returned from SecureTransport, and transform them to SvDotProviders.
                if (discoveryResponse.Discovery != null && discoveryResponse.Discovery.ServiceProviders != null)
                {
                    foreach (var serviceProvider in discoveryResponse.Discovery.ServiceProviders)
                    {
                        var svDotProvider = PingServiceProvider(serviceProvider.Url, out pingStatus);

                        if (pingStatus == ReturnStatus.OK && svDotProvider != null && !string.IsNullOrEmpty(svDotProvider.ServiceProviderUrl))
                        {
                            //Set the expiry.
                            svDotProvider.Expiry = expiry;
                            providers.Add(svDotProvider);
                        }
                    }
                }

                //If at least one ping was successful, and we have at least one valid service provider, set the service providers.
                //if (pingStatus == ReturnStatus.OK && providers.Count > 0)
                if (providers.Count > 0)
                {
                    SetProviders(providers);
                    //Clear out any errors that might have been set by the pings.
                    returnStatus = SetTransportStatus(false, string.Format("Success. Providers Set. Provider Count: {0}", providers.Count), ReturnStatus.OK, "Service Discovery Complete");
                }
                else
                {
                    //We'll return the error from the ping.
                    returnStatus = pingStatus;
                }
            }

            return returnStatus;
        }


        /// <summary>
        /// Checks the round-trip return time of transactions for a service provider.
        /// </summary>
        /// <param name="returnStatus">The <see cref="ReturnStatus"/> the call.  Will be <see cref="ReturnStatus.OK"/> if successful.</param>
        /// <returns>An <see cref="SvDotProvider"/> with the round-trip time of the Service Provider</returns>
        private SvDotProvider PingServiceProvider(string serviceProviderUrl, out ReturnStatus returnStatus)
        {
            returnStatus = ReturnStatus.OK;
            //            var provider = new SvDotProvider();
            SvDotProvider provider = null;
            var ping = new PingRequest
                {
                    Client = TransportRequestClient,
                    Ping = new ServicePing
                        {
                            ServiceId = ServiceId
                        },
                };


            try
            {
                using (var client = CreateHttpClient(serviceProviderUrl, PingTimeout))
                {
                    var xmlPing = SerializeXml(ping);
                    var request = new HttpRequestMessage
                        {
                            Content = new StringContent(xmlPing, Encoding.UTF8, "text/xml")
                        };
                    var startTime = DateTime.Now;
                    var response = client.PostAsync(serviceProviderUrl, request.Content).Result;
                    var endTime = DateTime.Now;

                    //Time the call to Secure Transport.  Will add this to the Service Provider transaction timing.
                    var callTime = (int) endTime.Subtract(startTime).TotalMilliseconds;

                    returnStatus = CheckHttpStatus(response.StatusCode);
                    if (returnStatus == ReturnStatus.OK)
                    {
                        var pingResponse = DeserializeXml<PingResponse>(response.Content.ReadAsStringAsync().Result);

                        if (pingResponse != null)
                        {
                            //Check for any secure transport errors.
                            returnStatus = ParseSecureTransportStatus(pingResponse.Status.StatusCode);

                            if (returnStatus == ReturnStatus.OK)
                            {
                                provider = new SvDotProvider
                                    {
                                        RoundtripTime = pingResponse.Ping.Cost.TransactionTime + callTime,
                                        ServiceProviderUrl = serviceProviderUrl
                                    };
                            }
                        }
                    }
                    else
                    {
                        SetHttpError(response.StatusCode,
                                     response.Content != null
                                         ? response.Content.ReadAsStringAsync().Result
                                         : string.Empty, returnStatus, "Service Provider Ping");
                    }
                }
            }
            catch (AggregateException aggregateException)
            {
                returnStatus = SetTransportStatus(false, aggregateException.Flatten().Message, ReturnStatus.NetworkError,
                                                  "Ping Error");
            }
            catch (HttpRequestException hpx)
            {
                //HttpClient error.
                returnStatus = SetTransportStatus(false, hpx.InnerException.Message, ReturnStatus.NetworkError,
                                                  "Ping Error");
            }
            catch (Exception ex)
            {
                returnStatus = SetTransportStatus(false, ex.Message, ReturnStatus.OtherError, "Ping Error");
            }

            return provider;
        }

        private string SerializeXml(object obj)
        {           
            string xml;

            using (var stream = new MemoryStream())
            using (var writer = XmlWriter.Create(stream))
            {
                new XmlSerializer(obj.GetType() ).Serialize(writer, obj);
                xml = Encoding.UTF8.GetString(stream.ToArray());
            }
            return xml;
        }

        private T DeserializeXml<T>(string xml)
        {
            T obj;                       
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(xml)))
            {
                obj = (T)new XmlSerializer(typeof(T)).Deserialize(ms);
            }
            return obj;
        }

        /// <summary>
        /// Sends the payload through Secure Transport to be processed by the service provider, in this case ValueLink.
        /// Sets the Response property with the transaction results.
        /// </summary>
        /// <param name="serviceProviderUrl">One of the Secure Transport URLs for the Service Provider</param>
        /// <returns>The <see cref="ReturnStatus"/> the call.  Will return <see cref="ReturnStatus.OK"/> if successful.</returns>
        private ReturnStatus SendPayload(string serviceProviderUrl)
        {
            ReturnStatus returnStatus = ReturnStatus.OK;
            var transaction = new SimpleTransactionRequest();
            if (!string.IsNullOrEmpty(Payload))
                transaction = new SimpleTransactionRequest
                {
                    Client = TransportRequestClient,
                    Transaction = new SimpleTransaction
                    {
                        ServiceId = ServiceId,
                        Payload = this.Payload.Replace(FIELD_SEPARATOR, FIELD_SEPARATOR_XML)
                    }
                };

            //var content = HttpContentExtensions.CreateXmlSerializable<SimpleTransactionRequest>(transaction,
            //                                                                       new XmlSerializer(typeof(SimpleTransactionRequest)),
            //                                                                       Encoding.UTF8, "text/xml");           
                   
            try
            {
                using (var client = CreateHttpClient(serviceProviderUrl, TransactionTimeout))
                {
                    var request = new HttpRequestMessage();
                    var transactionXml = SerializeXml(transaction);   
                    request.Content = new StringContent(transactionXml, Encoding.UTF8, "text/xml"); 
           
                    var response = client.PostAsync( serviceProviderUrl, request.Content ).Result ;
                    returnStatus = CheckHttpStatus(response.StatusCode);
                    if (returnStatus == ReturnStatus.OK)
                    {
                        if (response.Content != null)
                        {                              
                            var transResponse = DeserializeXml<SimpleTransactionResponse>(response.Content.ReadAsStringAsync().Result);                            

                            if (transResponse != null)
                            {
                                //Try to check either the Return Code from the Transaction or the Status Code from the response.

                                if (string.IsNullOrEmpty(transResponse.TransactionResponse.ReturnCode))
                                {
                                    //Check for any secure transport errors.
                                    returnStatus = ParseSecureTransportStatus(transResponse.Status.StatusCode);
                                }
                                else
                                {
                                    //Try to parse the Return Code.
                                    returnStatus = ParseTransactionReturnCode(transResponse.TransactionResponse.ReturnCode);
                                }

                                var payload = transResponse.TransactionResponse.Payload;
                                if (!string.IsNullOrEmpty(payload))
                                {
                                    //Replace the XML-encoded field separator with the actual field separator character.
                                    Response = payload.Replace(FIELD_SEPARATOR_XML, FIELD_SEPARATOR);
                                }
                            }
                        }
                    }
                    else
                    {
                        SetHttpError(response.StatusCode, response.Content != null ? response.Content.ReadAsStringAsync().Result   : string.Empty, returnStatus, "Simple Transaction");

                        //If it's a 500 error, we do not want to retry, since who knows what happened to the transaction.
                        if (returnStatus == ReturnStatus.Http500Error)
                        {
                            Retry = false;
                        }
                    }
                }
            }
            catch (HttpRequestException hpx)
            {
                //HttpClient error.
                returnStatus = SetTransportStatus(false, string.Format("{0} : Payload : {1}", hpx.InnerException.Message, Payload), ReturnStatus.NetworkError, "Simple Transaction Error");
            }
            catch (Exception ex)
            {
                returnStatus = SetTransportStatus(false, string.Format("{0} : Payload : {1}", ex.Message, Payload), ReturnStatus.OtherError, "Simple Transaction Error");
            }

            return returnStatus;
        }

        /// <summary>
        /// Creates a client with request headers set for SvDot requests.
        /// </summary>
        /// <param name="url">The uri string for the base address</param>
        /// <param name="timeout">The http timeout in seconds</param>
        /// <returns>An <see cref="HttpClient"/></returns>
        private HttpClient CreateHttpClient(string url, int timeout)
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.CacheControl = new CacheControlHeaderValue
            {
                NoCache = true
            };
            client.DefaultRequestHeaders.Add( "Connection", "Keep-Alive");
            client.DefaultRequestHeaders.Add( "Cache-Control", "no-cache");
            client.Timeout = TimeSpan.FromSeconds(timeout);

            return client;
        }

        private void SetProviders(IEnumerable<SvDotProvider> providers)
        {
            //Set the providers.
            if (ServiceDiscoverySingleton.SvDotProviders == null)
            {
                //Lock when setting the providers.
                lock (syncRoot)
                {
                    //Double check after locking, in case another thread set providers before the lock.
                    if (ServiceDiscoverySingleton.SvDotProviders == null)
                    {
                        //Order the providers by response times and set the static SvDotProviders list.
                        ServiceDiscoverySingleton.SvDotProviders = providers.OrderBy(p => p.RoundtripTime).ToList();
                        ServiceDiscoverySingleton.SvDotProviders[0].Active = true;
                        _activeProvider = ServiceDiscoverySingleton.SvDotProviders[0];
                    }
                }
            }
        }

        /// <summary>
        /// Switches to the next available provider in <see cref="ServiceDiscoverySingleton.SvDotProviders"/>
        /// or removes all service providers so a service discovery can be conducted.
        ///<param name="providerUrl">The URL of the current service provider.</param>
        /// <param name="forceServiceDiscovery">If true, will remove all Service Providers to force a service discovery on the next call.</param>
        /// </summary>
        private void FailOverProvider(string providerUrl, bool forceServiceDiscovery)
        {
            var providers = ServiceDiscoverySingleton.SvDotProviders;
            if (providers != null && providers.Find(p => p.ServiceProviderUrl == providerUrl) != null)
            {
                //Lock when setting the providers.
                lock (syncRoot)
                {
                    try
                    {
                        if (forceServiceDiscovery)
                        {
                            //Remove the Service Provider list and return.
                            ServiceDiscoverySingleton.SvDotProviders = null;
                            return;
                        }

                        //Double check after locking, in case another thread set providers before the lock.
                        providers = ServiceDiscoverySingleton.SvDotProviders;
                        if (providers != null && providers.Find(p => p.ServiceProviderUrl == providerUrl) != null)
                        {
                            //Remove the current provider.
                            providers.RemoveAt(providers.FindIndex(p => p.ServiceProviderUrl == providerUrl));

                            //Order the providers by response times and set the static SvDotProviders list.
                            ServiceDiscoverySingleton.SvDotProviders = providers.OrderBy(p => p.RoundtripTime).ToList();

                            if (providers.Count > 0)
                            {
                                ServiceDiscoverySingleton.SvDotProviders[0].Active = true;
                                _activeProvider = ServiceDiscoverySingleton.SvDotProviders[0];
                                //If we got this far, everything should be good.
                            }
                            else
                            {
                                //If this provider list is empty, then remove it.
                                ServiceDiscoverySingleton.SvDotProviders = null;
                            }
                        }
                    }
                    catch
                    {
                        ServiceDiscoverySingleton.SvDotProviders = null;
                    }
                }
            }
        }

        /// <summary>
        /// Sets the Retry flag and adds an error message to the Response.
        /// </summary>
        /// <param name="retry">Sets the Retry property</param>
        /// <param name="message">An error message to place in the Response property</param>
        /// <param name="status">The status after the error is set.  Will be returned to the caller.</param>
        /// <returns>The <see cref="ReturnStatus"/> the call.  Echoed back from the status parameter</returns>
        private ReturnStatus SetTransportStatus(bool retry, string message, ReturnStatus status, string eventName = "")
        {
            this.Retry = retry;
            this.Response = message;
            Helpers.Log(eventName, string.Format("{0} : {1}", status, message), (status != ReturnStatus.OK));
            //Echo back the status so it can be returned.
            return status;
        }

        /// <summary>
        /// Parses the Status of the Secure Transport call, checking for errors.
        /// <param name="statusText">The text value of the Status to parse</param>
        /// </summary>
        /// <returns>The <see cref="ReturnStatus"/> the call.  Will return <see cref="ReturnStatus.OK"/> if Secure Transport call was successful.</returns>
        private ReturnStatus ParseSecureTransportStatus(string statusText)
        {
            var returnStatus = ReturnStatus.OK;

            try
            {
                if (!string.IsNullOrEmpty(statusText))
                {
                    var status = (SecureTransportStatus)Enum.Parse(typeof(SecureTransportStatus), statusText, true);

                    switch (status)
                    {
                        case SecureTransportStatus.OK:
                            returnStatus = ReturnStatus.OK;
                            break;
                        case SecureTransportStatus.Timeout:
                            //We're not sure what happened here. So set the Retry status to false so we don't retry the transaction. 
                            returnStatus = SetTransportStatus(false, string.Format("Timeout or Unknown Secure Transport Error: {0} : Payload : {1}", statusText, Payload), ReturnStatus.SecureTransportError);
                            break;
                        default:
                            //Error in the transport.
                            returnStatus = SetTransportStatus(false, string.Format("Secure Transport Error: {0} : Payload : {1}", statusText, Payload), ReturnStatus.SecureTransportError);
                            break;
                    }
                }
                else
                {
                    returnStatus = SetTransportStatus(false, string.Format("Unknown Secure Transport Error: {0} : Payload : {1}", statusText, Payload), ReturnStatus.OtherError);
                }
            }
            catch (ArgumentException)
            {
                //Parse failed. It's some other unknown error.
                returnStatus = ReturnStatus.OtherError;
            }

            return returnStatus;
        }

        /// <summary>
        /// Checks the return code of the Simple Transaction for errors.
        /// </summary>
        /// <param name="returnCode">The text value of the Simple Transaction return code number to parse.
        /// <example>"000" for success</example>
        /// </param>
        /// <returns>The <see cref="ReturnStatus"/> the call.  Will return <see cref="ReturnStatus.OK"/> if Simple Transaction call was successful.</returns>
        /// <returns></returns>
        private ReturnStatus ParseTransactionReturnCode(string returnCode)
        {
            var returnStatus = ReturnStatus.OK;
            int codeNumber;
            if (int.TryParse(returnCode, out codeNumber))
            {
                try
                {
                    var code = (TransactionReturnCode)codeNumber;

                    switch (code)
                    {
                        case (TransactionReturnCode.OK):
                            returnStatus = ReturnStatus.OK;
                            break;
                        case (TransactionReturnCode.HostBusy):
                        case (TransactionReturnCode.HostUnvailable):
                        case (TransactionReturnCode.HostConnectError):
                            //Could not connect; transaction not received.  Set Retry to true.
                            returnStatus = SetTransportStatus(false, string.Format("Connection error to Service Host: {0} : Payload : {1}", code, Payload), ReturnStatus.TransactionError);
                            break;
                        default:
                            //Could not connect; transaction not received.  Set Retry to true.
                            returnStatus = SetTransportStatus(false, string.Format("Transaction error in Service Host: {0} : Payload : {1}", code, Payload), ReturnStatus.TransactionError);
                            break;
                    }
                }
                catch (InvalidCastException)
                {
                    //Cast failed.
                    returnStatus = ReturnStatus.OtherError;
                }
            }
            else
            {
                //Parse failed. It's some other unknown error.
                returnStatus = SetTransportStatus(false, string.Format("Transaction error in Service Host: {0} : Payload : {1}", returnCode, Payload), ReturnStatus.OtherError);
            }

            return returnStatus;
        }

        /// <summary>
        /// Checks the Http status for any errors.
        /// </summary>
        /// <param name="statusCode">The <see cref="HttpStatusCode"/> to parse</param>
        /// <returns>The <see cref="ReturnStatus"/> the call.  Will return <see cref="ReturnStatus.OK"/> if Http Status in the 200 range.</returns>
        private ReturnStatus CheckHttpStatus(HttpStatusCode statusCode)
        {
            var returnStatus = ReturnStatus.OK;

            //Check for 20x response.
            int statusNumber = (int)statusCode;
            if (statusNumber >= 200 && statusNumber < 300)
            {
                returnStatus = ReturnStatus.OK;
            }
            else if (statusNumber >= 500)
            {
                returnStatus = ReturnStatus.Http500Error;
            }
            else
            {
                returnStatus = ReturnStatus.HttpError;
            }

            return returnStatus;
        }

        /// <summary>
        /// Builds the Http error message to place in the Response property
        /// </summary>
        /// <param name="statusCode">The <see cref="HttpStatusCode"/> from the Http response</param>
        /// <param name="message">The message from the content body if any</param>
        /// <param name="returnStatus">The <see cref="ReturnStatus"/></param>
        private void SetHttpError(HttpStatusCode statusCode, string message, ReturnStatus returnStatus, string eventName)
        {
            string errorResponse = string.Format("HttpError: {0} - {1}", statusCode, message);
            SetTransportStatus(false, errorResponse, returnStatus, eventName);
        }


    }
}
