﻿using System;

namespace Starbucks.CardIssuer.Dal.SvDot.TransportModels
{
    internal class SvDotProvider
    {
        /// <summary>
        /// URL of this instance of the service provider
        /// </summary>
        public string ServiceProviderUrl { get; set; }

        /// <summary>
        /// Time of ping call to service provider
        /// </summary>
        public int RoundtripTime { get; set; }

        /// <summary>
        /// Indicates currently active provider
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// If Expiry has value, the expiration DateTime of this
        /// service provider instance, after which another Service Discover
        /// should be done to get new providers.
        /// </summary>
        public DateTime? Expiry { get; set; }
    }
}
