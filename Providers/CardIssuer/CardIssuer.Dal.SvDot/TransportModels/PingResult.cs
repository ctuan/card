﻿using System.Xml.Serialization;

namespace Starbucks.CardIssuer.Dal.SvDot.TransportModels
{
    public class PingResult
    {
        [XmlElement("ServiceCost")]
        public ServiceCost Cost { get; set; }
    }
}