﻿using System.Xml.Serialization;

namespace Starbucks.CardIssuer.Dal.SvDot.TransportModels
{
    /// <summary>
    /// Client ID information to send in the request
    /// </summary>
    public class RequestClient
    {
        [XmlElement("DID")]
        public string TransportId { get; set; }
        [XmlElement("App")]
        public string ApplicationId { get; set; }
        [XmlElement("Auth")]
        public string MerchantTerminalId { get; set; }
        [XmlElement("ClientRef")]
        public string TransactionId { get; set; }
    }
}
