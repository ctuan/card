﻿using System.Xml.Serialization;

namespace Starbucks.CardIssuer.Dal.SvDot.TransportModels
{
    public class ServiceCost
    {
        [XmlElement("ServiceID")]
        public string ServiceId { get; set; }
        [XmlElement("TransactionTimeMs")]
        public int TransactionTime { get; set; }
    }
}