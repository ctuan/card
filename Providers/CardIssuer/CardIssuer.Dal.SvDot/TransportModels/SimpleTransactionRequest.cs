using System.Xml.Serialization;

namespace Starbucks.CardIssuer.Dal.SvDot.TransportModels
{
    [XmlRoot("Request")]
    public class SimpleTransactionRequest
    {
        [XmlElement("ReqClientID")]
        public RequestClient Client { get; set; }

        [XmlElement("Transaction")]
        public SimpleTransaction Transaction { get; set; }

    }

    public class SimpleTransaction
    {
        [XmlElement("ServiceID")]
        public string ServiceId { get; set; }
        [XmlElement("Payload")]
        public string Payload { get; set; }
    }
}
