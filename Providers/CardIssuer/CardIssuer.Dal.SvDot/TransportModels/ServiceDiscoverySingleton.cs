﻿using System.Collections.Generic;

namespace Starbucks.CardIssuer.Dal.SvDot.TransportModels
{
    /// <summary>
    /// A singleton class to hold the service discovery providers and active provider uri.
    /// </summary>
    internal static class ServiceDiscoverySingleton
    {

        private static volatile List<SvDotProvider> _svDotProviders;
        private static object syncProviders = new object();

        /// <summary>
        /// The list of currently available service providers.
        /// </summary>
        public static List<SvDotProvider> SvDotProviders
        {
            get { return _svDotProviders; }
            set
            {
                lock (syncProviders)
                {
                    _svDotProviders = value;
                }
            }
        }

    }
}
