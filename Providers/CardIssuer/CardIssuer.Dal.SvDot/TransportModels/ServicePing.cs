﻿using System.Xml.Serialization;

namespace Starbucks.CardIssuer.Dal.SvDot.TransportModels
{

    public class ServicePing
    {
        [XmlElement("ServiceID")]
        public string ServiceId { get; set; }
    }
}