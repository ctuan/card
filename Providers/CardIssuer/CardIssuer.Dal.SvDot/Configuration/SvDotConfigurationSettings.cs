﻿using System.Configuration;

namespace Starbucks.CardIssuer.Dal.SvDot.Configuration
{
    public class SvDotConfigurationSettings : ConfigurationSection
    {     
        [ConfigurationProperty("rollbackRetryAttempts", IsRequired = false)]
        public int? RollbackAttempts 
        {
            get { return (int)this["rollbackRetryAttempts"] ; }
            set { this["rollbackRetryAttempts"] = value; }
        }

        [ConfigurationProperty("mid", IsRequired = true)]
        public string MerchantId
        {
            get { return (string) this["mid"]; }
            set { this["mid"] = value; }
        }


        [ConfigurationProperty("terminalId", IsRequired = true)]
        public string TerminalIdWtf
        {
            get { return (string)this["terminalId"]; }
            set { this["terminalId"] = value; }
        }

        [ConfigurationProperty("tid", IsRequired = true)]
        public string TerminalIdCfg
        {
            get { return (string)this["tid"]; }
            set { this["tid"] = value; }
        }

        [ConfigurationProperty("did", IsRequired = true)]
        public string DatawireId
        {
            get { return (string)this["did"]; }
            set { this["did"] = value; }
        }

        [ConfigurationProperty("applicationId", IsRequired = true)]
        public string ApplicationId
        {
            get { return (string)this["applicationId"]; }
            set { this["applicationId"] = value; }
        }

        [ConfigurationProperty("serviceId", IsRequired = true)]
        public string ServiceId
        {
            get { return (string)this["serviceId"]; }
            set { this["serviceId"] = value; }
        }

        [ConfigurationProperty("primarySdcUrl", IsRequired = true)]
        public string PrimaryServiceDiscoveryUrl
        {
            get { return (string)this["primarySdcUrl"]; }
            set { this["primarySdcUrl"] = value; }
        }

        [ConfigurationProperty("secondarySdcUrl", IsRequired = true)]
        public string SecondaryServiceDiscoveryUrl
        {
            get { return (string)this["secondarySdcUrl"]; }
            set { this["secondarySdcUrl"] = value; }
        }

        [ConfigurationProperty("discoveryTimeout", IsRequired = false)]
        public int? DiscoveryTimeout
        {
            get { return (int)this["discoveryTimeout"]; }
            set { this["discoveryTimeout"] = value; }
        }

        [ConfigurationProperty("pingTimeout", IsRequired = false)]
        public int? PingTimeout
        {
            get { return (int)this["pingTimeout"]; }
            set { this["pingTimeout"] = value; }
        }


        [ConfigurationProperty("transactionTimeout", IsRequired = false)]
        public int? TransactionTimeout
        {
            get { return (int)this["transactionTimeout"]; }
            set { this["transactionTimeout"] = value; }
        }

        [ConfigurationProperty("serviceProviderExpiry", IsRequired = true)]
        public int ServiceProviderExpiry
        {
            get { return (int)this["serviceProviderExpiry"]; }
            set { this["serviceProviderExpiry"] = value; }
        }

        [ConfigurationProperty("SvDotTransportType", IsRequired = true)]
        public string SvDotTransportType
        {
            get { return (string)this["SvDotTransportType"]; }
            set { this["SvDotTransportType"] = value; }
        }

        [ConfigurationProperty("transactionHistoryCount", IsRequired = true)]
        public int TransactionHistoryCount
        {
            get { return (int)this["transactionHistoryCount"]; }
            set { this["transactionHistoryCount"] = value; }
        }

        [ConfigurationProperty("payloadIdLength", IsRequired = false)]
        public int? PayloadIdLengthCfg
        {
            get { return (int)this["payloadIdLength"]; }
            set { this["payloadIdLength"] = value; }
        }

        public const int MAX_TRANSACTION_COUNT = 12;  //The maximum number of transactions that can be returned from history format '85'

        /// <summary>
        /// The number of transactions to return
        /// </summary>
        public  int TransactionCount
        {
            get
            {
                 return  TransactionHistoryCount > MAX_TRANSACTION_COUNT ? MAX_TRANSACTION_COUNT : TransactionHistoryCount;
            }            
        }

        /// <summary>
        /// The starting point of the transaction history. Negative indicates LIFO order.  So, -12 will return the 12 most recent transactions.
        /// </summary>
        public int TransactionStartValue
        {
            get
            {
                return -1*TransactionCount;
            }
        }

        public string TerminalId { get { return TerminalIdCfg ?? "0001"; } }

        //private static int _payloadIdLength;

        /// <summary>
        /// The length of the payload ID we are sending through the transport.  Should be 14-16 characters.
        /// </summary>
        public  int PayloadIdLength { get{return  PayloadIdLengthCfg ?? 14;  }}
       
    }
}
