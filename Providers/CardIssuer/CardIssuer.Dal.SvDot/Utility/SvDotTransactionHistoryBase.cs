﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.SvDot.Db;
using Starbucks.CardIssuer.Dal.SvDot.Models;

namespace Starbucks.CardIssuer.Dal.SvDot.Utility
{
    public abstract class SvDotTransactionHistoryBase
    {
        protected int TransactionRecordLength { get; set; }

        //The full response string from SvDot
        protected string SvDotResponse { get; set; }
        protected ICardTransactionDal CardDal { get; set; }

        //The "D0" history field, comprised of XML-escaped hex values.
        protected string _svDotHistory;

        //A collection of byte values converted from hex.
        protected List<byte> _historyBytes;

        //The entire decompressed history string.
        protected string _historyDecompressed;

        //The history string broken up into a list of individual records.
        protected List<string> _recordList;

        //The history records converted into CardTransaction objects.
        protected List<ICardTransaction> _transactionHistory;

        //Value of Plus (+) sign in transaction history: 11+32 = 43 (ASCII decimal value).
        protected const string Plus = "11";
        //Value of Minus (+) sign in transaction history: 13+32 = 45 (ASCII decimal value).
        protected const string Minus = "13";

        /// <summary>
        /// Parses out the transaction history in the SvDot response
        /// </summary>
        /// <returns>A collection of <see cref="CardTransaction"/> objects summarizing the history</returns>
        public virtual List<ICardTransaction> GetTransactionHistory()
        {
            _transactionHistory = new List<ICardTransaction>();

            //get the history.
            GetHistoryField();

            GetHistoryBytes();

            DecompressHistory();

            GetHistoryRecords();

            if (_recordList != null)
            {
                _transactionHistory = _recordList.Select(r => ToCardTransaction(r)).ToList();
            }

            return _transactionHistory;
        }



        /// <summary>
        /// Finds the history field in the response, and sets the field value.
        /// </summary>
        protected virtual void GetHistoryField()
        {
            _svDotHistory = string.Empty;
            if (!string.IsNullOrEmpty(SvDotResponse))
            {
                StringBuilder pattern = new StringBuilder(SvDotFieldCodes.FIELD_SEPARATOR);
                //Starts with a field separator
                pattern.AppendFormat(@"(?<fieldCode>{0})", SvDotFieldCodes.TRANSACTION_HISTORY_DETAIL);
                //Grouping construct for the field code.
                pattern.AppendFormat(@"(?<responseCodeValue>[^{0}]+)", (SvDotFieldCodes.FIELD_SEPARATOR));
                //Grouping construct for the value - any character except the field separator.
                pattern.Append(SvDotFieldCodes.FIELD_SEPARATOR); //Ends with a field separator.

                var match = Regex.Match(SvDotResponse, pattern.ToString());

                if (match.Success)
                {
                    _svDotHistory = match.Groups["responseCodeValue"].Value;
                }
            }
        }

        /// <summary>
        /// Converts the hex string values into the byte representation.
        /// </summary>
        protected virtual void GetHistoryBytes()
        {
            string hexEx = @"\|(?<hex>\w{2})(?<ascii>\w*)"; //@"\|(?<hex>\w{2})";
            //Find hex values encoded as |xx, where xx is the hexadecimal.
            if (!string.IsNullOrEmpty(_svDotHistory) && Regex.IsMatch(_svDotHistory, hexEx))
            {
                var matches = Regex.Matches(_svDotHistory, hexEx);

                _historyBytes = new List<byte>();

                foreach (Match match in matches)
                {
                    //Get the hex group in the match.
                    string hexString = match.Groups["hex"].Value;
                    byte byteVal;

                    //Replace the encoded hex in the results with the binary byte value.
                    byte.TryParse(hexString, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out byteVal);

                    _historyBytes.Add(byteVal);

                    //Get any standard 127 bit ASCII character matches.
                    string asciiString = match.Groups["ascii"].Value;

                    if (!string.IsNullOrEmpty(asciiString))
                    {
                        byte[] asciiBytes = Encoding.ASCII.GetBytes(asciiString);
                        _historyBytes.AddRange(asciiBytes);
                    }
                }
            }
        }

        /// <summary>
        /// Applies the decompression algorithm to the byte values to get the actual value.
        /// </summary>
        protected virtual void DecompressHistory()
        {
            var historyReader = new StringBuilder();

            if (_historyBytes != null)
            {
                foreach (byte byteVal in _historyBytes)
                {
                    //Subtract 32 from the byte value to get the actual numerical value.
                    historyReader.AppendFormat("{0:0#}", byteVal - 32);
                }
            }

            //Remove the header values so only the transaction records remain.
            _historyDecompressed = (historyReader.Length > 8)
                                       ? historyReader.ToString().Substring(8, historyReader.Length - 8)
                                       : string.Empty;
        }

        /// <summary>
        /// Parses the decompressed history field value into separate records.
        /// </summary>
        protected virtual  void GetHistoryRecords()
        {
            var recordPattern = string.Format("(?<record>\\d{{{0}}})", TransactionRecordLength);

            if (Regex.IsMatch(_historyDecompressed, recordPattern))
            {
                var matches = Regex.Matches(_historyDecompressed, recordPattern);

                _recordList = (from Match recordMatch in matches
                               select recordMatch.Groups["record"].Value).ToList();
            }
        }

        /// <summary>
        /// Parses a history record from SvDot request into a CardTransaction
        /// </summary>
        /// <param name="transactionRecord">The history record with values concatenated into a string</param>
        /// <returns>A CardTransaction with </returns>
        protected abstract ICardTransaction ToCardTransaction(string transactionRecord);



    }
}
