﻿using Starbucks.CardIssuer.Dal.SvDot.Configuration;

namespace Starbucks.CardIssuer.Dal.SvDot.Utility
{
    internal class SvDotFieldCodes
    {
        //private static readonly NameValueCollection _svDotConfiguration = (NameValueCollection)ConfigurationManager.GetSection("svDotConfiguration");

      

        public const string SV = "SV.";  //Start of every SVdot transaction. 
        public const string VERSION = "4";  //The SVdot version.
        public const string FORMAT = "0";  //Format code.
        public const string FIELD_SEPARATOR = "\x1C"; //Hex code for the field separator, using the hex escape sequence.
        public const string AUTH_CODE = "38"; //6-digit authorization code returned in transaction response.
        public const string RESPONSE_CODE = "39"; //The response code field for the transaction.
        public const string RESPONSE_SUCCESS = "00"; //Value of "success" response code.
        public const string PREVIOUS_BALANCE = "75";
        public const string NEW_BALANCE = "76";
        public const string BASE_PREVIOUS_BALANCE = "80";
        public const string BASE_NEW_BALANCE = "81";
        public const string CARD_CLASS = "B0";
        public const string BASE_CURRENCY = "BC";
        public const string LOCAL_CURRENCY = "C0";
        public const string FIRST_TRANSACTION_NUMBER = "CE"; //For transaction history, indicates which transaction should come first. 1 = FIFO, -1 = LIFO.
        public const string TRANSACTION_COUNT_REQUEST = "CF"; //Number of transactions to return in transaction history.
        public const string TRANSACTION_HISTORY_DETAIL = "D0"; //Compressed string containing history detail
        public const string PIN_TRANSACTION = "30"; //Source code value indicating the transaction uses a PIN.
        public const string PINLESS_TRANSACTION = "31"; //Source code value indicating the transaction uses a PIN.
        public const string HISTORY_FORMAT = "E8";  //The format in which to return the history records.
        public const string EXCHANGE_RATE = "E9";
        public const string SOURCE_CODE = "EA"; //Code indicating whether the transaction has PIN or does not have a PIN.
        public const string TRANSACTION_COUNT_RETURNED = "EB"; //Count of transactions in transaction history
        public const string TRANSACTION_RECORD_LENGTH = "EC"; //Length of the transaction string.
        public const string TARGET_CARD_NUMBER = "ED"; //Target card for balance merge
        public const string PROMOTION_CODE = "F2"; //Promotion code for new card activation
        public const string MERCHANT_KEY_ID = "F3"; //Merchant key ID for encryption
        public const string ORIGINAL_TRANSACTION_REQUEST = "F6"; //Request code for original transaction.
        public const string HISTORY_FORMAT_VALUE = "85";  //Default value of the history format.
		public const string LOCK_AMOUNT = "78";  //The value on the card that is locked due to balance lock transaction received.
 

    }
}
