﻿using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.Common.Resources;
using Starbucks.CardIssuer.Dal.SvDot.Common;
using Starbucks.CardIssuer.Dal.SvDot.Db;
using Starbucks.Platform.Security;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Starbucks.CardIssuer.Dal.SvDot.Utility
{
    public class SvDotRequestBuilder : ISvDotRequestBuilder
    {
        private readonly ICardTransactionDal _cardDal;

        //public SvDotRequestBuilder(ICardTransactionDal cardDal)
        //{
        //    _cardDal = cardDal;
        public Dictionary<string, string> Fields { get; set; }

        private IMerchantInfo _merchantInfo;
        
        private bool _withCurrency = true;  //Default to true, since most operations require it.
        private string _cardNumber;

        /// <summary>
        /// The unique ID of the request transaction
        /// </summary>
        public long TransactionId { get; private set; }

        /// <summary>
        /// The Request code of the First Data transaction
        /// </summary>
        public Enums.CallItCommand RequestCode { get; set; }

        /// <summary>
        /// The Merchant ID to be used in the transaction
        /// </summary>
        public string MerchantId { get { return (_merchantInfo == null) ? null : _merchantInfo.Mid; } }

        /// <summary>
        /// The ID of the terminal/register a transaction occurred on.
        /// </summary>
        public string TerminalId { get { return (_merchantInfo == null) ? null : _merchantInfo.TerminalId; } }

        /// <summary>
        /// The full merchant information for the transaction.
        /// </summary>
        public IMerchantInfo MerchantInfo { get { return _merchantInfo; } }

        /// <summary>
        /// The SVC number to use in the transaction
        /// </summary>
        public string CardNumber
        {
            get { return _cardNumber; }
            set
            {
                value = value ?? string.Empty;
                _cardNumber = Encryption.DecryptCardNumber(value.Trim());
            }
        }

        /// <summary>
        /// Add a PIN for transactions that require a PIN.
        /// </summary>
        public string Pin { get; set; }

        /// <summary>
        /// The amount of the transaction.
        /// <remarks>
        ///     Use only for transactions that require amount, such as Reload or Redeem.
        ///     Do not use for non-monetary transactions, such as Balance or History.
        /// </remarks>
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Flag indicating that currency for merchant info should be sent with the request.
        /// Optional field, but should not be sent for non-monetary transactions--Balance, History, Cashout, Disable PIN
        /// Default value is true
        /// </summary>
        public bool WithCurrency
        {
            get { return _withCurrency; }
            set { _withCurrency = value; }
        }

        public DateTime LocalTransactionTime { get; set; }

        private SvDotRequestBuilder()
        {
        }

        /// <summary>
        /// Constructor for new SvDotRequestBuilder
        /// </summary>
        /// <param name="merchantInfo">The Merchant information to use for the transaction</param>
        /// <param name="requestCode">The request command to execute</param>
        /// <param name="cardDal">The card data provider.</param>
        /// <param name="localTransactionTime">The local transaction time of the transaction.</param>
        /// <param name="localTransactionId">The ID of the transaction from the merchant terminal.</param>
        public SvDotRequestBuilder(IMerchantInfo merchantInfo, Enums.CallItCommand requestCode,
                                   ICardTransactionDal cardDal, DateTime localTransactionTime, long localTransactionId)
            : this(merchantInfo, requestCode, cardDal, localTransactionTime)
        {
            this.TransactionId = localTransactionId;
        }

        /// <summary>
        /// Constructor for new SvDotRequestBuilder
        /// </summary>
        /// <param name="merchantInfo">The Merchant information to use for the transaction</param>
        /// <param name="requestCode">The request command to execute</param>
        /// <param name="cardDal">The card data provider.</param>
        /// <param name="localTransactionTime">(optional) The local transaction time of the transaction.</param>
        public SvDotRequestBuilder(IMerchantInfo merchantInfo, Enums.CallItCommand requestCode, ICardTransactionDal cardDal, DateTime? localTransactionTime = null)
        {
            _cardDal = cardDal;
            //Initialize fields.
            Fields = new Dictionary<string, string>();

            _merchantInfo = merchantInfo;
            RequestCode = requestCode;
            LocalTransactionTime = localTransactionTime ?? DateTime.Now;

            SetCommonFields();
        }

        /// <summary>
        /// Adds or sets a field and value to the request, specific to the request type.
        /// </summary>
        /// <param name="field">The request field code to add. Numeric field codes must have leading 0s</param>
        /// <param name="value">The value of the field</param>
        public void AddField(string field, string value)
        {
            //Set the field value.
            Fields[field] = value;
        }


        /// <summary>
        /// Populates the parameter values from a previously 
        /// executed and saved request.
        /// </summary>
        /// <param name="merchantInfo">The merchant information that the transaction was conducted under.  Must match the merchant in the transaction.</param>
        /// <param name="transactionId">The transaction ID of the SvDot request</param>
        public virtual ISvDotRequestBuilder GetTransactionRequest(IMerchantInfo merchantInfo, long transactionId)
        {

            //CardDatabaseUtility cardTransactionData = new CardDatabaseUtility();

            //var request = new SvDotRequestBuilder(merchantInfo, , , , );

            //request.TransactionId = transactionId;
            

            var serviceTransaction = _cardDal.GetTransaction(transactionId);
             
            //Make sure the merchants match.
            if (merchantInfo.Mid != MerchantId)
            {
                throw new CardIssuerException(
                    CardIssuerErrorResource.MerchantIdDoesNotMatchMerchantIdOfTransactionCode,
                    CardIssuerErrorResource.MerchantIdDoesNotMatchMerchantIdOfTransactionMessage);
                //Throw an exception if they do not match.

            }


            Enums.CallItCommand callItCommand;
            if (Enum.TryParse<Enums.CallItCommand>(serviceTransaction.RequestCode, true, out callItCommand))
            {
                RequestCode = callItCommand;
            }
            else
            {
                //If parsing or casting fails, there's something wrong with the request code we retrieved.
                //Throw an exception.
                throw new CardIssuerException(CardIssuerErrorResource.InvalidRequestCodeCode, CardIssuerErrorResource.InvalidRequestCodeMessage);
            }


            //Set the altmid to the database value, since it may be different from the altMid passed in.
            merchantInfo.AlternateMid = serviceTransaction.AltMerchNo;
            _merchantInfo = merchantInfo;
            Fields = new Dictionary<string, string>();

            SetCommonFields();

            //Set the amount, but only for reload, redeem, activation.
            switch (RequestCode)
            {
                case Enums.CallItCommand.Activate:
                case Enums.CallItCommand.ActivatePhysical:
                case Enums.CallItCommand.Redeem:
                case Enums.CallItCommand.Reload:
                case Enums.CallItCommand.AutomaticReload:
                case Enums.CallItCommand.Cashout:
                    Amount = Math.Abs(serviceTransaction.Amount); //Redeem may be negative. Need to send a positive #.
                    break;
                default:
                    Amount = 0;
                    break;
            }

            CardNumber = serviceTransaction.CardNumber;
            if (RequestCode == Enums.CallItCommand.BalanceMerge && !string.IsNullOrEmpty(serviceTransaction.SourceCardNumber))
            {
                //For balance merges, the card number field is the source card.
                CardNumber = serviceTransaction.SourceCardNumber;
                //Add the target card number for the merge, which is saved as the CardNo value of the transaction.
                AddField(SvDotFieldCodes.TARGET_CARD_NUMBER, serviceTransaction.CardNumber);
            }

	        return this;
        }

        /// <summary>
        /// Gets the delimited string containing the field codes and values to
        /// send in the SvDot request
        /// </summary>
        /// <returns>The request string
        ///     <remarks>Request string must be sent without a field separator at the end. If fields must be added to the string, append a field separator to the return value.</remarks>
        /// </returns>
        public string GetRequest()
        {
            const string fieldSeparator = SvDotFieldCodes.FIELD_SEPARATOR;
            var request = new StringBuilder();

            //Build the header: SV. + MID + field separator + Version# + Format#
            request.Append(SvDotFieldCodes.SV);
            request.Append(MerchantId);
            request.Append(fieldSeparator);
            request.Append(SvDotFieldCodes.VERSION);
            request.Append(SvDotFieldCodes.FORMAT);
            request.AppendFormat("{0:0###}", (int)RequestCode);
            request.Append(fieldSeparator);

            if (!string.IsNullOrEmpty(CardNumber))
            {
                //Append the card number.
                AddField(SvDotRequestField.CardNumber.GetField(), CardNumber);
            }

            if (Amount > 0 || RequestCode == Enums.CallItCommand.Activate || RequestCode == Enums.CallItCommand.ActivatePhysical || RequestCode == Enums.CallItCommand.RedemptionUnlock)
            {
                //Add the amount in "implied decimal" format.
                AddField(SvDotRequestField.TransactionAmount.GetField(), Amount.ToImpliedDecimal());
            }

            if (WithCurrency)
            {
                //Add the currency from the Merchant information.
                AddField(SvDotFieldCodes.LOCAL_CURRENCY, _merchantInfo.CurrencyNumber);
            }

            //Set transaction to "pinless" unless a value is provided, such as for virtual activations.
            var sourceCode = Fields.GetValue(SvDotFieldCodes.SOURCE_CODE) ?? SvDotFieldCodes.PINLESS_TRANSACTION;
            //Check if this is a transaction with PIN.
            if (!string.IsNullOrEmpty(Pin))
            {
                //Encrypt the PIN before we send it.
                var encryptedPin = ValueLinkEncryption.EncryptPIN(_merchantInfo.Mwk, Pin);

                //Append the PIN and the encryption key ID.
                AddField(SvDotRequestField.Pin.GetField(), encryptedPin);
                AddField(SvDotFieldCodes.MERCHANT_KEY_ID, _merchantInfo.EncryptionId);

                sourceCode = SvDotFieldCodes.PIN_TRANSACTION;
            }

            //Add the pin/no-pin indicator.
            AddField(SvDotFieldCodes.SOURCE_CODE, sourceCode);

            //We have enough information in the request to save the request and get the transaction ID.
            //If there is an existing transaction ID, then it's likely a Void or Rollback; don't create a new card transaction record.
            if (TransactionId == 0)
            {
                SaveCardTransaction();
            }

            AddField(SvDotRequestField.TransactionId.GetField(), TransactionId.ToString());

			//if (RequestCode == Enums.CallItCommand.BalanceLock)
			//{
			//	DateTime lockTime = DateTime.Now + new TimeSpan(3, 0, 0, 0);
			//	AddField(SvDotRequestField.PostDate.GetField(), lockTime.ToString("MMddyyyy"));
			//}

            var count = 0;
            //Loop through the fields, and append each field code and value.
            foreach (var field in Fields)
            {
                count++;
                //Transaction field code and value.  Add a field separator to all but the last field.
                request.AppendFormat("{0}{1}{2}", field.Key, field.Value, (count < Fields.Count) ? fieldSeparator : string.Empty);
            }

            return request.ToString();
        }

        /// <summary>
        /// Adds fields to the request that go with each transaction
        /// MID, Terminal ID, Alt MID, Local Date, Local Time
        /// </summary>
        private void SetCommonFields()
        {
            //Automatically add fields that we send with every transaction.
            AddField(SvDotRequestField.LocalTransactionDate.GetField(), LocalTransactionTime.ToString("MMddyyyy"));
            AddField(SvDotRequestField.LocalTransactionTime.GetField(), LocalTransactionTime.ToString("HHmmss"));
            AddField(SvDotRequestField.MerchantTerminalId.GetField(), string.Format("{0}{1}", MerchantId, TerminalId));
            AddField(SvDotRequestField.AltMerchantId.GetField(), _merchantInfo.AlternateMid);
        }

        /// <summary>
        /// Saves the request and its values to the database.
        /// </summary>
        private void SaveCardTransaction()
        {
            bool isBalanceMerge = (RequestCode == Enums.CallItCommand.BalanceMerge);

            //If it's a balance merge, the target card will be recorded in the transaction.
            string cardNumber = isBalanceMerge ? Fields.GetValue(SvDotFieldCodes.TARGET_CARD_NUMBER) : CardNumber;
            string sourceCardNumber = isBalanceMerge ? CardNumber : null;
            string pin = Pin;
            //Concatenate the transaction date and time.
            string merchTime = string.Format("{0}{1}",
                                             Fields.GetValue(SvDotRequestField.LocalTransactionDate.GetField()),
                                             Fields.GetValue(SvDotRequestField.LocalTransactionTime.GetField()));

            this.TransactionId = _cardDal.GetTerminalTransactionNumber(cardNumber, sourceCardNumber, pin, this.Amount, _merchantInfo, (int)RequestCode, merchTime);

        }

    }
}