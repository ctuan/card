﻿using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.SvDot.Common;
using Starbucks.CardIssuer.Dal.SvDot.Db;
using Starbucks.CardIssuer.Dal.SvDot.Models;
using Starbucks.CardIssuer.Dal.SvDot.TransportModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace Starbucks.CardIssuer.Dal.SvDot.Utility
{
    internal class SvDotResponseBuilder
    {
        private readonly ICardTransactionDal _cardDal;

        enum ResponseStatus
        {
            Completed = 0,
            RolledBack = 1,
            Unknown = 2, //Request was sent, but response is unknown.
            Error = 3,
            Voided = 4
        }

        //Dictionary to hold the fields of the response.
        private Dictionary<string, string> _fields;
        private CardTransaction _trans;
        private decimal _exchangeRate;
        private Int16 _localCurrency;
        private Int16 _baseCurrency;
        private readonly DateTime _localTransactionTime;

        /// <summary>
        /// The response fields from the SvDot call
        /// </summary>
        public string SvDotResponse { get; private set; }

        /// <summary>
        /// The response code for the SvDot call
        /// 00 means success
        /// </summary>
        public string ResponseCode { get; private set; }

        /// <summary>
        /// Constructor that sets the SvDotResponse property
        /// </summary>
        /// <param name="svDotResponse">The sv dot response.</param>
        /// <param name="cardDal">The card dal.</param>
        /// <param name="localTransactionTime">The local transaction time.</param>
        public SvDotResponseBuilder(string svDotResponse, ICardTransactionDal cardDal, DateTime localTransactionTime)
        {
            _cardDal = cardDal;
            SvDotResponse = svDotResponse;
            this._localTransactionTime = localTransactionTime;

            //Set the response code right away, so we can verify success.
            SetResponseCode();

        }

        /// <summary>
        /// Parses the fields of the SvDotResponse into a <see cref="CardTransaction"/> object.
        /// without saving the transaction response.
        /// </summary>
        /// <returns>A <see cref="CardTransaction"/> describing the SvDot call response</returns>
        public CardTransaction GetCardTransaction()
        {
            //Populate the dictionary object with the name/value pairs from the response.
            GetResponseFields();

            //TODO: CACHE THE CURRENCY TABLE.
            //Open the transaction database to look up the currency.           

            //Get the numeric currency values from the dictionary..
            _baseCurrency = _fields.GetValueAndConvert<Int16>(SvDotFieldCodes.BASE_CURRENCY, Convert.ToInt16);
            _localCurrency = _fields.GetValueAndConvert<Int16>(SvDotFieldCodes.LOCAL_CURRENCY, Convert.ToInt16);

            //Get the 3-letter alpha currency code to send back in the transaction.
            string alphaCurrency = string.Empty;
            if (_baseCurrency != 0)
            {
                alphaCurrency = _cardDal.GetIsoAlphaCurrency(_baseCurrency.ToString());
            }
            string alphaCurrencyLocal = _cardDal.GetIsoAlphaCurrency(_localCurrency.ToString());

            //Set the currency; default to local currency if it's null.
            if (string.IsNullOrEmpty(alphaCurrency))
            {
                alphaCurrency = alphaCurrencyLocal;
            }

            this._trans = new CardTransaction
            {
                AuthorizationCode = _fields.GetValue(SvDotFieldCodes.AUTH_CODE),
                BeginingBalanceInBaseCurrency = _fields.GetValueAndConvert<decimal>(SvDotFieldCodes.BASE_PREVIOUS_BALANCE, Helpers.ParseImpliedDecimal),
                BeginningBalance = _fields.GetValueAndConvert<decimal>(SvDotFieldCodes.PREVIOUS_BALANCE, Helpers.ParseImpliedDecimal),
                EndingBalance = _fields.GetValueAndConvert<decimal>(SvDotFieldCodes.NEW_BALANCE, Helpers.ParseImpliedDecimal),
                EndingBalanceInBaseCurrency = _fields.GetValueAndConvert<decimal>(SvDotFieldCodes.BASE_NEW_BALANCE, Helpers.ParseImpliedDecimal),
                CardClass = _fields.GetValue(SvDotFieldCodes.CARD_CLASS),
                RequestCode = _fields.GetValue(SvDotFieldCodes.ORIGINAL_TRANSACTION_REQUEST),
                ResponseCode = _fields.GetValue(SvDotFieldCodes.RESPONSE_CODE),
                TransactionId = _fields.GetValue(SvDotRequestField.TransactionId.GetField()),
                CardNumber = _fields.GetValue(SvDotRequestField.CardNumber.GetField()),
                Pin = _fields.GetValue(SvDotRequestField.Pin.GetField()),
                Currency = alphaCurrencyLocal,
                BaseCurrency = alphaCurrency,
                MerchantIdAndTerminalId = _fields.GetValue(SvDotRequestField.MerchantTerminalId.GetField()),
                StoreId = _fields.GetValue(SvDotRequestField.AltMerchantId.GetField()),
                UtcDate = DateTime.UtcNow,
				LockAmount = _fields.GetValueAndConvert<decimal>(SvDotFieldCodes.LOCK_AMOUNT, Helpers.ParseImpliedDecimal),
            };

            _trans.TransactionDate = _localTransactionTime;

            //Set the amounts, if they're not already set.
            if (_trans.Amount == default(decimal))
            {
                _trans.Amount = Math.Abs(_trans.EndingBalance - _trans.BeginningBalance);
            }
            if (_trans.AmountInBaseCurrency == default(decimal))
            {
                _trans.AmountInBaseCurrency = Math.Abs(_trans.EndingBalanceInBaseCurrency - _trans.BeginingBalanceInBaseCurrency);
            }

            //Get the exchange rate.
            _exchangeRate = _fields.GetValueAndConvert(SvDotFieldCodes.EXCHANGE_RATE, Helpers.ParseImpliedDecimal);

            return _trans;
        }
        
        /// <summary>
        /// Saves the card transaction.
        /// </summary>
        /// <returns>A <see cref="CardTransaction"/> describing the SvDot call response</returns>
        public CardTransaction SaveResponse()
        {
            if (_trans == null)
            {
                GetCardTransaction();
            }

            var status = ResponseStatus.Completed;
            string voidRequestCode = null;
            string voidResponseCode = null;
            long transactionId;
            long.TryParse(_trans.TransactionId, out transactionId);

            int requestNum;
            int.TryParse(_trans.RequestCode, out requestNum);      
            //Check if this was a rollback.
            if (requestNum == 704)
            {
                status = ResponseStatus.RolledBack; //Rolled back.
            }
            //Check if it's one of the "Void" variants.
            else if (requestNum >= 2800)
            {
                status = ResponseStatus.Voided;
            }

            //Check for errors in the response.
            switch (_trans.ResponseCode)
            {
                case "00": //success
                case "33": //for rollbacks - already rolled back.
                case "34": //for rollbacks - transaction does not exist (never made it)
                case "38": //for history - card has no history.
                    //we're good.
                    break;
                default:
                    //Anything else is a VL error.
                    status = ResponseStatus.Error;
                    break;
            }

            //If this is a void or rollback, we want to pass the request and response codes as void/rollback parameters, so we don't overwrite the original transaction.
            if (status == ResponseStatus.RolledBack || status == ResponseStatus.Voided)
            {
                voidRequestCode = _trans.RequestCode;
                voidResponseCode = _trans.ResponseCode;
            }

            // CardTransactionsDataUtility cardTransactionData = new CardTransactionsDataUtility();

            _cardDal.SaveReturnValues(transactionId, _trans.AuthorizationCode, _trans.Amount, _trans.EndingBalance, _trans.ResponseCode, (Int16)status, _exchangeRate, _baseCurrency, _trans.BeginingBalanceInBaseCurrency, _trans.EndingBalanceInBaseCurrency, _localCurrency, _trans.CardNumber, voidRequestCode, voidResponseCode);

            return _trans;
        }

        /// <summary>
        /// Finds the response code in the response string and sets the property.
        /// </summary>
        private void SetResponseCode()
        {
            if (!string.IsNullOrEmpty(this.SvDotResponse))
            {
                //Build the pattern to find the response code: [FIELD SEPARATOR](RESPONSE CODE FIELD)(RESPONSE CODE VALUE)
                var pattern = new StringBuilder(SvDotFieldCodes.FIELD_SEPARATOR);
                pattern.AppendFormat("(?<fieldCode>{0})", SvDotFieldCodes.RESPONSE_CODE); //response code field.
                pattern.Append(@"(?<responseCodeValue>\d{2})");  //two-digit response code.
                var match = Regex.Match(this.SvDotResponse, pattern.ToString());

                if (match.Success)
                {
                    this.ResponseCode = match.Groups["responseCodeValue"].Value;
                }
            }
        }

        /// <summary>
        /// Parses the field/value pairs in the response and adds them to the fields dictionary.
        /// </summary>
        private void GetResponseFields()
        {
            if (!string.IsNullOrEmpty(this.SvDotResponse))
            {
                _fields = new Dictionary<string, string>();

                StringBuilder pattern = new StringBuilder(SvDotFieldCodes.FIELD_SEPARATOR);
                pattern.Append(@"(?<fieldCode>\w{2})"); //Two digit/character field code.
                pattern.Append(@"(?<responseCodeValue>\w+)"); //...followed by field value.

                var matches = Regex.Matches(this.SvDotResponse, pattern.ToString());

                //Loop through the Match collection of all the regex matches, and add the field code and value to the dictionary.
                foreach (Match fieldMatch in matches)
                {
                    _fields.Add(fieldMatch.Groups["fieldCode"].Value,
                        fieldMatch.Groups["responseCodeValue"].Value);
                }
            }
        }

        /// <summary>
        /// Parses out the transaction history in the SvDot response
        /// </summary>
        /// <returns>A collection of <see cref="CardTransaction"/> objects summarizing the history</returns>
        public List<ICardTransaction> GetTransactionHistory(Enums.CallItCommand callItCommand)
        {
            
            var history = SvDotTransactionHistoryFactory.GetSvDotTransactionHistory(callItCommand, SvDotResponse, _cardDal);

            return history.GetTransactionHistory();
        }
    }
}