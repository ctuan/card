﻿using System;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.SvDot.Common;
using Starbucks.CardIssuer.Dal.SvDot.Db;
using Starbucks.CardIssuer.Dal.SvDot.Models;
using Starbucks.CardIssuer.Dal.SvDot.TransportModels;

namespace Starbucks.CardIssuer.Dal.SvDot.Utility
{
    public class SvDotTransactionHistoryCondensed : SvDotTransactionHistoryBase
    {
        private static readonly TimeZoneInfo EstSystemTimeZoneById =
            TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");

        public SvDotTransactionHistoryCondensed(string response, ICardTransactionDal cardDal)
        {
            SvDotResponse = response;
            CardDal = cardDal;
            TransactionRecordLength = 44;
        }

        protected override ICardTransaction ToCardTransaction(string transactionRecord)
        {
            var fieldPatternGroups = new StringBuilder(); //"(?<field>\\d{length})";
            fieldPatternGroups.Append("(?<mid>\\d{12})");
            fieldPatternGroups.Append("(?<requestCode>\\d{4})");
            fieldPatternGroups.Append("(?<localCurrency>\\d{4})");
            fieldPatternGroups.Append("(?<localAmount>\\d{12})");
            fieldPatternGroups.Append("(?<transactionDate>\\d{12})");

            var transaction = new CardTransaction();

            var fieldMatch = Regex.Match(transactionRecord, fieldPatternGroups.ToString());

            if (fieldMatch.Success)
            {
                //Assign values to the card transaction.

                transaction.Amount = Helpers.ParseImpliedDecimal(fieldMatch.Groups["localAmount"].Value);

                transaction.RequestCode = fieldMatch.Groups["requestCode"].Value;

                string description;
                int requestCode;
                if (int.TryParse(transaction.RequestCode, out requestCode))
                {
                    try
                    {
                        description = ((Enums.CallItCommand)requestCode).ToString().ParsePascal();
                    }
                    catch (InvalidCastException)
                    {
                        description = string.Empty;
                    }
                    transaction.Description = description;
                }

                DateTime transactionDate;

                if (DateTime.TryParseExact(fieldMatch.Groups["transactionDate"].Value,
                                           "MMddyyyyHHmm",
                                           CultureInfo.InvariantCulture,
                                           DateTimeStyles.None,
                                           out transactionDate))
                {
                    transaction.TransactionDate = transactionDate;
                    //FD sends an eastern time zone value, get the UTC value

                    transaction.UtcDate =
                        TimeZoneInfo.ConvertTimeToUtc(transactionDate,
                                                      EstSystemTimeZoneById)
                                    .ToUniversalTime();
                }
                //CardTransactionsDataUtility cardTransactionData = new CardTransactionsDataUtility();
                //Get the local currency.  History does not return Base Currency.
                transaction.Currency = CardDal.GetIsoAlphaCurrency(fieldMatch.Groups["localCurrency"].Value);

            }
            return transaction;
        }
    }
}