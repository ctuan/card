﻿using System;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.SvDot.Common;
using Starbucks.CardIssuer.Dal.SvDot.Db;
using Starbucks.CardIssuer.Dal.SvDot.Models;
using Starbucks.CardIssuer.Dal.SvDot.TransportModels;

namespace Starbucks.CardIssuer.Dal.SvDot.Utility
{
    internal class SvDotTransactionHistory : SvDotTransactionHistoryBase 
    {
        

        public SvDotTransactionHistory(string response, ICardTransactionDal  cardDal)
        {
            SvDotResponse = response;
            CardDal = cardDal;
            TransactionRecordLength = 82;
        }
               
        /// <summary>
        /// Parses a history record from SvDot request into a CardTransaction
        /// </summary>
        /// <param name="transactionRecord">The history record with values concatenated into a string</param>
        /// <returns>A CardTransaction with </returns>
        protected override ICardTransaction ToCardTransaction(string transactionRecord)
        {
            var fieldPatternGroups = new StringBuilder(); //"(?<field>\\d{length})";
            fieldPatternGroups.Append("(?<mid>\\d{12})");
            fieldPatternGroups.Append("(?<requestCode>\\d{4})");
            fieldPatternGroups.Append("(?<sign>\\d{2})");
            fieldPatternGroups.Append("(?<localCurrency>\\d{4})");
            fieldPatternGroups.Append("(?<localAmount>\\d{12})");
            fieldPatternGroups.Append("(?<baseAmount>\\d{12})");
            fieldPatternGroups.Append("(?<baseBalance>\\d{12})");
            fieldPatternGroups.Append("(?<lockAmount>\\d{12})");
            fieldPatternGroups.Append("(?<transactionDate>\\d{12})");

            var transaction = new CardTransaction();

            var fieldMatch = Regex.Match(transactionRecord, fieldPatternGroups.ToString());

            if (fieldMatch.Success)
            {
                //Assign values to the card transaction.

                transaction.Amount = Helpers.ParseImpliedDecimal(fieldMatch.Groups["localAmount"].Value);
                transaction.AmountInBaseCurrency = Helpers.ParseImpliedDecimal(fieldMatch.Groups["baseAmount"].Value);
                transaction.EndingBalanceInBaseCurrency =
                    Helpers.ParseImpliedDecimal(fieldMatch.Groups["baseBalance"].Value);
                transaction.EndingBalance = transaction.EndingBalanceInBaseCurrency;
                    //History from VL only contains balance in base currency.
                transaction.RequestCode = fieldMatch.Groups["requestCode"].Value;

                //Check the sign value of the amount.
                int sign = fieldMatch.Groups["sign"].Value == Plus ? 1 : -1;

                //Set the amount to positive/negative.
                transaction.Amount *= sign;

                string description;
                int requestCode;
                if (int.TryParse(transaction.RequestCode, out requestCode))
                {
                    try
                    {
                        description = ((Enums.CallItCommand) requestCode).ToString().ParsePascal();
                    }
                    catch (InvalidCastException)
                    {
                        description = string.Empty;
                    }
                    transaction.Description = description;
                }

                DateTime transactionDate;

                if (DateTime.TryParseExact(fieldMatch.Groups["transactionDate"].Value,
                                           "MMddyyyyHHmm",
                                           CultureInfo.InvariantCulture,
                                           DateTimeStyles.None,
                                           out transactionDate))
                {
                    transaction.TransactionDate = transactionDate;
                }

                //CardTransactionsDataUtility cardTransactionData = new CardTransactionsDataUtility();
                //Get the local currency.  History does not return Base Currency.
                transaction.Currency = CardDal.GetIsoAlphaCurrency(fieldMatch.Groups["localCurrency"].Value);

            }

            return transaction;
        }
    }
}
