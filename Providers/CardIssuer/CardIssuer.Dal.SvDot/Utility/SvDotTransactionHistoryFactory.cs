﻿using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.SvDot.Db;

namespace Starbucks.CardIssuer.Dal.SvDot.Utility
{
    public class SvDotTransactionHistoryFactory
    {
        public static SvDotTransactionHistoryBase  GetSvDotTransactionHistory(Enums.CallItCommand callItCommand, string response, ICardTransactionDal cardTransactionDal )
        {
            switch (callItCommand)
            {
                case Enums.CallItCommand.History :
                    return new SvDotTransactionHistory(response, cardTransactionDal);                    
                case Enums.CallItCommand.HistoryCondensed  :
                    return new SvDotTransactionHistoryCondensed(response, cardTransactionDal);
                default :
                    return new SvDotTransactionHistory(response, cardTransactionDal); 
            }
        }
    }
}
