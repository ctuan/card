﻿using System;
using System.Collections.Concurrent;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.SvDot.Common;

namespace Starbucks.CardIssuer.Dal.SvDot.Db
{
    public class CachedCardTransactionDal : ICardTransactionDal
    {
        private static readonly ConcurrentDictionary<string, string> IsoAlphyCurrencyByCurrencyNumber =
            new ConcurrentDictionary<string, string>();

        private static readonly ConcurrentDictionary<string, IMerchantInfo> MerchantInfoByFilterExpression =
            new ConcurrentDictionary<string, IMerchantInfo>();

        private static readonly ConcurrentDictionary<string, IMerchantInfo> MerchantInfoByMerchantId =
            new ConcurrentDictionary<string, IMerchantInfo>();

        private static readonly ConcurrentDictionary<string, string> AltMerchNoBySubmarket =
            new ConcurrentDictionary<string, string>();

        private readonly ICardTransactionDal _cardTransactionDal;

        public CachedCardTransactionDal(ICardTransactionDal cardTransactionDal)
        {
            _cardTransactionDal = cardTransactionDal;
        }

        public long GetTerminalTransactionNumber(string cardNumber, string transferFromCardNumber, string pin, decimal amount,
            IMerchantInfo merchantInfo, int requestCode, string merchTime)
        {
            return _cardTransactionDal.GetTerminalTransactionNumber(cardNumber, transferFromCardNumber, pin, amount,
                merchantInfo, requestCode, merchTime);
        }

        public IServiceTransaction GetTransaction(long transactionId)
        {
            return _cardTransactionDal.GetTransaction(transactionId);
        }

        public string GetIsoAlphaCurrency(string isoCurrencyNumber)
        {
            return IsoAlphyCurrencyByCurrencyNumber.GetOrAdd(isoCurrencyNumber, arg => _cardTransactionDal.GetIsoAlphaCurrency(isoCurrencyNumber));
        }

        public void SaveReturnValues(long transactionId, string authCode, decimal amount, decimal balance, string responseCode,
            int status, decimal? exchangeRate, int? baseCurrency, decimal basePreviousBalance,
            decimal baseNewBalance, int localCurrency, string cardNumber, string voidRollbackCode,
            string voidRollbackResponse)
        {
            _cardTransactionDal.SaveReturnValues(transactionId, authCode, amount, balance, responseCode, status,
                exchangeRate, baseCurrency, basePreviousBalance, baseNewBalance,
                localCurrency, cardNumber, voidRollbackCode, voidRollbackResponse);
        }

        public void SetVirtualActivationValues(long transactionId, string cardNumber, string pin)
        {
            _cardTransactionDal.SetVirtualActivationValues(transactionId, cardNumber, pin);
        }

        public IMerchantInfo GetMerchantInfo(string filterExpression, string platform)
        {
            platform = string.IsNullOrEmpty(platform) ? "unknown" : platform;
            string key = string.Format("{0}|{1}", filterExpression, platform);
            return MerchantInfoByFilterExpression.GetOrAdd(key, arg => _cardTransactionDal.GetMerchantInfo(
                filterExpression, platform));
        }

        public IMerchantInfo GetMerchantInfoByMerchantId(string merchantId, string platform)
        {
            platform = string.IsNullOrEmpty(platform) ? "unknown" : platform;
            string key = string.Format("{0}|{1}", merchantId, platform);
            return MerchantInfoByMerchantId.GetOrAdd(key, arg => _cardTransactionDal.GetMerchantInfoByMerchantId(merchantId, platform));
        }

        public IMerchantInfo GetMerchantInfoByCurrency(string currency)
        {
            return _cardTransactionDal.GetMerchantInfoByCurrency(currency);
        }

        public IOrderAddressInfo GetOrderGroupAddress(string orderId)
        {
            return _cardTransactionDal.GetOrderGroupAddress(orderId);
        }

        public string GetAltMerchNo(string platform)
        {
            return AltMerchNoBySubmarket.GetOrAdd(platform, arg => _cardTransactionDal.GetAltMerchNo(platform));
        }

        public long UpsertPartialTransferTransactionDetails(long cardAmountTransactionId, string toCardTransactionId,
            DateTime toCardTransactionDate, string fromCardTransactionId,
            DateTime? fromCardTransactionDate)
        {
            return _cardTransactionDal.UpsertPartialTransferTransactionDetails(cardAmountTransactionId,
                toCardTransactionId,
                toCardTransactionDate,
                fromCardTransactionId,
                fromCardTransactionDate);
        }
    }
}