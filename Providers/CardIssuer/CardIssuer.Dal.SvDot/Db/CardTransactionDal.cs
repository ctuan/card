﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.Common.Resources;
using Starbucks.CardIssuer.Dal.Common.Rule;
using Starbucks.CardIssuer.Dal.SvDot.Common;
using Starbucks.CardIssuer.Dal.SvDot.Models;
using Starbucks.Platform.Security;

namespace Starbucks.CardIssuer.Dal.SvDot.Db
{
    public class CardTransactionDal : ICardTransactionDal
    {
        private readonly ICardLogger _cardLogger;

        private static readonly NameValueCollection SubMarketAltIdMappings =
            (NameValueCollection)ConfigurationManager.GetSection("subMarketAltIDMap");

        private readonly string _connectionString;
        private readonly string _commerce_connectionString = null;
        private readonly string UnKnownPlatform = "unknown";

        public CardTransactionDal(string databaseName, string commerce_databaseName) : this(databaseName, commerce_databaseName, null) { }
        public CardTransactionDal(string databaseName, string commerce_databaseName, ICardLogger cardLogger)
        {
            // Note: Intentionally keeping cardLogger optional
            _cardLogger = cardLogger;

            _connectionString = ConfigurationManager.ConnectionStrings[databaseName].ConnectionString;
            if (commerce_databaseName != null)
                _commerce_connectionString = ConfigurationManager.ConnectionStrings[commerce_databaseName].ConnectionString;
        }

        /// <summary>
        ///     Gets the ISO alpha currency code based on the ISO number
        /// </summary>
        /// <param name="isoCurrencyNumber">The 3-digit ISO currency number, e.g., 840 (USD)</param>
        /// <returns>The 3-letter ISO currency code, e.g., "USD"</returns>
        public string GetIsoAlphaCurrency(string isoCurrencyNumber)
        {
            int isoNo;
            int.TryParse(isoCurrencyNumber, out isoNo);
            using (var connection = new SqlConnection(_connectionString))
            using (var command = new SqlCommand("SELECT dbo.GetCurrencyIsoCodeByNumber(@IsoNo)", connection))
            {
                command.CommandType = CommandType.Text;
                command.Parameters.AddWithValue("@IsoNo", isoNo);
                connection.Open();

                return command.ExecuteScalar().FromDBNullable<string>();
            }
        }


        /// <summary>
        ///     Saves the card transaction result from ValueLink back to the svc_transactions table
        /// </summary>
        /// <param name="transactionId">The terminal transaction ID matching the original transaction request</param>
        /// <param name="authCode">The unique autohrization code from ValueLink</param>
        /// <param name="amount">The amount of the transaction, or 0 for balance and history checks.</param>
        /// <param name="balance">The ending balance after the transaction</param>
        /// <param name="responseCode">The response code from ValueLink, '00' for success, or an error code</param>
        /// <param name="status">The status code</param>
        /// <param name="exchangeRate">The exchange rate</param>
        /// <param name="baseCurrency">The base currency of the card</param>
        /// <param name="basePreviousBalance">The previous balance in the base currency</param>
        /// <param name="baseNewBalance">The new balance in the base currency</param>
        /// <param name="localCurrency">The local currency passed to VL (based on merchant ID)</param>
        /// <param name="cardNumber">The card number</param>
        /// <param name="voidRollbackCode">The request code for void or rollback</param>
        /// <param name="voidRollbackResponse">The response code for a void or rollback</param>
        public void SaveReturnValues(long transactionId, string authCode, decimal amount, decimal balance,
                                     string responseCode, int status, decimal? exchangeRate, int? baseCurrency,
                                     decimal basePreviousBalance, decimal baseNewBalance, int localCurrency,
                                     string cardNumber, string voidRollbackCode, string voidRollbackResponse)
        {
            QueueUserWorkItem(new
            {
                transactionId,
                authCode,
                amount,
                balance,
                responseCode,
                status,
                exchangeRate,
                baseCurrency,
                basePreviousBalance,
                baseNewBalance,
                localCurrency,
                cardNumber,
                voidRollbackCode,
                voidRollbackResponse
            },
                              o =>
                              {
                                  try
                                  {
                                      using (var connection = new SqlConnection(_connectionString))
                                      using (var command = new SqlCommand("svc_SetReturnValues", connection))
                                      {
                                          command.CommandType = CommandType.StoredProcedure;

                                          command.Parameters.AddWithValue("@TransactionID", o.transactionId);
                                          command.Parameters.AddWithValue("@AuthCode", o.authCode ?? (object)DBNull.Value);
                                          command.Parameters.AddWithValue("@Amount", o.amount);
                                          command.Parameters.AddWithValue("@Balance", o.balance);
                                          command.Parameters.AddWithValue("@ResponseCode", o.responseCode ?? (object)DBNull.Value);
                                          command.Parameters.AddWithValue("@Status", o.status);
                                          command.Parameters.AddWithValue("@ExchRate", o.exchangeRate == 0 ? (object)DBNull.Value : o.exchangeRate);
                                          command.Parameters.AddWithValue("@BaseCurr", o.baseCurrency == 0 ? (object)DBNull.Value : o.baseCurrency);
                                          command.Parameters.AddWithValue("@BasePrevBal", o.basePreviousBalance);
                                          command.Parameters.AddWithValue("@BaseNewBal", o.baseNewBalance);
                                          command.Parameters.AddWithValue("@LocalCurr", o.localCurrency);
                                          command.Parameters.AddWithValue("@ReturnedCardNo", Encryption.EncryptCardNumber(o.cardNumber));
                                          command.Parameters.AddWithValue("@VoidRollbackCode", o.voidRollbackCode ?? (object)DBNull.Value);
                                          command.Parameters.AddWithValue("@VoidRollbackResponse", o.voidRollbackResponse ?? (object)DBNull.Value);

                                          connection.Open();
                                          command.ExecuteNonQuery();
                                      }
                                  }
                                  catch (Exception exception)
                                  {
                                      var cardLog = new CardLog
                                      {
                                          CardNumber = Encryption.EncryptCardNumber(o.cardNumber),
                                          TransactionId = o.transactionId.ToString(CultureInfo.InvariantCulture)
                                      };
                                      Log(cardLog, exception.Message, TraceEventType.Error);
                                  }
                              });
        }

        public void SetVirtualActivationValues(long transactionId, string cardNumber, string pin)
        {
            QueueUserWorkItem(new
            {
                transactionId,
                cardNumber,
                pin,
            },
                              o =>
                              {
                                  try
                                  {
                                      using (var connection = new SqlConnection(_connectionString))
                                      using (var command = new SqlCommand("svc_SetVirtualActivationValues", connection))
                                      {
                                          command.CommandType = CommandType.StoredProcedure;
                                          //add parameters
                                          command.Parameters.AddWithValue("@TransactionID", o.transactionId);
                                          command.Parameters.AddWithValue("@CardNo", o.cardNumber ?? (object)DBNull.Value);
                                          command.Parameters.AddWithValue("@PIN", o.pin ?? (object)DBNull.Value);

                                          connection.Open();
                                          command.ExecuteNonQuery();
                                      }
                                  }
                                  catch (Exception exception)
                                  {
                                      var cardLog = new CardLog
                                      {
                                          CardNumber = Encryption.EncryptCardNumber(o.cardNumber),
                                          TransactionId = o.transactionId.ToString(CultureInfo.InvariantCulture)
                                      };
                                      Log(cardLog, exception.Message, TraceEventType.Error);
                                  }
                              });
        }

        /// <summary>
        ///     Saves the transaction to the card_transactions database and returns the terminal transaction number to uniquely identify the card transaction against ValueLink
        /// </summary>
        /// <param name="cardNumber">encrypted card number</param>
        /// <param name="transferFromCardNumber">For Balance Transfers, the source card number</param>
        /// <param name="PIN">encrypted PIN</param>
        /// <param name="amount">The amount of the transaction</param>
        /// <param name="merchantInfo">The merchant information, such as merchant ID</param>
        /// <param name="terminalID">terminal id - get this from GetMerchantInfo in this class</param>
        /// <param name="requestCode">VL Request code for the operation to complete; these are four digit codes</param>
        /// <param name="merchTime">The date/time stamp of the transaction in ddMMyyyyHHmmss format</param>
        /// <returns>generated terminal transaction number</returns>
        public long GetTerminalTransactionNumber(string cardNumber, string transferFromCardNumber, string PIN,
                                                decimal amount, IMerchantInfo merchantInfo, int requestCode,
                                                string merchTime)
        {
            long terminalTransactionNumber = -1;

            using (var connection = new SqlConnection(_connectionString))
            using (var command = new SqlCommand("svc_createTransaction", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                //add parameters
                command.Parameters.AddWithValue("@MerchantID", merchantInfo.Mid);
                command.Parameters.AddWithValue("@AltMerchNo", Convert.ToInt32(merchantInfo.AlternateMid));
                command.Parameters.AddWithValue("@TerminalID", Convert.ToInt32(merchantInfo.TerminalId));
                command.Parameters.AddWithValue("@RequestCode", requestCode.ToString());
                command.Parameters.AddWithValue("@CardNo", Encryption.EncryptCardNumber(cardNumber));
                command.Parameters.AddWithValue("@PIN", string.IsNullOrEmpty(PIN) ? string.Empty : Encryption.EncryptPin(PIN));
                command.Parameters.AddWithValue("@Amount", amount);
                command.Parameters.AddWithValue("@MerchTime", merchTime ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("@TransferFromCardNo", Encryption.EncryptCardNumber(transferFromCardNumber));

                connection.Open();
                terminalTransactionNumber = Convert.ToInt64(command.ExecuteScalar());
            }
            return terminalTransactionNumber;
        }

        /// <summary>
        ///     Gets a starbucks card transaction
        /// </summary>
        /// <param name="transactionId">The terminal transaction ID matching the original transaction request</param>
        /// <returns>
        ///     An <c>IDataReader</c> containing the transaction data.  Calling method is responsible for closing the reader.
        /// </returns>
        public IServiceTransaction GetTransaction(long transactionId)
        {
            var a = GetOrderGroupAddress("D1B80M50V2394L7TQ6GFAQAQ31");

            using (var connection = new SqlConnection(_connectionString))
            using (var command = new SqlCommand("GetTransaction", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@TransactionID", transactionId);
                connection.Open();

                IServiceTransaction serviceTransaction;
                using (IDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        serviceTransaction = LoadServiceTransaction(reader);
                    }
                    else
                    {
                        throw new CardIssuerException(CardIssuerErrorResource.InvalidTransactionIdCode,
                                                      CardIssuerErrorResource.InvalidTransactionIdMessage);
                        //throw new ValueLinkException(string.Format("Transaction ID {0} not found", transactionId));
                    }
                }
                return serviceTransaction;
            }
        }

        public IOrderAddressInfo GetOrderGroupAddress(string orderId)
        {
            using (var connection = new SqlConnection(_commerce_connectionString))
            using (var command = new SqlCommand("GetOrderAddressInformation", connection))
            using (var adapter = new SqlDataAdapter(command))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@order_id", orderId);
                connection.Open();

                DataSet ds = new DataSet();
                adapter.Fill(ds);

                string filterExpression = string.Format("address_type=2");
                //this will be stored in cache later
                DataTable dt = ds.Tables[0];
                DataRow[] dr = dt.Select(filterExpression);

                return dr.Length > 0 ? LoadOrderAddresssInfo(dr[0]) : null;
            }
        }


        public IMerchantInfo GetMerchantInfoByCurrency(string currency)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var command = new SqlCommand("Card_GetURLMapping", connection))
            using (var adapter = new SqlDataAdapter(command))
            {
                command.CommandType = CommandType.StoredProcedure;
                connection.Open();

                DataSet ds = new DataSet();
                adapter.Fill(ds);

                string filterExpression = string.Format("Currency='{0}'", currency);
                //this will be stored in cache later
                DataTable dt = ds.Tables[0];
                DataRow[] dr = dt.Select(filterExpression);

                return dr.Length > 0 ? LoadMerchantInfo(dr[0], true, UnKnownPlatform) : null;
            }
        }

        public IMerchantInfo GetMerchantInfoByMerchantId(string merchantId, string platform)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var command = new SqlCommand("Card_GetURLMapping_Submarket", connection))
            using (var adapter = new SqlDataAdapter(command))
            {
                command.CommandType = CommandType.StoredProcedure;
                connection.Open();

                DataSet ds = new DataSet();
                adapter.Fill(ds);

                string filterExpression = string.Format("MerchantId='{0}'", merchantId);
                //this will be stored in cache later
                DataTable dt = ds.Tables[0];
                DataRow[] dr = dt.Select(filterExpression);

                return dr.Length > 0 ? LoadMerchantInfo(dr[0], true, platform) : null;
            }
        }

        /// <summary>
        ///     This returns the merchant info (MID, Currency, etc) based on a filter expression.
        /// </summary>
        /// <returns>The Merchant Info for the market based on the filter passed in</returns>
        public IMerchantInfo GetMerchantInfo(string submarket, string platform)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var command = new SqlCommand("Card_GetURLMapping_Submarket", connection))
            using (var adapter = new SqlDataAdapter(command))
            {
                command.CommandType = CommandType.StoredProcedure;
                connection.Open();

                DataSet ds = new DataSet();
                adapter.Fill(ds);

                string filterExpression = string.Format("submarket_id='{0}'", submarket);
                //this will be stored in cache later
                DataTable dt = ds.Tables[0];
                DataRow[] dr = dt.Select(filterExpression);

                return dr.Length > 0 ? LoadMerchantInfo(dr[0], true, platform) : null;
            }
        }

        public string GetAltMerchNo(string platform)
        {

            //alt mid 10= mobile and 1=web
            string altMID = "10"; // default  alt merchID
            if (platform.Equals("web", StringComparison.CurrentCultureIgnoreCase))
            {
                return "1";
            }

            return altMID;
        }

        public static bool QueueUserWorkItem<T>(T state, Action<T> callback)
        {
            return ThreadPool.QueueUserWorkItem(s => callback((T)s), state);
        }

        protected void Log(CardLog cardLog, string errorMessage, TraceEventType eventType)
        {
            if (_cardLogger != null)
            {
                _cardLogger.LogCard(cardLog, errorMessage, eventType);
            }
            //var logEntry = new LogEntry
            //    {
            //        ActivityId = Guid.NewGuid(),
            //        Title = "SVC_Transaction_Update",
            //        Severity = eventType,
            //        Message = errorMessage
            //    };
            //logEntry.ExtendedProperties.Add("__Type", "CardError");
            //logEntry.ExtendedProperties.Add("UserId", cardLog.UserId);
            //logEntry.ExtendedProperties.Add("CardId", cardLog.CardId);
            //logEntry.ExtendedProperties.Add("CardNumber", cardLog.CardNumber);

            //Logger.Write(logEntry);
        }

        private IServiceTransaction LoadServiceTransaction(IDataRecord transReader)
        {
            var serviceTransaction = new ServiceTransaction
            {
                RequestCode = transReader["RequestCode"].ToString(),
                MerchantId = transReader["MerchantID"].ToString().Trim(),
                TerminalId = transReader["TerminalID"].FromDBNullable(Convert.ToInt16).ToString("000#"),
                AltMerchNo = transReader["AltMerchNo"].ToString().Trim(),
                CardNumber = Encryption.DecryptCardNumber(transReader["CardNo"].ToString()),
                Pin = Encryption.DecryptPin(transReader["Pin"].ToString().Trim()),
                Amount = transReader["Amount"].FromDBNullable(Convert.ToDecimal),
                SourceCardNumber = Encryption.DecryptCardNumber(transReader["TransferFromCardNo"].ToString().Trim())
            };
            return serviceTransaction;
        }

        private IOrderAddressInfo LoadOrderAddresssInfo(DataRow orderAddressInfo)
        {
            if (orderAddressInfo == null)
            {
                return null;
            }

            var info = new OrderAddressInfo();

            info.AddressType = (int)orderAddressInfo["address_type"];

            if (orderAddressInfo["first_name"] != DBNull.Value)
            {
                info.FirstName = orderAddressInfo["first_name"].ToString();
            }

            if (orderAddressInfo["last_name"] != DBNull.Value)
            {
                info.LastName = orderAddressInfo["last_name"].ToString();
            }

            if (orderAddressInfo["address_line1"] != DBNull.Value)
            {
                info.AddressLine1 = orderAddressInfo["address_line1"].ToString();
            }

            if (orderAddressInfo["address_line2"] != DBNull.Value)
            {
                info.AddressLine2 = orderAddressInfo["address_line2"].ToString();
            }

            if (orderAddressInfo["city"] != DBNull.Value)
            {
                info.City = orderAddressInfo["city"].ToString();
            }

            if (orderAddressInfo["region_code"] != DBNull.Value)
            {
                info.Region = orderAddressInfo["region_code"].ToString();
            }

            if (orderAddressInfo["country_code"] != DBNull.Value)
            {
                info.Country = orderAddressInfo["country_code"].ToString();
            }

            if (orderAddressInfo["tel_number"] != DBNull.Value)
            {
                info.PhoneNumber = orderAddressInfo["tel_number"].ToString();
            }

            if (orderAddressInfo["postal_code"] != DBNull.Value)
            {
                info.PostalCode = orderAddressInfo["postal_code"].ToString();
            }

            return info;
        }

        private IMerchantInfo LoadMerchantInfo(DataRow merchantInfoDr, bool includeEncryption, string platform)
        {
            if (merchantInfoDr == null)
            {
                throw new CardIssuerException(CardIssuerErrorResource.InvalidSubMarketCode, CardIssuerErrorResource.InvalidSubMarketMessage);
            }

            var info = new MerchantInfo();

            if (merchantInfoDr["MerchantID"] != DBNull.Value)
            {
                //Get the encryption keys and add them to the object.
                if (includeEncryption)
                {
                    GetEncryptionData(merchantInfoDr["MerchantID"].ToString(), info);
                }
                info.HasEncryption = includeEncryption;
                info.Mid = merchantInfoDr["MerchantID"].ToString();
            }

            // Note: merchantInfoDr["field"] as string; may be a good alternative to nullcheck-toString();

            if (merchantInfoDr["Currency"] != DBNull.Value)
            {
                info.CurrencyCode = merchantInfoDr["Currency"].ToString();
            }

            if (merchantInfoDr["CurrencyISONo"] != DBNull.Value)
            {
                info.CurrencyNumber = merchantInfoDr["CurrencyISONo"].ToString();
            }

            if (merchantInfoDr["MerchantKey"] != DBNull.Value)
            {
                info.MerchantKey = merchantInfoDr["MerchantKey"].ToString();
            }

            if (merchantInfoDr["TransactionSecurityKey"] != DBNull.Value)
            {
                info.TransactionSecurityKey = merchantInfoDr["TransactionSecurityKey"].ToString();
            }

            if (merchantInfoDr["FraudThreshold"] != DBNull.Value)
            {
                info.FraudThreshold = merchantInfoDr["FraudThreshold"].ToString();
            }

            info.SubMarketId = merchantInfoDr["submarket_id"] == DBNull.Value
                                   ? null
                                   : merchantInfoDr["submarket_id"].ToString();

            //TODO: add altMID and TerminalID to db            
            info.AlternateMid = GetAltMerchNo(platform);
            info.TerminalId = "0001";

            return info;
        }


        public void GetEncryptionData(string mid, IMerchantInfo info)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var command = new SqlCommand("svc_GetEncryptionTable", connection))
            using (var adapter = new SqlDataAdapter(command))
            {
                command.CommandType = CommandType.StoredProcedure;

                connection.Open();

                DataSet ds = new DataSet();
                adapter.Fill(ds);

                DataTable encryptionData = ds.Tables[0];
                DataRow[] dr = encryptionData.Select("MerchantID=" + mid);
                if (dr.Length > 0)
                {
                    info.EncryptionId = dr[0]["encryptionid"].ToString();
                    info.Mwk = dr[0]["MWK"].ToString();
                    info.Emwk = dr[0]["eMWK"].ToString();
                }
            }
        }

        public long UpsertPartialTransferTransactionDetails(
            long cardAmountTransactionId,
            string toCardTransactionId,
            DateTime toCardTransactionDate,
            string fromCardTransactionId,
            DateTime? fromCardTransactionDate)
        {
            using (var connection = new SqlConnection(_connectionString))
            using (var command = new SqlCommand("UpsertPartialTransferTransactionDetails", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                connection.Open();

                command.Parameters.AddWithValue("@CardAmountTransactionId", cardAmountTransactionId);
                command.Parameters.AddWithValue("@ToCardTransactionId", toCardTransactionId ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("@ToCardTransactionDate", toCardTransactionDate);
                command.Parameters.AddWithValue("@FromCardTransactionId", fromCardTransactionId ?? (object)DBNull.Value);
                command.Parameters.AddWithValue("@FromCardTransactionDate", fromCardTransactionDate ?? (object)DBNull.Value);

                long returnValue = Convert.ToInt64(command.ExecuteScalar());
                return returnValue;
            }
        }

    }
}
