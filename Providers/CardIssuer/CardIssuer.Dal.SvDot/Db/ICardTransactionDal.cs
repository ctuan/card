﻿using System;
using System.Collections.Generic;
using System.Data;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.SvDot.Common;

namespace Starbucks.CardIssuer.Dal.SvDot.Db
{
    public interface ICardTransactionDal
    {
        /// <summary>
        /// Saves the transaction to the card_transactions database and returns the terminal transaction number to uniquely identify the card transaction against ValueLink
        /// </summary>
        /// <param name="cardNumber">encrypted card number</param>
        /// <param name="transferFromCardNumber">For Balance Transfers, the source card number</param>
        /// <param name="pin">encrypted PIN</param>
        /// <param name="amount">The amount of the transaction</param>
        /// <param name="merchantInfo">The merchant information, such as merchant ID</param>        
        /// <param name="requestCode">VL Request code for the operation to complete; these are four digit codes</param>
        /// <param name="merchTime">The date/time stamp of the transaction in ddMMyyyyHHmmss format</param>
        /// <returns>generated terminal transaction number</returns>
        long GetTerminalTransactionNumber(string cardNumber, string transferFromCardNumber, string pin, decimal amount,
                                         IMerchantInfo merchantInfo, int requestCode, string merchTime);

        /// <summary>
        /// Gets a starbucks card transaction
        /// </summary>
        /// <param name="transactionId">The terminal transaction ID matching the original transaction request</param>
        /// <returns>An <c>IDataReader</c> containing the transaction data.  Calling method is responsible for closing the reader.</returns>
        IServiceTransaction GetTransaction(long transactionId);


        /// <summary>
        /// Gets the ISO alpha currency code based on the ISO number
        /// </summary>
        /// <param name="isoCurrencyNumber">The 3-digit ISO currency number, e.g., 840 (USD)</param>
        /// <returns>The 3-letter ISO currency code, e.g., "USD"</returns>
        string GetIsoAlphaCurrency(string isoCurrencyNumber);

        /// <summary>
        /// Saves the card transaction result from ValueLink back to the svc_transactions table
        /// </summary>
        /// <param name="transactionId">The terminal transaction ID matching the original transaction request</param>
        /// <param name="authCode">The unique autohrization code from ValueLink</param>
        /// <param name="amount">The amount of the transaction, or 0 for balance and history checks.</param>
        /// <param name="balance">The ending balance after the transaction</param>
        /// <param name="responseCode">The response code from ValueLink, '00' for success, or an error code</param>
        /// <param name="status">The status code</param>
        /// <param name="exchangeRate">The exchange rate</param>
        /// <param name="baseCurrency">The base currency of the card</param>
        /// <param name="basePreviousBalance">The previous balance in the base currency</param>
        /// <param name="baseNewBalance">The new balance in the base currency</param>
        /// <param name="localCurrency">The local currency passed to VL (based on merchant ID)</param>
        /// <param name="cardNumber">The card number</param>
        /// <param name="voidRollbackCode">The request code for void or rollback</param>
        /// <param name="voidRollbackResponse">The response code for a void or rollback</param>
        void SaveReturnValues(long transactionId, string authCode, decimal amount, decimal balance, string responseCode,
                              int status, decimal? exchangeRate, int? baseCurrency, decimal basePreviousBalance,
                              decimal baseNewBalance, int localCurrency, string cardNumber, string voidRollbackCode,
                              string voidRollbackResponse);

        void SetVirtualActivationValues(long transactionId, string cardNumber, string pin);

        IMerchantInfo GetMerchantInfo(string filterExpression, string platform);

        IMerchantInfo GetMerchantInfoByMerchantId(string merchantId, string platform);

        IMerchantInfo GetMerchantInfoByCurrency(string currency);

        IOrderAddressInfo GetOrderGroupAddress(string orderId);

        string GetAltMerchNo(string platform); //todo: map to a transaction and submarket

        long UpsertPartialTransferTransactionDetails(long cardAmountTransactionId, string toCardTransactionId,
                                                    DateTime toCardTransactionDate
                                                    , string fromCardTransactionId, DateTime? fromCardTransactionDate);
    }
}
