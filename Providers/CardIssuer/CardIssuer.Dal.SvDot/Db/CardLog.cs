﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Starbucks.CardIssuer.Dal.SvDot.Models;

namespace Starbucks.CardIssuer.Dal.SvDot.Db
{
    [DataContract]
    public class CardLog : ICardLog
    {
        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public string CardNumber { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string Submarket { get; set; }

        [DataMember]
        public IDictionary<string, string> ParameterCollection { get; set; }

        [DataMember]
        public string CardId { get; set; }

        [DataMember]
        public string CardName { get; set; }

        [DataMember]
        public decimal Amount { get; set; }

        [DataMember]
        public string TransactionId { get; set; }
    }
}
