﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.CardIssuer.Dal.SvDot.Models;

namespace Starbucks.CardIssuer.Dal.SvDot
{
    public interface ICardLogger
    {
        void LogCard(ICardLog cardLog, string errorMessage, TraceEventType eventType);
    }
}
