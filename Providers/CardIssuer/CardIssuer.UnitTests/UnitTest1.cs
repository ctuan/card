﻿//using System;
//using System.Configuration;
//using System.Globalization;
//using Account.Dal.Sql;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Moq;
//using Starbucks.Address.Dal.Sql.Configuration;
//using Starbucks.BadWords.Dal.Provider.Sql;
//using Starbucks.BadWords.Dal.Provider.Sql.Configuration;
//using Starbucks.Card.Dal.Sql;
//using Starbucks.Card.Provider.Common.Enums;
//using Starbucks.CardIssuer.Dal.Common.Models;
//using Starbucks.CardIssuer.Dal.SvDot;
//using Starbucks.CardIssuer.Dal.SvDot.Db;
//using Starbucks.CardIssuer.Dal.SvDot.Requestor;
//using Starbucks.CardIssuer.Dal.SvDot.TransportModels;
//using Starbucks.CardTransaction.Provider;
//using Starbucks.LogCounter.Provider;
//using Starbucks.MessageBroker.Common;
//using Starbucks.PaymentMethod.Provider.SqlHybrid.Configuration;
//using Starbucks.SecurePayment.Provider;

//namespace Starbucks.CardIssuer.UnitTests
//{
//    [TestClass]
//    public class UnitTest1
//    {
//        [TestMethod]
//        public void TestMethod1()
//        {
//            var cardIssuer = new SvDotCardIssuer(new SvDotTransportXml(),                                                                      
//                                                                          new ServiceRequestor(
//                                                                              new CardTransactionDal(),
//                                                                              new SvDotTransportXml()),                                                                      
//                                                                      new CardTransactionDal(),
//                                                                      new MessageBroker.MessageBroker(null));


//            var pmi = cardIssuer.GetMerchantInfo("us");          
//            var b = cardIssuer.GetBalance(pmi, "7777005535661783", "54373259");
//            Assert.IsNotNull(b);
//        }

//        //[TestMethod]
//        //public void GetStarbucksCardByNumberAndPin()
//        //{
//        //    var cardTransactionProvider = CardTransactionProvider();

//        //    var b = cardTransactionProvider.GetCardByNumberAndPin("7777005535661783", "54373259");
//        //    Assert.IsNotNull(b);

//        //}

//        //[TestMethod]
//        //public void GetStarbucksCardByCardIdUserId()
//        //{
//        //    var cardTransactionProvider = CardTransactionProvider();

//        //    var b = cardTransactionProvider.GetCardByUserIdCardId("2127F74D-1852-4702-8831-196D214DD9AA", "866471FD");
//        //    Assert.IsNotNull(b);
//        //}

//        //[TestMethod]
//        //public void GetBalanceByNumberRealTime()
//        //{
//        //    var cardTransactionProvider = CardTransactionProvider();

//        //    var b = cardTransactionProvider.GetBalanceByNumberRealTime("7777005535661783", "54373259");
//        //    Assert.IsNotNull(b);
//        //}

//        //[TestMethod]
//        //public void GetBalanceByCardIdRealTime()
//        //{
//        //    var cardTransactionProvider = CardTransactionProvider();


//        //    var b = cardTransactionProvider.GetBalanceRegisteredRealTime("05810D95-CEC4-4274-8DEF-A5B1A17C84E4", "85607BFB99D21B");
//        //    Assert.IsNotNull(b);
//        //}

//        //[TestMethod]
//        //public void GetCardImages()
//        //{
//        //    var cardTransactionProvider = CardTransactionProvider();

//        //    var b = cardTransactionProvider.GetStarbucksCardImageUrlByCardNumber("7777005535661783");
//        //    Assert.IsNotNull(b);
//        //}

//        //[TestMethod]
//        //public void GetCards()
//        //{
//        //    var cardTransactionProvider = CardTransactionProvider();

//        //    var userid = "0AEB0D1F-05AD-4F6D-9545-3ED56E72B3A8";
//        //    //var userid = "05810D95-CEC4-4274-8DEF-A5B1A17C84E4";

//        //    var b = cardTransactionProvider.GetAssociatedCardsByVisibilityLevel(userid, VisibilityLevel.Decrypted);
//        //    var c = cardTransactionProvider.GetAssociatedCardsByVisibilityLevel(userid, VisibilityLevel.None);
//        //    var d = cardTransactionProvider.GetAssociatedCardsByVisibilityLevel(userid, VisibilityLevel.Encrypted);
//        //    var e = cardTransactionProvider.GetAssociatedCardsByVisibilityLevel(userid, VisibilityLevel.Masked);
//        //    Assert.IsNotNull(b);
//        //}

//        //[TestMethod]
//        //public void GetCardStatuses()
//        //{
//        //    var cardTransactionProvider = CardTransactionProvider();
//        //    var b = cardTransactionProvider.GetStatusesForUserCards("05810D95-CEC4-4274-8DEF-A5B1A17C84E4", new string[] { "1", "2" });
//        //    Assert.IsNotNull(b);
//        //}

//        [TestMethod]
//        public void ReloadCardByNumberPin()
//        {
//            var cardTransactionProvider = CardTransactionProvider();
//            var cardNumber = "7777005535661783";
//            var pin = "54373259";
//            var paymentMethodId = "806E71FD9E";

//            var t = cardTransactionProvider.ReloadStarbucksCardByCardNumberPin(cardNumber, pin, 13.00M, paymentMethodId, "iamanemailaddress@iamanemailaddress.com", "US");
//           // Assert.IsNotNull(t);
//        }


//        [TestMethod]
//        public void ReloadCardByCardIdUserId()
//        {
//            var cardTransactionProvider = CardTransactionProvider();

//            var cardId = "856477F0";
//            var paymentMethodId = "806073FA98";
//            var userId = "2127F74D-1852-4702-8831-196D214DD9AA";

//            var t = cardTransactionProvider.ReloadCardByUserIdCardId(userId, cardId, 13.00M, paymentMethodId, string.Empty, "US", "US");
//            Assert.IsNotNull(t);
//        }

//        //[TestMethod]
//        //public void ActivateDigitalCard()
//        //{
//        //    var cardTransactionProvider = CardTransactionProvider();

//        //    var userId = "2127F74D-1852-4702-8831-196D214DD9AA";

//        //    var t = cardTransactionProvider.ActivateAndRegisterCard( userId);
//        //    Assert.IsNotNull(t);
//        //}        

//        //[TestMethod]
//        //public void RegisterCard()
//        //{
//        //    var cardTransactionProvider = CardTransactionProvider();

//        //    var userId = "2127F74D-1852-4702-8831-196D214DD9AA";
//        //    var cardNumber = "7777064088788256";
//        //    var pin = "05025241";

//        //    var t = cardTransactionProvider.RegisterStarbucksCard(userId, cardNumber, pin);
//        //    Assert.IsNotNull(t);
//        //}

//        //[TestMethod]
//        //public void EnableAutoReload()
//        //{
//        //    var cardTransactionprovider = CardTransactionProvider();

//        //    var userId = "D86E59EC-385E-4093-AC83-D4C012ABD261";
//        //    var cardId = "8D6F75";
                        	
//        //    cardTransactionprovider.EnableAutoReload(userId, cardId);
//        //}

//        //[TestMethod]
//        //public void DisableAutoReload()
//        //{
//        //    var cardTransactionprovider = CardTransactionProvider();

//        //    var userId = "D86E59EC-385E-4093-AC83-D4C012ABD261";
//        //    var cardId = "8D6F75";

//        //    cardTransactionprovider.DisableAutoReload(userId, cardId);
//        //}

//        //[TestMethod]
//        //public void CreateAutoReload()
//        //{
//        //    var cardTransactionprovider = CardTransactionProvider();

//        //    var userId = "B5E242A7-3D5E-4EAD-8A7C-A24419D4B32F";
//        //    var cardId = "856570FE";

//        //    var autoReloadProfile = new AutoReloadProfile();
//        //    autoReloadProfile.Amount = 20;
//        //    autoReloadProfile.AutoReloadType = "Amount";
//        //    autoReloadProfile.TriggerAmount = 10;
//        //    autoReloadProfile.PaymentMethodId = "8D6F7BF8";
//        //    autoReloadProfile.UserId = userId;
//        //    autoReloadProfile.CardId = cardId;

//        //    cardTransactionprovider.SetupAutoReload(userId, cardId, autoReloadProfile );
//        //}


//        //[TestMethod]
//        //public void UpdateAutoReload()
//        //{
//        //    var cardTransactionprovider = CardTransactionProvider();

//        //    var userId = "B5E242A7-3D5E-4EAD-8A7C-A24419D4B32F";
//        //    var cardId = "856570FE";

//        //    var autoReloadProfile = new AutoReloadProfile();
//        //    autoReloadProfile.Amount = 20;
//        //    autoReloadProfile.AutoReloadType = "Amount";
//        //    autoReloadProfile.TriggerAmount = 10;
//        //    autoReloadProfile.PaymentMethodId = "8D6F7BF8";
//        //    autoReloadProfile.UserId = userId;
//        //    autoReloadProfile.CardId = cardId;
//        //    autoReloadProfile.AutoReloadId = "04da1a3c-a947-4310-bfb3-0befa6f8d59b";

//        //    cardTransactionprovider.UpdateAutoReload( userId, cardId, autoReloadProfile);
//        //}


//        //[TestMethod]
//        //public void UpdateNickname()
//        //{
//        //    var cardTransactionprovider = CardTransactionProvider();
//        //    var userId = "bb3a5e48-0544-4e1e-af8c-3ec11ee6a970";
//        //    var cardId = "85607BFE99D71D";
//        //    var nickname = "TestNickName";

//        //    cardTransactionprovider.UpdateCardNickname(userId, cardId, nickname);
//        //}

//        //[TestMethod]
//        //public void UpdateDefault()
//        //{
//        //    var cardTransactionprovider = CardTransactionProvider();
//        //    var userId = "bb3a5e48-0544-4e1e-af8c-3ec11ee6a970";
//        //    var cardId = "85607BFE99D71D";
           
//        //    cardTransactionprovider.UpdateDefaultCard(userId, cardId);
//        //}

//        private static CardTransactionProvider CardTransactionProvider()
//        {
//            var cardIssuerDal = new SvDotCardIssuer(new SvDotTransportXml(),                                                   
//                                                        new ServiceRequestor(
//                                                            new CardTransactionDal(),
//                                                            new SvDotTransportXml()),                                                   
//                                                    new CardTransactionDal(),
//                                                    new MessageBroker.MessageBroker(null));

//            //var cardIssuer = new Provider.CardIssuer(cardIssuerDal);

//            var mockMessageBroker = new Mock<IMessageBroker>();

//            var cardProvider = new Starbucks.Card.Provider.CardProvider(new CardDal("Profiles"), cardIssuerDal, LogCounterManager.Instance(), mockMessageBroker.Object);

//            var paymentMethodSettings =
//                ConfigurationManager.GetSection("paymentMethodProviderSettings") as PaymentMethodProviderSection;

//            var addressSettings =
//                ConfigurationManager.GetSection("addressProviderSettings") as AddressDataProviderSection;

//            var badWordsSettings = ConfigurationManager.GetSection("badWordProviderSettings") as BadWordsSection;


//            var addressDataProvider = new Address.Dal.Sql.AddressDataProvider(addressSettings);

//            var accountProvider = new Account.Provider.AccountProvider(new AccountSqlDataProvider("Profiles"), addressDataProvider, new MessageBroker.MessageBroker(null));
        
//            var badWordsProvider = new BadWords.Provider.BadWordsProvider(new BadWordsDataProvider(badWordsSettings));

//            var paymentMethodProvider =
//                new PaymentMethod.Provider.PaymentMethodProvider(
//                    new PaymentMethod.Provider.SqlHybrid.PaymentMethodDataProvider(paymentMethodSettings,
//                                                                                   addressDataProvider, badWordsProvider));

//            var paymentProvider = new SecurePaymentClient();

//            var cardTransactionProvider = new CardTransactionProvider(cardProvider, cardIssuerDal, paymentMethodProvider, accountProvider, paymentProvider, LogCounterManager.Instance(), null);
//            return cardTransactionProvider;
//        }
//    }
//}
