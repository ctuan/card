﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.CardIssuer.UnitTests
{
    class AutoReloadProfile : IAutoReloadProfile
    {
        public string UserId { get; set; }
        public string CardId { get; set; }
        public string AutoReloadType { get; set; }
        public int? Day { get; set; }
        public decimal? TriggerAmount { get; set; }
        public decimal Amount { get; set; }
        public string PaymentType { get; set; }
        public string PaymentMethodId { get; set; }
        public string AutoReloadId { get; set; }
        public int Status { get; set; }
        public DateTime? DisableUntilDate { get; set; }
        public DateTime? StoppedDate { get; set; }
        public string BillingAgreementId { get; set; }
    }
}
