﻿namespace Starbucks.CardIssuer.Dal.SvDot.Common
{
    public interface IServiceTransaction
    {
        string RequestCode { get; set; }
        string MerchantId { get; set; }
        string TerminalId { get; set; }
        string AltMerchNo { get; set; }
        string CardNumber { get; set; }
        string Pin { get; set; }
        decimal Amount { get; set; }
        string SourceCardNumber { get; set; }
    }
}
