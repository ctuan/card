﻿namespace Starbucks.CardIssuer.Dal.SvDot.Common
{
    public interface ISvDotTransport
    {       

        /// <summary>
        /// A string representation of the payload to send through the secure transport
        /// </summary>
        string Payload { get; set; }

        /// <summary>
        /// A unique ID for the payload transaction sent through the secure transport
        /// <remarks>Can use this as the ClientRef ID for certain Secure Transport APIs</remarks>
        /// </summary>
        string PayloadTransactionId { get; set; }

        /// <summary>
        /// The string representation of the response from the card processor
        /// </summary>
        string Response { get; }

        /// <summary>
        /// A byte array containing the response from the card processor
        /// </summary>
        byte[] ResponseBytes { get; }

        /// <summary>
        /// If true, indicates that there was a transport-specific error, the payload was not sent successfully,
        /// and the transaction can be retried without rollback.  
        /// If false, caller should, initiate a rollback if the return code is not zero.
        /// </summary>
        bool Retry { get; }

        /// <summary>
        /// Sends the payload to the card processor using the property settings.
        /// </summary>
        /// <returns>The status of the transport transaction, 0 = success</returns>
        int Send();

    }
}
