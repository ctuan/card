﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;

namespace Starbucks.CardIssuer.Dal.SvDot.Common
{
    /// <summary>
    /// Delegate place-holder for a conversion function, such as Convert.ToInt32().
    /// </summary>
    /// <typeparam name="T">The type the conversion method converts to</typeparam>
    /// <param name="value">The object to convert</param>
    /// <returns>The value converted to type T.</returns>
    public delegate T ConvertValue<T, V>(V value);

    public static class SvDotExtensions
    {
        /// <summary>
        /// Returns the string representation of the Field code number.
        /// </summary>
        /// <param name="field">The request field</param>
        /// <returns>String </returns>
        public static string GetField(this SvDotRequestField field)
        {
            //Format the int value with leading 0s.
            return string.Format("{0:00}", (int)field);
        }

        /// <summary>
        /// Formats the decimal value as an implied decimal
        /// </summary>
        /// <param name="value">The decimal value</param>
        /// <returns>A string in implied decimal format, where the last two digits are understood to be to the right of the decimal</returns>
        public static string ToImpliedDecimal(this decimal value)
        {
            //Convert the decimal to a string, and remove the decimal point.
            return value.ToString("###0.00", CultureInfo.InvariantCulture).Replace(".", string.Empty);
        }

        /// <summary>
        /// Tries to get the string value of a key from a generic string Dictionary object,
        /// </summary>
        /// <param name="dictionary">The Dictionary object</param>
        /// <returns>The value of the key, or the default value of TVal, if the key does not exist</returns>
        public static string GetValue(this Dictionary<string, string> dictionary, string key)
        {
            string value;
            dictionary.TryGetValue(key, out value);
            return value;
        }

        /// <summary>
        /// Returns a null reference if the object is an instance of DBNull.
        /// Otherwise returns the object.
        /// </summary>
        /// <param name="value">The object to convert from DBNull.</param>
        /// <returns>Returns an object that is either null or a non-DBNull object.</returns>
        /// <remarks>
        /// Use this method to convert from a value that might be a DBNull to a true null. 
        /// </remarks>
        public static T FromDBNullable<T>(this object value)
        {
            if (Convert.IsDBNull(value))
            {
                return default(T);
            }
            else
            {
                //TODO:  HARD CASTS FAIL WITH NULLABLE TYPES.  NEED TO FIND A WAY TO FAIL GRACEFULLY....
                return (T)value;
            }
        }

        /// <summary>
        /// Returns a null reference (or default value) if the object is an instance of DBNull.
        /// Otherwise convert the value 
        /// </summary>
        /// <typeparam name="T">The type to which the object will be converted, and the type returned.</typeparam>
        /// <param name="value">The object to convert.</param>
        /// <param name="convert">The <c>DBConvert<typeparamref name=">"/></c> conversion method delegate</param>
        /// <returns>Returns an object that is either null or a non-DBNull object.</returns>
        /// <remarks>
        /// Use this method to convert from a value that might be a DBNull to a true null, or the appropriate type. 
        /// </remarks>
        public static T FromDBNullable<T>(this object value, ConvertValue<T, object> convert)
        {
            return Convert.IsDBNull(value) ? default(T) : convert(value);
        }

        /// <summary>
        /// Tries to get the string value of a key from a generic string Dictionary object,
        /// and converts it to a value of the given type.
        /// </summary>
        /// <typeparam name="T">The type to convert the Dictionary value into.</typeparam>
        /// <param name="dictionary">The Dictionary object</param>
        /// <param name="convert">The <c>DBConvert</c> conversion method delegate</param>
        /// <returns>The value converted to the type T, or the default value of T</returns>
        public static T GetValueAndConvert<T>(this Dictionary<string, string> dictionary, string key, ConvertValue<T, string> convert)
        {
            string value;
            if (dictionary.TryGetValue(key, out value))
            {
                if (!string.IsNullOrEmpty(value))
                {
                    try
                    {
                        return convert(value);
                    }
                    catch
                    {
                        return default(T);
                    }
                }
            }
            return default(T);
        }


        public static string GetFaultText(this KnownFault knownFault)
        {
            return knownFault.ToString().Replace("_", " ");
        }

        public static int GetFaultCode(this KnownFault knownFault)
        {
            return (int)knownFault;
        }

        public static FaultException FaultException(this KnownFault knownFault)
        {
            var reasonText = GetFaultText(knownFault);
            var code = GetFaultCode(knownFault);
            var reason = new FaultReason(new List<FaultReasonText>
			{
				new FaultReasonText(reasonText, new CultureInfo("en-US"))
			});
            var fault = new FaultException(reason, new FaultCode(code.ToString()));
            fault.Data.Add("StarbucksKnownFaultCode", code);
            return fault;
        }

        /// <summary>
        /// Converts a Pascal-cased enum string value into a string of words separated by spaces.
        /// <example>"MyValue" into "My Value"</example>
        /// </summary>
        /// <param name="value">The string with Pascal-cased characters to parse.</param>
        /// <returns>The Pascal-cased string parsed into separate words</returns>
        public static string ParsePascal(this string value)
        {
            //Regex for a capitalized word.
            Regex wordUp = new Regex(@"[A-Z]{1}[a-z0-9]+");

            //Find all of the capitalized words in the enum string value.
            MatchCollection pascalWords = wordUp.Matches(value.ToString());

            StringBuilder words = new StringBuilder();

            //Loop through the matches, and add each word to the string builder.
            foreach (Match wordMatch in pascalWords)
            {
                //Append the word found followed by a space.
                words.AppendFormat("{0} ", wordMatch.Value);
            }

            //Return the entire string trimmed of leading/tailing spaces.
            return words.ToString().Trim();

        }


        public static bool IsInt(this string expression)
        {
            if (string.IsNullOrEmpty(expression))
                return false;
            int temp;
            return int.TryParse(Convert.ToString(expression, CultureInfo.InvariantCulture), System.Globalization.NumberStyles.Any, NumberFormatInfo.InvariantInfo, out temp);
        }
    }
}
