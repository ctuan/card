﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Starbucks.CardIssuer.Dal.SvDot.Common")]
[assembly: AssemblyDescription("Starbucks.CardIssuer.Dal.SvDot.Common")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Starbucks Coffee Company")]
[assembly: AssemblyProduct("Starbucks.CardIssuer.Dal.SvDot.Common")]
[assembly: AssemblyCopyright("Starbucks Coffee Company ©  2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("deb8a0c4-2179-451f-ab19-21c0002fe66b")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]
