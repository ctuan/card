﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Resources;
using Starbucks.CardIssuer.Dal.Common.Rule;

namespace Starbucks.CardIssuer.Dal.SvDot.Common
{
    public class CardIssuerExceptionHandler
    {
        public static CardIssuerException HandleCardIssuerResponse(string responseCode)
        {
            //Parse the error code into an int.
            int responseNumber;
            if (!int.TryParse(responseCode, out responseNumber))
            {
                //Throw an error if the response code is invalid.
                return new CardIssuerException(CardIssuerErrorResource.UnknownErrorCode, CardIssuerErrorResource.UnknownErrorMessage);
            }

            switch (responseNumber)
            {
                case 0: //No error. Return.
                case 34: //Already reversed.  For rollbacks this is just an extra call, so we can ignore it.
                case 38: //No transaction history--could be a new card, so just return.
                    return null;
                case 1:
                    //Insufficient funds
                    return new CardIssuerException(CardIssuerErrorResource.InsufficientFundsForTransactionCode, CardIssuerErrorResource.InsufficientFundsForTransactionMessage);
                case 2:
                    //Account closed                    
                    return new CardIssuerException(CardIssuerErrorResource.AccountIsClosedCode, CardIssuerErrorResource.AccountIsClosedMessage);
                case 3:
                    //Unknown account                    
                    return new CardIssuerException(CardIssuerErrorResource.InvalidCardNumberCode, CardIssuerErrorResource.InvalidCardNumberMessage);
                case 4:
                    //Inactive account                    
                    return new CardIssuerException(CardIssuerErrorResource.CardIsInactiveCode, CardIssuerErrorResource.CardIsInactiveMessage);
                case 10:
                    //Lost or stolen                    
                    return new CardIssuerException(CardIssuerErrorResource.CardReportedLostStolenCode, CardIssuerErrorResource.CardReportedLostStolenMessage);
                case 12:
                    return
                        new CardIssuerException(
                            CardIssuerErrorResource.InvalidAmountMoreOrLessThanMinOrMaxAmountSpecifiedCode,
                            CardIssuerErrorResource.InvalidAmountMoreOrLessThanMinOrMaxAmountSpecifiedMessage);
                case 17:
                    //Max balance                    
                    return new CardIssuerException(CardIssuerErrorResource.MaximumBalanceExceededCode, CardIssuerErrorResource.MaximumBalanceExceededMessage);
                case 18:
                    return new CardIssuerException(CardIssuerErrorResource.InvalidAmountMoreOrLessThanMinOrMaxAmountSpecifiedCode, CardIssuerErrorResource.InvalidAmountMoreOrLessThanMinOrMaxAmountSpecifiedMessage);
                case 27:
                    //Request not permitted by this account
                    return new CardIssuerException(CardIssuerErrorResource.RequestNotPermittedByThisAccountCode, CardIssuerErrorResource.RequestNotPermittedByThisAccountMessage);
                case 44:
                    //Lost or stolen (disabled)                    
                    return new CardIssuerException(CardIssuerErrorResource.CardReportedLostStolenInternetDisabledCode, CardIssuerErrorResource.CardReportedLostStolenInternetDisabledMessage);
                case 45:
                    //Invalid PIN                    
                    return new CardIssuerException(CardIssuerErrorResource.InvalidPinCode, CardIssuerErrorResource.InvalidPinMessage);
                case 52:
                    //Interactive and VL timeout                    
                    return new CardIssuerException(CardIssuerErrorResource.ValueLinkHostDownCode, CardIssuerErrorResource.ValueLinkHostDownMessage);
                default:
                    //Another error occurred.
                    return new CardIssuerException(CardIssuerErrorResource.UnknownErrorCode, CardIssuerErrorResource.UnknownErrorMessage);
            }          
        }
    }
}
