﻿namespace Starbucks.CardIssuer.Dal.SvDot.Common
{
    /// <summary>
    /// Status Code to return
    /// </summary>
    public enum ReturnStatus
    {
        OK = 0,
        //Connection or web request error
        NetworkError = 1,
        //Http Error - Response code that is not 200
        HttpError = 2,
        //Http 500 Error
        Http500Error = 3,
        //A SecureTransport Status other than 'OK'
        SecureTransportError = 4,
        //An error in the simple transaction.
        TransactionError = 5,
        //Other error
        OtherError = 6
    }

    /// <summary>
    /// Status code returned in SecureTransport call
    /// </summary>
    internal enum SecureTransportStatus
    {
        OK = 0,
        AuthenticationError = 1,
        UnknownServiceID = 2,
        WrongSessionContext = 3,
        AccessDenied = 4,
        Failed = 5,
        Retry = 6,
        Timeout = 7,
        XMLError = 8,
        OtherError = 9
    }

    /// <summary>
    /// Return code from the SimpleTransaction call.
    /// </summary>
    internal enum TransactionReturnCode
    {
        OK = 0,
        HostBusy = 200,
        HostUnvailable = 201,
        HostConnectError = 202,
        HostDrop = 203,
        HostCommError = 204,
        NoResponse = 205,
        HostSendError = 206,
        SecureTransportTimeout = 405,
        NetworkError = 505
    }
}
