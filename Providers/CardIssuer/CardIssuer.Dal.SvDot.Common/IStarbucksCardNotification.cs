﻿using System;
using System.ServiceModel;
using Starbucks.CardIssuer.Dal.Common.Models;

namespace Starbucks.CardIssuer.Dal.SvDot.Common
{
    [ServiceContract]
    public interface IStarbucksCardNotification
    {
        [OperationContract(Name = "StarbucksCardUpdate", IsOneWay = true)]
        void StarbucksCardUpdate(string notificationType, string transactionId, DateTime transactionTime, string cardNumber, string baseCurrency, string localCurrency, decimal? balance, decimal? previousBalance, string storeNumber, string nickname, string source, string sourceAction);


        void PublishTransaction(ICardTransaction trans);
    }
}
