﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Starbucks.CardIssuer.Dal.SvDot.Common
{
    public class ValueLinkEncryption
    {

        /// <summary>
        /// Encrypts the PIN for transmission to ValueLink
        /// </summary>
        /// <param name="sMWK"></param>
        /// <param name="sPIN"></param>
        /// <returns>encrypted base64string of the encrypted PIN</returns>
        public static string EncryptPIN(string sMWK, string sPIN)
        {
            byte[] pbMWK = new byte[sMWK.Length];
            byte[] pbPIN = new byte[8];
            byte[] pbData = new byte[16];
            byte[] encPIN = new byte[0];

            //generate a random seed value
            Random srand = new Random(unchecked((int)DateTime.Now.Ticks));

            pbMWK = StringToByte(sMWK);

            //this replaces the above lines of code
            pbPIN = Encoding.ASCII.GetBytes(sPIN);

            //add 8 values to the array for encryption
            for (int i = 0; i < 7; i++)
            {
                pbData[i] = (byte)srand.Next();
            }
            //get the checksum value for the final byte
            pbData[7] = CheckSum(pbPIN, 8);

            //add the pin Array to the data to create the full value to be
            //encrypted
            pbPIN.CopyTo(pbData, 8);

            encPIN = EncryptValue(pbData, pbMWK);

            //cleanup
            pbMWK = null;
            pbPIN = null;
            pbData = null;

            //convert the encrypted bytes to an ASCII string of hex values
            return ByteToString(encPIN, 16);

        }

        /// <summary>
        /// converts a byte array into an ASCII string of hex values
        /// </summary>
        /// <param name="InputBytes"></param>
        /// <param name="length"></param>
        /// <returns>hex values as a string of the specified length</returns>
        private static string ByteToString(byte[] InputBytes, int length)
        {
            StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                hexString.Append(InputBytes[i].ToString("x2"));
            }


            return hexString.ToString();
        }

        /// <summary>
        /// converts a string to a byte array
        /// </summary>
        /// <param name="InputValue"></param>
        /// <returns>byte array of the input string</returns>
        private static byte[] StringToByte(string InputValue)
        {
            long dwData;

            dwData = InputValue.Length / 2;

            InputValue = InputValue.ToLower();

            byte[] pbData = new byte[dwData];

            for (int i = 0; i < dwData; i++)
            {
                string val = InputValue.Substring(i * 2, 1) + InputValue.Substring(i * 2 + 1, 1);
                pbData[i] = System.Convert.ToByte(val, 16);
            }

            return pbData;
        }
        /// <summary>
        /// Creates the checksum value for the PIN as specified by ValueLink for encryption
        /// </summary>
        /// <param name="block">the byte array of values to use to create the checksum</param>
        /// <param name="nBytes">total number of bytes to be used in the calculation</param>
        /// <returns>the checksum value as a byte</returns>
        private static byte CheckSum(byte[] block, long nBytes)
        {
            byte sum = 0;

            for (int i = 0; i < nBytes; ++i)
                sum += block[i];


            return sum;
        }

        /// <summary>
        /// returns a byte array of the encrypted value
        /// </summary>
        /// <param name="pbValue">value to encrypt converted to a byte array</param>
        /// <param name="pbKey">encryption key converted to a byte array</param>
        /// <returns>byte array of the encrypted value so each individual function can then process
        /// what it needs from the array</returns>
        private static byte[] EncryptValue(byte[] pbValue, byte[] pbKey)
        {
            byte[] EncryptedBytes;
            TripleDESCryptoServiceProvider objEncryptor;
            MemoryStream OutputStringEncrypted;
            ICryptoTransform DESEncrypt;
            CryptoStream cryptoStream;
            try
            {
                //set up the encryption provider and the output stream to write the encrypted value to
                objEncryptor = new TripleDESCryptoServiceProvider();

                //initialize the vector to a default value 
                objEncryptor.IV = new byte[8];
                //attach the encryption public key
                objEncryptor.Key = pbKey;

                OutputStringEncrypted = new MemoryStream();



                //create the encryptor
                //this is the object that does the actual work
                DESEncrypt = objEncryptor.CreateEncryptor();
                cryptoStream = new CryptoStream(OutputStringEncrypted,
                   DESEncrypt, CryptoStreamMode.Write);

                cryptoStream.Write(pbValue, 0, pbValue.Length);
                cryptoStream.FlushFinalBlock();

                //create a new byte array to hold the encrypted data
                EncryptedBytes = new byte[OutputStringEncrypted.Length];

                OutputStringEncrypted.Position = 0;
                OutputStringEncrypted.Read(EncryptedBytes, 0, (int)OutputStringEncrypted.Length);
                cryptoStream.Close();
                OutputStringEncrypted.Close();

                return EncryptedBytes;
            }
            catch (Exception e)
            {
                //RW: we should just throw a regular exception and let the calling app decide if they 
                //need to throw a soap exception
                //throw new SoapException(e.Message, System.Xml.XmlQualifiedName.Empty, e);
                throw new Exception(e.Message, e);
            }
            finally
            {
                //clean up the objects
                objEncryptor = null;
                DESEncrypt = null;
                OutputStringEncrypted = null;
                cryptoStream = null;
            }

        }

        /// <summary>
        /// decrypts a PIN 
        /// </summary>
        /// <param name="sMWK">a valid Merchant Working Key</param>
        /// <param name="sPIN">the PIN to be encrypted</param>
        /// <returns>UTF8 encoded string of the cleartext value</returns>
        public static string DecryptPIN(string sMWK, string sPIN)
        {

            byte[] pbMWK = new byte[sMWK.Length];
            byte[] pbPIN = new byte[sPIN.Length];
            byte[] pbReturn = new byte[0];

            pbPIN = StringToByte(sPIN);
            pbMWK = StringToByte(sMWK);

            pbReturn = DecryptValue(pbPIN, pbMWK);
            //clean up objects
            pbMWK = null;
            pbPIN = null;

            return System.Text.Encoding.UTF8.GetString(pbReturn, 8, 8);
        }

        /// <summary>
        /// decrypts a value into a byte array
        /// </summary>
        /// <param name="pbValue">byte array of the value to decrypt</param>
        /// <param name="pbKey">the encryption key to use</param>
        /// <returns>a byte array of the cleartext value</returns>
        private static byte[] DecryptValue(byte[] pbValue, byte[] pbKey)
        {

            //set up the encryption provider and the output stream to write the encrypted value to
            TripleDESCryptoServiceProvider objDecryptor = new TripleDESCryptoServiceProvider();

            objDecryptor.IV = new byte[8];
            objDecryptor.Key = pbKey;

            objDecryptor.Padding = System.Security.Cryptography.PaddingMode.None;
            //create an inMemory stream for the operation
            MemoryStream OutputStringDecrypted = new MemoryStream();


            //create the decryptor
            ICryptoTransform DESDecrypt = objDecryptor.CreateDecryptor();

            CryptoStream decryptoStream = new CryptoStream(OutputStringDecrypted,
                DESDecrypt, CryptoStreamMode.Write);
            decryptoStream.Write(pbValue, 0, pbValue.Length);
            decryptoStream.FlushFinalBlock();

            //create a byte array to hold the decrypted bytes
            byte[] DecryptedBytes = new byte[OutputStringDecrypted.Length];
            OutputStringDecrypted.Position = 0;
            OutputStringDecrypted.Read(DecryptedBytes, 0, (int)OutputStringDecrypted.Length);
            decryptoStream.Close();
            OutputStringDecrypted.Close();
            //clean up the objects
            objDecryptor = null;
            DESDecrypt = null;
            OutputStringDecrypted = null;
            decryptoStream = null;

            //return the decrypted value as a byte array for more processing
            //by the function that called this method

            return DecryptedBytes;

        }
    }
}
