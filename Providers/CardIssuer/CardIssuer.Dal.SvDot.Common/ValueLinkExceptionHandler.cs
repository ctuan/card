﻿//using System.ServiceModel;

//namespace Starbucks.CardIssuer.Dal.SvDot.Common
//{
//    private class ValueLinkExceptionHandler
//    {

//        /// <summary>
//        /// Handles error codes returned from ValueLink/First Data.
//        /// <remarks>Throws a <c>ValueLinkException</c> with the appropriate resource code for the message.
//        ///   Returns if response is 00, indicating no error.
//        /// </remarks>
//        /// </summary>
//        /// <param name="responseCode">The numeric response code from ValueLink</param>
//        public static ValueLinkException GetValueLinkExceptionFromResponse(string responseCode)
//        {
//            //Parse the error code into an int.
//            int responseNumber;
//            if (!int.TryParse(responseCode, out responseNumber))
//            {
//                //Throw an error if the response code is invalid.
//                return new ValueLinkException("", KnownFault.Unexpected_Error);
//            }

//            KnownFault knownFault;

//            switch (responseNumber)
//            {
//                case 0: //No error. Return.
//                case 34: //Already reversed.  For rollbacks this is just an extra call, so we can ignore it.
//                case 38: //No transaction history--could be a new card, so just return.
//                    return null;
//                case 1:
//                    //Insufficient funds
//                    knownFault = KnownFault.Insufficient_funds_for_transaction;
//                    break;
//                case 2:
//                    //Account closed
//                    knownFault = KnownFault.Account_is_closed;
//                    break;
//                case 3:
//                    //Unknown account
//                    knownFault = KnownFault.Invalid_card_number;
//                    break;
//                case 4:
//                    //Inactive account
//                    knownFault = KnownFault.Card_is_inactive;
//                    break;
//                case 10:
//                    //Lost or stolen
//                    knownFault = KnownFault.Card_reported_as_lost_or_stolen;
//                    break;
//                case 17:
//                    //Max balance
//                    knownFault = KnownFault.Maximum_balance_exceeded;
//                    break;
//                case 18:
//                    knownFault = KnownFault.InvalidAmount_MoreOrLess_than_MinOrMax_amount_specified;
//                    break;
//                case 27:
//                    //Request not permitted by this account
//                    knownFault = KnownFault.Request_not_permitted_by_this_account;
//                    break;
//                case 44:
//                    //Lost or stolen (disabled)
//                    knownFault = KnownFault.Card_reported_lost_or_stolen_internet_disabled;
//                    break;
//                case 45:
//                    //Invalid PIN
//                    knownFault = KnownFault.Invalid_PIN;
//                    break;
//                case 52:
//                    //Interactive and VL timeout
//                    knownFault = KnownFault.ValueLink_host_down;
//                    break;
//                default:
//                    //Another error occurred.
//                    knownFault = KnownFault.Unexpected_Error;
//                    break;
//            }

//            //If we got this far without exiting, there was some sort of error.  Throw it up the chain.
//            return new  ValueLinkException(responseCode, knownFault);
//        }

//        /// <summary>
//        /// Handles error codes returned from ValueLink/First Data.
//        /// <remarks>Throws a <c>ValueLinkException</c> with the appropriate resource code for the message.
//        ///   Returns if response is 00, indicating no error.
//        /// </remarks>
//        /// </summary>
//        /// <param name="responseCode">The numeric response code from ValueLink</param>
//        public static void CheckValueLinkResponse(string responseCode)
//        {
//            //Parse the error code into an int.
//            int responseNumber;
//            if (!int.TryParse(responseCode, out responseNumber))
//            {
//                //Throw an error if the response code is invalid.
//                throw new ValueLinkException("", KnownFault.Unexpected_Error);
//            }

//            KnownFault knownFault;

//            switch (responseNumber)
//            {
//                case 0: //No error. Return.
//                case 34: //Already reversed.  For rollbacks this is just an extra call, so we can ignore it.
//                case 38: //No transaction history--could be a new card, so just return.
//                    return;
//                case 1:
//                    //Insufficient funds
//                    knownFault = KnownFault.Insufficient_funds_for_transaction;
//                    break;
//                case 2:
//                    //Account closed
//                    knownFault = KnownFault.Account_is_closed;
//                    break;
//                case 3:
//                    //Unknown account
//                    knownFault = KnownFault.Invalid_card_number;
//                    break;
//                case 4:
//                    //Inactive account
//                    knownFault = KnownFault.Card_is_inactive;
//                    break;
//                case 10:
//                    //Lost or stolen
//                    knownFault = KnownFault.Card_reported_as_lost_or_stolen;
//                    break;
//                case 17:
//                    //Max balance
//                    knownFault = KnownFault.Maximum_balance_exceeded;
//                    break;
//                case 18:
//                    knownFault = KnownFault.InvalidAmount_MoreOrLess_than_MinOrMax_amount_specified;
//                    break;
//                case 27:
//                    //Request not permitted by this account
//                    knownFault = KnownFault.Request_not_permitted_by_this_account;
//                    break;
//                case 44:
//                    //Lost or stolen (disabled)
//                    knownFault = KnownFault.Card_reported_lost_or_stolen_internet_disabled;
//                    break;
//                case 45:
//                    //Invalid PIN
//                    knownFault = KnownFault.Invalid_PIN;
//                    break;
//                case 52:
//                    //Interactive and VL timeout
//                    knownFault = KnownFault.ValueLink_host_down;
//                    break;
//                default:
//                    //Another error occurred.
//                    knownFault = KnownFault.Unexpected_Error;
//                    break;
//            }

//            //If we got this far without exiting, there was some sort of error.  Throw it up the chain.
//            throw new ValueLinkException(responseCode, knownFault);

//        }

//        /// <summary>
//        /// Translates a <see>
//        ///                  <cref>Starbucks.Platform.Exceptions.ValueLinkException</cref>
//        ///              </see>
//        ///     to a <see>
//        ///              <cref>Starbucks.Platform.Exceptions.StarbucksFaultException</cref>
//        ///          </see>
//        ///     using the <c>ResourceCode</c> property
//        /// </summary>
//        /// <param name="vlex">The <see>
//        ///                            <cref>Starbucks.Platform.Exceptions.ValueLinkException</cref>
//        ///                        </see>
//        ///     thrown</param>
//        /// <returns>A <see>
//        ///                <cref>Starbucks.Platform.Exceptions.StarbucksFaultException</cref>
//        ///            </see>
//        ///     with the appropriate resource code</returns>
//        public static FaultException ValueLinkToFaultException(ValueLinkException vlex)
//        {
//            return vlex.KnownFault.FaultException();
//        }
//    }
//}
