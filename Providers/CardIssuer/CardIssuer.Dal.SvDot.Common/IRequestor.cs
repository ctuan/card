﻿using System.Collections.Generic;
using Starbucks.CardIssuer.Dal.Common.Models;

namespace Starbucks.CardIssuer.Dal.SvDot.Common
{
    public interface IRequestor
    {
        ICardTransaction SendRequest(ISvDotRequestBuilder request, int rollbackAttempts);

        List<ICardTransaction> GetHistory(IMerchantInfo merchantInfo, string cardNumber, string pin, Enums.CallItCommand callItCommand);
    }
}
