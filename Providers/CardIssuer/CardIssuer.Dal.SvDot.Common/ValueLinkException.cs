﻿//using System;

//namespace Starbucks.CardIssuer.Dal.SvDot.Common
//{
//    public class ValueLinkException : Exception
//    {
//        string _responseCode;

//        public ValueLinkException(string responseCode, KnownFault knownFault)
//        {
//            _responseCode = responseCode;
//            KnownFault = knownFault;
//        }

//        public ValueLinkException()
//        {
//            KnownFault = KnownFault.Unexpected_Error;
//        }

//        public ValueLinkException(string msg)
//            : base(msg)
//        {
//            KnownFault = KnownFault.Unexpected_Error;
//        }

//        public ValueLinkException(string msg, Exception innerException)
//            : base(msg, innerException)
//        {
//            KnownFault = KnownFault.Unexpected_Error;
//        }

//        /// <summary>
//        /// The response code value from ValueLink - Any non-zero response is an error.
//        /// </summary>
//        public string ResponseCode
//        {
//            get { return _responseCode; }
//        }

//        /// <summary>
//        /// The Starbucks resource code for the error message to return.
//        /// </summary>
//        public KnownFault KnownFault { get; set; }
//    }
//}
