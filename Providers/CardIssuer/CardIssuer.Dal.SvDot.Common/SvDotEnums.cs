﻿namespace Starbucks.CardIssuer.Dal.SvDot.Common
{
    /// <summary>
    /// Field name and numerical value for SvDot Request Transactions
    /// </summary>
    public enum SvDotRequestField
    {
        TransactionAmount = 4,
        CardCost = 6,
        ReferenceNumber = 8,
        LocalTransactionTime = 12,
        LocalTransactionDate = 13,
        TransactionId = 15, //Terminal Transaction Number
        Pin = 34, //EAN
        MerchantTerminalId = 42, //Merchant ID + Terminal ID
        AltMerchantId = 44, //Alternate MID
        PostDate = 53,
        WorkingKey = 63,
        CardNumber = 70
    }


    /// <summary>
    /// Response code for the SvDot transaction.
    /// </summary>
    public enum SvDotResponseCode
    {
        Success = 0
    }

    	public enum KnownFault
	{
		Unexpected_Error = 500,
		Error_Communicating_With_ValueLink = 501,
		Card_Id_Not_Found = 503,
		Card_Already_Associated_To_User = 504,
		Card_is_not_associated_to_user = 505,
		No_registration_address_on_file = 506,
		Nickname_already_in_use = 507,
		Card_is_not_registered_to_user = 508,
		Card_must_be_unregistered_before_it_can_be_removed_from_user = 509,
		Card_is_already_registered = 510,
		Invalid_card_number = 511,
		Invalid_PIN = 512,
		Insufficient_funds_for_transaction = 513,
		Account_is_closed = 514,
		Card_is_inactive = 515,
		Card_reported_as_lost_or_stolen = 516,
		Card_reported_lost_or_stolen_internet_disabled = 517,
		Maximum_balance_exceeded = 518,
		Credit_card_number_is_invalid = 519,
		Credit_card_number_does_not_match_type = 520,
		Username_is_already_taken = 521,
		Invalid_user = 522,
		User_could_not_be_found_with_the_Username_Password_provided = 523,
		Partner_number_not_found = 524,
		Invalid_partner_number = 525,
		Partner_number_is_in_use_on_an_existing_user_account = 526,
		Invalid_date = 527,
		Over_100_transaction_limit = 528,
		Email_address_not_found = 529,
		Token_expired = 530,
		Invalid_token = 531,
		Timeout_occurred = 533,
		Does_not_meet_password_requirements = 534,
		Does_not_meet_username_requirements = 535,
		No_account_matching_email_and_username = 536,
		Billing_information_missing_fields = 537,
		Billing_information_contains_invalid_fields = 538,
		Timeout_processing_payment_information = 539,
		Credit_card_is_expired = 540,
		Credit_card_insufficient_funds = 541,
		Invalid_CVN = 542,
		Invalid_credit_card_number = 543,
		AVS_Failed = 544,
		Payment_method_not_associated_to_account = 545,
		Request_not_permitted_by_this_account = 546,
		Payment_method_already_exists = 547,
		ValueLink_host_down = 548,
		Invalid_submarket_code = 549,
		Cannot_remove_registration_address = 550,
		Address_does_not_belong_to_user = 551,
		AFS_Failed = 552,
		General_payment_error = 553,
		Card_from_unsupported_submarket = 554,
		Card_does_not_have_zero_balance = 555,
		Card_has_zero_balance = 556,
		Duetto_card_cannot_be_reported_lost_or_stolen = 557,
		Address_required_for_Gold_lost_stolen = 558,
		Minimum_reload_amount_not_met = 559,
		Maximum_reload_amount_exceeded = 560,
		Cannot_delete_payment_method_address = 561,
		Address_information_missing_fields = 562,
		Please_enter_a_valid_name = 563,
        No_address_verification_settings_found_in_site_configuration_for_this_country = 564,
		Not_a_custom_card_order = 565,
		Cancel_order_failed = 566,
		Reprint_order_failed = 567,
		Rebill_order_failed = 568,
		No_matching_order_found = 569,
		Refund_order_failed = 570,
		Autoreload_Not_Found = 571,
		Autoreload_Inactive = 572,
		Email_address_not_Unique = 573,
		Link_Connect_UserAccount_Failed = 574,
		Get_Link_Connect_UserAccount_Failed = 575,
		Username_is_already_taken_or_connectUserId_connectUserType_existed = 576,
		General_Connect_error = 577,
		Order_source_code_not_found = 578,
        Wall_post_failed = 579,
        Nickname_Not_Found = 580,
        InvalidAmount_MoreOrLess_than_MinOrMax_amount_specified = 581,
        UnregisterDigitalCardWithBalance = 582,
        Invalid_day = 583,
        Invalid_reload_amount = 584,
        EmailAddress_is_required = 585,
        UserName_is_required = 586,
        Password_is_required = 587,
        FirstName_is_required = 588,
        LastName_is_required=589,
        City_is_required = 590,
        CountrySubdivision_is_required = 591,
        PostalCode_is_required = 592,
        AdddressLine1_is_required = 593,
        Invalid_BirthDay=594,
        Invalid_BirthMonth=595,
        Invalid_BirthYear=596,
        MobilePhoneNumber_is_required_if_ReceiveStarbucksMobileMessages_is_true=597,
        AuthenticationType_can_only_be_Starbucks_or_StarbucksEnhanced=600,
        RegistrationSource_is_required=601,
        Invalid_PostalCode=602,
        Invalid_EmailAddress=603,
        Invalid_Password = 604,
        Invalid_UserName = 605,
        Country_is_required = 606,
        Invalid_InterestId = 607,
        DuplicateReloadRequest = 608,
		Not_a_valid_card_order = 609
	}

	
}
