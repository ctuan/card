﻿using System;
using System.Collections.Generic;
using Starbucks.CardIssuer.Dal.Common.Models;

namespace Starbucks.CardIssuer.Dal.SvDot.Common
{
    public interface ISvDotRequestBuilder
    {
        /// <summary>
        /// Adds or sets a field and value to the request, specific to the request type.
        /// </summary>
        /// <param name="requestField">The request field code to add. Numeric field codes must have leading 0s</param>
        /// <param name="value">The value of the field</param>
        void AddField(string field, string value);

        /// <summary>
        /// Gets the delimited string containing the field codes and values to
        /// send in the SvDot request
        /// </summary>
        /// <returns>The request string
        ///     <remarks>Request string must be sent without a field separator at the end. If fields must be added to the string, append a field separator to the return value.</remarks>
        /// </returns>
        string GetRequest();


        /// <summary>
        /// The unique ID of the request transaction
        /// </summary>
        long TransactionId { get; }

        /// <summary>
        /// The Request code of the First Data transaction
        /// </summary>
        Enums.CallItCommand RequestCode { get; set; }

        /// <summary>
        /// The Merchant ID to be used in the transaction
        /// </summary>
        string MerchantId { get; }

        /// <summary>
        /// The full merchant information for the transaction.
        /// </summary>
        IMerchantInfo MerchantInfo { get; }

        /// <summary>
        /// The SVC number to use in the transaction
        /// </summary>
        string CardNumber { get; set; }

        /// <summary>
        /// Add a PIN for transactions that require a PIN.
        /// </summary>
        string Pin { get; set; }

        /// <summary>
        /// The amount of the transaction.
        /// <remarks>
        ///     Use only for transactions that require amount, such as Reload or Redeem.
        ///     Do not use for non-monetary transactions, such as Balance or History.
        /// </remarks>
        /// </summary>
        decimal Amount { get; set; }

        /// <summary>
        /// Flag indicating that currency for merchant info should be sent with the request.
        /// Optional field, but should not be sent for non-monetary transactions--Balance, History, Cashout, Disable PIN
        /// Default value is true
        /// </summary>
        bool WithCurrency { get; set; }

        /// <summary>
        /// The date and local time at which the transaction occurred.
        /// </summary>
        DateTime LocalTransactionTime { get; set; }

        ISvDotRequestBuilder GetTransactionRequest(IMerchantInfo merchantInfo, long transactionId);

        Dictionary<string, string> Fields { get; set; }
    }
}