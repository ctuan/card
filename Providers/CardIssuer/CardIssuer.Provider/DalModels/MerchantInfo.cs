﻿using Starbucks.CardIssuer.Dal.Common.Models;

namespace Starbucks.CardIssuer.Provider.DalModels
{
    public class MerchantInfo : IMerchantInfo
    {
        private readonly Common.Models.IMerchantInfo _merchantInfo;

        public MerchantInfo(Provider.Common.Models.IMerchantInfo merchantInfo)
        {
            _merchantInfo = merchantInfo;
        }

        /// <summary>
        /// Indicator that Encryption ID and keys are included.
        /// </summary>
        public bool HasEncryption { get { return _merchantInfo.HasEncryption; } set { _merchantInfo.HasEncryption = value; } }

        /// <summary>
        /// A 4-digit unique ID for the encryption key, 
        /// used when making ValueLink calls so they can identify
        /// the key used for PIN encyryption.
        /// </summary>
        public string EncryptionId { get { return _merchantInfo.EncryptionId; } set { _merchantInfo.EncryptionId = value; } }

        /// <summary>
        /// The ValueLink encryption key
        /// </summary>
        public string Mwk { get { return _merchantInfo.Mwk; } set { _merchantInfo.Mwk = value; } }

        /// <summary>
        /// The extended encryption key
        /// </summary>
        public string Emwk { get { return _merchantInfo.Emwk; } set { _merchantInfo.Emwk = value; } }

        /// <summary>
        /// The ISO 4217 alpha currency code
        /// </summary>
        public string CurrencyCode { get { return _merchantInfo.CurrencyCode; } set { _merchantInfo.CurrencyCode = value; } }

        /// <summary>
        /// The ISO 4217 numeric currency code
        /// </summary>
        public string CurrencyNumber { get { return _merchantInfo.CurrencyNumber; } set { _merchantInfo.CurrencyNumber = value; } }

        /// <summary>
        /// The Merchant ID for ValueLink transactions
        /// </summary>
        public string Mid { get { return _merchantInfo.Mid; } set { _merchantInfo.Mid = value; } }

        /// <summary>
        /// An alternate merchant ID.  This is usually "1" for our purposes.
        /// </summary>
        public string AlternateMid { get { return _merchantInfo.AlternateMid; } set { _merchantInfo.AlternateMid = value; } }

        /// <summary>
        /// Market ID, such as North America - "NoAm"
        /// </summary>
        public string MarketId { get { return _merchantInfo.MarketId; } set { _merchantInfo.MarketId = value; } }

        /// <summary>
        /// The submarket ID, such as "US"
        /// </summary>
        public string SubMarketId { get { return _merchantInfo.SubMarketId; } set { _merchantInfo.SubMarketId = value; } }

        /// <summary>
        /// A ValueLink customer-defined value for the terminal used in 
        /// a transaction.  Usually "0001" for web transactions.
        /// </summary>
        public string TerminalId { get { return _merchantInfo.TerminalId; } set { _merchantInfo.TerminalId = value; } }

        /// <summary>
        /// Merchant key, sometimes referred to as merchant id. This is the string not the numerical value (e.g. starbucks1)
        /// </summary>
        public string MerchantKey { get { return _merchantInfo.MerchantKey; } set { _merchantInfo.MerchantKey = value; } }

        /// <summary>
        /// Key used as password for connecting to CyberSource Soap service
        /// </summary>
        public string TransactionSecurityKey { get { return _merchantInfo.TransactionSecurityKey; } set { _merchantInfo.TransactionSecurityKey = value; } }

        /// <summary>
        /// The fraud threshold for the transaction.
        /// </summary>
        public string FraudThreshold { get { return _merchantInfo.FraudThreshold; } set { _merchantInfo.FraudThreshold = value; } }

    }
}
