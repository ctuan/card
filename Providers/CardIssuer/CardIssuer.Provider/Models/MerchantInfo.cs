﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.CardIssuer.Provider.Common.Models;

namespace Starbucks.CardIssuer.Provider.Models
{
    public class MerchantInfo : IMerchantInfo
    {
        private readonly Dal.Common.Models.IMerchantInfo _merchantInfo;

        public MerchantInfo(Dal.Common.Models.IMerchantInfo merchantInfo)
        {
            _merchantInfo = merchantInfo;
        }

        public bool HasEncryption { get { return _merchantInfo.HasEncryption; } set { _merchantInfo.HasEncryption = value; }}
        public string EncryptionId { get { return _merchantInfo.EncryptionId; } set { _merchantInfo.EncryptionId = value; } }
        public string Mwk { get { return _merchantInfo.Mwk; } set { _merchantInfo.Mwk = value; } }
        public string Emwk { get { return _merchantInfo.Emwk; } set { _merchantInfo.Emwk = value; } }
        public string CurrencyCode { get { return _merchantInfo.CurrencyCode; } set { _merchantInfo.CurrencyCode = value; } }
        public string CurrencyNumber { get { return _merchantInfo.CurrencyNumber; } set { _merchantInfo.CurrencyNumber = value; } }
        public string Mid { get { return _merchantInfo.Mid; } set { _merchantInfo.Mid = value; } }
        public string AlternateMid { get { return _merchantInfo.AlternateMid; } set { _merchantInfo.AlternateMid = value; } }
        public string MarketId { get { return _merchantInfo.MarketId; } set { _merchantInfo.MarketId = value; } }
        public string SubMarketId { get { return _merchantInfo.SubMarketId; } set { _merchantInfo.SubMarketId = value; } }
        public string TerminalId { get { return _merchantInfo.TerminalId; } set { _merchantInfo.TerminalId = value; } }
        public string MerchantKey { get { return _merchantInfo.MerchantKey; } set { _merchantInfo.MerchantKey = value; } }
        public string TransactionSecurityKey { get { return _merchantInfo.TransactionSecurityKey; } set { _merchantInfo.TransactionSecurityKey = value; } }
        public string FraudThreshold { get { return _merchantInfo.FraudThreshold; } set { _merchantInfo.FraudThreshold = value; } }
    }
}
