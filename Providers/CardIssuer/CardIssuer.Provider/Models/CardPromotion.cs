﻿using Starbucks.CardIssuer.Provider.Common.Models;

namespace Starbucks.CardIssuer.Provider.Models
{
    public class CardPromotion : ICardPromotion 
    {
        private readonly Dal.Common.Models.ICardPromotion _cardPromotion;

        public CardPromotion(Dal.Common.Models.ICardPromotion cardPromotion)
        {
            _cardPromotion = cardPromotion;
        }

        public string Code { get { return _cardPromotion.Code; } set { _cardPromotion.Code = value; } }
        public decimal Amount { get { return _cardPromotion.Amount; } set { _cardPromotion.Amount = value; } }
        public string Message { get { return _cardPromotion.Message; } set { _cardPromotion.Message = value; } }
               
    }
}
