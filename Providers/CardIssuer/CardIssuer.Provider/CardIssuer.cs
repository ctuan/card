﻿using System;
using System.Collections.Generic;
using System.Linq;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Provider.Common;
using Starbucks.CardIssuer.Provider.Common.Models;
using Starbucks.CardIssuer.Provider.DalModels;
using Starbucks.CardIssuer.Provider.Models;
using CardTransaction = Starbucks.CardIssuer.Provider.Models.CardTransaction;
using MerchantInfo = Starbucks.CardIssuer.Provider.DalModels.MerchantInfo;

namespace Starbucks.CardIssuer.Provider
{
    public class CardIssuer : ICardIssuer
    {
        private readonly ICardIssuerDal _cardIssuerDal;

        public CardIssuer(ICardIssuerDal cardIssuerDal)
        {
            _cardIssuerDal = cardIssuerDal;
        }

        public ICardTransaction SendMwk(IMerchantInfo merchantInfo)
        {
            var result = _cardIssuerDal.SendMwk(new MerchantInfo(merchantInfo));
            return result == null ? null : new CardTransaction(result);
        }

        public ICardTransaction Activate(IMerchantInfo merchantInfo, string cardNumber, string pin, decimal amount)
        {
            var result = _cardIssuerDal.Activate(new MerchantInfo(merchantInfo), cardNumber, pin, amount);
            return result == null ? null : new CardTransaction(result);
        }

        public ICardTransaction CashOut(IMerchantInfo merchantInfo, string cardNumber, string pin, decimal amount)
        {
            var result = _cardIssuerDal.CashOut(new MerchantInfo(merchantInfo), cardNumber, pin, amount);
            return result == null ? null : new CardTransaction(result);
        }

        public ICardTransaction DisablePin(IMerchantInfo merchantInfo, string cardNumber, string pin)
        {
            var result = _cardIssuerDal.DisablePin(new MerchantInfo(merchantInfo), cardNumber, pin );
            return result == null ? null : new CardTransaction(result);
        }

        public ICardTransaction GetBalance(IMerchantInfo merchantInfo, string cardNumber, string pin)
        {
            var result = _cardIssuerDal.GetBalance(new MerchantInfo(merchantInfo), cardNumber, pin);
            return result == null ? null : new CardTransaction(result);
        }

        public List<ICardTransaction> GetHistory(IMerchantInfo merchantInfo, string cardNumber, string pin)
        {
            var result = _cardIssuerDal.GetHistory(new MerchantInfo(merchantInfo), cardNumber, pin);
            return result == null ? null : result.Select(p => new CardTransaction(p) as ICardTransaction ).ToList()   ;
        }

        public List<ICardTransaction> GetHistoryPinless(IMerchantInfo merchantInfo, string cardNumber)
        {
            var result = _cardIssuerDal.GetHistoryPinless(new MerchantInfo(merchantInfo), cardNumber);
            return result == null ? null : result.Select(p => new CardTransaction(p) as ICardTransaction).ToList();
        }

        public ICardTransaction Reload(IMerchantInfo merchantInfo, string cardNumber, decimal amount)
        {
            var result = _cardIssuerDal.Reload(new MerchantInfo(merchantInfo), cardNumber,  amount);
            return result == null ? null : new CardTransaction(result);
        }

        public ICardTransaction Reload(IMerchantInfo merchantInfo, string cardNumber, string pin, decimal amount)
        {
            var result = _cardIssuerDal.Reload(new MerchantInfo(merchantInfo), cardNumber, pin, amount);
            return result == null ? null : new CardTransaction(result);
        }

        public ICardTransaction Reload(IMerchantInfo merchantInfo, string cardNumber, string pin, decimal amount,
                                       bool isAutoreload)
        {
            var result = _cardIssuerDal.Reload(new MerchantInfo(merchantInfo), cardNumber, pin, amount, isAutoreload );
            return result == null ? null : new CardTransaction(result);
        }

        public ICardTransaction VoidRequest(ICardTransaction voidTransaction, IMerchantInfo merchantInfo)
        {           
            var result = _cardIssuerDal.VoidRequest(new Provider.DalModels.CardTransaction( voidTransaction), new MerchantInfo(merchantInfo));
            return result == null ? null : new CardTransaction(result);
        }

        public ICardTransaction VoidRequest(Enums.CallItCommand command, IMerchantInfo merchInfo, int transactionId)
        {
            var result = _cardIssuerDal.VoidRequest((Dal.Common.Models.Enums.CallItCommand)command, new MerchantInfo(merchInfo), transactionId);
            return result == null ? null : new CardTransaction(result);
        }

        public ICardTransaction Redeem(IMerchantInfo merchantInfo, string cardNumber, string pin, decimal amount)
        {
            var result = _cardIssuerDal.Redeem(new MerchantInfo(merchantInfo), cardNumber, pin, amount);
            return result == null ? null : new CardTransaction(result);
        }

        public ICardTransaction TransferBalance(IMerchantInfo merchantInfo, string cardNumberFrom, string cardNumberTo)
        {
            var result = _cardIssuerDal.TransferBalance(new MerchantInfo(merchantInfo), cardNumberFrom, cardNumberTo);
            return result == null ? null : new CardTransaction(result);
        }

        public ICardTransaction ActivateVirtual(IMerchantInfo merchantInfo, string promoCode, decimal amount)
        {
            var result = _cardIssuerDal.ActivateVirtual(new MerchantInfo(merchantInfo), promoCode,  amount);
            return result == null ? null : new CardTransaction(result);
        }

        //Move to other provider?
        
        public IMerchantInfo GetMerchantInfo(string subMarket)
        {
            var mi = _cardIssuerDal.GetMerchantInfo(subMarket);
            return new  Provider.Models.MerchantInfo(mi) ;
        }

        public long UpsertPartialTransferTransactionDetails(int cardAmountTransactionId, string toCardTransactionId,
                                                           DateTime toCardTransactionDate
                                                           , string fromCardTransactionId,
                                                           DateTime? fromCardTransactionDate)
        {
            return _cardIssuerDal.UpsertPartialTransferTransactionDetails(cardAmountTransactionId, toCardTransactionId,
                                                                          toCardTransactionDate
                                                                          , fromCardTransactionId,
                                                                          fromCardTransactionDate);
        }
    }
}