﻿namespace Starbucks.LogCounter.Provider.Common
{
    public interface ILogCounterManager
    {
        void Increment(string name);
        void Increment(string name, decimal value);
    }

}
