﻿using System;
using System.Collections.Generic;
using Starbucks.CardIssuer.Provider.Common.Models;

namespace Starbucks.CardIssuer.Provider.Common 
{
    public interface ICardIssuer
    {
        /// Sends a call to CallInteractive to save the Merchant Working Key (MWK) - used for PIN encryption -
        ICardTransaction SendMwk(IMerchantInfo merchantInfo);
        /// Activates a card.
        ICardTransaction Activate(IMerchantInfo merchantInfo, string cardNumber, string pin, decimal amount);
        /// Removes remaining balance on card.  For use in lost/stolen scenarios.
        ICardTransaction CashOut(IMerchantInfo merchantInfo, string cardNumber, string PIN, decimal amount);
        /// Disables the card's PIN so it can no longer be used for online transactions.
        ICardTransaction DisablePin(IMerchantInfo merchantInfo, string cardNumber, string PIN);
        /// Sends a call to CallInteractive to retrieve Balance in base currency of card
        ICardTransaction GetBalance(IMerchantInfo merchantInfo, string cardNumber, string PIN);
        /// <summary>
        List<ICardTransaction> GetHistory(IMerchantInfo merchantInfo, string cardNumber, string PIN);
        /// <summary>
        List<ICardTransaction> GetHistoryPinless(IMerchantInfo merchantInfo, string cardNumber);
        /// Reload the card with a given amount.
        ICardTransaction Reload(IMerchantInfo merchantInfo, string cardNumber, decimal amount);
        ICardTransaction Reload(IMerchantInfo merchantInfo, string cardNumber, string PIN, decimal amount);
        ICardTransaction Reload(IMerchantInfo merchantInfo, string cardNumber, string PIN, decimal amount, bool isAutoreload);
        /// Sends a void request to ValueLink.
        ICardTransaction VoidRequest(ICardTransaction voidTransaction, IMerchantInfo merchInfo);
        ICardTransaction VoidRequest(Enums.CallItCommand  command, IMerchantInfo merchInfo, int transactionId);
        /// Redeem an amount from the card.
        ICardTransaction Redeem(IMerchantInfo merchantInfo, string cardNumber, string PIN, decimal amount);
        /// Transfers full balance of one card to another card.  Transaction is PIN-less.
        ICardTransaction TransferBalance(IMerchantInfo merchantInfo, string cardNumberFrom, string cardNumberTo);
        ICardTransaction ActivateVirtual(IMerchantInfo merchantInfo, string promoCode, decimal amount);

        //Move to other provider?
        IMerchantInfo GetMerchantInfo(string subMarket);
        long UpsertPartialTransferTransactionDetails(int cardAmountTransactionId, string toCardTransactionId,
                                                 DateTime toCardTransactionDate
                                                 , string fromCardTransactionId, DateTime? fromCardTransactionDate);


    }



}
