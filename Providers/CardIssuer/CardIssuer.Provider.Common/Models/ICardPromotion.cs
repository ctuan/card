﻿namespace Starbucks.CardIssuer.Provider.Common.Models
{
    public interface ICardPromotion
    {
        string Code { get; set; }
        decimal Amount { get; set; }
        string Message { get; set; }
    }
}
