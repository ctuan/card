﻿using System;
using System.ServiceModel;
using Starbucks.SecurePayPalPayment.Provider.Common;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;
using Starbucks.SecurePayPalPayment.Provider.Configuration;
using Starbucks.SecurePayPalPayment.Provider.ProviderModels;
using Starbucks.SecurePayPalPayment.Provider.ServiceModels;
using Starbucks.ServiceProxies.Extensions;
using Starbucks.ServiceProxies.PayPalPaymentService.Wcf;

namespace Starbucks.SecurePayPalPayment.Provider
{
    public class SecurePayPalPaymentClient : ISecurePayPalPaymentClient
    {
        public ISetupResponse Setup(ISetupRequest setupRequest)
        {
            throw new System.NotImplementedException();
        }

        public void Void(IVoidRequest voidRequest)
        {
            try
            {
                PayPalPaymentService.GetChannelFactory(
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.ServiceConfigurationName,
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.Username,
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.Password)
                                    .Use(c => c.Void(voidRequest.ToServiceModel()));
            }
            catch (FaultException<Starbucks.Platform.Services.Payment.Faults.PayPalFault> e)
            {
                throw e.GetException();
            }
            catch (FaultException e)
            {
                throw e.GetException();
            }
            catch (Exception e)
            {
                throw new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.UnknownCode,
                                                       SecurePayPalPaymentErrorResource.UnknownMessage,
                                                       e);
            }
        }

        public IAuthorizationResponse Auth(IAuthorizationRequest authorizationRequest)
        {
            try
            {
                var response = PayPalPaymentService.GetChannelFactory(
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.ServiceConfigurationName,
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.Username,
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.Password)
                                                   .Use(c => c.Auth(authorizationRequest.ToServiceModel()));

                return response.ToProviderModel();
            }
            catch (FaultException<Starbucks.Platform.Services.Payment.Faults.PayPalFault> e)
            {
                throw e.GetException();
            }
            catch (FaultException e)
            {
                throw e.GetException();
            }
            catch (Exception e)
            {
                throw new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.UnknownCode,
                                                       SecurePayPalPaymentErrorResource.UnknownMessage,
                                                       e);
            }
        }

        public IBillResponse Bill(IBillRequest billRequest)
        {
            try
            {
                var response = PayPalPaymentService.GetChannelFactory(
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.ServiceConfigurationName,
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.Username,
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.Password)
                                                   .Use(c => c.Bill(billRequest.ToServiceModel()));

                return response.ToProviderModel();
            }
            catch (FaultException<Starbucks.Platform.Services.Payment.Faults.PayPalFault> e)
            {
                throw e.GetException();
            }           
            catch (FaultException e)
            {
                throw e.GetException();
            }
            catch (Exception e)
            {
                throw new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.UnknownCode,
                                                       SecurePayPalPaymentErrorResource.UnknownMessage,
                                                       e);
            }
        }

        public IRefundTransactionDetails RefundTransactionInFull(IRefundRequest refundRequest)
        {
            throw new System.NotImplementedException();
        }

        public IRefundTransactionDetails RefundTransactionPartialAmount(IPartialRefundRequest partialRefundRequest)
        {
            throw new System.NotImplementedException();
        }

        public ITransactionDetails GetTransactionDetails(ITransactionDetailsRequest transactionDetailsRequest)
        {
            try
            {
                var response = PayPalPaymentService.GetChannelFactory(
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.ServiceConfigurationName,
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.Username,
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.Password)
                                                   .Use(
                                                       c =>
                                                       c.GetTransactionDetails(
                                                           transactionDetailsRequest.ToServiceModel()));

                return response.ToProviderModel();
            }
            catch (FaultException<Starbucks.Platform.Services.Payment.Faults.PayPalFault> e)
            {
                throw e.GetException();
            }
            catch (FaultException e)
            {
                throw e.GetException();
            }
            catch (Exception e)
            {
                throw new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.UnknownCode,
                                                       SecurePayPalPaymentErrorResource.UnknownMessage,
                                                       e);
            }
        }

        public ITransactionDetails GetExpressCheckoutDetails(
            IExpressCheckoutDetailsRequest expressCheckoutDetailsRequest)
        {
            throw new System.NotImplementedException();
        }

        public ITransactionSearchResults SearchTransactions(ITransactionSearchRequest transactionSearchRequest)
        {
            throw new System.NotImplementedException();
        }

        public string GetOrderIdByPayPalTransactionId(string transactionId)
        {
            throw new System.NotImplementedException();
        }

        public ISetCustomerBillingAgreementResponse SetCustomerBillingAgreement(
            ISetCustomerBillingAgreementRequest setCustomerBillingAgreementRequest)
        {
            throw new System.NotImplementedException();
        }

        public IGetBillingAgreementCustomerDetailsResponse GetBillingAgreementCustomerDetails(
            IGetBillingAgreementCustomerDetailsRequest getBillingAgreementCustomerDetailsRequest)
        {
            throw new System.NotImplementedException();
        }

        public IBillingAgreementUdpateResponse UpdateBillingAgreement(IBillingAgreementUdpateRequest request)
        {
            try
            {
                var response = PayPalPaymentService.GetChannelFactory(
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.ServiceConfigurationName,
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.Username,
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.Password)
                                                   .Use(c => c.UpdateBillingAgreement(request.ToServiceModel()));

                return response.ToProviderModel();
            }
            catch (FaultException<Starbucks.Platform.Services.Payment.Faults.PayPalFault> e)
            {
                throw e.GetException();
            }
            catch (FaultException e)
            {
                throw e.GetException();
            }
            catch (Exception e)
            {
                throw new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.UnknownCode,
                                                       SecurePayPalPaymentErrorResource.UnknownMessage,
                                                       e);
            }
        }

        public ICreateBillingAgreementResponse CreateBillingAgreement(
            ICreateBillingAgreementRequest createBillingAgreementRequest)
        {
            throw new System.NotImplementedException();
        }

        public IDoReferenceTransactionResponse DoReferenceTransaction(
            IDoReferenceTransactionRequest doReferenceTransactionRequest)
        {
            try
            {
                var response = PayPalPaymentService.GetChannelFactory(
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.ServiceConfigurationName,
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.Username,
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.Password)
                                                   .Use(
                                                       c =>
                                                       c.DoReferenceTransaction(
                                                           doReferenceTransactionRequest.ToServiceModel()));

                return response.ToProviderModel();
            }
            catch (FaultException<Starbucks.Platform.Services.Payment.Faults.PayPalFault> e)
            {
                throw e.GetException();
            }
            catch (FaultException e)
            {
                throw e.GetException();
            }
            catch (Exception e)
            {
                throw new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.UnknownCode,
                                                       SecurePayPalPaymentErrorResource.UnknownMessage,
                                                       e);
            }
        }

        public string GetPayPalSubject(string currency)
        {
            try
            {
                var response = PayPalPaymentService.GetChannelFactory(
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.ServiceConfigurationName,
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.Username,
                    SecurePayPalPaymentProviderSettings.Settings.PaymentServiceSettings.Password)
                                                   .Use(
                                                       c =>
                                                       c.GetPayPalSubject(currency));

                return response;
            }
            catch (FaultException<Starbucks.Platform.Services.Payment.Faults.PayPalFault> e)
            {
                throw e.GetException();
            }
            catch (FaultException e)
            {
                throw e.GetException();
            }
            catch (Exception e)
            {
                throw new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.UnknownCode,
                                                       SecurePayPalPaymentErrorResource.UnknownMessage,
                                                       e);
            }
        }
    }
}
