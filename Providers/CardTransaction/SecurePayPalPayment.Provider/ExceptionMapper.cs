﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayPalPayment.Provider.Common;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;


namespace Starbucks.SecurePayPalPayment.Provider
{
    public static class ExceptionMapper
    {
   
        public static SecurePayPalPaymentException GetException(this FaultException faultException)
        {
            string errorCode = string.Empty;
            Type fexType = faultException.GetType();
            if (fexType.IsGenericType && fexType.GetGenericTypeDefinition() == typeof(FaultException<>))
            {
                if (typeof(Starbucks.Platform.Services.Payment.Faults.PayPalFault).IsAssignableFrom(fexType.GetGenericArguments()[0]))
                {
                    var detail = fexType.GetProperty("Detail").GetValue(faultException, null) as Starbucks.Platform.Services.Payment.Faults.PayPalFault;
                    if (detail != null && detail.Errors.Any( ))
                    {
                        errorCode = detail.Errors[0].ErrorCode;
                    }                                           
                }
            }
            else
            {
                errorCode = faultException.Code.Name;
            }

            switch (errorCode)
            {
                case "10004":
                    return new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.TransactionIdInvalidCode,
                                                            SecurePayPalPaymentErrorResource.TransactionIdInvalidMessage,
                                                            faultException);
                case "10201":
                case "10202":
                case "11451":
                case "11457":
                    {
                        return
                            new SecurePayPalPaymentException(
                                SecurePayPalPaymentErrorResource.BillingAgreementInvalidCode,
                                SecurePayPalPaymentErrorResource.BillingAgreementInvalidMessage, faultException);
                    }
                case "10203":
                    return
                        new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.UserActionRequiredCode,
                                                         SecurePayPalPaymentErrorResource.UserActionRequiredMessage,
                                                         faultException);
                case "10204":
                    return
                        new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.AccountClosedCode,
                                                         SecurePayPalPaymentErrorResource.AccountClosedMessage,
                                                         faultException);
                case "10205":
                case "10538":
                case "10539":
                    return
                        new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.RiskCode,
                                                         SecurePayPalPaymentErrorResource.RiskMessage, faultException);
                case "10206":
                    return
                        new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.DuplicateTransactionCode,
                                                         SecurePayPalPaymentErrorResource.DuplicateTransactionMessage,
                                                         faultException);
                case "10445":
                    return
                        new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.TemporaryFailureCode,
                                                         SecurePayPalPaymentErrorResource.TemporaryFailureMessage,
                                                         faultException);
                case "10748":
                case "10504":
                case "18014":
                    return
                        new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.InvalidCCVCode,
                                                         SecurePayPalPaymentErrorResource.InvalidCCVMessage,
                                                         faultException);
                case "10527":
                    return
                        new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.InvalidCreditCardNumberCode,
                                                         SecurePayPalPaymentErrorResource.InvalidCreditCardNumberMessage,
                                                         faultException);
                case "10606":
                    return
                        new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.BuyerCannotPayCode,
                                                         SecurePayPalPaymentErrorResource.BuyerCannotPayMessage,
                                                         faultException);
                case "11084":
                case "10210":
                case "10422":
                    return
                        new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.InvalidFundingSourceCode,
                                                         SecurePayPalPaymentErrorResource.InvalidFundingSourceMessage,
                                                         faultException);
                case "11610":
                    return
                        new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.FraudReviewCode,
                                                         SecurePayPalPaymentErrorResource.FraudReviewMessage,
                                                         faultException);
                case "13109":
                    return
                        new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.BlockedByAmexCode,
                                                         SecurePayPalPaymentErrorResource.BlockedByAmexMessage,
                                                         faultException);
                default:
                    return
                        new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.UnknownCode,
                                                         SecurePayPalPaymentErrorResource.UnknownMessage, faultException);

            }
        }
    }
}
