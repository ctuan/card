﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Starbucks.Platform.Services.Payment;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;
using PaymentActionCode = Starbucks.Platform.Services.Payment.PaymentActionCode;

namespace Starbucks.SecurePayPalPayment.Provider.ServiceModels
{
    public static class ServiceModelConverters
    {
        public static VoidRequest ToServiceModel(this IVoidRequest request)
        {
            if (request == null)
            {
                return null;
            }
            return new VoidRequest()
                {
                    AuthorizationId = request.AuthorizationId,
                    Note = request.NoteField,
                    Subject = request.Subject 
                };
        }

        public static PaymentDetails  ToServiceModel(this IPaymentDetails  paymentDetails )
        {
            if (paymentDetails == null)
            {
                return null;
            }
            return new PaymentDetails
                {
                    Currency = paymentDetails.Currency,
                    Discount = paymentDetails.Discount,
                    LineItems = paymentDetails.LineItems != null ? paymentDetails.LineItems.Select(p => p.ToServiceModel()).ToList() : null,
                    MaxAmount = paymentDetails.MaxAmount,
                    OrderAmount = paymentDetails.OrderAmount,
                    OrderDescription = paymentDetails.OrderDescription,
                    PaymentDate = paymentDetails.PaymentDate,
                    ShippingAmount = paymentDetails.ShippingAmount,
                    TaxAmount = paymentDetails.TaxAmount
                };
        }

        public static PaymentDetailsLineItem ToServiceModel(this IPaymentDetailsLineItem paymentDetailsLineItem )
        {
            if (paymentDetailsLineItem == null)
            {
                return null;
            }
            return new PaymentDetailsLineItem
                {
                    ItemAmount = paymentDetailsLineItem.ItemAmount,
                    ItemCode = paymentDetailsLineItem.ItemCode,
                    ItemDescription = paymentDetailsLineItem.ItemDescription,
                    ItemName = paymentDetailsLineItem.ItemName,
                    Quantity = paymentDetailsLineItem.Quantity
                };
        }

        public static AuthorizationRequest ToServiceModel(this IAuthorizationRequest request)
        {
            if (request == null)
            {
                return null;
            }
            return new AuthorizationRequest
                {
                    OrderId = request.OrderId,
                    PaymentDetails = request.PaymentDetails.ToServiceModel() ,
                    Token = request.Token
                };
        }

        public static BillRequest ToServiceModel(this IBillRequest request)
        {
            if (request == null)
            {
                return null;
            }
            return new BillRequest
                {
                    AuthorizationId = request.AuthorizationId,
                    Note = request.Note,
                    OrderId = request.OrderId,
                    Subject = request.Subject
                };
        }

        public static DoReferenceTransactionRequest ToServiceModel(this IDoReferenceTransactionRequest request)
        {
            if (request == null)
            {
                return null;
            }
            return new DoReferenceTransactionRequest
                {
                   PaymentAction = (PaymentActionCode ) request.PaymentAction ,
                   PaymentDetails = request.PaymentDetails.ToServiceModel() ,
                   ReferenceId = request.ReferenceId ,
                   RequireShippingConfirmation = request.RequireShippingConfirmation ,
                   SoftDescriptor = request.SoftDescriptor ,
                   IPAddress = request.IPAddress,
                   UserId = request.UserId,
                   Locale = request.Locale,
                   ExtensionData = (ExtensionDataObject) request.ExtensionData 
                };
        }

        public static TransactionDetailsRequest ToServiceModel(this ITransactionDetailsRequest request)
        {
            if (request == null)
            {
                return null;
            }
            return new TransactionDetailsRequest
                {
                    Currency = request.Currency,
                    TransactionId = request.TransactionId
                };
        }
        
        public static BillingAgreementUdpateRequest ToServiceModel(this IBillingAgreementUdpateRequest request)
        {
            if (request == null)
            {
                return null;
            }
            return new BillingAgreementUdpateRequest
                {
                    Currency = request.Currency,
                    Custom = request.Custom,
                    Description = request.Description,
                    ReferenceId = request.ReferenceId,
                    Status = BillingAgreementStatus.Active
                };
        }
    }
}
