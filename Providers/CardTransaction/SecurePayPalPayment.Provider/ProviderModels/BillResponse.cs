﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Starbucks.Platform.Services.Payment;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;
using PayPalAckCode = Starbucks.SecurePayPalPayment.Provider.Common.Models.PayPalAckCode;
using PaymentStatusCode = Starbucks.SecurePayPalPayment.Provider.Common.Models.PaymentStatusCode;
using PendingStatusCode = Starbucks.SecurePayPalPayment.Provider.Common.Models.PendingStatusCode;

namespace Starbucks.SecurePayPalPayment.Provider.ProviderModels
{
    public class BillResponse : IBillResponse
    {
        public PaymentStatusCode PaymentStatusCode { get; set; }
        public PendingStatusCode PendingStatusCode { get; set; }
        public string TransactionId { get; set; }

        public IEnumerable< IPayPalError> Errors { get; set; }
        public object ExtensionData { get; set; }
        public PayPalAckCode PayPalAckCode { get; set; }
    }
}
