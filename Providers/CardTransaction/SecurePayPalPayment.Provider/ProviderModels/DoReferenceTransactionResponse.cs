﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;

namespace Starbucks.SecurePayPalPayment.Provider.ProviderModels
{
    public class DoReferenceTransactionResponse : IDoReferenceTransactionResponse
    {
        public string BillingAgreementId { get; set; }
        public ITransactionDetails TransactionDetails { get; set; }
        public IEnumerable<IPayPalError> Errors { get; set; }
        public object ExtensionData { get; set; }
        public PayPalAckCode PayPalAckCode { get; set; }
    }
}
