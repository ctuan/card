﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;

namespace Starbucks.SecurePayPalPayment.Provider.ProviderModels
{
    public static class ProviderModelConverters
    {
        public static IAuthorizationResponse ToProviderModel(
            this Starbucks.Platform.Services.Payment.AuthorizationResponse authorizationResponse)
        {
            if (authorizationResponse == null)
            {
                return null;
            }
            return new AuthorizationResponse
                {
                    PayerInformation = authorizationResponse.Payer.ToProviderModel(),
                    PaymentStatusCode = (PaymentStatusCode) authorizationResponse.PaymentStatus,
                    PendingStatusCode = (PendingStatusCode) authorizationResponse.PendingReason,
                    Subject = authorizationResponse.Subject,
                    TransactionId = authorizationResponse.TransactionId
                };
        }

        public static IPayerInformation ToProviderModel(
            this Starbucks.Platform.Services.Payment.PayerInformation payerInformation)
        {
            if (payerInformation == null)
            {
                return null;
            }
            return new PayerInformation
                {
                    Address = payerInformation.Address.ToProviderModel(),
                    FirstName = payerInformation.FirstName,
                    LastName = payerInformation.LastName,
                    Payer = payerInformation.Payer,
                    PayerId = payerInformation.PayerId
                };
        }

        public static IAddress ToProviderModel(this Starbucks.Platform.Services.Payment.Address address)
        {
            if (address == null)
            {
                return null;
            }
            return new Address
                {
                    AddressId = address.AddressId,
                    City = address.City,
                    Country = address.Country,
                    Name = address.Name,
                    Phone = address.Phone,
                    PostalCode = address.PostalCode,
                    StateOrProvince = address.StateOrProvince,
                    Street1 = address.Street1,
                    Street2 = address.Street2
                };
        }

        public static IBillResponse ToProviderModel(this Platform.Services.Payment.BillResponse billResponse)
        {
            if (billResponse == null)
            {
                return null;
            }
            return new BillResponse
                {
                    PaymentStatusCode = (PaymentStatusCode) billResponse.PaymentStatus,
                    PendingStatusCode = (PendingStatusCode) billResponse.PendingReason,
                    PayPalAckCode = (PayPalAckCode) billResponse.PayPalAckCode,
                    Errors = billResponse.Errors == null ? null : billResponse.Errors.Select(p => p.ToProviderModel()),
                    TransactionId = billResponse.TransactionId

                };
        }

        public static IPayPalError ToProviderModel(this Platform.Services.Payment.PayPalError payPalError)
        {
            if (payPalError == null)
            {
                return null;
            }
            return new PayPalError
                {
                    ErrorCode = payPalError.ErrorCode,
                    ErrorParameters = payPalError.ErrorParameters,
                    IndicatesBuyerAcccountProblem = payPalError.IndicatesBuyerAcccountProblem,
                    LongMessage = payPalError.LongMessage,
                    SeverityCode = (PayPalErrorSeverityCode) payPalError.SeverityCode,
                    ShortMessage = payPalError.ShortMessage
                };
        }

        public static IDoReferenceTransactionResponse ToProviderModel(
            this Platform.Services.Payment.DoReferenceTransactionResponse response)
        {
            if (response == null)
            {
                return null;
            }
            return new DoReferenceTransactionResponse
                {
                    BillingAgreementId = response.BillingAgreementId,
                    TransactionDetails = response.TransactionDetails.ToProviderModel(),
                    Errors = response.Errors == null ? null : response.Errors.Select(p => p.ToProviderModel()),
                    ExtensionData = response.ExtensionData,
                    PayPalAckCode = (PayPalAckCode) response.PayPalAckCode
                };
        }

        public static ITransactionDetails ToProviderModel(
            this Platform.Services.Payment.TransactionDetails transactionDetails)
        {
            if (transactionDetails == null)
            {
                return null;
            }
            return new TransactionDetails
                {
                    BillingAddress = transactionDetails.BillingAddress.ToProviderModel(),
                    OrderId = transactionDetails.OrderId,
                    ParentTransactionId = transactionDetails.ParentTransactionId,
                    PayerInformation = transactionDetails.Payer.ToProviderModel(),
                    PaymentDetails = transactionDetails.PaymentDetails.ToProviderModel(),
                    PaymentStatusCode = (PaymentStatusCode) transactionDetails.PaymentStatus,
                    PendingStatusCode = (PendingStatusCode) transactionDetails.PendingReason,
                    TransactionId = transactionDetails.TransactionId,
                    Subject = transactionDetails.Subject,
                    Errors =
                        transactionDetails.Errors == null
                            ? null
                            : transactionDetails.Errors.Select(p => p.ToProviderModel()),
                    ExtensionData = transactionDetails.ExtensionData,
                    PayPalAckCode = (PayPalAckCode) transactionDetails.PayPalAckCode
                };
        }

        public static IPaymentDetails ToProviderModel(this Platform.Services.Payment.PaymentDetails paymentDetails)
        {
            if (paymentDetails == null)
            {
                return null;
            }
            return new PaymentDetails
                {
                    Currency = paymentDetails.Currency,
                    Discount = paymentDetails.Discount,
                    LineItems = paymentDetails.LineItems != null ? paymentDetails.LineItems.Select(p => p.ToProviderModel()) : null,
                    MaxAmount = paymentDetails.MaxAmount,
                    OrderAmount = paymentDetails.OrderAmount,
                    OrderDescription = paymentDetails.OrderDescription,
                    PaymentDate = paymentDetails.PaymentDate,
                    ShippingAmount = paymentDetails.ShippingAmount,
                    TaxAmount = paymentDetails.TaxAmount,
                };

        }

        public static IPaymentDetailsLineItem ToProviderModel(
            this Platform.Services.Payment.PaymentDetailsLineItem paymentDetailsLineItem)
        {
            if (paymentDetailsLineItem == null)
            {
                return null;
            }
            return new PaymentDetailsLineItem
                {
                    ItemAmount = paymentDetailsLineItem.ItemAmount,
                    ItemCode = paymentDetailsLineItem.ItemCode,
                    ItemDescription = paymentDetailsLineItem.ItemDescription,
                    ItemName = paymentDetailsLineItem.ItemName,
                    Quantity = paymentDetailsLineItem.Quantity
                };
        }

        public static IBillingAgreementUdpateResponse ToProviderModel(
            this Platform.Services.Payment.BillingAgreementUdpateResponse billingAgreementUdpateResponse)
        {
            if (billingAgreementUdpateResponse == null)
            {
                return null;
            }
            return new BillingAgreementUpdateResponse
                {
                    BillingAddress = billingAgreementUdpateResponse.BillingAddress.ToProviderModel(),
                    BillingAgreementId = billingAgreementUdpateResponse.BillingAgreementId,
                    Currency = billingAgreementUdpateResponse.Currency,
                    Custom = billingAgreementUdpateResponse.Custom,
                    Description = billingAgreementUdpateResponse.Description,
                    MaxAmount = billingAgreementUdpateResponse.MaxAmount,
                    PayerInformation = billingAgreementUdpateResponse.PayerInformation.ToProviderModel()
                };
        }
    }
}
