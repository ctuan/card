﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;

namespace Starbucks.SecurePayPalPayment.Provider.ProviderModels
{
    public class BillingAgreementUpdateResponse : IBillingAgreementUdpateResponse
    {
        public IAddress BillingAddress { get; set; }
        public string Description { get; set; }
        public string Custom { get; set; }
        public string BillingAgreementId { get; set; }
        public string Currency { get; set; }
        public decimal? MaxAmount { get; set; }
        public IPayerInformation PayerInformation { get; set; }
    }
}
