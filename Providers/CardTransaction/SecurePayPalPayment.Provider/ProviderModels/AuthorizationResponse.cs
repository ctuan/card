﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;

namespace Starbucks.SecurePayPalPayment.Provider.ProviderModels
{
    public class AuthorizationResponse : IAuthorizationResponse
    {
        public IPayerInformation PayerInformation { get; set; }
        public PaymentStatusCode PaymentStatusCode { get; set; }
        public PendingStatusCode PendingStatusCode { get; set; }
        public string Subject { get; set; }
        public string TransactionId { get; set; }
    }
}
