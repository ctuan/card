﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;

namespace Starbucks.SecurePayPalPayment.Provider.ProviderModels
{
    public class TransactionDetails : ITransactionDetails
    {
        public IAddress BillingAddress { get; set; }
        public string OrderId { get; set; }
        public IPayerInformation PayerInformation { get; set; }
        public IPaymentDetails PaymentDetails { get; set; }
        public PaymentStatusCode PaymentStatusCode { get; set; }
        public PendingStatusCode PendingStatusCode { get; set; }
        public string Subject { get; set; }
        public string TransactionId { get; set; }
        public string ParentTransactionId { get; set; }
        public IEnumerable<IPayPalError> Errors { get; set; }
        public object ExtensionData { get; set; }
        public PayPalAckCode PayPalAckCode { get; set; }
    }
}
