﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;

namespace Starbucks.SecurePayPalPayment.Provider.ProviderModels
{
    public class PayPalError : IPayPalError
    {
        public string ErrorCode { get; set; }
        public StringDictionary ErrorParameters { get; set; }
        public bool IndicatesBuyerAcccountProblem { get; set; }
        public string LongMessage { get; set; }
        public PayPalErrorSeverityCode SeverityCode { get; set; }
        public string ShortMessage { get; set; }
    }
}
