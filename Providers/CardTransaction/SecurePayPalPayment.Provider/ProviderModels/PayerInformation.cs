﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;

namespace Starbucks.SecurePayPalPayment.Provider.ProviderModels
{
    public class PayerInformation : IPayerInformation
    {
        public IAddress Address { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Payer { get; set; }
        public string PayerId { get; set; }
    }
}
