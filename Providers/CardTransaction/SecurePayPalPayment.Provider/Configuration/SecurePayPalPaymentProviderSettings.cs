﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.ServiceProxies.PayPalPaymentService.Wcf.Configuration;

namespace Starbucks.SecurePayPalPayment.Provider.Configuration
{
    public class SecurePayPalPaymentProviderSettings : ConfigurationSection
    {
        private static SecurePayPalPaymentProviderSettings _settings;
        public static SecurePayPalPaymentProviderSettings Settings
        {
            get
            {
                return _settings ??
                       (_settings =
                        ConfigurationManager.GetSection("payPalPaymentProviderSettings") as SecurePayPalPaymentProviderSettings);
            }
        }
        [ConfigurationProperty("payPalPaymentServiceSettings", IsRequired = true)]
        public PayPalPaymentServiceElement  PaymentServiceSettings
        {
            get
            {
                return this["payPalPaymentServiceSettings"] as PayPalPaymentServiceElement;
            }
            set
            {
                this["payPalPaymentServiceSettings"] = value;
            }
        }
    }
}
