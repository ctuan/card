﻿using Starbucks.Commerce.Dal.Common.Models;

namespace Starbucks.Commerce.Dal.Common
{
    public interface ICommerceDal
    {
        IPurchaserAccount GetPurchaserAccount(string userId);
    }
}
