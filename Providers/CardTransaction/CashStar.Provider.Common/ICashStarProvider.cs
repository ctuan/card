﻿using System.Collections.Generic;
using Starbucks.CashStar.Provider.Common.Models;

namespace Starbucks.CashStar.Provider.Common
{
    public interface ICashStarProvider
    {
        IOrder CreateOrder(IBillingInfo billingInfo, IOrder order);
        IOrder CreateOrderForAccount(IBillingInfo billingInfo, IOrder order, string accountName);
        IEnumerable<IMerchant> GetMerchants();
        IEnumerable<IFaceplate> GetFaceplates(string merchantCode);
        IOrder GetOrderByStarbucksOrderId(string orderId);
    }
}
