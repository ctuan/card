﻿using System;

namespace Starbucks.CashStar.Provider.Common
{
    public class CashstarProviderException : Exception 
    {
        public string Code { get; private set; }

        public CashstarProviderException(string code, string message, Exception innerException)
            : base(message, innerException)
        {
            Code = code;
        }

        public CashstarProviderException (string code, string message) : this (code, message, null){}
    }
}
