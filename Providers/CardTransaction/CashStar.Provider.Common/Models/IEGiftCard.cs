namespace Starbucks.CashStar.Provider.Common.Models
{
    public interface IEGiftCard
    {
        string eGiftCardCode { get; set; }
        string eGiftCardNumber { get; set; }
        string AccessCode { get; set; }
        string MerchantCode { get; set; }
        decimal InitialBalance { get; set; }
        decimal CurrentBalance { get; set; }
        string BalanceLastUpdated { get; set; }
        string BalanceUrl { get; set; }
        string Currency { get; set; }
        string Active { get; set; }
        string Status { get; set; }
        string Url { get; set; }
        string Challenge { get; set; }
        string Challenge_Description { get; set; }
        string Challenge_Type { get; set; }
        string FaceplateCode { get; set; }
        string AuditNumber { get; set; }
        string CountryIssued { get; set; }      
        string PromoCode { get; set; }
        string TemplateCode { get; set; }
        string EmailTemplateCode { get; set; }
        IDelivery Delivery { get; set; }
        IMessage Message { get; set; }
        IAdditionalContent AdditionalContent { get; set; }
    }
}