namespace Starbucks.CashStar.Provider.Common.Models
{
    public interface ICreditCard
    {
        string CardType { get; set; }
        string AccountNumber { get; set; }
        string SecurityCode { get; set; }
        string ExpirationDate { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Address { get; set; }
        string Address2 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string PostalCode { get; set; }
        string CountryCode { get; set; }
        string Phone { get; set; }
        string Email { get; set; }
    }
}