namespace Starbucks.CashStar.Provider.Common.Models
{
    public interface IAdditionalContent
    {        
         string Type { get; set; }
         string Content { get; set; }
    }
}