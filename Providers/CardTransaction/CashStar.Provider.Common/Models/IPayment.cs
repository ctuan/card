namespace Starbucks.CashStar.Provider.Common.Models
{
    public interface IPayment
    {
        decimal Amount { get; set; }

        string Currency { get; set; }

        
        IPurchaser Purchaser { get; set; }

        IPurchaserAccount PurchaserAccount { get; set; }

        ICreditCard CreditCard { get; set; }

        IWebSession WebSession { get; set; }

        IMobileAppSession MobileSession { get; set; }
    }
}