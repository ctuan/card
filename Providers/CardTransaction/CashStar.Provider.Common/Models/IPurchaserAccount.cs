namespace Starbucks.CashStar.Provider.Common.Models
{
    public interface IPurchaserAccount
    {
     string UserId { get; set; }
      int AccountAge { get; set; }
      int DaysSinceLastPurchase { get; set; }
      int NumberPurchases { get; set; }
      int NumberDisputes { get; set; }
      int NumberReturns { get; set; }
    }
}