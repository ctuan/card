namespace Starbucks.CashStar.Provider.Common.Models
{
    public interface IPurchaser
    {
       string FirstName { get; set; }
        string LastName { get; set; }
        string Phone { get; set; }
        string Email { get; set; }
    }
}