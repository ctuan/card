﻿namespace Starbucks.CashStar.Provider.Common.Models
{
    public interface  IMessage
    {        
        string From { get; set; }        
        string To { get; set; }        
        string Body { get; set; }
    }
}
