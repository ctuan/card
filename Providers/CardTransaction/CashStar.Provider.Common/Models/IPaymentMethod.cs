﻿using System;

namespace Starbucks.CashStar.Provider.Common.Models
{
    public interface IPaymentMethod
    {
        string PaymentMethodId { get; set; }
        string UserId { get; set; }
        PaymentType PaymentType { get; set; }

        PaymentTypeGroup Group { get; set; }

        string BillingAddressId { get; set; }

        string Nickname { get; set; }

        string FullName { get; set; }
        string AccountNumber { get; set; }

        string FullAccountNumber { get; set; }
        bool IsDefault { get; set; }

        string FirstSix { get; set; }

        string LastFour { get; set; }
        string SurrogateAccountNumber { get; set; }
        bool IsTemporary { get; set; }

        int? ExpirationYear { get; set; }

        int? ExpirationMonth { get; set; }

        string Cvn { get; set; }

        bool IsFraudChecked { get; set; }
        DateTime? FraudCheckedDate { get; set; }

        string RoutingNumber { get; set; }

        string BankName { get; set; }
    }

    public enum PaymentType
    {      
        None,       
        Visa,       
        MasterCard,       
        Amex,       
        Discover,       
        ACH,       
        PayPal,       
        LostUS,       
        LostCAN,       
        LostGB,       
        Costcenter,
    }

    public enum PaymentTypeGroup
    {      
        None,     
        CreditCard,        
        Checking,        
        Lost,        
        Internal,        
        SvcCard,
    }
}
