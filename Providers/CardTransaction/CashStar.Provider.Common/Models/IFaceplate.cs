namespace Starbucks.CashStar.Provider.Common.Models
{
    public interface IFaceplate
    {
        string FaceplateCode { get; set; }
        string Name { get; set; }
        string TextColor { get; set; }
        string ThumbnailImage { get; set; }
        string PreviewImage { get; set; }
        string PrintImage { get; set; }
    }
}