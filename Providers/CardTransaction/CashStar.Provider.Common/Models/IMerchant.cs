namespace Starbucks.CashStar.Provider.Common.Models
{
    public interface IMerchant
    {
       string MerchantCode { get; set; }

       string Name { get; set; }

      string LegalName { get; set; }

       string Description { get; set; }

       string Locations { get; set; }

      string LogoImage { get; set; }
    }
}