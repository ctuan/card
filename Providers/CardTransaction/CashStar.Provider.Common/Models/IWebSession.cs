namespace Starbucks.CashStar.Provider.Common.Models
{
    public interface IWebSession
    {
        string HttpHeaders { get; set; }
        string JavascriptEnabled { get; set; }
        string PurchaserIp { get; set; }
        string PurchaserSessionId { get; set; }
        string PurchaserSessionDuration { get; set; }
        string ThreatMetrixSessionId { get; set; }
        string IovationBlackbox { get; set; }
    }
}