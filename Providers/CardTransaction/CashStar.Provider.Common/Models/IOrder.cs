using System.Collections.Generic;

namespace Starbucks.CashStar.Provider.Common.Models
{
    public interface IOrder
    {
        
         string OrderNumber { get; set; }
        
         string TransactionId { get; set; }
        
         string AuditNumber { get; set; }
        
         string ActivationCallbackUrl { get; set; }
        
         string OriginationLanguage { get; set; }

        
         IEnumerable< IEGiftCard> eGiftCards { get; set; }
        
         IPayment Payment { get; set; }
    }
}