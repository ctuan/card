namespace Starbucks.CashStar.Provider.Common.Models
{
    public interface IDelivery
    {
        string DeliveredBy { get; set; }
        string Method { get; set; }
        string Target { get; set; }
        string Scheduled { get; set; }
        string Delivered { get; set; }
        string Received { get; set; }
        string Status { get; set; }
    }
}