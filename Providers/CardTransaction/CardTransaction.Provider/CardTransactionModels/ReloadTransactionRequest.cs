﻿using System.Collections.Generic;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.SecurePayment.Provider.Common.Models;
using IBillingInfo = Starbucks.SecurePayment.Provider.Common.Models.IBillingInfo;
using IRisk = Starbucks.CardTransaction.Provider.Common.Model.IRisk;

namespace Starbucks.CardTransaction.Provider.CardTransactionModels
{
    internal class ReloadTransactionRequest
    {
        public string UserId;
        public IAssociatedCard Card;
        public string EmailAddress;
        public string OrderId;
        public IMerchantInfo MerchantInfo;
        public IBillingInfo BillingInfo;
        public string Submarket;
        public string IpAddress;
        public decimal Amount;
        public string Platform;
        public IRisk Risk;
        public List<IOrderLineItem> Items;
    }
}
