﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.CardTransaction.Provider.Common.Model;

namespace Starbucks.CardTransaction.Provider.CardTransactionModels
{
    public class CardPromotion : ICardPromotion
    {
        public string Code { get; set; }
        public decimal Amount { get; set; }
        public string Message { get; set; }
    }
}
