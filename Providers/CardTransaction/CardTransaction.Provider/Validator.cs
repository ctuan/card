﻿using Starbucks.CardTransaction.Provider.Common;
using Starbucks.CardTransaction.Provider.Common.ErrorResource;
using Starbucks.CardTransaction.Provider.Common.Model;

namespace Starbucks.CardTransaction.Provider
{
    public class Validator
    {
        public static void ValidateUserId(string userId)
        {
            if (string.IsNullOrEmpty(userId))
                throw new CardTransactionValidationException(CardTransactionValidationErrorResource.InvalidUserIdCode,
                                                             CardTransactionValidationErrorResource.InvalidUserIdMessage);
        }

        public static void ValidateCardId(string cardId)
        {

            if (string.IsNullOrEmpty(cardId))
                throw new CardTransactionValidationException(CardTransactionValidationErrorResource.InvalidCardIdCode,
                                                             CardTransactionValidationErrorResource.InvalidCardIdMessage);
        }

        public static void ValidateCardNumber(string cardNumber)
        {
            if (string.IsNullOrEmpty(cardNumber))
                throw new CardTransactionValidationException(
                    CardTransactionValidationErrorResource.InvalidCardNumberCode,
                    CardTransactionValidationErrorResource.InvalidCardNumberMessage);

            if (cardNumber.Length != 16)
                throw new CardTransactionValidationException(
                    CardTransactionValidationErrorResource.InvalidCardNumberCode,
                    CardTransactionValidationErrorResource.InvalidCardNumberMessage);

        }

        public static void ValidatePin(string pin)
        {
            if (string.IsNullOrEmpty(pin))
                throw new CardTransactionValidationException(CardTransactionValidationErrorResource.InvalidPinCode,
                                                             CardTransactionValidationErrorResource.InvalidPinMessage);

            if (pin.Length != 8)
                throw new CardTransactionValidationException(CardTransactionValidationErrorResource.InvalidPinCode,
                                                             CardTransactionValidationErrorResource.InvalidPinMessage);
        }

        public static void ValidateEmailAddress(string emailAddress)
        {
            if (string.IsNullOrEmpty(emailAddress))
                throw new CardTransactionValidationException(
                    CardTransactionValidationErrorResource.InvalidEmailAddressCode,
                    CardTransactionValidationErrorResource.InvalidEmailAddressMessage);
        }

        public static void ValidateReloadAmount(decimal amount)
        {
            if (amount < 5)
            {
                throw new CardTransactionValidationException(
                    CardTransactionValidationErrorResource.InvalidReloadAmountCode,
                    CardTransactionValidationErrorResource.InvalidReloadAmountMessage);
            }
        }

        internal static void ValidateString(string val, string errorCode, string errorMessage)
        {
            if (string.IsNullOrEmpty(val))
                throw new CardTransactionValidationException(errorCode, errorMessage);
        }

        internal static void ValidateTipAmount(decimal amount)
        {
            if (amount <= 0)
                throw new CardTransactionValidationException(
                    CardTransactionValidationErrorResource.TipAmountInvalidCode,
                    CardTransactionValidationErrorResource.TipAmountInvalidMessage);
        }
        internal static void ValidatePaymentMethod(string paymentMethodId, IPaymentTokenDetails token)
        {
            if (!string.IsNullOrEmpty(paymentMethodId)) { return; }

            if (token != null && !string.IsNullOrEmpty(token.PaymentToken)) { return; }

            throw new CardTransactionValidationException(
                CardTransactionValidationErrorResource.InvalidPaymentMethodIdCode,
                CardTransactionValidationErrorResource.InvalidPaymentMethodIdMessage);

        }

        internal static void ValidatePaymentMethod(string paymentMethodId)
        {
            if (string.IsNullOrEmpty(paymentMethodId))
                throw new CardTransactionValidationException(
                    CardTransactionValidationErrorResource.InvalidPaymentMethodIdCode,
                    CardTransactionValidationErrorResource.InvalidPaymentMethodIdMessage);
        }
    }
}
