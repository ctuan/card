﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Starbucks.Api.Sdk.PaymentService.Common;
using Starbucks.Api.Sdk.PaymentService.Interfaces;
using Starbucks.Api.Sdk.WebRequestWrapper;
using SdkModels = Starbucks.Api.Sdk.PaymentService.Models;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardTransaction.Provider.Common.Model;
using Starbucks.SecurePayPalPayment.Provider.Common;

namespace Starbucks.CardTransaction.Provider
{
    class LogPSApiCallDetails
    {
        internal const string NullDisplay = "<NULL>";

        internal static void LogAuthResult(SdkModels.AuthRequest sdkRequest, IAuthResult result)
        {
            try
            {
                Dictionary<string, object> properties = new Dictionary<string, object>();
                BuildRequestProperties(properties, sdkRequest);
                BuildResponseProperties(properties, result);

                var level = GetLoggingLevel(result);
                PerformLogging("CardTransactionAuthResult", level, "CardApi - ReloadCardCCAuth", properties);
            }
            catch (Exception ex)  // logging exception, we have to eat it
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        internal static void LogBillResult(SdkModels.BillRequest sdkRequest, IBillResult result)
        {
            try
            {
                Dictionary<string, object> properties = new Dictionary<string, object>();
                BuildRequestProperties(properties, sdkRequest);
                BuildResponseProperties(properties, result);

                var level = GetLoggingLevel(result);
                PerformLogging("CardTransactionBillResult", level, "CardApi - ReloadCardCCBill", properties);
            }
            catch (Exception ex)  // logging exception, we have to eat it
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        internal static void LogAuthException(SdkModels.AuthRequest sdkRequest, Exception ex)
        {
            try
            {
                Dictionary<string, object> properties = new Dictionary<string, object>();
                BuildRequestProperties(properties, sdkRequest);
                BuildExceptionProperties(properties, ex);

                PerformLogging("CardTransactionAuthException", TraceEventType.Error, "CardApi - ReloadCardCCAuth", properties);
            }
            catch (Exception err)  // logging exception, we have to eat it
            {
                System.Diagnostics.Debug.WriteLine(err.ToString());
            }
        }

        internal static void LogReloadExceptionCC(SdkModels.AuthRequest sdkRequest, Exception ex)
        {
            try
            {
                Dictionary<string, object> properties = new Dictionary<string, object>();
                BuildRequestProperties(properties, sdkRequest);
                BuildExceptionProperties(properties, ex);

                PerformLogging("CardTransactionReloadValueLinkException", TraceEventType.Error, "CardApi - Reload Exception", properties);
            }
            catch (Exception err)  // logging exception, we have to eat it
            {
                System.Diagnostics.Debug.WriteLine(err.ToString());
            }
        }

        internal static void LogBillException(SdkModels.BillRequest sdkRequest, Exception ex, ICardTransaction cardResult, string message = null)
        {
            try
            {
                Dictionary<string, object> properties = new Dictionary<string, object>();

                if (!string.IsNullOrEmpty(message))
                {
                    properties.Add("LogBillException Message", message.ToDisplay());
                }
                BuildRequestProperties(properties, sdkRequest);

                properties.Add("VL Load result: RequestCode", cardResult.RequestCode.ToDisplay());
                properties.Add("VL Load result: ResponseCode", cardResult.ResponseCode.ToDisplay());

                BuildExceptionProperties(properties, ex);

                PerformLogging("CardTransactionBillException", TraceEventType.Error, "CardApi - ReloadCardCBill", properties);
            }
            catch (Exception err)  // logging exception, we have to eat it
            {
                System.Diagnostics.Debug.WriteLine(err.ToString());
            }
        }

        internal static void LogFraudCheckResult(SdkModels.FraudCheckRequest sdkRequest, IFraudCheckResult result)
        {
            try
            {
                Dictionary<string, object> properties = new Dictionary<string, object>();
                BuildRequestProperties(properties, sdkRequest);
                BuildResponseProperties(properties, result);

                var level = GetLoggingLevel(result);
                PerformLogging("CardTransactionReloadPaypalFraudCheckResult", level, "CardApi - ReloadCardPaypalFraudCheck", properties);
            }
            catch (Exception ex)  // logging exception, we have to eat it
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        internal static void LogFraudCheckException(SdkModels.FraudCheckRequest sdkRequest, Exception ex)
        {
            try
            {
                Dictionary<string, object> properties = new Dictionary<string, object>();
                BuildRequestProperties(properties, sdkRequest);
                BuildExceptionProperties(properties, ex);

                PerformLogging("CardTransactionReloadPaypalFraudCheckException", TraceEventType.Error,
                    "CardApi - ReloadCardPaypalFraudCheck", properties);
            }
            catch (Exception err)  // logging exception, we have to eat it
            {
                System.Diagnostics.Debug.WriteLine(err.ToString());
            }
        }

        internal static void BuildExceptionProperties(Dictionary<string, object> properties, Exception ex)
        {
            properties.Add("Result: hasExceptionObject", (ex != null).ToString());

            if (ex != null)
            {
                properties.Add("Result: Exception", ex.ToString());
                SecurePaymentException secEx = ex as SecurePaymentException;
                if (secEx != null)
                {
                    properties.Add("SecurePaymentException: ApiErrorMessage", secEx.ApiErrorMessage.ToDisplay());
                    properties.Add("SecurePaymentException: ApiErrorCode", secEx.ApiErrorCode.ToDisplay());

                }
                SecurePayPalPaymentException payPalEx = ex as SecurePayPalPaymentException;
                if (payPalEx != null)
                {
                    properties.Add("SecurePayPalPaymentException: Message", payPalEx.Message.ToDisplay());
                    properties.Add("SecurePayPalPaymentException: Code", payPalEx.Code.ToDisplay());
                }
                CardIssuerException cardIssuerEx = ex as CardIssuerException;
                if (cardIssuerEx != null)
                {
                    properties.Add("CardIssuerException: Message", cardIssuerEx.Message.ToDisplay());
                    properties.Add("CardIssuerException: Code", cardIssuerEx.Code.ToDisplay());
                }
                BuildInnerExceptionProperties(properties, ex);
            }
        }

        internal static void BuildInnerExceptionProperties(Dictionary<string, object> properties, Exception ex)
        {
            // todo: bsa. Improve to handle aggregate exceptions.

            properties.Add("Result: hasInnerExceptionObject", (ex != null).ToString());
            if (ex == null) return;

            ApiException apiExInner = ex.InnerException as ApiException;
            if (apiExInner != null)
                    {
                        //properties.Add("Inner ApiException Result: HttpStatus", apiEx.HttpStatus.ToString());
                        properties.Add("Inner ApiException: StatusCode", apiExInner.StatusCode.ToString());
                        properties.Add("Inner ApiException: ApiErrorMessage", apiExInner.ApiErrorMessage.ToDisplay());
                        properties.Add("Inner ApiException: ApiErrorCode", apiExInner.ApiErrorCode.ToDisplay());
                        properties.Add("Inner ApiException: HttpStatus", apiExInner.HttpStatus.ToString().ToDisplay());
                        properties.Add("Inner ApiException: HResult", String.Format("0x{0:X8}", apiExInner.HResult));
                    }
            else
            {
                properties.Add("Inner Exception: Message", ex.Message);
                properties.Add("Inner Exception: type", ex.GetType());
            }
        }

        internal static void BuildRequestProperties(Dictionary<string, object> properties, SdkModels.BillRequest sdkRequest)
        {
            if (sdkRequest != null)
            {
                properties.Add("request: TransactionType", sdkRequest.TransactionType.ToString());
                properties.Add("request: AuthorizationRequestId", sdkRequest.AuthorizationRequestId.ToDisplay());
                properties.Add("request: CurrentTime", DateTime.Now.ToString());
                properties.Add("request: Market", sdkRequest.Market.ToDisplay());
                properties.Add("request: MerchantReferenceCode", sdkRequest.MerchantReferenceCode.ToDisplay());
                properties.Add("request: CurrencyCode", sdkRequest.CurrencyCode.ToDisplay());
                properties.Add("request: UserId", sdkRequest.UserId.ToDisplay());

                BuildRequestProperties(properties, sdkRequest.Basket);
            }
        }


        internal static void BuildRequestProperties(Dictionary<string, object> properties, SdkModels.AuthRequest sdkRequest)
        {
            if (sdkRequest != null)
            {
                properties.Add("request: TransactionType", sdkRequest.TransactionType.ToString());
                properties.Add("request: TransactionDateTime", sdkRequest.TransactionDateTime.ToString());
                properties.Add("request: CurrentTime", DateTime.Now.ToString());
                properties.Add("request: Market", sdkRequest.Market.ToDisplay());
                properties.Add("request: Platform", sdkRequest.Platform.ToDisplay());
                properties.Add("request: CurrencyCode", sdkRequest.CurrencyCode.ToDisplay());
                properties.Add("request: TransactionId(OrderId)", sdkRequest.TransactionId.ToDisplay());

                properties.Add("request: hasRiskObject", (sdkRequest.Risk != null).ToString());
                if (sdkRequest.Risk != null)
                {
                    properties.Add("request: UserId", sdkRequest.Risk.UserId.ToDisplay());
                    properties.Add("request: CCAgentName", sdkRequest.Risk.CCAgentName.ToDisplay());
                    properties.Add("request: SignedIn", sdkRequest.Risk.SignedIn.ToDisplay());

                    properties.Add("request: hasDeviceReputation", (sdkRequest.Risk.DeviceReputation != null).ToString());
                    if (sdkRequest.Risk.DeviceReputation != null)
                    {
                        properties.Add("request: ipAddress", sdkRequest.Risk.DeviceReputation.IpAddress.ToDisplay());
                        properties.Add("request: DeviceFingerprint", sdkRequest.Risk.DeviceReputation.DeviceFingerprint.ToDisplay());
                    }
                    properties.Add("request: CardNumber", sdkRequest.Risk.StarbucksCardDetails.CardNumber.ToDisplay());
                }

                BuildRequestProperties(properties, sdkRequest.BillingInfo);
            }
        }

        internal static void BuildRequestProperties(Dictionary<string, object> properties, SdkModels.FraudCheckRequest sdkRequest)
        {
            properties.Add("request: hasFraudCheckRequestObject", (sdkRequest != null).ToString());
            if (sdkRequest != null)
            {
                properties.Add("request: TransactionType", sdkRequest.TransactionType.ToString());
                properties.Add("request: TransactionDateTime", sdkRequest.TransactionDateTime.ToString());
                properties.Add("request: CurrentTime", DateTime.Now.ToString());
                properties.Add("request: Market", sdkRequest.Market.ToDisplay());
                properties.Add("request: Platform", sdkRequest.Platform.ToDisplay());
                properties.Add("request: CurrencyCode", sdkRequest.CurrencyCode.ToDisplay());

                properties.Add("request: hasRiskObject", (sdkRequest.Risk != null).ToString());
                if (sdkRequest.Risk != null)
                {
                    properties.Add("request: UserId", sdkRequest.Risk.UserId.ToDisplay());
                    properties.Add("request: CCAgentName", sdkRequest.Risk.CCAgentName.ToDisplay());
                    properties.Add("request: SignedIn", sdkRequest.Risk.SignedIn.ToDisplay());

                    properties.Add("request: hasDeviceReputation", (sdkRequest.Risk.DeviceReputation != null).ToString());
                    if (sdkRequest.Risk.DeviceReputation != null)
                    {
                        properties.Add("request: ipAddress", sdkRequest.Risk.DeviceReputation.IpAddress.ToDisplay());
                        properties.Add("request: DeviceFingerprint", sdkRequest.Risk.DeviceReputation.DeviceFingerprint.ToDisplay());
                    }
                }
                BuildRequestProperties(properties, sdkRequest.BillingInfo);
            }
        }

        internal static void BuildRequestProperties(Dictionary<string, object> properties, List<SdkModels.OrderItem> basket)
        {
            properties.Add("request: hasBasketObject", (basket != null).ToString());
            if (basket != null)
            {
                foreach (SdkModels.OrderItem orderitem in basket)
                {
                    properties.Add("request: orderitem Amount", orderitem.Amount);
                    properties.Add("request: orderitem Category", orderitem.Category.ToDisplay());
                    properties.Add("request: orderitem Name", orderitem.Name.ToDisplay());
                    properties.Add("request: orderitem Quantity", orderitem.Quantity);
                    properties.Add("request: orderitem Sku", orderitem.Sku.ToDisplay());
                }
            }
        }

        internal static void BuildRequestProperties(Dictionary<string, object> properties, SdkModels.BillingInfo billing)
        {
            properties.Add("request: hasBillingObject", (billing != null).ToString());
            if (billing != null)
            {
                properties.Add("request: hasBillingAddressInfoObject", (billing.AddressInfo != null).ToString());
                if (billing.AddressInfo != null)
                {
                    properties.Add("request: billingAddressId", billing.AddressInfo.AddressId.ToDisplay());
                    properties.Add("request: billingAddressDetails", (billing.AddressInfo.AddressDetails != null).ToString());
                    if (billing.AddressInfo.AddressDetails != null)
                    {
                        properties.Add("request: billingAddress FirstName", billing.AddressInfo.AddressDetails.FirstName.ToDisplay());
                        properties.Add("request: billingAddress LastName", billing.AddressInfo.AddressDetails.LastName.ToDisplay());
                        properties.Add("request: billingAddress AddressLine1", billing.AddressInfo.AddressDetails.AddressLine1.ToDisplay());
                        properties.Add("request: billingAddress AddressLine2", billing.AddressInfo.AddressDetails.AddressLine2.ToDisplay());
                        properties.Add("request: billingAddress City", billing.AddressInfo.AddressDetails.City.ToDisplay());
                        properties.Add("request: billingAddress CountrySubdivision", billing.AddressInfo.AddressDetails.CountrySubdivision.ToDisplay());
                        properties.Add("request: billingAddress Country", billing.AddressInfo.AddressDetails.Country.ToDisplay());
                        properties.Add("request: billingAddress PostalCode", billing.AddressInfo.AddressDetails.PostalCode.ToDisplay());
                        properties.Add("request: billingAddress PhoneNumber", billing.AddressInfo.AddressDetails.PhoneNumber.ToDisplay());
                    }
                }

                properties.Add("request: hasBillingPaymentMethodObject", (billing.PaymentMethod != null).ToString());

                BuildCreditCardProperties(properties, billing.PaymentMethod.CreditCardDetails);
                BuildPaypalProperties(properties, billing.PaymentMethod.PayPalDetails);
                BuildTokenProperties(properties, billing.PaymentMethod.TokenDetails);

            }
        }

        internal static void BuildTokenProperties(Dictionary<string, object> properties, SdkModels.TokenDetails token)
        {
            properties.Add("request: hasBilling Token Object", (token != null).ToString());

            if (token != null)
            {
                properties.Add("request [TokenDetails]: TokenType ", token.PaymentTokenType.ToDisplay());
                properties.Add("request [TokenDetails]: has Token string", (!string.IsNullOrEmpty(token.PaymentToken)).ToString());

                properties.Add("request [TokenDetails]: hasExpireMonth ", token.ExpireMonth.HasValue.ToString());
                properties.Add("request [TokenDetails]: hasExpireYear ", token.ExpireYear.HasValue.ToString());
                properties.Add("request [TokenDetails]: Indicator ", token.Indicator.ToDisplay());
                properties.Add("request [TokenDetails]: hasPaymentCryptogram ", (!string.IsNullOrEmpty(token.PaymentCryptogram)).ToString());

                properties.Add("request [TokenDetails]: POS Detail ", (token.POSDetails != null).ToString());
                if (token.POSDetails != null)
                {
                    properties.Add("request [TokenDetails.POSDetail]: hasCombinedTags ", (!string.IsNullOrEmpty(token.POSDetails.CombinedTags)).ToString());
                    properties.Add("request [TokenDetails.POSDetail]: hasTrackData ", (!string.IsNullOrEmpty(token.POSDetails.TrackData)).ToString());
                    properties.Add("request [TokenDetails.POSDetail]: TransactionReferenceKey ", token.POSDetails.TransactionReferenceKey.ToDisplay());
                    properties.Add("request [TokenDetails.POSDetail]: TerminalId ", token.POSDetails.TerminalId.ToDisplay());
                }
            }
        }

        internal static void BuildCreditCardProperties(Dictionary<string, object> properties, SdkModels.CreditCardDetails cc)
        {
            properties.Add("request: hasBilling CC Object", (cc != null).ToString());

            if (cc != null)
            {
                BuildCreditCardBaseProperties(properties, cc, "CC");
                properties.Add("request: CC hasCvn", (!string.IsNullOrEmpty(cc.Cvn)).ToString());
                properties.Add("request: CC hasSurrogateNumber", (!string.IsNullOrEmpty(cc.SurrogateNumber)).ToString());
            }
        }

        internal static void BuildCreditCardBaseProperties(Dictionary<string, object> properties, SdkModels.CreditCardDetailsBase cc, string type)
        {
            if (cc != null)
            {
                properties.Add(string.Format("request: {0} Type", type), cc.Type.ToString());
                properties.Add(string.Format("request: {0} hasExpireYear", type), (cc.ExpireYear != null).ToString());
                properties.Add(string.Format("request: {0} hasExpireMonth", type), (cc.ExpireMonth != null).ToString());
                properties.Add(string.Format("request: {0} hasFullName", type), (!string.IsNullOrEmpty(cc.FullName)).ToString());
            }
        }
        internal static void BuildPaypalProperties(Dictionary<string, object> properties, SdkModels.PayPalDetails pp)
        {
            properties.Add("request: hasBilling Paypal Object", (pp != null).ToString());
            if (pp != null)
            {
                properties.Add("request: has Paypal BillAgreementId", (!string.IsNullOrEmpty(pp.BillAgreementId)).ToString());
                properties.Add("request: Paypal PayPalPayerId", pp.PayPalPayerId.ToDisplay());
                properties.Add("request: Paypal PayPayEmail", pp.PayPayEmail.ToDisplay());
                properties.Add("request: Paypal PayPayFullName", pp.PayPayFullName.ToDisplay());
                properties.Add("request: Paypal PendingReason", pp.PendingReason.ToDisplay());
            }
        }

        internal static void BuildResponseProperties(Dictionary<string, object> properties, IBillResult result)
        {
            BuildResponseProperties(properties, result as IFraudCheckResult);
            properties.Add("Result: hasBillResultInfo", (result != null).ToString());
            if (result != null)
            {
                properties.Add("Result: DateTime", result.DateTime);
                properties.Add("Result: Amount", result.Amount);
                properties.Add("Result: MerchantReferenceCode", result.MerchantReferenceCode.ToDisplay());
                properties.Add("Result: Decision", result.Decision.ToString());
                properties.Add("Result: ReasonCode", result.ReasonCode.ToString());
                properties.Add("Result: ReconciliationId", result.ReconciliationId.ToDisplay());
                properties.Add("Result: RequestId", result.RequestId.ToDisplay());
            }
        }

        internal static void BuildResponseProperties(Dictionary<string, object> properties, IAuthResult result)
        {
            BuildResponseProperties(properties, result as IFraudCheckResult);
            properties.Add("Result: hasCreditCardAuthInfo", (result != null && result.CreditCardAuthorizationDetails != null).ToString());
            if (result != null && result.CreditCardAuthorizationDetails != null)
            {
                var cc = result.CreditCardAuthorizationDetails;
                properties.Add("Result: Native AuthorizationDateTime", cc.AuthorizationDateTime);
                properties.Add("Result: Native AuthorizedAmount", cc.AuthorizedAmount);
                properties.Add("Result: Native MerchantReferenceCode", cc.MerchantReferenceCode.ToDisplay());
                properties.Add("Result: Native AVCode", cc.AVCode.ToDisplay());
                properties.Add("Result: Native CVCode", cc.CVCode.ToDisplay());
                properties.Add("Result: Native RequestId", cc.RequestId.ToDisplay());
                properties.Add("Result: Native ReasonCode", cc.ReasonCode != null ? cc.ReasonCode.Value.ToString() : NullDisplay);
                properties.Add("Result: Native Decision", cc.Decision.ToDisplay());
            }
        }

        internal static void BuildResponseProperties(Dictionary<string, object> properties, IFraudCheckResult result)
        {
            properties.Add("Result: hasResultObject", (result != null).ToString());
            if (result != null)
            {
                properties.Add("Result: DecisionType", result.DecisionType.ToString());
                properties.Add("Result: DecisionReasonCode", result.DecisionReason == null ? NullDisplay : result.DecisionReason.Code.ToString());
                properties.Add("Result: DecisionReasonMessage", result.DecisionReason == null ? NullDisplay : result.DecisionReason.Description.ToDisplay());
            }
        }

        internal static TraceEventType GetLoggingLevel(IBillResult result)
        {
            if (result == null)
            {
                return TraceEventType.Warning;
            }

            if (result.Decision != SdkModels.DecisionType.Accept)
            {
                return TraceEventType.Warning;
            }

            return TraceEventType.Information;
        }

        internal static TraceEventType GetLoggingLevel(IFraudCheckResult result)
        {
            if (result == null)
            {
                return TraceEventType.Warning;
            }

            if (result.DecisionType != SdkModels.DecisionType.Accept)
            {
                return TraceEventType.Warning;
            }

            return TraceEventType.Information;
        }

        internal static void PerformLogging(string message, TraceEventType severity, string title, Dictionary<string, object> properties)
        {
            var logEntry = new LogEntry();
            logEntry.Categories = new string[] { "APILog" };
            logEntry.Title = title;
            logEntry.Severity = severity;
            logEntry.Message = message;
            logEntry.ExtendedProperties = properties;
            Logger.Write(logEntry);
        }
    }

    public static class Extionsions
    {
        public static void EnsureObject(this object input, string message)
        {
            if (input == null)
            {
                throw new ArgumentNullException(message);
            }
        }

        public static string ToDisplay(this string value)
        {
            if (value == null)
            {
                return LogPSApiCallDetails.NullDisplay;
            }

            return value;
        }
    }

}
