﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayment.Provider.Common.Models;

namespace Starbucks.CardTransaction.Provider.PaymentModels
{
    public class BillingInfo : IBillingInfo
    {        
        public IUserAddress Address { get; set; }
        public IPaymentMethod PaymentMethod { get; set; }
        public string AuthCode { get; set; }
        public DateTime AuthDate { get; set; }
        public string AuthRequestId { get; set; }
        public string EmailAddress { get; set; }
        public string Currency { get; set; }
        public string CurrencyNumericCode { get; set; }
        public string County { get; set; }
        public string PayPalTransactionId { get; set; }
        public string PayPalExpressCheckoutToken { get; set; }
        public string PayPalPayerId { get; set; }
        public string PayPalEmailAddress { get; set; }
        public string PayPalSubject { get; set; }
        public bool IsPayPalBillFailed { get; set; }
       
    }
}
