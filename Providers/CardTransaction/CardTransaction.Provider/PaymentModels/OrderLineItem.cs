﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayment.Provider.Common.Models;

namespace Starbucks.CardTransaction.Provider.PaymentModels
{
    public class OrderLineItem : IOrderLineItem
    {
        public IProduct ProductOrdered { get; set; }
        public int Quantity { get; set; }
        public string OrderType { get; set; }
        public decimal TotalAmount { get; private set; }//TODO: Implement get if needed since this is a derived field
        public decimal Subtotal { get; private set; }//TODO: Implement get if needed since this is a derived field
        public decimal LineItemTotal { get; private set; }//TODO: Implement get if needed since this is a derived field
        public decimal Tax { get; set; }
        public decimal CityTax { get; set; }
        public decimal StateTax { get; set; }
        public decimal DistrictTax { get; set; }
        public decimal CountyTax { get; set; }
        public decimal ShippingCost { get; set; }
        public IShipping Shipping { get; set; }
        public string CustomCardImageId { get; set; }
        public string CustomCardTheme { get; set; }
        public decimal UnitPrice { get; set; }
        public IEGiftNotificationLineItem EGift { get; set; }
        public string[] GiftMessages { get; set; }
        public string ParentLineItemId { get; set; }
        public string MarketingMessage { get; set; }
    }
}
