﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayment.Provider.Common.Models;

namespace Starbucks.CardTransaction.Provider.PaymentModels
{
    public class PaymentMethod : IPaymentMethod
    {
        public string PaymentMethodId { get; set; }
        public string PaymentType { get; set; }
        public string FullName { get; set; }
        public string BillingAddressId { get; set; }
        public string AccountNumber { get; set; }
        public int? ExpirationMonth { get; set; }
        public int? ExpirationYear { get; set; }
        public string Cvn { get; set; }
        public bool IsDefault { get; set; }
        public string Nickname { get; set; }
        public string AccountNumberLastFour { get; set; }
        public bool IsTemporary { get; set; }
        public string RoutingNumber { get; set; }
        public string BankName { get; set; }
        public string UserId { get; set; }
        public bool IsFraudChecked { get; set; }
        public string SurrogateAccountNumber { get; set; }
        public int PaymentTypeId { get; set; }
        public DateTime? FraudCheckedDate { get; set; }
    }

    
}
