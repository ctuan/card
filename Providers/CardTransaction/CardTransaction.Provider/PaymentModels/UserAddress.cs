﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayment.Provider.Common.Models;
using UserAddressVerificationLevel = Starbucks.SecurePayment.Provider.Common.Models.UserAddressVerificationLevel;
using UserAddressVerificationStatus = Starbucks.SecurePayment.Provider.Common.Models.UserAddressVerificationStatus;

namespace Starbucks.CardTransaction.Provider.PaymentModels
{
   public class UserAddress : IUserAddress
    {
       public UserAddressType AddressType { get; set; }
       public string AddressId { get; set; }
       public string UserId { get; set; }
       public string AddressName { get; set; }
       public string FirstName { get; set; }
       public string LastName { get; set; }
       public string PhoneNumber { get; set; }
       public string PhoneExtension { get; set; }
       public string RegionDescription { get; set; }
       public bool IsTemporary { get; set; }
       public UserAddressVerificationStatus VerificationStatus { get; set; }
       public UserAddressVerificationLevel VerificationLevel { get; set; }
       public List<IUserAddress> VerificationSearchResults { get; set; }
       public string EmailAddress { get; set; }
       public decimal ShippingTotal { get; set; }
       public string CompanyName { get; set; }
       public string AddressLine1 { get; set; }
       public string AddressLine2 { get; set; }
       public string City { get; set; }
       public string CountrySubdivision { get; set; }
       public string CountrySubdivisionDescription { get; set; }
       public string PostalCode { get; set; }
       public string Country { get; set; }
       public string CountryName { get; set; }
    }    
}
