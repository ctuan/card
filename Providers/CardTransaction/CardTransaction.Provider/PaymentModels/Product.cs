﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayment.Provider.Common.Models;

namespace Starbucks.CardTransaction.Provider.PaymentModels
{
    public class Product : IProduct
    {
        public string ProductDescription { get; set; }
        public string ProductID { get; set; }
        public string ProductVariantID { get; set; }
        public string JDAItemNumber { get; set; }
        public decimal ItemAmount { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string SKU { get; set; }
        public string GiftCategory { get; set; }
        public string TimeCategory { get; set; }
        public string HostHedge { get; set; }
        public string TimeHedge { get; set; }
        public string VelocityHedge { get; set; }
        public string Threshold { get; set; }
        public int ProductTypeId { get; set; }
        public string Locale { get; set; }
        public string Site { get; set; }
    }
}
