﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Logging;

public enum CardLogMessageCategory
{
    PinUpdate = 0,
    Reload = 1,
    BalanceTransfer = 2,
    UserPreferenceUpdates = 3,
    RegisterCard = 4,
    RegisterUser = 5,
    RecoverLostStolen = 6,
}

namespace Starbucks.CardTransaction.Provider
{
    public class LogBase
    {
        public string UserId { get; set; }

        public string CardNumber { get; set; }
        
        public string Message { get; set; }

        public string Submarket { get; set; }
        
        public IDictionary<string, string> ParameterCollection { get; set; }
    }

    public class CardLog : LogBase
    {    
        public string CardId { get; set; }
    
        public string CardName { get; set; }
    
        public decimal Amount { get; set; }
    
        public string TransactionId { get; set; }
    }

    public class LogHelper
    {
        public static void Log(CardLog log, CardLogMessageCategory cardMessageCategory)
        {
            LogMessage(log, System.Diagnostics.TraceEventType.Information, cardMessageCategory);
        }

        public static void Log(string userId, string message, IDictionary<string, string> parameterCollection, CardLogMessageCategory cardMessageCategory)
        {
            LogMessage(new CardLog { UserId = userId, CardId = string.Empty, CardName = string.Empty, CardNumber = string.Empty, Amount = 0, Submarket = string.Empty, Message = message, TransactionId = string.Empty, ParameterCollection = parameterCollection }, System.Diagnostics.TraceEventType.Information, cardMessageCategory);
        }

        public static void Log(string userId, string cardId, string cardName, string cardNumber, decimal amount, string submarket, string message, IDictionary<string, string> parameterCollection, string transactionId, CardLogMessageCategory cardMessageCategory)
        {
            LogMessage(new CardLog { UserId = userId, CardId = cardId, CardName = cardName, CardNumber = cardNumber, Amount = amount, Submarket = submarket, Message = message, TransactionId = transactionId, ParameterCollection = parameterCollection }, System.Diagnostics.TraceEventType.Information, cardMessageCategory);
        }

        private static void LogMessage(CardLog cardLog, System.Diagnostics.TraceEventType traceEventType, CardLogMessageCategory cardMessageCategory)
        {
            var logEntry = new LogEntry();
            logEntry.ActivityId = Guid.NewGuid();
            logEntry.Title = "CardService";
            logEntry.Severity = traceEventType;
            logEntry.Message = cardLog.Message;
            logEntry.ExtendedProperties.Add("__Type", "CardError");
            logEntry.ExtendedProperties.Add("UserId", cardLog.UserId);
            logEntry.ExtendedProperties.Add("CardId", cardLog.CardId);
            logEntry.ExtendedProperties.Add("CardNumber", cardLog.CardNumber);
            logEntry.ExtendedProperties.Add("LogMessageCategory", cardMessageCategory.ToString());
            logEntry.ExtendedProperties.Add("CardLogJson", cardLog.ToJson());
            logEntry.ExtendedProperties.Add("CardLogXml", cardLog.ToXml());
            
            if (cardLog.ParameterCollection != null)
            {
                foreach (KeyValuePair<string, string> item in cardLog.ParameterCollection)
                {
                    logEntry.ExtendedProperties.Add(item.Key, item.Value);
                }
            }

            Logger.Write(logEntry);
        }

        public static void LogException(Exception ex, string message, string title, System.Diagnostics.TraceEventType eventType)
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            LogPSApiCallDetails.BuildExceptionProperties(properties, ex);
            LogPSApiCallDetails.PerformLogging(message, eventType, title, properties);
        }

        public static void Log(string errorMessage, string title, System.Diagnostics.TraceEventType eventType)
        {
            var logEntry = new LogEntry();
            logEntry.ActivityId = Guid.NewGuid();
            logEntry.Categories = new string[] { "APILog" };
            logEntry.Title = title;
            logEntry.Severity = eventType;
            logEntry.Message = errorMessage;
            Logger.Write(logEntry);
        }
    }
    internal class SerializationHelper<T>
    {
        public string ToJson(T serializable)
        {
            string objectAsJson = string.Empty;

            // Write object to memory stream
            using (MemoryStream meomryStream = new MemoryStream())
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
                serializer.WriteObject(meomryStream, serializable);

                // Extract JSON representation
                meomryStream.Position = 0;
                StreamReader reader = new StreamReader(meomryStream);
                objectAsJson = reader.ReadToEnd();
            }
            return objectAsJson;
        }


        public T FromJson(string objectAsJson)
        {
            T deserialized = default(T);

            using (MemoryStream memoryStream = new MemoryStream(Encoding.Unicode.GetBytes(objectAsJson)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
                deserialized = (T)serializer.ReadObject(memoryStream);
            }

            return deserialized;
        }

        public string ToXml(T serializable)
        {
            string objectAsXml = string.Empty;

            // Write object to memory stream
            using (MemoryStream meomryStream = new MemoryStream())
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(T));
                serializer.WriteObject(meomryStream, serializable);

                meomryStream.Position = 0;
                StreamReader reader = new StreamReader(meomryStream);
                objectAsXml = reader.ReadToEnd();
            }
            return objectAsXml;
        }

        public T FromXml(string objectAsXml)
        {
            T deserialized = default(T);

            using (MemoryStream memoryStream = new MemoryStream(Encoding.Unicode.GetBytes(objectAsXml)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
                deserialized = (T)serializer.ReadObject(memoryStream);
            }

            return deserialized;
        }
    }

    public static class SerializationExtensions
    {
        public static string ToJson<T>(this T parameters)
        {
            SerializationHelper<T> serializer = new SerializationHelper<T>();
            string jsonValue = serializer.ToJson(parameters);
            return jsonValue;
        }

        public static T FromJson<T>(this string jSonString)
        {
            SerializationHelper<T> serializer = new SerializationHelper<T>();
            T myObject = serializer.FromJson(jSonString);
            return myObject;
        }

        public static string ToXml<T>(this T parameters)
        {
            SerializationHelper<T> serializer = new SerializationHelper<T>();
            string xmlValue = serializer.ToXml(parameters);
            return xmlValue;
        }
    }

    public class LogMessages
    {
        public const string Pin_Updated = "Pin updated";
        public const string Card_Reload = "Card reload";
        public const string Paypal_Reload = "Paypal reload";
        public const string Paypal_RemoveReload = "Remove paypal auto reload";
        public const string Remove_Reload = "Remove auto reload";
        public const string Remove_GiftReload = "Remove gift reload";
        public const string Enable_AutoReload = "Enable auto reload";
        public const string Disable_Autoreload = "Disable auto reload";
        public const string Save_AutoReload = "Save auto reload";
        public const string Suspend_AutoReload = "Suspend auto reload";
        public const string AutoReloadBilling_Updated = "Auto reload billing updated";
        public const string GiftReloadBilling_Updated = "Gift reload billing updated";
        public const string Save_GiftReload = "Save gift reload";
        public const string GiftReload_Update = "Gift reload updated";

        public const string Promo_Reload = "Promotion reload";
        public const string Auto_Reload_By_CreditCard = "Auto reload by creditCard";
        public const string Balance_Transfer = "Balance transfer";
        public const string Card_Unregister = "Card unregistered";
        public const string Card_Activation = "Activated digital card";
        public const string Card_Register = "Card registered";
        public const string Card_RecoverLostStolen = "Recover lost stolen";
        public const string Card_ReportZeroLostStolen = "Report lost stolen with zero balance";
    }
}
