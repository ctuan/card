﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Starbucks.Api.Sdk.PaymentService.Models;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.CardTransaction.Provider.Common.Model;
using Starbucks.ChasePayConfirmation.QueueService.Common.Commands;
using Starbucks.Platform.Bom.Shared;
using Starbucks.SecurePayment.Provider.Common.Models;
using BillingInfo = Starbucks.Platform.Bom.BillingInfo;
using CardPromotion = Starbucks.CardTransaction.Provider.CardTransactionModels.CardPromotion;
using DeliveryStatus = Starbucks.Platform.Bom.Shared.eGiftEntities.DeliveryStatus;
using IBillingInfo = Starbucks.SecurePayment.Provider.Common.Models.IBillingInfo;
using ICardPromotion = Starbucks.CardIssuer.Dal.Common.Models.ICardPromotion;
using ICardTransaction = Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction;
using IRisk = Starbucks.CardTransaction.Provider.Common.Model.IRisk;
using OrderLineItem = Starbucks.Platform.Bom.OrderLineItem;
using UserAddress = Starbucks.CardTransaction.Provider.PaymentModels.UserAddress;
using UserAddressType = Starbucks.SecurePayment.Provider.Common.Models.UserAddressType;
using StarbucksAccountProvider = Account.Provider;

namespace Starbucks.CardTransaction.Provider
{
    public static class Converters
    {
        public static IUserAddress ToSecurePaymentUserAddress(this StarbucksAccountProvider.Common.Models.IAddress address)
        {
            if (address == null)
            {
                return null;
            }

            return new UserAddress()
                {
                    AddressId = address.AddressId ,
                    AddressLine1 = address.Line1 ,
                    AddressLine2 = address.Line2 ,
                    AddressName = address.Nickname ,
                    AddressType = address.AddressType != null ? (UserAddressType)Enum.Parse(typeof(UserAddressType), address.AddressType, true): UserAddressType.Billing ,
                    City = address.City ,
                    CompanyName = address.CompanyName ,
                    Country = address.Country ,
                    CountryName = "",
                    CountrySubdivision = address.CountrySubdivision ,
                    CountrySubdivisionDescription = "",
                    EmailAddress = address.EmailAddress ,
                    FirstName = address.FirstName,
                    IsTemporary = address.IsTemporary ,
                    LastName = address.LastName,
                    PhoneExtension = address.PhoneExtension ,
                    PhoneNumber = address.PhoneNumber ,
                    PostalCode = address.PostalCode ,
                    RegionDescription = address.RegionDescription ,
                    ShippingTotal = address.ShippingTotal ,
                };
        }

        public static StarbucksAccountProvider.Common.Models.IAddress ToBillingAddress (this Common.Model.IAddress address)
        {
            if (address == null)
            {
                return null;
            }

            return new StarbucksAccountProvider.Models.Address()
            {
                Line1 = address.AddressLine1,
                Line2 = address.AddressLine2,
                City = address.City,
                Country = address.Country,
                CountrySubdivision = address.CountrySubdivision,
                FirstName = address.FirstName,
                LastName = address.LastName,
                PhoneNumber = address.PhoneNumber,
                PostalCode = address.PostalCode,
            };

        }

        public static IPaymentMethod ToSecurePaymentPaymentMethod(this PaymentMethod.Provider.Common.Models.IPaymentMethod paymentMethod)
        {
            if (paymentMethod == null)
            {
                return null;
            }

            return new PaymentModels.PaymentMethod()
                {
                    AccountNumber = paymentMethod.AccountNumber,
                    AccountNumberLastFour = paymentMethod.AccountNumberLastFour,
                    BankName = paymentMethod.BankName,
                    BillingAddressId = paymentMethod.BillingAddressId,
                    Cvn = paymentMethod.Cvn,
                    ExpirationMonth = paymentMethod.ExpirationMonth,
                    ExpirationYear = paymentMethod.ExpirationYear,
                    FullName = paymentMethod.FullName,
                    IsDefault = paymentMethod.IsDefault,
                    IsFraudChecked = paymentMethod.IsFraudChecked,
                    IsTemporary = paymentMethod.IsTemporary,
                    Nickname = paymentMethod.Nickname,
                    PaymentMethodId = paymentMethod.PaymentMethodId,
                    PaymentType = paymentMethod.PaymentType,
                    PaymentTypeId = paymentMethod.PaymentTypeId ,
                    RoutingNumber = paymentMethod.RoutingNumber,
                    SurrogateAccountNumber = paymentMethod.SurrogateAccountNumber,
                    FraudCheckedDate = paymentMethod.FraudCheckedDate
                };
        }

        public static Api.Sdk.PaymentService.Models.PaymentMethod ToPaymentServiceApiSdkPaymentTokenForPaymentToken(this IPaymentMethod paymentMethod, IPaymentTokenDetails paymentToken)
        {
            if (paymentMethod == null || paymentToken == null)
            {
                return null;
            }

            return new Api.Sdk.PaymentService.Models.PaymentMethod()
            {
                TokenDetails = new TokenDetails()
                {
                    PaymentToken = paymentToken.PaymentToken,
                    PaymentTokenType = paymentToken.PaymentTokenType.ToString(),

                    PaymentCryptogram = paymentToken.PaymentCryptogram,
                    ExpireMonth = paymentToken.ExpirationMonth,
                    ExpireYear = paymentToken.ExpirationYear,
                    Indicator = paymentToken.ECIndicator,
                    TokenRequestorId = paymentToken.TokenRequestorId,

                    POSDetails = (paymentToken.MobilePOS == null) ? null :
                                    new MobilePOS() 
                                    {
                                        CombinedTags = paymentToken.MobilePOS.CombinedTags,
                                        TerminalId = paymentToken.MobilePOS.TerminalId,
                                        TrackData = paymentToken.MobilePOS.TrackData,
                                        TransactionReferenceKey = paymentToken.MobilePOS.TransactionReferenceKey
                                    }
                }
            };
        }

        public static Api.Sdk.PaymentService.Models.PaymentMethod ToPaymentServiceApiSdkPaymentMethod(this IPaymentMethod paymentMethod)
        {
            if (paymentMethod == null)
            {
                return null;
            }

            CreditCardTypes cardType;
            Enum.TryParse(paymentMethod.PaymentType, out cardType); // todo: if this fails, throw a bad request except.

            return new Api.Sdk.PaymentService.Models.PaymentMethod()
            {
                CreditCardDetails = new CreditCardDetails()
                {
                    Type = GetCreditCardType(paymentMethod.PaymentTypeId),     
                    Cvn = paymentMethod.Cvn,
                    SurrogateNumber = paymentMethod.SurrogateAccountNumber,
                    ExpireMonth = paymentMethod.ExpirationMonth,
                    ExpireYear = paymentMethod.ExpirationYear,
                    FullName = paymentMethod.FullName
                },
                PaymentId = paymentMethod.PaymentMethodId,
            };
        }

        private static CreditCardTypes GetCreditCardType(int paymentTypeId)
        {
            switch (paymentTypeId)
            {
                case 0:
                    return CreditCardTypes.None;

                case 1:
                    return CreditCardTypes.Visa;

                case 2:
                    return CreditCardTypes.MasterCard;

                case 3:
                    return CreditCardTypes.Amex;

                case 4:
                    return CreditCardTypes.Discover;

                default:
                    return CreditCardTypes.None;
            }
        }

        public static AddressInfo ToPaymentServiceApiSdkAddress(this IUserAddress userAddress)
        {
            return userAddress == null
                ? null
                : new AddressInfo()
                {
                    AddressId = userAddress.AddressId,
                    AddressDetails = new Api.Sdk.PaymentService.Models.Address()
                    {
                        FirstName = userAddress.FirstName,
                        LastName = userAddress.LastName,
                        AddressLine1 = userAddress.AddressLine1,
                        AddressLine2 = userAddress.AddressLine2,
                        City = userAddress.City,
                        CountrySubdivision = userAddress.CountrySubdivision,
                        Country = userAddress.Country,
                        PostalCode = userAddress.PostalCode,
                        PhoneNumber = userAddress.PhoneNumber,
                    }
                };
        }

        public static DeviceReputationData ToPaymentServiceApiSdkReputation(this IRisk risk)
        {
            if (risk == null || risk.IovationFields == null)
            {
                return null;
            }

            return new DeviceReputationData()
                    {
                        DeviceFingerprint = risk.IovationFields.DeviceFingerprint,
                        IpAddress = risk.IovationFields.IpAddress
                    };
        }

        public static AutoReloadProfile ToPaymentServiceApiSdkAutoReloadProfile(this IAutoReloadProfile autoReloadProfile)
        {
            if (autoReloadProfile== null)
            {
                return null;
            }

            return new AutoReloadProfile()
            {
                AutoReloadId = autoReloadProfile.AutoReloadId,
                AutoReloadType = autoReloadProfile.AutoReloadType,
                Day = autoReloadProfile.Day,
                TriggerAmount = autoReloadProfile.TriggerAmount,
                AutoReloadAmount = autoReloadProfile.Amount,
                PaymentMethodId = autoReloadProfile.PaymentMethodId,
                Status = autoReloadProfile.Status.ToString(),
                DisableUntilDate = autoReloadProfile.DisableUntilDate,
                StoppedDate = autoReloadProfile.StoppedDate,
            };
        }

        public static List<OrderItem> ToPaymentServiceApiSdkOrderItemList(this List<IOrderLineItem> orderLineItems)
        {
            List<OrderItem> sdkOrderLineItems = new List<OrderItem>();

            foreach(IOrderLineItem lineItem in orderLineItems) 
            {  
                sdkOrderLineItems.Add(new OrderItem
                    {
                        Amount = lineItem.UnitPrice,
                        Quantity = lineItem.Quantity,
                        Name = (lineItem.ProductOrdered == null) ? null : lineItem.ProductOrdered.ProductName,
                        Sku = (lineItem.ProductOrdered == null) ? null : lineItem.ProductOrdered.SKU,
                        Category = (lineItem.ProductOrdered == null) ? null : lineItem.ProductOrdered.GiftCategory
                    });  
            }

            return sdkOrderLineItems;
        }

        public static Common.Model.ICardTransaction ToProvider(this ICardTransaction cardTransaction)
        {
            if (cardTransaction == null)
            {
                return null;
            }

            return new CardTransactionModels.CardTransaction
                {
                    Amount = cardTransaction.Amount,
                    AmountInBaseCurrency = cardTransaction.AmountInBaseCurrency,
                    AuthorizationCode = cardTransaction.AuthorizationCode,
                    BaseCurrency = cardTransaction.BaseCurrency,
                    BeginingBalanceInBaseCurrency = cardTransaction.BeginingBalanceInBaseCurrency,
                    BeginningBalance = cardTransaction.BeginningBalance,
                    CardClass = cardTransaction.CardClass,
                    CardId = cardTransaction.CardId,
                    CardNumber = cardTransaction.CardNumber,
                    Currency = cardTransaction.Currency,
                    Description = cardTransaction.Description,
                    EndingBalance = cardTransaction.EndingBalance,
                    EndingBalanceInBaseCurrency = cardTransaction.EndingBalanceInBaseCurrency,
                    Pin = cardTransaction.Pin,
                    Promotion = cardTransaction.Promotion == null ? null : cardTransaction.Promotion.ToProvider(),
                    RequestCode = cardTransaction.RequestCode,
                    ResponseCode = cardTransaction.ResponseCode,
                    TransactionDate = cardTransaction.UtcDate,
                    TransactionId = cardTransaction.TransactionId
                };
        }

        public static Common.Model.ICardPromotion ToProvider(this ICardPromotion cardPromotion)
        {
            if (cardPromotion == null)
            {
                return null;
            }

            return new CardPromotion
                {
                    Amount = cardPromotion.Amount,
                    Code = cardPromotion.Code,
                    Message = cardPromotion.Message
                };
        }

        public static ICardTransaction ToCardIssuer(this Common.Model.ICardTransaction cardTransaction)
        {
            if (cardTransaction == null)
            {
                return null;
            }

            return new CardIssuerModels.CardTransaction() 
            {
                Amount = cardTransaction.Amount,
                AmountInBaseCurrency = cardTransaction.AmountInBaseCurrency,
                AuthorizationCode = cardTransaction.AuthorizationCode,
                BaseCurrency = cardTransaction.BaseCurrency,
                BeginingBalanceInBaseCurrency = cardTransaction.BeginingBalanceInBaseCurrency,
                BeginningBalance = cardTransaction.BeginningBalance,
                CardClass = cardTransaction.CardClass,
                CardId = cardTransaction.CardId,
                CardNumber = cardTransaction.CardNumber,
                Currency = cardTransaction.Currency,
                Description = cardTransaction.Description,
                EndingBalance = cardTransaction.EndingBalance,
                EndingBalanceInBaseCurrency = cardTransaction.EndingBalanceInBaseCurrency,
                Pin = cardTransaction.Pin,
                Promotion = cardTransaction.Promotion == null ? null : cardTransaction.Promotion.ToCardIssuer(),
                RequestCode = cardTransaction.RequestCode,
                ResponseCode = cardTransaction.ResponseCode,
                TransactionDate = cardTransaction.TransactionDate,
                TransactionId = cardTransaction.TransactionId
            };
        }

        public static ICardPromotion ToCardIssuer(this Common.Model.ICardPromotion cardPromotion)
        {
            if (cardPromotion == null)
            {
                return null;
            }

            return new CardIssuerModels.CardPromotion
            {
                Amount = cardPromotion.Amount,
                Code = cardPromotion.Code,
                Message = cardPromotion.Message
            };
        }

        public static OrderLineItem ToBom(this IOrderLineItem orderLineItem)
        {
            return orderLineItem == null ? null :
            new OrderLineItem()
            {
                CityTax = orderLineItem.CityTax,
                CountyTax = orderLineItem.CountyTax,
                CustomCardImageId = orderLineItem.CustomCardImageId,
                CustomCardTheme = orderLineItem.CustomCardTheme,
                DistrictTax = orderLineItem.DistrictTax,
                EGift = orderLineItem.EGift.ToBom(),
                GiftMessages = orderLineItem.GiftMessages,
                MarketingMessage = orderLineItem.MarketingMessage,
                OrderType = orderLineItem.OrderType,
                ParentLineItemId = orderLineItem.ParentLineItemId,
                ProductOrdered = orderLineItem.ProductOrdered.ToBom(),
                Quantity = orderLineItem.Quantity,
                Shipping = orderLineItem.Shipping.ToBom(),
                ShippingCost = orderLineItem.ShippingCost,
                StateTax = orderLineItem.StateTax,
                Tax = orderLineItem.Tax,
                UnitPrice = orderLineItem.UnitPrice
            };
        }

        public static Starbucks.Platform.Bom.Shared.eGiftEntities.EGiftNotificationLineItem ToBom(this IEGiftNotificationLineItem gift)
        {
            return gift == null ? null :
             new Platform.Bom.Shared.eGiftEntities.EGiftNotificationLineItem()
             {
                 Amount = gift.Amount,
                 CardStatus = gift.CardStatus,
                 CashstarDeliveryMethod = gift.CashstarDeliveryMethod,
                 CashstarDeliveryStatus = gift.CashstarDeliveryStatus,
                 Challenge = gift.Challenge,
                 ChallengeDescription = gift.ChallengeDescription,
                 Currency = gift.Currency,
                 Delivered = gift.Delivered,
                 DeliveryStatus = (DeliveryStatus)gift.DeliveryStatus,
                 DeliveryType = (Platform.Bom.Shared.eGiftEntities.DeliveryType)gift.DeliveryType,
                 EGCCode = gift.EGCCode,
                 EGiftId = gift.EGiftId,
                 FacebookToken = gift.FacebookToken,
                 ImageFileName = gift.ImageFileName,
                 ItemTotal = gift.ItemTotal,
                 OrderId = gift.OrderId,
                 OrderNumber = gift.OrderNumber,
                 PIN = gift.PIN,
                 ProductId = gift.ProductId,
                 Received = gift.Received,
                 RecipientEmail = gift.RecipientEmail,
                 RecipientFacebookId = gift.RecipientFacebookId,
                 RecipientMessage = gift.RecipientMessage,
                 RecipientName = gift.RecipientName,
                 RefundTransactionId = gift.RefundTransactionId,
                 ReloadCardId = gift.ReloadCardId,
                 ReloadCardNumber = gift.ReloadCardNumber,
                 ReloadTransactionId = gift.ReloadTransactionId,
                 Scheduled = gift.Scheduled,
                 ScheduledIso = gift.ScheduledIso,
                 SenderEmail = gift.SenderEmail,
                 SenderFacebookId = gift.SenderFacebookId,
                 SenderName = gift.SenderName,
                 SvcNumber = gift.SvcNumber

             };
        }

        public static Platform.Bom.BusinessObjects.Shipping ToBom(this IShipping shipping)
        {
            return shipping == null ? null :
                new Platform.Bom.BusinessObjects.Shipping()
                {
                    Address = shipping.Address.ToBom()
                };
        }

        public static Platform.Bom.Product ToBom(this IProduct product)
        {
            return product == null
                ? null
                : new Platform.Bom.Product()
                {
                    GiftCategory = product.GiftCategory,
                    HostHedge = product.HostHedge,
                    ItemAmount = product.ItemAmount,
                    JDAItemNumber = product.JDAItemNumber,
                    Locale = product.Locale,
                    ProductCode = product.ProductCode,
                    ProductDescription = product.ProductDescription,
                    ProductID = product.ProductID,
                    ProductName = product.ProductName,
                    ProductTypeId = product.ProductTypeId,
                    ProductVariantID = product.ProductVariantID,
                    SKU = product.SKU,
                    Site = product.Site,
                    Threshold = product.Threshold,
                    TimeCategory = product.TimeCategory,
                    TimeHedge = product.TimeHedge,
                    VelocityHedge = product.VelocityHedge
                };
        }

        #region SaveOrderBOM Hack
        public static BillingInfo ToSavedOrderBom(this IBillingInfo billingInfo)
        {
            return new BillingInfo()
            {
                Address = (billingInfo.Address == null) ? (new Platform.Bom.Shared.UserAddress()) : billingInfo.Address.ToBom(),
                AuthCode = billingInfo.AuthCode,
                AuthDate = billingInfo.AuthDate,
                AuthRequestId = billingInfo.AuthRequestId,
                County = billingInfo.County,
                Currency = billingInfo.Currency,
                CurrencyNumericCode = billingInfo.CurrencyNumericCode,
                EmailAddress = billingInfo.EmailAddress,
                IsPayPalBillFailed = billingInfo.IsPayPalBillFailed,
                PayPalEmailAddress = billingInfo.PayPalEmailAddress,
                PayPalExpressCheckoutToken = billingInfo.PayPalExpressCheckoutToken,
                PayPalPayerId = billingInfo.PayPalPayerId,
                PayPalSubject = billingInfo.PayPalSubject,
                PayPalTransactionId = billingInfo.PayPalTransactionId,
                PaymentMethod = billingInfo.PaymentMethod.ToSavedOrderBom()
            };
        }

        public static Platform.Bom.Shared.PaymentMethod ToSavedOrderBom(this IPaymentMethod paymentMethod)
        {
            return new Platform.Bom.Shared.PaymentMethod()
            {
                AccountNumber = paymentMethod.AccountNumber,
                BankName = paymentMethod.BankName, // todo: deal with this?
                BillingAddressId = paymentMethod.BillingAddressId,
                Cvn = paymentMethod.Cvn,
                ExpirationMonth = paymentMethod.ExpirationMonth,
                ExpirationYear = paymentMethod.ExpirationYear,
                // FirstSix = paymentMethod.FirstSix, // todo: deal with this?
                FullAccountNumber = paymentMethod.AccountNumber,
                FullName = paymentMethod.FullName,
                // Group = paymentMethod.Group, // todo: deal with this?
                IsDefault = paymentMethod.IsDefault,
                IsFraudChecked = paymentMethod.IsFraudChecked,
                IsTemporary = paymentMethod.IsTemporary, // todo: deal with this?
                LastFour = paymentMethod.AccountNumberLastFour,
                Nickname = paymentMethod.Nickname,
                PaymentMethodId = paymentMethod.PaymentMethodId,
                PaymentType = GetPaymentType(paymentMethod),
                RoutingNumber = paymentMethod.RoutingNumber, // todo: deal with this?
                SurrogateAccountNumber = paymentMethod.SurrogateAccountNumber, // todo: deal with this?
                UserId = paymentMethod.UserId,
                FraudCheckedDate = paymentMethod.FraudCheckedDate
            };
        }

        private static PaymentType GetPaymentType(IPaymentMethod paymentMethod)
        {
            var paymentType = (PaymentType)paymentMethod.PaymentTypeId;
            if (paymentType == PaymentType.None)//Token based reloads (ApplePay etc) will not have PaymentTypeId
            {
                if (string.Equals(paymentMethod.PaymentType, "ChasePay", StringComparison.InvariantCultureIgnoreCase))
                {
                    paymentType = PaymentType.ChasePayVisa;
                }
                else
                {
                    Enum.TryParse(paymentMethod.PaymentType, true, out paymentType);
                }
            }
            return paymentType;
        }

        public static Platform.Bom.Shared.UserAddress ToBom(this IUserAddress userAddress)
        {
            return userAddress == null ? null :
            new Platform.Bom.Shared.UserAddress()
            {
                AddressId = userAddress.AddressId,
                AddressLine1 = userAddress.AddressLine1,
                AddressLine2 = userAddress.AddressLine2,
                AddressName = userAddress.AddressName,
                AddressType = (Platform.Bom.Shared.UserAddressType) userAddress.AddressType,
                City = userAddress.City,
                CompanyName = userAddress.CompanyName,
                CountryName = userAddress.CountryName,
                Country = userAddress.Country,
                CountrySubdivision = userAddress.CountrySubdivision,
                CountrySubdivisionDescription = userAddress.CountrySubdivisionDescription,
                EmailAddress = userAddress.EmailAddress,
                FirstName = userAddress.FirstName,
                IsTemporary = userAddress.IsTemporary,
                LastName = userAddress.LastName,
                PhoneExtension = userAddress.PhoneExtension,
                PhoneNumber = userAddress.PhoneNumber,
                PostalCode = userAddress.PostalCode,
                RegionDescription = userAddress.RegionDescription,
                ShippingTotal = userAddress.ShippingTotal,
                UserId = userAddress.UserId,
                VerificationLevel = (Platform.Bom.Shared.UserAddressVerificationLevel) userAddress.VerificationLevel,
//                VerificationSearchResults = address.VerificationSearchResults , // todo: investigate this.
                VerificationStatus = (Platform.Bom.Shared.UserAddressVerificationStatus) userAddress.VerificationStatus
            };
        }

        public static string Serialize(this ConfirmPay confirmPay)
        {
            var response = string.Empty;
            if (confirmPay!=null)
                response=Newtonsoft.Json.JsonConvert.SerializeObject(confirmPay);
            return response;
        }


        #endregion
    }
}
