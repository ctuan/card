﻿using System;
using Starbucks.Card.Provider.Common.Models;

namespace Starbucks.CardTransaction.Provider.CardModels
{
    public class CardRegistrationStatus : ICardRegistrationStatus
    {
        public string CardId { get; set; }
        public string CardNumber { get; set; }
        public bool Successful { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
        public Type ExceptionType { get; set; }
    }
}
