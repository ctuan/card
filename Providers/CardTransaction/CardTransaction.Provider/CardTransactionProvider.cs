﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using ServiceProxies.Order.Wcf;
using Starbucks.Api.Sdk.PaymentService;
using Starbucks.Api.Sdk.PaymentService.Interfaces;
using Starbucks.Api.Sdk.PaymentService.Models;
using Starbucks.Api.Sdk.WebRequestWrapper;
using Starbucks.Card.Provider.Common;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.Common.Rule;
using Starbucks.CardIssuer.Dal.SvDot.Common;
using Starbucks.CardTransaction.Provider.CardTransactionModels;
using Starbucks.CardTransaction.Provider.Common;
using Starbucks.CardTransaction.Provider.Common.ErrorResource;
using Starbucks.CardTransaction.Provider.Common.Model;
using Starbucks.CardTransaction.Provider.Configuration;
using Starbucks.CardTransaction.Provider.Models;
using Starbucks.CardTransaction.Provider.PaymentModels;
using Starbucks.CardTransaction.Provider.PayPalModels;
using Starbucks.ChasePayConfirmation.QueueService.Common;
using Starbucks.ChasePayConfirmation.QueueService.Common.Commands;
using Starbucks.LogCounter.Provider.Common;
using Starbucks.MessageBroker.Common;
using Starbucks.MessageBroker.StageEmailListener;
using Starbucks.OpenApi.ServiceExtensions.Culture;
using Starbucks.OrderManagement.Dal.Common.Models;
using Starbucks.OrderManagement.Provider.Common;
using Starbucks.PaymentMethod.Provider.Common;
using Starbucks.Platform.Security;
using Starbucks.Rewards.Provider.Common;
using Starbucks.SecurePayment.Provider.Common;
using Starbucks.SecurePayment.Provider.Common.Models;
using Starbucks.SecurePayPalPayment.Provider.Common;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;
using Starbucks.SecurePayPalPayment.Provider.ProviderModels;
using Starbucks.ServiceProxies.Extensions;
using Starbucks.Settings.Provider.Common;
using Starbucks.Tipping.Provider.Common;
using Starbucks.TransactionHistory.Common.Enums;
using Starbucks.TransactionHistory.Dal.Common;
using IBillingInfo = Starbucks.SecurePayment.Provider.Common.Models.IBillingInfo;
using ICardTransaction = Starbucks.CardTransaction.Provider.Common.Model.ICardTransaction;
using IPaymentMethod = Starbucks.PaymentMethod.Provider.Common.Models.IPaymentMethod;
using IReloadForStarbucksCard = Starbucks.CardTransaction.Provider.Common.Model.IReloadForStarbucksCard;
using TipStatus = Starbucks.Tipping.Dal.Common.Enums.TipStatus;
using ICard = Starbucks.Card.Provider.Common.Models.ICard;
using UserAddress = Starbucks.CardTransaction.Provider.PaymentModels.UserAddress;
using VisibilityLevel = Starbucks.Card.Provider.Common.Enums.VisibilityLevel;
using AuthRequest = Starbucks.Api.Sdk.PaymentService.Models.AuthRequest;
using BillingInfo = Starbucks.CardTransaction.Provider.PaymentModels.BillingInfo;
using BillRequest = Starbucks.Api.Sdk.PaymentService.Models.BillRequest;
using IAddress = Starbucks.SecurePayPalPayment.Provider.Common.Models.IAddress;
using PaymentDetails = Starbucks.SecurePayPalPayment.Provider.ProviderModels.PaymentDetails;
using PaymentDetailsLineItem = Starbucks.SecurePayPalPayment.Provider.ProviderModels.PaymentDetailsLineItem;
using VoidRequest = Starbucks.Api.Sdk.PaymentService.Models.VoidRequest;
using StarbucksAccountProvider = Account.Provider;
using NServiceBus;


namespace Starbucks.CardTransaction.Provider
{
    public class CardTransactionProvider : ICardTransactionProvider
    {
        private const int PayPalFailedOrderReasonCode = 120;
        private const int PSApiBillTimeoutExceptionReasonCode = 130;
        private const int PSApiBillExceptionReasonCode = 131;
        private const string IsLoggedInDefault = "true";
        const string UserIdForTempPaymentMethods = "00000000-0000-0000-0000-000000000000";
        const string defaultOrderSource = "ios";
        private const string UnKnownPlatform = "unknown";

        private readonly StarbucksAccountProvider.Common.IAccountProvider _accountProvider;
        private readonly ICardIssuerDal _cardIssuer;
        private readonly ICardProvider _cardProvider;
        private readonly CardTransactionProviderSettingsSection _cardTransactionProviderSettingsSection;
        private readonly IPaymentServiceApiClient _paymentServiceApiClient;
        private readonly ILogCounterManager _loggingManager;
        private readonly ISecurePayPalPaymentClient _securePayPalPayment;
        private readonly ITippingProvider _tippingProvider;
        private readonly ITransactionHistoryDataProvider _transactionHistoryDataProvider;
        private readonly IMessageBroker _messageBroker;
        private readonly IOrderManagementProvider _orderManagementProvider;
        private readonly IPaymentMethodProvider _paymentMethodProvider;
        private readonly IRewardsProvider _rewardsProvider;
        private readonly ISettingsProvider _settingsProvider;
        private readonly ISendOnlyBus _bus;
        private readonly Dictionary<string, string> _normalizeMarket = new Dictionary<string, string>()
        {
            {"ROI", "IE" },
            {"GB", "UK" }
        };


        public CardTransactionProvider(
            ICardProvider cardProvider
            , ICardIssuerDal cardIssuer
            , IPaymentMethodProvider paymentMethodProvider
            , StarbucksAccountProvider.Common.IAccountProvider accountProvider
            , ILogCounterManager loggingManager
            , ISecurePayPalPaymentClient securePayPalPayment
            , ITippingProvider tippingProvider
            , ITransactionHistoryDataProvider transactionHistoryDataProvider
            , IMessageBroker messageBroker
            , ISettingsProvider settingsProvider
            , CardTransactionProviderSettingsSection cardTransactionProviderSettingsSection
            , IRewardsProvider rewardsProvider
            , IOrderManagementProvider orderManagementProvider
            , IPaymentServiceApiClient paymentServiceApiClient
            , ISendOnlyBus bus)
        {
            _cardProvider = cardProvider;
            _cardIssuer = cardIssuer;
            _paymentMethodProvider = paymentMethodProvider;
            _accountProvider = accountProvider;
            _loggingManager = loggingManager;
            _securePayPalPayment = securePayPalPayment;
            _tippingProvider = tippingProvider;
            _transactionHistoryDataProvider = transactionHistoryDataProvider;
            _messageBroker = messageBroker;
            _settingsProvider = settingsProvider;
            _cardTransactionProviderSettingsSection = cardTransactionProviderSettingsSection;
            _rewardsProvider = rewardsProvider;
            _orderManagementProvider = orderManagementProvider;
            _paymentServiceApiClient = paymentServiceApiClient;
            _bus = bus;
        }

        public IActivateAndReloadCardResponse ActivateAndReloadCardAsGift(decimal amount, string paymentMethodId,
            string paymentType)
        {
            throw new NotImplementedException();
        }

        public ICardTransaction ReloadAsGiftContribution(string cardNumber, decimal amount, string paymentMethdodId,
            string paymentType)
        {
            throw new NotImplementedException();
        }


        private static IFailedOrder CreateFailedOrderObject(IBillingInfo billingInfo, IMerchantInfo merchantInfo,
            string orderId, string cardNumber, string orderType, string userId, string orderSourceCode, string requestId,
            int reasonCode)
        {
            var billingFirstName = string.Empty;
            var billingLastName = string.Empty;
            var failedOrderStatus = 3;
            var paymentMethodId = 0;
            var isValidPaymentId = false;

            if (billingInfo != null)
            {
                if (billingInfo.PaymentMethod != null &&
                    !string.IsNullOrWhiteSpace(billingInfo.PaymentMethod.PaymentMethodId))
                {
                    isValidPaymentId = int.TryParse(Encryption.ORCADecrypt(billingInfo.PaymentMethod.PaymentMethodId),
                        out paymentMethodId);
                }

                if (billingInfo.Address != null)
                {
                    billingFirstName = billingInfo.Address.FirstName;
                    billingLastName = billingInfo.Address.LastName;
                }
            }

            return new FailedOrder
            {
                OrderId = orderId,
                MerchantKey = merchantInfo.MerchantKey,
                RequestedId = requestId,
                OrderStatus = failedOrderStatus,
                PaymentMethodId = isValidPaymentId ? paymentMethodId : (int?)null,
                SubMarketId = merchantInfo.SubMarketId,
                UserId = userId,
                BillingCurrency = billingInfo != null ? billingInfo.Currency : string.Empty,
                BillingEmail = billingInfo != null ? billingInfo.EmailAddress : string.Empty,
                BillingFirstName = billingFirstName,
                BillingLastName = billingLastName,
                BillingZipCode =
                    billingInfo != null && billingInfo.Address != null ? billingInfo.Address.PostalCode : string.Empty,
                BillingPhone =
                    billingInfo != null && billingInfo.Address != null ? billingInfo.Address.PhoneNumber : string.Empty,
                PaypalPayerId = billingInfo != null ? billingInfo.PayPalPayerId : string.Empty,
                CardFirstSix = string.Empty,
                CardLastFour =
                    billingInfo != null && billingInfo.PaymentMethod != null
                        ? billingInfo.PaymentMethod.AccountNumberLastFour
                        : string.Empty,
                StarbucksCardNumber = cardNumber,
                OrderSource = orderSourceCode,
                OrderType = orderType,
                ReasonCode = reasonCode,
                OrderDate = DateTime.UtcNow.Date
            };
        }

        #region "MarketAndCulture"

        private static readonly Dictionary<string, string> CultureConversion;
        private static readonly Dictionary<string, string> MarketConversion;

        static CardTransactionProvider()
        {
            CultureConversion = new Dictionary<string, string>
            {
                {"en-us", "MOUS"},
                {"en-ca", "MOCA"},
                {"en-uk", "MOUK"},
                {"fr-ca", "MOCA"}
            };
            MarketConversion = new Dictionary<string, string>
            {
                {"US", "MOUS"},
                {"GB", "MOUK"},
                {"UK", "MOUK"},
                {"CA", "MOCA"}
            };
        }

        private static string CurrentSubMarket(string accountMarket)
        {
            return String.IsNullOrWhiteSpace(accountMarket)
                ? CurrentSubMarket()
                : MarketConversion.ContainsKey(accountMarket.ToUpperInvariant())
                    ? MarketConversion[accountMarket.ToUpperInvariant()]
                    : accountMarket;
        }

        private static string CurrentSubMarket()
        {
            var culture = CultureContext.CurrentCulture;
            culture = culture.ToLowerInvariant();
            if (CultureConversion.ContainsKey(culture))
            {
                return CultureConversion[culture];
            }
            return String.Empty;
        }

        #endregion

        #region "Tip"

        //TODO: Evaluate putting this in DB table
        private string ConvertCurrencyToTipMarket(string currency)
        {
            switch (currency)
            {
                case "USD":
                    return "TIUS";
                case "CAD":
                    return "TICA";
                default:
                    throw new Exception(String.Format("Cannot find tipping submarket for {0}", currency));
            }
        }

        public ICardTransaction Tip(IProcessTip tip)
        {
            if (tip == null)
            {
                throw new ArgumentNullException("tip");
            }
            var actualTipStatus = UpdateTipStatus(tip, TipStatus.Processing);
            if (actualTipStatus != TipStatus.Processing)
            {
                throw new CardTransactionException(CardTransactionErrorResource.InvalidTipStatusCode,
                    CardTransactionErrorResource.InvalidTipStatusMessage);
            }
            IAssociatedCard card;
            IMerchantInfo merchantInfo;
            try
            {
                ValidateTip(tip);
                merchantInfo = GetMerchantInfo(tip);
                //override the merchantInfo
                merchantInfo.AlternateMid = tip.OriginalStoreId;
                merchantInfo.TerminalId = tip.OriginalTerminalId;
                card = GetAssociatedCard(tip.UserId, tip.CardId, VisibilityLevel.Decrypted, false, tip.UserMarket,
                    tip.UserMarket, includeAssociatedCards: false, platform: string.Empty);
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex,
                    String.Format("Error tipping original transaction: {0} {1} {2}", tip.OriginalTransactionId,
                        ex.Message, ex.StackTrace), "CardApi - Tip", TraceEventType.Error);
                UpdateTipStatus(tip, TipStatus.Failed);
                throw;
            }
            CardIssuer.Dal.Common.Models.ICardTransaction result;
            try
            {
                result = _cardIssuer.Tip(merchantInfo, card.Number, tip.Amount, tip.LocalTransactionTime,
                    Convert.ToInt32(tip.LocalTransactionId));
                tip.Amount = result.Amount;
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex,
                    String.Format(
                        "cardIssuer.Tip Exception. OriginalTransactionId: {0}, Message: {1}, StackTrace: {2}",
                        tip.OriginalTransactionId,
                        ex.Message, ex.StackTrace), "CardApi - Tip", TraceEventType.Error);
                UpdateTipStatus(tip, TipStatus.Failed);
                throw new CardTransactionException(CardTransactionErrorResource.CannotTipCode,
                    CardTransactionErrorResource.CannotTipMessage);
            }

            if (result == null)
            {
                UpdateTipStatus(tip, TipStatus.Failed);
                throw new CardTransactionException(CardTransactionErrorResource.CannotTipCode,
                    CardTransactionErrorResource.CannotTipMessage);
            }

            if (result.TransactionSucceeded)
            {
                UpdateTipStatus(tip, TipStatus.Processed);
            }
            else
            {
                UpdateTipStatus(tip, TipStatus.Failed);
                throw CardIssuerExceptionHandler.HandleCardIssuerResponse(result.ResponseCode);
            }
            return result.ToProvider();
        }

        private IMerchantInfo GetMerchantInfo(IProcessTip tip)
        {
            IMerchantInfo merchantInfo;
            ValidateTipUserMerchantInfo(tip);
            if (string.IsNullOrWhiteSpace(tip.MerchantId))
            {
                var tipMarket = ConvertCurrencyToTipMarket(tip.OriginalTransactionCurrency);
                merchantInfo = _cardIssuer.GetMerchantInfo(tipMarket);
            }
            else
            {
                merchantInfo = _cardIssuer.GetMerchantInfoByMerchantId(tip.MerchantId);
            }

            return merchantInfo;
        }

        private void ValidateTipUserMerchantInfo(IProcessTip tip)
        {
            var userMerchantInfo = GetMerchantInfo(tip.UserMarket, platform: UnKnownPlatform);

            if (userMerchantInfo == null)
            {
                throw new CardTransactionValidationException(
                    CardTransactionValidationErrorResource.TipInvalidUserCurrencyTransactionCurrencyCode,
                    CardTransactionValidationErrorResource.TipInvalidUserCurrencyTransactionCurrencyMessage);
            }

            if (userMerchantInfo.CurrencyCode != tip.OriginalTransactionCurrency)
            {
                throw new CardTransactionValidationException(
                    CardTransactionValidationErrorResource.TipInvalidUserCurrencyTransactionCurrencyCode,
                    CardTransactionValidationErrorResource.TipInvalidUserCurrencyTransactionCurrencyMessage);
            }
        }

        private static void ValidateTip(IProcessTip tip)
        {
            Validator.ValidateString(tip.OriginalTransactionCurrency,
                CardTransactionValidationErrorResource.TipOriginalCurrencyRequiredCode,
                CardTransactionValidationErrorResource.TipOriginalCurrencyRequiredCode);
            Validator.ValidateString(tip.UserMarket,
                CardTransactionValidationErrorResource.TipUserMarketRequiredCode,
                CardTransactionValidationErrorResource.TipUserMarketRequiredMessage);
            Validator.ValidateTipAmount(tip.Amount);
        }

        private TipStatus UpdateTipStatus(IProcessTip tip, TipStatus expectedTipStatus)
        {
            var actualTipStatus = _tippingProvider.UpdateTipStatus(tip.UserId, tip.OriginalTransactionId,
                expectedTipStatus);
            if (actualTipStatus == expectedTipStatus)
            {
                UpdateOriginalTransaction(tip, expectedTipStatus);
            }

            return actualTipStatus;
        }

        private void UpdateOriginalTransaction(IProcessTip tip, TipStatus tipStatus)
        {
            try
            {
                var sourceId = _transactionHistoryDataProvider.GenerateTransactionSourceId(tip.CardId,
                    TransactionType.Redemption,
                    tip.OriginalStoreId,
                    tip.LocalTransactionId,
                    tip.LocalTransactionTime);
                var originalTransaction = _transactionHistoryDataProvider.GetHistoryItemBySourceId(sourceId);
                originalTransaction.TransactionDetails.TipInfo.Amount = tip.Amount;
                originalTransaction.TransactionDetails.TipInfo.TipStatus =
                    (TransactionHistory.Common.Enums.TipStatus)tipStatus;
                _transactionHistoryDataProvider.UpsertItem(originalTransaction);
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex,
                    "TipStatus was updated, but the original transaction (ID: " + tip.OriginalTransactionId +
                    ") was not.  Exception: " + ex,
                    "CardApi - Tip",
                    TraceEventType.Error);
            }
        }

        #endregion

        #region "Reload"

        internal ICardTransaction Reload(IMerchantInfo merchantInfo, string cardNumber, decimal amount)
        {
            Stopwatch reloadWatch = new Stopwatch();
            reloadWatch.Start();

            var providerTransaction = _cardIssuer.Reload(merchantInfo, cardNumber, amount);
            reloadWatch.Stop();
            LogTimer(reloadWatch, "Valuelink Load");

            return providerTransaction == null ? null : providerTransaction.ToProvider();
        }

        public ICardTransaction ReloadStarbucksCard(string userId, string cardId,
            IReloadForStarbucksCard reloadForStarbucksCard,
            string market, string userMarket, string email = null)
        {
            return ReloadCardByUserIdCardId(userId, cardId, reloadForStarbucksCard, "", market, userMarket, email);
        }

        public ICardTransaction ReloadCardByUserIdCardId(string userId, string cardId,
            IReloadForStarbucksCard reloadForStarbucksCard,
            string cvn, string market, string userMarket, string email = null)
        {
            LogHelper.Log(
                String.Format("ReloadCardByUserIdCardId called for userId {0}. cardId {1}  SessionId {2}", userId,
                    cardId, reloadForStarbucksCard.SessionId),
                "CardApi - Reload ReloadCardByUserIdCardId", TraceEventType.Information);

            Validator.ValidateCardId(cardId);
            Validator.ValidateUserId(userId);
            Validator.ValidatePaymentMethod(reloadForStarbucksCard.PaymentMethodId, reloadForStarbucksCard.PaymentTokenDetails);
            Validator.ValidateReloadAmount(reloadForStarbucksCard.Amount);

            var user = GetUser(userId);
            if (user == null)
            {
                throw new CardTransactionException(CardTransactionErrorResource.UserNotFoundCode,
                    CardTransactionErrorResource.UserNotFoundMessage);
            }

            string platform = reloadForStarbucksCard.Risk != null ? reloadForStarbucksCard.Risk.Platform : UnKnownPlatform;
            var card = GetAssociatedCard(userId, cardId, VisibilityLevel.Decrypted, false, market, userMarket, includeAssociatedCards: true, platform: platform);
            if (card == null)
            {
                throw new CardTransactionException(CardTransactionErrorResource.CardNotFoundCode,
                    CardTransactionErrorResource.CardNotFoundMessage);
            }

            if (!card.Actions.Contains(CardActions.Reload))
            {
                throw new CardTransactionException(CardTransactionErrorResource.CannotReloadCardMarketCode,
                    CardTransactionErrorResource.CannotReloadCardMarketMessage);
            }

            var result = ReloadStarbucksCardByCardNumber(card.Number, reloadForStarbucksCard,
                !string.IsNullOrWhiteSpace(user.EmailAddress) ? user.EmailAddress : email,
                userId, card.Class,
                userMarket, card, user);
            LogHelper.Log(
                String.Format(
                    "ReloadCardByUserIdCardId called for userId {0}. cardId {1}  SessionId {2}, ResponseCode {3}",
                    userId,
                    cardId, reloadForStarbucksCard.SessionId, result != null ? result.ResponseCode : String.Empty),
                "CardApi - Reload ReloadCardByUserIdCardId end", TraceEventType.Information);

            return result;
        }

        public ICardTransaction ReloadStarbucksCardByCardNumberPin(string cardNumber, string pin,
            IReloadForStarbucksCard reloadForStarbucksCard, string emailAddress, string market)
        {
            var card = GetCard(cardNumber, pin, market);

            AutoMapper.Mapper.CreateMap<ICard, IAssociatedCard>();
            var associatedCard = AutoMapper.Mapper.Map<IAssociatedCard>(card);

            return ReloadStarbucksCardByCardNumber(cardNumber, reloadForStarbucksCard, emailAddress, null, card.Class,
                market, associatedCard, null);
        }

        private ICardTransaction ReloadStarbucksCardByCardNumber(string cardNumber,
            IReloadForStarbucksCard reloadForStarbucksCard, string emailAddress,
            string userId, string cardClassId, string userMarket, IAssociatedCard card,
            StarbucksAccountProvider.Common.Models.IUser user)
        {
            var rules = new Rules();
            if (!rules.CanReload(cardClassId))
            {
                throw new CardTransactionException(CardTransactionErrorResource.CannotReloadCardClassCode,
                    CardTransactionErrorResource.CannotReloadCardClassMessage);
            }

            Validator.ValidateCardNumber(cardNumber);
            Validator.ValidatePaymentMethod(reloadForStarbucksCard.PaymentMethodId, reloadForStarbucksCard.PaymentTokenDetails);
            Validator.ValidateEmailAddress(emailAddress);

            Validator.ValidateReloadAmount(reloadForStarbucksCard.Amount);

            // If no market passed in risk use the user's.
            var market = GetMarket(userMarket, reloadForStarbucksCard);

            if (!String.IsNullOrWhiteSpace(reloadForStarbucksCard.PaymentMethodId))
            {
                return ReloadStarbucksCardByCardNumberWithPaymentMethod(reloadForStarbucksCard, emailAddress, userId,
                    market, card, user);
            }
            else if (reloadForStarbucksCard.PaymentTokenDetails != null && !string.IsNullOrWhiteSpace(reloadForStarbucksCard.PaymentTokenDetails.PaymentToken))
            {
                return ReloadStarbucksCardByCardNumberWithPaymentToken(reloadForStarbucksCard, emailAddress, userId,
                    market,
                    card, user);
            }
            //Validator.ValidatePaymentMethod should throw prior to this point
            return null;
        }

        private ICardTransaction ReloadStarbucksCardByCardNumberWithPaymentToken(
            IReloadForStarbucksCard reloadForStarbucksCard, string emailAddress, string userId, string market,
            IAssociatedCard card,
            StarbucksAccountProvider.Common.Models.IUser user)
        {
            var orderId = GetNewCommerceServerOrderId();

            StarbucksAccountProvider.Common.Models.IAddress billingAddress = null;
            if (reloadForStarbucksCard.PaymentTokenDetails == null || !IsChasePayPos(reloadForStarbucksCard))
            {
                billingAddress = reloadForStarbucksCard.BillingAddress.ToBillingAddress();
                if (billingAddress == null)
                    throw new CardTransactionException(CardTransactionErrorResource.BillingAddressNotFoundCode,
                        CardTransactionErrorResource.BillingAddressNotFoundMessage);
            }

            IMerchantInfo merchantInfo = null;
            merchantInfo = GetMerchantInfo(CurrentSubMarket(market), reloadForStarbucksCard.Risk != null ? reloadForStarbucksCard.Risk.Platform : UnKnownPlatform);

            if (merchantInfo == null)
                throw new CardTransactionException(CardTransactionErrorResource.MerchantInfoNotFoundCode,
                    CardTransactionErrorResource.MerchantInfoNotFoundMessage);
            var billingInfo = CreateBillingInfo(emailAddress, billingAddress, merchantInfo);
            if (billingInfo == null)
                throw new CardTransactionException(CardTransactionErrorResource.BillingInfoNotFoundCode,
                    CardTransactionErrorResource.BillingInfoNotFoundMessage);

            //Tokens currently don't have a save payment method, 
            billingInfo.PaymentMethod.PaymentType =
                reloadForStarbucksCard.PaymentTokenDetails.PaymentTokenType.ToString();

            return ReloadStarbucksCardByCardNumberWithCreditCardOrPaymentToken(user, card, emailAddress,
                reloadForStarbucksCard,
                orderId, merchantInfo,
                billingInfo, market, true);
        }

        private ICardTransaction ReloadStarbucksCardByCardNumberWithPaymentMethod(
            IReloadForStarbucksCard reloadForStarbucksCard, string emailAddress, string userId, string market,
            IAssociatedCard card,
            StarbucksAccountProvider.Common.Models.IUser user)
        {
            var paymentMethod = GetPaymentMethod(userId, reloadForStarbucksCard.PaymentMethodId);
            if (paymentMethod == null)
            {
                throw new CardTransactionException(CardTransactionErrorResource.PaymentMethodNotFoundCode,
                    CardTransactionErrorResource.PaymentMethodNotFoundMessage);
            }

            var orderId = GetNewCommerceServerOrderId();

            var billingAddress = GetAddress(userId, paymentMethod);
            if (billingAddress == null)
                throw new CardTransactionException(CardTransactionErrorResource.BillingAddressNotFoundCode,
                    CardTransactionErrorResource.BillingAddressNotFoundMessage);

            IMerchantInfo merchantInfo = null;

            merchantInfo = GetMerchantInfo(CurrentSubMarket(market), reloadForStarbucksCard.Risk != null ? reloadForStarbucksCard.Risk.Platform : UnKnownPlatform);

            if (merchantInfo == null)
                throw new CardTransactionException(CardTransactionErrorResource.MerchantInfoNotFoundCode,
                    CardTransactionErrorResource.MerchantInfoNotFoundMessage);

            var billingInfo = CreateBillingInfo(emailAddress, billingAddress, paymentMethod, merchantInfo);
            if (billingInfo == null)
                throw new CardTransactionException(CardTransactionErrorResource.BillingInfoNotFoundCode,
                    CardTransactionErrorResource.BillingInfoNotFoundMessage);

            // All credit card and Paypal reloads come through this so use a try-finally so that we can clear the
            // surrogate CC number and Paypal agreement number for temporary payment methods.
            ICardTransaction cardTransaction = null;
            try
            {
                if (paymentMethod.PaymentType.ToLower().Contains("paypal"))
                {
                    // If this flag is set to true we will not process PayPal reloads.
                    if (_cardTransactionProviderSettingsSection.BlockPayPalReloads)
                    {
                        LogHelper.Log(
                            String.Format(
                                "In ReloadStarbucksCardByCardNumberWithPaymentMethod. Skipping PayPal reload. User Id = {0}, Order Id = {1}, Mid = {2}, Currency Code = {3}",
                                user.UserId, orderId, merchantInfo.Mid, merchantInfo.CurrencyCode),
                            "CardApi - ReloadCard", TraceEventType.Warning);

                        throw new Exception("_cardTransactionProviderSettingsSection.BlockPayPalReloads = true. Skipping PayPal reload.");
                    }
                    else
                    {
                        cardTransaction = ReloadStarbucksCardByCardNumberWithPayPal(user, card, emailAddress,
                            reloadForStarbucksCard, orderId,
                            merchantInfo, billingInfo, market);
                    }
                }
                else
                {
                    cardTransaction = ReloadStarbucksCardByCardNumberWithCreditCardOrPaymentToken(user, card, emailAddress,
                        reloadForStarbucksCard,
                        orderId, merchantInfo, billingInfo, market, isPaymentToken: false);
                }
            }
            finally
            {
                // Clear the account numbers so no hacker can reuse this payment method.
                if (paymentMethod != null && paymentMethod.IsTemporary)
                {
                    try
                    {
                        _cardProvider.ClearPaymentMethodSurrogateAndPaypalAccountNumbers(UserIdForTempPaymentMethods,
                            paymentMethod);
                    }
                    catch (Exception ex)
                    {
                        LogHelper.LogException(ex,
                            String.Format(
                                "Error in ReloadStarbucksCardByCardNumberWithPaymentMethod clearing account numbers.{0}\n{1}",
                                ex.Message, ex.StackTrace),
                            "CardApi - ReloadCard", TraceEventType.Error);

                        throw;
                    }
                }
            }
            return cardTransaction;
        }

        #region SaveToCommerceServer

        private void SaveOrder(string userId, string orderId, IEnumerable<IOrderLineItem> orderLineItems,
            IBillingInfo billingInfo,
            string transactionId, string billRequestId)
        {
            try
            {
                var userGuid = Guid.Empty;
                Guid.TryParse(userId, out userGuid);
                var channel =
                    OrderService.GetChannelFactory(
                        _cardTransactionProviderSettingsSection.OrderService.ServiceConfigurationName);
                var savedOrderBom = billingInfo.ToSavedOrderBom();
                savedOrderBom.BillRequestId = billRequestId;
                channel.Use(c =>
                    c.SaveOrder(orderId, userGuid,
                        orderLineItems.Select(p => p.ToBom()).ToArray(),
                        (billingInfo.Address == null) ? new Platform.Bom.Shared.UserAddress() : billingInfo.Address.ToBom(),
                        savedOrderBom, transactionId));
            }
            catch (Exception ex)
            {
                _loggingManager.Increment(string.Format("card_reload_SaveOrder_failed"));

                LogHelper.LogException(ex, string.Format("Error while saving order.{0}\n{1}", ex.Message, ex.StackTrace),
                    "CardApi - ReloadCard", TraceEventType.Error);
            }
        }

        #endregion

        #region Enqueue Card Reload Confirmation Stage Email

        private void EmailReloadConfirmation(string submarket, string emailAddress, string orderId, decimal amount,
            IBillingInfo billingInfo, List<IOrderLineItem> items)
        {
            try
            {
                IStarbucksCardReloadConfirmation reloadConfirmation = new StarbucksCardReloadConfirmation();
                var locale = _settingsProvider.GetLocaleByMarket(submarket).FirstOrDefault();
                var siteCoreCountry = _settingsProvider.GetSiteCoreSiteFromMarket(submarket);
                reloadConfirmation.Site = siteCoreCountry;
                reloadConfirmation.Language = locale;

                // billing address
                reloadConfirmation.FullName = (billingInfo == null || billingInfo.Address == null) ? "" : billingInfo.Address.AddressName;

                reloadConfirmation.AddressLines = (billingInfo == null || billingInfo.Address == null)
                    ? ""
                    : (billingInfo.Address.AddressLine1 + Environment.NewLine + billingInfo.Address.AddressLine2);

                reloadConfirmation.CityStateZip = (billingInfo == null || billingInfo.Address == null)
                    ? "" 
                    : billingInfo.Address.City + ", " + billingInfo.Address.CountrySubdivision + " " + billingInfo.Address.PostalCode;

                reloadConfirmation.EmailAddress = emailAddress;
                reloadConfirmation.CurrencyType = _settingsProvider.GetCurrencyCodeForMarketCode(submarket);

                reloadConfirmation.DateReceived = DateTime.Now.ToString("D", CultureInfo.CreateSpecificCulture(locale));
                reloadConfirmation.OrderID = orderId;
                reloadConfirmation.Items = items;

                var brokerStageEmailArgs = reloadConfirmation.ToMailMessageBroker();
                _messageBroker.SubmitMessageWithRepeater(brokerStageEmailArgs);
            }
            catch (Exception ex)
            {
                // Log it and eat the exception.
                LogHelper.LogException(ex,
                    string.Format(
                        "EmailReloadConfirmation called for emailAddress {0}. orderId {1}  PaymentMethodId {2}",
                        emailAddress,
                        orderId, billingInfo.PaymentMethod == null ? "" : billingInfo.PaymentMethod.PaymentMethodId),
                    "CardApi - ReloadCard", TraceEventType.Error);
            }
        }

        #endregion

        private void CallPaymentServiceClientVoidAuth(AuthRequest authRequest, IAuthResult authResult)
        {
            var voidAuthRequest = new VoidRequest();
            voidAuthRequest.Market = authRequest.Market;
            voidAuthRequest.TransactionType = authRequest.TransactionType;
            voidAuthRequest.CurrencyCode = authRequest.CurrencyCode;

            if (authResult != null && authResult.CreditCardAuthorizationDetails != null)
            {
                voidAuthRequest.AuthorizationRequestId = authResult.CreditCardAuthorizationDetails.RequestId;
                voidAuthRequest.MerchantReferenceCode = authResult.CreditCardAuthorizationDetails.MerchantReferenceCode;
                voidAuthRequest.TotalAmount = authResult.CreditCardAuthorizationDetails.AuthorizedAmount;
            }

            _paymentServiceApiClient.VoidAuth(voidAuthRequest);
        }

        private void VoidAuthorization(AuthRequest authRequest, IAuthResult authResult, string submarket)
        {
            _loggingManager.Increment("card_reload_valuelink_transaction_failed");
            _loggingManager.Increment(string.Format("card_reload_valuelink_transaction_failed_{0}",
                string.IsNullOrWhiteSpace(submarket) ? string.Empty : submarket));
            try
            {
                CallPaymentServiceClientVoidAuth(authRequest, authResult);
            }
            catch (Exception voidEx)
            {
                _loggingManager.Increment("card_void_transaction_failed");

                LogHelper.LogException(voidEx,
                    message:
                        String.Format("PS API void auth failed: Transaction Id = {0}, Message = {1}, Stack = {2}",
                            authRequest.TransactionId, voidEx.Message, voidEx.StackTrace),
                    title: "CardApi - Reload VoidAuthorization", eventType: TraceEventType.Error);
                throw;
            }
        }

        private ICardTransaction ReloadStarbucksCardByCardNumberWithCreditCardOrPaymentToken(StarbucksAccountProvider.Common.Models.IUser user,
            IAssociatedCard card, string emailAddress,
            IReloadForStarbucksCard reloadForStarbucksCard,
            string orderId,
            IMerchantInfo merchantInfo,
            IBillingInfo billingInfo,
            string submarket,
          bool isPaymentToken)
        {
            IAuthResult authResult = null;

            decimal amount = reloadForStarbucksCard.Amount;
            List<IOrderLineItem> items = GetReloadLineItem(amount, merchantInfo.FraudThreshold);
            var merchantReferenceCode = orderId;
            IFailedOrder failedOrder = null;
            AuthRequest authRequest = null;
            Stopwatch reloadWatch = new Stopwatch();

            try
            {
                LogHelper.Log(
                    String.Format("In ReloadStarbucksCardByCardNumberWithCreditCardOrApplePay. User Id = {0}, Order Id = {1}, Mid = {2}, Currency Code = {3}",
                    (user == null) ? string.Empty : user.UserId, orderId, merchantInfo.Mid, merchantInfo.CurrencyCode),
                    "CardApi - ReloadCard", TraceEventType.Information);

                //Mocking the Amount to Get PaymentNetworkTransactionId.
                if (IsChasePayPos(reloadForStarbucksCard) && _cardTransactionProviderSettingsSection.MockAmountEnabled)
                {
                    var mockitems = GetReloadLineItem(_cardTransactionProviderSettingsSection.MockAmount,
                        merchantInfo.FraudThreshold);

                    authRequest = CreateAuthRequestForCreditCardOrPaymentToken(TransactionTypes.Reload, reloadForStarbucksCard,
                    submarket, billingInfo, mockitems, user == null ? null : user.UserId, emailAddress, card, orderId, isPaymentToken);
                }
                else
                {
                    authRequest = CreateAuthRequestForCreditCardOrPaymentToken(TransactionTypes.Reload, reloadForStarbucksCard,
                    submarket, billingInfo, items, user == null ? null : user.UserId, emailAddress, card, orderId, isPaymentToken);
                }

                var platform = GetPlatform(reloadForStarbucksCard);

                failedOrder = CreateFailedOrderObject(billingInfo, merchantInfo, merchantReferenceCode, card.Number,
                    items.First().OrderType,
                    (user == null) ? string.Empty : user.UserId,
                    platform,
                    authRequest.TransactionId,
                    -1);

                reloadWatch.Start();

                authResult = _paymentServiceApiClient.Auth(authRequest);
                reloadWatch.Stop();
                LogTimer(reloadWatch, "CC PSAPI Auth");

                LogPSApiCallDetails.LogAuthResult(authRequest, authResult);
            }
            catch (Exception ex)
            {
                reloadWatch.Stop();
                LogTimer(reloadWatch, "CC PSAPI Auth Exception");

                LogPSApiCallDetails.LogAuthException(authRequest, ex);
                _loggingManager.Increment("card_reload_order_Auth_error");

                // Save the failed order and rethrow. 
                SaveFailedOrder(failedOrder);

                if (ex is Api.Sdk.PaymentService.Common.SecurePaymentException)
                {
                    throw;
                }

                throw new CardTransactionException(CardTransactionErrorResource.CannotAuthorizeCreditCardCode,
                    CardTransactionErrorResource.CannotAuthorizeCreditCardMessage);
            }

            if (authResult == null)
            {
                // log it, save the failed order, and throw an exception. 
                _loggingManager.Increment("card_reload_order_Auth_failed with result == null");

                LogHelper.Log(
                    String.Format("Result from calling Auth() is null for user Id {0}, orderId {1}, card id {2}.",
                        user.UserId, orderId, card.CardId),
                    "CardApi - ReloadCard", TraceEventType.Error);

                SaveFailedOrder(failedOrder);

                throw new CardTransactionException(CardTransactionErrorResource.CannotAuthorizeCreditCardCode,
                    CardTransactionErrorResource.CannotAuthorizeCreditCardMessage);
            }

            if (authResult.DecisionType != DecisionType.Accept && authResult.DecisionType != DecisionType.Review)
            {
                // log it, save the failed order, and throw an exception. 
                var decisionReason = authResult.DecisionReason == null ? -1 : authResult.DecisionReason.Code;
                _loggingManager.Increment("card_reload_order_Auth_failed");
                LogHelper.Log(
                    String.Format(
                        "Result from payment service Auth call is not Accept or Review: User Id {0}, OrderId {1}. Card id {2}. DecisionType: {3}, Reason Code: {4}.",
                         (user == null) ? string.Empty : user.UserId, orderId, card.CardId, authResult.DecisionType, decisionReason),
                    "CardApi - ReloadCard", TraceEventType.Warning);

                SaveFailedOrder(failedOrder, reloadForStarbucksCard, authResult, merchantReferenceCode, decisionReason);

                // Map decision codes to legacy exception. Default is generic one.
                var legacyException = MapReasonCodeToException(decisionReason);
                if (legacyException != null)
                {
                    throw legacyException;
                }

                throw new CardTransactionException(CardTransactionErrorResource.CannotAuthorizeCreditCardCode,
                    CardTransactionErrorResource.CannotAuthorizeCreditCardMessage);
            }
            else
            {
                //posting Auth success to ChasePayConfirmation NSB
                SendChasePayConfirmation(reloadForStarbucksCard, authResult, merchantReferenceCode);
            }

            ICardTransaction cardResult;
            try
            {
                cardResult = Reload(merchantInfo, card.Number, amount);
            }
            catch (Exception ex)
            {
                LogPSApiCallDetails.LogReloadExceptionCC(authRequest, ex);

                SaveFailedOrder(failedOrder);
                VoidAuthorization(authRequest, authResult, submarket);

                throw;
            }

            //Handle any valuelink errors

            //TODO: Change to return exc instead of try/catch/throw

            var vlResponse = CardIssuerExceptionHandler.HandleCardIssuerResponse(cardResult.ResponseCode);
            if (vlResponse != null)
            {
                LogHelper.Log(
                    String.Format(
                        "Card Issuer Reload failed with error response (not exception): User Id {0}, Order Id {1}, Response Code {2}, generating exception with code: {3}.",
                       (user == null) ? string.Empty : user.UserId, orderId, cardResult.ResponseCode, vlResponse.Code),
                    "CardApi - ReloadCard", TraceEventType.Error);

                SaveFailedOrder(failedOrder);
                VoidAuthorization(authRequest, authResult, submarket);

                throw vlResponse;
            }

            if (!cardResult.TransactionSucceeded)
            {
                _loggingManager.Increment("card_reload_valuelink_transaction_failed");
                _loggingManager.Increment(String.Format("card_reload_valuelink_transaction_failed_{0}",
                    String.IsNullOrWhiteSpace(submarket) ? String.Empty : submarket));

                LogHelper.Log(
                    String.Format(
                        "Card Issuer Reload Transaction not Succeeded: User Id {0}, Order Id {1}, CardResult Response Code{2}",
                        (user == null) ? string.Empty : user.UserId, orderId, cardResult.ResponseCode),
                    "CardApi - ReloadCard", TraceEventType.Warning);

                SaveFailedOrder(failedOrder);
                VoidAuthorization(authRequest, authResult, submarket);

                return cardResult;
            }

            // Log successful VL Reload response.
            var properties = new Dictionary<string, object>();
            properties.Add("UserId", (user == null) ? string.Empty : user.UserId.ToDisplay());
            properties.Add("CardId", card.CardId.ToDisplay());
            properties.Add("PaymentMethodId", reloadForStarbucksCard.PaymentMethodId);
            properties.Add("OrderId", orderId.ToDisplay());
            properties.Add("EmailAddress", emailAddress.ToDisplay());
            properties.Add("ResponseCode", cardResult.ResponseCode.ToDisplay());

            LogReloadDetails(TraceEventType.Information, null,
                "CardApi - Reload Value Link",
                "ReloadStarbucksCardByCardNumberWithCreditCardOrApplePay, VL Reload result.", properties);

            // Conditionally bill and update ledger tables
            //4. call Cybersource to bill
            BillRequest billRequest = null;
            IBillResult billResult = null;
            try
            {
                billRequest = CreateBillRequest(TransactionTypes.Reload,
                    user == null ? null : user.UserId,
                    authResult, submarket, billingInfo.Currency, items, reloadForStarbucksCard);

                reloadWatch.Restart();

                billResult = _paymentServiceApiClient.Bill(billRequest);
                reloadWatch.Stop();
                LogTimer(reloadWatch, "CC PSAPI Bill");

                LogPSApiCallDetails.LogBillResult(billRequest, billResult);
            }
            catch (Exception ex)
            {
                _loggingManager.Increment("card_reload_creditcard_transactionfailed");
                _loggingManager.Increment(string.Format("card_reload_valuelink_transaction_failed_{0}",
                    string.IsNullOrEmpty(submarket) ? string.Empty : submarket));

                // Since we are seeing timeouts from the Bill call which eventually succeed,
                // don't want to rollback the SVC reload if config flag tells us to. 
                // Save this as a failed order with a new Reason Code so that Cust. Srvc. can review and resolve.
                if (SkipVLCardReloadRollbackOnBillTimeout && IsTimeoutException(ex))
                {
                    LogPSApiCallDetails.LogBillException(billRequest, ex, cardResult, "Skipping card load rollback after PSAPI Bill exception.");

                    ProcessValueLinkResponse(card.Number, cardResult.ToCardIssuer());

                    SaveFailedOrder(failedOrder, PSApiBillTimeoutExceptionReasonCode);
                }
                else
                {
                    LogPSApiCallDetails.LogBillException(billRequest, ex, cardResult);

                    SaveFailedOrder(failedOrder, PSApiBillExceptionReasonCode);

                    PerformCardIssuersVoidAndVoidAuth(cardResult.ToCardIssuer(), merchantInfo, submarket, authRequest,
                        authResult);
                }

                throw;
            }

            if (billResult == null ||
                (billResult.Decision != DecisionType.Accept && billResult.Decision != DecisionType.Review))
            {
                LogHelper.Log(
                    String.Format(
                        "Error while settling transaction in Cybersource for card {0} and AuthRequestId {1}, User Id {2}, Order Id {3}.",
                        card.Number, (authResult.CreditCardAuthorizationDetails == null)
                            ? null
                            : authResult.CreditCardAuthorizationDetails.RequestId, (user == null) ? string.Empty : user.UserId, orderId),
                    "CardApi - ReloadCard", TraceEventType.Warning);

                SaveFailedOrder(failedOrder);
                PerformCardIssuersVoidAndVoidAuth(cardResult.ToCardIssuer(), merchantInfo, submarket, authRequest,
                    authResult);

                throw new CardTransactionException(CardTransactionErrorResource.CreditCardBillFailedCode,
                    CardTransactionErrorResource.CreditCardBillFailedMessage);
            }

            ProcessValueLinkResponse(card.Number, cardResult.ToCardIssuer());

            EmailReloadConfirmation(submarket, emailAddress, orderId, amount, billingInfo, items);
            var userId = user != null ? user.UserId : String.Empty;
            SaveOrder(userId, orderId, items, billingInfo, cardResult.TransactionId, billResult.RequestId);

            return cardResult;
        }

        private void SendChasePayConfirmation(
           IReloadForStarbucksCard reloadForStarbucksCard,
            IAuthResult authResult, string merchantReferenceCode)
        {
            ConfirmPay confirmPay = null;
            try
            {
                if (!IsChasePayPos(reloadForStarbucksCard))
                {
                    return;
                }

                //reading the mockSessionId from Configuration
                var transactionReferenceKey = _cardTransactionProviderSettingsSection.MockTransactionReferenceKeyEnabled
               ? _cardTransactionProviderSettingsSection.MockTransactionReferenceKey
               : reloadForStarbucksCard.PaymentTokenDetails.MobilePOS.TransactionReferenceKey;
                confirmPay = new ConfirmPay
                {
                    Platform = reloadForStarbucksCard.Platform,
                    Amount = reloadForStarbucksCard.Amount.ToString(CultureInfo.InvariantCulture),
                    SessionId = string.Empty,
                    TransactionAuthCode = (authResult == null || authResult.CreditCardAuthorizationDetails == null)
                    ? null : authResult.CreditCardAuthorizationDetails.AuthCode,
                    TransactionDateTime = (authResult == null || authResult.CreditCardAuthorizationDetails == null)
                    ? DateTime.UtcNow.ToString("yyyyMMddHHmmss") : authResult.CreditCardAuthorizationDetails.AuthorizationDateTime.ToString("yyyyMMddHHmmss"),
                    TransactionId = merchantReferenceCode,
                    TransactionReferenceKey = transactionReferenceKey,
                    TransactionStatus = (authResult == null) ? "D" : GetTransactionStatus(authResult.DecisionType),
                    VisaTransactionId = (authResult == null || authResult.CreditCardAuthorizationDetails == null)
                    ? null : authResult.CreditCardAuthorizationDetails.PaymentNetworkTransactionId
                };

                _bus.SendPayConfirmation(confirmPay);
                var message = string.Format(
                  "Successfully send the ConfirmPay message to NSB.Message:{0} ", confirmPay.Serialize());
                LogHelper.Log(message, "CardApi - ReloadCard", TraceEventType.Information);

            }
            catch (Exception ex)
            {
                var errorMessage = string.Format(
                    "Error while posting message to ChasePayConfirmation to NSB.Message:{0} ", confirmPay.Serialize());
                LogHelper.LogException(ex, errorMessage, "CardApi - ReloadCard", TraceEventType.Error);
            }
        }

        private string GetTransactionStatus(DecisionType decisionType)
        {
            var response = "A";
            switch (decisionType)
            {
                case DecisionType.Accept:
                case DecisionType.None:
                case DecisionType.Review:
                    response = "A";
                    break;
                case DecisionType.Deny:
                    response = "D";
                    break;
            }
            return response;
        }

        public bool IsTimeoutException(Exception ex)
        {
            Starbucks.Api.Sdk.PaymentService.Common.SecurePaymentException secEx =
                ex as Starbucks.Api.Sdk.PaymentService.Common.SecurePaymentException;
            if (secEx != null)
            {
                if (secEx.ApiErrorCode == SecurePaymentErrorResource.TimeoutProcessingPaymentInformationCode ||
                    secEx.ApiErrorCode == SecurePaymentErrorResource.UnexpectedErrorCode)
                {
                    return true;
                }
                ApiException apiExInner = secEx.InnerException as ApiException;
                if (apiExInner != null)
                {
                    if (apiExInner.HttpStatus == HttpStatusCode.GatewayTimeout ||
                        apiExInner.HttpStatus == HttpStatusCode.RequestTimeout
                        || apiExInner.StatusCode == WebExceptionStatus.Timeout)
                        return true;
                }
            }
            return false;
        }

        public void SaveFailedOrder(IFailedOrder failedOrder, IReloadForStarbucksCard reloadForStarbucksCard,
            IAuthResult authResult, string merchantReferenceCode, int? reasonCode = null)
        {

            SaveFailedOrder(failedOrder, reasonCode);
            SendChasePayConfirmation(reloadForStarbucksCard, authResult, merchantReferenceCode);
        }
        public void SaveFailedOrder(IFailedOrder failedOrder, int? reasonCode = null)
        {
            if (failedOrder != null)
            {
                // We don't have a MerchantKey for PayPal transactions but the OrderManagementProvider.SaveFailedOrder requires one so set it to a dummy value.
                failedOrder.MerchantKey = "starbucksreload";

                if (reasonCode != null)
                {
                    failedOrder.ReasonCode = reasonCode;
                }

                try
                {
                    _orderManagementProvider.SaveFailedOrder(failedOrder);
                }
                catch (Exception ex)
                {
                    LogHelper.LogException(ex, String.Format(
                        "SaveFailedOrder exception error for orderId {0}. Message {1}, RequestId: {2}, BillingEmail: {3}, UserId: {4}",
                        failedOrder.OrderId, ex.Message, failedOrder.RequestedId, failedOrder.BillingEmail,
                        failedOrder.UserId),
                        "CardApi - ReloadCard", TraceEventType.Error);
                }
            }
        }

        public static SecurePaymentException MapReasonCodeToException(int decisionReasonCode)
        {
            switch (decisionReasonCode)
            {
                case 210:
                    return new SecurePaymentException(SecurePaymentErrorResource.CreditCardInsufficientFundsCode,
                        SecurePaymentErrorResource.CreditCardInsufficientFundsMessage);
                case 400:
                    return new SecurePaymentException(SecurePaymentErrorResource.FraudScoreExceedsThresholdCode,
                        SecurePaymentErrorResource.FraudScoreExceedsThresholdMessage);
                case 481:
                    return new SecurePaymentException(SecurePaymentErrorResource.OrderRejectedByDecisionManagerCode,
                        SecurePaymentErrorResource.OrderRejectedByDecisionManagerMessage);
                case 500:
                    return new SecurePaymentException(SecurePaymentErrorResource.UnexpectedErrorCode,
                        SecurePaymentErrorResource.UnexpectedErrorCode);
                case 519:
                    return new SecurePaymentException(SecurePaymentErrorResource.CreditCardNumberIsInvalidCode,
                        SecurePaymentErrorResource.CreditCardNumberIsInvalidMessage);
                case 520:
                    return new SecurePaymentException(SecurePaymentErrorResource.CreditCardNumberDoesNotMatchTypeCode,
                        SecurePaymentErrorResource.CreditCardNumberDoesNotMatchTypeMessage);
                case 537:
                    return new SecurePaymentException(SecurePaymentErrorResource.BillingInformationMissingFieldsCode,
                        SecurePaymentErrorResource.BillingInformationMissingFieldsMessage);
                case 538:
                    return
                        new SecurePaymentException(
                            SecurePaymentErrorResource.BillingInformationContainsInvalidFieldsCode,
                            SecurePaymentErrorResource.BillingInformationContainsInvalidFieldsMessage);
                case 539:
                    return new SecurePaymentException(
                        SecurePaymentErrorResource.TimeoutProcessingPaymentInformationCode,
                        SecurePaymentErrorResource.TimeoutProcessingPaymentInformationMessage);
                case 540:
                    return new SecurePaymentException(SecurePaymentErrorResource.CreditCardIsExpiredCode,
                        SecurePaymentErrorResource.CreditCardIsExpiredMessage);
                case 541:
                    return new SecurePaymentException(SecurePaymentErrorResource.CreditCardInsufficientFundsCode,
                        SecurePaymentErrorResource.CreditCardInsufficientFundsMessage);
                case 542:
                    return new SecurePaymentException(SecurePaymentErrorResource.InvalidCVNCode,
                        SecurePaymentErrorResource.InvalidCVNMessage);
                case 543:
                    return new SecurePaymentException(SecurePaymentErrorResource.CreditCardNumberIsInvalidCode,
                        SecurePaymentErrorResource.CreditCardNumberIsInvalidMessage);
                case 544:
                    return new SecurePaymentException(SecurePaymentErrorResource.AVSFailedCode,
                        SecurePaymentErrorResource.AVSFailedMessage);
                case 552:
                    return new SecurePaymentException(SecurePaymentErrorResource.AFSFailedCode,
                        SecurePaymentErrorResource.AFSFailedMessage);
                case 553:
                    return new SecurePaymentException(SecurePaymentErrorResource.GeneralPaymentErrorCode,
                        SecurePaymentErrorResource.GeneralPaymentErrorMessage);
                default:
                    return null;
            }
        }

        private void PerformCardIssuersVoidAndVoidAuth(
            Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction cardTransaction,
            IMerchantInfo merchantInfo,
            string submarket,
            AuthRequest authRequest,
            IAuthResult authResult)
        {
            try
            {
                _cardIssuer.VoidRequest(cardTransaction, merchantInfo);
            }
            catch (Exception ex)
            {
                // Log but swallow since we need to void auth.
                var billingEmail = string.Empty;
                var paypalEmail = string.Empty;
                if (authRequest.BillingInfo != null)
                {
                    billingEmail = authRequest.BillingInfo.Email;
                    paypalEmail = authRequest.BillingInfo == null
                        ? string.Empty
                        : authRequest.BillingInfo.PaymentMethod == null
                            ? string.Empty
                            : authRequest.BillingInfo.PaymentMethod.PayPalDetails == null
                                ? string.Empty
                                : authRequest.BillingInfo.PaymentMethod.PayPalDetails.PayPayEmail;
                }
                LogHelper.LogException(ex,
                    String.Format(
                        "PerformCardIssuersVoidAndVoidAuth Exception calling VoidRequest. Billing email: {0} PayPal account email: {1} PayPal Auth ReconciliationId: {2}",
                        billingEmail, paypalEmail,
                        authResult.CreditCardAuthorizationDetails == null
                            ? null
                            : authResult.CreditCardAuthorizationDetails.ReconciliationId),
                    "CardApi - Reload", TraceEventType.Error);
            }

            try
            {
                VoidAuthorization(authRequest, authResult, submarket);
            }
            catch (Exception)
            {
                // already logged inside VoidAuthorization
                _loggingManager.Increment("card_void_transaction_failed");
            }
        }

        private string GetMarket(string market, IReloadForStarbucksCard reloadForStarbucksCard)
        {
            // Prefer market specified in request, if there. Else get from userAccount, if we have one.
            if (reloadForStarbucksCard != null && reloadForStarbucksCard.Risk != null && !string.IsNullOrEmpty(reloadForStarbucksCard.Risk.Market))
            {
                market = reloadForStarbucksCard.Risk.Market;
            }
            market = market ?? String.Empty;

            // To handle the situation where the alternate country code is stored with a user, map to what we need for the fraud check.
            if (_normalizeMarket.ContainsKey(market))
            {
                return _normalizeMarket[market] ?? market;
            }
            return market;
        }

        private bool IsChasePayPos(IReloadForStarbucksCard reloadForStarbucksCard)
        {
            var result = reloadForStarbucksCard != null && reloadForStarbucksCard.PaymentTokenDetails != null &&
                reloadForStarbucksCard.PaymentTokenDetails.PaymentTokenType == PaymentTokenType.ChasePay &&
                reloadForStarbucksCard.PaymentTokenDetails.MobilePOS != null;

            return result;
        }


        private AuthRequest CreateAuthRequestForCreditCardOrPaymentToken(TransactionTypes type,
            IReloadForStarbucksCard reloadForStarbucksCard,
            string market, IBillingInfo billingInfo,
            List<IOrderLineItem> orderLineItems,
            string userId, string email, IAssociatedCard card, string orderId, bool isPaymentToken)
        {
            if (billingInfo.Address == null && !IsChasePayPos(reloadForStarbucksCard))
            {
                throw new CardTransactionException(CardTransactionErrorResource.BillingAddressNotFoundCode,
                    CardTransactionErrorResource.BillingAddressNotFoundMessage);
            }

            if (billingInfo.PaymentMethod == null)
            {
                throw new CardTransactionException(CardTransactionErrorResource.PaymentMethodNotFoundCode,
                    CardTransactionErrorResource.PaymentMethodNotFoundMessage);
            }

            // Setup the defaults for when reloadForStarbucksCard.Risk is not present.
            DeviceReputationData deviceReputationData = null;
            string platform = null;
            string ccAgentName = null;
            string isLoggedIn = IsLoggedInDefault;
            string authCurrencyCode = billingInfo.Currency;

            if (reloadForStarbucksCard.Risk != null)
            {
                platform = reloadForStarbucksCard.Risk.Platform;
                ccAgentName = reloadForStarbucksCard.Risk.CcAgentName;

                if (reloadForStarbucksCard.Risk.IsLoggedIn != null)
                {
                    isLoggedIn = reloadForStarbucksCard.Risk.IsLoggedIn.ToString();
                }

                if (!string.IsNullOrWhiteSpace(market))
                {
                    authCurrencyCode = _settingsProvider.GetCurrencyCodeForMarketCode(market);
                }

                if (reloadForStarbucksCard.Risk.IovationFields != null)
                {
                    deviceReputationData = new DeviceReputationData()
                    {
                        DeviceFingerprint = reloadForStarbucksCard.Risk.IovationFields.DeviceFingerprint,
                        IpAddress = string.IsNullOrEmpty(reloadForStarbucksCard.Risk.IovationFields.IpAddress) ? reloadForStarbucksCard.IpAddress : reloadForStarbucksCard.Risk.IovationFields.IpAddress
                    };
                }
            }

            var request = new AuthRequest()
            {
                TransactionType = type,
                TransactionId = orderId,
                Platform = platform,
                Market = market,
                CurrencyCode = authCurrencyCode,
                Shipping = null,
                Basket = orderLineItems.ToPaymentServiceApiSdkOrderItemList(),
                Risk = new AuthRiskInfo()
                {
                    UserId = userId,
                    SignedIn = isLoggedIn,
                    OrderMessage = string.Empty,
                    CCAgentName = ccAgentName,
                    DeviceReputation = deviceReputationData,
                    DecisionManager = GetDecisionManagerData(userId, reloadForStarbucksCard, card, billingInfo),
                    StarbucksCardDetails = GetPaymentServiceApiSdkCard(card, platform)
                },

                BillingInfo = (isPaymentToken == true) ? GetPaymentServiceApiSdkBillingInfoForPaymentToken(billingInfo, reloadForStarbucksCard.PaymentTokenDetails, email) :
                                GetPaymentServiceApiSdkBillingInfo(billingInfo, email)
            };

            return request;
        }

        public static Api.Sdk.PaymentService.Models.BillingInfo GetPaymentServiceApiSdkBillingInfo(
            IBillingInfo billingInfo, string email)
        {
            if (billingInfo == null ||
                billingInfo.PaymentMethod == null ||
                billingInfo.Address == null ||
                string.IsNullOrEmpty(email))
            {
                // This should not happen but log and throw if it does.
                LogHelper.Log(
                    String.Format(
                        "GetPaymentServiceApiSdkBillingInfo null parameter. email: {0} billingInfo.PaymentMethod.PaymentMethodId: {1} billingInfo.AuthRequestId: {2}",
                        email ?? "",
                        billingInfo == null
                            ? string.Empty
                            : billingInfo.PaymentMethod == null
                                ? string.Empty
                                : billingInfo.PaymentMethod.PaymentMethodId,
                        billingInfo == null
                            ? string.Empty
                            : billingInfo == null ? string.Empty : billingInfo.AuthRequestId),
                    "CardApi - Reload", TraceEventType.Error);
                throw new CardTransactionException(CardTransactionErrorResource.BillingInfoNotFoundCode,
                    CardTransactionErrorResource.BillingInfoNotFoundMessage);
            }

            return new Api.Sdk.PaymentService.Models.BillingInfo()
            {
                Email = email,
                PaymentMethod = billingInfo.PaymentMethod.ToPaymentServiceApiSdkPaymentMethod(),
                AddressInfo = billingInfo.Address.ToPaymentServiceApiSdkAddress()
            };
        }

        public static Api.Sdk.PaymentService.Models.BillingInfo GetPaymentServiceApiSdkBillingInfoForPaymentToken(IBillingInfo billingInfo, IPaymentTokenDetails token, string email)
        {
            if (billingInfo == null || billingInfo.PaymentMethod == null)
            {
                return null;
            }

            return new Api.Sdk.PaymentService.Models.BillingInfo()
            {
                Email = email,
                PaymentMethod = billingInfo.PaymentMethod.ToPaymentServiceApiSdkPaymentTokenForPaymentToken(token),
                AddressInfo = billingInfo.Address.ToPaymentServiceApiSdkAddress()
            };
        }

        private BillRequest CreateBillRequest(TransactionTypes type, string userId,
            IAuthResult authResult,
            string market, string currency,
            List<IOrderLineItem> orderLineItems, IReloadForStarbucksCard reloadForStarbucksCard)
        {
            string billCurrency = currency;

            if (reloadForStarbucksCard.Risk != null && !string.IsNullOrWhiteSpace(market))
            {
                billCurrency = _settingsProvider.GetCurrencyCodeForMarketCode(market);
            }

            var request = new BillRequest()
            {
                UserId = userId,
                TransactionType = type,
                Market = market,
                CurrencyCode = billCurrency,
                Basket = orderLineItems.ToPaymentServiceApiSdkOrderItemList(),
                MerchantReferenceCode = (authResult.CreditCardAuthorizationDetails == null)
                    ? null
                    : authResult.CreditCardAuthorizationDetails.MerchantReferenceCode,
                AuthorizationRequestId = (authResult.CreditCardAuthorizationDetails == null)
                    ? null
                    : authResult.CreditCardAuthorizationDetails.RequestId
            };
            return request;
        }

        private DecisionManagerData GetDecisionManagerData(string userId, IReloadForStarbucksCard reloadForStarbucksCard,
            ICard card, IBillingInfo billingInfo)
        {
            string orderSource = null;
            if (reloadForStarbucksCard.Risk != null && !string.IsNullOrWhiteSpace(reloadForStarbucksCard.Risk.Platform))
            {
                orderSource = reloadForStarbucksCard.Risk.Platform;
            }

            if (orderSource == null && !string.IsNullOrWhiteSpace(reloadForStarbucksCard.Platform))
            {
                orderSource = reloadForStarbucksCard.Platform;
            }

            if (orderSource == null)
            {
                orderSource = defaultOrderSource;
            }

            var decisionManagerData = new DecisionManagerData()
            {
                SessionId = reloadForStarbucksCard.SessionId,
                MerchantData = new Api.Sdk.PaymentService.Models.MerchantData()
                {
                    UserId = userId,
                    OrderSource = orderSource,
                    CardId = card.CardId,
                    IsAutoReload = false,
                    PaymentMethodNickname =
                        billingInfo.PaymentMethod == null ? string.Empty : billingInfo.PaymentMethod.Nickname,
                    PayPalPayerId = billingInfo.PayPalPayerId,
                    PurchaserIpAddress = !string.IsNullOrEmpty(reloadForStarbucksCard.IpAddress) ?
                                             reloadForStarbucksCard.IpAddress :
                                             (reloadForStarbucksCard.Risk != null && reloadForStarbucksCard.Risk.IovationFields != null) ?
                                                reloadForStarbucksCard.Risk.IovationFields.IpAddress :
                                                null,

                    PurchaserSessionDuration = reloadForStarbucksCard.PurchaserSessionDuration,
                }
            };
            return decisionManagerData;
        }

        #endregion

        #region CreditCardHelpers

        private static StarbucksCardDetails GetPaymentServiceApiSdkCard(IAssociatedCard card, string platform)
        {
            return card == null
                ? null
                : new StarbucksCardDetails()
                {
                    CardId = card.CardId,
                    CardNumber = card.Number,
                    CardNickname = card.Nickname,
                    Balance = card.Balance,
                    CurrencyCode = card.Currency,
                    CardType = card.Type.ToString(),
                    CardMarket = card.SubMarketCode,
                    MarketingRegSource = card.MarketingRegSourceCode,
                    PlatformRegSource = card.PlatformRegSourceCode,
                    IsPrimary = card.IsDefault,
                    IsPartner = card.IsPartner,
                    IsDigital = card.IsDigitalCard,
                    IsOwner = card.IsOwner,
                    AutoReloadProfile = card.AutoReloadProfile.ToPaymentServiceApiSdkAutoReloadProfile()
                };
        }

        #endregion

        #region PayPal

        private static IPaymentDetails BuildPayPalPaymentDetails(string cardNameForDisplay, string currency,
            decimal amount)
        {
            var payPalItems = new List<IPaymentDetailsLineItem>
            {
                new PaymentDetailsLineItem
                {
                    ItemName = String.Format("${0} auto-reload for Starbucks Card {1}", amount, cardNameForDisplay),
                    ItemAmount = amount,
                    Quantity = 1
                }
            };


            string description = String.Empty;
            var paymentDetails = new PaymentDetails
            {
                Currency = currency,
                LineItems = payPalItems.ToArray(),
                OrderDescription = description,
                OrderAmount = amount,
                MaxAmount = amount,
                ShippingAmount = 0,
                TaxAmount = 0
            };

            return paymentDetails;
        }

        private ICardTransaction ReloadStarbucksCardByCardNumberWithPayPal(
            StarbucksAccountProvider.Common.Models.IUser user, IAssociatedCard card, string emailAddress,
            IReloadForStarbucksCard reloadForStarbucksCard, string orderId, IMerchantInfo merchantInfo,
            IBillingInfo billingInfo, string submarket)
        {
            // A few helpful notes about the data returned from the various PayPal calls - based on some testing.
            // The DoReferenceTransaction (authorization on a billing agreement Id) does not fill in BillingAddress, OrderId, PayerInformation, 
            // PaymentDetails, or Subject.
            //
            // UpdateBillingAgreement call returns the BillingAddress but no address in the PayerInformation but it does not return Subject.
            //
            // GetTransactionDetails returns Subject but not BillingAddress. PayerInformation is filled in but it does not include the address field. 
            //
            // We need all three calls to get all the information needed for the Fraud Check.
            //
            // With limited testing is appears that none of these three PayPal calls returns an address in the PayerInformation.Address fields.

            if (card == null)
                throw new CardTransactionException(CardTransactionErrorResource.CardNotFoundCode,
                    CardTransactionErrorResource.CardNotFoundMessage);

            var reloadDetails = new ReloadTransactionRequest();
            reloadDetails.Amount = reloadForStarbucksCard.Amount;
            reloadDetails.UserId = (user != null) ? user.UserId : String.Empty;
            reloadDetails.Submarket = submarket;
            reloadDetails.EmailAddress = emailAddress;
            reloadDetails.MerchantInfo = merchantInfo;
            reloadDetails.OrderId = orderId;
            reloadDetails.BillingInfo = billingInfo;
            reloadDetails.IpAddress = reloadForStarbucksCard.IpAddress;
            reloadDetails.Platform = reloadForStarbucksCard.Platform;
            reloadDetails.Risk = reloadForStarbucksCard.Risk;
            reloadDetails.Card = card;

            List<IOrderLineItem> items = GetReloadLineItem(reloadDetails.Amount, merchantInfo.FraudThreshold);
            reloadDetails.Items = items;
            Stopwatch reloadWatch = new Stopwatch();

            // For reporting auth failure or doing a fraud check we need the correct address from PayPal.
            var paymentMethod = reloadDetails.BillingInfo == null
                ? null
                : reloadDetails.BillingInfo.PaymentMethod;

            Task<AuthorizePayPalResults> authTask = null;
            AuthorizePayPalResults authPayPalResult;
            Task<IBillingAgreementUdpateResponse> getPayPalInfoTask = null;
            IBillingAgreementUdpateResponse billingAgreementInfo = null;

            reloadWatch.Start();

            try
            {
                authTask = Task.Run(() => AuthorizePayPal(reloadDetails));

                getPayPalInfoTask = Task.Run(() => GetBillingAgreementInfo(
                    reloadDetails.MerchantInfo == null ? null : reloadDetails.MerchantInfo.CurrencyCode, paymentMethod));

                Task.WaitAll(getPayPalInfoTask, authTask);
                authPayPalResult = authTask.Result;
                billingAgreementInfo = getPayPalInfoTask.Result;
            }
            catch (Exception ex)
            {
                authPayPalResult = GetPayPalAuthResult(authTask, ex);
                LogAggregateExceptionPayPal("PayPal Auth, UpdateBillingAgreementInfo aggregate Exception", reloadDetails, authPayPalResult, ex);
            }

            reloadWatch.Stop();
            LogTimer(reloadWatch, "PayPal Auth & GetBillingAgreementInfo");

            var authResult = authPayPalResult.PayPalResponse;
            var transactionId = authResult == null
                ? null
                : authResult.TransactionDetails == null ? null : authResult.TransactionDetails.TransactionId;

            // Create failed order for saving if something fails. Save PayerId from PayPal GetBillingAgreementInfo result.
            if (billingAgreementInfo != null && billingAgreementInfo.PayerInformation != null)
            {
                billingInfo.PayPalPayerId = billingAgreementInfo.PayerInformation.PayerId;
            }
            var platform = GetPlatform(reloadForStarbucksCard);

            IBillingInfo payPalBillingInfo = GetPayPalBillingInfo(billingAgreementInfo, billingInfo);
            payPalBillingInfo.PayPalTransactionId = transactionId;

            var failedOrder = CreateFailedOrderObject(billingInfo, merchantInfo, orderId,
                card.Number, items.First().OrderType,
                user == null ? null : user.UserId,
                platform, transactionId, reasonCode: -1);

            // If Auth failed, save failed order, report it and exit.
            if (PayPalAuthFailed(authResult))
            {
                SaveFailedOrder(failedOrder, PayPalFailedOrderReasonCode);

                // Throw error indicating that the auth failed.

                // If we got an exception from the PayPal auth, don't report it.
                if (authPayPalResult.Exception != null)
                {
                    throw authPayPalResult.Exception;
                }

                ReportFailedPayPalAuthorization(reloadDetails, authResult, billingAgreementInfo);

                throw new CardTransactionException(CardTransactionErrorResource.CannotAuthorizePayPalCode,
                    CardTransactionErrorResource.CannotAuthorizePayPalMessage);
            }

            PendingStatusCode authPendingStatusCode = PendingStatusCode.None;
            if (authPayPalResult.PayPalResponse != null && authPayPalResult.PayPalResponse.TransactionDetails != null)
            {
                authPendingStatusCode = authPayPalResult.PayPalResponse.TransactionDetails.PendingStatusCode;
            }

            // If we're here, the Auth call succeeded and was not denied. 
            // Get the Subject from the PayPal transaction detials and do a fraud check.
            string payPalSubject = string.Empty;
            var properties = new Dictionary<string, object>();
            try
            {
                reloadWatch.Restart();

                payPalSubject = GetPaypalSubject(merchantInfo.CurrencyCode);

                payPalBillingInfo.PayPalSubject = payPalSubject;
                reloadWatch.Stop();
                LogTimer(reloadWatch, "PayPal get subject");

                properties = new Dictionary<string, object>
                {
                    {"transactionId", transactionId.ToDisplay()},
                    {
                        "Transaction Details Subject", payPalSubject.ToDisplay()
                    },
                    {"PaymentMethodId", reloadForStarbucksCard.PaymentMethodId.ToDisplay()},
                    {"UserId", user.UserId.ToDisplay()},
                    {"CardId", card.CardId.ToDisplay()},
                    {"OrderId", orderId.ToDisplay()},
                    {"EmailAddress", emailAddress.ToDisplay()},
                    {"ResponseCode", authPendingStatusCode.ToString().ToDisplay()},
                };
                LogReloadDetails(TraceEventType.Information, null, "CardApi - Reload GetPaypalSubject",
                    "Result of _securePayPalPayment.GetPayPalSubject w/no exception.", properties);
            }
            catch (Exception ex)
            {
                LogReloadExceptionPayPal("GetPayPalSubject Exception", reloadDetails, transactionId, ex);
                throw;
            }

            reloadWatch.Restart();

            var fraudCheckResult = ConfigurableFraudCheckWithPayPal(reloadDetails, authResult, billingAgreementInfo);

            reloadWatch.Stop();
            LogTimer(reloadWatch, "PayPal PSAPI FraudCheck (Iovation & Accertify)");

            // If we get null back treat it as an accept and continue processing.
            if (fraudCheckResult != null)
            {
                if (fraudCheckResult.DecisionType == DecisionType.Deny)
                {
                    _loggingManager.Increment("CardTransactionProvider_AuthorizeAndCheckFraudPayPal_FraudCheck_Denied");

                    LogHelper.Log(
                        String.Format(
                            "ReloadStarbucksCardByCardNumberWithPayPal, FraudCheck Deny. userId: {0} cardId: {1}, PaymentMethodId: {2} orderId: {3}, email: {4}",
                            user.UserId ?? "", card.CardId, reloadForStarbucksCard.PaymentMethodId, orderId,
                            emailAddress),
                        "CardApi - Reload",
                        TraceEventType.Warning);

                    // Backout out the Auth, save the order, and throw an exception.
                    VoidPayPalAuthorization(transactionId,
                        reloadDetails.MerchantInfo == null ? null : reloadDetails.MerchantInfo.CurrencyCode,
                        payPalSubject, orderId);

                    SaveFailedOrder(failedOrder,
                        fraudCheckResult.DecisionReason == null
                            ? PayPalFailedOrderReasonCode
                            : fraudCheckResult.DecisionReason.Code);

                    throw new CardTransactionException(CardTransactionErrorResource.FraudCheckFailedCode,
                        CardTransactionErrorResource.FraudCheckFailedMessage);
                }
            }

            ICardTransaction cardIssuerReloadResult;
            try
            {
                cardIssuerReloadResult = Reload(merchantInfo, card.Number, reloadDetails.Amount);
            }
            catch (Exception ex)
            {
                _loggingManager.Increment("card_reload_paypal_reload_failed_with_exception");

                LogReloadExceptionPayPal("CardTransactionValueLinkReloadException", reloadDetails, transactionId, ex);

                VoidPayPalAuthorization(transactionId, merchantInfo.CurrencyCode, payPalSubject, orderId);

                SaveFailedOrder(failedOrder);
                throw;
            }

            if (cardIssuerReloadResult == null)
            {
                _loggingManager.Increment("card_reload_paypal_failed_with_null_result");

                LogHelper.Log(
                    String.Format(
                        "ReloadStarbucksCardByCardNumberWithPayPal, cardResult == null. userId: {0}, cardId: {1}, PaymentMethodId: {2}, orderId: {3}, email: {4}",
                        user.UserId ?? "", card.CardId, reloadForStarbucksCard.PaymentMethodId, orderId, emailAddress),
                    "CardApi - Reload",
                    TraceEventType.Error);

                VoidPayPalAuthorization(transactionId, merchantInfo.CurrencyCode, payPalSubject, orderId);

                SaveFailedOrder(failedOrder);
                return null;
            }
            var responseCode = cardIssuerReloadResult != null ? cardIssuerReloadResult.ResponseCode : null;

            properties.Clear();
            properties.Add("UserId", user.UserId.ToDisplay());
            properties.Add("CardId", card.CardId.ToDisplay());
            properties.Add("PaymentMethodId", reloadForStarbucksCard.PaymentMethodId);
            properties.Add("OrderId", orderId.ToDisplay());
            properties.Add("EmailAddress", emailAddress.ToDisplay());
            properties.Add("ResponseCode", responseCode.ToDisplay());
            properties.Add("SVCTransactionId", cardIssuerReloadResult != null ? cardIssuerReloadResult.TransactionId.ToDisplay() : string.Empty);
            properties.Add("PayPalAuthTransactionId", transactionId.ToDisplay());

            LogReloadDetails(TraceEventType.Information, fault: null,
                title: "CardApi - Reload Value Link",
                message: "ReloadStarbucksCardByCardNumberWithPayPal, VL Reload result.", extendedProperties: properties);

            // Handle any Valuelink (SB card manager) errors
            var cardIssuerException = CardIssuerExceptionHandler.HandleCardIssuerResponse(cardIssuerReloadResult.ResponseCode);

            if (cardIssuerException != null)
            {
                _loggingManager.Increment("card_reload_paypal_failed_with_valuelink_exception");

                LogHelper.Log(
                    String.Format(
                        "ReloadStarbucksCardByCardNumberWithPayPal, Card issuer returned error. ResponseCode: {0}, userId: {1}, cardId: {2}, PaymentMethodId: {3}, orderId: {4}, email: {5}, throwing exception: {6}",
                        cardIssuerReloadResult.ResponseCode, user.UserId ?? "", card.CardId, reloadForStarbucksCard.PaymentMethodId,
                        orderId, emailAddress, cardIssuerException.Message),
                    "CardApi - Reload",
                    TraceEventType.Warning);

                VoidPayPalAuthorization(transactionId, merchantInfo.CurrencyCode, payPalSubject, orderId);

                SaveFailedOrder(failedOrder);
                throw cardIssuerException;
            }

            try
            {
                Task.Run(() =>
                {
                    PayPalBillAction(
                            transactionId,
                            payPalSubject,
                             user,
                             card,
                             emailAddress,
                             reloadForStarbucksCard,
                             orderId,
                             merchantInfo,
                             submarket,
                             payPalBillingInfo,
                             reloadDetails,
                             cardIssuerReloadResult,
                             failedOrder,
                             items);
                }
                );
            }
            catch (System.Exception runPayPalBillException)
            {
                string message = string.Format("PayPal Bill Failed (from Main Thread)");
                LogPayPalBillExectipn(message, user, card, orderId, reloadForStarbucksCard, runPayPalBillException);
            }

            return cardIssuerReloadResult;
        }

        #region PaypalBill related functions

        private void LogPayPalBillExectipn(string message,
                        StarbucksAccountProvider.Common.Models.IUser user,
                        IAssociatedCard card,
                        string orderId,
                        IReloadForStarbucksCard reloadForStarbucksCard,
                        Exception ex
            )
        {

            try
            {
                Dictionary<string, object> properties = new Dictionary<string, object>();
                properties.Add("OrderId", orderId.ToDisplay());
                properties.Add("UserId", user != null ? user.UserId.ToDisplay() : "<NULL>");
                properties.Add("CardId", card != null ? card.CardId.ToDisplay() : "<NULL>");
                properties.Add("PaymentMethodId", reloadForStarbucksCard != null ? reloadForStarbucksCard.PaymentMethodId.ToDisplay() : "<NULL>");
                properties.Add("Exception", ex.ToString());
                if (ex.InnerException != null)
                {
                    properties.Add("InnerException", ex.InnerException.ToString());
                }

                LogPSApiCallDetails.PerformLogging(message, TraceEventType.Error, "CardApi - Bill Exception PayPal", properties);
            }
            catch (Exception localErr)
            {
                // logging errro, has to eat it.

                System.Diagnostics.Debug.WriteLine(localErr.ToString());
            }

        }

        private void PayPalBillAction(
                    string transactionId,
                    string payPalSubject,
                    StarbucksAccountProvider.Common.Models.IUser user,
                    IAssociatedCard card,
                    string emailAddress,
                    IReloadForStarbucksCard reloadForStarbucksCard,
                    string orderId,
                    IMerchantInfo merchantInfo,
                    string submarket,
                    IBillingInfo payPalBillingInfo,
                    ReloadTransactionRequest reloadDetails,
                    ICardTransaction cardIssuerReloadResult,
                    IFailedOrder failedOrder,
                     List<IOrderLineItem> items
                     )
        {
            try
            {

                PerformPayPalBill(
                                    transactionId,
                                    payPalSubject,
                                     user,
                                     card,
                                     emailAddress,
                                     reloadForStarbucksCard,
                                     orderId,
                                     merchantInfo,
                                     submarket,
                                     payPalBillingInfo,
                                     reloadDetails,
                                     cardIssuerReloadResult,
                                     failedOrder,
                                     items);
            }
            catch (Exception ex)
            {
                string message = string.Format("PayPal Bill Failed (from background Thread)");
                LogPayPalBillExectipn(message, user, card, orderId, reloadForStarbucksCard, ex);
            }
        }

        private void PerformPayPalBill(
            string transactionId,
            string payPalSubject,
            StarbucksAccountProvider.Common.Models.IUser user,
            IAssociatedCard card,
            string emailAddress,
            IReloadForStarbucksCard reloadForStarbucksCard,
            string orderId,
            IMerchantInfo merchantInfo,
            string submarket,
            IBillingInfo payPalBillingInfo,
            ReloadTransactionRequest reloadDetails,
            ICardTransaction cardIssuerReloadResult,
            IFailedOrder failedOrder,
             List<IOrderLineItem> items
             )
        {
            // Bill the transaction to PayPal.
            IBillResponse billResponse;
            Stopwatch reloadWatch = new Stopwatch();
            var properties = new Dictionary<string, object>();

            try
            {
                reloadWatch.Start();

                billResponse = BillPayPal(orderId, transactionId, payPalSubject);
                reloadWatch.Stop();
                LogTimer(reloadWatch, "PayPal Bill");

                var paymentStatusCode = billResponse != null ? billResponse.PaymentStatusCode.ToString() : null;
                properties.Clear();
                properties.Add("UserId", user.UserId.ToDisplay());
                properties.Add("CardId", card.CardId.ToDisplay());
                properties.Add("PaymentMethodId", reloadForStarbucksCard.PaymentMethodId.ToDisplay());
                properties.Add("OrderId", orderId.ToDisplay());
                properties.Add("EmailAddress", emailAddress.ToDisplay());
                properties.Add("ResponseCode", paymentStatusCode.ToDisplay());
                properties.Add("SVCTransactionId", cardIssuerReloadResult != null ? cardIssuerReloadResult.TransactionId.ToDisplay() : string.Empty);
                properties.Add("PayPalAuthTransactionId", transactionId.ToDisplay());
                properties.Add("PayPalBillTransactionId", billResponse != null ? billResponse.TransactionId.ToDisplay() : null);

                LogReloadDetails(TraceEventType.Information, null,
                    "CardApi - Reload BillPayPal",
                    "ReloadStarbucksCardByCardNumberWithPayPal, Bill PayPal response.", properties);
                payPalBillingInfo.IsPayPalBillFailed = false;
            }
            catch (Exception ex)
            {
                if (reloadWatch.IsRunning)
                {
                    reloadWatch.Stop();
                }

                _loggingManager.Increment("card_reload_paypal_bill_failed_with_exception");

                LogReloadExceptionPayPal("CardTransactionPayPalBillException", reloadDetails, transactionId, ex);

                VoidCardReload(cardIssuerReloadResult.ToCardIssuer(), merchantInfo); // Will catch exceptions and log them.

                VoidPayPalAuthorization(transactionId, merchantInfo.CurrencyCode, payPalSubject, orderId);

                SaveFailedOrder(failedOrder, PayPalFailedOrderReasonCode);

                throw;
            }

            if (billResponse.PaymentStatusCode != PaymentStatusCode.Completed)
            {
                _loggingManager.Increment("card_reload_paypal_bill_failed_with_payment_status_not_completed");

                VoidCardReload(cardIssuerReloadResult.ToCardIssuer(), merchantInfo); // Will catch exceptions and log them.

                VoidPayPalAuthorization(transactionId, merchantInfo.CurrencyCode, payPalSubject, orderId);

                SaveFailedOrder(failedOrder, PayPalFailedOrderReasonCode);

                throw new CardTransactionException(
                    CardTransactionErrorResource.GeneralPaymentErrorCode,
                    CardTransactionErrorResource.GeneralPaymentErrorMessage);
            }

            EmailReloadConfirmation(submarket, emailAddress, orderId, reloadDetails.Amount, payPalBillingInfo,
                items);

            SaveOrder(reloadDetails.UserId, orderId, items, payPalBillingInfo, cardIssuerReloadResult.TransactionId,
                billRequestId: null);
        }

        #endregion

        private AuthorizePayPalResults GetPayPalAuthResult(Task<AuthorizePayPalResults> authTask, Exception ex)
        {
            if (authTask != null)
            {
                return authTask.Result;
            }
            return new AuthorizePayPalResults()
            {
                PayPalResponse = null,
                Exception = ex
            };
        }

        private string GetPlatform(IReloadForStarbucksCard reloadForStarbucksCard)
        {
            // If the Platform in the outer request is missing use the one in the risk object if it exists.
            if (!String.IsNullOrEmpty(reloadForStarbucksCard.Platform))
            {
                return reloadForStarbucksCard.Platform;
            }
            return reloadForStarbucksCard.Risk != null ? reloadForStarbucksCard.Risk.Platform : String.Empty;
        }

        private static UserAddress GetUserAddress(IBillingAgreementUdpateResponse billingAgreementInfo,
            IBillingInfo billingInfo)
        {
            IAddress billingAddress = null;
            IAddress payPaladdress = null;

            if (billingAgreementInfo != null)
            {
                billingAddress = billingAgreementInfo.BillingAddress;
                payPaladdress = billingAgreementInfo.PayerInformation != null
                    ? billingAgreementInfo.PayerInformation.Address
                    : null;
            }

            var address = new UserAddress();
            if (AddressIsNotEmpty(payPaladdress))
            {
                address.Country = payPaladdress.Country;
                address.CountrySubdivision = payPaladdress.StateOrProvince;
                address.City = payPaladdress.City;
                address.AddressLine1 = payPaladdress.Street1;
                address.AddressLine2 = payPaladdress.Street2;
                address.AddressName = payPaladdress.Name;
                address.PostalCode = payPaladdress.PostalCode;
            }
            if (AddressIsNotEmpty(billingAddress))
            {
                address.Country = billingAddress.Country;
                address.CountrySubdivision = billingAddress.StateOrProvince;
                address.City = billingAddress.City;
                address.AddressLine1 = billingAddress.Street1;
                address.AddressLine2 = billingAddress.Street2;
                address.AddressName = billingAddress.Name;
                address.PostalCode = billingAddress.PostalCode;
            }
            else
            {
                address.Country = billingInfo.Address.Country;
                address.CountrySubdivision = billingInfo.Address.CountrySubdivision;
                address.City = billingInfo.Address.City;
                address.AddressLine1 = billingInfo.Address.AddressLine1;
                address.AddressLine2 = billingInfo.Address.AddressLine2;
                address.AddressName = billingInfo.Address.AddressName;
                address.PostalCode = billingInfo.Address.PostalCode;
            }
            return address;
        }

        private static bool AddressIsNotEmpty(IAddress address)
        {
            return (address != null && !string.IsNullOrEmpty(address.City) && !string.IsNullOrEmpty(address.Street1));
        }

        private bool PayPalAuthFailed(IDoReferenceTransactionResponse authResult)
        {
            return (authResult == null || authResult.TransactionDetails == null || string.IsNullOrEmpty(authResult.TransactionDetails.TransactionId) ||
                    !(authResult.TransactionDetails.PendingStatusCode == PendingStatusCode.Authorization ||
                      authResult.TransactionDetails.PendingStatusCode == PendingStatusCode.PaymentReview));
        }

        private void VoidCardReload(CardIssuer.Dal.Common.Models.ICardTransaction voidTransaction,
            IMerchantInfo merchantInfo)
        {
            try
            {
                _cardIssuer.VoidRequest(voidTransaction, merchantInfo);
            }
            catch (Exception ex)
            {
                _loggingManager.Increment("void_card_reload_paypal_void_request_exception");
                var properties = new Dictionary<string, object>
                {
                    {"voidTransaction.CardNumber", voidTransaction == null ? string.Empty : voidTransaction.CardNumber},
                    {
                        "voidTransaction.AuthorizationCode",
                        voidTransaction == null ? string.Empty : voidTransaction.AuthorizationCode
                    },
                    {"voidTransaction.CardId", voidTransaction == null ? string.Empty : voidTransaction.CardId},
                    {"voidTransaction.MerchantId", voidTransaction == null ? string.Empty : voidTransaction.MerchantId},
                    {
                        "voidTransaction.TransactionId",
                        voidTransaction == null ? string.Empty : voidTransaction.TransactionId
                    },
                    {"merchantInfo.MarketId", merchantInfo == null ? string.Empty : merchantInfo.MarketId},
                };

                LogReloadDetails(TraceEventType.Error, fault: null,
                            title: "CardApi - Reload VoidCardReload_VoidRequest_failed_with_exception",
                            message: ex.Message, extendedProperties: properties);
                throw;
            }
        }

        private void ReportFailedPayPalAuthorization(ReloadTransactionRequest transactionRequest,
            IDoReferenceTransactionResponse authResult,
            IBillingAgreementUdpateResponse billingAgreementInfo, bool doNotCallIovation = true)
        {
            // Can't successfully report to Accertify w/o info from authResult.
            if (authResult == null) return;

            try
            {
                // Report, asynchronously, this failed Auth to our Fraud Check service with as much info from the Auth call as we have.
                var reportFraudRequest = GetFraudRequest(transactionRequest, authResult, billingAgreementInfo);

                if (doNotCallIovation)
                {
                    reportFraudRequest.Risk.DeviceReputation = null;      // Clear DeviceReputation for reporting w/o calling Iovation.
                }

                _paymentServiceApiClient.ReportPayPalAuthFailure(reportFraudRequest);
            }
            catch (Exception exception)
            {
                // Just log this but continue so we throw the more important exception.

                var properties = new Dictionary<string, object>
                {
                    {"UserId", transactionRequest.UserId},
                    {
                        "transactionRequest.Card",
                        transactionRequest.Card == null ? string.Empty : transactionRequest.Card.CardId
                    },
                    {
                        "PaymentMethodId", transactionRequest.BillingInfo == null
                            ? string.Empty
                            : transactionRequest.BillingInfo.PaymentMethod == null
                                ? string.Empty
                                : transactionRequest.BillingInfo.PaymentMethod.PaymentMethodId
                    },
                    {
                        "BillingAgreementId (encrypted)", transactionRequest.BillingInfo == null
                            ? string.Empty
                            : transactionRequest.BillingInfo.PaymentMethod == null
                                ? string.Empty
                                : transactionRequest.BillingInfo.PaymentMethod.AccountNumber
                    },
                    {"Amount", transactionRequest.Amount.ToString(CultureInfo.InvariantCulture)},
                    {"Submarket", transactionRequest.Submarket}
                };

                LogReloadDetails(TraceEventType.Error, fault: null,
                            title: "CardApi - Reload ReportFailedPayPalAuthorization_failed_with_exception",
                            message: exception.Message, extendedProperties: properties);
            }
        }

        private IFraudCheckResult ConfigurableFraudCheckWithPayPal(ReloadTransactionRequest transactionRequest,
            IDoReferenceTransactionResponse authResult,
            IBillingAgreementUdpateResponse billingAgreementInfo)
        {
            if (_cardTransactionProviderSettingsSection.IgnorePayPalReloadsFraudCheck)
            {
                ReportFailedPayPalAuthorization(transactionRequest, authResult, billingAgreementInfo, doNotCallIovation: false);
                var fraudCheckResult = new FakeFruadCheckResult()
                {
                    DecisionType = DecisionType.Accept
                };
                // Get the request we would have got in FraudCheckWithPayPal since its useful and we already have a logger for it .
                var fraudRequest = GetFraudRequest(transactionRequest, authResult, billingAgreementInfo);

                LogPSApiCallDetails.LogFraudCheckResult(fraudRequest, fraudCheckResult);
                return fraudCheckResult;
            }

            return FraudCheckWithPayPal(transactionRequest, authResult, billingAgreementInfo);
        }


        private IFraudCheckResult FraudCheckWithPayPal(ReloadTransactionRequest transactionRequest,
            IDoReferenceTransactionResponse authResult,
            IBillingAgreementUdpateResponse billingAgreementInfo)
        {
            var fraudRequest = GetFraudRequest(transactionRequest, authResult, billingAgreementInfo);

            IFraudCheckResult fraudCheckResult = null;
            try
            {
                fraudCheckResult = _paymentServiceApiClient.FraudCheck(fraudRequest);

                LogPSApiCallDetails.LogFraudCheckResult(fraudRequest, fraudCheckResult);
            }
            catch (Exception ex)
            {
                // Log the exception but eat it because failure to check fraud should not abort the transaction.

                LogPSApiCallDetails.LogFraudCheckException(fraudRequest, ex);
                _loggingManager.Increment(
                    "CardTransactionProvider_FraudCheckWithPayPal_FraudCheck_failed_with_exception");

                // We fall through and return true in this case because we treat an exception as an Accept because we don't 
                // want to block transactions if our fraud service is not currently available.
            }
            return fraudCheckResult;
        }

        private FraudCheckRequest GetFraudRequest(ReloadTransactionRequest transactionRequest,
            IDoReferenceTransactionResponse authResult,
            IBillingAgreementUdpateResponse billingAgreementInfo)
        {
            string transactionId = null;
            var pendingStatusCode = PendingStatusCode.None;
            if (authResult != null && authResult.TransactionDetails != null)
            {
                transactionId = authResult.TransactionDetails.TransactionId;
                pendingStatusCode = authResult.TransactionDetails.PendingStatusCode;
            }

            var riskInfo = new FraudCheckRiskInfo()
            {
                UserId = transactionRequest.UserId,
                OrderMessage = null,
                StarbucksCardDetails = GetPaymentServiceApiSdkCard(transactionRequest.Card, transactionRequest.Platform)
            };
            if (transactionRequest.Risk != null)
            {
                riskInfo.CCAgentName = transactionRequest.Risk.CcAgentName;
                // Set our default logged in state if none was specified.
                riskInfo.SignedIn = transactionRequest.Risk.IsLoggedIn == null
                    ? IsLoggedInDefault
                    : transactionRequest.Risk.IsLoggedIn.ToString();
                riskInfo.DeviceReputation = transactionRequest.Risk.ToPaymentServiceApiSdkReputation();
            }

            return new FraudCheckRequest()
            {
                TransactionType = TransactionTypes.Reload,
                TransactionId = transactionRequest.OrderId,
                Platform = (transactionRequest.Risk == null) ? null : transactionRequest.Risk.Platform,
                Market = transactionRequest.Submarket,
                CurrencyCode =
                    transactionRequest.MerchantInfo == null ? null : transactionRequest.MerchantInfo.CurrencyCode,
                BillingInfo =
                    GetFraudCheckBillingInfo(transactionRequest.BillingInfo, billingAgreementInfo, pendingStatusCode,
                        transactionRequest.EmailAddress),
                Basket = transactionRequest.Items.ToPaymentServiceApiSdkOrderItemList(),
                Shipping = null,
                Risk = riskInfo
            };
        }

        private static Api.Sdk.PaymentService.Models.BillingInfo GetFraudCheckBillingInfo(IBillingInfo billingInfo,
            IBillingAgreementUdpateResponse billingAgreement, PendingStatusCode pendingStatusCode, string email)
        {
            var payPalBillingAddress = GetUserAddress(billingAgreement, billingInfo);

            AddressInfo paymentServiceAddress = payPalBillingAddress.ToPaymentServiceApiSdkAddress();
            IPayerInformation payerInformation = null;
            if (billingAgreement != null)
            {
                payerInformation = billingAgreement.PayerInformation;
                paymentServiceAddress.AddressDetails.FirstName = payerInformation == null
                    ? null
                    : payerInformation.FirstName;
                paymentServiceAddress.AddressDetails.LastName = payerInformation == null
                    ? null
                    : payerInformation.LastName;
            }

            return new Api.Sdk.PaymentService.Models.BillingInfo()
            {
                Email = email,
                PaymentMethod =
                    GetFraudCheckPaymentMethod(billingInfo.PaymentMethod, payerInformation, pendingStatusCode,
                        payPalBillingAddress.AddressName),
                AddressInfo = paymentServiceAddress
            };
        }

        private static Api.Sdk.PaymentService.Models.PaymentMethod GetFraudCheckPaymentMethod(
            SecurePayment.Provider.Common.Models.IPaymentMethod paymentMethod,
            IPayerInformation payerInformation, PendingStatusCode pendingStatusCode, string payPalFullName)
        {
            //            var payerInformation = transactionDetails == null ? null : transactionDetails.PayerInformation;

            return new Api.Sdk.PaymentService.Models.PaymentMethod()
            {
                PaymentId = paymentMethod == null ? null : paymentMethod.PaymentMethodId,
                PayPalDetails = new PayPalDetails()
                {
                    BillAgreementId = paymentMethod == null ? null : paymentMethod.AccountNumber,
                    PayPalPayerId = payerInformation == null ? null : payerInformation.PayerId,
                    PayPayEmail = payerInformation == null ? null : payerInformation.Payer,
                    PendingReason = pendingStatusCode.ToString(),
                    PayPayFullName = payPalFullName,
                }
            };
        }

        private IBillingInfo GetPayPalBillingInfo(IBillingAgreementUdpateResponse billingAgreementInfo,
            IBillingInfo billingInfo)
        {
            // See notes in set express checkout about billing address vs shipping address

            var payPalBillingInfo = new BillingInfo()
            {
                Address = GetUserAddress(billingAgreementInfo, billingInfo),
                PaymentMethod = billingInfo.PaymentMethod,
                Currency = billingInfo.Currency,
                CurrencyNumericCode = billingInfo.CurrencyNumericCode,
                EmailAddress = billingInfo.EmailAddress
            };
            if (billingAgreementInfo != null && billingAgreementInfo.PayerInformation != null)
            {
                var payerInfo = billingAgreementInfo.PayerInformation;
                if (payerInfo.Address != null &&
                    !String.IsNullOrWhiteSpace(payerInfo.Address.AddressId))
                {
                    payPalBillingInfo.Address.PhoneNumber = payerInfo.Address.Phone;
                    payPalBillingInfo.Address.EmailAddress = payerInfo.Payer;
                }

                payPalBillingInfo.Address.FirstName = payerInfo.FirstName;
                payPalBillingInfo.Address.LastName = payerInfo.LastName;
                payPalBillingInfo.PayPalPayerId = payerInfo.PayerId;
                payPalBillingInfo.PayPalEmailAddress = payerInfo.Payer;
            }
            return payPalBillingInfo;
        }

        private AuthorizePayPalResults AuthorizePayPal(ReloadTransactionRequest transactionRequest)
        {
            var authorizePayPalResults = new AuthorizePayPalResults()
            {
                PayPalResponse = null,
                Exception = null
            };

            // PayPal throws an exception if the currency code is null so save the external call by pre-checking it here.
            if (transactionRequest.MerchantInfo == null ||
                String.IsNullOrWhiteSpace(transactionRequest.MerchantInfo.CurrencyCode))
            {
                authorizePayPalResults.Exception =
                    new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.UnknownCode,
                        SecurePayPalPaymentErrorResource.UnknownMessage);
                return authorizePayPalResults;
            }

            // For PayPal payment methods, the AccountNumber field contains the PayPal billing agreement Id.
            var billingAgreementId = (transactionRequest.BillingInfo == null
                ? null
                : transactionRequest.BillingInfo.PaymentMethod) ==
                                     null
                ? null
                : transactionRequest.BillingInfo.PaymentMethod.AccountNumber;
            var currency = transactionRequest.MerchantInfo == null ? null : transactionRequest.MerchantInfo.CurrencyCode;
            var cardId = transactionRequest.Card == null ? null : transactionRequest.Card.CardId;
            var cardNumber = transactionRequest.Card == null ? null : transactionRequest.Card.Number;

            if (String.IsNullOrWhiteSpace(billingAgreementId))
            {
                LogPayPalAuthException(cardId, cardNumber, billingAgreementId, transactionRequest,
                    exception: null, title: "CardApi - Reload AuthorizePayPal", message: "Billing Agreement Missing.");

                authorizePayPalResults.Exception =
                    new CardTransactionValidationException(
                        CardTransactionValidationErrorResource.InvalidBillingAgreementIdCode,
                        CardTransactionValidationErrorResource.InvalidBillingAgreementIdMessage);

                return authorizePayPalResults;
            }

            try
            {
                var cardNumberForDisplay = cardNumber == null ? null : cardNumber.Substring(cardNumber.Length - 4, 4);
                var paymentDetails = BuildPayPalPaymentDetails(cardNumberForDisplay, currency, transactionRequest.Amount);
                var locale = _settingsProvider.GetLocaleByMarket(transactionRequest.Submarket).FirstOrDefault();
                var billingAgreementIdClearText = Encryption.DecryptCardNumber(billingAgreementId);
                // handles null value

                // Authorize against existing billing agreement
                var authorizationRequest = new DoReferenceTransactionRequest
                {
                    PaymentAction = PaymentActionCode.Authorization,
                    PaymentDetails = paymentDetails,
                    SoftDescriptor = ("Starbucks Card Reload - ") + cardNumberForDisplay,
                    IPAddress = transactionRequest.IpAddress,
                    RequireShippingConfirmation = false,
                    ReferenceId = billingAgreementIdClearText,
                    UserId = transactionRequest.UserId,
                    Locale = locale
                };
                authorizePayPalResults.PayPalResponse = _securePayPalPayment.DoReferenceTransaction(authorizationRequest);

                LogPayPalAuthResult(transactionRequest, authorizePayPalResults.PayPalResponse);
                return authorizePayPalResults;
            }

            // Paypal Auth has failed. Catch whichever exception was throw, log it but don't rethrow here.
            // Return an error indication. We need to report this failure to our fraud detection service (Accertify). 
            // Will then throw exception captured here.

            catch (Exception ex)
            {
                var securePayPalPaymentException = ex as SecurePayPalPaymentException;
                if (securePayPalPaymentException != null)
                {
                    // These exceptions will contain SecurePayPalPaymentErrorResource. codes which need to be rethrown to 
                    // the Api after reporting the failure.
                    // Examples:
                    //          SecurePayPalPaymentErrorResource.BillingAgreementInvalidCode
                    //          AccountClosedCode
                    //          TemporaryFailureCode
                    //          InvalidCCVCode, etc.

                    LogPayPalAuthException(cardId, cardNumber, billingAgreementId, transactionRequest,
                        securePayPalPaymentException,
                            "CardApi - Reload AuthorizePayPal",
                            String.Format("SecurePayPalPaymentException from SecurePayPalPaymentClient. {0}", securePayPalPaymentException.Message));
                }
                else
                {
                    LogPayPalAuthException(cardId, cardNumber, billingAgreementId, transactionRequest, ex,
                            "CardApi - Reload AuthorizePayPal", String.Format("Exception from SecurePayPalPaymentClient. {0}", ex.Message));
                }
                authorizePayPalResults.Exception = ex;
            }
            return authorizePayPalResults;
        }

        private void LogPayPalAuthException(string cardId, string cardNumber, string billingAgreementId,
            ReloadTransactionRequest transactionRequest, Exception exception, string title, string message)
        {
            var properties = new Dictionary<string, object>
            {
                {"CardToReload.CardId", cardId},
                {"CardToReload.CardNumber", cardNumber},
                {"UserId", transactionRequest.UserId},
                {"OrderId", transactionRequest.OrderId},
                {"Amount", transactionRequest.Amount.ToString(CultureInfo.InvariantCulture)},
                {"BillingAgreementId", billingAgreementId},
                {"Submarket", transactionRequest.Submarket.ToDisplay()},
                {"EmailAddress", transactionRequest.EmailAddress.ToDisplay()}
            };
            LogPSApiCallDetails.BuildExceptionProperties(properties, exception);

            var faultException = exception == null
                ? null
                : exception.InnerException as FaultException<IPayPalFault>;

            LogReloadDetails(TraceEventType.Error, faultException != null ? faultException.Detail : null,
                title, message, properties);
        }


        private IBillResponse BillPayPal(string orderId, string transactionId, string subject)
        {
            var billResponse =
                _securePayPalPayment.Bill(new PayPalModels.BillRequest
                {
                    AuthorizationId = transactionId,
                    Subject = subject,
                    OrderId = orderId
                });
            return billResponse;
        }

        private ITransactionDetails GetTransactionDetails(ITransactionDetails transactionDetails, ReloadTransactionRequest payPalReloadRequest)
        {
            Stopwatch reloadWatch = new Stopwatch();
            ITransactionDetails response = null;

            try
            {
                reloadWatch.Start();

                response = _securePayPalPayment.GetTransactionDetails(new TransactionDetailsRequest
                {
                    Currency = payPalReloadRequest.MerchantInfo.CurrencyCode,
                    TransactionId = transactionDetails.TransactionId
                });
                // Log time taken and successful result.
                reloadWatch.Stop();
                LogTimer(reloadWatch, "PayPal GetTransactionDetails");

                var properties = new Dictionary<string, object>
                {
                    {"transactionId", transactionDetails.TransactionId.ToDisplay()},
                    {"Transaction Details Subject", transactionDetails.Subject.ToDisplay()},
                    {"PaymentMethodId", payPalReloadRequest.BillingInfo.PaymentMethod.PaymentMethodId.ToDisplay()},
                    {"UserId", payPalReloadRequest.UserId.ToDisplay()},
                    {"CardId", payPalReloadRequest.Card.CardId.ToDisplay()},
                    {"OrderId", payPalReloadRequest.OrderId.ToDisplay()},
                    {"EmailAddress", payPalReloadRequest.EmailAddress.ToDisplay()},
                    {"ResponseCode", transactionDetails.PendingStatusCode.ToString().ToDisplay()},
                };

                LogReloadDetails(TraceEventType.Information, null, "CardApi - Reload GetTransactionDetails",
                    "Result of _securePayPalPayment.GetTransactionDetails w/no exception.", properties);
            }
            catch (Exception ex)
            {
                LogReloadExceptionPayPal("GetTransactionDetails Exception", payPalReloadRequest, transactionDetails.TransactionId, ex);
            }

            return response;
        }

        private string GetPaypalSubject(string currency)
        {
            return _securePayPalPayment.GetPayPalSubject(currency);
        }

        private void VoidPayPalAuthorization(string transactionId, string currencyCode, string subject, string orderId)
        {
            try
            {
                var voidRequest = new PayPalModels.VoidRequest { AuthorizationId = transactionId };
                voidRequest.Subject = subject;

                LogHelper.Log(
                    String.Format(
                        "Voiding PayPal authorization: orderId: {0} transactionId: {1} PayPal 'subject': {2}", orderId,
                        transactionId, subject),
                    "CardApi - Reload",
                    TraceEventType.Information);

                _securePayPalPayment.Void(voidRequest);
            }
            catch (Exception ex)
            {
                _loggingManager.Increment("card_reload_paypal_void_auth_with_exception");

                var properties = new Dictionary<string, object>
                {
                    {"PayPalTransactionId", transactionId},
                    {"OrderId", orderId}
                };
                LogReloadDetails(TraceEventType.Error, null, "CardApi - Reload VoidPayPalAuthorization", ex.Message,
                    properties);
            }
        }

        private IBillingAgreementUdpateResponse GetBillingAgreementInfo(string currency,
            SecurePayment.Provider.Common.Models.IPaymentMethod paymentMethod)
        {
            var billingAgreementId = paymentMethod == null ? null : paymentMethod.AccountNumber;

            var billingAgreementRequest = new BillingAgreementUpdateRequest()
            {
                Currency = currency,
                ReferenceId = Encryption.DecryptCardNumber(billingAgreementId)
            };
            IBillingAgreementUdpateResponse billingAgreementInfoResponse;
            try
            {
                billingAgreementInfoResponse = _securePayPalPayment.UpdateBillingAgreement(billingAgreementRequest);

                var properties = new Dictionary<string, object>
                {
                    {"BillingAgreemendId", billingAgreementId},
                    {
                        "PaymentMethodId",
                        (paymentMethod != null) ? paymentMethod.PaymentMethodId.ToDisplay() : string.Empty
                    }
                };
                if (billingAgreementInfoResponse != null && billingAgreementInfoResponse.BillingAddress != null)
                {
                    properties.Add("Billing Address Street",
                        billingAgreementInfoResponse.BillingAddress.Street1.ToDisplay());
                    properties.Add("Billing Address City", billingAgreementInfoResponse.BillingAddress.City.ToDisplay());
                }
                LogReloadDetails(TraceEventType.Information, null,
                    "CardApi - Reload GetBillingAgreementInfo_UpdateBillingAgreement",
                    "Result of _securePayPalPayment.UpdateBillingAgreement w/no exception.", properties);
            }
            catch (Exception ex)
            {
                _loggingManager.Increment(
                    "cardTransactionProvider_GetBillingAgreementInfo_UpdateBillingAgreement_FailedWithException");

                // Log it and swallow the exception. Won't abort reporting/check if we can't get this.

                var properties = new Dictionary<string, object>
                {
                    {"BillingAgreemendId", billingAgreementId},
                    {
                        "PaymentMethodId",
                        (paymentMethod != null) ? paymentMethod.PaymentMethodId.ToDisplay() : string.Empty
                    }
                };
                LogReloadDetails(TraceEventType.Error, null,
                    "CardApi - Reload GetBillingAgreementInfo_UpdateBillingAgreement_FailedWithException",
                    ex.Message, properties);

                return null;
            }
            return billingAgreementInfoResponse;
        }

        private ICardTransaction ReloadByNumberWithPayPal(string orderId, Guid guid, string number, object o,
            decimal amount, IBillingInfo billingInfo, string submarket, bool isAutoReload, string candidateId,
            bool isGiftReload)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region HelperFunctions

        private bool UseAfs
        {
            get { return _cardTransactionProviderSettingsSection.AFS; }
        }

        private bool UseAvs
        {
            get { return _cardTransactionProviderSettingsSection.AVS; }
        }

        private bool UseDecisionManager
        {
            get { return _cardTransactionProviderSettingsSection.DecisionManager; }
        }

        private bool SkipVLCardReloadRollbackOnBillTimeout
        {
            get { return _cardTransactionProviderSettingsSection.SkipVLCardReloadRollbackOnPsapiBillTimeout; }
        }

        private static BillingInfo CreateBillingInfo(string emailAddress,
            StarbucksAccountProvider.Common.Models.IAddress billingAddress,
            IPaymentMethod paymentMethod, IMerchantInfo merchantInfo)
        {
            var billingInfo = new BillingInfo
            {
                EmailAddress = emailAddress,
                CurrencyNumericCode = merchantInfo.CurrencyNumber,
                Currency = merchantInfo.CurrencyCode,
                Address = billingAddress.ToSecurePaymentUserAddress(),
                PaymentMethod = paymentMethod.ToSecurePaymentPaymentMethod()
            };
            return billingInfo;
        }

        private static BillingInfo CreateBillingInfo(string emailAddress,
            StarbucksAccountProvider.Common.Models.IAddress billingAddress, IMerchantInfo merchantInfo)
        {
            var billingInfo = new BillingInfo
            {
                EmailAddress = emailAddress,
                CurrencyNumericCode = merchantInfo.CurrencyNumber,
                Currency = merchantInfo.CurrencyCode,
                Address = (billingAddress == null) ? null : billingAddress.ToSecurePaymentUserAddress(),
                PaymentMethod = new PaymentModels.PaymentMethod()
            };
            return billingInfo;
        }

        private StarbucksAccountProvider.Common.Models.IAddress GetAddress(string userId, IPaymentMethod paymentMethod)
        {
            StarbucksAccountProvider.Common.Models.IAddress billingAddress = null;

            try
            {
                var billingAddressId = paymentMethod.BillingAddressId;

                // _accountProvider.GetAddress will not work for Guest Payment method addresses.

                billingAddress = _accountProvider.GetAddress(userId, billingAddressId);
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex,
                    "cardTransactionProvider_GetAddress_accountProvider.GetAddress_FailedWithException",
                    "CardApi - Reload", TraceEventType.Error);
            }

            if (billingAddress == null)
            {
                throw new CardTransactionException(CardTransactionErrorResource.BillingAddressNotFoundCode,
                    CardTransactionErrorResource.BillingAddressNotFoundMessage);
            }
            return billingAddress;
        }

        private IPaymentMethod GetPaymentMethod(string userId, string paymentMethodId)
        {
            Validator.ValidatePaymentMethod(paymentMethodId);

            IPaymentMethod paymentMethod = ValidGetPaymentUserId(userId)
                ? _paymentMethodProvider.GetPaymentMethod(userId, paymentMethodId)
                : _paymentMethodProvider.GetPaymentMethod(paymentMethodId);

            if (paymentMethod == null)
            {
                throw new CardTransactionException(CardTransactionErrorResource.PaymentMethodNotFoundCode,
                    CardTransactionErrorResource.PaymentMethodNotFoundMessage);
            }
            return paymentMethod;
        }


        private bool ValidGetPaymentUserId(string userId)
        {
            return !(String.IsNullOrWhiteSpace(userId) || userId.Contains(UserIdForTempPaymentMethods));
        }


        private void ProcessValueLinkResponse(string cardNumber,
            CardIssuer.Dal.Common.Models.ICardTransaction cardTransaction)
        {
            //Parse out the request code from the enum value.
            int requestCode;

            if (Int32.TryParse(cardTransaction.RequestCode, out requestCode))
            {
                var request = (Enums.CallItCommand)requestCode;
                //Set the description name from the enum value, parsing out any Pascal-cased values.
                cardTransaction.Description = request.ToString().ParsePacal();
            }

            try
            {
                var trans = new CardModels.CardTransaction(cardTransaction);

                //Update transaction history and balance.
                if (cardNumber == null)
                {
                    _cardProvider.UpsertCardTransactionHistory(trans);
                    _cardProvider.UpsertCardBalance(cardTransaction.CardId, cardTransaction.EndingBalance,
                        cardTransaction.TransactionDate, cardTransaction.Currency);
                }
                else
                {
                    _cardProvider.UpsertCardTransactionHistory(cardNumber, trans);
                    _cardProvider.UpsertCardBalanceByDecryptedNumber(cardNumber, cardTransaction.EndingBalance,
                        cardTransaction.TransactionDate,
                        cardTransaction.Currency);
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex, "Error in ProcessValueLinkResponse.", "CardApi - Reload",
                    TraceEventType.Error);
            }
        }

        internal static void BuildRequestProperties(Dictionary<string, object> properties,
            ReloadTransactionRequest transactionRequest)
        {
            if (transactionRequest != null)
            {
                properties.Add("request: UserId", transactionRequest.UserId.ToDisplay());
                properties.Add("request: CardId", transactionRequest.Card.CardId.ToDisplay());
                properties.Add("request: CurrentTime", DateTime.Now.ToString().ToDisplay());
                properties.Add("request: PaymentMethodId",
                    transactionRequest.BillingInfo.PaymentMethod.PaymentMethodId.ToDisplay());
                properties.Add("request: Account Number (BillingAgreemendId - encrypted)",
                    transactionRequest.BillingInfo.PaymentMethod.AccountNumber.ToDisplay());
                properties.Add("request: OrderId", transactionRequest.OrderId.ToDisplay());
                properties.Add("request: EmailAddress", transactionRequest.EmailAddress.ToDisplay());
            }
        }

        internal static void BuildResponseProperties(Dictionary<string, object> properties,
            IDoReferenceTransactionResponse payPalResponse)
        {
            if (payPalResponse != null)
            {
                properties.Add("Response: BillingAgreementId", payPalResponse.BillingAgreementId.ToDisplay());
                if (payPalResponse.TransactionDetails != null)
                {
                    var transactionDetails = payPalResponse.TransactionDetails;
                    properties.Add("Response: PayPal Auth transactionId", transactionDetails.TransactionId.ToDisplay());
                    properties.Add("Response: PaymentStatusCode",
                        transactionDetails.PaymentStatusCode.ToString().ToDisplay());
                    properties.Add("Response: PendingStatusCode",
                        transactionDetails.PendingStatusCode.ToString().ToDisplay());
                    properties.Add("Response: PayPalAckCode",
                        transactionDetails.PayPalAckCode.ToString().ToDisplay());

                    properties.Add("Response: hasErrors", (transactionDetails.Errors != null).ToString());

                    if (transactionDetails.Errors != null)
                    {
                        using (var item = transactionDetails.Errors.GetEnumerator())
                        {
                            int count = 0;
                            while (item.MoveNext())
                            {
                                count++;
                                properties.Add(String.Format("response: Errors #{0}.ErrorCode", count), item.Current.ErrorCode.ToDisplay());
                                properties.Add(String.Format("response: Errors #{0}.LongMessage", count), item.Current.LongMessage.ToDisplay());
                            }
                        }
                    }
                }
            }
        }


        internal void LogPayPalAuthResult(ReloadTransactionRequest transactionRequest,
            IDoReferenceTransactionResponse payPalResponse)
        {
            try
            {
                var properties = new Dictionary<string, object>();
                bool authFailed = PayPalAuthFailed(payPalResponse);
                var level = authFailed ? TraceEventType.Warning : TraceEventType.Information;

                properties.Add("response: PayPal Auth {0}", authFailed ? "Failed" : "Succeeded");

                BuildRequestProperties(properties, transactionRequest);

                BuildResponseProperties(properties, payPalResponse);

                LogReloadDetails(level, null, "CardApi - ReloadCardPayPalAuth",
                    "ReloadStarbucksCardByCardNumberWithPayPal, PayPal Auth Result", properties);
            }
            catch (Exception ex) // logging exception, we have to eat it
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        static void LogTimer(Stopwatch stopWatch, string message, Dictionary<string, object> extendedProperties = null)
        {
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds);
            LogReloadDetails(TraceEventType.Information, null, "CardApi - Reload Timer", string.Format("{0}: elapsedTime: {1}", message, elapsedTime), extendedProperties);
        }

        private static void LogReloadDetails(TraceEventType eventType, IPayPalFault fault, string title, string message, Dictionary<string, object> extendedProperties)
        {
            var log = new LogEntry { Message = message, EventId = 100, Title = title, Severity = eventType };
            log.Categories.Add("APILog");
            if (extendedProperties != null)
            {
                foreach (var item in extendedProperties)
                {
                    log.ExtendedProperties.Add(item.Key, item.Value);
                }
            }

            if (fault != null)
            {
                foreach (var x in fault.Errors)
                {
                    log.AddErrorMessage(x.ErrorCode + " " + x.LongMessage);
                }
            }

            Logger.Write(log);
        }

        internal static void LogAggregateExceptionPayPal(string message, ReloadTransactionRequest transactionRequest, AuthorizePayPalResults payPalAuthResults, Exception ex)
        {
            try
            {
                Dictionary<string, object> properties = new Dictionary<string, object>();
                var transactionId = payPalAuthResults == null
                    ? null
                    : payPalAuthResults.PayPalResponse == null
                        ? null
                        : payPalAuthResults.PayPalResponse.TransactionDetails == null
                            ? null
                            : payPalAuthResults.PayPalResponse.TransactionDetails.TransactionId;

                BuildRequestPropertiesPayPal(properties, transactionRequest, transactionId);
                LogPSApiCallDetails.BuildExceptionProperties(properties, ex);

                LogPSApiCallDetails.PerformLogging(message, TraceEventType.Error, "CardApi - Reload Aggregate Exception PayPal",
                    properties);
            }
            catch (Exception err) // logging exception, we have to eat it
            {
                System.Diagnostics.Debug.WriteLine(err.ToString());
            }
        }

        internal static void LogReloadExceptionPayPal(string message, ReloadTransactionRequest transactionRequest, string transactionId, Exception ex)
        {
            try
            {
                Dictionary<string, object> properties = new Dictionary<string, object>();
                BuildRequestPropertiesPayPal(properties, transactionRequest, transactionId);
                LogPSApiCallDetails.BuildExceptionProperties(properties, ex);

                LogPSApiCallDetails.PerformLogging(message, TraceEventType.Error, "CardApi - Reload Exception PayPal",
                    properties);
            }
            catch (Exception err) // logging exception, we have to eat it
            {
                System.Diagnostics.Debug.WriteLine(err.ToString());
            }
        }

        internal static void BuildRequestPropertiesPayPal(Dictionary<string, object> properties,
            ReloadTransactionRequest transactionRequest, string transactionId)
        {
            if (transactionRequest != null)
            {
                properties.Add("request: TransactionType", "ReloadWithPayPal");
                properties.Add("request: TransactionId", transactionId.ToDisplay());
                properties.Add("request: OrderId", transactionRequest.OrderId.ToDisplay());
                properties.Add("request: AccountNumber",
                    transactionRequest.BillingInfo.PaymentMethod.AccountNumber.ToDisplay());
                properties.Add("request: BillingAgreementId", transactionRequest.BillingInfo.PaymentMethod.AccountNumber.ToDisplay());
                properties.Add("request: CurrentTime", DateTime.Now.ToString());
                properties.Add("request: Submarket", transactionRequest.Submarket.ToDisplay());
                properties.Add("request: PayPalPayerId", transactionRequest.BillingInfo.PayPalPayerId.ToDisplay());
                properties.Add("request: PaymentMethodId",
                    transactionRequest.BillingInfo.PaymentMethod.PaymentMethodId.ToDisplay());
                properties.Add("request: CardId", transactionRequest.Card.CardId.ToDisplay());
                properties.Add("request: Currency", transactionRequest.BillingInfo.Currency.ToDisplay());
                properties.Add("request: EmailAddress", transactionRequest.EmailAddress.ToDisplay());
                properties.Add("request: UserId", transactionRequest.UserId.ToDisplay());
            }
        }

        public static List<IOrderLineItem> GetReloadLineItem(decimal amount, string fraudThreshold = null)
        {
            var items = new List<IOrderLineItem>();
            var item = new OrderLineItem
            {
                ProductOrdered = new PaymentModels.Product
                {
                    ProductDescription = "starbucks card reload",
                    ProductCode = "SCRD",
                    ProductName = "starbucks card reload",
                    SKU = "SCRD",
                    JDAItemNumber = "000176691",
                    ProductID = "SCRD",
                    ItemAmount = amount
                },
                Quantity = 1,
                UnitPrice = amount,
                OrderType = "SbuxReld",
            };

            //TODO: this is hard-coded now, but needs to be pulled from Commerce Server once
            //    we get the CS test server set up
            SetCybersourceAfsProperties(item, fraudThreshold);

            items.Add(item);

            return items;
        }

        //TODO: Move to PaymentProviderSettings?
        public static void SetCybersourceAfsProperties(OrderLineItem item, string fraudThreshold = null)
        {
            item.ProductOrdered.GiftCategory = ConfigurationManager.AppSettings["giftCategory"]; //"No";
            item.ProductOrdered.HostHedge = ConfigurationManager.AppSettings["hostHedge"]; //"Normal";
            item.ProductOrdered.Threshold = (fraudThreshold ?? ConfigurationManager.AppSettings["threshold"]); //"99";
            item.ProductOrdered.TimeCategory = ConfigurationManager.AppSettings["timeCategory"]; //"Off";
            item.ProductOrdered.TimeHedge = ConfigurationManager.AppSettings["timeHedge"]; //"Off";
            item.ProductOrdered.VelocityHedge = ConfigurationManager.AppSettings["velocityHedge"]; //"High";
        }

        private IMerchantInfo GetMerchantInfo(string currentSubMarket, string platform)
        {
            return _cardIssuer.GetMerchantInfo(currentSubMarket, platform);
        }

        private IAssociatedCard GetAssociatedCard(string userId, string cardId, VisibilityLevel visibilityLevel,
                                             bool includeAutoReload, string market, string userMarket, bool includeAssociatedCards = false, string platform = UnKnownPlatform)
        {
            var card = _cardProvider.GetCardById(userId, cardId, visibilityLevel, includeAutoReload, market, userMarket,
                includeAssociatedCards, platform);
            if (card == null)
            {
                throw new CardTransactionException(CardTransactionErrorResource.CardNotFoundCode,
                                                   CardTransactionErrorResource.CardNotFoundMessage);
            }
            return card;
        }

        private ICard GetCard(string cardNumber, string pin, string market)
        {
            var card = _cardProvider.GetCardByNumberAndPin(cardNumber, pin, VisibilityLevel.Decrypted, market);
            if (card == null)
            {
                throw new CardTransactionException(CardTransactionErrorResource.CardNotFoundCode,
                                                   CardTransactionErrorResource.CardNotFoundMessage);
            }
            return card;
        }

        /// <summary>
        ///     Gets a newly provisioned order ID for Commerce Server.
        /// </summary>
        /// <returns>Returns the new order ID.</returns>
        public static string GetNewCommerceServerOrderId()
        {
            return Guid.NewGuid().ToBase5GuidString();
        }

        private StarbucksAccountProvider.Common.Models.IUser GetUser(string userId)
        {
            var user = _accountProvider.GetUser(userId);
            if (user == null)
            {
                throw new CardTransactionException(CardTransactionErrorResource.UserNotFoundCode,
                                                   CardTransactionErrorResource.UserNotFoundMessage);
            }
            return user;
        }

        #endregion

        internal class AuthorizePayPalResults
        {
            public IDoReferenceTransactionResponse PayPalResponse { get; set; }
            public Exception Exception { get; set; }
        }

        internal class FakeFruadCheckResult : IFraudCheckResult
        {
            public DecisionType DecisionType { get; set; }

            public DecisionReason DecisionReason { get; set; }
        }

    }
}
