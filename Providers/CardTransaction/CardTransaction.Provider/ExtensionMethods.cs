﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Starbucks.CardTransaction.Provider
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Converts a GUID to a base5 GUID string for use with Commerce Server.
        /// </summary>
        /// <param name="guid">The GUID to convert to a base5 string.</param>
        /// <returns>Returns the base5 GUID version of the GUID.</returns>
        /// <remarks>
        /// This is a purely .NET implementation that does not rely upon any
        /// Commerce Server libraries. The following Commerce Server documentation
        /// describes a base 5 GUID:
        /// 
        /// A base5 GUID is used to store a slightly more compact version of the
        /// traditional GUID. The 128 binary digits are divided into groups of 5
        /// instead of 4 with each group represented by the characters 0-9, and
        /// 22 characters of the alphabet. This results in a 26-digit representation
        /// for the GUID rather than the customary 32-digit representation.
        /// </remarks>
        public static string ToBase5GuidString(this Guid guid)
        {
            var sb = new StringBuilder();

            // Get the byte representation of the GUID, consisting of 16 bytes.
            byte[] bytes = guid.ToByteArray();

            // Since each character in a base5 GUID is represented by 5 bits,
            // we'll do this 5 bytes at a time. This is because each 5-byte group
            // contains exactly 8 of the 5-bit characters.

            // A GUID consists of 16 bytes, so do there are 3 groups of 5.
            for (int groupIndex = 0; groupIndex < 3; groupIndex++)
            {
                // Separate out the group of 5 bytes from the byte representation
                // of the GUID.
                byte[] byteGroup = new byte[5];
                for (int byteIndex = 0; byteIndex < byteGroup.Length; byteIndex++)
                {
                    byteGroup[byteIndex] = bytes[(groupIndex * 5) + byteIndex];
                }

                // Now get each of the 5-bit characters contained in this byte group.
                // Since there are 5 bytes (40 bits) in the group, there are 8 characters.
                var characters = new int[8];

                // These bitwise operations isolate 5 bits from a byte to form a character.
                // They then shift the byte to the right by 5 bits. Since this leaves only
                // 3 bits for the next character, 2 more bits are grabbed from the next
                // byte in the group.
                //
                // That byte is then shifted right by 2 bits and 5 bits are isolated.
                // For the next character, some more bits are grabbed from the *next* byte,
                // which is again shifted to the right by a few bytes, and so on.
                //
                // Since there are 8 bytes, for a total of 40 bits, we end up with no
                // extra bits left over at the end.
                characters[0] = byteGroup[0] & 31;
                characters[1] = (byteGroup[0] >> 5) + (byteGroup[1] & 3);
                characters[2] = (byteGroup[1] >> 2) & 31;
                characters[3] = (byteGroup[1] >> 7) + (byteGroup[2] & 15);
                characters[4] = (byteGroup[2] >> 4) + (byteGroup[3] & 1);
                characters[5] = (byteGroup[3] >> 1) & 31;
                characters[6] = (byteGroup[3] >> 6) + (byteGroup[4] & 7);
                characters[7] = (byteGroup[4] >> 3) & 31;

                // Convert each of the 5-bit characters to a base32 char and append
                // it to the StringBuilder.
                for (int numberIndex = 0; numberIndex < characters.Length; numberIndex++)
                {
                    sb.Append((char) characters[numberIndex].ToBase32Char());
                }
            }

            // We have one byte left over from the original GUID. This byte contains
            // one 5-bit character and one 3-bit character. Convert the first one to
            // base32 char and append it to the StringBuilder. The second one doesn't
            // have to be converted since it's only 3 bits (so it's already in base32).
            sb.Append((char) (bytes[15] & 31).ToBase32Char());
            sb.Append(bytes[15] >> 5);

            return sb.ToString();
        }
        /// <summary>
        /// Converts a Pascal-cased enum string value into a string of words separated by spaces.
        /// <example>"MyValue" into "My Value"</example>
        /// </summary>
        /// <param name="value">The string with Pascal-cased characters to parse.</param>
        /// <returns>The Pascal-cased string parsed into separate words</returns>
        public static string ParsePacal(this string value)
        {
            //Regex for a capitalized word.
            Regex wordUp = new Regex(@"[A-Z]{1}[a-z0-9]+");

            //Find all of the capitalized words in the enum string value.
            MatchCollection pascalWords = wordUp.Matches(value.ToString(CultureInfo.InvariantCulture));

            StringBuilder words = new StringBuilder();

            //Loop through the matches, and add each word to the string builder.
            foreach (Match wordMatch in pascalWords)
            {
                //Append the word found followed by a space.
                words.AppendFormat("{0} ", wordMatch.Value);
            }

            //Return the entire string trimmed of leading/tailing spaces.
            return words.ToString().Trim();

        }
        /// <summary>
        /// Returns a base32 character representing an integer. A base32
        /// character consists of the digits 0-9 and 22 letters of the alphabet,
        /// not including I, O, Y, or Z.
        /// </summary>
        /// <param name="value">The integer value to convert to a base32 character.</param>
        /// <returns>Returns a base32 character representing the specified integer.</returns>
        public static char ToBase32Char(this int value)
        {
            if (value > 31)
            {
                throw new ArgumentOutOfRangeException("value", @"Value must be less than or equal to 31.");
            }

            char c;

            if (value < 10)
            {
                // Since the value is less than 10, just get the char
                // that is the same as the digit.
                c = (char)(value + 48);
            }
            else
            {
                // The value is 10 or more. Convert it to an uppercase letter
                // consisting of the first 22 characters of the alphabet.
                c = (char)(value + 55);
            }

            // Switch out some chars for others that are less easily confused
            // with numeric digits. The chars we are substituting are from the
            // last 4 letters of the alphabet to ensure that we originally
            // didn't use.
            switch (c)
            {
                case 'I':
                    c = 'W';
                    break;

                case 'O':
                    c = 'X';
                    break;
            }

            return c;
        }

    }
}
