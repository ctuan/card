﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Starbucks.Platform.OrderManagement.Contracts.Email;
using OMC = Starbucks.Platform.OrderManagement.Contracts.DataContracts;
using Starbucks.CardTransaction.Provider.Common.Model;
using Starbucks.Platform.OrderManagement.Contracts.DataContracts;

namespace Starbucks.CardTransaction.Provider
{
	/// <summary>
	/// Transform various types into other types
	/// </summary>
	public static class Transforms
	{
        internal static StageEmailArgs ToMailMessageBroker(this IStarbucksCardReloadConfirmation reloadConfirmation)
        {
            var replacements = new Dictionary<string, string>();

            var repeaterRows = new List<RepeaterRow>();
            decimal subTotal = 0;

            NumberFormatInfo nfi = CultureInfo.CurrentCulture.NumberFormat;
            nfi = (NumberFormatInfo)nfi.Clone();
            nfi.CurrencySymbol = "";

            foreach (var reloadItem in reloadConfirmation.Items)
            {
                var row = new RepeaterRow();
                row.Columns = new List<RepeaterColumn>();
                row.Columns.Add(new RepeaterColumn() { Alignment = ColumnAlignment.Left, Value = reloadItem.ProductOrdered.SKU });
                row.Columns.Add(new RepeaterColumn() { Alignment = ColumnAlignment.Left, Value = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(reloadItem.ProductOrdered.ProductName)});
                row.Columns.Add(new RepeaterColumn() { Alignment = ColumnAlignment.Center, Value = reloadItem.Quantity.ToString() });
                row.Columns.Add(new RepeaterColumn() { Alignment = ColumnAlignment.Right, Value = string.Format(nfi, "{0:0.00}", reloadItem.ProductOrdered.ItemAmount) });
                row.Columns.Add(new RepeaterColumn() { Alignment = ColumnAlignment.Right, Value = string.Format(nfi, "{0:0.00}", (reloadItem.ProductOrdered.ItemAmount * reloadItem.Quantity)) });
                repeaterRows.Add(row);
                subTotal += reloadItem.Quantity * reloadItem.ProductOrdered.ItemAmount;
            }

            // Subject
            replacements.Add("ORDERID", reloadConfirmation.OrderID);
            replacements.Add("CURRENCY", reloadConfirmation.CurrencyType);
            replacements.Add("DATERECIEVED", reloadConfirmation.DateReceived);
            replacements.Add("FULLNAME", reloadConfirmation.FullName);
            replacements.Add("ADDRESSLINES", reloadConfirmation.AddressLines);
            replacements.Add("CITYSTATEZIP", reloadConfirmation.CityStateZip);
            replacements.Add("EMAILADDRESS", reloadConfirmation.EmailAddress);
      
            string subTotalString = string.Format(nfi, "{0:0.00}", subTotal);
            replacements.Add("SUBTOTAL", subTotalString); 

            // Tax, shipping and total are inapplicable to card reload.
            replacements.Add("TAX", "0.00"); 
            replacements.Add("SHIPPING", "0.00");
            replacements.Add("TOTAL", string.Format(CultureInfo.CurrentCulture, "{0} {1}", reloadConfirmation.CurrencyType, subTotalString)); 

            var item = new StageEmailArgs
            {
                MessageId = Guid.NewGuid().ToString(),
                EmailTemplateName = QueuedEmailType.ReloadConfirmation.ToString(),
                RecipientList = new List<string> { reloadConfirmation.EmailAddress },
                ReplacementValues = replacements,
                Site = reloadConfirmation.Site,
                Language = reloadConfirmation.Language,
                RepeaterRows = repeaterRows,
                SendOn = null, //![Obsolete]?
                SendDelayAsHours = 0 //![Obsolete]?
            };

            return item;
        }
	}
}
