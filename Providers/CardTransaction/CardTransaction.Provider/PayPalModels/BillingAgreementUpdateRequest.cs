﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;

namespace Starbucks.CardTransaction.Provider.PayPalModels
{
    public class BillingAgreementUpdateRequest : IBillingAgreementUdpateRequest 
    {
        public string ReferenceId { get; set; }
        public string Currency { get; set; }
        public string Custom { get; set; }
        public string Description { get; set; }
    }
}
