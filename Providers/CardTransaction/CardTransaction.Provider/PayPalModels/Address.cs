﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;

namespace Starbucks.CardTransaction.Provider.PayPalModels
{
    public class Address : IAddress
    {
        public string AddressId { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string ExternalAddressId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string PostalCode { get; set; }
        public string StateOrProvince { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
    }
}
