﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;

namespace Starbucks.CardTransaction.Provider.PayPalModels
{
    public class PaymentDetailsLineItem : IPaymentDetailsLineItem
    {
        public decimal ItemAmount { get; set; }
        public string ItemCode { get; set; }
        public string ItemDescription { get; set; }
        public string ItemName { get; set; }
        public int Quantity { get; set; }
    }
}
