﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;

namespace Starbucks.CardTransaction.Provider.PayPalModels
{
    public class PaymentDetails : IPaymentDetails
    {
        public string Currency { get; set; }
        public decimal Discount { get; set; }
        public IEnumerable<IPaymentDetailsLineItem> LineItems { get; set; }
        public decimal MaxAmount { get; set; }
        public decimal OrderAmount { get; set; }
        public string OrderDescription { get; set; }
        public DateTime? PaymentDate { get; set; }
        public decimal ShippingAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal OrderSubTotal { get; private set; }
        public decimal OrderTotal { get; private set; }
    }
}
