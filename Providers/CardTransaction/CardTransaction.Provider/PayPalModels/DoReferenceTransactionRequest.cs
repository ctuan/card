﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;

namespace Starbucks.CardTransaction.Provider.PayPalModels
{
    public class DoReferenceTransactionRequest : IDoReferenceTransactionRequest
    {
        public PaymentActionCode PaymentAction { get; set; }
        public IPaymentDetails PaymentDetails { get; set; }
        public string ReferenceId { get; set; }
        public bool RequireShippingConfirmation { get; set; }
        public string SoftDescriptor { get; set; }
        public object ExtensionData { get; set; }
        public string IPAddress { get; set; }
        public string UserId { get; set; }
        public string Locale { get; set; }
    }
}
