﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;

namespace Starbucks.CardTransaction.Provider.PayPalModels
{
    public class BillRequest : IBillRequest 
    {
        public string AuthorizationId { get; set; }
        public string Note { get; set; }
        public string OrderId { get; set; }
        public string Subject { get; set; }
    }
}
