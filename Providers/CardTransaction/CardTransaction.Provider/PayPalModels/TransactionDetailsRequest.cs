﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;

namespace Starbucks.CardTransaction.Provider.PayPalModels
{
    public class TransactionDetailsRequest : ITransactionDetailsRequest 
    {
        public string TransactionId { get; set; }
        public string Currency { get; set; }
    }
}
