﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceProxies.Order.Wcf.Configuration;

namespace Starbucks.CardTransaction.Provider.Configuration
{
    public class CardTransactionProviderSettingsSection : ConfigurationSection
    {
        //private static CardTransactionProviderSettingsSection _settings;
        //public static CardTransactionProviderSettingsSection Settings
        //{
        //    get
        //    {
        //        return _settings ??
        //               (_settings =
        //                ConfigurationManager.GetSection("cardTransactionProviderSettingsSection") as CardTransactionProviderSettingsSection);
        //    }
        //}

        [ConfigurationProperty("AFS")]
        public bool AFS
        {
            get { return (bool)this["AFS"]; }
            set { this["AFS"] = value; }
        }

        [ConfigurationProperty("AVS")]
        public bool AVS
        {
            get
            {
                return (bool)this["AVS"];
            }
            set
            {
                this["AVS"] = value;
            }
        }

        [ConfigurationProperty("decisionManager")]
        public bool DecisionManager
        {
            get
            {
                return (bool)this["decisionManager"];
            }
            set
            {
                this["decisionManager"] = value;
            }
        }

        [ConfigurationProperty("fraudCheckThresholdDays")]
        public int FraudCheckThresholdDays
        {
            get { return (int)this["fraudCheckThresholdDays"]; }
            set { this["fraudCheckThresholdDays"] = value; }
        }

        [ConfigurationProperty("twacEmailTemplateCode")]
        public string TwacEmailTemplateCode
        {
            get { return (string)(this["twacEmailTemplateCode"] ?? string.Empty); }
            set { this["twacEmailTemplateCode"] = value; }
        }

        [ConfigurationProperty("twacTemplateCode")]
        public string TwacTemplateCode
        {
            get { return (string)(this["twacTemplateCode"] ?? string.Empty); }
            set { this["twacTemplateCode"] = value; }
        }

        [ConfigurationProperty("twacPromoCode")]
        public string TwacPromoCode
        {
            get { return (string)(this["twacPromoCode"] ?? string.Empty); }
            set { this["twacPromoCode"] = value; }
        }

        [ConfigurationProperty("twacProductIds")]
        public string TwacProductIds
        {
            get { return (string)(this["twacProductIds"] ?? string.Empty); }
            set { this["twacProductIds"] = value; }
        }

        [ConfigurationProperty("deliveredBy")]
        public string DeliveredBy
        {
            get { return (string)(this["deliveredBy"] ?? string.Empty); }
            set { this["deliveredBy"] = value; }
        }

        [ConfigurationProperty("deliveryMethod")]
        public string DeliveryMethod
        {
            get { return (string)(this["deliveryMethod"] ?? string.Empty); }
            set { this["deliveryMethod"] = value; }
        }

        [ConfigurationProperty("eCardEmailDelivery")]
        public string ECardEmailDelivery
        {
            get { return (string)this["eCardEmailDelivery"] ?? string.Empty; }
            set { this["eCardEmailDelivery"] = value; }
        }

        [ConfigurationProperty("eCardEmailChallengeDescription")]
        public string ECardEmailChallengeDescription
        {
            get { return (string)this["eCardEmailChallengeDescription"] ?? string.Empty; }
            set { this["eCardEmailChallengeDescription"] = value; }
        }

        [ConfigurationProperty("eCardFacebookChallengeDescription")]
        public string ECardFacebookChallengeDescription
        {
            get { return (string)this["eCardFacebookChallengeDescription"] ?? string.Empty; }
            set { this["eCardFacebookChallengeDescription"] = value; }
        }

        [ConfigurationProperty("eCardFacebookDelivery")]
        public string ECardFacebookDelivery
        {
            get { return (string)this["eCardFacebookDelivery"] ?? string.Empty; }
            set { this["eCardFacebookDelivery"] = value; }
        }

        [ConfigurationProperty("orderService")]
        public OrderServiceElement OrderService
        {
            get { return (OrderServiceElement)this["orderService"]; }
            set { this["orderService"] = value; }
        }

        [ConfigurationProperty("skipVLCardReloadRollbackOnPsapiBillTimeout")]
        public bool SkipVLCardReloadRollbackOnPsapiBillTimeout
        {
            get { return (bool)this["skipVLCardReloadRollbackOnPsapiBillTimeout"]; }
            set { this["skipVLCardReloadRollbackOnPsapiBillTimeout"] = value; }
        }

        [ConfigurationProperty("blockPayPalReloads", DefaultValue = false)]
        public bool BlockPayPalReloads
        {
            get { return (bool)this["blockPayPalReloads"]; }
            set { this["blockPayPalReloads"] = value; }
        }

        [ConfigurationProperty("ignorePayPalReloadsFraudCheck", DefaultValue = false)]
        public bool IgnorePayPalReloadsFraudCheck
        {
            get { return (bool)this["ignorePayPalReloadsFraudCheck"]; }
            set { this["ignorePayPalReloadsFraudCheck"] = value; }
        }

        [ConfigurationProperty("mockAmountEnabled")]
        public bool MockAmountEnabled
        {
            get { return (bool)this["mockAmountEnabled"]; }
            set { this["mockAmountEnabled"] = value; }
        }

        [ConfigurationProperty("mockTransactionReferenceKeyEnabled")]
        public bool MockTransactionReferenceKeyEnabled
        {
            get { return (bool)this["mockTransactionReferenceKeyEnabled"]; }
            set { this["mockTransactionReferenceKeyEnabled"] = value; }
        }

        [ConfigurationProperty("mockTransactionReferenceKey")]
        public string MockTransactionReferenceKey
        {
            get { return (string)this["mockTransactionReferenceKey"]; }
            set { this["mockTransactionReferenceKey"] = value; }
        }

        [ConfigurationProperty("mockAmount")]
        public decimal MockAmount
        {
            get { return (decimal)this["mockAmount"]; }
            set { this["mockAmount"] = value; }
        }

    }
}
