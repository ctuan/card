﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.Product.Provider.Common.Model.Request;

namespace Starbucks.CardTransaction.Provider.ProductModels
{
    public class ProductQuery : IProductQuery
    {
        public int? ProductNumber { get; set; }
        public Guid? SystemId { get; set; }
        public string Urn { get; set; }
        public string Sku { get; set; }
        public string CountryCode { get; set; }
        public string Locale { get; set; }
        public bool IncludeInactive { get; set; }
    }
}
