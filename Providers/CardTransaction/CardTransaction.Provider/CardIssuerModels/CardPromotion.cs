﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.CardIssuer.Dal.Common.Models;

namespace Starbucks.CardTransaction.Provider.CardIssuerModels
{
    public class CardPromotion : ICardPromotion
    {
        public string Code { get; set; }
        public decimal Amount { get; set; }
        public string Message { get; set; }
    }
}
