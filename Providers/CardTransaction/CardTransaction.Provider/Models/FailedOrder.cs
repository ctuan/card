﻿using Starbucks.OrderManagement.Dal.Common.Models;
using System;

namespace Starbucks.CardTransaction.Provider.Models
{
    public class FailedOrder : IFailedOrder
    {
        public string BillingCurrency { get; set; }
        public string BillingEmail { get; set; }
        public string BillingFirstName { get; set; }
        public string BillingLastName { get; set; }
        public string BillingPhone { get; set; }
        public string BillingZipCode { get; set; }
        public int? CFSPoNumber { get; set; }
        public string CardFirstSix { get; set; }
        public string CardLastFour { get; set; }
        public string CaseNumber { get; set; }
        public string MerchantKey { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderId { get; set; }
        public string OrderSource { get; set; }
        public int? OrderStatus { get; set; }
        public string OrderType { get; set; }
        public int? PaymentMethodId { get; set; }
        public string PaypalPayerId { get; set; }
        public int? ReasonCode { get; set; }
        public string RequestedId { get; set; }
        public string ShipToLastName { get; set; }
        public string ShipToZipCode { get; set; }
        public string StarbucksCardNumber { get; set; } 
        public string SubMarketId { get; set; }
        public string UserId { get; set; }
    }
}
