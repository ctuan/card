﻿using Starbucks.CashStar.Provider.Common.Models;

namespace Starbucks.CashStar.Provider.Models
{
    public class EGiftCard : IEGiftCard
    {
        public string eGiftCardCode { get; set; }
        public string eGiftCardNumber { get; set; }
        public string AccessCode { get; set; }
        public string MerchantCode { get; set; }
        public decimal InitialBalance { get; set; }
        public decimal CurrentBalance { get; set; }
        public string BalanceLastUpdated { get; set; }
        public string BalanceUrl { get; set; }
        public string Currency { get; set; }
        public string Active { get; set; }
        public string Status { get; set; }
        public string Url { get; set; }
        public string Challenge { get; set; }
        public string Challenge_Description { get; set; }
        public string Challenge_Type { get; set; }
        public string FaceplateCode { get; set; }
        public string AuditNumber { get; set; }
        public string CountryIssued { get; set; }
        public string PromoCode { get; set; }
        public string TemplateCode { get; set; }
        public string EmailTemplateCode { get; set; }
        public IDelivery Delivery { get; set; }
        public IMessage Message { get; set; }
        public IAdditionalContent AdditionalContent { get; set; }
    }
}
