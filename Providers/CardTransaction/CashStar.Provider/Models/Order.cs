﻿using System.Collections.Generic;
using Starbucks.CashStar.Provider.Common.Models;

namespace Starbucks.CashStar.Provider.Models
{
    public class Order : IOrder 
    {
        public string OrderNumber { get; set; }
        public string TransactionId { get; set; }
        public string AuditNumber { get; set; }
        public string ActivationCallbackUrl { get; set; }
        public string OriginationLanguage { get; set; }
        public IEnumerable<IEGiftCard> eGiftCards { get; set; }
        public IPayment Payment { get; set; }
    }
}
