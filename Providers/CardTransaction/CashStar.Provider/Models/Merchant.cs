﻿using Starbucks.CashStar.Provider.Common.Models;

namespace Starbucks.CashStar.Provider.Models
{
    public class Merchant : IMerchant
    {
        public string MerchantCode { get; set; }
        public string Name { get; set; }
        public string LegalName { get; set; }
        public string Description { get; set; }
        public string Locations { get; set; }
        public string LogoImage { get; set; }
    }
}
