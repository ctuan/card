﻿using Starbucks.CashStar.Provider.Common.Models;

namespace Starbucks.CashStar.Provider.Models
{
    public class Message : IMessage
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Body { get; set; }
    }
}