﻿using Starbucks.CashStar.Provider.Common.Models;

namespace Starbucks.CashStar.Provider.Models
{
    public class WebSession : IWebSession
    {
        public string HttpHeaders { get; set; }
        public string JavascriptEnabled { get; set; }
        public string PurchaserIp { get; set; }
        public string PurchaserSessionId { get; set; }
        public string PurchaserSessionDuration { get; set; }
        public string ThreatMetrixSessionId { get; set; }
        public string IovationBlackbox { get; set; }
    }
}