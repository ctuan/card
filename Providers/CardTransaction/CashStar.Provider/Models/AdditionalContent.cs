﻿using Starbucks.CashStar.Provider.Common.Models;

namespace Starbucks.CashStar.Provider.Models
{
    public class AdditionalContent : IAdditionalContent
    {
        public string Type { get; set; }
        public string Content { get; set; }
    }
}