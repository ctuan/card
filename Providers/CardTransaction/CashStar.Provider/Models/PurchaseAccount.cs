﻿using Starbucks.CashStar.Provider.Common.Models;

namespace Starbucks.CashStar.Provider.Models
{
    public class PurchaseAccount  : IPurchaserAccount
    {
        public string UserId { get; set; }
        public int AccountAge { get; set; }
        public int DaysSinceLastPurchase { get; set; }
        public int NumberPurchases { get; set; }
        public int NumberDisputes { get; set; }
        public int NumberReturns { get; set; }
    }
}