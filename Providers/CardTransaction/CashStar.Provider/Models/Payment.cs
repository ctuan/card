﻿using Starbucks.CashStar.Provider.Common.Models;

namespace Starbucks.CashStar.Provider.Models
{
    public class Payment : IPayment
    {
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public IPurchaser Purchaser { get; set; }
        public IPurchaserAccount PurchaserAccount { get; set; }
        public ICreditCard CreditCard { get; set; }
        public IWebSession WebSession { get; set; }
        public IMobileAppSession MobileSession { get; set; }
    }

    public class MobilAppSession : IMobileAppSession
    {
        public string DeviceName { get; set; }
        public string DeviceModel { get; set; }
        public string SystemName { get; set; }
        public string SystemVersion { get; set; }
        public string DeviceId { get; set; }
        public string IpAddress { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string PhoneNumber { get; set; }
        public string PurchaserSessionId { get; set; }
        public string PurchaserSessionDuration { get; set; }
    }
}
