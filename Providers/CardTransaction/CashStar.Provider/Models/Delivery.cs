﻿using Starbucks.CashStar.Provider.Common.Models;

namespace Starbucks.CashStar.Provider.Models
{
    public class Delivery:IDelivery
    {
        public string DeliveredBy { get; set; }
        public string Method { get; set; }
        public string Target { get; set; }
        public string Scheduled { get; set; }
        public string Delivered { get; set; }
        public string Received { get; set; }
        public string Status { get; set; }
    }
}