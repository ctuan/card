﻿using Starbucks.CashStar.Provider.Common.Models;

namespace Starbucks.CashStar.Provider.Models
{
    public class Faceplate : IFaceplate
    {
       public string FaceplateCode { get; set; }
       public string Name { get; set; }
       public string TextColor { get; set; }
       public string ThumbnailImage { get; set; }
       public string PreviewImage { get; set; }
       public string PrintImage { get; set; }
    }
}
