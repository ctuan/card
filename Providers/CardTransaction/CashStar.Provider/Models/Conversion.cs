﻿using System.Linq;
using Starbucks.CashStar.Provider.Common.Models;
using Starbucks.Platform.Bom;
using Starbucks.Platform.Bom.Shared;
using Starbucks.Platform.Bom.Shared.eGiftEntities;
using CreditCard = Starbucks.CashStar.Provider.Models.CreditCard;
using Delivery = Starbucks.CashStar.Provider.Models.Delivery;
using Faceplate = Starbucks.CashStar.Provider.Models.Faceplate;
using Merchant = Starbucks.CashStar.Provider.Models.Merchant;
using Message = Starbucks.CashStar.Provider.Models.Message;
using Order = Starbucks.CashStar.Provider.Models.Order;
using Payment = Starbucks.CashStar.Provider.Models.Payment;
using PaymentType = Starbucks.Platform.Bom.Shared.PaymentType;
using PaymentTypeGroup = Starbucks.Platform.Bom.Shared.PaymentTypeGroup;
using Purchaser = Starbucks.CashStar.Provider.Models.Purchaser;
using UserAddressType = Starbucks.CashStar.Provider.Common.Models.UserAddressType;

namespace Starbucks.CashStar.Provider.Models
{
    public static class  Conversion
    {
        public static Starbucks.Platform.Bom.BillingInfo ToPlatform(this IBillingInfo billingInfo)
        {
            return new BillingInfo
                {
                    Address = billingInfo.Address.ToPlatform()  ,
                    AuthCode = billingInfo.AuthCode ,
                    AuthDate = billingInfo.AuthDate,
                    AuthRequestId = billingInfo.AuthRequestId ,
                    County = billingInfo.County ,
                    Currency = billingInfo.Currency ,
                    CurrencyNumericCode = billingInfo.CurrencyNumericCode ,
                    EmailAddress = billingInfo.EmailAddress ,
                    IsPayPalBillFailed = billingInfo.IsPayPalBillFailed ,
                    PayPalEmailAddress = billingInfo.PayPalEmailAddress ,
                    PayPalExpressCheckoutToken = billingInfo.PayPalExpressCheckoutToken ,
                    PayPalPayerId = billingInfo.PayPalPayerId ,
                    PayPalSubject = billingInfo.PayPalSubject ,
                    PayPalTransactionId = billingInfo.PayPalTransactionId ,
                    PaymentMethod = billingInfo.PaymentMethod.ToPlatform()  ,
                   
                };
        }

        public static PaymentMethod ToPlatform(this IPaymentMethod paymentMethod)
        {
            if (paymentMethod == null) return null;
            return new PaymentMethod
                {
                    AccountNumber = paymentMethod.AccountNumber,
                    BankName = paymentMethod.BankName,
                    BillingAddressId = paymentMethod.BillingAddressId,
                    Cvn = paymentMethod.Cvn,
                    ExpirationMonth = paymentMethod.ExpirationMonth,
                    ExpirationYear = paymentMethod.ExpirationYear,
                    FirstSix = paymentMethod.FirstSix,
                    FraudCheckedDate = paymentMethod.FraudCheckedDate,
                    FullAccountNumber = paymentMethod.FullAccountNumber,
                    FullName = paymentMethod.FullName,
                    Group = paymentMethod.Group.ToPlatform() ,
                    IsDefault = paymentMethod.IsDefault,
                    IsFraudChecked = paymentMethod.IsFraudChecked,
                    IsTemporary = paymentMethod.IsTemporary,
                    LastFour = paymentMethod.LastFour,
                    Nickname = paymentMethod.Nickname,
                    PaymentMethodId = paymentMethod.PaymentMethodId,
                    PaymentType = paymentMethod.PaymentType.ToPlatform() ,
                    RoutingNumber = paymentMethod.RoutingNumber,
                    SurrogateAccountNumber = paymentMethod.SurrogateAccountNumber,
                    UserId = paymentMethod.UserId
                };
        }

        public static PaymentType ToPlatform(this global::Starbucks.CashStar.Provider.Common.Models.PaymentType paymentType)
        {
            switch (paymentType)
            {
                case global::Starbucks.CashStar.Provider.Common.Models.PaymentType.ACH :
                    return PaymentType.ACH;
                case global::Starbucks.CashStar.Provider.Common.Models.PaymentType.Amex :
                    return PaymentType.Amex;
                case global::Starbucks.CashStar.Provider.Common.Models.PaymentType.Costcenter :
                    return PaymentType.Costcenter;
                case global::Starbucks.CashStar.Provider.Common.Models.PaymentType.Discover :
                    return PaymentType.Discover;
                case global::Starbucks.CashStar.Provider.Common.Models.PaymentType.LostCAN :
                    return PaymentType.LostCAN;
                case global::Starbucks.CashStar.Provider.Common.Models.PaymentType.LostGB :
                    return PaymentType.LostGB;
                case global::Starbucks.CashStar.Provider.Common.Models.PaymentType.LostUS :
                    return PaymentType.LostUS;
                case global::Starbucks.CashStar.Provider.Common.Models.PaymentType.MasterCard :
                    return PaymentType.MasterCard;
                case global::Starbucks.CashStar.Provider.Common.Models.PaymentType.None :
                    return PaymentType.None;
                case global::Starbucks.CashStar.Provider.Common.Models.PaymentType.PayPal :
                    return PaymentType.PayPal;
                case global::Starbucks.CashStar.Provider.Common.Models.PaymentType.Visa :
                    return PaymentType.Visa;
                default :
                    return PaymentType.None;
            }
        }

        public static PaymentTypeGroup ToPlatform(this global::Starbucks.CashStar.Provider.Common.Models.PaymentTypeGroup paymentTypeGroup)
        {
            switch(paymentTypeGroup)
            {
                case global::Starbucks.CashStar.Provider.Common.Models.PaymentTypeGroup.None :
                    return PaymentTypeGroup.None;
                case global::Starbucks.CashStar.Provider.Common.Models.PaymentTypeGroup.Checking :
                    return PaymentTypeGroup.Checking;
                case global::Starbucks.CashStar.Provider.Common.Models.PaymentTypeGroup.CreditCard :
                    return PaymentTypeGroup.CreditCard;
                case global::Starbucks.CashStar.Provider.Common.Models.PaymentTypeGroup.Internal :
                    return PaymentTypeGroup.Internal;
                case global::Starbucks.CashStar.Provider.Common.Models.PaymentTypeGroup.Lost :
                    return PaymentTypeGroup.Lost;
                case global::Starbucks.CashStar.Provider.Common.Models.PaymentTypeGroup.SvcCard :
                    return PaymentTypeGroup.SvcCard;
                default :
                    return PaymentTypeGroup.None;
            }
        }

        public static Starbucks.Platform.Bom.Shared.UserAddress ToPlatform(this IUserAddress userAddress)
        {
            if (userAddress  == null) return null;
            return new UserAddress
                {
                    AddressId = userAddress.AddressId,
                    AddressLine1 = userAddress.AddressLine1,
                    AddressLine2 = userAddress.AddressLine2,
                    AddressName = userAddress.AddressName,
                    AddressType = userAddress.AddressType.ToPlatform(),
                    City = userAddress.City,
                    CompanyName = userAddress.CompanyName,
                    Country = userAddress.Country,
                    CountryName = userAddress.CountryName,
                    CountrySubdivision = userAddress.CountrySubdivision,
                    CountrySubdivisionDescription = userAddress.CountrySubdivisionDescription,
                    EmailAddress = userAddress.EmailAddress,
                    FirstName = userAddress.FirstName,
                    IsTemporary = userAddress.IsTemporary,
                    LastName = userAddress.LastName,
                    PhoneExtension = userAddress.PhoneExtension,
                    PhoneNumber = userAddress.PhoneNumber,
                    PostalCode = userAddress.PostalCode,
                    RegionDescription = userAddress.RegionDescription,
                    ShippingTotal = userAddress.ShippingTotal,
                    UserId = userAddress.UserId,
                    //VerificationLevel = userAddress.VerificationLevel ,
                    //VerificationSearchResults = userAddress.VerificationSearchResults ,
                    //VerificationStatus = userAddress.VerificationStatus 

                };
        }

        public static Starbucks.Platform.Bom.Shared.UserAddressType ToPlatform(this UserAddressType userAddressType)
        {
            switch (userAddressType)
            {
                case UserAddressType.Billing :
                    return Starbucks.Platform.Bom.Shared.UserAddressType.Billing;
                case UserAddressType.None :
                    return Starbucks.Platform.Bom.Shared.UserAddressType.None;
                case UserAddressType.Registration :
                    return Starbucks.Platform.Bom.Shared.UserAddressType.Registration;
                case UserAddressType.Shipping :
                    return Starbucks.Platform.Bom.Shared.UserAddressType.Shipping;
                default :
                    return Starbucks.Platform.Bom.Shared.UserAddressType.Billing;
            }
        }



        public static order ToPlatform(this IOrder order)
        {
            if (order  == null) return null;
            return new order
                {
                    ActivationCallbackUrl = order.ActivationCallbackUrl ,
                    AuditNumber = order.AuditNumber ,
                    OrderNumber= order.OrderNumber ,
                    OriginationLanguage = order.OriginationLanguage ,
                    Payment = order.Payment.ToPlatform() ,
                    TransactionId = order.TransactionId ,
                    eGiftCards = order.eGiftCards != null ? order.eGiftCards.Select( p=>p.ToPlatform() ).ToList()  :null,
                };
        }

        public static Starbucks.Platform.Bom.Shared.eGiftEntities.Payment ToPlatform(this IPayment payment)
        {
            if (payment == null) return null;
            return new Starbucks.Platform.Bom.Shared.eGiftEntities.Payment
                {
                    Amount = payment.Amount,
                    CreditCard = payment.CreditCard.ToPlatform() ,
                    Currency = payment.Currency,
                    MobileSession = payment.MobileSession.ToPlatform() ,
                    Purchaser = payment.Purchaser.ToPlatform() ,
                    PurchaserAccount = payment.PurchaserAccount.ToPlatform() ,
                    WebSession = payment.WebSession.ToPlatform() ,
                };
        }

        public static Starbucks.Platform.Bom.Shared.eGiftEntities.CreditCard ToPlatform(this ICreditCard creditCard)
        {
            if (creditCard == null) return null;
            return new Starbucks.Platform.Bom.Shared.eGiftEntities.CreditCard
                {
                    AccountNumber = creditCard.AccountNumber,
                    Address = creditCard.Address,
                    Address2 = creditCard.Address2,
                    CardType = creditCard.CardType,
                    City = creditCard.CardType,
                    CountryCode = creditCard.CountryCode,
                    Email = creditCard.Email,
                    ExpirationDate = creditCard.ExpirationDate,
                    FirstName = creditCard.FirstName,
                    LastName = creditCard.LastName,
                    Phone = creditCard.Phone,
                    PostalCode = creditCard.PostalCode,
                    SecurityCode = creditCard.SecurityCode,
                    State = creditCard.State
                };
        }

        public static Starbucks.Platform.Bom.Shared.eGiftEntities.MobileAppSession ToPlatform(this IMobileAppSession mobileAppSession)
        {
            if (mobileAppSession  == null) return null;
            return new Starbucks.Platform.Bom.Shared.eGiftEntities.MobileAppSession
                {
                    DeviceId = mobileAppSession.DeviceId,
                    DeviceModel = mobileAppSession.DeviceModel,
                    DeviceName = mobileAppSession.DeviceName,
                    IpAddress = mobileAppSession.IpAddress,
                    Latitude = mobileAppSession.Latitude,
                    Longitude = mobileAppSession.Longitude,
                    PhoneNumber = mobileAppSession.PhoneNumber,
                    PurchaserSessionDuration = mobileAppSession.PurchaserSessionDuration,
                    PurchaserSessionId = mobileAppSession.PurchaserSessionId,
                    SystemName = mobileAppSession.SystemName,
                    SystemVersion = mobileAppSession.SystemVersion,
                };
        }

        public static Starbucks.Platform.Bom.Shared.eGiftEntities.Purchaser ToPlatform(this IPurchaser purchaser)
        {
            if (purchaser == null) return null;
            return new Starbucks.Platform.Bom.Shared.eGiftEntities.Purchaser
                {
                    Email = purchaser.Email,
                    FirstName = purchaser.FirstName,
                    LastName = purchaser.LastName,
                    Phone = purchaser.Phone,
                };
        }

        public static Starbucks.Platform.Bom.Shared.eGiftEntities.PurchaserAccount ToPlatform(this IPurchaserAccount purchaserAccount)
        {
            if (purchaserAccount == null) return null;
            return new Starbucks.Platform.Bom.Shared.eGiftEntities.PurchaserAccount
                {
                    AccountAge = purchaserAccount.AccountAge,
                    DaysSinceLastPurchase = purchaserAccount.DaysSinceLastPurchase,
                    NumberDisputes = purchaserAccount.NumberDisputes,
                    NumberPurchases = purchaserAccount.NumberPurchases,
                    NumberReturns = purchaserAccount.NumberReturns,
                    UserId = purchaserAccount.UserId,
                };
        }

        public static Starbucks.Platform.Bom.Shared.eGiftEntities.WebSession ToPlatform(this IWebSession webSession)
        {
            if (webSession == null) return null;
            return new Starbucks.Platform.Bom.Shared.eGiftEntities.WebSession
                {
                    HttpHeaders = webSession.HttpHeaders,
                    IovationBlackbox = webSession.IovationBlackbox,
                    JavascriptEnabled = webSession.JavascriptEnabled,
                    PurchaserIP = webSession.PurchaserIp,
                    PurchaserSessionDuration = webSession.PurchaserSessionDuration,
                    PurchaserSessionId = webSession.PurchaserSessionId,
                    ThreatMetrixSessionId = webSession.ThreatMetrixSessionId
                };
        }

        public static eGiftCard ToPlatform(this IEGiftCard eGiftCard)
        {
            if (eGiftCard == null) return null;
            return new Starbucks.Platform.Bom.Shared.eGiftEntities.eGiftCard
                {
                    AccessCode = eGiftCard.AccessCode,
                    Active = eGiftCard.Active,
                    BalanceLastUpdated = eGiftCard.BalanceLastUpdated,
                    AdditionalContent = eGiftCard.AdditionalContent.ToPlatform() ,
                    AuditNumber = eGiftCard.AuditNumber,
                    BalanceUrl = eGiftCard.BalanceUrl,
                    Challenge = eGiftCard.Challenge,
                    Challenge_Description = eGiftCard.Challenge,
                    Challenge_Type = eGiftCard.Challenge_Type,
                    CountryIssued = eGiftCard.CountryIssued,
                    Currency = eGiftCard.Currency,
                    CurrentBalance = eGiftCard.CurrentBalance,
                    Delivery = eGiftCard.Delivery.ToPlatform(),
                    FaceplateCode = eGiftCard.FaceplateCode,
                    InitialBalance = eGiftCard.InitialBalance,
                    MerchantCode = eGiftCard.MerchantCode,
                    Message = eGiftCard.Message.ToPlatform() ,
                    Status = eGiftCard.Status,
                    Url = eGiftCard.Url,
                    eGiftCardCode = eGiftCard.eGiftCardCode,
                    eGiftCardNumber = eGiftCard.eGiftCardNumber,
                };
        }

        public static Starbucks.Platform.Bom.Shared.eGiftEntities.AdditionalContent ToPlatform(this IAdditionalContent additionalContent)
        {
            if (additionalContent == null) return null;
            return new Starbucks.Platform.Bom.Shared.eGiftEntities.AdditionalContent
                {
                    content = additionalContent.Content,
                    type = additionalContent.Type
                };
        }

        public static Starbucks.Platform.Bom.Shared.eGiftEntities.Delivery ToPlatform(this IDelivery delivery)
        {
            if (delivery == null) return null;
            return new Starbucks.Platform.Bom.Shared.eGiftEntities.Delivery
                {
                    Delivered = delivery.Delivered,
                    DeliveredBy = delivery.DeliveredBy,
                    Method = delivery.Method,
                    Received = delivery.Received,
                    Scheduled = delivery.Scheduled,
                    Status = delivery.Status,
                    Target = delivery.Target
                };
        }

        public static Starbucks.Platform.Bom.Shared.eGiftEntities.Message ToPlatform(this IMessage message)
        {
            if (message == null) return null;
            return new Starbucks.Platform.Bom.Shared.eGiftEntities.Message
                {
                    Body = message.Body,
                    From = message.From,
                    To = message.To
                };
        }



        public static IOrder ToApi(this order order)
        {
            if (order == null) return null;
            return new Order
                {
                    ActivationCallbackUrl = order.ActivationCallbackUrl ,
                    AuditNumber = order.AuditNumber ,
                    eGiftCards = order.eGiftCards.Select( p=> p.ToApi())  ,
                    OrderNumber = order.OrderNumber ,
                    OriginationLanguage = order.OriginationLanguage ,
                    Payment = order.Payment.ToApi()  ,
                    TransactionId = order.TransactionId ,
                };
        }

        public static IPayment ToApi(this Starbucks.Platform.Bom.Shared.eGiftEntities.Payment payment)
        {
            if (payment == null) return null;
            return new Payment
                {
                    Amount = payment.Amount,
                    CreditCard = payment.CreditCard.ToApi() ,
                    Currency = payment.Currency,
                    MobileSession = payment.MobileSession.ToApi() ,
                    Purchaser = payment.Purchaser.ToApi() ,
                    PurchaserAccount = payment.PurchaserAccount.ToApi() ,
                    WebSession = payment.WebSession.ToApi(),
                };
        }


        public static ICreditCard ToApi(this Starbucks.Platform.Bom.Shared.eGiftEntities.CreditCard creditCard)
        {
            if (creditCard == null) return null;
            return new CreditCard
                {
                    AccountNumber = creditCard.AccountNumber,
                    Address = creditCard.Address,
                    Address2 = creditCard.Address2,
                    CardType = creditCard.CardType,
                    City = creditCard.City,
                    CountryCode = creditCard.CountryCode,
                    Email = creditCard.Email,
                    ExpirationDate = creditCard.ExpirationDate,
                    FirstName = creditCard.FirstName,
                    LastName = creditCard.LastName,
                    Phone = creditCard.Phone,
                    PostalCode = creditCard.PostalCode,
                    SecurityCode = creditCard.SecurityCode,
                    State = creditCard.State,
                };
        }

        public static IMobileAppSession ToApi(this MobileAppSession mobileAppSession)
        {
            if (mobileAppSession == null) return null;
            return new global::Starbucks.CashStar.Provider.Models.MobilAppSession
                {
                    DeviceId = mobileAppSession.DeviceId,
                    DeviceModel = mobileAppSession.DeviceModel,
                    DeviceName = mobileAppSession.DeviceName,
                    IpAddress = mobileAppSession.IpAddress,
                    Latitude = mobileAppSession.Latitude,
                    Longitude = mobileAppSession.Longitude,
                    PhoneNumber = mobileAppSession.PhoneNumber,
                    PurchaserSessionDuration = mobileAppSession.PurchaserSessionDuration,
                    PurchaserSessionId = mobileAppSession.PurchaserSessionId,
                    SystemName = mobileAppSession.SystemName,
                    SystemVersion = mobileAppSession.SystemVersion
                };
        }

        public static IPurchaser ToApi(this  Starbucks.Platform.Bom.Shared.eGiftEntities.Purchaser purchaser)
        {
            if (purchaser == null) return null;
            return new Purchaser
                {
                    Email = purchaser.Email,
                    FirstName = purchaser.FirstName,
                    LastName = purchaser.LastName,
                    Phone = purchaser.Phone
                };
        }

        public static IPurchaserAccount ToApi(this PurchaserAccount purchaserAccount)
        {
            if (purchaserAccount == null) return null;
            return new PurchaseAccount
                {
                    AccountAge = purchaserAccount.AccountAge,
                    DaysSinceLastPurchase = purchaserAccount.DaysSinceLastPurchase,
                    NumberDisputes = purchaserAccount.NumberDisputes,
                    NumberPurchases = purchaserAccount.NumberPurchases,
                    NumberReturns = purchaserAccount.NumberReturns,
                    UserId = purchaserAccount.UserId
                };
        }

        public static IWebSession ToApi(this Starbucks.Platform.Bom.Shared.eGiftEntities.WebSession webSession)
        {
            if (webSession == null) return null;
            return new global::Starbucks.CashStar.Provider.Models.WebSession
                {
                    HttpHeaders = webSession.HttpHeaders ,
                    IovationBlackbox = webSession.IovationBlackbox,
                    JavascriptEnabled = webSession.JavascriptEnabled,
                    PurchaserIp = webSession.PurchaserIP  ,
                    PurchaserSessionDuration=webSession.PurchaserSessionDuration ,
                    PurchaserSessionId = webSession.PurchaserSessionId ,
                    ThreatMetrixSessionId = webSession.ThreatMetrixSessionId ,
                };
        }

        public static IEGiftCard ToApi(this Starbucks.Platform.Bom.Shared.eGiftEntities.eGiftCard eGiftCard)
        {
            if (eGiftCard == null) return null;
            return new EGiftCard
                {
                    AccessCode = eGiftCard.AccessCode ,
                    Active = eGiftCard.Active ,
                    AdditionalContent = eGiftCard.AdditionalContent.ToApi(),
                    AuditNumber = eGiftCard.AuditNumber ,
                    BalanceLastUpdated = eGiftCard.BalanceLastUpdated ,
                    BalanceUrl =eGiftCard.BalanceUrl ,
                    Challenge = eGiftCard.Challenge ,
                    Challenge_Description = eGiftCard.Challenge_Description ,
                    Challenge_Type = eGiftCard.Challenge_Type,
                    CountryIssued = eGiftCard.CountryIssued , 
                    Currency = eGiftCard.Currency ,
                    CurrentBalance = eGiftCard.CurrentBalance ,
                    Delivery = eGiftCard.Delivery.ToApi() ,
                    FaceplateCode = eGiftCard.FaceplateCode , 
                    InitialBalance = eGiftCard.InitialBalance ,
                    MerchantCode = eGiftCard.MerchantCode ,
                    Message = eGiftCard.Message.ToApi(),
                    Status  = eGiftCard.Status ,
                    Url = eGiftCard.Url ,
                    eGiftCardCode = eGiftCard.eGiftCardCode ,
                    eGiftCardNumber = eGiftCard.eGiftCardNumber ,
                };
        }

        public static IAdditionalContent ToApi(
            this Starbucks.Platform.Bom.Shared.eGiftEntities.AdditionalContent additionalContent)
        {
            if (additionalContent == null) return null;
            return new AdditionalContent
                {
                    Content = additionalContent.content,
                    Type = additionalContent.type,
                };
        }

        public static IDelivery ToApi(this Starbucks.Platform.Bom.Shared.eGiftEntities.Delivery delivery)
        {
            if (delivery == null) return null;
            return new Delivery
                {
                    Delivered = delivery.Delivered,
                    DeliveredBy = delivery.DeliveredBy,
                    Method = delivery.Method,
                    Received = delivery.Received,
                    Scheduled = delivery.Scheduled,
                    Status = delivery.Status,
                    Target = delivery.Target,
                };
        }

        public static IMessage ToApi(this Starbucks.Platform.Bom.Shared.eGiftEntities.Message message)
        {
            if (message == null) return null;
            return new Message
                {
                    Body = message.Body,
                    From = message.From,
                    To = message.To,
                };
        }

        public static IMerchant ToApi(this Starbucks.Platform.Bom.Shared.eGiftEntities.Merchant  merchant )
        {
            if (merchant == null) return null;
            return new Merchant
                {
                    Description = merchant.Description,
                    LegalName = merchant.LegalName,
                    Locations = merchant.Locations,
                    LogoImage = merchant.LogoImage,
                    MerchantCode = merchant.MerchantCode,
                    Name = merchant.Name,
                };
        }

        public static IFaceplate ToApi(this Starbucks.Platform.Bom.Shared.eGiftEntities.Faceplate  faceplate)
        {
            if (faceplate == null) return null;
            return new Faceplate
            {
                FaceplateCode = faceplate.FaceplateCode ,
                Name = faceplate.Name ,
                PreviewImage = faceplate.PreviewImage ,
                PrintImage = faceplate.PrintImage ,
                TextColor = faceplate.TextColor ,
                ThumbnailImage = faceplate.ThumbnailImage ,                
            };
        }

    }
}
