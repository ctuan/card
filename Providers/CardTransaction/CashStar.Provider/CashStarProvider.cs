﻿using System;
using System.Collections.Generic;
using System.Linq;
using Starbucks.CashStar.Provider.Common;
using Starbucks.CashStar.Provider.Common.Models;
using Starbucks.CashStar.Provider.Configuration;
using Starbucks.CashStar.Provider.Models;
using Starbucks.ServiceProxies.CashStar.Wcf;
using Starbucks.ServiceProxies.Extensions;

namespace Starbucks.CashStar.Provider
{
    public class CashStarProvider : ICashStarProvider
    {
        public IOrder CreateOrder(IBillingInfo billingInfo, IOrder order)
        {
            var result = CashStarService.GetChannelFactory(
                CashStarProviderSetttings.Settings.CashstarServiceConfigurationName)
                .Use(c => c.CreateOrder(billingInfo.ToPlatform(), order.ToPlatform()));

            return result != null ? result.ToApi() : null;
        }

        public IOrder CreateOrderForAccount(IBillingInfo billingInfo, IOrder order, string accountName)
        {
            try
            {
                var result = CashStarService.GetChannelFactory(
                    CashStarProviderSetttings.Settings.CashstarServiceConfigurationName)
                                            .Use(c => c.CreateOrderForAccount(billingInfo.ToPlatform(), order.ToPlatform(),
                                                                        accountName));
                return result != null ? result.ToApi() : null;
            }
            catch (Exception)
            {               
                throw;
            }
        }

        public IEnumerable<IMerchant> GetMerchants()
        {
            var result =
                CashStarService.GetChannelFactory(
                    CashStarProviderSetttings.Settings.CashstarServiceConfigurationName)
                               .Use(c => c.GetMerchants());

            return result != null ? result.Select( p=>p.ToApi() ) : null;
        }

        public IEnumerable<IFaceplate> GetFaceplates(string merchantCode)
        {
            var result = CashStarService.GetChannelFactory(
                CashStarProviderSetttings.Settings.CashstarServiceConfigurationName)
                                        .Use(c => c.GetFaceplates(merchantCode));
            return result != null ? result.Select(p => p.ToApi()) : null;
        }

        public IOrder GetOrderByStarbucksOrderId(string orderId)
        {
            var result = CashStarService.GetChannelFactory(
                CashStarProviderSetttings.Settings.CashstarServiceConfigurationName)
                                        .Use(c => c.GetOrderByStarbucksOrderId(orderId));
            return result != null ? result.ToApi() : null;
        }
    }
}
