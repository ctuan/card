﻿using System.Configuration;

namespace Starbucks.CashStar.Provider.Configuration
{
    public class CashStarProviderSetttings : ConfigurationSection
    {
        private static CashStarProviderSetttings _settings;
        public static CashStarProviderSetttings Settings
        {
            get
            {
                return _settings ??
                       (_settings =
                        ConfigurationManager.GetSection("cashStarProviderSetttings") as CashStarProviderSetttings);
            }
        }

        [ConfigurationProperty("cashstarServiceConfigurationName", IsRequired = true)]
        public string CashstarServiceConfigurationName
        {
            get { return (string)this["cashstarServiceConfigurationName"]; }
            set { this["cashstarServiceConfigurationName"] = value; }
        }
        
    }
}
