﻿using System.Configuration;
using Starbucks.ServiceProxies.PaymentService.Wcf.Configuration;

namespace Starbucks.SecurePayment.Provider.Configuration
{
    public class PaymentProviderSetttings : ConfigurationSection
    {
        private static PaymentProviderSetttings _settings;
        public static PaymentProviderSetttings Settings
        {
            get
            {
                return _settings ??
                       (_settings =
                        ConfigurationManager.GetSection("paymentProviderSettings") as PaymentProviderSetttings);
            }
        }
        [ConfigurationProperty("paymentServiceSettings", IsRequired = true)]
        public PaymentServiceElement PaymentServiceSettings
        {
            get
            {
                return this["paymentServiceSettings"] as PaymentServiceElement;
            }
            set
            {
                this["paymentServiceSettings"] = value;
            }
        }
    }
}
