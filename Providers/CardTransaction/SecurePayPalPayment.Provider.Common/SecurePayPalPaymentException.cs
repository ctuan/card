﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;

namespace Starbucks.SecurePayPalPayment.Provider.Common
{
    public class SecurePayPalPaymentException : Exception 
    {
        public string Code { get; private set; }
        public SecurePayPalPaymentException(string code, string message) : this(code, message, null)
        {         
        }

        public SecurePayPalPaymentException(string code, string message, Exception exception) : base(message, exception )
        {
            Code = code;
        }       
    }
}
