namespace Starbucks.SecurePayPalPayment.Provider.Common.Models
{
    public interface IBillingAgreementUdpateResponse
    {
        IAddress BillingAddress { get; set; }

        string Description { get; set; }

        string Custom { get; set; }

        string BillingAgreementId { get; set; }

        string Currency { get; set; }

        decimal? MaxAmount { get; set; }

        IPayerInformation PayerInformation { get; set; }
    }
}