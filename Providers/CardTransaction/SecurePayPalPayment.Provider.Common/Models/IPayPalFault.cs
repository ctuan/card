﻿using System.Collections.Generic;

namespace Starbucks.SecurePayPalPayment.Provider.Common.Models
{
    public interface IPayPalFault
    {
        object extensionData { get; set; }

        IEnumerable<IPayPalError> Errors { get; set; } 
    }
}