﻿namespace Starbucks.SecurePayPalPayment.Provider.Common.Models
{
    public interface IAuthorizationRequest
    {
        string OrderId { get; set; }

        IPaymentDetails PaymentDetails { get; set; }

        string Token { get; set; }
    }
}