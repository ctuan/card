﻿namespace Starbucks.SecurePayPalPayment.Provider.Common.Models
{
    public interface IVoidRequest
    {
        string AuthorizationId { get; set; }

        string NoteField { get; set; }

        string Subject { get; set; }
    }
}