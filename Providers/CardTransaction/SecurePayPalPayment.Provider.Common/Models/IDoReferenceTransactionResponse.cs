﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.SecurePayPalPayment.Provider.Common.Models
{
    public interface IDoReferenceTransactionResponse : IPayPalResponse 
    {
        string BillingAgreementId { get; set; }

        ITransactionDetails TransactionDetails { get; set; }
    }
}
