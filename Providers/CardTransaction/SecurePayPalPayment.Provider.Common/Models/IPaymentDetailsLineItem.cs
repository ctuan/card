﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.SecurePayPalPayment.Provider.Common.Models
{
    public interface IPaymentDetailsLineItem
    {
        decimal ItemAmount { get; set; }

        string ItemCode { get; set; }

        string ItemDescription { get; set; }

        string ItemName { get; set; }

        int Quantity { get; set; }
    }
}
