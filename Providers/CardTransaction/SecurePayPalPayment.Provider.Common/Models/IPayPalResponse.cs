﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.SecurePayPalPayment.Provider.Common.Models
{
    public interface  IPayPalResponse
    {
        IEnumerable< IPayPalError> Errors { get; set; }
        object ExtensionData { get; set; }
        PayPalAckCode PayPalAckCode { get; set; }
    }
}
