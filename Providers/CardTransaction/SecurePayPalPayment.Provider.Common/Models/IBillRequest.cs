﻿namespace Starbucks.SecurePayPalPayment.Provider.Common.Models
{
    public interface IBillRequest
    {
        string AuthorizationId { get; set; }

        string Note { get; set; }

        string OrderId { get; set; }

        string Subject { get; set; }
    }
}