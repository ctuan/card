﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.SecurePayPalPayment.Provider.Common.Models
{
    public enum PaymentStatusCode
    {
        None = 0,
        Completed = 1,
        Failed = 2,
        Pending = 3,
        Denied = 4,
        Refunded = 5,
        Reversed = 6,
        CanceledReversal = 7,
        Processed = 8,
        PartiallyRefunded = 9,
        Voided = 10,
        Expired = 11,
        InProgress = 12,
        Created = 13,
    }

    public enum PendingStatusCode
    {
        None = 0,
        Echeck = 1,
        Intl = 2,
        Verify = 3,
        Address = 4,
        Unilateral = 5,
        Other = 6,
        Upgrade = 7,
        MultiCurrency = 8,
        Authorization = 9,
        Order = 10,
        PaymentReview = 11,
    }

    public enum PaymentActionCode
    {        
        None = 0,     
        Authorization = 1,        
        Sale = 2,        
        Order = 3,
    }

    public enum PayPalAckCode
    {
        Success = 0,
        Failure = 1,
        Warning = 2,
        SuccessWithWarning = 3,
        FailureWithWarning = 4,
        PartialSuccess = 5,
        CustomCode = 6,
    }

    public enum PayPalErrorSeverityCode
    {      
        Warning = 0,      
        Error = 1,      
        PartialSuccess = 2,      
        CustomCode = 3,
    }
}
