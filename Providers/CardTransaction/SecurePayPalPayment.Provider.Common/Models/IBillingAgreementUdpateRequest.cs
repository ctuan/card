﻿namespace Starbucks.SecurePayPalPayment.Provider.Common.Models
{
    public interface IBillingAgreementUdpateRequest
    {
       string ReferenceId { get; set; }

        string Currency { get; set; }

        string Custom { get; set; }

        string Description { get; set; }
    
        //There is an enum for status but for now always pass active to not risk cancelling a BA
    }
}