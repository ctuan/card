﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.SecurePayPalPayment.Provider.Common.Models
{
    public interface IAddress
    {
        string AddressId { get; set; }

        string City { get; set; }

        string Country { get; set; }

        string ExternalAddressId { get; set; }

        string Name { get; set; }

        string Phone { get; set; }

        string PostalCode { get; set; }

        string StateOrProvince { get; set; }

        string Street1 { get; set; }

        string Street2 { get; set; }
    }
}
