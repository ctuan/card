﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.SecurePayPalPayment.Provider.Common.Models
{
    public interface ITransactionDetails : IPayPalResponse 
    {
        IAddress BillingAddress { get; set; }

        string OrderId { get; set; }

        IPayerInformation PayerInformation { get; set; }

        IPaymentDetails PaymentDetails { get; set; }

        PaymentStatusCode PaymentStatusCode { get; set; }

        PendingStatusCode PendingStatusCode { get; set; }

        string Subject { get; set; }

        string TransactionId { get; set; }

        string ParentTransactionId { get; set; }

    }
}
