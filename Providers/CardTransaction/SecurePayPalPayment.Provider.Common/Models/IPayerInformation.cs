﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.SecurePayPalPayment.Provider.Common.Models
{
    public interface IPayerInformation
    {
        IAddress Address { get; set; }

        string FirstName { get; set; }

        string LastName { get; set; }

        string Payer { get; set; }

        string PayerId { get; set; }
    }
}
