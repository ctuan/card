﻿namespace Starbucks.SecurePayPalPayment.Provider.Common.Models
{
    public interface IAuthorizationResponse
    {
        IPayerInformation PayerInformation { get; set; }

        PaymentStatusCode PaymentStatusCode { get; set; }

        PendingStatusCode PendingStatusCode { get; set; }

        string Subject { get; set; }

        string TransactionId { get; set; }
    }
}