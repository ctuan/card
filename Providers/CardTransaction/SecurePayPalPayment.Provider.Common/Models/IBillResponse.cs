﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.SecurePayPalPayment.Provider.Common.Models
{
    public interface  IBillResponse: IPayPalResponse 
    {
        PaymentStatusCode PaymentStatusCode { get; set; }
        PendingStatusCode PendingStatusCode { get; set; }
        string TransactionId { get; set; }      
    }
}
