﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.SecurePayPalPayment.Provider.Common.Models
{
    public interface IDoReferenceTransactionRequest : IPayPalRequest 
    {
        PaymentActionCode PaymentAction { get; set; }

        IPaymentDetails PaymentDetails { get; set; }

        string ReferenceId { get; set; }

        bool RequireShippingConfirmation { get; set; }

        string SoftDescriptor { get; set; }

        string IPAddress { get; set; }

        string UserId { get; set; }

        string Locale { get; set; }
    }
}
