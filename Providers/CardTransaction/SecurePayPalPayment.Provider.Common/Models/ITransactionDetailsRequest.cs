﻿namespace Starbucks.SecurePayPalPayment.Provider.Common.Models
{
    public interface ITransactionDetailsRequest
    {
        string TransactionId { get; set; }

        string Currency { get; set; }
    }
}