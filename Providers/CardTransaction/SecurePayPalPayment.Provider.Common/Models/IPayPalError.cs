﻿using System.Collections.Specialized;

namespace Starbucks.SecurePayPalPayment.Provider.Common.Models
{
    public interface IPayPalError
    {
        string ErrorCode { get; set; }
        StringDictionary ErrorParameters { get; set; }
        bool IndicatesBuyerAcccountProblem { get; set; }
        string LongMessage { get; set; }
        PayPalErrorSeverityCode SeverityCode { get; set; }
        string ShortMessage { get; set; }
    }
}
