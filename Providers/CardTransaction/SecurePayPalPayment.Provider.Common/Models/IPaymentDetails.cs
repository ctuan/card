﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.SecurePayPalPayment.Provider.Common.Models
{
    public interface IPaymentDetails
    {
        string Currency { get; set; }
        decimal Discount { get; set; }
        IEnumerable <IPaymentDetailsLineItem> LineItems { get; set; }
        decimal MaxAmount { get; set; }
        decimal OrderAmount { get; set; }
        string OrderDescription { get; set; }
        System.DateTime? PaymentDate { get; set; }
        decimal ShippingAmount { get; set; }
        decimal TaxAmount { get; set; }
        decimal OrderSubTotal { get; }
        decimal OrderTotal { get; }
    }
}
