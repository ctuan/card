﻿using Starbucks.SecurePayPalPayment.Provider.Common.Models;

namespace Starbucks.SecurePayPalPayment.Provider.Common
{
    public interface ISecurePayPalPaymentClient
    {
        ISetupResponse Setup(ISetupRequest setupRequest);

        void Void(IVoidRequest voidRequest);

        IAuthorizationResponse Auth(IAuthorizationRequest authorizationRequest);

        IBillResponse Bill(IBillRequest billRequest);

        IRefundTransactionDetails RefundTransactionInFull(IRefundRequest refundRequest);

        IRefundTransactionDetails RefundTransactionPartialAmount(IPartialRefundRequest partialRefundRequest);

        ITransactionDetails GetTransactionDetails(ITransactionDetailsRequest transactionDetailsRequest);

        ITransactionDetails GetExpressCheckoutDetails(IExpressCheckoutDetailsRequest expressCheckoutDetailsRequest);

        ITransactionSearchResults SearchTransactions(ITransactionSearchRequest transactionSearchRequest);

        string GetOrderIdByPayPalTransactionId(string transactionId);

        ISetCustomerBillingAgreementResponse SetCustomerBillingAgreement(ISetCustomerBillingAgreementRequest setCustomerBillingAgreementRequest);

        IGetBillingAgreementCustomerDetailsResponse GetBillingAgreementCustomerDetails(IGetBillingAgreementCustomerDetailsRequest getBillingAgreementCustomerDetailsRequest);

        IBillingAgreementUdpateResponse UpdateBillingAgreement(IBillingAgreementUdpateRequest request);

        ICreateBillingAgreementResponse CreateBillingAgreement(ICreateBillingAgreementRequest createBillingAgreementRequest);

        IDoReferenceTransactionResponse DoReferenceTransaction(IDoReferenceTransactionRequest doReferenceTransactionRequest);

        string GetPayPalSubject(string currency);

    }
}
