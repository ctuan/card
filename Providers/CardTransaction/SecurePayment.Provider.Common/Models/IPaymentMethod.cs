﻿using System;

namespace Starbucks.SecurePayment.Provider.Common.Models
{
    public interface IPaymentMethod
    {
        string PaymentMethodId { get; set; }
        string PaymentType { get; set; }

        string FullName { get; set; }
        string BillingAddressId { get; set; }

        // new payment method
        string AccountNumber { get; set; }
        int? ExpirationMonth { get; set; }
        int? ExpirationYear { get; set; }
        string Cvn { get; set; }

        bool IsDefault { get; set; }
        string Nickname { get; set; }
        string AccountNumberLastFour { get; set; }

        bool IsTemporary { get; set; }
        // todo: add paypal related fields.

        string RoutingNumber { get; set; }
        string BankName { get; set; }

        string UserId { get; set; }
        bool IsFraudChecked { get; set; }
        string SurrogateAccountNumber { get; set; }
        int PaymentTypeId { get; set; }
        DateTime? FraudCheckedDate { get; set; }
    }
}
