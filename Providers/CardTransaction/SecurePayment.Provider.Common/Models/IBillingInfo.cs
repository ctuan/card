﻿using System;

namespace Starbucks.SecurePayment.Provider.Common.Models
{
    public interface IBillingInfo
    {
        /// <summary>
        /// The billing address
        /// </summary>
        IUserAddress Address { get; set; }

        /// <summary>
        /// The Payment information
        /// </summary>
        IPaymentMethod PaymentMethod { get; set; }


        /// <summary>
        /// Authorization code for charges to this account.
        /// </summary>
        string AuthCode { get; set; }

        /// <summary>
        /// Date the account was authorized.
        /// </summary>
        DateTime AuthDate { get; set; }

        /// <summary>
        /// Request ID of the authorization call.
        /// </summary>
        string AuthRequestId { get; set; }

        /// <summary>
        /// The e-mail address for the billing record
        /// </summary>
        string EmailAddress { get; set; }

        /// <summary>
        /// ISO currency alpha code
        /// </summary>
        string Currency { get; set; }

        /// <summary>
        /// ISO numeric currency code
        /// </summary>
        string CurrencyNumericCode { get; set; }

        string County { get; set; }
        string PayPalTransactionId { get; set; }
        string PayPalExpressCheckoutToken { get; set; }
        string PayPalPayerId { get; set; }
        string PayPalEmailAddress { get; set; }
        string PayPalSubject { get; set; }
        bool IsPayPalBillFailed { get; set; }
       
    }
}
