﻿using System.Collections.Generic;

namespace Starbucks.SecurePayment.Provider.Common.Models
{
    public enum UserAddressType
    {
        None = 0,
        Shipping = 1,
        Billing = 2,
        Registration = 3
    }

    public enum UserAddressVerificationStatus
    {

        ///<summary>
        /// Status is not verified
        ///</summary>
        NotVerified = 0,

        ///<summary>
        /// Status is verified
        ///</summary>
        Verified = 1,

        /// <summary>
        /// Overridden by user
        /// </summary>
        Override = 2,

        /// <summary>
        /// AV service retunrned an error
        /// </summary>
        Error = 3
    }

    public enum UserAddressVerificationLevel
    {
        // No verified matches found, or not application
        None = 0,
        // High confidence match found (address returned)
        Verified = 1,
        // Single match found, but user confirmation is recommended (address returned)
        InteractionRequired = 2,
        // Address was verified to premises level only (picklist returned)
        PremisesPartial = 3,
        // Address was verified to street level only (picklist returned)
        StreetPartial = 4,
        // Address was verified to multiple addresses (picklist returned)
        Multiple = 5
    }

    public interface IUserAddress
    {
        UserAddressType AddressType { get; set; }
        string AddressId { get; set; }
        string UserId { get; set; }
        string AddressName { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string PhoneNumber { get; set; }
        string PhoneExtension { get; set; }
        string RegionDescription { get; set; }
        bool IsTemporary { get; set; }
        UserAddressVerificationStatus VerificationStatus { get; set; }
        UserAddressVerificationLevel VerificationLevel { get; set; }
        List<IUserAddress> VerificationSearchResults { get; set; }
        string EmailAddress { get; set; }
        decimal ShippingTotal { get; set; }
        string CompanyName { get; set; }
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }        
        string City { get; set; }        
        string CountrySubdivision { get; set; }        
        string CountrySubdivisionDescription { get; set; }        
        string PostalCode { get; set; }        
        string Country { get; set; }
        string CountryName { get; set; }
    }
}
