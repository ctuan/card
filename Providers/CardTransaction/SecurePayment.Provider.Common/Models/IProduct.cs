﻿namespace Starbucks.SecurePayment.Provider.Common.Models
{
    public interface IProduct
    {
        string ProductDescription { get; set; }

        string ProductID { get; set; }

        string ProductVariantID { get; set; }

        string JDAItemNumber { get; set; }

        decimal ItemAmount { get; set; }
    
        string ProductCode { get; set; }

        string ProductName { get; set; }

        string SKU { get; set; }

        string GiftCategory { get; set; }

        string TimeCategory { get; set; }

        string HostHedge { get; set; }

        string TimeHedge { get; set; }

        string VelocityHedge { get; set; }

        string Threshold { get; set; }

        int ProductTypeId { get; set; }

        string Locale { get; set; }

        string Site { get; set; }
    }
}
