﻿namespace Starbucks.SecurePayment.Provider.Common.Models
{
    public interface IShipping
    {
        IUserAddress Address { get; set; }
    }
}
