﻿using System;

namespace Starbucks.SecurePayment.Provider.Common.Models
{
    public enum DeliveryType
    {
        None = 0,
        Email = 1,
        Wall = 2,
    }

    public enum DeliveryStatus
    {
        None = 0,
        EmailDelivered = 1,
        WallDelivered = 2,
        FailureDelivered = 4
    }

    public interface  IEGiftNotificationLineItem
    {
        string OrderNumber { get; set; }
        string EGCCode { get; set; }
        string EGiftId { get; set; }
        string ImageFileName { get; set; }
        string OrderId { get; set; }
        string SvcNumber { get; set; }
        string PIN { get; set; }
        decimal Amount { get; set; }
        string Currency { get; set; }
        string CardStatus { get; set; }
        string Challenge { get; set; }
        string ChallengeDescription { get; set; }
        decimal ItemTotal { get; set; }
        DeliveryType DeliveryType { get; set; }
        DeliveryStatus DeliveryStatus { get; set; }
        string CashstarDeliveryMethod { get; set; }
        DateTime Scheduled { get; set; }
        string ScheduledIso { get; set; }
        DateTime? Delivered { get; set; }
        DateTime? Received { get; set; }
        string CashstarDeliveryStatus { get; set; }
        string SenderName { get; set; }
        string SenderEmail { get; set; }
        string SenderFacebookId { get; set; }
        string RecipientName { get; set; }
        string RecipientEmail { get; set; }
        string RecipientFacebookId { get; set; }
        string RecipientMessage { get; set; }
        string ProductId { get; set; }
        string FacebookToken { get; set; }
        string ReloadCardId { get; set; }
        string ReloadCardNumber { get; set; }
        string ReloadTransactionId { get; set; }
        string RefundTransactionId { get; set; }
    }
}
