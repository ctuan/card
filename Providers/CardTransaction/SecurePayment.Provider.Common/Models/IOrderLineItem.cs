﻿namespace Starbucks.SecurePayment.Provider.Common.Models
{
    public interface IOrderLineItem
    {
        IProduct ProductOrdered { get; set; }
        int Quantity { get; set; }
        string OrderType { get; set; }
        decimal TotalAmount { get; }        
        decimal Subtotal { get; }        
        decimal LineItemTotal { get; }        
        decimal Tax { get; set; }
        decimal CityTax { get; set; }
        decimal StateTax { get; set; }
        decimal DistrictTax { get; set; }
        decimal CountyTax { get; set; }

        decimal ShippingCost { get; set; }
        IShipping Shipping { get; set; }
        string CustomCardImageId { get; set; }

        string CustomCardTheme { get; set; }
        decimal UnitPrice { get; set; }

        IEGiftNotificationLineItem EGift { get; set; }

        string[] GiftMessages { get; set; }

        string ParentLineItemId { get; set; }

        string MarketingMessage { get; set; }
    }
}
