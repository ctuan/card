﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.SecurePayment.Provider.Common
{
    public class SecurePaymentException : Exception
    {
        public string Code { get; private set; }

        public SecurePaymentException(string code, string message, Exception innerException)
            : base(message, innerException)
        {
            Code = code;

        }

        public SecurePaymentException(string code, string message) : this (code, message, null)
        {
            
        }
    }
}
