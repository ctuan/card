﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cart.Provider.Models;
using Starbucks.Cart.Provider.Common;
using Starbucks.Cart.Provider.Common.Models;
using Starbucks.Cart.Provider.Configuration;
using Starbucks.ServiceProxies.CartService.Wcf;
using Starbucks.ServiceProxies.Extensions;

namespace Starbucks.Cart.Provider
{
    public class CartProvider: ICartProvider
    {
        public ICart GetCart(Guid token)
        {
            try
            {
                var result = CartService.GetChannelFactory(CartProviderSettings.Settings.CartServiceElement.ServiceConfigurationName).Use(c => c.GetCart(token));
                return result != null ? result.ToApi () : null;
            }
            catch 
            {
                throw;
            }            
        }

        public string GetCartCurrency(Guid token)
        {
            try
            {
                var result = CartService.GetChannelFactory(CartProviderSettings.Settings.CartServiceElement.ServiceConfigurationName).Use(c => c.GetCartCurrency(token));
                return result ;
            }
            catch
            {
                throw;
            }            
        }

        public decimal GetCartTotal(Guid token)
        {
            try
            {
                var result = CartService.GetChannelFactory(CartProviderSettings.Settings.CartServiceElement.ServiceConfigurationName).Use(c => c.GetCartTotal(token));
                return result;
            }
            catch
            {
                throw;
            }          
        }

        public ICart SaveCart(Guid token, IEnumerable<ICartItem> items)
        {
            try
            {
                var result = CartService.GetChannelFactory(CartProviderSettings.Settings.CartServiceElement.ServiceConfigurationName).Use(c => c.SaveCart(token, items.Select( p=>p.ToPlatform() )));
                return result != null ? result.ToApi() : null;
            }
            catch
            {
                throw;
            }          
        }

        public ICart SaveCartWithDiscounts(Guid token, IEnumerable<ICartItem> items, bool applyDiscount)
        {
            try
            {
                var result = CartService.GetChannelFactory(CartProviderSettings.Settings.CartServiceElement.ServiceConfigurationName).Use(c => c.SaveCartWithDiscounts(token, items.Select(p => p.ToPlatform()), applyDiscount ));
                return result != null ? result.ToApi() : null;
            }
            catch
            {
                throw;
            }          
        }

        public void SaveRawCart(Guid token, IEnumerable<ICartItem> items)
        {
            try
            {
                CartService.GetChannelFactory(CartProviderSettings.Settings.CartServiceElement.ServiceConfigurationName).Use(c => c.SaveRawCart(token, items.Select(p => p.ToPlatform())));                
            }
            catch
            {
                throw;
            }        
        }
    }
}
