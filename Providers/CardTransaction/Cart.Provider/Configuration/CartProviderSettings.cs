﻿using System.Configuration;
using Starbucks.ServiceProxies.CartService.Wcf.Configuration;

namespace Starbucks.Cart.Provider.Configuration
{
    public class CartProviderSettings : ConfigurationSection
    {
        private static CartProviderSettings _settings;
        public static CartProviderSettings Settings
        {
            get
            {
                return _settings ??
                       (_settings =
                        ConfigurationManager.GetSection("cartProviderSettings") as CartProviderSettings);
            }
        }

        [ConfigurationProperty("cartService", IsRequired = true)]
        public CartServiceElement  CartServiceElement 
        {
            get { return (CartServiceElement)this["cartService"]; }
            set { this["cartService"] = value; }
        }
    }
}
