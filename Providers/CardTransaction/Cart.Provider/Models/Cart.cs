﻿using System;
using System.Collections.Generic;
using Starbucks.Cart.Provider.Common.Models;

namespace Starbucks.Cart.Provider.Models
{
    public class Cart : ICart
    {
        public int CartId { get; set; }
        public Guid Token { get; set; }
        public DateTime DateCreated { get; set; }
        public IEnumerable<ICartItem> Items { get; set; }
        public decimal PartnerDiscount { get; set; }
        public CartTypes CartType { get; set; }
    }
}
