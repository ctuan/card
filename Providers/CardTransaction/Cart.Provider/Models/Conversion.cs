﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.Cart.Provider.Common.Models;
using Starbucks.Services.Commerce.DataContracts;
using CartItem = Starbucks.Cart.Provider.Models.CartItem;
using CartTypes = Starbucks.Cart.Provider.Common.Models.CartTypes;

namespace Cart.Provider.Models
{
    internal static class Conversion
    {
        internal static ICart ToApi(this Starbucks.Services.Commerce.DataContracts.Cart cart)
        {
            if (cart == null) return null;

            return new Starbucks.Cart.Provider.Models.Cart
                {
                    CartId = cart.CartId,
                    CartType = cart.CartType.ToApi(),
                    DateCreated = cart.DateCreated,
                    Items = cart.Items != null ? cart.Items.Select(p => p.ToApi()) : null,
                    PartnerDiscount = cart.PartnerDiscount,
                    Token = cart.Token
                };
        }

        internal static CartTypes ToApi(this Starbucks.Services.Commerce.DataContracts.CartTypes cartType)
        {
            switch (cartType)
            {
                case Starbucks.Services.Commerce.DataContracts.CartTypes.BAC :
                    return CartTypes.BAC;
                case Starbucks.Services.Commerce.DataContracts.CartTypes.Custom :
                    return CartTypes.Custom;
                case Starbucks.Services.Commerce.DataContracts.CartTypes.eCard :
                    return CartTypes.eCard;
                case Starbucks.Services.Commerce.DataContracts.CartTypes.ePromoCard :
                    return CartTypes.ePromoCard;
                default :
                    throw new ArgumentOutOfRangeException( "cartType");
            }
        }

        internal static ICartItem ToApi(this Starbucks.Services.Commerce.DataContracts.CartItem cartItem)
        {
            if (cartItem == null) return null;
            return new CartItem
                {
                    Id = cartItem.Id,
                    Locale = cartItem.locale,
                    ProductId = cartItem.ProductId,
                    ProductOptions = cartItem.ProductOptions,
                    Quantity = cartItem.Quantity,
                    Site = cartItem.Site,
                    SubItems = cartItem.SubItems != null ? cartItem.SubItems.Select(p => p.ToApi()) : null
                };
        }


        internal static Starbucks.Services.Commerce.DataContracts.Cart ToPlatform(this ICart cart)
        {
            if (cart == null) return null;

            return new Starbucks.Services.Commerce.DataContracts.Cart
            {
                CartId = cart.CartId,
                CartType = cart.CartType.ToPlatform() ,
                DateCreated = cart.DateCreated,
                Items = cart.Items != null ? cart.Items.Select(p => p.ToPlatform()).ToList()  : null,
                PartnerDiscount = cart.PartnerDiscount,
                Token = cart.Token
            };
        }

        internal static Starbucks.Services.Commerce.DataContracts.CartTypes ToPlatform(this CartTypes cartType)
        {
            switch (cartType)
            {
                case CartTypes.BAC:
                    return Starbucks.Services.Commerce.DataContracts.CartTypes.BAC;
                case CartTypes.Custom:
                    return Starbucks.Services.Commerce.DataContracts.CartTypes.Custom;
                case CartTypes.eCard:
                    return Starbucks.Services.Commerce.DataContracts.CartTypes.eCard;
                case CartTypes.ePromoCard:
                    return Starbucks.Services.Commerce.DataContracts.CartTypes.ePromoCard;
                default:
                    throw new ArgumentOutOfRangeException("cartType");
            }
        }

        internal static Starbucks.Services.Commerce.DataContracts.CartItem ToPlatform(this ICartItem cartItem)
        {
            if (cartItem == null) return null;
            return new Starbucks.Services.Commerce.DataContracts.CartItem
            {
                Id = cartItem.Id,
                locale = cartItem.Locale,
                ProductId = cartItem.ProductId,
                ProductOptions = (SerializableDictionary<string, string> ) cartItem.ProductOptions,
                Quantity = cartItem.Quantity,
                Site = cartItem.Site,
                SubItems = cartItem.SubItems != null ? cartItem.SubItems.Select(p => p.ToPlatform() ).ToList()  : null
            };
        }
    }
}
