﻿using System.Collections.Generic;
using Starbucks.Cart.Provider.Common.Models;

namespace Starbucks.Cart.Provider.Models
{
    public class CartItem : ICartItem
    {
        public string ProductId { get; set; }
        public IDictionary<string, string> ProductOptions { get; set; }
        public int Quantity { get; set; }
        public IEnumerable<ICartItem> SubItems { get; set; }
        public string Id { get; set; }
        public string Locale { get; set; }
        public decimal DiscountMultiplier { get; private set; }
        public string Site { get; set; }
    }
}
