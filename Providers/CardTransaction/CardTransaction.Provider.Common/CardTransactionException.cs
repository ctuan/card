﻿using System;

namespace Starbucks.CardTransaction.Provider.Common
{
    public class CardTransactionException : Exception 
    {
        private readonly string _code;

        public CardTransactionException(string code, string message) : base(message)
        {
            _code = code;
        }

        public string Code
        {
            get { return _code; }
        }
    }
}
