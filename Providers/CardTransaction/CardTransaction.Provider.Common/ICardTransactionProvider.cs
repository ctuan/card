﻿using Starbucks.CardTransaction.Provider.Common.Model;


namespace Starbucks.CardTransaction.Provider.Common
{      
    public interface ICardTransactionProvider
    {
        ICardTransaction ReloadCardByUserIdCardId(string userId, string cardId, IReloadForStarbucksCard reloadForStarbucksCard,
            string cvn, string market, string userMarket, string email = null);
        ICardTransaction ReloadStarbucksCard(string userId, string cardId, IReloadForStarbucksCard reloadForStarbucksCard, 
            string market, string userMarket, string email = null);
        ICardTransaction ReloadStarbucksCardByCardNumberPin(string cardNumber, string pin, 
            IReloadForStarbucksCard reloadForStarbucksCard, string emailAddress, string market);
        IActivateAndReloadCardResponse ActivateAndReloadCardAsGift(decimal amount, string paymentMethodId,
                                                                  string paymentType);
        ICardTransaction ReloadAsGiftContribution(string cardNumber, decimal amount, string paymentMethdodId,
                                                  string paymentType);

        ICardTransaction Tip(IProcessTip tip);
    }
}
