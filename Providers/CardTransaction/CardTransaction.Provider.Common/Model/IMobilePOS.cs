﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.CardTransaction.Provider.Common.Model
{
    public interface IMobilePOS
    {
        string TransactionReferenceKey { get; set; }

        string TrackData { get; set; }

        string CombinedTags { get; set; }

        string TerminalId { get; set; }
    }
}
