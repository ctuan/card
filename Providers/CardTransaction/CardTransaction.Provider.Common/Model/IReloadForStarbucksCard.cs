﻿namespace Starbucks.CardTransaction.Provider.Common.Model
{
    public enum PaymentTokenType
    {
        ApplePay,
        ChasePay,
    };

    public interface IReloadForStarbucksCard
    {
        decimal Amount { get; set; }
        string SessionId { get; set; }
        string PaymentMethodId { get; set; }
        string Type { get; set; }
        string IpAddress { get; set; }
        string Platform { get; set; }
        decimal PurchaserSessionDuration { get; set; }
        IAddress BillingAddress { get; set; }
        string PaymentTokenAddressId { get; set; }
        IRisk Risk { get; set; }
        IPaymentTokenDetails PaymentTokenDetails { get; set; }
    }

    public interface IBalanceTransfer
    {
        string SourceCardId { get; set; }
        string TargetCardId { get; set; }
        decimal? Amount { get; set; }
        IRisk Risk { get; set; }
    }

}
