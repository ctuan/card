﻿namespace Starbucks.CardTransaction.Provider.Common.Model
{
    public interface ICardPromotion
    {
        string Code { get; set; }
        decimal Amount { get; set; }
        string Message { get; set; }
    }
}
