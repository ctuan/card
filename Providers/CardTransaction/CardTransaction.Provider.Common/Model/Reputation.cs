﻿namespace Starbucks.CardTransaction.Provider.Common.Model
{
    public class Reputation : IReputation
    {
        public string IpAddress { get; set; }
        public string DeviceFingerprint { get; set; }
    }

    public class Risk : IRisk
    {
        public string Platform { get; set; }
        public string Market { get; set; }
        public bool? IsLoggedIn { get; set; }
        public string CcAgentName { get; set; }
        public IReputation IovationFields { get; set; }
    }
}
