﻿namespace Starbucks.CardTransaction.Provider.Common.Model
{
    public interface IMobileAppSession
    {
        string DeviceName { get; set; }

        string DeviceModel { get; set; }

        string SystemName { get; set; }

        string SystemVersion { get; set; }

        string DeviceId { get; set; }

        string IpAddress { get; set; }

        string Latitude { get; set; }

        string Longitude { get; set; }

        string PhoneNumber { get; set; }

        string PurchaserSessionId { get; set; }

        string PurchaserSessionDuration { get; set; }
    }
}