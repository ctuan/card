﻿using System;

namespace Starbucks.CardTransaction.Provider.Common.Model
{
    public interface IProcessTip
    {
        string UserId { get; set; }
        string CardId { get; set; }
        decimal Amount { get; set; }
        DateTime LocalTransactionTime { get; set; }
        string LocalTransactionId { get; set; }
        string OriginalStoreId { get; set; }
        string OriginalTerminalId { get; set; }
        string OrginalMerchantId { get; set; }
        long OriginalTransactionId { get; set; }
        string UserMarket { get; set; }       
        string OriginalTransactionCurrency { get; set; }
        string MerchantId { get; set; }
    }
}