﻿using Starbucks.Platform.OrderManagement.Contracts.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.SecurePayment.Provider.Common;
using Starbucks.SecurePayment.Provider.Common.Models;

namespace Starbucks.CardTransaction.Provider.Common.Model
{
    /// IStarbucksCardReloadConfirmation
    /// </summary>
    public interface IStarbucksCardReloadConfirmation
    {
        string EmailAddress { get; set; }
        string Site { get; set; }
        string Language { get; set; }
        string PageEntered { get; set; }
        string Host { get; set; }
        string SecurityToken { get; set; }
        string OrderID { get; set; }
        string CurrencyType { get; set; }
        string DateReceived { get; set; }
        string FullName { get; set; }
        string AddressLines { get; set; }
        string CityStateZip {get; set;}
        List<IOrderLineItem> Items {get; set;}
        List<RepeaterRow> RepeaterRows { get; set; }
        DateTime LastUpdateDate { get; set; }
        DateTime CreateDate { get; set; }
    }

    public class StarbucksCardReloadConfirmation : IStarbucksCardReloadConfirmation
    {
        public string EmailAddress { get; set; }
        public string Site { get; set; }
        public string Language { get; set; }
        public string PageEntered { get; set; }
        public string Host { get; set; }
        public string SecurityToken { get; set; }
        public string OrderID { get; set; }
        public string CurrencyType { get; set; }
        public string DateReceived { get; set; }
        public string FullName { get; set; }
        public string AddressLines { get; set; }
        public string CityStateZip { get; set; }
        public List<IOrderLineItem> Items { get; set; }
        public List<RepeaterRow> RepeaterRows { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
