﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.CardTransaction.Provider.Common.Model
{
    public enum ReloadTransactionResult
    {
        None = 0,
        Success = 1,
        Error = 2,
        Void = 3
    }

    public interface IReloadTransaction
    {
        string CartItemId { get; set; }
        string CardId { get; set; }
        string TransactionId { get; set; }
        decimal Amount { get; set; }
        string Currency { get; set; }
        decimal BeginningBalance { get; set; }
        decimal EndingBalance { get; set; }
        ReloadTransactionResult TransactionResult { get; set; }
        string ErrorCode { get; set; }
        string Message { get; set; }
    }
}
