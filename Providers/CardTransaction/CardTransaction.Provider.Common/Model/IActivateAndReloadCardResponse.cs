﻿namespace Starbucks.CardTransaction.Provider.Common.Model
{
    public interface IActivateAndReloadCardResponse
    {
        ICardTransaction CardActivationTransaction { get; set; }
        ICardTransaction CardReloadTransaction { get; set; }
    }
}
