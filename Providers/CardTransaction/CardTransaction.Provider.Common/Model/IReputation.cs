﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.CardTransaction.Provider.Common.Model
{
    public interface IReputation    {
        string IpAddress { get; set; }
        string DeviceFingerprint { get; set; }
    }

    public interface IRisk
    {
        string Platform { get; set; }
        string Market { get; set; }
        bool? IsLoggedIn { get; set; }
        string CcAgentName { get; set; }
        IReputation IovationFields { get; set; }
    }
}
