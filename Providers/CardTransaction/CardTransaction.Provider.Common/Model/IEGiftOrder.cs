﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.CardTransaction.Provider.Common.Model
{
    public interface IEGiftOrder
    {
        string Theme { get; set; }

        string DeliveryMethod { get; set; }

        int Amount { get; set; }

        string Currency { get; set; }

        string SenderName { get; set; }

        string SenderEmail { get; set; }

        bool SendGiftToSenderEmail { get; set; }

        DateTime DeliveryDate { get; set; }

        string RecipientName { get; set; }

        string RecipientEmail { get; set; }

        string Message { get; set; }

        string PaymentType { get; set; }

        string PaymentMethodId { get; set; }

        string PaymentMethodCvn { get; set; }

        string StarbucksCardId { get; set; }

        IMobileAppSession MobileAppSession { get; set; }
    }
}
