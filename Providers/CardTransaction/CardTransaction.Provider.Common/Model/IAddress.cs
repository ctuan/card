﻿namespace Starbucks.CardTransaction.Provider.Common.Model
{
    public interface IAddress
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        string PhoneNumber { get; set; }
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        string City { get; set; }
        string CountrySubdivision { get; set; }
        string Country { get; set; }
        string PostalCode { get; set; }
    }
}
