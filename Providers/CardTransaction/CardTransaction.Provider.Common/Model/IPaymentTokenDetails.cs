﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.CardTransaction.Provider.Common.Model
{
    public interface IPaymentTokenDetails
    {
        PaymentTokenType PaymentTokenType { get; set; }
        string PaymentToken { get; set; }

        string PaymentCryptogram { get; set; }

        int? ExpirationMonth { get; set; }

        int? ExpirationYear { get; set; }

        string ECIndicator { get; set; }

        string TokenRequestorId { get; set; }

        IMobilePOS MobilePOS { get; set; }

    }
}
