﻿namespace Starbucks.CardTransaction.Provider.Common.Model
{
    public class ReloadForStarbucksCard : IReloadForStarbucksCard
    {
        public decimal Amount { get; set; }
        public string SessionId { get; set; }
        public string PaymentMethodId { get; set; }
        public string Type { get; set; }
        public string IpAddress { get; set; }
        public string Platform { get; set; }
        public decimal PurchaserSessionDuration { get; set; }
        public IAddress BillingAddress { get; set; }
        public IPaymentTokenDetails PaymentTokenDetails { get; set; }
        public string PaymentTokenAddressId { get; set; }
        public IRisk Risk { get; set; }
    }

    public class BalanceTransfer : IBalanceTransfer
    {
        public string SourceCardId { get; set; }
        public string TargetCardId { get; set; }
        public decimal? Amount { get; set; }
        public IRisk Risk { get; set; }
    }

}
