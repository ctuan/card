﻿using System;

namespace Starbucks.CardTransaction.Provider.Common
{
    public class CardTransactionValidationException : Exception
    {
        
        private readonly string _code;

        public CardTransactionValidationException(string code, string message)
            : base(message)
        {
            _code = code;
        }

        public string Code
        {
            get { return _code; }
        }
    }
}
