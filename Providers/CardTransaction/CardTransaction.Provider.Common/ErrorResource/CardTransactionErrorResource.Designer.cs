﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Starbucks.CardTransaction.Provider.Common.ErrorResource {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class CardTransactionErrorResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal CardTransactionErrorResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Starbucks.CardTransaction.Provider.Common.ErrorResource.CardTransactionErrorResou" +
                            "rce", typeof(CardTransactionErrorResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3004.
        /// </summary>
        public static string BillingAddressNotFoundCode {
            get {
                return ResourceManager.GetString("BillingAddressNotFoundCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Billing address not found.
        /// </summary>
        public static string BillingAddressNotFoundMessage {
            get {
                return ResourceManager.GetString("BillingAddressNotFoundMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3013.
        /// </summary>
        public static string BillingInfoNotFoundCode {
            get {
                return ResourceManager.GetString("BillingInfoNotFoundCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Billing info not found.
        /// </summary>
        public static string BillingInfoNotFoundMessage {
            get {
                return ResourceManager.GetString("BillingInfoNotFoundMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3009.
        /// </summary>
        public static string CannotAuthorizeCreditCardCode {
            get {
                return ResourceManager.GetString("CannotAuthorizeCreditCardCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error authorizing credit card.
        /// </summary>
        public static string CannotAuthorizeCreditCardMessage {
            get {
                return ResourceManager.GetString("CannotAuthorizeCreditCardMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3003.
        /// </summary>
        public static string CannotAuthorizePayPalCode {
            get {
                return ResourceManager.GetString("CannotAuthorizePayPalCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error authorizing paypal.
        /// </summary>
        public static string CannotAuthorizePayPalMessage {
            get {
                return ResourceManager.GetString("CannotAuthorizePayPalMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3007.
        /// </summary>
        public static string CannotReloadCardClassCode {
            get {
                return ResourceManager.GetString("CannotReloadCardClassCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot reload this card class.
        /// </summary>
        public static string CannotReloadCardClassMessage {
            get {
                return ResourceManager.GetString("CannotReloadCardClassMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3010.
        /// </summary>
        public static string CannotReloadCardMarketCode {
            get {
                return ResourceManager.GetString("CannotReloadCardMarketCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot reload card for this market..
        /// </summary>
        public static string CannotReloadCardMarketMessage {
            get {
                return ResourceManager.GetString("CannotReloadCardMarketMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3011.
        /// </summary>
        public static string CannotTipCode {
            get {
                return ResourceManager.GetString("CannotTipCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error tipping..
        /// </summary>
        public static string CannotTipMessage {
            get {
                return ResourceManager.GetString("CannotTipMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 2000.
        /// </summary>
        public static string CannotUnregisterDuettoCardCode {
            get {
                return ResourceManager.GetString("CannotUnregisterDuettoCardCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot unregister Duetto card.
        /// </summary>
        public static string CannotUnregisterDuettoCardMessage {
            get {
                return ResourceManager.GetString("CannotUnregisterDuettoCardMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3001.
        /// </summary>
        public static string CardNotFoundCode {
            get {
                return ResourceManager.GetString("CardNotFoundCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Card not found.
        /// </summary>
        public static string CardNotFoundMessage {
            get {
                return ResourceManager.GetString("CardNotFoundMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3005.
        /// </summary>
        public static string CreditCardBillFailedCode {
            get {
                return ResourceManager.GetString("CreditCardBillFailedCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to bill credit card.
        /// </summary>
        public static string CreditCardBillFailedMessage {
            get {
                return ResourceManager.GetString("CreditCardBillFailedMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3008.
        /// </summary>
        public static string CreditCardBillingAddressErrorCode {
            get {
                return ResourceManager.GetString("CreditCardBillingAddressErrorCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Card card billing address missing data.
        /// </summary>
        public static string CreditCardBillingAddressErrorMessage {
            get {
                return ResourceManager.GetString("CreditCardBillingAddressErrorMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3015.
        /// </summary>
        public static string FraudCheckFailedCode {
            get {
                return ResourceManager.GetString("FraudCheckFailedCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fraud check failed..
        /// </summary>
        public static string FraudCheckFailedMessage {
            get {
                return ResourceManager.GetString("FraudCheckFailedMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3006.
        /// </summary>
        public static string GeneralPaymentErrorCode {
            get {
                return ResourceManager.GetString("GeneralPaymentErrorCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to General payment error .
        /// </summary>
        public static string GeneralPaymentErrorMessage {
            get {
                return ResourceManager.GetString("GeneralPaymentErrorMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3012.
        /// </summary>
        public static string InvalidTipStatusCode {
            get {
                return ResourceManager.GetString("InvalidTipStatusCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid tip status to process tip..
        /// </summary>
        public static string InvalidTipStatusMessage {
            get {
                return ResourceManager.GetString("InvalidTipStatusMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3014.
        /// </summary>
        public static string MerchantInfoNotFoundCode {
            get {
                return ResourceManager.GetString("MerchantInfoNotFoundCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Merchant info not found.
        /// </summary>
        public static string MerchantInfoNotFoundMessage {
            get {
                return ResourceManager.GetString("MerchantInfoNotFoundMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3002.
        /// </summary>
        public static string PaymentMethodNotFoundCode {
            get {
                return ResourceManager.GetString("PaymentMethodNotFoundCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Payment method not found.
        /// </summary>
        public static string PaymentMethodNotFoundMessage {
            get {
                return ResourceManager.GetString("PaymentMethodNotFoundMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3000.
        /// </summary>
        public static string UserNotFoundCode {
            get {
                return ResourceManager.GetString("UserNotFoundCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User not found.
        /// </summary>
        public static string UserNotFoundMessage {
            get {
                return ResourceManager.GetString("UserNotFoundMessage", resourceCulture);
            }
        }
    }
}
