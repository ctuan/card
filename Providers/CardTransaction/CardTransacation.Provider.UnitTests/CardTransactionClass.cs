﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NServiceBus;
using Starbucks.Api.Sdk.PaymentService.Interfaces;
using Starbucks.Api.Sdk.PaymentService.Models;
using Starbucks.Card.Provider.Common;
using Starbucks.Card.Provider.Common.Enums;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.Common.Resources;
using Starbucks.CardTransaction.Provider;
using Starbucks.CardTransaction.Provider.Common;
using Starbucks.CardTransaction.Provider.Common.ErrorResource;
using Starbucks.CardTransaction.Provider.Common.Model;
using Starbucks.CardTransaction.Provider.Configuration;
using Starbucks.CardTransaction.Provider.PayPalModels;
using Starbucks.LogCounter.Provider.Common;
using Starbucks.MessageBroker.Common;
using Starbucks.OrderManagement.Dal.Common.Models;
using Starbucks.OrderManagement.Provider.Common;
using Starbucks.PaymentMethod.Provider.Common;
using Starbucks.Rewards.Provider.Common;
using Starbucks.SecurePayPalPayment.Provider.Common;
using Starbucks.SecurePayPalPayment.Provider.Common.Models;
using Starbucks.SecurePayment.Provider.Common;
using Starbucks.Settings.Provider.Common;
using Starbucks.Tipping.Dal.Common.Enums;
using Starbucks.Tipping.Provider.Common;
using Starbucks.TransactionHistory.Dal.Common;
using IAddress = Starbucks.CardTransaction.Provider.Common.Model.IAddress;
using ICardTransaction = Starbucks.CardTransaction.Provider.Common.Model.ICardTransaction;
using IPaymentMethod = Starbucks.PaymentMethod.Provider.Common.Models.IPaymentMethod;
using IReloadForStarbucksCard = Starbucks.CardTransaction.Provider.Common.Model.IReloadForStarbucksCard;
using Starbucks.Api.Sdk.PaymentService;
using Starbucks.Api.Sdk.WebRequestWrapper;
using Address = Starbucks.CardTransaction.Provider.PayPalModels.Address;
using BillRequest = Starbucks.Api.Sdk.PaymentService.Models.BillRequest;
using IReputation = Starbucks.CardTransaction.Provider.Common.Model.IReputation;
using Starbucks.CardTransaction.Provider.CardIssuerModels;
using StarbucksAccountProvider = Account.Provider;


namespace CardTransacation.Provider.UnitTests
{
    [TestClass]
    public class CardTransactionClass
    {
        protected CardTransactionProvider CardTransactionProvider;
        protected Mock<ICardProvider> CardProvider = new Mock<ICardProvider>();
        protected Mock<ICardIssuerDal> CardIssuerDal = new Mock<ICardIssuerDal>();
        protected Mock<IPaymentMethodProvider> PaymentMethodProvider = new Mock<IPaymentMethodProvider>();
        protected Mock<StarbucksAccountProvider.Common.IAccountProvider> AccountProvider = new Mock<StarbucksAccountProvider.Common.IAccountProvider>();
        protected Mock<IPaymentServiceApiClient> PaymentServiceApiClient = new Mock<IPaymentServiceApiClient>();
        protected Mock<ILogCounterManager> LogCounterManager = new Mock<ILogCounterManager>();
        protected Mock<ISecurePayPalPaymentClient> SecurePayPalPaymentClient = new Mock<ISecurePayPalPaymentClient>();
        protected Mock<ITippingProvider> TippingProvider = new Mock<ITippingProvider>();

        protected Mock<ITransactionHistoryDataProvider> TransactionHistoryDataProvider =
            new Mock<ITransactionHistoryDataProvider>();

        protected Mock<IMessageBroker> MessageBroker = new Mock<IMessageBroker>();
        protected Mock<ISettingsProvider> SettingsProvider = new Mock<ISettingsProvider>();
        protected Mock<IRewardsProvider> RewardsProvider = new Mock<IRewardsProvider>();
        protected Mock<IOrderManagementProvider> OrderManagementProvider = new Mock<IOrderManagementProvider>();
        protected Mock<ISendOnlyBus> bus = new Mock<ISendOnlyBus>();

        protected void Given_That_I_Have_A_Card_Transaction(string sectionName = "cardTransactionProviderSettingsSection")
        {
            var transactionProviderSettings = ConfigurationManager.GetSection(
                sectionName) as
                CardTransactionProviderSettingsSection;
            CardTransactionProvider = new CardTransactionProvider(CardProvider.Object, CardIssuerDal.Object,
                                                                  PaymentMethodProvider.Object,
                                                                  AccountProvider.Object,
                                                                  LogCounterManager.Object,
                                                                  SecurePayPalPaymentClient.Object,
                                                                  TippingProvider.Object,
                                                                  TransactionHistoryDataProvider.Object,
                                                                  MessageBroker.Object,
                                                                  SettingsProvider.Object,
                                                                  transactionProviderSettings,
                                                                  RewardsProvider.Object,
                                                                  OrderManagementProvider.Object,
                                                                  PaymentServiceApiClient.Object,
                                                                  bus.Object
                );
        }

        protected void Given_A_Valid_Address(Mock<Starbucks.CardTransaction.Provider.Common.Model.IAddress> address)
        {
            Given_An_Address(address, "Test", "User", "123 A St", null, "Acity", "Astate", "12354", "US");
        }

        protected void Given_An_Address(Mock<Starbucks.CardTransaction.Provider.Common.Model.IAddress> address,
                                        string firstName = null, string lastName = null, string addressLine1 = null,
                                        string addressLine2 = null
                                        , string city = null, string state = null, string postalcode = null,
                                        string country = null)
        {
            address.SetupProperty(p => p.AddressLine1, addressLine1);
            address.SetupProperty(p => p.AddressLine2, addressLine2);
            address.SetupProperty(p => p.City, city);
            address.SetupProperty(p => p.CountrySubdivision, state);
            address.SetupProperty(p => p.FirstName, firstName);
            address.SetupProperty(p => p.LastName, lastName);
            address.SetupProperty(p => p.PostalCode, postalcode);

        }

        private void Given_A_PaymentMethod(Mock<IPaymentMethodProvider> paymentMethodProvider, string paymentMethodId,
                                           string paymentType, string accountNumber, bool isFraudChecked,
                                           DateTime fraudCheckDate, bool isTemporaryPaymentMethod = false)
        {
            var paymentMethod = new Mock<IPaymentMethod>();
            paymentMethod.SetupProperty(pm => pm.PaymentType, paymentType);
            paymentMethod.SetupProperty(pm => pm.PaymentMethodId, paymentMethodId);
            paymentMethod.SetupProperty(pm => pm.AccountNumber, accountNumber);
            paymentMethod.SetupProperty(pm => pm.IsFraudChecked, isFraudChecked);
            paymentMethod.SetupProperty(pm => pm.FraudCheckedDate, fraudCheckDate);
            paymentMethod.SetupProperty(pm => pm.IsTemporary, isTemporaryPaymentMethod);
            paymentMethod.SetupProperty(pm => pm.BillingAddressId, Guid.NewGuid().ToString());

            paymentMethodProvider.Setup(pmp => pmp.GetPaymentMethod(paymentMethodId)).Returns(paymentMethod.Object);
            paymentMethodProvider.Setup(pmp => pmp.GetPaymentMethod(It.IsAny<string>(), paymentMethodId))
                                 .Returns(paymentMethod.Object);
            paymentMethodProvider.Setup(
                pmp => pmp.UpdateFraudCheckFlag(It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<DateTime?>()));
        }


        private void Given_A_User(Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider, string userId, string emailAddress, string lastName = null)
        {
            var user = new Mock<StarbucksAccountProvider.Common.Models.IUser>();
            user.SetupProperty(p => p.UserId, userId);
            user.SetupProperty(p => p.EmailAddress, emailAddress);
            user.SetupProperty(p => p.LastName, lastName);
            accountProvider.Setup(ap => ap.GetUser(It.IsAny<string>())).Returns(user.Object);
        }

        private void Given_Address(Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider, bool throwExceptionOnGetAddress = false)
        {
            var address = new Mock<StarbucksAccountProvider.Common.Models.IAddress>();
            address.SetupProperty(p => p.AddressType, "Billing");

            if (!throwExceptionOnGetAddress)
            {
                accountProvider.Setup(ap => ap.GetAddress(It.IsAny<string>(), It.IsAny<string>()))
                    .Returns(address.Object);
            }
            else
            {
                accountProvider.Setup(ap => ap.GetAddress(It.IsAny<string>(), It.IsAny<string>()))
                    .Throws(new Exception());
            }
        }

        private void Given_A_Card(Mock<ICardProvider> cardProvider, string userId, string cardId, string cardClass,
            string number, string pin, IEnumerable<string> actions, bool throwUpsertCardTransactionHistory = false)
        {
            var card = new Mock<Starbucks.Card.Provider.Common.Models.IAssociatedCard>();
            card.SetupProperty(c => c.RegisteredUserId, userId);
            card.SetupProperty(c => c.CardId, cardId);
            card.SetupProperty(c => c.Class, cardClass);
            card.SetupProperty(c => c.Number, number);
            card.SetupProperty(c => c.Pin, pin);
            card.SetupProperty(c => c.Actions, actions);
            cardProvider.Setup(
                cp => cp.GetCardById(card.Object.RegisteredUserId, card.Object.CardId, VisibilityLevel.Decrypted,
                    false, "US", "US", It.IsAny<bool>(), It.IsAny<string>())).Returns(card.Object);
            if (throwUpsertCardTransactionHistory)
            {
                cardProvider.Setup(c => c.UpsertCardTransactionHistory(It.IsAny<string>(),
                    It.IsAny<Starbucks.Card.Provider.Common.Models.ICardTransaction>()))
                    .Throws(new Exception("Mock UpsertCardTransactionHistory exception."));
            }
            cardProvider.Setup(
                cp => cp.ClearPaymentMethodSurrogateAndPaypalAccountNumbers(It.IsAny<string>(), It.IsAny<IPaymentMethod>()));
        }

        private void Given_Fraud_Check_Data(Mock<ICardProvider> cardProvider, string userId,
                                            int ageOfAccountSinceCreation, bool isPartner, int birthDay, int birthMonth,
                                            bool mailSignup, bool eMailSignup, bool twitterSignup, bool facebookSignup)
        {
            var userData = new Mock<Starbucks.Card.Provider.Common.Models.IFraudCheckUserData>();
            userData.SetupProperty(ud => ud.UserId, userId);
            userData.SetupProperty(ud => ud.AgeOfAccountSinceCreation, ageOfAccountSinceCreation);
            userData.SetupProperty(ud => ud.IsPartner, isPartner);
            userData.SetupProperty(ud => ud.BirthDay, birthDay);
            userData.SetupProperty(ud => ud.BirthMonth, birthMonth);
            userData.SetupProperty(ud => ud.MailSignUp, mailSignup);
            userData.SetupProperty(ud => ud.eMailSignUp, eMailSignup);
            userData.SetupProperty(ud => ud.TwitterSignUp, twitterSignup);
            userData.SetupProperty(ud => ud.FacebookSignUp, facebookSignup);

            cardProvider.Setup(cp => cp.GetUserDataForFraudCheck(userId)).Returns(userData.Object);
        }

        private void Given_Fraud_Check_Data(Mock<ICardProvider> cardProvider, string userId, string cardId, string nickname, bool defaultCard, int totalCardsRegistered)
        {
            var cardData = new Mock<Starbucks.Card.Provider.Common.Models.IFraudCheckCardData>();
            cardData.SetupProperty(cd => cd.Nickname, nickname);
            cardData.SetupProperty(cd => cd.IsDefaultSvcCard, defaultCard);
            cardData.SetupProperty(cd => cd.TotalCardsRegistered, totalCardsRegistered);

            cardProvider.Setup(cp => cp.GetCardDataForFraudCheck(userId, cardId)).Returns(cardData.Object);
        }

        private void Given_Settings(Mock<ISettingsProvider> settingsProvider)
        {
            settingsProvider.Setup(x => x.GetLocaleByMarket(It.IsAny<string>()))
                .Returns(new List<string>
                {
                    "en-US"
                }
            );
        }

        private const string PayPalBillExceptionMessage = "Unit test Paypal Bill exception message";

        private void Given_Secure_PayPal_Client(Mock<ISecurePayPalPaymentClient> securePayPalPaymentClient, PaymentStatusCode authPaymentStatusCode,
            PendingStatusCode authPendingStatusCode, bool billingAgreementResult,
            bool throwOnDoReferenceTransaction = false, bool throwOnUpdateBillingAgreement = false,
            bool throwOnBill = false)
        {
            // Use the same flag for both of these for now.
            var authResponse = new Mock<IDoReferenceTransactionResponse>();
            var transactionDetails = new Mock<ITransactionDetails>();
            transactionDetails.SetupProperty(td => td.TransactionId, "1234");
            transactionDetails.SetupProperty(td => td.PendingStatusCode, authPendingStatusCode);
            var payPalBillingAddress = new Mock<Starbucks.SecurePayPalPayment.Provider.Common.Models.IAddress>();
            transactionDetails.SetupProperty(td => td.BillingAddress, payPalBillingAddress.Object);

            var payerInformation = new PayerInformation()
            {

                Address = new Address()
                {
                    AddressId = Guid.NewGuid().ToString(),
                    Street1 = "123 Main St.",
                },
                FirstName = "Chuck",
                LastName = "Berry"
            };
            transactionDetails.SetupProperty(td => td.PayerInformation, payerInformation);

            var billingAgreementUdpateResponse = new Mock<IBillingAgreementUdpateResponse>();

            billingAgreementUdpateResponse.SetupProperty(x => x.PayerInformation.PayerId, billingAgreementResult ? "test payer id" : string.Empty);

            if (throwOnDoReferenceTransaction)
            {
                //                var exception = new SecurePayPalPaymentException("Code:888", 
                //                    "Unit test DoReferenceTransaction exception message");
                var exception = new SecurePayPalPaymentException(SecurePayPalPaymentErrorResource.AccountClosedCode,
                    SecurePayPalPaymentErrorResource.AccountClosedMessage);

                securePayPalPaymentClient.Setup(sppc => sppc.DoReferenceTransaction(It.IsAny<IDoReferenceTransactionRequest>())).Throws(exception);
            }
            else
            {
                authResponse.SetupProperty(a => a.TransactionDetails, transactionDetails.Object);
                securePayPalPaymentClient.Setup(sppc => sppc.DoReferenceTransaction(It.IsAny<IDoReferenceTransactionRequest>())).Returns(authResponse.Object);
            }

            if (throwOnBill)
            {
                var exception = new Exception(PayPalBillExceptionMessage);
                securePayPalPaymentClient.Setup(sppc => sppc.Bill(It.IsAny<IBillRequest>()))
                    .Throws(exception);
            }
            else
            {
                var billResponse = new Mock<IBillResponse>();
                billResponse.SetupProperty(br => br.PaymentStatusCode, authPaymentStatusCode);
                securePayPalPaymentClient.Setup(sppc => sppc.Bill(It.IsAny<IBillRequest>())).Returns(billResponse.Object);
            }

            securePayPalPaymentClient.Setup(sppc => sppc.Void(It.IsAny<IVoidRequest>()));

            if (throwOnUpdateBillingAgreement)
            {
                var exception = new Exception("Unit test Paypal billing agreement exception message");
                securePayPalPaymentClient.Setup(sppc => sppc.UpdateBillingAgreement(It.IsAny<BillingAgreementUpdateRequest>()))
                    .Throws(exception);
            }
            else
            {
                securePayPalPaymentClient.Setup(
                sppc => sppc.UpdateBillingAgreement(It.IsAny<BillingAgreementUpdateRequest>()))
                .Returns(billingAgreementUdpateResponse.Object);
            }

            securePayPalPaymentClient.Setup(sppc => sppc.GetTransactionDetails(It.IsAny<ITransactionDetailsRequest>())).
                Returns(transactionDetails.Object);

        }

        private void Given_Log_Counter_Manager(Mock<ILogCounterManager> logCounterManager)
        {
            logCounterManager.Setup(lcm => lcm.Increment(It.IsAny<string>()));
        }

        private void Given_Reload_ForStarbucks(
            Mock<Starbucks.CardTransaction.Provider.Common.Model.IReloadForStarbucksCard> reload, decimal amount,
            string paymentMethodId, string token, PaymentTokenType? paymentTokenType, IAddress address, string sessionId = null,
            string platform = null, Mock<IRisk> reputation = null)
        {
            var paymentToken = new Mock<IPaymentTokenDetails>();
            paymentToken.SetupProperty(p => p.PaymentToken, token);
            if (paymentTokenType != null)
            {
                paymentToken.SetupProperty(p => p.PaymentTokenType, paymentTokenType.Value);
            }

            reload.SetupProperty(p => p.Amount, amount);
            reload.SetupProperty(p => p.PaymentMethodId, paymentMethodId);
            reload.SetupProperty(p => p.PaymentTokenDetails, paymentToken.Object);
            reload.SetupProperty(p => p.BillingAddress, address);
            reload.SetupProperty(p => p.SessionId, sessionId);
            reload.SetupProperty(p => p.Platform, platform);
            if (reputation != null) reload.SetupProperty(p => p.Risk, reputation.Object);
        }

        private void Given_A_Reputation(Mock<IRisk> reputation,
            string platform = null, Mock<IReputation> iovationFields = null)
        {
            reputation.SetupProperty(p => p.Platform, platform);
            reputation.SetupProperty<IReputation>(p => p.IovationFields, iovationFields.Object);
        }


        enum PsBillCallResult
        {
            Accept,
            Deny,
            Throw,
        }
        private void Given_PaymentServiceApiClient(Mock<IPaymentServiceApiClient> securePaymentClient, bool fruadCheckSucceeds,
            bool throwExceptionForAuthAndFraudCheck = false, string apiErrorCode = "0", PsBillCallResult psBillCallResult = PsBillCallResult.Accept)
        {
            // Note. This is applying 'fruadCheckSucceeds' to both the FraudCheck and Auth calls. Separate into two if needed.
            // Note. This is applying 'throwFraudCheckException' to both the FraudCheck and Auth calls. Separate into two if needed.

            if (throwExceptionForAuthAndFraudCheck)
            {
                securePaymentClient.Setup(x => x.FraudCheck(It.IsAny<FraudCheckRequest>()))
                    .Throws(new Starbucks.Api.Sdk.PaymentService.Common.SecurePaymentException("Dummy"));

                var exception = new Starbucks.Api.Sdk.PaymentService.Common.SecurePaymentException(apiErrorCode, "Auth SecurePaymentException");
                securePaymentClient.Setup(x => x.Auth(It.IsAny<AuthRequest>())).Throws(exception);
            }
            else
            {
                var fraudCheckResult = new Mock<IFraudCheckResult>();
                fraudCheckResult.SetupProperty(x => x.DecisionType, fruadCheckSucceeds ? DecisionType.Accept : DecisionType.Deny);
                securePaymentClient.Setup(x => x.FraudCheck(It.IsAny<FraudCheckRequest>())).Returns(fraudCheckResult.Object);

                var authResult = new Mock<IAuthResult>();
                if (fruadCheckSucceeds)
                {
                    authResult.SetupProperty(x => x.DecisionType, DecisionType.Accept);
                }
                else
                {
                    authResult.SetupProperty(x => x.DecisionType, DecisionType.Deny);
                    var decisionReason = new DecisionReason()
                    {
                        Code = 210                    // 210 is Decision Manager code for Insufficient Funds.
                    };
                    authResult.SetupProperty(x => x.DecisionReason, decisionReason);

                }
                securePaymentClient.Setup(x => x.Auth(It.IsAny<AuthRequest>())).Returns(authResult.Object);
            }

            var billResult = new Mock<IBillResult>();
            if (psBillCallResult == PsBillCallResult.Throw)
            {
                var innerTimeoutException = new ApiException(WebExceptionStatus.Timeout, String.Empty,
                    System.Net.HttpStatusCode.RequestTimeout);
                securePaymentClient.Setup(x => x.Bill(It.IsAny<Starbucks.Api.Sdk.PaymentService.Models.BillRequest>()))
                    .Throws(new Starbucks.Api.Sdk.PaymentService.Common.SecurePaymentException(null, String.Empty,
                        innerTimeoutException));
            }
            else
            {
                billResult.SetupProperty(x => x.Decision, psBillCallResult == PsBillCallResult.Accept ? DecisionType.Accept : DecisionType.Deny);
                securePaymentClient.Setup(x => x.Bill(It.IsAny<Starbucks.Api.Sdk.PaymentService.Models.BillRequest>())).Returns(billResult.Object);
            }
            securePaymentClient.Setup(x => x.ReportPayPalAuthFailure(It.IsAny<FraudCheckRequest>()));
        }


        private void Given_Merchant_Info(Mock<ICardIssuerDal> cardIssuer, string currency, string market = null)
        {
            var merchantInfo = new Mock<IMerchantInfo>();
            merchantInfo.SetupProperty(p => p.CurrencyCode, currency);
            merchantInfo.SetupProperty(p => p.MarketId, market);

            cardIssuer.Setup(ci => ci.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);
        }

        private void Given_An_Order_Manager(Mock<IOrderManagementProvider> orderManagementProvider)
        {
            orderManagementProvider.Setup(ci => ci.SaveFailedOrder(It.IsAny<IFailedOrder>()));
        }

        private void Given_Value_Link_Reload(Mock<ICardIssuerDal> cardIssuer, string responseCode,
            bool throwOnVoidRequest = false, bool throwOnReload = false)
        {
            const string Reload = "2300";
            var result = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>();
            result.SetupGet(r => r.TransactionSucceeded).Returns(true);
            result.SetupGet(ct => ct.RequestCode).Returns(Reload);

            if (throwOnVoidRequest)
            {
                cardIssuer.Setup(
                    ci => ci.VoidRequest(It.IsAny<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>(), It.IsAny<IMerchantInfo>()))
                    .Throws<Exception>();
            }
            else
            {
                cardIssuer.Setup(ci => ci.VoidRequest(It.IsAny<Enums.CallItCommand>(), It.IsAny<IMerchantInfo>(), It.IsAny<int>())).Returns(result.Object);
            }

            if (throwOnReload)
            {
                cardIssuer.Setup(ci => ci.Reload(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<decimal>()))
                    .Throws(new CardIssuerException(CardIssuerErrorResource.ValueLinkHostDownCode, "Dummy Message"));
            }
            else
            {
                result.SetupProperty(r => r.ResponseCode, responseCode);
                cardIssuer.Setup(ci => ci.Reload(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<decimal>()))
                          .Returns(result.Object);
            }
        }

        private void Given_Value_Link_Tip(Mock<ICardIssuerDal> cardIssuer, string cardNumber,
                                          decimal amount, DateTime localTransactionTime, long localTransactionId, string responseCode, bool success)
        {
            var result = new Mock<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>();
            result.SetupProperty(r => r.ResponseCode, responseCode);
            result.SetupProperty(r => r.Amount, amount);
            result.SetupGet(r => r.TransactionSucceeded).Returns(true);
            //cardIssuer.Setup(p => p.Tip(It.IsAny<IMerchantInfo>(), cardNumber, amount, localTransactionTime, localTransactionId)).Returns(result.Object);
            cardIssuer.Setup(p => p.Tip(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), amount, It.IsAny<DateTime>(), It.IsAny<long>())).Returns(result.Object);
        }

        private ICardTransaction When_Reload_A_Card(string userId, string cardId, Mock<IReloadForStarbucksCard> mockReloadForStarbucksCard, string cvn, string market, string userMarket)
        {
            return CardTransactionProvider.ReloadCardByUserIdCardId(userId, cardId, mockReloadForStarbucksCard.Object,
                                                                                      cvn, market, userMarket);
        }

        private void Then_Success(ICardTransaction cardTransaction)
        {
            Assert.AreEqual(true, cardTransaction.TransactionSucceeded);
        }

        [TestClass]
        public class ReloadCreditCard : CardTransactionClass
        {
            readonly string _cardId = Starbucks.Platform.Security.Encryption.EncryptCardId(12345);
            private const string CardClass = "69";
            private const string CardNumber = "7111111111111111";
            private const string Pin = "12345678";
            readonly List<string> _actions = new List<string>() { "Reload" };
            private const string EmailAddress = "test@test.com";
            private const string Cvn = "123";
            private const string Market = "US";
            private const string UserMarket = "US";
            readonly string _paymentMethodId = Starbucks.Platform.Security.Encryption.EncryptCardId(98764);
            private const string PaymentType = "Visa";
            private const string AccountNumber = "123";


            [TestMethod]
            public void Given_A_Reload_When_CC_And_All_Is_Ok_Reload_With_Risk_Data_Then_Ok()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD", "US");
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, Guid.NewGuid().ToString(), "Web", mockReputation);
                Given_PaymentServiceApiClient(PaymentServiceApiClient, fruadCheckSucceeds: true);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                Then_Success(result);
            }

            [TestMethod]
            public void Given_A_Reload_When_CC_And_Card_Throws_UpsertCardTransactionHistory_Exception_Then_Ok()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD", "US");
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions, true);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, Guid.NewGuid().ToString(), "Web", mockReputation);
                Given_PaymentServiceApiClient(PaymentServiceApiClient, fruadCheckSucceeds: true);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                Then_Success(result);
            }

            [TestMethod]
            public void Given_A_Reload_When_CC_And_All_Is_Ok_Reload_But_No_Risk_Data_Then_Ok()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD", "US");
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, Guid.NewGuid().ToString(), "Web");
                Given_PaymentServiceApiClient(PaymentServiceApiClient, fruadCheckSucceeds: true);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                Then_Success(result);
            }

            [TestMethod]
            [ExpectedException(typeof(CardTransactionException))]
            public void Given_A_Reload_When_Auth_Result_Is_Null()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD", "US");
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, Guid.NewGuid().ToString(), "Web");
                Given_PaymentServiceApiClient(PaymentServiceApiClient, fruadCheckSucceeds: true);
                PaymentServiceApiClient.Setup(x => x.Auth(It.IsAny<AuthRequest>())).Returns((IAuthResult)null);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                Then_Success(result);
            }

            [TestMethod]
            [ExpectedException(typeof(CardTransactionException))]
            public void Given_A_Reload_When_BillResult_Is_Null()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD", "US");
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, Guid.NewGuid().ToString(), "Web");
                Given_PaymentServiceApiClient(PaymentServiceApiClient, fruadCheckSucceeds: true);
                PaymentServiceApiClient.Setup(x => x.Bill(It.IsAny<Starbucks.Api.Sdk.PaymentService.Models.BillRequest>())).Returns((IBillResult)null);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                Then_Success(result);
            }

            [TestMethod]
            [ExpectedException(typeof(CardTransactionException))]
            public void Given_A_Reload_When_CC_And_Invalid_Card_Class_Reload_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                const string cardClass = "254";

                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, cardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                Then_Success(result);
            }

            [TestMethod]
            [ExpectedException(typeof(CardTransactionValidationException))]
            public void Given_A_Reload_When_Invalid_Reload_Amount_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                Given_Reload_ForStarbucks(mockReload, -5.00M, _paymentMethodId, null, null, null);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                //should exception
            }

            [TestMethod]
            [ExpectedException(typeof(CardTransactionValidationException))]
            public void Given_A_Reload_When_Missing_PaymentMethod_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                string paymentMethodId = string.Empty;
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                Given_Reload_ForStarbucks(mockReload, 5.00M, paymentMethodId, null, null, null);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                //should exception
            }

            [TestMethod]
            [ExpectedException(typeof(CardTransactionException))]
            public void Given_A_Reload_When_Invalid_PaymentMethod_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                var differentPaymentMethodId = Starbucks.Platform.Security.Encryption.EncryptCardId(2548);
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                Given_Reload_ForStarbucks(mockReload, 5.00M, differentPaymentMethodId, null, null, null);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                //should exception
            }

            [TestMethod]
            [ExpectedException(typeof(CardTransactionValidationException))]
            public void Given_A_Reload_When_Invalid_UserId_Then_Fail()
            {
                var userId = string.Empty;
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                //should exception
            }

            [TestMethod]
            [ExpectedException(typeof(CardTransactionValidationException))]
            public void Given_A_Reload_When_Invalid_CardId_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                var cardId = string.Empty;
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null);

                var result = When_Reload_A_Card(userId, cardId, mockReload, Cvn, Market, UserMarket);
                //should exception
            }

            [TestMethod]
            [ExpectedException(typeof(CardTransactionValidationException))]
            public void Given_A_Reload_When_Invalid_EmailAddress_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                var emailAddress = string.Empty;

                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, emailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                //should exception
            }

            [TestMethod]
            [ExpectedException(typeof(CardTransactionException))]
            public void Given_A_Reload_When_Invalid_Action_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                var actions = new List<string>() { };

                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                //should exception
            }

            [TestMethod]
            [ExpectedException(typeof(CardTransactionValidationException))]
            public void Given_A_Reload_When_Invalid_Card_Number_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                var cardNumber = string.Empty;
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, cardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                //should exception
            }

            [TestMethod]
            public void Given_A_Reload_When_TempPaymentMethod_And_All_Is_Ok()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD", "US");
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now, true);

                var mockReload = new Mock<IReloadForStarbucksCard>();
                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, Guid.NewGuid().ToString(), "Web", mockReputation);
                Given_PaymentServiceApiClient(PaymentServiceApiClient, fruadCheckSucceeds: true);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                CardProvider.Verify(
                    cp => cp.ClearPaymentMethodSurrogateAndPaypalAccountNumbers(It.IsAny<string>(), It.IsAny<IPaymentMethod>()));

                Then_Success(result);
            }

            [TestMethod]
            [ExpectedException(typeof(Starbucks.Api.Sdk.PaymentService.Common.SecurePaymentException))]
            public void Given_A_Reload_When_CC_And_Auth_SecurePaymentException_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();

                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_PaymentServiceApiClient(PaymentServiceApiClient, fruadCheckSucceeds: true, throwExceptionForAuthAndFraudCheck: true);
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                Then_Success(result);
            }

            [TestMethod]
            [ExpectedException(typeof(Starbucks.Api.Sdk.PaymentService.Common.SecurePaymentException))]
            public void Given_A_Reload_When_CC_And_Auth_SecurePaymentException_NonZero_ReasonCode_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();

                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_PaymentServiceApiClient(PaymentServiceApiClient, fruadCheckSucceeds: true, throwExceptionForAuthAndFraudCheck: true, apiErrorCode: "1");
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                Then_Success(result);
            }

            [TestMethod]
            [ExpectedException(typeof(CardTransactionException))]
            public void Given_A_Reload_When_CC_And_Auth_Exception_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();

                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_PaymentServiceApiClient(PaymentServiceApiClient, fruadCheckSucceeds: true, throwExceptionForAuthAndFraudCheck: true);
                PaymentServiceApiClient.Setup(x => x.Auth(It.IsAny<AuthRequest>())).Throws(new Exception("Dummy"));       // Cheat

                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                Then_Success(result);
            }

            [TestMethod]
            [ExpectedException(typeof(SecurePaymentException))]
            public void Given_A_Reload_When_CC_And_Auth_Deny_With_ReasonCode_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD", "US");

                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, Guid.NewGuid().ToString(), "Web", mockReputation);
                Given_PaymentServiceApiClient(PaymentServiceApiClient, fruadCheckSucceeds: false);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                Then_Success(result);
            }

            [TestMethod]
            public void Given_A_Reload_When_CC_And_VoidRequest_Exception_In_VoidBill_Then_Ok()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00", throwOnVoidRequest: true);
                Given_Merchant_Info(CardIssuerDal, "USD");

                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, null, "Web", mockReputation);

                Given_PaymentServiceApiClient(PaymentServiceApiClient, fruadCheckSucceeds: true, psBillCallResult: PsBillCallResult.Deny);

                try
                {
                    var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                    Assert.Fail();
                }
                catch (Exception ex)
                {
                    Assert.IsInstanceOfType(ex, typeof(CardTransactionException), "Incorrect exception type thrown.");
                    Assert.IsTrue(ex.Message.Equals(CardTransactionErrorResource.CreditCardBillFailedMessage));
                    PaymentServiceApiClient.Verify(x => x.Bill(It.IsAny<BillRequest>()));
                }
            }

            [TestMethod]
            public void Given_A_Reload_When_CC_And_Bill_Timeout_Exception_DoNot_Rollback_VLReload_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction(sectionName: "cardTransactionProviderSettingsSectionDoNotRollbackCardReload");
                Given_Value_Link_Reload(CardIssuerDal, "00", throwOnVoidRequest: false);
                Given_Merchant_Info(CardIssuerDal, "USD");

                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, null, "Web", mockReputation);

                Given_PaymentServiceApiClient(PaymentServiceApiClient, fruadCheckSucceeds: true, psBillCallResult: PsBillCallResult.Throw);

                try
                {
                    var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                    Assert.Fail();
                }
                catch (Exception ex)
                {
                    CardIssuerDal.Verify(x => x.VoidRequest(It.IsAny<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>(), It.IsAny<IMerchantInfo>()), Times.Never());
                    Assert.IsInstanceOfType(ex, typeof(Starbucks.Api.Sdk.PaymentService.Common.SecurePaymentException), "Incorrect exception type thrown.");
                }
            }

            [TestMethod]
            public void Given_A_Reload_When_CC_And_Bill_Timeout_Exception_Rollback_VLReload_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00", throwOnVoidRequest: false);
                Given_Merchant_Info(CardIssuerDal, "USD");

                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, null, "Web", mockReputation);

                Given_PaymentServiceApiClient(PaymentServiceApiClient, fruadCheckSucceeds: true, psBillCallResult: PsBillCallResult.Throw);

                try
                {
                    var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                    Assert.Fail();
                }
                catch (Exception ex)
                {
                    CardIssuerDal.Verify(x => x.VoidRequest(It.IsAny<Starbucks.CardIssuer.Dal.Common.Models.ICardTransaction>(), It.IsAny<IMerchantInfo>()));
                    Assert.IsInstanceOfType(ex, typeof(Starbucks.Api.Sdk.PaymentService.Common.SecurePaymentException), "Incorrect exception type thrown.");
                }
            }

            [TestMethod]
            public void Given_A_Reload_When_CC_And_AccoutProvider_GetAddress_Fails_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD", "US");
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider, throwExceptionOnGetAddress: true);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, Guid.NewGuid().ToString(), "Web", mockReputation);
                Given_PaymentServiceApiClient(PaymentServiceApiClient, fruadCheckSucceeds: true);

                try
                {
                    var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                    Assert.Fail();
                }
                catch (Exception ex)
                {
                    Assert.IsInstanceOfType(ex, typeof(CardTransactionException), "Incorrect exception type thrown.");
                    var ctEx = ex as CardTransactionException;
                    Assert.IsTrue(ctEx.Code == CardTransactionErrorResource.BillingAddressNotFoundCode);
                }
            }

            [TestMethod]
            public void Given_A_Reload_When_CC_And_ValueLink_Reload_Fails_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, responseCode: "00", throwOnReload: true);
                Given_Merchant_Info(CardIssuerDal, "USD", "US");
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, Guid.NewGuid().ToString(), "Web", mockReputation);
                Given_PaymentServiceApiClient(PaymentServiceApiClient, fruadCheckSucceeds: true);

                try
                {
                    var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                    Assert.Fail();
                }
                catch (Exception ex)
                {
                    Assert.IsInstanceOfType(ex, typeof(CardIssuerException), "Incorrect exception type thrown.");
                    var ciEx = ex as CardIssuerException;
                    Assert.IsTrue(ciEx.Code == CardIssuerErrorResource.ValueLinkHostDownCode);
                }
            }
        }


        [TestClass]
        public class ReloadPayPal : CardTransactionClass
        {
            readonly string _cardId = Starbucks.Platform.Security.Encryption.EncryptCardId(12345);
            private const string CardClass = "69";
            private const string CardNumber = "7111111111111111";
            private const string Pin = "12345678";
            readonly List<string> _actions = new List<string>() { "Reload" };
            private const string EmailAddress = "test@test.com";
            private const string Cvn = "123";
            private const string Market = "US";
            private const string UserMarket = "US";
            readonly string _paymentMethodId = Starbucks.Platform.Security.Encryption.EncryptCardId(98764);
            private const string PaymentType = "PayPal";
            private const string AccountNumber = "123";


            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public void Given_A_Reload_When_Reload_Fails_Throw_Exception_On_CardIssuer()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Secure_PayPal_Client(SecurePayPalPaymentClient, PaymentStatusCode.Completed,
                                           PendingStatusCode.Authorization, true);
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, null, "Web", mockReputation);
                Given_PaymentServiceApiClient(PaymentServiceApiClient, true);
                CardIssuerDal.Setup(x => x.Reload(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<decimal>())).Throws(new Exception("Reload exception."));

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                Then_Success(result);
            }

            [TestMethod]
            public void Given_A_Reload_When_Reload_Fails_Return_Invalid_Response_Code_On_CardIssuer()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Secure_PayPal_Client(SecurePayPalPaymentClient, PaymentStatusCode.Completed,
                                           PendingStatusCode.Authorization, true);
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, null, "Web", mockReputation);
                Given_PaymentServiceApiClient(PaymentServiceApiClient, true);

                var cardTransaction = new Mock<CardTransaction>();
                cardTransaction.Object.RequestCode = "1";
                CardIssuerDal.Setup(x => x.Reload(It.IsAny<IMerchantInfo>(), It.IsAny<string>(), It.IsAny<decimal>())).Returns(cardTransaction.Object);

                try
                {
                    var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                    Task.Delay(1000);
                    Assert.Fail();
                }
                catch (Exception ex)
                {
                    Assert.IsInstanceOfType(ex, typeof(CardIssuerException), "Incorrect exception type thrown.");
                    OrderManagementProvider.Verify(x => x.SaveFailedOrder(It.IsAny<IFailedOrder>()), Times.Once());
                }
            }

            [TestMethod]
            public async Task Given_A_Reload_When_Reload_Fails_Return_Invalid_Paypal_Bill_Response_Code_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Secure_PayPal_Client(SecurePayPalPaymentClient, PaymentStatusCode.Completed,
                                           PendingStatusCode.Authorization, billingAgreementResult: true);
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, defaultCard: true, totalCardsRegistered: 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, isFraudChecked: true,
                                      fraudCheckDate: DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, paymentMethodId: _paymentMethodId, token: null, paymentTokenType: null,
                    address: null, sessionId: null, platform: "Web", reputation: mockReputation);
                Given_PaymentServiceApiClient(PaymentServiceApiClient, fruadCheckSucceeds: true);

                var cardTransaction = new Mock<CardTransaction>();

                var billResult = new Mock<IBillResponse>();
                billResult.Object.PaymentStatusCode = PaymentStatusCode.Failed;
                SecurePayPalPaymentClient.Setup(x => x.Bill(It.IsAny<Starbucks.CardTransaction.Provider.PayPalModels.BillRequest>())).Returns(billResult.Object);

                try
                {
                    var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                    Then_Success(result);

                    // Delay long enough for the Bill thread to complete so we can verify that it failed correctly.
                    var wait = Task.Delay(2000);
                    await wait;
                }
                catch (Exception ex)
                {
                    Assert.IsInstanceOfType(ex, typeof(CardTransactionException), "Incorrect exception type thrown.");
                }
                OrderManagementProvider.Verify(x => x.SaveFailedOrder(It.IsAny<IFailedOrder>()), Times.Once());
            }

            [TestMethod]
            public void Given_A_Reload_When_PayPal_And_All_Is_Ok_Reload_Then_Ok()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Secure_PayPal_Client(SecurePayPalPaymentClient, PaymentStatusCode.Completed,
                                           PendingStatusCode.Authorization, true);
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, null, "Web", mockReputation);
                Given_PaymentServiceApiClient(PaymentServiceApiClient, true);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                Then_Success(result);
            }

            [TestMethod]
            public void Given_A_Reload_When_PayPal_And_All_Is_Ok_No_Risk_Data_Then_Ok()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Secure_PayPal_Client(SecurePayPalPaymentClient, PaymentStatusCode.Completed,
                                           PendingStatusCode.Authorization, true);
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, null, "Web");
                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                Then_Success(result);
            }

            [TestMethod]
            public void Given_A_Reload_When_PayPal_And_On_Authorize_Fail_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Secure_PayPal_Client(SecurePayPalPaymentClient, PaymentStatusCode.Completed,
                                           PendingStatusCode.Verify, true);
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();

                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, null, "Web", mockReputation);

                Given_PaymentServiceApiClient(PaymentServiceApiClient, false, true);

                try
                {
                    var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                    Assert.Fail();
                }
                catch (Exception ex)
                {
                    if (ex.GetType() == typeof(AggregateException))
                    {
                        var agEx = ex as AggregateException;
                        ex = agEx.InnerExceptions.FirstOrDefault();
                    }

                    Assert.IsInstanceOfType(ex, typeof(CardTransactionException), "Incorrect exception type thrown.");
                    Assert.IsTrue(ex.Message.Equals(CardTransactionErrorResource.CannotAuthorizePayPalMessage));
                    OrderManagementProvider.Verify(x => x.SaveFailedOrder(It.IsAny<IFailedOrder>()), Times.Once());
                }
            }


            [TestMethod]
            public void Given_A_Reload_When_PayPal_And_Auth_Returns_PaymentReview_Then_Ok()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Secure_PayPal_Client(SecurePayPalPaymentClient, PaymentStatusCode.Completed,
                                           PendingStatusCode.PaymentReview, true);
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();

                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, null, "Web", mockReputation);

                Given_PaymentServiceApiClient(PaymentServiceApiClient, false, true);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                Then_Success(result);
            }

            [TestMethod]
            public void Given_A_Reload_When_PayPal_And_Authorize_With_Exception_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Secure_PayPal_Client(SecurePayPalPaymentClient, PaymentStatusCode.Completed,
                                           PendingStatusCode.PaymentReview, true, true);
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();

                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, null, "Web", mockReputation);

                Given_PaymentServiceApiClient(PaymentServiceApiClient, false, true);

                try
                {
                    var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                    Assert.Fail();
                }
                catch (Exception ex)
                {
                    if (ex.GetType() == typeof(AggregateException))
                    {
                        var agEx = ex as AggregateException;
                        ex = agEx.InnerExceptions.FirstOrDefault();
                    }

                    Assert.IsInstanceOfType(ex, typeof(SecurePayPalPaymentException), "Did not catch correct exception type.");
                    Assert.IsTrue(ex.Message.Equals(SecurePayPalPaymentErrorResource.AccountClosedMessage));

                    // Make sure we have not made call to report this failure to Accertify.
                    PaymentServiceApiClient.Verify(x => x.ReportPayPalAuthFailure(It.IsAny<FraudCheckRequest>()), Times.Never());
                }
            }

            [TestMethod]
            public void Given_A_Reload_When_PayPal_Auth_Fails_And_UpdateBillingAgreement_Exception_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Secure_PayPal_Client(SecurePayPalPaymentClient, PaymentStatusCode.Completed,
                    PendingStatusCode.Other, billingAgreementResult: true, throwOnUpdateBillingAgreement: true);
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();

                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, null, "Web", mockReputation);

                Given_PaymentServiceApiClient(PaymentServiceApiClient, false, true);

                try
                {
                    var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                    Assert.Fail();
                }
                catch (Exception ex)
                {
                    if (ex.GetType() == typeof(AggregateException))
                    {
                        var agEx = ex as AggregateException;
                        ex = agEx.InnerExceptions.FirstOrDefault();
                    }
                    Assert.IsInstanceOfType(ex, typeof(CardTransactionException), "Did not catch correct exception type.");
                    Assert.IsTrue(ex.Message.Equals(CardTransactionErrorResource.CannotAuthorizePayPalMessage));

                    // Make sure we have called PayPal call to get billing agreement details.
                    SecurePayPalPaymentClient.Verify(x => x.UpdateBillingAgreement(It.IsAny<IBillingAgreementUdpateRequest>()), Times.Once());

                    // Make sure we have called to report this failure to Accertify.
                    PaymentServiceApiClient.Verify(x => x.ReportPayPalAuthFailure(It.IsAny<FraudCheckRequest>()), Times.Once());
                }
            }

            [TestMethod]
            public void Given_A_Reload_When_PayPal_And_FraudCheck_Fails_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Secure_PayPal_Client(SecurePayPalPaymentClient, PaymentStatusCode.Completed,
                                           PendingStatusCode.Authorization, true);
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();

                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, null, "Web", mockReputation);

                Given_PaymentServiceApiClient(PaymentServiceApiClient, false);

                try
                {
                    var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                    Assert.Fail();
                }
                catch (Exception ex)
                {
                    Assert.IsInstanceOfType(ex, typeof(CardTransactionException), "Incorrect exception type thrown.");
                    Assert.IsTrue(ex.Message.Equals(CardTransactionErrorResource.FraudCheckFailedMessage));
                }
            }

            [TestMethod]
            public void Given_A_Reload_When_PayPal_And_Auth_Deny_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Secure_PayPal_Client(SecurePayPalPaymentClient, PaymentStatusCode.Completed,
                                           PendingStatusCode.Other, false);
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();

                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, null, "Web", mockReputation);

                Given_PaymentServiceApiClient(PaymentServiceApiClient, false);

                try
                {
                    var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                    Assert.Fail();
                }
                catch (Exception ex)
                {
                    Assert.IsInstanceOfType(ex, typeof(CardTransactionException), "Incorrect exception type thrown.");
                    // Make sure that we called method to report the Auth failure to Accertify.
                    PaymentServiceApiClient.Verify(x => x.ReportPayPalAuthFailure(It.IsAny<FraudCheckRequest>()), Times.Once());
                }
            }

            [TestMethod]

            public void Given_A_Reload_When_PayPal_And_FraudCheck_Throws_Then_Success()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Secure_PayPal_Client(SecurePayPalPaymentClient, PaymentStatusCode.Completed,
                                           PendingStatusCode.Authorization, true);
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress, "Reject");
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();

                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, null, "Web", mockReputation);

                Given_PaymentServiceApiClient(PaymentServiceApiClient, false, true);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                Then_Success(result);
            }

            [TestMethod]
            public void Given_A_Reload_When_PayPal_And_Billing_Agreement_Missing_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                string accountNumber = null;

                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Secure_PayPal_Client(SecurePayPalPaymentClient, PaymentStatusCode.Completed,
                                           PendingStatusCode.Authorization, false);
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, accountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();

                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, null, "Web", mockReputation);

                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null);
                Given_PaymentServiceApiClient(PaymentServiceApiClient, true, true);
                try
                {
                    var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                    Task.Delay(1000);
                    Assert.Fail();
                }
                catch (Exception ex)
                {
                    if (ex.GetType() == typeof(AggregateException))
                    {
                        var agEx = ex as AggregateException;
                        ex = agEx.InnerExceptions.FirstOrDefault();
                    }
                    Assert.IsInstanceOfType(ex, typeof(CardTransactionValidationException), "Incorrect exception type thrown.");
                    Assert.IsTrue(ex.Message.Equals(CardTransactionValidationErrorResource.InvalidBillingAgreementIdMessage));
                }
            }

            [TestMethod]
            public async Task Given_A_Reload_With_PayPal_Bill_Exception_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Secure_PayPal_Client(SecurePayPalPaymentClient, PaymentStatusCode.Completed,
                    PendingStatusCode.Authorization, true, throwOnBill: true);
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, null, "Web", mockReputation);
                Given_PaymentServiceApiClient(PaymentServiceApiClient, true);
                Given_An_Order_Manager(OrderManagementProvider);

                try
                {
                    var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);

                    // Delay long enough for the Bill thread to complete so we can verify that it failed correctly.
                    var wait = Task.Delay(2000);
                    await wait;
                }
                catch (Exception ex)
                {
                    Assert.IsInstanceOfType(ex, typeof(Exception), "Incorrect exception type thrown.");
                    Assert.IsTrue(ex.Message.Equals(PayPalBillExceptionMessage));
                }
                OrderManagementProvider.Verify(x => x.SaveFailedOrder(It.IsAny<IFailedOrder>()), Times.Once());
            }

            [TestMethod]
            public async Task Given_A_Reload_When_PayPal_When_PayPal_Bill_And_Void_Exceptions_Then_Ok()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Secure_PayPal_Client(SecurePayPalPaymentClient, PaymentStatusCode.Completed,
                    PendingStatusCode.Authorization, true, throwOnBill: true);
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, null, "Web", mockReputation);
                Given_PaymentServiceApiClient(PaymentServiceApiClient, true);

                try
                {
                    var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                    // Delay long enough for the Bill thread to complete so we can verify that it failed correctly.
                    var wait = Task.Delay(2000);
                    await wait;
                }
                catch (Exception ex)
                {
                    Assert.IsInstanceOfType(ex, typeof(Exception), "Incorrect exception type thrown.");
                    Assert.IsTrue(ex.Message.Equals(PayPalBillExceptionMessage));
                }
                SecurePayPalPaymentClient.Verify(x => x.Void(It.IsAny<IVoidRequest>()), Times.Once());
            }

            [TestMethod]
            public void Given_A_Reload_When_PayPal_And_Block_Reloads_With_PayPal_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction(sectionName: "cardTransactionProviderSettingsSectionBlockPayPalReloads");
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Secure_PayPal_Client(SecurePayPalPaymentClient, PaymentStatusCode.Completed,
                                           PendingStatusCode.Authorization, true);
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, null, "Web", mockReputation);
                Given_PaymentServiceApiClient(PaymentServiceApiClient, true);

                try
                {
                    var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                    Assert.Fail();
                }
                catch (Exception ex)
                {
                    Assert.IsInstanceOfType(ex, typeof(Exception), "Incorrect exception type thrown.");
                    Assert.IsTrue(ex.Message.Equals("_cardTransactionProviderSettingsSection.BlockPayPalReloads = true. Skipping PayPal reload."));
                }
            }

            [TestMethod]

            public void Given_A_Reload_When_PayPal_And_Ignore_PayPal_Reload_FraudCheck_Then_Success()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction(sectionName: "cardTransactionProviderSettingsSectionIgnorePayPalReloadsFraudCheck");
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Secure_PayPal_Client(SecurePayPalPaymentClient, PaymentStatusCode.Completed,
                                           PendingStatusCode.Authorization, true);
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                Given_Reload_ForStarbucks(mockReload, 5.00M, _paymentMethodId, null, null, null, null, "Web", mockReputation);
                Given_PaymentServiceApiClient(PaymentServiceApiClient, true);

                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                Task.Delay(2000);       // or System.Threading.Thread.Sleep(2000);
                Then_Success(result);
            }
        }

        [TestClass]
        public class ReloadApplePay : CardTransactionClass
        {
            readonly string _cardId = Starbucks.Platform.Security.Encryption.EncryptCardId(12345);
            private const string CardClass = "69";
            private const string CardNumber = "7111111111111111";
            private const string Pin = "12345678";
            readonly List<string> _actions = new List<string>() { "Reload" };
            private const string EmailAddress = "test@test.com";
            private const string Cvn = "123";
            private const string Market = "US";
            private const string UserMarket = "US";
            readonly string _paymentMethodId = Starbucks.Platform.Security.Encryption.EncryptCardId(98764);
            private const string PaymentType = "ApplePay";
            private const string AccountNumber = "123";
            readonly string _token = Guid.NewGuid().ToString();
            private const PaymentTokenType TokenType = PaymentTokenType.ApplePay;

            [TestMethod]
            public void Given_A_Reload_When_Apple_Pay_And_All_Is_Ok_Reload_Then_Ok()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_PaymentServiceApiClient(PaymentServiceApiClient, true);
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockAddress = new Mock<IAddress>();
                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                Given_Reload_ForStarbucks(mockReload, 5.00M, null, _token, TokenType, mockAddress.Object, Guid.NewGuid().ToString(), "Web", mockReputation);
                var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);

                Then_Success(result);
            }

            [TestMethod]
            public void Given_A_Reload_When_Apple_Pay_And_Missing_Billing_Address_Failure()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Reload(CardIssuerDal, "00");
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_PaymentServiceApiClient(PaymentServiceApiClient, true);
                Given_Settings(SettingsProvider);
                Given_Fraud_Check_Data(CardProvider, userId, _cardId,
                                      string.Empty, true, 5);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_Address(AccountProvider);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                Given_A_PaymentMethod(PaymentMethodProvider, _paymentMethodId, PaymentType, AccountNumber, true,
                                      DateTime.Now);
                var mockReputation = new Mock<IRisk>();
                var mockIovation = new Mock<IReputation>();
                Given_A_Reputation(mockReputation, "Web", mockIovation);
                var mockReload = new Mock<IReloadForStarbucksCard>();
                Given_Reload_ForStarbucks(mockReload, 5.00M, null, _token, TokenType, address: null, sessionId: Guid.NewGuid().ToString(), platform: "Web", reputation: mockReputation);

                try
                {
                    var result = When_Reload_A_Card(userId, _cardId, mockReload, Cvn, Market, UserMarket);
                    Assert.Fail();
                }
                catch (Exception ex)
                {
                    Assert.IsInstanceOfType(ex, typeof(Exception), "Incorrect exception type thrown.");
                    Assert.IsTrue(ex.Message.Equals(CardTransactionErrorResource.BillingAddressNotFoundMessage));
                }
            }

        }

        [TestClass]
        public class Tip : CardTransactionClass
        {
            readonly string _cardId = Starbucks.Platform.Security.Encryption.EncryptCardId(12345);
            private const string CardClass = "69";
            private const string CardNumber = "7111111111111111";
            private const string Pin = "12345678";
            readonly List<string> _actions = new List<string>() { "Reload" };
            private const string EmailAddress = "test@test.com";
            private const decimal Amount = 1.50M;
            private const long LocalTransactionId = 1234L;
            readonly DateTime _localTransactionTime = DateTime.Now.AddMinutes(-10);
            private const string OriginalMerchantId = "5678";
            private const string OriginalStoreId = "567";
            private const string OriginalTerminalId = "1";
            private const string OriginalTransactionCurrency = "USD";
            private const long OriginalTransactionId = 133232323L;
            private const string UserMarket = "US";


            protected void Given_Process_Tip(Mock<IProcessTip> tip, decimal amount, string cardId,
                                             string localTransactionId, DateTime localTransactionTime,
                                             string originalMerchantId, string originalStoreId,
                                             string originalTerminalId, string originalTransactionCurrency,
                                             long originalTransactionId, string userId, string userMarket)
            {
                tip.SetupProperty(p => p.Amount, amount);
                tip.SetupProperty(p => p.CardId, cardId);
                tip.SetupProperty(p => p.LocalTransactionId, localTransactionId);
                tip.SetupProperty(p => p.LocalTransactionTime, localTransactionTime);
                tip.SetupProperty(p => p.OrginalMerchantId, originalMerchantId);
                tip.SetupProperty(p => p.OriginalStoreId, originalStoreId);
                tip.SetupProperty(p => p.OriginalTerminalId, originalTerminalId);
                tip.SetupProperty(p => p.OriginalTransactionCurrency, originalTransactionCurrency);
                tip.SetupProperty(p => p.OriginalTransactionId, originalTransactionId);
                tip.SetupProperty(p => p.UserId, userId);
                tip.SetupProperty(p => p.UserMarket, userMarket);
            }

            protected ICardTransaction When_Tip(IProcessTip processTip)
            {
                return CardTransactionProvider.Tip(processTip);
            }

            protected void Given_Tip_Provider(string userId, long originalTransactionId, TipStatus processingChangeResult, TipStatus insertedChangeResult, TipStatus processedChangeResult)
            {
                TippingProvider.Setup(p => p.UpdateTipStatus(userId, originalTransactionId, TipStatus.Processing))
                               .Returns(processingChangeResult);

                TippingProvider.Setup(p => p.UpdateTipStatus(userId, originalTransactionId, TipStatus.Inserted))
                               .Returns(insertedChangeResult);

                TippingProvider.Setup(p => p.UpdateTipStatus(userId, originalTransactionId, TipStatus.Processed))
                               .Returns(insertedChangeResult);
            }

            [TestMethod]
            public void Given_A_CardTransaction_When_Tip_And_All_Is_Good_Then_Success()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Tip(CardIssuerDal, CardNumber, Amount, _localTransactionTime, LocalTransactionId, "00", true);
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Settings(SettingsProvider);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                var mockTip = new Mock<IProcessTip>();

                Given_Process_Tip(mockTip, Amount, _cardId, LocalTransactionId.ToString(CultureInfo.InvariantCulture), _localTransactionTime, OriginalMerchantId,
                                  OriginalStoreId, OriginalTerminalId, OriginalTransactionCurrency,
                                  OriginalTransactionId, userId, UserMarket);

                Given_Tip_Provider(userId, OriginalTransactionId, TipStatus.Processing, TipStatus.Inserted, TipStatus.Processed);
                var tipResult = When_Tip(mockTip.Object);

                Then_Success(tipResult);
            }

            [TestMethod]
            [ExpectedException(typeof(CardTransactionException))]
            public void Given_A_CardTransaction_When_Tip_Invalid_Processing_Status_Then_Fail()
            {
                var userId = Guid.NewGuid().ToString();
                Given_That_I_Have_A_Card_Transaction();
                Given_Value_Link_Tip(CardIssuerDal, CardNumber, Amount, _localTransactionTime, LocalTransactionId, "00", true);
                Given_Merchant_Info(CardIssuerDal, "USD");
                Given_Settings(SettingsProvider);
                Given_Log_Counter_Manager(LogCounterManager);
                Given_A_Card(CardProvider, userId, _cardId, CardClass, CardNumber, Pin, _actions);
                Given_A_User(AccountProvider, userId, EmailAddress);
                var mockTip = new Mock<IProcessTip>();

                Given_Process_Tip(mockTip, Amount, _cardId, LocalTransactionId.ToString(CultureInfo.InvariantCulture), _localTransactionTime, OriginalMerchantId,
                                  OriginalStoreId, OriginalTerminalId, OriginalTransactionCurrency,
                                  OriginalTransactionId, userId, UserMarket);

                Given_Tip_Provider(userId, OriginalTransactionId, TipStatus.Processed, TipStatus.Inserted, TipStatus.Processed);
                var tipResult = When_Tip(mockTip.Object);
            }
        }
    }
}
