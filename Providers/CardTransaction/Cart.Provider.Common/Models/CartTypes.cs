﻿namespace Starbucks.Cart.Provider.Common.Models
{
    public enum CartTypes
    {
        eCard,
        Custom,
        BAC,
        ePromoCard
    }
}