﻿namespace Starbucks.Cart.Provider.Common.Models
{
    public class CartConstants
    {
        /// <summary>
        /// The key for the product option representing the amount
        /// on a Starbucks Card.
        /// </summary>
        public const string AmountOnCardProductOption = "AmountOnCard";

        /// <summary>
        /// The key for the product option representing the pre-defined amount
        /// on a Starbucks Card.
        /// </summary>
        public const string DefinedAmountOnCardProductOption = "DefinedAmountOnCard";

        /// <summary>
        /// The key for the product option representing the currency
        /// for a Starbucks Card.
        /// </summary>
        public const string CurrencyProductOption = "Currency";

        /// <summary>
        /// Recipient name for eCard
        /// </summary>
        public const string RecipientNameProductOption = "recipientName";

        /// <summary>
        /// Recipient email for eCard
        /// </summary>
        public const string RecipientEmailProductOption = "recipientEmail";

        /// <summary>
        /// Sender Name for eCard
        /// </summary>
        public const string SenderNameProductOption = "senderName";

        /// <summary>
        /// Sender Email for eCard
        /// </summary>
        public const string SenderEmailProductOption = "senderEmail";

        /// <summary>
        /// Message for eCard
        /// </summary>
        public const string MessageProductOption = "message";

        /// <summary>
        /// Message for eCard
        /// </summary>
        public const string SendMyselfProductOption = "sendMyself";

        /// <summary>
        /// The key for the product option representing the custom card
        /// image ID for a customized card.
        /// </summary>
        public const string CustomCardImageIdProductOption = "CustomCardImageId";

        /// <summary>
        /// The key for the product option representing the custom card
        /// theme for a customized card.
        /// </summary>
        public const string CustomCardThemeProductOption = "CustomCardTheme";

        /// <summary>
        /// The key for the product option representing the greeting card
        /// message for a card order.
        /// </summary>
        public const string GreetingCardMessageProductOption = "GreetingCardMessage";

        /// <summary>
        /// The key for the product option representing the greeting card
        /// message for a card order.
        /// </summary>
        public const string ECardDeliveryDateProductOption = "ECardDeliveryDate";

        /// <summary>
        /// The key for the product option representing the greeting card
        /// message for a card order.
        /// </summary>
        public const string PercentDiscount = "PercentDiscount";

        /// <summary>
        /// The facebook id for delivery method.
        /// </summary>
        public const string ECardRecipientFacebookId = "RecipientFacebookId";

        /// <summary>
        /// The facebook id for delivery method.
        /// </summary>
        public const string ECardSenderFacebookId = "SenderFacebookId";

        /// <summary>
        /// The facebook profile image url.
        /// </summary>
        public const string ECardFacebookProfileImageUrl = "FacebookProfileImageUrl";

        /// <summary>
        /// The facebook profile name.
        /// </summary>
        public const string ECardFacebookProfileFirstName = "FacebookProfileFirstName";

        /// <summary>
        /// The facebook profile name.
        /// </summary>
        public const string ECardFacebookProfileLastName = "FacebookProfileLastName";

        /// <summary>
        /// The facebook profile name.
        /// </summary>
        public const string ECardSenderFacebookProfileLastName = "FacebookSenderProfileLastName";

        /// <summary>
        /// The facebook profile image url.
        /// </summary>
        public const string ECardSenderFacebookProfileImageUrl = "FacebookSenderProfileImageUrl";

        /// <summary>
        /// The facebook profile name.
        /// </summary>
        public const string ECardSenderFacebookProfileFirstName = "FacebookSenderProfileFirstName";

        /// <summary>
        /// The facebook id for delivery method.
        /// </summary>
        public const string ECardReloadCardId = "CardId";

        /// <summary>
        /// The facebook id for delivery method.
        /// </summary>
        public const string ECardDeliveryType = "DeliveryType";

        /// <summary>
        /// The AssociatedOrderId for mutliple egift orders.
        /// </summary>
        public const string AssociatedOrderId = "AssociatedOrderId";

        /// <summary>
        /// Get the token value from the cookie on the client machine.
        /// </summary>
        public const string FacebookToken = "FacebookToken";

        /// <summary>
        /// Gets the pretty product name for the url.
        /// </summary>
        public const string Href = "HRef";
    }
}