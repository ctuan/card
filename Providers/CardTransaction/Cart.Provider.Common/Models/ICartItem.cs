﻿using System.Collections.Generic;

namespace Starbucks.Cart.Provider.Common.Models
{
    public interface ICartItem
    {
        string ProductId { get; set; }
        IDictionary<string, string> ProductOptions { get; set; }
        int Quantity { get; set; }
        IEnumerable<ICartItem> SubItems { get; set; }
        string Id { get; set; }
        string Locale { get; set; }
        decimal DiscountMultiplier { get; }
        string Site { get; set; }
    }
}
