﻿using System;
using System.Collections.Generic;

namespace Starbucks.Cart.Provider.Common.Models
{
    public interface ICart
    {
        int CartId { get; set; }
        Guid Token { get; set; }
        DateTime DateCreated { get; set; }
        IEnumerable<ICartItem> Items { get; set; }
        decimal PartnerDiscount { get; set; }
        CartTypes CartType { get; set; }
    }
}
