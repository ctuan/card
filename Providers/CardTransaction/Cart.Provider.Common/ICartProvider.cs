﻿using System;
using System.Collections.Generic;
using Starbucks.Cart.Provider.Common.Models;

namespace Starbucks.Cart.Provider.Common
{
    public interface ICartProvider
    {        
        ICart GetCart(Guid token);        
        string GetCartCurrency(Guid token);        
        decimal GetCartTotal(Guid token);        
        ICart SaveCart(Guid token, IEnumerable<ICartItem> items);
        ICart SaveCartWithDiscounts(Guid token, IEnumerable<ICartItem> items, bool applyDiscount);
        void SaveRawCart(Guid token, IEnumerable<ICartItem> items);
    }
}
