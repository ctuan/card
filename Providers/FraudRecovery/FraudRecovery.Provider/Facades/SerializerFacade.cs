﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Xml;

namespace Starbucks.FraudRecovery.Provider.Facades
{

    public static class SerializerHelper
    {
        public static string Serialize<T>(this XmlSerializer serializer, T obj)
        {
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings xws = new XmlWriterSettings();
            xws.OmitXmlDeclaration = true;
			XmlSerializerNamespaces ns = new XmlSerializerNamespaces(
				   new[] { XmlQualifiedName.Empty });

            using (XmlWriter writer = XmlWriter.Create(sb, xws))
            {
                serializer.Serialize(writer, obj, ns);
            }

            return sb.ToString();
        }

        public static T Deserialize<T>(this XmlSerializer serializer, TextReader tr)
            where T : class
        {
            object obj = serializer.Deserialize(tr);
            T t = obj as T;
            return t;
        }

        public static T Deserialize<T>(this XmlSerializer serializer, Stream stream)
            where T : class
        {
            T t = serializer.Deserialize(stream) as T;
            return t;
        }

        public static T Deserialize<T>(this XmlSerializer serializer, string strXml)
             where T : class
        {
            StringReader sr = new StringReader(strXml);
            T t = serializer.Deserialize<T>(sr);
            sr.Close();
            return t;
        }
        
        public static XmlSerializer GetSerializer(Type t)
        {
            return GetSerializer(t, null);
        }
        public static XmlSerializer GetSerializer(object obj)
        {
            return GetSerializer(obj.GetType());
        }

        public static XmlSerializer GetSerializer(Type t, string strRootName)
        {
            string strKey = t.AssemblyQualifiedName;
            if (!string.IsNullOrEmpty(strRootName))
            {
                strKey += "(" + strRootName + ")";
            }

            // handle multiple thread case
            lock (g_dictSerializer)
            {
                if (g_dictSerializer.ContainsKey(strKey))
                {
                    return g_dictSerializer[strKey];
                }

                XmlSerializer serializer = null;
                if (string.IsNullOrEmpty(strRootName))
                {
                    serializer = new XmlSerializer(t);
                }
                else
                {
                    XmlRootAttribute attrRoot = new XmlRootAttribute(strRootName);
                    serializer = new XmlSerializer(t, attrRoot);
                }
                g_dictSerializer.Add(strKey, serializer);
                return serializer;
            }
        }

        private static Dictionary<string, XmlSerializer> g_dictSerializer
            = new Dictionary<string, XmlSerializer>();

    }

    public class SerializerFacade<T> where T : class, new()
    {
        public SerializerFacade()
        {
        }

        public void Serialize(Stream stream, T t)
        {
            m_objeSerializer.Serialize(stream, t);
        }

        public void Serialize(TextWriter tw, T t)
        {
            m_objeSerializer.Serialize(tw, t);
        }

        public string Serialize(T t)
        {
            return m_objeSerializer.Serialize(t);
        }


        public T Deserialize(Stream stream)
        {
            T t = m_objeSerializer.Deserialize(stream) as T;
            return t;
        }

        public T Deserialize(TextReader tr)
        {
            T t = m_objeSerializer.Deserialize<T>(tr);
            return t;
        }

        public T Deserialize (string strXml)
        {
            T t = m_objeSerializer.Deserialize<T>(strXml);
            return t;
        }
        
        private XmlSerializer m_objeSerializer = new XmlSerializer(typeof(T));
    }
}
