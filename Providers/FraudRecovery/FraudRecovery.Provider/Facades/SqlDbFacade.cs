﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;

namespace Starbucks.FraudRecovery.Provider.Facades
{
	public class SqlDbFacade : ISqlDbFacade
	{
		public SqlDbFacade(ILoggingUtility logging)
		{
			logging.EnsureObjectNotNull("logging is required");

			this.Logging = logging;
		}

		protected ILoggingUtility Logging { get; private set; }

		public virtual void ExecuteNonQuery(string sprocName, string connectStringName, IList<SqlParameter> parameters)
		{
			try
			{
				sprocName.EnsureStringNotNullOrEmpty("sproc Name is required.   Missing from the application configuration");
				connectStringName.EnsureStringNotNullOrEmpty("connectStringName Name is required.   Missing from the application configuration");

				string connectonString = GetConnectionString(connectStringName);
				using (var connection = new SqlConnection(connectonString))
				{
					using (var command = GetCommand(sprocName, connection))
					{
						connection.Open();
						if (parameters != null && parameters.Any())
						{
							command.Parameters.AddRange(parameters.ToArray());
						}
						command.ExecuteNonQuery();
					}
				}
			}
			catch (Exception ex)
			{
				this.Logging.Log(LogType.Error, "FraudRecoveryAuditorBase::ExecuteStoreProcedure", "Execution of SQL store procedure is failed",
								"DB or configuration issue",
								new Dictionary<string, object>()
					                {
						                {"connectionStringName", connectStringName?? "<NULL>"},
						                {"store procedure name", sprocName?? "<NULL>"},
						                {"exception", ex}
					                }
					);
			}
		}

		protected virtual SqlCommand GetCommand(string sproc, SqlConnection connection)
		{
			return new SqlCommand(sproc, connection) { CommandType = CommandType.StoredProcedure };
		}


		protected virtual string GetConnectionString(string connectionStringName)
		{
			var connectionStringObj = ConfigurationManager.ConnectionStrings[connectionStringName];
			connectionStringObj.EnsureObjectNotNull("connection string is missing from Application ConnectionString configuration section");

			string result = connectionStringObj.ConnectionString;
			result.EnsureStringNotNullOrEmpty("connection string is missing from Application ConnectionString configuration section");
			return result;
		}
	}
}
