﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.Api.Sdk.PaymentService;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;
using Starbucks.FraudRecovery.Dal.Common;
using Starbucks.FraudRecovery.Provider.Auditing;
using Starbucks.FraudRecovery.Provider.Facades;

namespace Starbucks.FraudRecovery.Provider
{
	public class DependencyFactory : IDependencyFactory
	{
		public DependencyFactory(ICardIssuerDal cardIssuer, IFraudRecoveryDal fraudRecoveryDal,
		                         IPaymentServiceApiClient paymentServiceApiClient,
		                         IFraudRecoveyConfiguration configuration,
		                         ILoggingUtility logging)
			: this(cardIssuer, fraudRecoveryDal, paymentServiceApiClient, new SqlDbFacade(logging), configuration, logging)
		{
		}

		public DependencyFactory(ICardIssuerDal cardIssuer, IFraudRecoveryDal fraudRecoveryDal,
		                         IPaymentServiceApiClient paymentServiceApiClient, ISqlDbFacade sqlFacade, IFraudRecoveyConfiguration configuration,
		                         ILoggingUtility logging)
		{
			logging.EnsureObjectNotNull("logging is required");
			fraudRecoveryDal.EnsureObjectNotNull("FraudRecoveryDal is required");
			cardIssuer.EnsureObjectNotNull("cardIssuer is required");
			sqlFacade.EnsureObjectNotNull("sqlFacade is required");
			configuration.EnsureObjectNotNull("configuration is required");
			paymentServiceApiClient.EnsureObjectNotNull("paymentServiceApiClient is required");

			this.Logging = logging;
			this.FraudRecoveryDal = fraudRecoveryDal;
			this.CardIssuer = cardIssuer;
			this.SqlDbFacade = sqlFacade;
			this.Configuration = configuration;
			this.PaymentServiceApiClient = paymentServiceApiClient;
		}

		public IFraudRecoveryDal FraudRecoveryDal { get; private set; }
		public ICardIssuerDal CardIssuer { get; private set; }
		public ILoggingUtility Logging { get; private set; }
		public IFraudRecoveyConfiguration Configuration { get; private set; }
		public IPaymentServiceApiClient PaymentServiceApiClient { get; private set; }
		public ISqlDbFacade SqlDbFacade { get; private set; }

		public IFraudRecoveryAuditor<TResult> GetSvcCardActionAuditor<TResult>() where TResult : OperationResponseWithResult<SvcCardOperationResult>
		{
			return new SvcCardActionAuditor<TResult>(this.SqlDbFacade, this.Configuration, this.Logging);
		}

		public IFraudRecoveryAuditor<TResult> GetTransactionWithCreditCardActionAuditor<TResult>() where TResult : OperationResponseWithResult<CreditCardOperationResult>
		{
            return new TransactionWithCreditAuditor<TResult>(this.SqlDbFacade, this.Configuration, this.Logging);
		}

		public IFraudRecoveryAuditor<TResult> GetTransactionWithSvcCardActionAuditor<TResult, TItemResult>()
			where TItemResult : OperationResponseWithResult<SvcCardOperationResult>, new()
			where TResult : OperationResponseCollectionResult<TItemResult>
		{
			return new TransactionWithSvcCardAuditor<TResult, TItemResult>(this.SqlDbFacade, this.Configuration, this.Logging);
		}
	}
}
