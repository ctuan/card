﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;

namespace Starbucks.FraudRecovery.Provider.Auditing
{
	public class TransactionWithSvcCardAuditor<TResult, TItemResult> : FraudRecoveryAuditorBase<TResult> 
			where TItemResult : OperationResponseWithResult<SvcCardOperationResult>, new ()
			where TResult : OperationResponseCollectionResult<TItemResult>
	{
		public TransactionWithSvcCardAuditor(ISqlDbFacade sqlFacade, IFraudRecoveyConfiguration config, ILoggingUtility logging)
			: base(RecoveryActionTypes.TransactionWithSvcCardsAction, sqlFacade, config, logging)
		{

		}

		protected override void FillInInsertSprocParameters(IList<SqlParameter> parameters, TResult result)
		{
            SqlParameter sqlParameter = new SqlParameter("ResultId", result.TraceNumber);
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("ActionName", result.Name);
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("Category", 1);  // TODO, should define as a constant for this. 1 for transaction ..., 2 for ..., 3 for ...
            parameters.Add(sqlParameter);
		}

        protected override void FillInUpdateSprocParameters(IList<SqlParameter> parameters, TResult result, Dictionary<string, string> additional, string parentTrackNumber, decimal? amount = null, string currency = null)
		{
            SqlParameter sqlParameter = new SqlParameter("ResultId", result.TraceNumber);
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("OperationStatus", result.Status.ToString());
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("HasResult", false);
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("RequestId", result.Id); 
            parameters.Add(sqlParameter);

            if (result.Reason != null)
            {
                sqlParameter = new SqlParameter("HasReason", true);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("NativeReasonCode", result.Reason.NativeReasonCode);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("ReasonCode", result.Reason.Code);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("ReasonMessage", result.Reason.Message);
                parameters.Add(sqlParameter);

            }
            else
            {
                sqlParameter = new SqlParameter("HasReason", false);
                parameters.Add(sqlParameter);
            }
        }
	}
}
