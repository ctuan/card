﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;

namespace Starbucks.FraudRecovery.Provider.Auditing
{
	public class SvcCardActionAuditor<TResult> : FraudRecoveryAuditorBase<TResult> where TResult : OperationResponseWithResult<SvcCardOperationResult>
	{
		public SvcCardActionAuditor(ISqlDbFacade sqlFacade, IFraudRecoveyConfiguration config, ILoggingUtility logging)
			: base(RecoveryActionTypes.SvcCardAction, sqlFacade, config, logging)
		{
		}

		protected override void FillInInsertSprocParameters(IList<SqlParameter> parameters, TResult result)
		{
            SqlParameter sqlParameter = new SqlParameter("ResultId", result.TraceNumber);
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("ActionName", result.Name);
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("Category", 3);  // TODO, should define as a constant for this. 1 for transaction ..., 2 for ..., 3 for ...
            parameters.Add(sqlParameter);
		}

        protected override void FillInUpdateSprocParameters(IList<SqlParameter> parameters, TResult result, Dictionary<string, string> additional, string parentTrackNumber, decimal? amount = null, string currency = null)
		{
            SqlParameter sqlParameter = new SqlParameter("ResultId", result.TraceNumber);
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("OperationStatus", result.Status.ToString());
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("RequestId", result.Id);
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("ParentHeaderID", parentTrackNumber);
            parameters.Add(sqlParameter);

            if (result.Result != null)
            {
                sqlParameter = new SqlParameter("HasResult", true);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("Amount", result.Result.Amount);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("BeginningBalance", result.Result.BeginningBalance);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("EndingBalance", result.Result.EndingBalance);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("CurrencyCode", result.Result.CurrencyCode);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("BalanceDateTime",  Convert.ToDateTime(result.Result.EndingBalanceDateTime));
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("Merchantid", result.Result.Mid);
                parameters.Add(sqlParameter);
            }
            else
            {
                sqlParameter = new SqlParameter("HasResult", false);
                parameters.Add(sqlParameter);

                if (amount != null)
                {
                    sqlParameter = new SqlParameter("Amount", amount.Value);
                    parameters.Add(sqlParameter);
                }

                if (currency != null)
                {
                    sqlParameter = new SqlParameter("CurrencyCode", currency);
                    parameters.Add(sqlParameter);
                }
            }

            if (result.Reason != null)
            {
                sqlParameter = new SqlParameter("HasReason", true);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("NativeReasonCode", result.Reason.NativeReasonCode);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("ReasonCode", result.Reason.Code);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("ReasonMessage", result.Reason.Message);
                parameters.Add(sqlParameter);

            }
            else
            {
                sqlParameter = new SqlParameter("HasReason", false);
                parameters.Add(sqlParameter);
            }
		}
	}
}
