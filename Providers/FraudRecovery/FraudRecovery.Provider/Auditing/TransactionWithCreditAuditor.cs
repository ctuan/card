﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;
using SdkModel = Starbucks.Api.Sdk.PaymentService.Models;

namespace Starbucks.FraudRecovery.Provider.Auditing
{
	public class TransactionWithCreditAuditor<TResult> : FraudRecoveryAuditorBase<TResult> where TResult : OperationResponseWithResult<CreditCardOperationResult>
	{

        public TransactionWithCreditAuditor(ISqlDbFacade sqlFacade, IFraudRecoveyConfiguration config, ILoggingUtility logging)
            : base(RecoveryActionTypes.TransactionWithCreditCard, sqlFacade, config, logging)
		{

		}

		protected override void FillInInsertSprocParameters(IList<SqlParameter> parameters, TResult result)
		{
            SqlParameter sqlParameter = new SqlParameter("ResultId", result.TraceNumber);
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("ActionName", result.Name);
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("Category", 3);  // TODO, should define as a constant for this. 1 for transaction ..., 2 for ..., 3 for ...
            parameters.Add(sqlParameter);
        }

        protected override void FillInUpdateSprocParameters(IList<SqlParameter> parameters, TResult result, Dictionary<string, string> additional, string parentTrackNumber, decimal? amount = null, string currency = null)
		{
            SqlParameter sqlParameter = new SqlParameter("ResultId", result.TraceNumber);
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("OperationStatus", result.Status.ToString());
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("RequestId", result.Id);
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("ParentHeaderID", parentTrackNumber);
            parameters.Add(sqlParameter);

            TransactionRefundResponse transactionRefundResponse = result as TransactionRefundResponse;
            if (transactionRefundResponse != null)
            {
                CreditCardOperationResult creditCardOperationResult = transactionRefundResponse.Result;
                FillInCardOperationResult(parameters, creditCardOperationResult);
            }

            if (result.Result != null)
            {
                sqlParameter = new SqlParameter("HasResult", true);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("Amount", result.Result.Amount);
                parameters.Add(sqlParameter);

				sqlParameter = new SqlParameter("BalanceDateTime", Convert.ToDateTime(DateTime.Now).ToUniversalTime());
				parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("Decision", result.Result.Decision);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("CurrencyCode", result.Result.CurrencyCode);
                parameters.Add(sqlParameter);
            }
            else
            {
                sqlParameter = new SqlParameter("HasResult", false);
                parameters.Add(sqlParameter);
            }

            if (result.Reason != null)
            {
                sqlParameter = new SqlParameter("HasReason", true);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("NativeReasonCode", result.Reason.NativeReasonCode);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("ReasonCode", result.Reason.Code);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("ReasonMessage", result.Reason.Message);
                parameters.Add(sqlParameter);

            }
            else
            {
                sqlParameter = new SqlParameter("HasReason", false);
                parameters.Add(sqlParameter);
            }
        }

        private void FillInCardOperationResult(IList<SqlParameter> parameters, 
                                                CreditCardOperationResult creditCardOperationResult)
        {
            if (creditCardOperationResult == null)
                return;

            if (creditCardOperationResult.MerchantId != null)
            {
                FillInNonEmptyField(parameters, "MerchantId", creditCardOperationResult.MerchantId);
            }

            SdkModel.CreditRequest creditRequest = creditCardOperationResult.CreditRequest;
            if (creditRequest == null)
                return;

            FillInNonEmptyField(parameters, "BillingRequestId", creditRequest.CaptureRequestId);

            SqlParameter sqlParameter = new SqlParameter("UserId", creditRequest.UserId);
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("Market", creditRequest.Market);
            parameters.Add(sqlParameter);

            if (creditRequest.BillingInfo == null)
                return;

            sqlParameter = new SqlParameter("EMail", creditRequest.BillingInfo.Email);
            parameters.Add(sqlParameter);

            if (creditRequest.BillingInfo.PaymentMethod != null)
            {
                if (creditRequest.BillingInfo.PaymentMethod.CreditCardDetails != null)
                {
                    FillInNonEmptyField(parameters, "CcFullName", creditRequest.BillingInfo.PaymentMethod.CreditCardDetails.FullName);
                    FillInNonEmptyField(parameters, "CcType", creditRequest.BillingInfo.PaymentMethod.CreditCardDetails.Type.ToString());
                    FillInNonEmptyField(parameters, "SurrogateNumber", creditRequest.BillingInfo.PaymentMethod.CreditCardDetails.SurrogateNumber);
                    FillInNonEmptyField(parameters, "ExpirationYear", creditRequest.BillingInfo.PaymentMethod.CreditCardDetails.ExpireYear);
                    FillInNonEmptyField(parameters, "ExpirationMonth", creditRequest.BillingInfo.PaymentMethod.CreditCardDetails.ExpireMonth);
                }
            }

            if (creditRequest.BillingInfo.AddressInfo != null)
            {
                if (creditRequest.BillingInfo.AddressInfo.AddressDetails != null)
                {
                    FillInNonEmptyField(parameters, "FirstName", creditRequest.BillingInfo.AddressInfo.AddressDetails.FirstName);
                    FillInNonEmptyField(parameters, "LastName", creditRequest.BillingInfo.AddressInfo.AddressDetails.LastName);
                    FillInNonEmptyField(parameters, "AddressLine1", creditRequest.BillingInfo.AddressInfo.AddressDetails.AddressLine1);
                    FillInNonEmptyField(parameters, "AddressLine2", creditRequest.BillingInfo.AddressInfo.AddressDetails.AddressLine2);
                    FillInNonEmptyField(parameters, "City", creditRequest.BillingInfo.AddressInfo.AddressDetails.City);
                    FillInNonEmptyField(parameters, "Region", creditRequest.BillingInfo.AddressInfo.AddressDetails.CountrySubdivision);
                    FillInNonEmptyField(parameters, "PostalCode", creditRequest.BillingInfo.AddressInfo.AddressDetails.PostalCode);
                    FillInNonEmptyField(parameters, "Country", creditRequest.BillingInfo.AddressInfo.AddressDetails.Country);
                    FillInNonEmptyField(parameters, "PhoneNumber", creditRequest.BillingInfo.AddressInfo.AddressDetails.PhoneNumber);
                }
            }
        }

        private void FillInNonEmptyField(IList<SqlParameter> parameters, string columnName, string value)
        {
            if (!string.IsNullOrWhiteSpace(columnName) && !string.IsNullOrWhiteSpace(value))
            {
                SqlParameter sqlParameter = new SqlParameter(columnName, value);
                parameters.Add(sqlParameter);
            }
        }

        private void FillInNonEmptyField(IList<SqlParameter> parameters, string columnName, int? value)
        {
            if (!string.IsNullOrWhiteSpace(columnName) && value != null)
            {
                SqlParameter sqlParameter = new SqlParameter(columnName, value.Value);
                parameters.Add(sqlParameter);
            }
        }

	}
}
