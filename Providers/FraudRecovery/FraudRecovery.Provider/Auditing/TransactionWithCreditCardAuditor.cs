﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;

namespace Starbucks.FraudRecovery.Provider.Auditing
{
	public class TransactionWithCreditCardAuditor<TResult, TItemResult> : FraudRecoveryAuditorBase<TResult> 
			where TItemResult : OperationResponseWithResult<SvcCardOperationResult>, new ()
			where TResult : OperationResponseCollectionResult<TItemResult>
	{
		public TransactionWithCreditCardAuditor(ISqlDbFacade sqlFacade, IFraudRecoveyConfiguration config, ILoggingUtility logging)
			: base(RecoveryActionTypes.TransactionWithSvcCardsAction, sqlFacade, config, logging)
		{

		}

		protected override void FillInInsertSprocParameters(IList<SqlParameter> parameters, TResult result)
		{
            SqlParameter sqlParameter = new SqlParameter("HeaderID", result.TraceNumber);
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("ActionName", result.Name);
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("Category", 1);  // TODO, should define as a constant for this. 1 for transaction ..., 2 for ..., 3 for ...
            parameters.Add(sqlParameter);
		}

        protected override void FillInUpdateSprocParameters(IList<SqlParameter> parameters, TResult result, Dictionary<string, string> additional, string parentTrackNumber)
		{
            SqlParameter sqlParameter = new SqlParameter("HeaderID", result.TraceNumber);
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("OperationStatus", result.Status.ToString());
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("HasResult", false);
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("HasReason", false);
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("RequestId", result.Id); 
            parameters.Add(sqlParameter);
		}
	}
}
