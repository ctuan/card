﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;

namespace Starbucks.FraudRecovery.Provider.Auditing
{
	public class TransactionWithSvcCardsAuditor<TResult> : FraudRecoveryAuditorBase<TResult> where TResult : OperationResponseWithResult<CreditCardOperationResult>
	{

		public TransactionWithSvcCardsAuditor(ISqlDbFacade sqlFacade, IFraudRecoveyConfiguration config, ILoggingUtility logging)
			: base(RecoveryActionTypes.TransactionWithSvcCardsAction, sqlFacade, config, logging)
		{

		}


		protected override void FillInInsertSprocParameters(IList<SqlParameter> parameters, TResult result)
		{
			// todo: fill in the store procedure parameters
			System.Diagnostics.Debug.WriteLine("TransactionWithSvcCardsAuditor::FillInInsertSprocParameters()");
		}

        protected override void FillInUpdateSprocParameters(IList<SqlParameter> parameters, TResult result, Dictionary<string, string> additional, string parentTrackNumber)
		{
            SqlParameter sqlParameter = new SqlParameter("ResultId", result.TraceNumber);
            parameters.Add(sqlParameter);

            sqlParameter = new SqlParameter("OperationStatus", result.Status.ToString());
            parameters.Add(sqlParameter);
/*
            if (result.Result != null)
            {
                sqlParameter = new SqlParameter("HasResult", true);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("Amount", result.Result.Amount);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("CurrencyCode", result.Result.
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("BalanceDateTme", Convert.ToDateTime(result.Result.EndingBalanceDateTime));
                parameters.Add(sqlParameter);
            }
            else
            {
                sqlParameter = new SqlParameter("HasResult", false);
                parameters.Add(sqlParameter);
            }

            if (result.Reason != null)
            {
                sqlParameter = new SqlParameter("HasReason", true);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("NativeReasonCode", result.Reason.NativeReasonCode);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("ReasonCode", result.Reason.Code);
                parameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("ReasonMessage", result.Reason.Message);
                parameters.Add(sqlParameter);

            }
            else
            {
                sqlParameter = new SqlParameter("HasReason", false);
                parameters.Add(sqlParameter);
            }
*/ 
		}
	}
}
