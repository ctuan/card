﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;

namespace Starbucks.FraudRecovery.Provider.Auditing
{
	public abstract class FraudRecoveryAuditorBase<TResult> : IFraudRecoveryAuditor<TResult>
		where TResult : OperationResponse
	{
        protected abstract void FillInInsertSprocParameters(IList<SqlParameter> parameters, TResult result);

        protected abstract void FillInUpdateSprocParameters(IList<SqlParameter> parameters, TResult result,
                                                            Dictionary<string, string> additional, string parentTrackNumber,
                                                            decimal? amount = null, string currency = null);

		protected FraudRecoveryAuditorBase(RecoveryActionTypes type, ISqlDbFacade sqlFacade, IFraudRecoveyConfiguration config,
		                                   ILoggingUtility logging)
		{
			logging.EnsureObjectNotNull("logging is required");
			config.EnsureObjectNotNull("configuration is required");
			sqlFacade.EnsureObjectNotNull("configuration is required");

			this.AuditingType = type;
			this.Logging = logging;
			this.Configuration = config;
			this.SqlDbFacade = sqlFacade;

			this.AdditionalUpdateParameters = new Dictionary<string, string>();
		}

		protected RecoveryActionTypes AuditingType { get; private set; }
		protected ILoggingUtility Logging { get; private set; }
		protected IFraudRecoveyConfiguration Configuration { get; private set; }
		protected ISqlDbFacade SqlDbFacade { get; private set; }

		public Dictionary<string, string> AdditionalUpdateParameters { get; private set; }

        public void Create(TResult result, string parentTrackNumber)
		{
            ExecuteQuery(result, "Create", FillInInsertSprocParameters);
		}

        public void Update(TResult result, string parentTrackNumber, decimal? amount = null, string currency = null)
		{
            ExecuteQuery(result, "Update", (x, y) => FillInUpdateSprocParameters(x, y, this.AdditionalUpdateParameters, parentTrackNumber, amount, currency));
		}


        protected void ExecuteQuery(TResult result, string actionName, Action<IList<SqlParameter>, TResult> updateQueryParamenter)
		{
			try
			{
				var auditingConfig = this.Configuration.AuditingConfig;
				auditingConfig.EnsureObjectNotNull("auditingConfig is missing in the FraudRecovery Configuration section");

				var sprocConfig = auditingConfig[this.AuditingType];
				sprocConfig.EnsureObjectNotNull("sprocConfig is missing in the FraudRecovery Configuration section");

				var sqlParamenters = new List<SqlParameter>();
                updateQueryParamenter(sqlParamenters, result);

                string sprocCommand = sprocConfig.UpdateCommand;
                if (actionName != null && actionName.Equals("Create"))
                {
                    sprocCommand = sprocConfig.InsertCommand;
                }
                SqlDbFacade.ExecuteNonQuery(sprocCommand, auditingConfig.DatabaseConnectionStringName, sqlParamenters);
			}
			catch (Exception ex)
			{
				string msg = string.Format("FraudRecoveryAuditorBase::{0}()", actionName ?? "<NULL>");
				this.Logging.Log(LogType.Error, msg , "Execution of SQL store procedure is failed",
								 "Application configuration issue or DB issue",
								 new Dictionary<string, object>()
					                 {
						                 {"actionType", this.AuditingType},
						                 {"exception", ex}
					                 });
			}
		}

	}
}
