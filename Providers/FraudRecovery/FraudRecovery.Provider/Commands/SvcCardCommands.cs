﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;
using Starbucks.FraudRecovery.Dal.Common;
using Starbucks.FraudRecovery.Dal.Common.Models;
using Starbucks.FraudRecovery.Provider.Configuration;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.CardIssuer.Dal.Common;
using CardIssuerModels = Starbucks.CardIssuer.Dal.Common.Models;
using Account.Provider.Common;

namespace Starbucks.FraudRecovery.Provider.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public class SvcCardGetBalanceCommand : SvcCardIdBasedCommand<SvcCardGetBalanceResponse>
	{
		public SvcCardGetBalanceCommand(int cardId, IDependencyFactory dependencyFactory)
			: base(cardId, dependencyFactory, null)
		{
		}

		protected override CardIssuerModels.ICardTransaction ExecuteNative(CardIssuerModels.IMerchantInfo merchantInfo, SvcCardInfo cardInfo)
		{
			return this.ActionProvider.GetBalance(merchantInfo, cardInfo.Number, cardInfo.Pin);
		}

		protected override void FinalizedResult(CardIssuerModels.ICardTransaction cardTransaction, SvcCardGetBalanceResponse result)
		{
			result.Result = base.GetCardOperationResult(cardTransaction);
		}
	}

	/// <summary>
	/// 
	/// </summary>
	public class SvcCardRevokeAmountCommand : SvcCardIdBasedCommand<SvcCardRevokeAmountResponse>
	{
        public SvcCardRevokeAmountCommand(SvcCardInfo cardInfo, decimal amount, IDependencyFactory dependencyFactory, string traceNumber, string currency = null)
            : base(cardInfo, dependencyFactory, traceNumber)
		{
			this.Amount = amount;
            this.Currency = currency;
		}

        public SvcCardRevokeAmountCommand(int cardId, decimal amount, IDependencyFactory dependencyFactory, IAccountProvider accountProvider)
			: base(cardId, dependencyFactory, null)
		{
			this.Amount = amount;
            this.AccountProvider = accountProvider;
		}

		public decimal Amount { get; private set; }
        public string Currency { get; private set; }
        protected IAccountProvider AccountProvider { get; private set; }

		protected override CardIssuerModels.ICardTransaction ExecuteNative(CardIssuerModels.IMerchantInfo merchantInfo, SvcCardInfo cardInfo)
		{
			return this.ActionProvider.Redeem(merchantInfo, cardInfo.Number, cardInfo.Pin, this.Amount);
		}

		protected override void FinalizedResult(CardIssuerModels.ICardTransaction cardTransaction, SvcCardRevokeAmountResponse result)
		{
			result.Result = base.GetCardOperationResult(cardTransaction);
		}
	}

	/// <summary>
	/// 
	/// </summary>
	public class SvcCardLoadAmountCommand : SvcCardIdBasedCommand<SvcCardLoadAmountResponse>
	{
		public SvcCardLoadAmountCommand(int cardId, decimal amount, IDependencyFactory dependencyFactory)
			: base(cardId, dependencyFactory, null)
		{
			this.Amount = amount;
		}

		public decimal Amount { get; private set; }

		protected override CardIssuerModels.ICardTransaction ExecuteNative(CardIssuerModels.IMerchantInfo merchantInfo, SvcCardInfo cardInfo)
		{
			return this.ActionProvider.Reload(merchantInfo, cardInfo.Number, cardInfo.Pin, this.Amount);
		}


		protected override void FinalizedResult(CardIssuerModels.ICardTransaction cardTransaction, SvcCardLoadAmountResponse result)
		{
			result.Result = base.GetCardOperationResult(cardTransaction);
		}
	}

	/// <summary>
	/// 
	/// </summary>
	public class SvcCardCashoutCommand : SvcCardIdBasedCommand<SvcCardCashOutResponse>
	{
		public SvcCardCashoutCommand(int cardId, IDependencyFactory dependencyFactory)
			: base(cardId, dependencyFactory, null)
		{
		}


		protected override CardIssuerModels.ICardTransaction ExecuteNative(CardIssuerModels.IMerchantInfo merchantInfo, SvcCardInfo cardInfo)
		{
			return this.ActionProvider.CashOut(merchantInfo, cardInfo.Number, cardInfo.Pin, 0);

			//var result = this.ActionProvider.GetBalance(merchantInfo, cardInfo.Number, cardInfo.Pin);
			//decimal balance = result != null && result.TransactionSucceeded ? result.EndingBalance : 0m;
			//if (balance > 0m)
			//{
			//	result = this.ActionProvider.Redeem(merchantInfo, cardInfo.Number, cardInfo.Pin, balance);
			//}

			//return result;
		}

		protected override void FinalizedResult(CardIssuerModels.ICardTransaction cardTransaction, SvcCardCashOutResponse result)
		{
			result.Result = base.GetCardOperationResult(cardTransaction);
		}
	}


	/// <summary>
	/// 
	/// </summary>
	public class SvcCardFreezeCommand : SvcCardIdBasedCommand<SvcCardFreezeResponse>
	{
        public SvcCardFreezeCommand(SvcCardInfo cardInfo, IDependencyFactory dependencyFactory, string traceNumber)
            : base(cardInfo, dependencyFactory, traceNumber)
		{
		}

		public SvcCardFreezeCommand(int cardId, IDependencyFactory dependencyFactory)
			: base(cardId, dependencyFactory, null)
		{
		}

		protected override CardIssuerModels.ICardTransaction ExecuteNative(CardIssuerModels.IMerchantInfo merchantInfo, SvcCardInfo cardInfo)
		{
			return this.ActionProvider.BalanceLock(merchantInfo, cardInfo.Number, cardInfo.Pin);
		}

		protected override void FinalizedResult(CardIssuerModels.ICardTransaction cardTransaction, SvcCardFreezeResponse result)
		{
			result.Result = base.GetCardOperationResult(cardTransaction);
		}
	}


	/// <summary>
	/// 
	/// </summary>
	public class SvcCardUnfreezeCommand : SvcCardIdBasedCommand<SvcCardUnfreezeResponse>
	{
        public SvcCardUnfreezeCommand(SvcCardInfo cardInfo, IDependencyFactory dependencyFactory, string traceNumber)
            : base(cardInfo, dependencyFactory, traceNumber)
		{
		}

		public SvcCardUnfreezeCommand(int cardId, IDependencyFactory dependencyFactory)
			: base(cardId, dependencyFactory, null)
		{
		}

		protected override CardIssuerModels.ICardTransaction ExecuteNative(CardIssuerModels.IMerchantInfo merchantInfo,
		                                                                   SvcCardInfo cardInfo)
		{

			decimal lockAmount = this.DependencyFactory.Configuration.UnfreezeRedeemAmount;
			var result = this.ActionProvider.RedemptionUnlock(merchantInfo, cardInfo.Number, cardInfo.Pin, lockAmount);
			return result;
		}

		protected override void FinalizedResult(CardIssuerModels.ICardTransaction cardTransaction, SvcCardUnfreezeResponse result)
		{
			result.Result = base.GetCardOperationResult(cardTransaction);
		}
	}
}
