﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Helpers;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;
using Starbucks.FraudRecovery.Dal.Common;
using Starbucks.FraudRecovery.Provider.Configuration;
using Starbucks.FraudRecovery.Provider.Facades;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.CardIssuer.Dal.Common;

namespace Starbucks.FraudRecovery.Provider.Commands
{
	public abstract class CommandBase<TResult, TProvider> : ICommand<TResult> where TResult: class
	{
		protected abstract TResult DoExecute(TResult result);
		protected abstract TResult HandleException(Exception ex, TResult result);
		protected abstract TResult CreateResult();

		protected CommandBase(TProvider provider, IFraudRecoveyConfiguration configuration, ILoggingUtility logging)
		{
			logging.EnsureObjectNotNull("logging is required");
			provider.EnsureObjectNotNull("provider is required");
			configuration.EnsureObjectNotNull("configuration is required");

			this.Logger = logging;
			this.ActionProvider = provider;
			this.Configuration = configuration;
		}


		protected TProvider ActionProvider { get; private set; }
		protected ILoggingUtility Logger { get; private set; }
		protected IFraudRecoveyConfiguration Configuration { get; private set; }
		protected virtual string LoggingTitle
		{
			get { return string.Format("FraudRecovery {0}::Execute()", this.GetType().Name); }
		}

		public virtual TResult Execute()
		{
			TResult result = CreateResult();
			try
			{
				InitializeOperation(result);
				result = DoExecute(result);
			}
			catch (Exception ex)
			{
				result = HandleException(ex, result);
			}
			finally
			{
				FinalizeOperation(result);
			}
			return result;
		}

		protected virtual void InitializeOperation(TResult result)
		{
		}

		protected virtual void FinalizeOperation(TResult result)
		{
		}

		protected virtual void ReportException(Exception ex, string title)
		{
			try
			{
				this.Logger.Log(LogType.Error, title, "Unexpected exception", ""
								, new Dictionary<string, object>()
				                {
					                {"Exception", ex}
				                }
					);
			}
			catch (Exception error)
			{
				// fail during logging.  have to eat the exception.
				System.Diagnostics.Debug.WriteLine(error.ToString());
			}

		}
	}

	public abstract class FraudRecoveryCommandBase<TResult, TGatewayProvider> : CommandBase<TResult, TGatewayProvider> where TResult : OperationResponse, new()
	{

		protected FraudRecoveryCommandBase(TGatewayProvider gatewayProvider, RecoveryActionTypes actionType, IDependencyFactory dependencyFactory, IFraudRecoveryAuditor<TResult> auditor, string parentTrackNumber)
			: base(gatewayProvider, dependencyFactory == null ? null : dependencyFactory.Configuration, dependencyFactory == null? null: dependencyFactory.Logging)
		{
			dependencyFactory.EnsureObjectNotNull("dependencyFactory is required");
			this.DependencyFactory = dependencyFactory;

			this.FraudRecoveryDal = dependencyFactory.FraudRecoveryDal;
			this.FraudRecoveryDal.EnsureObjectNotNull("fraudRecoveryDal is required");

			this.Auditor = auditor;
			//this.Auditor.EnsureObjectNotNull("auditor is required");

            this.ParentTrackNumber = parentTrackNumber;

			this.ActionType = actionType;
		}

		protected IDependencyFactory DependencyFactory { get; private set; }
		protected IFraudRecoveryDal FraudRecoveryDal { get; private set; }
		protected IFraudRecoveryAuditor<TResult> Auditor { get; private set; }
        protected string ParentTrackNumber { get; private set; }

		public RecoveryActionTypes ActionType { get; private set; }

		protected override TResult CreateResult()
		{
			var result = new TResult();
			return result;
		}

		protected override void InitializeOperation(TResult result)
		{
			base.InitializeOperation(result);
            result.TraceNumber = Guid.NewGuid().ToString();
			this.Auditor.AdditionalUpdateParameters.Clear();

            Auditor.Create(result, ParentTrackNumber);
		}

		protected override void FinalizeOperation(TResult result)
		{
            decimal? amount = null;
            string currency = null;

            SvcCardRevokeAmountCommand svcCardRevokeAmountCommand = this as SvcCardRevokeAmountCommand;
            if (svcCardRevokeAmountCommand != null)
            {
                amount = svcCardRevokeAmountCommand.Amount;
                currency = svcCardRevokeAmountCommand.Currency;
            }

            SvcCardLoadAmountCommand SvcCardLoadAmountCommand = this as SvcCardLoadAmountCommand;
            if (SvcCardLoadAmountCommand != null)
            {
                amount = SvcCardLoadAmountCommand.Amount;
            }

            Auditor.Update(result, ParentTrackNumber, amount, currency);
			base.FinalizeOperation(result);
		}

		protected virtual ReasonTypes ExceptionErrorType
		{
			get {return ReasonTypes.UnexpectedInternalError; }
		}

		protected override TResult HandleException(Exception ex, TResult result)
		{
			ReportException(ex, this.LoggingTitle);
			if (result != null)
			{
				result.UpdateErrorResult(ExceptionErrorType);
			}
			return result;
		}

	}
}