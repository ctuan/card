﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Helpers;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;
using Starbucks.FraudRecovery.Dal.Common;
using Starbucks.FraudRecovery.Dal.Common.Models;
using Starbucks.FraudRecovery.Provider.Configuration;
using Starbucks.FraudRecovery.Provider.Facades;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Models;

namespace Starbucks.FraudRecovery.Provider.Commands
{
	public abstract class SvcCardIdBasedCommand<TResult> : FraudRecoveryCommandBase<TResult, ICardIssuerDal> where TResult : OperationResponseWithResult<SvcCardOperationResult>, new()
	{
		protected abstract ICardTransaction ExecuteNative(IMerchantInfo merchantInfo, SvcCardInfo cardInfo);
		protected abstract void FinalizedResult(ICardTransaction cardTransaction, TResult result);

		protected SvcCardIdBasedCommand(SvcCardInfo cardInfo, IDependencyFactory dependencyFactory, string parentTrackNumber)
            : base(dependencyFactory == null ? null : dependencyFactory.CardIssuer, RecoveryActionTypes.SvcCardAction, dependencyFactory, dependencyFactory == null ? null : dependencyFactory.GetSvcCardActionAuditor<TResult>(), parentTrackNumber)
		{
			this.CardInfo = cardInfo;
			this.CardId = cardInfo != null ? cardInfo.Id : -1;
		}

		protected SvcCardIdBasedCommand(int cardId, IDependencyFactory dependencyFactory, string parentTrackNumber)
            : base(dependencyFactory == null ? null : dependencyFactory.CardIssuer, RecoveryActionTypes.SvcCardAction, dependencyFactory, dependencyFactory == null ? null : dependencyFactory.GetSvcCardActionAuditor<TResult>(), parentTrackNumber)
		{
			this.CardId = cardId;
			this.CardInfo = null;
		}


		protected int CardId { get; private set; }
		protected SvcCardInfo CardInfo { get; private set; }
//		protected string ParentTrackNumber { get; private set; }

		protected override TResult DoExecute(TResult initResult)
		{
			var result = initResult ?? new TResult();
			result.Id = this.CardId.ToString();

			SvcCardInfo cardInfo = this.CardInfo;
			if (cardInfo == null)
			{
				if (this.CardId < 0)
				{
					return result.UpdateErrorResult(ReasonTypes.InvalidCardId);
				}
				cardInfo = this.FraudRecoveryDal.GetSvcCard(this.CardId);
			}
			if (cardInfo == null || string.IsNullOrWhiteSpace(cardInfo.Market) || string.IsNullOrWhiteSpace(cardInfo.Number))
			{
				return result.UpdateErrorResult(ReasonTypes.InvalidCard);
			}

			string cardMarket = cardInfo.Market;
			IMerchantInfo merchantInfo = ActionProvider.GetMerchantInfo(GetMarket(cardMarket));
			if (merchantInfo == null)
			{
				return result.UpdateErrorResult(ReasonTypes.MissingMerchantInfo);
			}

            // cross market check. will only query merchant info again if the market's currency is different than
            // the card's market
            SvcCardRevokeAmountCommand svcCardRevokeAmountCommand = this as SvcCardRevokeAmountCommand;
            if (svcCardRevokeAmountCommand != null)
            {
                if (svcCardRevokeAmountCommand.Currency != null &&
                    !svcCardRevokeAmountCommand.Currency.Equals(merchantInfo.CurrencyCode))
                {
                    merchantInfo = ActionProvider.GetMerchantInfoByCurrency(svcCardRevokeAmountCommand.Currency);
                    if (merchantInfo == null)
                    {
                        return result.UpdateErrorResult(ReasonTypes.MissingMerchantInfo);
                    }
                }
            }

			if (this.Configuration.IsPinless)
			{
				cardInfo.Pin = null;
			}
			return DoNativeExecute(result, merchantInfo, cardInfo);
		}

		protected virtual TResult DoNativeExecute(TResult result, IMerchantInfo merchantInfo, SvcCardInfo cardInfo)
		{
			ICardTransaction native = null;
			try
			{
				native = ExecuteNative(merchantInfo, cardInfo);
			}
			catch (Exception ex)
			{
				ReportException(ex, LoggingTitle);
				return result.UpdateErrorResult( ReasonTypes.UnexpectedExternalError);
			}

			if (native == null)
			{
				return result.UpdateErrorResult( ReasonTypes.ExternalServiceNoResponse);
			}

            FinalizedResult(native, result);
			if (native.TransactionSucceeded)
			{
				result.Status = OperationStatus.Succeeded;
			}
			else
			{
				HandleFailure(native, result);
			}

			return result;
		}

		protected virtual void HandleFailure(ICardTransaction native, TResult result)
		{
			result.UpdateErrorResult(ReasonTypes.ExternalServiceResponseWithError);
			if (result.Reason != null && native != null)
			{
				result.Reason.NativeReasonCode = native.ResponseCode;
			}
		}

		protected virtual SvcCardOperationResult GetCardOperationResult(ICardTransaction native)
		{
			var result = new SvcCardOperationResult()
				{
					Amount = native!= null? native.Amount : 0m,
					BeginningBalance = native!= null? native.BeginningBalance : 0m,
					EndingBalance = native!= null? native.EndingBalance: 0m,
					CurrencyCode = native != null? native.Currency: string.Empty,
					EndingBalanceDateTime = native != null ? native.UtcDate.ToUtcDateTimeString() : string.Empty,
					LockAmount = native!= null? native.LockAmount: 0m,
                    Mid = native != null ? native.MerchantId : null,
				};
			return result;
		}

		protected virtual string GetMarket(string cardMarket)
		{
			string market = string.Format("{0}{1}", this.Configuration.MarketPrefix ?? "", cardMarket ?? "");
			return market;
		}
	}
}
