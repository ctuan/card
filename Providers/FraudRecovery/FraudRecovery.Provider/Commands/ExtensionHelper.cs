﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common.Models;
using Sdk = Starbucks.Api.Sdk.PaymentService;
using SdkModels = Starbucks.Api.Sdk.PaymentService.Models;
using DalModels = Starbucks.FraudRecovery.Dal.Common.Models;
using Starbucks.FraudRecovery.Common.Helpers;
using SdkModel = Starbucks.Api.Sdk.PaymentService.Models;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.FraudRecovery.Dal;
using Starbucks.FraudRecovery.Dal.Common.Models;
using Starbucks.FraudRecovery.Dal.Common;


namespace Starbucks.FraudRecovery.Provider.Commands
{
	public static class ExtensionHelper
	{
		#region data converting

		public const int PaymentGatewayAccptCode = 100;


		public static CreditCardOperationResult ToModel(this Sdk.Interfaces.ICreditResult native, 
                                                        OperationResponse response,
                                                        string currencyCode,
                                                        SdkModel.CreditRequest sdkRequest,
                                                        IMerchantInfo merchantInfo,
                                                        IFraudRecoveryDal fraudRecoveryDal)
		{
			if (native == null || native.ReasonCode == null)
			{
				return null;
			}

            string transactionType = "Reload";
            if (sdkRequest.TransactionType == Starbucks.Api.Sdk.PaymentService.Models.TransactionTypes.BuyACard)
            {
                transactionType = "BuyACard";
            }
            else if (sdkRequest.TransactionType == Starbucks.Api.Sdk.PaymentService.Models.TransactionTypes.AutoReload)
            {
                transactionType = "AutoReload";
            }

            CyberSourceMerchantInfo info = fraudRecoveryDal.GetCyberSourceMerchantInfo(merchantInfo.SubMarketId, sdkRequest.CurrencyCode, transactionType);

			var model = new CreditCardOperationResult()
				{
					Amount = native.Amount,
					Decision = native.Decision.ToString(),
					PaymentGatewayId = native.RequestId,
                    CurrencyCode = currencyCode,
                    CreditRequest = sdkRequest,
                    MerchantId = (info == null) ? string.Empty : info.MerchantKey
				};

			response.Status = OperationStatus.Succeeded;
			if (native.ReasonCode.Code != PaymentGatewayAccptCode )
			{
                response.UpdateErrorResult(ReasonTypes.ExternalServiceResponseWithError, native.ReasonCode.Code.ToString(), native.ReasonCode.Description ?? "<NULL>");

			}
			return model;
		}

		public static string ToMarket(this DalModels.BillingInfo billing)
		{
			// todo (wyang) currently map country code to market.  Need revise the code if there is Market info
			// that can be retrieve from DB that associcated with order
			//
			if (billing == null || billing.AddressInfo == null || billing.AddressInfo.AddressDetails == null)
			{
				return null;
			}

			return billing.AddressInfo.AddressDetails.Country;
		}

		public static decimal ToOrderItemTotal(this DalModels.OrderItem item)
		{
			decimal result = 0m;
			if (item != null)
			{
				if (item.Total != null)
				{
					result = item.Total.Value;
				}
				else if (item.Quantity != null && item.UnitPrice != null)
				{
					result = (item.Quantity.Value * item.UnitPrice.Value);
				}
			}
			return result;
		}

		public static List<SdkModels.OrderItem> ToBasket(this DalModels.OrderInfo order)
		{
			var basket = new List<SdkModels.OrderItem>();

			if (order != null && order.Items != null)
			{
				foreach (var item in order.Items)
				{
					var nativeItem = new SdkModels.OrderItem()
						{
							Name = item.Name,
							Amount = item.UnitPrice?? 0,
							Quantity = item.Quantity?? 0,
							Sku = item.Sku,
							Category =item.Catalog,
						};
					basket.Add(nativeItem);
				}
			}

			return basket;
		}

		public static SdkModels.BillingInfo ToNative(this DalModels.BillingInfo model)
		{
			if (model == null)
			{
				return null;
			}

			var native = new SdkModels.BillingInfo()
				{
					AddressInfo = model.AddressInfo.ToNative (),
					Email = model.Email,
					PaymentMethod = model.PaymentMethodInfo.ToNative (),
				};
			return native;
		}

		public static SdkModels.AddressInfo ToNative(this DalModels.AddressInfo model)
		{
			if (model == null)
			{
				return null;
			}

			var native = new SdkModels.AddressInfo()
				{
					AddressId = model.AddressId,
					AddressDetails = null,
				};

			if (model.AddressDetails != null)
			{
				native.AddressDetails = new SdkModels.Address()
					{
						AddressLine1 = model.AddressDetails.AddressLine1,
						AddressLine2 = model.AddressDetails.AddressLine2,
						City = model.AddressDetails.City,
						CountrySubdivision = model.AddressDetails.CountrySubdivision,
						Country = model.AddressDetails.Country,
						PostalCode = model.AddressDetails.PostalCode,
						PhoneNumber = model.AddressDetails.PhoneNumber,
						FirstName = model.AddressDetails.FirstName,
						LastName = model.AddressDetails.LastName,
					};
			}

			return native;
		}

		public static SdkModels.PaymentMethod ToNative(this DalModels.PaymentMethodInfo info)
		{
			if (info == null)
			{
				return null;
			}

			var native = new SdkModels.PaymentMethod()
				{
					PaymentId = info.PaymentId,
					CreditCardDetails = null,
					PayPalDetails = null,
					TokenDetails = null,
				};

			if (info.CreditCard != null)
			{
				native.CreditCardDetails = new SdkModels.CreditCardDetails()
					{
						Type = (SdkModels.CreditCardTypes) info.CreditCard.Type,
						FullName = info.CreditCard.FullName,
						ExpireMonth = info.CreditCard.ExpireMonth,
						ExpireYear = info.CreditCard.ExpireYear,
						Cvn = info.CreditCard.Cvn,
						SurrogateNumber = info.CreditCard.SurrogateNumber
					};
			}
			return native;
		}

		public static SdkModels.TransactionTypes ToTransactionType(this DalModels.OrderItem item)
		{
            if (item.ProductType == null)
            {
                return SdkModels.TransactionTypes.Reload;
            }

            switch (item.ProductType.Value)
            {
                case 0: return SdkModels.TransactionTypes.Reload;
                case 1: return SdkModels.TransactionTypes.Egift;
                case 2:
                case 3:
                    return SdkModels.TransactionTypes.BuyACard;
                default:
                    return SdkModels.TransactionTypes.Reload;
            }
		}

		#endregion

		#region data validation

		public static bool IsValid(this SdkModels.CreditRequest request)
		{
			if (request == null)
			{
				return false;
			}

            if (request.BillingInfo != null && string.IsNullOrWhiteSpace(request.Market))
			{
				return false;
			}

			if (string.IsNullOrWhiteSpace(request.CurrencyCode))
			{
				return false;
			}

            if (request.BillingInfo != null && string.IsNullOrWhiteSpace(request.MerchantReferenceCode))
			{
				return false;
			}

            if (request.BillingInfo != null && request.TransactionType == SdkModels.TransactionTypes.None)
			{
				return false;
			}

			if (string.IsNullOrWhiteSpace(request.CaptureRequestId))
			{
				if (!request.BillingInfo.IsValid())
				{
					return false;
				}

				if (request.Basket == null || !request.Basket.Any())
				{
					return false;
				}

			}
			return true;
		}

		public static bool IsValid(this SdkModels.BillingInfo billing)
		{
			if (billing == null)
			{
				return false;
			}

			if (billing.AddressInfo == null || !billing.AddressInfo.IsValid ())
			{
				return false;
			}

			if (billing.PaymentMethod == null || !billing.PaymentMethod.IsValid ())
			{
				return false;
			}

			return true;
		}

		public static bool IsValid(this SdkModels.PaymentMethod payment)
		{
			if (payment == null)
			{
				return false;
			}
			if (string.IsNullOrWhiteSpace(payment.PaymentId))
			{
				if (payment.CreditCardDetails == null)
				{
					return false;
				}

				var creditCard = payment.CreditCardDetails;
				if (string.IsNullOrWhiteSpace(creditCard.FullName)
				    || string.IsNullOrWhiteSpace(creditCard.SurrogateNumber)
				    || creditCard.ExpireMonth == null
				    || creditCard.ExpireYear == null
				    || creditCard.Type == SdkModels.CreditCardTypes.None)
				{
					return false;
				}
			}
			return true;
		}

		public static bool IsValid(this SdkModels.AddressInfo info)
		{
			if (info == null || info.AddressDetails == null)
			{
				return false;
			}

			SdkModels.Address address = info.AddressDetails;
			if (string.IsNullOrWhiteSpace(address.AddressLine1)
			    || string.IsNullOrWhiteSpace(address.City)
			    || string.IsNullOrWhiteSpace(address.Country))
			{
				return false;
			}
			return true;
		}

		#endregion
	}
}
