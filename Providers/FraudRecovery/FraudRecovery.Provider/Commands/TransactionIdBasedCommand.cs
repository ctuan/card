﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Helpers;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;
using Starbucks.FraudRecovery.Dal.Common;
using Starbucks.FraudRecovery.Dal.Common.Models;

namespace Starbucks.FraudRecovery.Provider.Commands
{
	public abstract class TransactionIdBasedCommand<TResult, TGatewayProvider> :
		FraudRecoveryCommandBase<TResult, TGatewayProvider>
		where TResult : OperationResponse, new()
	{
		protected abstract void HandleOrder(TResult result, OrderInfo order);
		protected abstract TResult FinalizeResult(TResult result);

        protected TransactionIdBasedCommand(string transactionId, TGatewayProvider gatewayProvider, RecoveryActionTypes actionType, IDependencyFactory dependency, IFraudRecoveryAuditor<TResult> auditor, string parentTrackNumber)
            : base(gatewayProvider, actionType, dependency, auditor, parentTrackNumber)
		{
			this.TransactionId = transactionId;
		}

		protected string TransactionId { get; private set; }
        protected string Currency { get; private set; }

		protected override TResult DoExecute(TResult initResult)
		{
			var result = initResult ?? new TResult();
			result.Id = this.TransactionId;

			if (string.IsNullOrWhiteSpace(this.TransactionId))
			{
				return result.UpdateErrorResult(ReasonTypes.MissingTransactionId);
			}

			var order = this.FraudRecoveryDal.GetOrder(this.TransactionId);
			if (order == null)
			{
				return result.UpdateErrorResult(ReasonTypes.InvalidTransaction);
			}

			if (order.Items != null && order.Items.Any())
			{
                Currency = order.Currency;
				HandleOrder(result, order);
			}
			return FinalizeResult(result);
		}
	}



	public abstract class TransactionIdWithCollectionBasedCommand<TResult, TItemResult, TGatewayProvider> : TransactionIdBasedCommand<TResult, TGatewayProvider>
		where TItemResult : OperationResponseWithResult<SvcCardOperationResult>, new()
		where TResult : OperationResponseCollectionResult<TItemResult>, new()
	{
		protected abstract TItemResult GetCardOperationResponse(string traceNumber, SvcCardInfo cardInfo, OrderItem orderItem);

        protected TransactionIdWithCollectionBasedCommand(string transactionId, TGatewayProvider gatewayProvider, IDependencyFactory dependency, string parentTrackNumber)
			: base(transactionId, gatewayProvider, RecoveryActionTypes.TransactionWithSvcCardsAction, dependency,
                dependency == null ? null : dependency.GetTransactionWithSvcCardActionAuditor<TResult, TItemResult>(), parentTrackNumber)
		{
		}

		protected override void HandleOrder(TResult result, OrderInfo order)
		{
			if (result.Results == null)
			{
				result.Results = new List<TItemResult>();
			}

			var svcTransactions = from c in order.Items
								  where !string.IsNullOrWhiteSpace(c.SvcTransactionId)
								  select c;
			HandleSvcTransactions(result, svcTransactions);

			var nonSvcTransactions = from c in order.Items
									 where string.IsNullOrWhiteSpace(c.SvcTransactionId)
									 select c;
			HandleNonSvcTransactions(result, nonSvcTransactions);
		}

		protected override TResult FinalizeResult(TResult result)
		{
			if (result == null)
			{
				return null;
			}

			int count = 0;
			int failedCount = 0;
			if (result.Results != null)
			{
				count = result.Results.Count;
				failedCount = (from c in result.Results
							   where c.Status != OperationStatus.Succeeded
							   select c).Count();
			}

			result.FailureCount = failedCount;
			result.TotalOperations = count;

			result.Status = failedCount == 0
								? OperationStatus.Succeeded
								: failedCount == count ? OperationStatus.Failed : OperationStatus.PartialSucceeded;
			return result;
		}

		protected virtual void HandleSvcTransactions(TResult result, IEnumerable<OrderItem> items)
		{
			if (items == null || !items.Any())
			{
				return;
			}

            // changed from Parallel.ForEach to for each.
            // when more than one order items that contain the same card number, value link may not handle 
            // well due to possible thread sync issue. Therefore changed to foreach for now.
            foreach (OrderItem item in items)
            {
                TItemResult itemResult = ProcessOrderItem(result, item);
                result.Results.Add(itemResult);
            }
        }

		protected virtual TItemResult ProcessOrderItem(TResult result, OrderItem orderItem)
		{
			SvcTransactionInfo info = null;
			long svc_transactionId = -1;
			if (!string.IsNullOrWhiteSpace(orderItem.SvcTransactionId) &&
				long.TryParse(orderItem.SvcTransactionId, out svc_transactionId))
			{
				info = this.FraudRecoveryDal.GetTransaction(svc_transactionId);
			}


			return ProcessSvcTransaction(result, info, orderItem);
		}

		protected virtual TItemResult ProcessSvcTransaction(TResult result, SvcTransactionInfo info, OrderItem orderItem)
		{
			if (info == null || info.SvcCard == null || string.IsNullOrWhiteSpace(info.SvcCard.Number))
			{
				return CreateErrorItem(ReasonTypes.MissingSvcInfoInTransaction);
			}

			try
			{
				SvcCardInfo cardInfo = this.FraudRecoveryDal.GetSvcCard(info.SvcCard.Number);
				TItemResult cardOperationResponse = GetCardOperationResponse(result.TraceNumber, cardInfo,  orderItem);
				return cardOperationResponse;
			}
			catch (Exception ex)
			{
				ReportException(ex, LoggingTitle);
				return CreateErrorItem(ReasonTypes.UnexpectedExternalError);
			}
		}

		protected TItemResult CreateErrorItem(ReasonTypes error)
		{
			var cardResponse = new TItemResult();

			cardResponse.UpdateErrorResult(error);
			return cardResponse;
		}

		protected virtual void HandleNonSvcTransactions(TResult result, IEnumerable<OrderItem> items)
		{
			if (items == null || !items.Any())
			{
				return;
			}

			foreach (var item in items)
			{
				result.Results.Add(CreateErrorItem(ReasonTypes.NotSvcOrder));
			}
		}

	}
}
