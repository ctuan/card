﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Helpers;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;
using Starbucks.FraudRecovery.Dal.Common;
using Account.Provider.Common;
using Account.Provider.Common.Models;
using Starbucks.Platform.Security;
using BillingInfo = Starbucks.FraudRecovery.Dal.Common.Models.BillingInfo;
using Sdk = Starbucks.Api.Sdk.PaymentService;
using Starbucks.CardIssuer.Dal.SvDot.Db;

namespace Starbucks.FraudRecovery.Provider.Commands
{
	public class TransactionRevokeCommand : TransactionIdWithCollectionBasedCommand<TransactionRevokeResponse, SvcCardRevokeAmountResponse, ICardIssuerDal>
	{
		public TransactionRevokeCommand(string transactionId, IDependencyFactory dependency)
			: base(transactionId, dependency == null ? null : dependency.CardIssuer, dependency, null)
		{
		}

		protected override SvcCardRevokeAmountResponse GetCardOperationResponse(string traceNumber, Dal.Common.Models.SvcCardInfo cardInfo, Dal.Common.Models.OrderItem orderItem)
		{
            var cmd = new SvcCardRevokeAmountCommand(cardInfo, orderItem.ToOrderItemTotal(), this.DependencyFactory, traceNumber, Currency);
			return cmd.Execute();
		}
	}


	public class TransactionFreezeCommand : TransactionIdWithCollectionBasedCommand<TransactionFreezeResponse, SvcCardFreezeResponse, ICardIssuerDal>
	{
		public TransactionFreezeCommand(string transactionId, IDependencyFactory dependency)
			: base(transactionId, dependency == null ? null : dependency.CardIssuer, dependency, null)

		{
		}

        protected override SvcCardFreezeResponse GetCardOperationResponse(string traceNumber, Dal.Common.Models.SvcCardInfo cardInfo, Dal.Common.Models.OrderItem orderItem)
		{
            var cmd = new SvcCardFreezeCommand(cardInfo, this.DependencyFactory, traceNumber);
			return cmd.Execute();
		}
	}


	public class TransactionUnfreezeCommand : TransactionIdWithCollectionBasedCommand<TransactionUnfreezeResponse, SvcCardUnfreezeResponse, ICardIssuerDal>
	{
		public TransactionUnfreezeCommand(string transactionId, IDependencyFactory dependency)
			: base(transactionId, dependency==null? null : dependency.CardIssuer, dependency, null)
		{
		}

		protected override SvcCardUnfreezeResponse GetCardOperationResponse(string traceNumber, Dal.Common.Models.SvcCardInfo cardInfo, Dal.Common.Models.OrderItem orderItem)
		{
            var cmd = new SvcCardUnfreezeCommand(cardInfo, this.DependencyFactory, traceNumber);
			return cmd.Execute();
		}
	}


	public class TransactionRefundCommand : TransactionIdBasedCommand<TransactionRefundResponse,  Sdk.IPaymentServiceApiClient>
	{
        protected IAccountProvider AccountProvider { get; private set; }
        protected ICardTransactionDal CardTransactionDal { get; private set; }
        protected IFraudRecoveryDal FraudRecoveryDal { get; private set; }

        public TransactionRefundCommand(string transactionId, IDependencyFactory dependency, string parentTrackNumber, IAccountProvider accountProvider, ICardTransactionDal cardTransactionDal, IFraudRecoveryDal fraudRecoveryDal)
			: base(transactionId, dependency == null ? null : dependency.PaymentServiceApiClient, RecoveryActionTypes.TransactionWithCreditCard, dependency,
			dependency == null ? null : dependency.GetTransactionWithCreditCardActionAuditor<TransactionRefundResponse>(), parentTrackNumber)
		{
            this.AccountProvider = accountProvider;
            this.CardTransactionDal = cardTransactionDal;
            this.FraudRecoveryDal = fraudRecoveryDal;
		}

		protected override void HandleOrder(TransactionRefundResponse result, Dal.Common.Models.OrderInfo order)
		{
            if (order.OrderType.Equals("eGift", StringComparison.InvariantCultureIgnoreCase) ||
                order.OrderType.Equals("Standard", StringComparison.InvariantCultureIgnoreCase))
            {
                result.UpdateErrorResult(ReasonTypes.NotSvcOrder);
                return;
            }

            var refundCmd = new CreditCardRefundCommand(this.ActionProvider, this.Configuration, this.Logger, result, order, null, AccountProvider, CardTransactionDal, FraudRecoveryDal);
			var refundResult = refundCmd.Execute();
			result.Result = refundResult;
		}

		protected override TransactionRefundResponse FinalizeResult(TransactionRefundResponse result)
		{
			if (result.Result == null && result.Reason == null)
			{
				result.UpdateErrorResult(ReasonTypes.UnexpectedInternalError);
			}
			return result;
		}

	}
}
