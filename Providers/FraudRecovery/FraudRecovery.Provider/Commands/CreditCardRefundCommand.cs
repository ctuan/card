﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common.Helpers;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;
using Account.Provider.Common;
using Account.Provider.Common.Models;
using Sdk = Starbucks.Api.Sdk.PaymentService;
using Starbucks.FraudRecovery.Common;
using SdkModels = Starbucks.Api.Sdk.PaymentService.Models;
using DalModels = Starbucks.FraudRecovery.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.SvDot.Db;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.FraudRecovery.Dal.Common;

namespace Starbucks.FraudRecovery.Provider.Commands
{
    public class CreditCardRefundCommand : CommandBase<CreditCardOperationResult, Sdk.IPaymentServiceApiClient>
    {
        private readonly IAccountProvider _accountProvider;
        private readonly ICardTransactionDal _cardTransactionDal;
        private readonly IFraudRecoveryDal _fraudRecoveryDal;

        private const string UnKnownPlatform = "unknown";

        public CreditCardRefundCommand(Sdk.IPaymentServiceApiClient apiClient, IFraudRecoveyConfiguration configuration, ILoggingUtility logging
            , OperationResponse result, DalModels.OrderInfo orderInfo, DalModels.BillingInfo billing, IAccountProvider accountProvider, ICardTransactionDal cardTransactionDal, IFraudRecoveryDal fraudRecoveryDal)
            : base(apiClient, configuration, logging)
        {
            result.EnsureObjectNotNull("caller result object is required");
            orderInfo.EnsureObjectNotNull("orderInfo is required");

            _accountProvider = accountProvider;
            _cardTransactionDal = cardTransactionDal;
            _fraudRecoveryDal = fraudRecoveryDal;

            this.CallerResult = result;
            this.OrderInfo = orderInfo;
            this.BillingInfo = billing;
        }

        protected OperationResponse CallerResult { get; private set; }
        protected DalModels.OrderInfo OrderInfo { get; private set; }
        protected DalModels.BillingInfo BillingInfo { get; private set; }

        public override CreditCardOperationResult Execute()
        {
            var result = base.Execute();
            if (result == null && CallerResult.Reason == null)
            {
                CallerResult.UpdateErrorResult(ReasonTypes.UnexpectedInternalErrorQueryPaymentMethod);
            }

            return result;
        }

        protected override CreditCardOperationResult DoExecute(CreditCardOperationResult result)
        {
            string market = GetUserMarket(this.OrderInfo.UserId);
            IMerchantInfo merchantInfo =
                _cardTransactionDal.GetMerchantInfo(market, UnKnownPlatform);

            SdkModels.CreditRequest sdkRequest = GetSdkRequest(this.BillingInfo, this.OrderInfo, merchantInfo);
            if (sdkRequest == null)
            {
                return null;
            }

            if (sdkRequest.TransactionType == SdkModels.TransactionTypes.Egift)
            {
                CallerResult.UpdateErrorResult(ReasonTypes.CannotRefundEGift);
                return null;
            }

            try
            {
                var sdkResult = this.ActionProvider.Credit(sdkRequest);
                return sdkResult.ToModel(CallerResult, sdkRequest.CurrencyCode, sdkRequest, merchantInfo, _fraudRecoveryDal);
            }
            catch (Exception ex)
            {
                ReportException(ex, LoggingTitle);
                var errorResult = CallerResult.UpdateErrorResult(ReasonTypes.UnexpectedInternalErrorQueryPaymentMethod, "-1", ex.ToString());
                CreditCardOperationResult creditCardOperationResult = new CreditCardOperationResult()
                {
                    Amount = (sdkRequest.Basket == null || sdkRequest.Basket.Count == 0) ? "0.0" : sdkRequest.Basket[0].Amount.ToString(),
                    CurrencyCode = sdkRequest.CurrencyCode,
                    CreditRequest = sdkRequest,
                    Decision = errorResult.Status.ToString()
                };

                return creditCardOperationResult;
            }
        }

        private bool HasEnoughBillingInfo(Dal.Common.Models.OrderInfo order)
        {
            if (string.IsNullOrWhiteSpace(order.FirstName))
                return false;

            if (string.IsNullOrWhiteSpace(order.LastName))
                return false;

            if (string.IsNullOrWhiteSpace(order.SurrogateAccountNumber))
                return false;

            if (string.IsNullOrWhiteSpace(order.CcType))
                return false;

            if (string.IsNullOrWhiteSpace(order.SavedCcExpiration))
                return false;

            return true;
        }

        private string GetUserMarket(string userId)
        {
            string market = "US";
            if (_accountProvider != null)
            {
                IUser user = _accountProvider.GetUser(userId);
                if (user != null)
                {
                    market = user.SubMarket;
                }
            }

            return market;
        }

        private SdkModels.AddressInfo GetAddressInfo(IOrderAddressInfo orderAddressInfo)
        {
            if (orderAddressInfo == null)
                return null;

            SdkModels.AddressInfo addressInfo = new SdkModels.AddressInfo()
            {
                AddressDetails = new SdkModels.Address()
                {
                    AddressLine1 = orderAddressInfo.AddressLine1,
                    AddressLine2 = orderAddressInfo.AddressLine2,
                    City = orderAddressInfo.City,
                    CountrySubdivision = orderAddressInfo.Region,
                    Country = orderAddressInfo.Country,
                    PostalCode = orderAddressInfo.PostalCode,
                    PhoneNumber = orderAddressInfo.PhoneNumber,
                    FirstName = orderAddressInfo.FirstName,
                    LastName = orderAddressInfo.LastName
                }
            };

            return addressInfo;
        }

        private SdkModels.CreditCardTypes GetCcType(string CcType)
        {
            if (string.IsNullOrWhiteSpace(CcType))
                return SdkModels.CreditCardTypes.None;

            string creditCardType = CcType.ToUpper();
            if (creditCardType.Contains("VISA"))
                return SdkModels.CreditCardTypes.Visa;

            if (creditCardType.Contains("MASTER"))
                return SdkModels.CreditCardTypes.MasterCard;

            if (creditCardType.Contains("AMEX"))
                return SdkModels.CreditCardTypes.Amex;

            if (creditCardType.Contains("DISCOVER"))
                return SdkModels.CreditCardTypes.Discover;

            return SdkModels.CreditCardTypes.None;

        }

        private SdkModels.PaymentMethod GetPaymentMethodInfo(DalModels.OrderInfo order)
        {
            if (order == null)
                return null;

            // order.SavedCcExpiration is in 082017 format. first 2 chars resprenent month
            if (string.IsNullOrWhiteSpace(order.SavedCcExpiration) || order.SavedCcExpiration.Length != 6)
                return null;

            if (string.IsNullOrWhiteSpace(order.FirstName) && string.IsNullOrWhiteSpace(order.LastName))
                return null;

            int month;
            if (!int.TryParse(order.SavedCcExpiration.Substring(0, 2), out month))
                return null;

            int year;
            if (!int.TryParse(order.SavedCcExpiration.Substring(2, 4), out year))
                return null;

            SdkModels.PaymentMethod paymentMethod = new SdkModels.PaymentMethod()
            {
                CreditCardDetails = new SdkModels.CreditCardDetails()
                {
                    ExpireMonth = month,
                    ExpireYear = year,
                    SurrogateNumber = order.SurrogateAccountNumber,
                    FullName = (order.FirstName + " " + order.LastName).Trim(),
                    Type = GetCcType(order.CcType)
                }
            };

            return paymentMethod;
        }

        private SdkModels.BillingInfo GetBillingInfoFromOrder(DalModels.OrderInfo order, IOrderAddressInfo orderAddressInfo)
        {
            if (order == null || orderAddressInfo == null)
            {
                return null;
            }

            var native = new SdkModels.BillingInfo()
            {
                AddressInfo = GetAddressInfo(orderAddressInfo),
                Email = order.UserEmail,
                PaymentMethod = GetPaymentMethodInfo(order),
            };

            return native;
        }

        protected Sdk.Models.CreditRequest GetSdkRequest(DalModels.BillingInfo billing,
                                                        Dal.Common.Models.OrderInfo order,
                                                        IMerchantInfo merchantInfo)
        {
            var item = order.Items.FirstOrDefault();
            if (item == null)
            {
                CallerResult.UpdateErrorResult(ReasonTypes.OrderWithoutItem);
                return null;
            }

            Sdk.Models.CreditRequest request = null;
            if (!string.IsNullOrWhiteSpace(order.BillingRequestId))
            {
                string market = GetUserMarket(order.UserId);
                request = new Sdk.Models.CreditRequest()
                {
                    Market = market,
                    Basket = order.ToBasket(),
                    CurrencyCode = order.Currency,
                    CaptureRequestId = order.BillingRequestId,
                    MerchantReferenceCode = order.OrderId
                };
            }
            else if (HasEnoughBillingInfo(order))
            {
                IOrderAddressInfo orderAddressInfo =
                    _cardTransactionDal.GetOrderGroupAddress(order.OrderId);

                if (!merchantInfo.CurrencyCode.Equals(order.Currency))
                {
                    Logger.Log(LogType.Error, "Refund Error", "Cannot support cross market refund", "",
                                new Dictionary<string, object>()
                                    {
                                        {"OrderCurrency", order.Currency?? "<NULL>"},
                                        {"merchantInfoCurrencyCode", merchantInfo.CurrencyCode?? "<NULL>"}
                                    }
                                );
                    return null;
                }

                request = new Sdk.Models.CreditRequest()
                {
                    MerchantReferenceCode = order.OrderId,
                    Market = (merchantInfo != null) ? merchantInfo.SubMarketId : null,
                    Basket = order.ToBasket(),
                    CurrencyCode = order.Currency,
                    BillingInfo = GetBillingInfoFromOrder(order, orderAddressInfo),
                    UserId = order.UserId,
                    TransactionType = item.ToTransactionType(),
                };
            }
            if (!request.IsValid())
            {
                CallerResult.UpdateErrorResult(ReasonTypes.OrderWithInvalidPaymentFields);
                return null;
            }

            return request;
        }

        protected override CreditCardOperationResult HandleException(Exception ex, CreditCardOperationResult result)
        {
            ReportException(ex, this.LoggingTitle);
            CallerResult.UpdateErrorResult(ReasonTypes.UnexpectedInternalError);
            return result;
        }

        protected override CreditCardOperationResult CreateResult()
        {
            return null;
        }
    }
}