﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Helpers;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;
using Starbucks.FraudRecovery.Dal.Common;
using Starbucks.Platform.Security;
using DalModels = Starbucks.FraudRecovery.Dal.Common.Models;

namespace Starbucks.FraudRecovery.Provider.Commands
{
	public class PaymentMethodInquireCommand : CommandBase<DalModels.BillingInfo, IFraudRecoveryDal>
	{
		public PaymentMethodInquireCommand(IFraudRecoveryDal dal, IFraudRecoveyConfiguration configuration, ILoggingUtility logging, OperationResponse result, DalModels.OrderInfo orderInfo )
			: base (dal, configuration, logging)
		{
			result.EnsureObjectNotNull("caller result object is required");
			orderInfo.EnsureObjectNotNull("orderInfo is required");

			this.CallerResult = result;
			this.OrderInfo = orderInfo;
		}

		protected OperationResponse CallerResult { get; private set; }
		protected DalModels.OrderInfo OrderInfo { get; private set; }

		public override DalModels.BillingInfo Execute()
		{
			var result = base.Execute();
			return result;
		}

		protected override DalModels.BillingInfo HandleException(Exception ex, DalModels.BillingInfo result)
		{
			ReportException(ex, this.LoggingTitle);
			CallerResult.UpdateErrorResult(ReasonTypes.UnexpectedInternalError);
			return result;
		}

		protected override DalModels.BillingInfo CreateResult()
		{
			return null;
		}


		protected override DalModels.BillingInfo DoExecute(DalModels.BillingInfo initResult)
		{
			if (string.IsNullOrWhiteSpace(OrderInfo.PaymentMethodId))
			{
				CallerResult.UpdateErrorResult( ReasonTypes.OrderWithoutPaymentMethod);
				return null;
			}
			int rawId = OrderInfo.PaymentMethodId.ToInt();
			if (rawId < 0)
			{
				CallerResult.UpdateErrorResult(ReasonTypes.OrderWithInvalidPaymentId);
				return null;
			}

			var billing = GetBilling(OrderInfo, rawId);
			return billing;
		}


		protected DalModels.BillingInfo GetBilling(Dal.Common.Models.OrderInfo order, int rawId)
		{
			DalModels.BillingInfo billing = null;
			try
			{
				billing = this.ActionProvider.GetBilling(order.UserId, Encryption.EncryptCardId(rawId));
			}
			catch (Exception ex)
			{
				ReportException(ex, LoggingTitle);
                CallerResult.UpdateErrorResult(ReasonTypes.UnexpectedInternalErrorQueryPaymentMethod, "-1", ex.ToString());
				return null;
			}

            if (billing != null)
            {
                billing.Email = order.UserEmail;
            }

			return billing;
		}

	}
}
