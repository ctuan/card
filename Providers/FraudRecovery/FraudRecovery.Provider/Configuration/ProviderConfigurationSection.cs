﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;
using Starbucks.FraudRecovery.Provider.Facades;


namespace Starbucks.FraudRecovery.Provider.Configuration
{
	/// <summary>
	/// 
	/// </summary>
	public class ProviderConfigurationSection : ConfigurationSection, IFraudRecoveyConfiguration
	{
		public static IFraudRecoveyConfiguration GetConfiguration(ILoggingUtility logging, string sectionName = "FraudRecoverySettings")
		{
			logging.EnsureObjectNotNull("logging is required");

			IFraudRecoveyConfiguration result = null;
			try
			{
				var section = ConfigurationManager.GetSection(sectionName);
				var providerSection = section.EnsureObjectIsType<ProviderConfigurationSection>("missing configuration section: " + sectionName);
				result = providerSection.Configuration;
				result.EnsureObjectNotNull("missing configuration details: " + sectionName);
			}
			catch (Exception ex)
			{
				logging.Log(LogType.Error, "FraudRecoveryConfiguration", ex.ToString(), "check FraudRecoveryProvider section in the configuration file");
			}
			return result;
		}

		public ProviderConfigurationSection()
		{
			this.Configuration = null;
		}

		public IFraudRecoveyConfiguration Configuration { get; set; }

		#region IFraudRecoveyConfiguration
		string IFraudRecoveyConfiguration.MarketPrefix
		{
			get
			{
				this.Configuration.EnsureObjectNotNull("missing configuration");
				return this.Configuration.MarketPrefix;
			}
		}

		bool IFraudRecoveyConfiguration.IsPinless
		{
			get
			{
				this.Configuration.EnsureObjectNotNull("missing configuration");
				return this.Configuration.IsPinless;
			}
		}

		decimal IFraudRecoveyConfiguration.UnfreezeRedeemAmount
		{
			get
			{
				this.Configuration.EnsureObjectNotNull("missing configuration");
				return this.Configuration.UnfreezeRedeemAmount;
			}
		}

		public AuditorConfiguration AuditingConfig
		{
			get
			{
				this.Configuration.EnsureObjectNotNull("missing configuration");
				this.Configuration.AuditingConfig.EnsureObjectNotNull("missing auditing configuration");
				return this.Configuration.AuditingConfig;
			}
		}


		ConnectionString[] IFraudRecoveyConfiguration.ConnectionStrings
		{
			get
			{
				this.Configuration.EnsureObjectNotNull("missing configuration");
				return this.Configuration != null ? this.Configuration.ConnectionStrings : null;
			}
		}

		MarketConfiguraiton IFraudRecoveyConfiguration.GetMaretConfig(string market)
		{
			this.Configuration.EnsureObjectNotNull("missing configuration");
			return this.Configuration != null ? this.Configuration.GetMaretConfig(market) : null;
		}
		#endregion

		protected override void DeserializeSection(XmlReader reader)
		{
			try
			{
				XmlDocument xDoc = new XmlDocument();
				xDoc.Load(reader);
				string configXml = xDoc.DocumentElement != null ? xDoc.DocumentElement.InnerXml : String.Empty;
				Debug.WriteLine(configXml);

				XmlSerializer serializer = SerializerHelper.GetSerializer(typeof(FraudRecoveryConfiguration));
				var configObject = serializer.Deserialize<FraudRecoveryConfiguration>(configXml);
				this.Configuration = configObject;
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.ToString());
				throw;
			}
		}
	}
}
