﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.Api.Sdk.PaymentService;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;
using Starbucks.FraudRecovery.Dal.Common;
using Starbucks.FraudRecovery.Provider.Commands;
using Starbucks.FraudRecovery.Provider.Configuration;
using Starbucks.FraudRecovery.Provider.Facades;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Models;
using Account.Provider.Common;
using Starbucks.CardIssuer.Dal.SvDot.Db;

namespace Starbucks.FraudRecovery.Provider
{
	public class FraudRecoveryProvider : IFraudRecoveryProvider
	{
        public FraudRecoveryProvider(IDependencyFactory dependencyFactory, 
                                    IAccountProvider accountProvider, 
                                    ICardTransactionDal cardTransactionDal,
                                    IFraudRecoveryDal fraudRecoveryDal)
		{
			dependencyFactory.EnsureObjectNotNull("dependencyFactory is required");

			this.DependencyFactory = dependencyFactory;
            this.AccountProvider = accountProvider;
            this.CardTransactionDal = cardTransactionDal;
            this.FraudRecoveryDal = fraudRecoveryDal;
		}

		protected IDependencyFactory DependencyFactory { get; private set; }
        protected IAccountProvider AccountProvider { get; private set; }
        protected ICardTransactionDal CardTransactionDal { get; private set; }
        protected IFraudRecoveryDal FraudRecoveryDal { get; private set; }

		#region transaction related opertion

		public TransactionRevokeResponse RevokeTransaction(string transactionId)
		{
			var command = new TransactionRevokeCommand(transactionId, this.DependencyFactory);
			return command.Execute();
		}

		public TransactionUnfreezeResponse  UnfreezeTransaction(string transactionId)
		{
			var command = new TransactionUnfreezeCommand(transactionId, this.DependencyFactory);
			return command.Execute();
		}

		public  TransactionFreezeResponse FreezeTransaction(string transactionId)
		{
			var command = new TransactionFreezeCommand(transactionId, this.DependencyFactory);
			return command.Execute();
		}


		public Common.Models.TransactionRefundResponse RefundTransaction(string transactionId)
		{
            var command = new TransactionRefundCommand(transactionId, this.DependencyFactory, null, AccountProvider, CardTransactionDal, FraudRecoveryDal);
			return command.Execute();
		}

		#endregion

		#region svc card related operations

		public Common.Models.SvcCardGetBalanceResponse SvcCardBalance(int decryptCardId)
		{
			var command = new SvcCardGetBalanceCommand(decryptCardId, this.DependencyFactory);
			return command.Execute();
		}

		public Common.Models.SvcCardRevokeAmountResponse SvcCardRevokeAmount(int decryptCardId, decimal amount)
		{
            var command = new SvcCardRevokeAmountCommand(decryptCardId, amount, this.DependencyFactory, AccountProvider);
			return command.Execute();
		}

		public Common.Models.SvcCardCashOutResponse SvcCardCashOut(int decryptCardId)
		{
			var command = new SvcCardCashoutCommand(decryptCardId, this.DependencyFactory);
			return command.Execute();
		}

		public Common.Models.SvcCardFreezeResponse SvcCardFreeze(int decryptCardId)
		{
			var command = new SvcCardFreezeCommand(decryptCardId, this.DependencyFactory);
			return command.Execute();
		}

		public Common.Models.SvcCardUnfreezeResponse SvcCardUnfreeze(int decryptCardId)
		{
			var command = new SvcCardUnfreezeCommand(decryptCardId, this.DependencyFactory);
			return command.Execute();
		}

		public Common.Models.SvcCardLoadAmountResponse SvcCardLoadAmount(int decryptCardId, decimal amount)
		{
			var command = new SvcCardLoadAmountCommand(decryptCardId, amount, this.DependencyFactory);
			return command.Execute();
		}

		#endregion
	}
}
