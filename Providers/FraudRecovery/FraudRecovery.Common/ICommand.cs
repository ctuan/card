﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common.Models;

namespace Starbucks.FraudRecovery.Common
{
	public interface ICommand<TResult> 
	{
		TResult Execute();
	}
}
