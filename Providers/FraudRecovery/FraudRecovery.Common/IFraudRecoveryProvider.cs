﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common.Models;

namespace Starbucks.FraudRecovery.Common
{
	public interface IFraudRecoveryProvider
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="transactionId"></param>
		/// <returns></returns>
		TransactionRefundResponse RefundTransaction(string transactionId);
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="transactionId"></param>
		/// <returns></returns>
		TransactionRevokeResponse RevokeTransaction(string transactionId);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="transactionId"></param>
		/// <returns></returns>
		TransactionUnfreezeResponse  UnfreezeTransaction(string transactionId);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="transactionId"></param>
		/// <returns></returns>
		TransactionFreezeResponse FreezeTransaction(string transactionId);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="decryptCardId"></param>
		/// <returns></returns>
		SvcCardGetBalanceResponse SvcCardBalance(int decryptCardId);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="decryptCardId"></param>
		/// <param name="amount"></param>
		/// <returns></returns>
		SvcCardRevokeAmountResponse SvcCardRevokeAmount(int decryptCardId, decimal amount);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="decryptCardId"></param>
		/// <returns></returns>
		SvcCardCashOutResponse SvcCardCashOut(int decryptCardId);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="decryptCardId"></param>
		/// <returns></returns>
		SvcCardFreezeResponse SvcCardFreeze(int decryptCardId);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="decryptCardId"></param>
		/// <returns></returns>
		SvcCardUnfreezeResponse SvcCardUnfreeze(int decryptCardId);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="decryptCardId"></param>
		/// <param name="amount"></param>
		/// <returns></returns>
		SvcCardLoadAmountResponse SvcCardLoadAmount(int decryptCardId, decimal amount);
	}
}
