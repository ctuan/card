﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18449
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Starbucks.FraudRecovery.Common.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class ReasonResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ReasonResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Starbucks.FraudRecovery.Common.Resources.ReasonResources", typeof(ReasonResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 650.
        /// </summary>
        public static string CannotRefundEGiftReasonCode {
            get {
                return ResourceManager.GetString("CannotRefundEGiftReasonCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, Can not refund EGift order.
        /// </summary>
        public static string CannotRefundEGiftReasonMessage {
            get {
                return ResourceManager.GetString("CannotRefundEGiftReasonMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3000.
        /// </summary>
        public static string ExternalServiceNoResponseReasonCode {
            get {
                return ResourceManager.GetString("ExternalServiceNoResponseReasonCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to External dependency service doesn&apos;t provide a valid response.
        /// </summary>
        public static string ExternalServiceNoResponseReasonMessage {
            get {
                return ResourceManager.GetString("ExternalServiceNoResponseReasonMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 4000.
        /// </summary>
        public static string ExternalServiceResponseWithErrorReasonCode {
            get {
                return ResourceManager.GetString("ExternalServiceResponseWithErrorReasonCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to External dependency service returns an error.
        /// </summary>
        public static string ExternalServiceResponseWithErrorReasonMessage {
            get {
                return ResourceManager.GetString("ExternalServiceResponseWithErrorReasonMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 700.
        /// </summary>
        public static string FailedQueryBillingInfoReasonCode {
            get {
                return ResourceManager.GetString("FailedQueryBillingInfoReasonCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to retrieve billing infomation.
        /// </summary>
        public static string FailedQueryBillingInfoReasonMessage {
            get {
                return ResourceManager.GetString("FailedQueryBillingInfoReasonMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 100.
        /// </summary>
        public static string InvalidCardIdReasonCode {
            get {
                return ResourceManager.GetString("InvalidCardIdReasonCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Starbucks Card Id is invalid..
        /// </summary>
        public static string InvalidCardIdReasonMessage {
            get {
                return ResourceManager.GetString("InvalidCardIdReasonMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 120.
        /// </summary>
        public static string InvalidCardReasonCode {
            get {
                return ResourceManager.GetString("InvalidCardReasonCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid Starbucks Card or missing Starbucks Card information.
        /// </summary>
        public static string InvalidCardReasonMessage {
            get {
                return ResourceManager.GetString("InvalidCardReasonMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 510.
        /// </summary>
        public static string InvalidTransactionReasonCode {
            get {
                return ResourceManager.GetString("InvalidTransactionReasonCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to find the order of transaction.
        /// </summary>
        public static string InvalidTransactionReasonMessage {
            get {
                return ResourceManager.GetString("InvalidTransactionReasonMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 180.
        /// </summary>
        public static string MissingMerchantInfoReasonCode {
            get {
                return ResourceManager.GetString("MissingMerchantInfoReasonCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to find merchant info.
        /// </summary>
        public static string MissingMerchantInfoReasonMessage {
            get {
                return ResourceManager.GetString("MissingMerchantInfoReasonMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 530.
        /// </summary>
        public static string MissingSvcInfoInTransactionReasonCode {
            get {
                return ResourceManager.GetString("MissingSvcInfoInTransactionReasonCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There is no Starbuck Card information in this transaction.
        /// </summary>
        public static string MissingSvcInfoInTransactionReasonMessage {
            get {
                return ResourceManager.GetString("MissingSvcInfoInTransactionReasonMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 500.
        /// </summary>
        public static string MissingTransactionIdReasonCode {
            get {
                return ResourceManager.GetString("MissingTransactionIdReasonCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Transaction Id is required.
        /// </summary>
        public static string MissingTransactionIdReasonMessage {
            get {
                return ResourceManager.GetString("MissingTransactionIdReasonMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 520.
        /// </summary>
        public static string NotSvcOrderReasonCode {
            get {
                return ResourceManager.GetString("NotSvcOrderReasonCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Order is not Starbuck Card transaction.
        /// </summary>
        public static string NotSvcOrderReasonMessage {
            get {
                return ResourceManager.GetString("NotSvcOrderReasonMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 630.
        /// </summary>
        public static string OrderWithInvalidPaymentFieldsReasonCode {
            get {
                return ResourceManager.GetString("OrderWithInvalidPaymentFieldsReasonCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid payment method; missing required field.
        /// </summary>
        public static string OrderWithInvalidPaymentFieldsReasonMessage {
            get {
                return ResourceManager.GetString("OrderWithInvalidPaymentFieldsReasonMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 620.
        /// </summary>
        public static string OrderWithInvalidPaymentIdReasonCode {
            get {
                return ResourceManager.GetString("OrderWithInvalidPaymentIdReasonCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Order doesn&apos;t have payment method id..
        /// </summary>
        public static string OrderWithInvalidPaymentIdReasonMessage {
            get {
                return ResourceManager.GetString("OrderWithInvalidPaymentIdReasonMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 600.
        /// </summary>
        public static string OrderWithoutItemReasonCode {
            get {
                return ResourceManager.GetString("OrderWithoutItemReasonCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Order doesn&apos;t have order item..
        /// </summary>
        public static string OrderWithoutItemReasonMessage {
            get {
                return ResourceManager.GetString("OrderWithoutItemReasonMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 610.
        /// </summary>
        public static string OrderWithoutPaymentMethodReasonCode {
            get {
                return ResourceManager.GetString("OrderWithoutPaymentMethodReasonCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Order doesn&apos;t have payment method.
        /// </summary>
        public static string OrderWithoutPaymentMethodReasonMessage {
            get {
                return ResourceManager.GetString("OrderWithoutPaymentMethodReasonMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 10000.
        /// </summary>
        public static string UnexpectedExternalErrorReasonCode {
            get {
                return ResourceManager.GetString("UnexpectedExternalErrorReasonCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, unexpected error from external dependency service.
        /// </summary>
        public static string UnexpectedExternalErrorReasonMessage {
            get {
                return ResourceManager.GetString("UnexpectedExternalErrorReasonMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1100.
        /// </summary>
        public static string UnexpectedInternalErrorQueryPaymentMethodReasonCode {
            get {
                return ResourceManager.GetString("UnexpectedInternalErrorQueryPaymentMethodReasonCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, unexpected internal error when query payment method.
        /// </summary>
        public static string UnexpectedInternalErrorQueryPaymentMethodReasonMessage {
            get {
                return ResourceManager.GetString("UnexpectedInternalErrorQueryPaymentMethodReasonMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1000.
        /// </summary>
        public static string UnexpectedInternalErrorReasonCode {
            get {
                return ResourceManager.GetString("UnexpectedInternalErrorReasonCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, unexpected internal error..
        /// </summary>
        public static string UnexpectedInternalErrorReasonMessage {
            get {
                return ResourceManager.GetString("UnexpectedInternalErrorReasonMessage", resourceCulture);
            }
        }
    }
}
