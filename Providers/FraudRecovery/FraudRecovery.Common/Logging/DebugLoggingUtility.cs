﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace Starbucks.FraudRecovery.Common.Logging
{
    public class DebugLoggingUtility : LoggingUtilityBase
    {
        private static Dictionary<string, decimal> _logCounterDictionary = new Dictionary<string, decimal>();

	    public static void PerformLogCounter(string countName, decimal increment)
	    {
		    decimal prev = 0;
		    decimal cur = increment;
		    string name = countName == null ? NullString : countName.Trim();

		    if (_logCounterDictionary.ContainsKey(name))
		    {
			    prev = _logCounterDictionary[name];
			    cur += prev;
			    _logCounterDictionary[name] = cur;
		    }
		    else
		    {
			    _logCounterDictionary.Add(name, cur);
		    }

			WriteToDebug(string.Format("{0} LogCounter[{1}] from {2} to {3}", DateTime.Now, name, prev,cur));
		}


		public DebugLoggingUtility()
			: this(false)
		{
		}

		public DebugLoggingUtility(bool flushDataFlag )
			: this("Root", PerformLogCounter, flushDataFlag, true, null)
		{
		}
		protected DebugLoggingUtility(string name, Action<string, decimal> logCounterAction, bool flushDataFlag, bool localSerializationValidation, LoggingRepository parent )
			: base(name, logCounterAction, localSerializationValidation, parent)
		{
			this.CanFlushData = flushDataFlag;
		}

		[JsonIgnore]
		protected bool CanFlushData { get; private set; }

	    protected override LoggingRepository CreateNewRepository(string name, Action<string, decimal> logCounterAction, LoggingRepository parent)
		{
			return new DebugLoggingUtility(name, logCounterAction, this.CanFlushData, true, parent);
		}

        protected override void PerformLog(LogType type, string title, string message, string troubleShootingHint,
	                                               Dictionary<string, object> dumps)
	    {
            base.PerformLog(type, title, message, troubleShootingHint, dumps);
			
			StringBuilder builder = new StringBuilder();

			builder.AppendLine(string.Format("{0} [{1}]: ", DateTime.Now, type.ToString()));
			builder.AppendLine(string.Format("\t\tMessage: {0}", message ?? NullString));
			builder.AppendLine(string.Format("\t\tHint:{0}", troubleShootingHint ?? NullString));

            if (dumps != null)
			{
                foreach (var item in dumps)
				{
                    if (item.Value != null)
                    {
                        string objString = SerializeObject(item.Value);
                        builder.AppendLine(string.Format("\t\tObject[{0}]: {1}", item.Key, objString));
                    }
				}
			}
			builder.AppendLine();

			WriteToDebug(builder.ToString());
	    }

		protected override void FlushLog(LoggingUtilityBase.FlushData data)
		{
			if (data != null && this.CanFlushData)
			{
				string msg = string.Format("{0} [{1}]: <{2}> {3}", DateTime.Now, data.LogType, data.Title?? string.Empty, data.Message?? string.Empty);
				WriteToDebug(msg);
			}
		}

	    protected static void WriteToDebug(string msg)
	    {
			System.Diagnostics.Debug.WriteLine(msg);
			System.Diagnostics.Debug.WriteLine("----------------------------------------------------------------------");
			System.Diagnostics.Debug.WriteLine("");
	    }
    }
}
