﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Starbucks.FraudRecovery.Common.Logging
{
	public abstract class LoggingUtilityBase : LoggingRepository, ILoggingUtilityCommittable
	{
		public class FlushData
		{
			public LogType LogType { get; set; }
			public String Title { get; set; }
			public String KeyEventPath { get; set; } 
			public String Message { get; set; }
		}

		protected abstract void FlushLog(FlushData data);


		protected LoggingUtilityBase(string name, Action<string, decimal> logCounterAction, bool localValidation, LoggingRepository parent)
			: base(name, logCounterAction, localValidation, parent)
		{
		}

		public virtual void Commit()
		{
			try
			{
				var data = GetFlushData(this);
				FlushLog(data);
			}
			catch (Exception ex)
			{
				// have to eat exception.  In logging model, we don't have fallover report system.
				System.Diagnostics.Debug.WriteLine(ex.ToString());
			}
			finally
			{
				Cleanup();
			}

		}

		protected virtual FlushData GetFlushData(LoggingRepository repository)
		{
			FlushData data = new FlushData()
				{
					LogType = LogType.Debug,
					Title = null,
					Message = null,
				};

			if (!BuildFlushData(repository, data, ""))
			{
				return null;
			}

			try
			{
				data.Message = SerializeObject(repository);
			}
			catch (Exception ex)
			{		
				data.Message = "Unable to serialize LoggingRepository: " + ex.ToString();
			}
			return data;
		}

		protected override Newtonsoft.Json.JsonSerializerSettings GetSerializerSettings()
		{
			var setting = base.GetSerializerSettings();
			setting.Converters.Add(new JsonMaskTriggerConverter());
			return setting;
		}

		protected bool BuildFlushData(LoggingRepository repository, FlushData data, string parentPath)
		{
			if (repository == null || data == null)
			{
				throw new ArgumentNullException("repository and flushdata objects are required");
			}
			bool result = repository.Entries.Any();
			string localPath = string.IsNullOrWhiteSpace(parentPath)
				                   ? repository.Name
				                   : string.Format("{0} -> {1}", parentPath, repository.Name);

			foreach (var item in repository.Entries)
			{
				if (item != null && item.LogType >= data.LogType)
				{
					data.LogType = item.LogType;
					data.Title = item.Title;
					data.KeyEventPath = localPath;
				}
			}

			foreach (var child in repository.ChildRepositories.Values)
			{
				if (child != null && child is LoggingRepository)
				{
					var childResult = BuildFlushData(child as LoggingRepository, data, localPath);
					result = result || childResult;
				}
			}

			return result;
		}
	}
}
