﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Diagnostics;


namespace Starbucks.FraudRecovery.Common.Logging
{
	public abstract class LoggingRepository : ILoggingUtility, IRepository<LoggingEntry>
	{
		protected const string NullString = "<NULL>";
		
		protected abstract LoggingRepository CreateNewRepository(string name, Action<string, decimal> logCounterAction, LoggingRepository parent);

		private LoggingRepository() : this(null, null, false, null)
		{
		}

		internal LoggingRepository(string name, Action<string, decimal> logCounterAction, bool localValidation, LoggingRepository parent)
		{
			name.EnsureStringNotNullOrEmpty("name is required");
			logCounterAction.EnsureObjectNotNull("logCounterAction is required");

			this.Name = name;
			this.LogCounterAction = logCounterAction;
			this.LocalSerializationValidation = localValidation;

			this.ChildRepositories = new Dictionary<string, IRepository<LoggingEntry>>();
			this.Entries = new List<LoggingEntry>();
			this.Parent = parent;
		}

		public virtual void Cleanup()
		{
			try
			{
				this.Entries.Clear();
				foreach (var key in this.ChildRepositories.Keys)
				{
					var child = this.ChildRepositories[key];
					child.Cleanup();
				}
				this.ChildRepositories.Clear();
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.ToString());
			}
			finally
			{
				this.Entries = new List<LoggingEntry>();
				this.ChildRepositories = new Dictionary<string, IRepository<LoggingEntry>>();
			}
		}


		public string Name { get; private set; }
		public IDictionary<string, IRepository<LoggingEntry>> ChildRepositories { get; private set; }
		public IList<LoggingEntry> Entries { get; private set; }


		[JsonIgnore]
		public IRepository<LoggingEntry> Parent { get; private set; }

		[JsonIgnore]
		protected Action<string, decimal> LogCounterAction { get; private set; }

		[JsonIgnore]
		protected bool LocalSerializationValidation { get; private set; }

		#region ILoggingUtility

		public ILoggingUtility GetChildLoggingUtility(string name)
		{
			LoggingRepository child = null;
			if (this.ChildRepositories.ContainsKey(name))
			{
				child = this.ChildRepositories[name] as LoggingRepository;
			}
			else
			{
				child = CreateNewRepository (name, this.LogCounterAction, this);
				this.ChildRepositories.Add(name, child);
			}

			return child;
		}

		public void Log(LogType type, string title, string message)
		{
			Log(type, title, message, string.Empty);
		}

		public void Log(LogType type, string title, string message, string troubleShootingHint)
		{
			Log(type, title, message, troubleShootingHint, null);
		}

		public void Log(LogType type, string title, string message, string troubleShootingHint, Dictionary<string, object> dumps)
		{
			PerformLog(type, title, message, troubleShootingHint, dumps);
		}

		public void LogCounter(string countName, decimal increment)
		{
			try
			{
				LogCounterAction(countName, increment);
			}
			catch (Exception ex)
			{
				// logging failure, we have to eat it
				System.Diagnostics.Debug.WriteLine(ex.ToString());
			}
		}

		public void LogCounter(string countName, int increment = 1)
		{
			LogCounter(countName, (decimal)increment);
		}

		public virtual void AddEntry(LoggingEntry entry)
		{
			if (entry == null)
			{
				return;
			}
			lock (this)
			{
				try
				{
					this.Entries.Add(entry);
				}
				catch (Exception ex)
				{
					// logging, eat exception
					System.Diagnostics.Debug.WriteLine( ex.ToString());
				}
			}
		}

		#endregion

		#region virtual function

        protected virtual void PerformLog(LogType type, string title, string message, string troubleShootingHint, Dictionary<string, object> properties)
		{
			try
			{
                var logEntry = new LogEntry();
                logEntry.Categories = new string[] { "APILog" };
                logEntry.Title = title;
                logEntry.Severity = TraceEventType.Error;
                logEntry.Message = message;
                logEntry.ExtendedProperties = properties;
                Logger.Write(logEntry);
			}
			catch (Exception ex)
			{
				// logging failure, we have to eat it
				System.Diagnostics.Debug.WriteLine(ex.ToString());
			}
		}

		protected virtual Dictionary<string, object> GetDumpObjects(Dictionary<string, object> dumps)
		{
			var result = new Dictionary<string, object>();

			if (dumps != null)
			{
				foreach (var key in dumps.Keys)
				{
					object obj = dumps[key];
					object newObj = this.LocalSerializationValidation && !CanSerialize(obj) ? obj.ToString() : obj;
					result.Add(key, newObj);
				}
			}
			return result;
		}

		protected virtual string SerializeObject(object obj)
		{
			var result = Newtonsoft.Json.JsonConvert.SerializeObject(obj
			                                                         , Newtonsoft.Json.Formatting.Indented,
			                                                         GetSerializerSettings());

			return result;
		}

		protected virtual Newtonsoft.Json.JsonSerializerSettings GetSerializerSettings()
		{
			var settings = new Newtonsoft.Json.JsonSerializerSettings()
				{
					ReferenceLoopHandling =
						Newtonsoft.Json.ReferenceLoopHandling.Error
				};
			settings.Converters.Add(new StringEnumConverter());
			return settings;

		}

		protected virtual bool CanSerialize(object obj)
		{
			try
			{
				SerializeObject(obj);
				return true;
			}
			catch (System.Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.ToString());
				return false;
			}
		}


		#endregion

	}
}
