﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudRecovery.Common.Logging
{
	public class LoggingEntry
	{
		public LoggingEntry()
		{
			this.LoggingTime = DateTime.Now;
		}

		public DateTime LoggingTime { get; private set; }

		public LogType LogType { get; set; }
		public string Title { get; set; }
		public string Message { get; set; }
		public string TroubleShootingHint { get; set; }
		public Dictionary<string, object> DumpObjects { get; set; }
	}
}