﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Starbucks.FraudRecovery.Common.Logging
{
	[JsonConverter(typeof(StringEnumConverter))]
	public enum LogType
	{
		Debug = 0,

		Information = 10,

		Warning = 50,

		Error =100,
	}
}
