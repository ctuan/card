﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace  Starbucks.FraudRecovery.Common.Logging
{
	public interface ILoggingUtility
	{
		void Log(LogType type, string title, string message);

		void Log(LogType type, string title, string message, string troubleShootingHint);

		void Log(LogType type, string title, string message, string troubleShootingHint, Dictionary<string, object> dumps);

		void LogCounter(string countName, int increment = 1);

		void LogCounter(string countName, decimal increment);

		ILoggingUtility GetChildLoggingUtility(string name);
	}

	public interface ILoggingUtilityCommittable : ILoggingUtility
	{
		void Commit();
	}
}
