﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudRecovery.Common
{
	public interface ISqlDbFacade
	{
		void ExecuteNonQuery(string sprocName, string connectStringName, IList<SqlParameter> parameters);
	}
}
