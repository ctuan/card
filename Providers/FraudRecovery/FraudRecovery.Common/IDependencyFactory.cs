﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.Api.Sdk.PaymentService;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;
using Starbucks.FraudRecovery.Dal.Common;
using Starbucks.CardIssuer.Dal.Common;

namespace Starbucks.FraudRecovery.Common
{
	public interface IDependencyFactory
	{
		ICardIssuerDal CardIssuer { get; }
		IFraudRecoveryDal FraudRecoveryDal { get; }
		IPaymentServiceApiClient PaymentServiceApiClient { get; }
		IFraudRecoveyConfiguration Configuration { get; }
		ILoggingUtility Logging { get; }

		IFraudRecoveryAuditor<TResult> GetSvcCardActionAuditor<TResult>()
			where TResult : OperationResponseWithResult<SvcCardOperationResult>;

		IFraudRecoveryAuditor<TResult> GetTransactionWithCreditCardActionAuditor<TResult>()
			where TResult : OperationResponseWithResult<CreditCardOperationResult>;

		IFraudRecoveryAuditor<TResult> GetTransactionWithSvcCardActionAuditor<TResult, TItemResult>()
			where TItemResult : OperationResponseWithResult<SvcCardOperationResult>, new ()
			where TResult : OperationResponseCollectionResult<TItemResult>;
	}
}
