﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Starbucks.FraudRecovery.Common.Models
{
	[DataContract (Name = "status")]
	public enum OperationStatus
	{
		[EnumMember()]
		Unknown = 0,

		[EnumMember()]
		Succeeded = 1,

		[EnumMember()]
		PartialSucceeded = 3,

		[EnumMember()]
		Failed = -1,
	}
}