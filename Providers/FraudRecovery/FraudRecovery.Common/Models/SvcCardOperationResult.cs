﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudRecovery.Common.Models
{
	/// <summary>
	/// 
	/// </summary>
	[DataContract(Name = "cardOperationResult")]
	public class SvcCardOperationResult
	{
		[DataMember(Name = "currencyCode", Order = 1)]
		public string CurrencyCode { get; set; }

		[DataMember(Name = "amount", Order = 2)]
		public decimal Amount { get; set; }

		[DataMember(Name = "beginningBalance", Order = 5)]
		public decimal BeginningBalance { get; set; }

		[DataMember(Name = "endingBalance", Order = 6)]
		public decimal EndingBalance { get; set; }

		[DataMember(Name = "endingBalanceDateTime", Order = 7)]
		public string EndingBalanceDateTime { get; set; }

		[DataMember(Name = "lockAmount", Order = 8)]
		public decimal LockAmount { get; set; }

        [DataMember(Name = "mid", Order = 9)]
        public string Mid { get; set; }
	}

}