﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudRecovery.Common.Models
{
	public enum ReasonTypes
	{
		InvalidCardId = 1,
		InvalidCard = 2,
		MissingMerchantInfo = 3,

		MissingTransactionId = 50,
		InvalidTransaction = 51,
		NotSvcOrder = 52,
		MissingSvcInfoInTransaction = 55,

		OrderWithoutItem = 60,
		OrderWithoutPaymentMethod = 61,
		OrderWithInvalidPaymentId = 62,
		OrderWithInvalidPaymentFields = 63,
       
        CannotRefundEGift = 65,

		FailedQueryBillingInfo = 70,

		UnexpectedInternalError = 100,
		UnexpectedInternalErrorQueryPaymentMethod = 101,

		ExternalServiceNoResponse = 1100,
		ExternalServiceResponseWithError = 1200,

		UnexpectedExternalError = 6200,
	}
}
