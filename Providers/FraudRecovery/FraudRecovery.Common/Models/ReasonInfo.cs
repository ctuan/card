﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Starbucks.FraudRecovery.Common.Helpers;

namespace Starbucks.FraudRecovery.Common.Models
{
	[Serializable]
	[XmlRoot("ErrorCode")]
	public class ReasonInfo
	{
		public ReasonInfo() : this (ReasonTypes.UnexpectedInternalError)
		{

		}

		public ReasonInfo(ReasonTypes type, string nativeCode = "")
		{
			this.Code = type.GetErrorCodeFromResource();
			this.Message = type.GetErrorMessageFromResource();
			this.NativeReasonCode = nativeCode;
		}

		[DataMember(Name = "code", Order = 1)]
		public string Code { get; set; }

		[DataMember(Name = "message", Order = 2)]
		public string Message { get; set; }


		[DataMember(Name = "nativeReasonCode", Order = 5)]
		public string NativeReasonCode { get; set; }

	}
}
