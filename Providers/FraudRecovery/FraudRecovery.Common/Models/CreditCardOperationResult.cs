﻿using Starbucks.Api.Sdk.PaymentService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudRecovery.Common.Models
{
	[DataContract(Name = "creditCardOperationResult")]
	public class CreditCardOperationResult
	{
		[DataMember(Name = "amount", Order = 1)]
		public string Amount { get; set; }

		[DataMember(Name = "decision", Order = 3)]
		public string Decision { get; set; }

		[DataMember(Name = "paymentGatewayId", Order = 5)]
		public string PaymentGatewayId { get; set; }

        [DataMember(Name = "currencyCode", Order = 7)]
        public string CurrencyCode { get; set; }

        [DataMember(Name = "creditRequest", Order = 9)]
        public CreditRequest CreditRequest { get; set; }

        [DataMember(Name = "merchantId", Order = 11)]
        public string MerchantId { get; set; }
	}
}
