﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudRecovery.Common.Models
{
	public enum RecoveryActionTypes
	{
		SvcCardAction = 1,

		TransactionWithSvcCardsAction = 2,
		
		TransactionWithCreditCard = 3,
	}
}
