﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Starbucks.FraudRecovery.Common.Helpers;
using Starbucks.FraudRecovery.Common.Models;

namespace Starbucks.FraudRecovery.Common.Models
{
	[DataContract(Name ="operation")]
	public class OperationResponse
	{
		public OperationResponse(string name)
		{
			this.Name = name;
			this.Status = OperationStatus.Unknown;
			this.RequestReceivedDateTime = DateTime.UtcNow.ToUtcDateTimeString();
			this.Reason = null;
			this.TraceNumber = Guid.NewGuid().ToString();
		}

		[DataMember(Name = "name", Order = 1)]
		public string Name { get; set; }

		[DataMember(Name = "status", Order = 2)]
		public OperationStatus Status { get; set; }

		[DataMember(Name = "Id", Order = 3)]
		public string Id { get; set; }

		[DataMember(Name = "date", Order = 5)]
		public string RequestReceivedDateTime { get; set; }

		[DataMember(Name = "trackNumber", Order = 6)]
		public string TraceNumber { get; set; }

		[DataMember(Name = "reason", Order = 7)]
		public ReasonInfo Reason { get; set; }
	}

	[DataContract(Name = "operationWithResult")]
	public class OperationResponseWithResult<TResult> : OperationResponse where TResult : new()
	{
		public OperationResponseWithResult() : this(string.Empty)
		{
		}

		public OperationResponseWithResult(string name) : base (name)
		{
			this.Result = default(TResult);
		}

		[DataMember(Name = "result", Order = 30)]
		public TResult Result { get; set; }
	}

	#region Card related action response
	/// <summary>
	/// 
	/// </summary>
	[DataContract(Name = "balance")]
	public class SvcCardGetBalanceResponse : OperationResponseWithResult<SvcCardOperationResult>
	{
		public SvcCardGetBalanceResponse()
			: base("CardGetBalance")
		{
		}
	}

	/// <summary>
	/// 
	/// </summary>
	[DataContract(Name = "revokeAmount")]
	public class SvcCardRevokeAmountResponse : OperationResponseWithResult<SvcCardOperationResult>
	{
		public SvcCardRevokeAmountResponse()
			: base("CardRevokeAmount")
		{
		}
	}

	/// <summary>
	/// 
	/// </summary>
	[DataContract(Name = "loadAmount")]
	public class SvcCardLoadAmountResponse : OperationResponseWithResult<SvcCardOperationResult>
	{
		public SvcCardLoadAmountResponse()
			: base("CardLoadAmount")
		{
		}
	}

	/// <summary>
	/// 
	/// </summary>
	[DataContract(Name = "cashOut")]
	public class SvcCardCashOutResponse : OperationResponseWithResult<SvcCardOperationResult>
	{
		public SvcCardCashOutResponse()
			: base("CardCashOut")
		{
		}
	}


	/// <summary>
	/// 
	/// </summary>
	[DataContract(Name = "svcCardfreeze")]
	public class SvcCardFreezeResponse : OperationResponseWithResult<SvcCardOperationResult>
	{
		public SvcCardFreezeResponse()
			: base("CardFreeze")
		{
		}
	}

	/// <summary>
	/// 
	/// </summary>
	[DataContract(Name = "SvcCardUnfreeze")]
	public class SvcCardUnfreezeResponse : OperationResponseWithResult<SvcCardOperationResult>
	{
		public SvcCardUnfreezeResponse()
			: base("CardUnfreeze")
		{
		}
	}

	#endregion

	[DataContract(Name = "operationWithCollectionResult")]
	public class OperationResponseCollectionResult<TResult> : OperationResponse where TResult : new()
	{
		public OperationResponseCollectionResult(string name)
			: base(name)
		{
			this.Results = new List<TResult>();
		}

		[DataMember(Name = "totalResults", Order = 20)]
		public int TotalOperations { get; set; }

		[DataMember(Name = "failureCount", Order = 21)]
		public int FailureCount { get; set; }

		[DataMember(Name = "results", Order = 30)]
		public List<TResult> Results { get; set; }
	}

	[DataContract(Name = "revoke")]
	public class TransactionRevokeResponse : OperationResponseCollectionResult<SvcCardRevokeAmountResponse>
	{
		public TransactionRevokeResponse()
			: base("TransactionRevoke")
		{
		}		
	}

	[DataContract(Name = "transactionFreeze")]
	public class TransactionFreezeResponse : OperationResponseCollectionResult<SvcCardFreezeResponse>
	{
		public TransactionFreezeResponse()
			: base("TransactionFreeze")
		{
		}
	}

	[DataContract(Name = "transactionUnfreeze")]
	public class TransactionUnfreezeResponse : OperationResponseCollectionResult<SvcCardUnfreezeResponse>
	{
		public TransactionUnfreezeResponse()
			: base("TransactionUnfreeze")
		{
		}
	}

	[DataContract(Name = "refund")]
	public class TransactionRefundResponse : OperationResponseWithResult<CreditCardOperationResult>
	{
		public TransactionRefundResponse()
			: base("TransactionRefund")
		{
		}
	}
}