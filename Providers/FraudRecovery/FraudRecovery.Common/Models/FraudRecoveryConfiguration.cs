﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Starbucks.FraudRecovery.Common;


namespace Starbucks.FraudRecovery.Common.Models
{
	[Serializable]
	[XmlRoot("Market")]
	[KnownType(typeof(MarketCollectionConfiguration))]
	public class MarketConfiguraiton
	{
		[XmlAttribute("name")]
		public string Name { get; set; }

		//[XmlAttribute("merchantId")]
		//public string MerchantId { get; set; }
	}

	[Serializable]
	[XmlRoot("Markets")]
	public class MarketCollectionConfiguration : MarketConfiguraiton
	{
		public MarketCollectionConfiguration()
		{
			this.Markets = new MarketConfiguraiton[0];
		}

		[XmlArray("Markets")]
		[XmlArrayItem("Market")]
		public MarketConfiguraiton[] Markets { get; set; }
	}

	[Serializable]
	[XmlRoot("ConnectionString")]
	public class ConnectionString
	{
		[XmlAttribute("name")]
		public string Name { get; set; }

		[XmlAttribute("connectionStringName")]
		public string ConnectionStringName { get; set; }
	}


	[Serializable]
	[XmlRoot("ConnectionString")]
	public class AuditorSprocConfiguration
	{
		[XmlAttribute("type")]
		public RecoveryActionTypes ActionType { get; set; }

		[XmlAttribute("insert")]
		public string InsertCommand { get; set; }

		[XmlAttribute("update")]
		public string UpdateCommand { get; set; }
	}

	[Serializable]
	[XmlRoot("Auditing")]
	public class AuditorConfiguration
	{
		public AuditorConfiguration()
		{
			this.Sprocs = new AuditorSprocConfiguration[0];
		}

		public AuditorSprocConfiguration this[RecoveryActionTypes actionType]
		{
			get
			{
				AuditorSprocConfiguration result = null;
				if (this.Sprocs != null)
				{
					var item = from c in this.Sprocs
					           where c.ActionType == actionType
					           select c;
					result = item.FirstOrDefault();
				}

				return result;
			}
		}

		[XmlAttribute("connectionStringName")]
		public string DatabaseConnectionStringName { get; set; }

		[XmlArray("DbSprocConfiguration")]
		[XmlArrayItem("SprocInfo")]
		public AuditorSprocConfiguration[] Sprocs { get; set; }
	}


	[Serializable]
	[XmlRoot("Configuration")]
	public class FraudRecoveryConfiguration : IFraudRecoveyConfiguration
	{
		public FraudRecoveryConfiguration()
		{
			this.MarketCollection = new MarketCollectionConfiguration();
			this.ConnectionStrings = new ConnectionString[0];
			this.MarketPrefix = "GS";
			this.IsPinless = false;
			this.UnfreezeRedeemAmount = 0m;
		}

		[XmlAttribute("marketPrefix")]
		public string MarketPrefix { get; set; }

		[XmlAttribute("pinless")]
		public bool IsPinless { get; set; }

		[XmlAttribute("unfreezeRedeem")]
		public decimal UnfreezeRedeemAmount { get; set; }

		[XmlElement("MarketConfiguration")]
		public MarketCollectionConfiguration MarketCollection { get; set; }

		[XmlElement("AuditingConfiguration")]
		public AuditorConfiguration AuditingConfig { get; set; }

		[XmlArray ("Connections")]
		[XmlArrayItem("Connection")]
		public ConnectionString[] ConnectionStrings { get; set; }


		MarketConfiguraiton IFraudRecoveyConfiguration.GetMaretConfig(string market)
		{
			if (this.MarketCollection == null || this.MarketCollection.Markets == null)
			{
				return null;
			}

			if (string.IsNullOrWhiteSpace(market))
			{
				return this.MarketCollection;
			}

			var config = (from c in this.MarketCollection.Markets
			              where c.Name.Equals(market, StringComparison.OrdinalIgnoreCase)
			              select c).FirstOrDefault();
			config = config ?? this.MarketCollection;

			return config;
		}

	}
}
