﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common.Models;

namespace Starbucks.FraudRecovery.Common
{
	public interface IFraudRecoveryAuditor<TResult> where TResult : OperationResponse
	{
		Dictionary<string, string> AdditionalUpdateParameters { get; }

        void Create(TResult result, string ParentTrackNumber);

        void Update(TResult result, string ParentTrackNumber, decimal? amount = null, string currency = null);
	}
}
