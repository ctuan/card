﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudRecovery.Common
{
	public interface IRepository<TResult>
	{
		IRepository<TResult> Parent { get; }
		IDictionary<string, IRepository<TResult>> ChildRepositories { get; }
		IList<TResult> Entries { get; }

		void AddEntry(TResult t);
		void Cleanup();
	}
}
