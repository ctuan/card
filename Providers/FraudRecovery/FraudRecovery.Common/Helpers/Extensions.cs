﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common.Models;

namespace Starbucks.FraudRecovery.Common.Helpers
{
	public static class Extensions
	{
		public static string ToUtcDateTimeString(this DateTime dt)
		{
			return dt.ToString("u");
		}

		public static string ToUtcDateTimeString(this string dt)
		{
			DateTime d;
			if (DateTime.TryParse(dt, out d))
			{
				return d.ToUtcDateTimeString();
			}
			return string.Empty;
		}

		public static string GetErrorCodeFromResource(this ReasonTypes errType)
		{
			return GetResourceString(string.Format("{0}ReasonCode", errType.ToString()));
		}

		public static string GetErrorMessageFromResource(this ReasonTypes errType)
		{
			return GetResourceString(string.Format("{0}ReasonMessage", errType.ToString()));
		}

		public static string GetResourceString(this string key)
		{
			try
			{
				return Resources.ReasonResources.ResourceManager.GetString(key);
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.ToString());
			}
			return string.Empty;
		}

		public static int ToInt(this string intValue, int defaultValue = -1)
		{
			if (string.IsNullOrWhiteSpace(intValue))
			{
				return defaultValue;
			}

			int result = defaultValue;
			if (!int.TryParse(intValue, out result))
			{
				return defaultValue;
			}

			return result;
		}

        public static TResponse UpdateErrorResult<TResponse>(this TResponse result, ReasonTypes errType, string nativeCode, string nativeMessage)
            where TResponse : OperationResponse
        {
            if (result != null)
            {
                result.Status = OperationStatus.Failed;
                result.Reason = new ReasonInfo();
                result.Reason.Code = errType.GetErrorCodeFromResource();
                result.Reason.Message = nativeMessage;
                result.Reason.NativeReasonCode = nativeCode;
            }
            return result;
        }

		public static TResponse UpdateErrorResult<TResponse>(this TResponse result, ReasonTypes errType, string nativeCode = "")
			where TResponse : OperationResponse
		{
			if (result != null)
			{
				result.Status = OperationStatus.Failed;
				result.Reason = new ReasonInfo(errType, nativeCode);
			}
			return result;
		}
	}
}
