﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common.Logging;


namespace Starbucks.FraudRecovery.Common
{
	public static class InputValidatinExtensions
	{
		public static void EnsureObjectNotNull(this object input, string message)
		{
			if (input == null)
			{
				throw new ArgumentNullException(message);
			}
		}

        public static void EnsureObjectNotNull(this object input, string title, ILoggingUtility logger, string message = null)
        {
            if (input == null)
            {
                logger.Log(LogType.Error, title, message);
                throw new ArgumentNullException(title);
            }
        }

        public static T EnsureObjectIsType<T>(this object input, string message) 
			where T: class
		{
			T inst = input as T;
			EnsureObjectNotNull(inst, message);
			return inst;
		}

		public static void EnsureStringNotNullOrEmpty(this string input, string message)
		{
			if (string.IsNullOrEmpty(input))
			{
				throw new ArgumentException(message);
			}
		}
	}
}
