﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common.Models;

namespace Starbucks.FraudRecovery.Common
{
	public interface IFraudRecoveyConfiguration
	{
		string MarketPrefix { get; }

		bool IsPinless { get; }

		decimal UnfreezeRedeemAmount { get; }

		MarketConfiguraiton GetMaretConfig(string market);

		AuditorConfiguration AuditingConfig { get; }

		ConnectionString[] ConnectionStrings { get; }
	}
}
