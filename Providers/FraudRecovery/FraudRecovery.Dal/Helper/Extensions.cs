﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.FraudRecovery.Dal.Common.Models;
using Starbucks.Platform.Security;

namespace Starbucks.FraudRecovery.Dal.Helper
{
	public static class Extensions
	{
		public static string ToDecryptCardNumber(this string number)
		{
			return string.IsNullOrWhiteSpace(number)
				       ? string.Empty
				       : Encryption.DecryptCardNumber(number);
		}

		public static string ToDecryptPin(this string pin)
		{
			return string.IsNullOrWhiteSpace(pin)
					   ? string.Empty
					   : Encryption.DecryptPin(pin);
		}

		public static int ToDecryptCardId(this string cardId)
		{
			return string.IsNullOrWhiteSpace(cardId)
					   ? -1
					   : Encryption.DecryptCardId(cardId);
		}


	}
}
