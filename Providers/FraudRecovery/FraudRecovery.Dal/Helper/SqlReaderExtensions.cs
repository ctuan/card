﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Dal.Common.Models;

namespace Starbucks.FraudRecovery.Dal.Helper
{
	public static class SqlReaderExtensions
	{
		public static OrderItem ToOrderItem(this IDataReader reader, ILoggingUtility logger)
		{
			var item = new OrderItem()
			{
				Name = GetStringValue(reader, "name", logger),
				Quantity = GetIntValue(reader, "quantity", logger),
				UnitPrice = GetDecimalValue(reader, "cy_unit_price", logger),
				Total = GetDecimalValue(reader, "cy_lineitem_total", logger),
				SvcTransactionId = GetStringValue(reader, "svc_trans_id", logger),
				Catalog = GetStringValue(reader, "product_catalog", logger),
				Sku = GetStringValue(reader, "sku", logger),
                ProductType = GetIntValue(reader, "ProductTypeId", logger),
                SavedCcExpiration = GetStringValue(reader, "saved_cc_expiration", logger)
			};

			return item;
		}

		public static void UpdateOrder(this IDataReader reader, OrderInfo order, ILoggingUtility logger)
		{
			if (order == null)
			{
				return;
			}

			order.OrderId = GetStringValue(reader, "order_id", logger);
			order.OrderType = GetStringValue(reader, "order_type", logger);

			order.UserId = GetStringValue(reader, "user_id", logger);
			order.UserEmail = GetStringValue(reader, "user_email_address", logger);

            order.PaymentMethodId = GetStringValue(reader, "PaymentMethodId", logger);
            order.SavedCcExpiration = GetStringValue(reader, "saved_cc_expiration", logger);
			order.BillingRequestId = GetStringValue(reader, "cc_bill_request_id", logger);
			order.AuthRequestId = GetStringValue(reader, "cc_auth_request_id", logger);
			order.Currency = GetStringValue(reader, "billing_currency", logger);
            
            if (order.TotalCost == null)
                order.TotalCost = GetDecimalValue(reader, "cy_lineitem_total", logger);
            else
                order.TotalCost += GetDecimalValue(reader, "cy_lineitem_total", logger);

   	        order.TotalCharged = GetDecimalValue(reader, "cc_total", logger);
            order.FirstName = GetStringValue(reader, "user_first_name", logger);
            order.LastName = GetStringValue(reader, "user_last_name", logger);
            order.SurrogateAccountNumber = GetStringValue(reader, "SurrogateAccountNumber", logger);
            order.CcType = GetStringValue(reader, "cc_type", logger);
		}

        public static CyberSourceMerchantInfo ToCyberSourceMerchantInfo(this IDataReader reader, ILoggingUtility logger)
        {
            var result = new CyberSourceMerchantInfo()
            {
                Currency = GetStringValue(reader, "CurrencyCode", logger),
                MarketCode = GetStringValue(reader, "MarketCode", logger),
                TransactionType = GetStringValue(reader, "TransactionType", logger),
                MerchantKey = GetStringValue(reader, "MerchantKey", logger)
            };
            return result;
        }

		public static SvcTransactionInfo ToSvcTransactionInfo(this IDataReader reader, ILoggingUtility logger)
		{
			var result = new SvcTransactionInfo()
				{
					SvcTransactionId = GetStringValue(reader, "TransactionID", logger),
					SvcCard = new SvcCardInfo()
						{
							Number = GetStringValue(reader, "CardNo", logger).ToDecryptCardNumber(),
							Pin = GetStringValue(reader, "Pin", logger).ToDecryptPin(),
							Market = "",
						}
				};
			return result;
		}

		#region helpers

		public static TOutput GetValue<TOutput>(this IDataReader reader, string key, Func<string, TOutput> converter, ILoggingUtility logging)
			where TOutput : class
		{
			TOutput result = default(TOutput);
			try
			{
				var raw = reader[key];
				if (raw != null)
				{
					result = converter(raw.ToString());
				}
			}
			catch (Exception ex)
			{
				logging.Log(LogType.Error, "DB missing data", "Field :" + key, "DB exception", new Dictionary<string, object>()
					{
						{"DB exception", ex}
					}
					);
			}

			if (result == null)
			{
				logging.Log(LogType.Warning, "DB missing data", "Field :" + key);
			}

			return result;
		}


		public static TOutput? GetNullableValue<TOutput>(this IDataReader reader, string key, Func<string, TOutput?> converter, ILoggingUtility logging)
			where TOutput : struct
		{
			TOutput? result = null;
			try
			{
				var raw = reader[key];
				if (raw != null)
				{
					result = converter(raw.ToString());
				}
			}
			catch (Exception ex)
			{
				logging.Log(LogType.Error, "DB missing data", "Field :" + key, "DB exception", new Dictionary<string, object>()
					{
						{"DB exception", ex}
					}
					);
			}

			if (result == null)
			{
				logging.Log(LogType.Warning, "DB missing data", "Field :" + key);
			}

			return result;
		}

		public static string GetStringValue(this IDataReader reader, string key, ILoggingUtility logging)
		{
			return GetValue<string>(reader, key, (x) => x, logging);
		}

		public static int? GetIntValue(this IDataReader reader, string key, ILoggingUtility logging)
		{
			return GetNullableValue<int>(reader, key, (x) =>
				{
					int result = -1;
					if (int.TryParse(x, out result))
					{
						return result;
					}

					return null;
				},
				logging
				);
		}

		public static decimal? GetDecimalValue(this IDataReader reader, string key, ILoggingUtility logging)
		{
			return GetNullableValue<decimal>(reader, key, (x) =>
				{
					decimal result = 0m;
					if (decimal.TryParse(x, out result))
					{
						return result;
					}
					return null;
				}, 
				logging );
		}
		#endregion
	}
}

