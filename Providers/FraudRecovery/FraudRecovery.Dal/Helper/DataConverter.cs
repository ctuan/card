﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.Address.Dal.Common.Models;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.FraudRecovery.Dal.Common.Models;
using Starbucks.PaymentMethod.Provider.Common.Models;

namespace Starbucks.FraudRecovery.Dal.Helper
{
	public static class DataConverter
	{
		public static SvcCardInfo ToCardInfo(this IAssociatedCard native)
		{
			if (native == null)
			{
				return null;
			}

			var result = new SvcCardInfo()
			{
				Id = native.CardId.ToDecryptCardId(),
				Number = native.Number.ToDecryptCardNumber(),
				Pin = native.Pin.ToDecryptPin(),
				Market = native.SubMarketCode,
			};

			return result;
		}

		public static AddressInfo ToAddressInfo(this IUserAddress native)
		{
			if (native == null)
			{
				return null;
			}

			var result = new AddressInfo()
				{
					AddressId = native.AddressId,
					AddressDetails = new Common.Models.Address()
						{
						AddressLine1	= native.AddressLine1,
						AddressLine2 =  native.AddressLine2,
						City = native.City,
						CountrySubdivision = native.CountrySubdivision,
						Country = native.Country,
						PostalCode = native.PostalCode,
						FirstName = native.FirstName,
						LastName = native.LastName,
						PhoneNumber = native.PhoneNumber,
						IsTemporary = native.IsTemporary,
						AddressTypeId = (int) native.AddressType,
						EmailAddress = native.EmailAddress,
						}
				};
			return result;
		}

		public static BillingInfo BillingInfo(this IPaymentMethod native)
		{
			if (native == null)
			{
				return null;
			}

			CreditCard credit =
				(native.PaymentTypeId <= (int) CreditCardTypes.None || native.PaymentTypeId > (int) CreditCardTypes.Discover)
					? null
					: new CreditCard()
						{
							Type = (CreditCardTypes) native.PaymentTypeId,
							FullName = native.FullName,
							Cvn = native.Cvn,
							ExpireYear = native.ExpirationYear,
							ExpireMonth = native.ExpirationMonth,
							SurrogateNumber = native.SurrogateAccountNumber,
							Currency = native.Currency,
							IsTemporary = native.IsTemporary,
							LastFourNumber = native.AccountNumberLastFour
						};

			var payment = new PaymentMethodInfo()
				{
					PaymentId = native.PaymentMethodId,
					CreditCard = credit,
				};

			AddressInfo address = string.IsNullOrWhiteSpace(native.BillingAddressId)
				                      ? null
				                      : new AddressInfo()
					                      {
											  AddressId = native.BillingAddressId,
											  AddressDetails = null,
					                      };

			var result = new BillingInfo()
				{
					PaymentMethodInfo =  payment,
					AddressInfo = address,
				};
			return result;
		}
	}
}
