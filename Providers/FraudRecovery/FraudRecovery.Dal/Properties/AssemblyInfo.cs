﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Starbucks.FraudRecovery.Dal")]
[assembly: AssemblyDescription("Provides implementation for Accertify Fraud Recovery Tool Data Access Layer")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Starbucks Coffee Company")]
[assembly: AssemblyProduct("Starbucks.FraudRecovery.Dal")]
[assembly: AssemblyCopyright("Starbucks Coffee Company ©  2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("132cdf6e-f1dd-48de-bb1c-8a3775e4a76f")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]
