﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.Address.Dal.Common;
using Starbucks.Address.Dal.Sql;
using Starbucks.Address.Dal.Sql.Configuration;
using Starbucks.BadWords.Provider.Common;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Dal.Commands;
using Starbucks.FraudRecovery.Dal.Common;
using Starbucks.FraudRecovery.Dal.Common.Models;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.PaymentMethod.Provider;
using Starbucks.PaymentMethod.Provider.SqlHybrid;
using Starbucks.PaymentMethod.Provider.SqlHybrid.Configuration;
using Starbucks.Platform.Security;

namespace Starbucks.FraudRecovery.Dal
{
	public class FraudRecoveryDal : IFraudRecoveryDal
	{
		public FraudRecoveryDal(IFraudRecoveyConfiguration config, ICardDal cardDal, IAddressDataProvider addressProvider, IPaymentMethodDataProvider paymentMethodProvider, ILoggingUtility logger)
		{
			config.EnsureObjectNotNull("config is required");
			logger.EnsureObjectNotNull("logging is required");
			cardDal.EnsureObjectNotNull("cardDal is required");
			addressProvider.EnsureObjectNotNull("addressProvider is required");
			paymentMethodProvider.EnsureObjectNotNull("paymentMethodProvider is required");

			this.Configuration = config;
			this.Logger = logger;
			this.SvcCardDal = cardDal;
			this.AddressProvider = addressProvider;
			this.PaymentMethodDataProvider = paymentMethodProvider;
		}

		protected IFraudRecoveyConfiguration Configuration { get; private set; }
		protected ILoggingUtility Logger { get; private set; }

		protected ICardDal SvcCardDal { get; private set; }
		protected IAddressDataProvider AddressProvider { get; private set; }
		protected IPaymentMethodDataProvider PaymentMethodDataProvider { get; private set; }

		public SvcCardInfo GetSvcCard(string cardNumber)
		{
			var command = new SvcCardQueryByNumberCommand(cardNumber, this.SvcCardDal, this.Logger);
			return command.Execute();
		}

		public SvcCardInfo GetSvcCard(int decryptCardId)
		{
			var command = new SvcCardQueryByIdCommand(decryptCardId, this.SvcCardDal, this.Logger);
			return command.Execute();
		}

		public OrderInfo GetOrder(string orderId)
		{
			var command = new QueryOrderCommand(this.Configuration, this.Logger, orderId);
			return command.Execute();
		}

		public SvcTransactionInfo GetTransaction(long transactionId)
		{
			var command = new QueryTransactionCommand(this.Configuration, this.Logger, transactionId);
			return command.Execute();
		}

        public CyberSourceMerchantInfo GetCyberSourceMerchantInfo(string submarketCode, 
                                                                    string currency, 
                                                                    string transactionType)
        {
            var command = new QueryCyberSourceMerchantInfoCommand(this.Configuration, this.Logger, submarketCode, currency, transactionType);
            return command.Execute();
        }

		public AddressInfo GetAddress(string userId, string addressId)
		{
			var command = new BillingAddressQueryCommand(userId, addressId, this.AddressProvider, this.Logger);
			return command.Execute();
		}

		public BillingInfo GetBilling(string userId, string paymentMethodId)
		{
			var command = new PaymentMethodQueryCommand(paymentMethodId, this.PaymentMethodDataProvider, this.Logger);
			var result = command.Execute();

			if (result != null && result.AddressInfo != null && result.AddressInfo.AddressDetails == null)
			{
				result.AddressInfo = GetAddress(userId, result.AddressInfo.AddressId);
			}

			return result;
		}

	}
}


