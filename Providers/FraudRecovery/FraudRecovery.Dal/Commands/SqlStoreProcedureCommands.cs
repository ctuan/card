﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Dal.Common.Models;
using Starbucks.FraudRecovery.Dal.Helper;

namespace Starbucks.FraudRecovery.Dal.Commands
{

	/// <summary>
	/// /
	/// </summary>
	/// <typeparam name="TResult"></typeparam>
	public abstract class SqlStoreProcedureCommands<TResult> : ICommand<TResult>
	{
		protected abstract void UpdateQueryParameters(SqlParameterCollection parameters);
		protected abstract bool ReadQueryResult(IDataReader reader, ref TResult result);

		protected SqlStoreProcedureCommands(IFraudRecoveyConfiguration config, string connectStringName, string spName,
		                                    bool isSingleResult, ILoggingUtility logger)
		{
			config.EnsureObjectNotNull("config is required");
			logger.EnsureObjectNotNull("logging is required");
			connectStringName.EnsureStringNotNullOrEmpty("connectStringName is required");
			spName.EnsureStringNotNullOrEmpty("store procedure name is required");

			this.Configuration = config;
			this.Logger = logger;

			this.ConnectionStringName = connectStringName;
			this.StoreProcedureName = spName;
			this.IsSingleResult = isSingleResult;
		}

		protected IFraudRecoveyConfiguration Configuration { get; private set; }
		protected ILoggingUtility Logger { get; private set; }
		protected string ConnectionStringName { get; private set; }
		protected string StoreProcedureName { get; private set; }
		protected bool IsSingleResult { get; private set; }


		public TResult Execute()
		{
			TResult result = default(TResult);
			try
			{
				string connectonString = GetConnectionString(this.ConnectionStringName);
				using (var connection = new SqlConnection(connectonString))
				{
					using (var command = GetCommand(this.StoreProcedureName, connection))
					{
						connection.Open();
						UpdateQueryParameters(command.Parameters);
						using (IDataReader reader = command.ExecuteReader())
						{
							while (reader.Read())
							{
								ReadQueryResult(reader, ref result);
								if (IsSingleResult)
								{
									break;
								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				this.Logger.Log(LogType.Error, "SqlStoreProcedureCommands::Execute", "Execution of SQL store procedure is failed",
				                "DB or configuration issue",
				                new Dictionary<string, object>()
					                {
						                {"connectionStringName", this.ConnectionStringName},
						                {"store procedure name", this.StoreProcedureName},
						                {"exception", ex}
					                }
					);
			}


			return result;
		}

		#region protected functions and helpers
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sproc"></param>
		/// <param name="connection"></param>
		/// <returns></returns>
		protected SqlCommand GetCommand(string sproc, SqlConnection connection)
		{
			return new SqlCommand(sproc, connection) {CommandType = CommandType.StoredProcedure};
		}


		protected string GetConnectionString(string connectionStringName)
		{
			if (this.Configuration.ConnectionStrings != null)
			{
				var item = (from c in this.Configuration.ConnectionStrings
				            where c.Name.Equals(connectionStringName, StringComparison.OrdinalIgnoreCase)
				            select c).FirstOrDefault();
				if (item != null && !string.IsNullOrWhiteSpace(item.ConnectionStringName))
				{
					var connectionStringObj = ConfigurationManager.ConnectionStrings[item.ConnectionStringName];
					if (connectionStringObj != null)
					{
						return connectionStringObj.ConnectionString;
					}
				}
			}
			throw new ConfigurationErrorsException("Unable to find configuration for connection string Name: " +
			                                       connectionStringName ?? "<NULL>");
		}


		#endregion
	}

	/// <summary>
	/// 
	/// </summary>
	public class QueryOrderCommand : SqlStoreProcedureCommands<OrderInfo>
	{
		public QueryOrderCommand(IFraudRecoveyConfiguration config, ILoggingUtility logger, string orderId)
			: base(config, "Commerce", "GetOrderLineItemById", false, logger)
		{
			this.OrderId = orderId;
		}

		protected string OrderId { get; private set; }

		protected override void UpdateQueryParameters(SqlParameterCollection parameters)
		{
			this.OrderId.EnsureStringNotNullOrEmpty("order id is required");
			parameters.AddWithValue("@orderId", this.OrderId);
		}

		protected override bool ReadQueryResult(IDataReader reader, ref OrderInfo result)
		{
			if (result == null)
			{
				result = new OrderInfo();
			}

			try
			{
				reader.UpdateOrder(result, this.Logger);
				var item = reader.ToOrderItem(this.Logger);
				result.Items.Add(item);
				return true;
			}
			catch (Exception ex)
			{
				this.Logger.Log(LogType.Error, "QueryOrderCommand::Execute", "Reader result from query",
								"DB or configuration or Data issue",
								new Dictionary<string, object>()
					                {
						                {"orderId", this.OrderId},
						                {"exception", ex}
					                }
					);
				return false;
			}
		}
	}

	/// <summary>
	/// 
	/// </summary>
	public class QueryTransactionCommand : SqlStoreProcedureCommands<SvcTransactionInfo>
	{
		public QueryTransactionCommand(IFraudRecoveyConfiguration config, ILoggingUtility logger, long transactionId)
			: base(config, "CardTransactions", "GetTransaction", true, logger)
		{
			this.SvcTransactionId = transactionId;
		}

		protected long SvcTransactionId { get; private set; }

		protected override void UpdateQueryParameters(SqlParameterCollection parameters)
		{
			parameters.AddWithValue("@TransactionId", this.SvcTransactionId);
		}

		protected override bool ReadQueryResult(IDataReader reader, ref SvcTransactionInfo result)
		{
			try
			{
				result = reader.ToSvcTransactionInfo(logger: Logger);
				return true;
			}
			catch (Exception ex)
			{
				this.Logger.Log(LogType.Error, "QueryTransactionCommand::Execute", "Reader result from query",
								"DB or configuration or Data issue",
								new Dictionary<string, object>()
					                {
						                {"transactionId", this.SvcTransactionId},
						                {"exception", ex}
					                }
					);
				return false;
			}
		}
	}

    public class QueryCyberSourceMerchantInfoCommand : SqlStoreProcedureCommands<CyberSourceMerchantInfo>
    {
        public QueryCyberSourceMerchantInfoCommand(IFraudRecoveyConfiguration config, 
                                                    ILoggingUtility logger, 
                                                    string submarketCode, 
                                                    string currency, 
                                                    string transactionType) : base(config, "CardTransactions", "GetCyberSourceKeys", true, logger)
        {
            this.MarketCode = submarketCode;
            this.Currency = currency;
            this.TransactionType = transactionType;
        }

        protected string MarketCode { get; private set; }
        protected string Currency { get; private set; }
        protected string TransactionType { get; private set; }

        protected override void UpdateQueryParameters(SqlParameterCollection parameters)
        {
            parameters.AddWithValue("@CurrencyCode", this.Currency);
            parameters.AddWithValue("@MarketCode", this.MarketCode);
            parameters.AddWithValue("@TransactionType", this.TransactionType);
        }

        protected override bool ReadQueryResult(IDataReader reader, ref CyberSourceMerchantInfo result)
        {
            try
            {
                result = reader.ToCyberSourceMerchantInfo(logger: Logger);
                return true;
            }
            catch (Exception ex)
            {
                this.Logger.Log(LogType.Error, "QueryTransactionCommand::Execute", "Reader result from query",
                                "DB or configuration or Data issue",
                                new Dictionary<string, object>()
					                {
						                {"currencyCode", this.Currency},
						                {"marketCode", this.MarketCode},
						                {"transactionType", this.TransactionType},
						                {"exception", ex}
					                }
                    );
                return false;
            }
        }
    }

}
