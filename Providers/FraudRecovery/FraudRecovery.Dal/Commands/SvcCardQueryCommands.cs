﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Dal.Common.Models;
using Starbucks.FraudRecovery.Dal.Helper;
using Starbucks.Platform.Security;

namespace Starbucks.FraudRecovery.Dal.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public class SvcCardQueryByIdCommand : QueryCommandBase <SvcCardInfo, IAssociatedCard, ICardDal>
	{
		public SvcCardQueryByIdCommand(int cardId, ICardDal cardDal, ILoggingUtility logger)
			: base(Encryption.EncryptCardId(cardId), cardDal, logger)
		{
		}

		protected override IAssociatedCard Query()
		{
			return this.Provider.GetCard(this.QueryKey);
		}

		protected override SvcCardInfo Convert(IAssociatedCard native)
		{
			return native == null ? null : native.ToCardInfo();
		}

		protected override void HandleException(Exception ex)
		{
			string message = string.Format("unable to get card info from card-Id: {0}", this.QueryKey);
			LogException(ex, message, hint: "input cardId should be decrypted CardId");
		}
	}

	/// <summary>
	/// 
	/// </summary>
	public class SvcCardQueryByNumberCommand : QueryCommandBase<SvcCardInfo, IAssociatedCard, ICardDal>
	{
		public SvcCardQueryByNumberCommand(string cardNumber, ICardDal cardDal, ILoggingUtility logger)
			: base(cardNumber, cardDal, logger)
		{
		}

		protected override IAssociatedCard Query()
		{
			return this.Provider.GetCardByNumber(this.QueryKey);
		}

		protected override SvcCardInfo Convert(IAssociatedCard native)
		{
			return native == null ? null : native.ToCardInfo();
		}

		protected override void HandleException(Exception ex)
		{
			string message = string.Format("unable to get card info from card-number: {0}", this.QueryKey ?? "<NULL>");
			LogException(ex, message, hint: "");
		}

	}
}
