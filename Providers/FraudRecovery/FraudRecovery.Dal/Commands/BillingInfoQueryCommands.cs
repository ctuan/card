﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.Address.Dal.Common;
using Starbucks.Address.Dal.Common.Models;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;
using Starbucks.FraudRecovery.Dal.Common.Models;
using Starbucks.FraudRecovery.Dal.Helper;
using Starbucks.PaymentMethod.Provider;
using Starbucks.PaymentMethod.Provider.Common.Models;

namespace Starbucks.FraudRecovery.Dal.Commands
{
	public class BillingAddressQueryCommand :
		QueryCommandBase<Common.Models.AddressInfo, IUserAddress, IAddressDataProvider>
	{
		public BillingAddressQueryCommand(string userId, string addressId, IAddressDataProvider provider,
		                                  ILoggingUtility logging)
			: base(addressId, provider, logging)
		{
			this.UserId = userId;
		}

		protected string UserId { get; private set; }


		protected override IUserAddress Query()
		{
			return Provider.GetUserAddress(UserId, this.QueryKey);
		}

		protected override Common.Models.AddressInfo Convert(IUserAddress native)
		{
			return native.ToAddressInfo();
		}

		protected override void HandleException(Exception ex)
		{
			string message = string.Format("Unable to get address for UserId({0}),  AddressId({1})", this.UserId ?? "<NULL>",
			                               this.QueryKey ?? "NULL");
			LogException(ex, message, hint: "Is it a Temp Address?");
		}
	}

	public class PaymentMethodQueryCommand : QueryCommandBase<Common.Models.BillingInfo, IPaymentMethod, IPaymentMethodDataProvider>
	{
		public PaymentMethodQueryCommand(string paymentMethodId, IPaymentMethodDataProvider provider, ILoggingUtility logging)
			: base(paymentMethodId, provider, logging)
		{
		}

		protected override IPaymentMethod Query()
		{
			return this.Provider.GetPaymentMethod(this.QueryKey);
		}

		protected override Common.Models.BillingInfo Convert(IPaymentMethod native)
		{
			return native.BillingInfo();
		}

		protected override void HandleException(Exception ex)
		{
			string message = string.Format("Unable to get paymentMethod for Id: ({0})", this.QueryKey ?? "<NULL>",
										   this.QueryKey ?? "NULL");
			LogException(ex, message, hint: "Is it a Temp Payment?");
		}
	}

}
