﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Logging;

namespace Starbucks.FraudRecovery.Dal.Commands
{
	public abstract class QueryCommandBase<TResult, TNative, TProvider> : ICommand<TResult>
		where TResult: class, new()
		where TNative: class
	{
		protected abstract TNative Query();
		protected abstract TResult Convert(TNative native);
		protected abstract void HandleException(Exception ex);

		protected QueryCommandBase(string key, TProvider provider, ILoggingUtility logger)
		{
			logger.EnsureObjectNotNull("logging is required");
			provider.EnsureObjectNotNull("query is required"); 
			
			this.Logger = logger;
			this.Provider = provider;
			this.QueryKey = key;
		}

		protected ILoggingUtility Logger { get; private set; }
		protected TProvider Provider { get; private set; }
		protected string QueryKey { get; private set; }

		public virtual TResult Execute()
		{
			try
			{
				if (! CanQuery())
				{
					return null;
				}

				TNative native = Query();

				return native == null ? null : Convert(native);
			}
			catch (Exception ex)
			{
				HandleException(ex);
			}
			return null;
		}

		protected virtual bool CanQuery()
		{
			return !string.IsNullOrWhiteSpace(this.QueryKey);
		}

		protected virtual void LogException(Exception ex, string message, string hint = "")
		{
			try
			{
				string title = string.Format("{0}::Execute", this.GetType().Name);
				this.Logger.Log(LogType.Error, title, message, hint,
				                new Dictionary<string, object>()
					                {
						                {"exception", ex}
					                }
					);
			}
			catch (Exception err)
			{
				// logging exception, have to eat it
				System.Diagnostics.Debug.WriteLine(err.ToString());
			}
		}
	}
}
