﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.Address.Dal.Common;
using Starbucks.Address.Dal.Sql;
using Starbucks.Address.Dal.Sql.Configuration;
using Starbucks.BadWords.Provider.Common;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.Card.Dal.Sql;
using Starbucks.Card.Dal.Sql.Configuration;
using Starbucks.PaymentMethod.Provider;
using Starbucks.PaymentMethod.Provider.SqlHybrid;
using Starbucks.PaymentMethod.Provider.SqlHybrid.Configuration;

namespace Starbucks.FraudRecovery.Dal
{
	public static class DalObjectFactory
	{
		public static IAddressDataProvider GetAddressDataProvider(
			string addressProviderConfigSectionName = "addressProviderSettings")
		{
			AddressDataProviderSection addressProviderSettings =
				ConfigurationManager.GetSection(addressProviderConfigSectionName) as AddressDataProviderSection;
			IAddressDataProvider addressData = new AddressDataProvider(addressProviderSettings);

			return addressData;
		}

		public static IPaymentMethodDataProvider GetPaymentMethodDataProvider
			(
			string addressProviderConfigSectionName = "addressProviderSettings",
			string paymentMethodConfigSectionName = "paymentMethodProviderSettings"
			)
		{
			try
			{
				var section = ConfigurationManager.GetSection(paymentMethodConfigSectionName);
				PaymentMethodProviderSection paymentSection = section as PaymentMethodProviderSection;
				
				var addressData = GetAddressDataProvider(addressProviderConfigSectionName);
				
				IPaymentMethodDataProvider provider = new PaymentMethodDataProvider(paymentSection, addressData, null);
				return provider;
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.ToString());
				throw;
			}
		}

		public static ICardDal GetCardDal(string sectionName = "cardDalSettings")
		{
			var configSection = ConfigurationManager.GetSection(sectionName) as CardDalSqlSettingsSection;

			ICardDal cardDal = new CardDal(configSection, new Starbucks.Settings.Provider.SettingsProvider());
			return cardDal;
		}
	}
}
