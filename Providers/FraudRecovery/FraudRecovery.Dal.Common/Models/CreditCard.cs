﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudRecovery.Dal.Common.Models
{
	/// <summary>
	/// Note:  enum integer value need to be sync up with DB at [Starbucks_Profiles].[dbo].[PaymentType]
	/// </summary>
	[DataContract(Name = "creditCardTypes")]
	public enum CreditCardTypes
    {
        None = 0,
        Visa = 1,
        MasterCard = 2,
        Amex = 3,
        Discover = 4,
    }


	[DataContract(Name = "creditCardDetails")]
    public class CreditCard
    {
        [DataMember(Name = "creditCardFullName", IsRequired = false)]
        public string FullName { get; set; }

        [DataMember(Name = "creditCardType", IsRequired = false)]
		public CreditCardTypes Type { get; set; }
        
		[DataMember(Name = "creditCardSurrogateNumber", IsRequired = false)]
        public string SurrogateNumber { get; set; }
        
		[DataMember(Name = "CVN", IsRequired = false)]
        public string Cvn { get; set; }
        
		[DataMember(Name = "creditCardExpireMonth", IsRequired = false)]
        public int? ExpireMonth { get; set; }
        
		[DataMember(Name = "creditCardExpireYear", IsRequired = false)]
        public int? ExpireYear { get; set; }

		[DataMember(Name = "creditCardCurrency", IsRequired = false)]
		public string Currency { get; set; }

		[DataMember(Name = "isTemporary", IsRequired = false)]
		public bool IsTemporary { get; set; }

		[DataMember(Name = "lastFourNumber", IsRequired = false)]
		public string LastFourNumber { get; set; }
    }
}
