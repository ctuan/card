﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudRecovery.Dal.Common.Models
{

	public class OrderItem
	{
		public string Name { get; set; }
		public string Sku { get; set; }
		public string Catalog { get; set; }
        public string SavedCcExpiration { get; set; }

        public int? ProductType { get; set; }

		public int?  Quantity { get; set; }
		public decimal? UnitPrice { get; set; }
		public decimal? Total { get; set; }
		public string SvcTransactionId { get; set; }
	}

	public class OrderInfo
	{
		public OrderInfo()
		{
			this.Items = new List<OrderItem>();
		}

		public string OrderId { get; set; }
		public string OrderType { get; set; }

		public decimal? TotalCost { get; set; }
		public decimal? TotalCharged { get; set; }

		public string UserId { get; set; }
		public string UserEmail { get; set; }

		public string PaymentMethodId { get; set; }
        public string SavedCcExpiration { get; set; }
		public string BillingRequestId { get; set; }
		public string AuthRequestId { get; set; }
		public string Currency { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SurrogateAccountNumber { get; set; }
        public string CcType { get; set; }

		public List<OrderItem> Items { get; set; } 
	}
}
