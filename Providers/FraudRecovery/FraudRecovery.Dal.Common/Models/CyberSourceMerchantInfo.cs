﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudRecovery.Dal.Common.Models
{
    public class CyberSourceMerchantInfo
    {
        public string Currency { get; set; }
        public string MarketCode { get; set; }
        public string TransactionType { get; set; }
        public string MerchantKey { get; set; }
    }
}
