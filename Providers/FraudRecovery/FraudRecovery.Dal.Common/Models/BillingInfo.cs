﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudRecovery.Dal.Common.Models
{
    [DataContract(Name = "billing")]
	public class BillingInfo
    {
        [DataMember(Name = "email", IsRequired = false)]
        public string Email { get; set; }

        [DataMember(Name = "paymentMethodInfo", IsRequired = false)]
        public PaymentMethodInfo PaymentMethodInfo { get; set; }

        [DataMember(Name = "addressInfo", IsRequired = false)]
		public AddressInfo AddressInfo { get; set; }
	}
}
