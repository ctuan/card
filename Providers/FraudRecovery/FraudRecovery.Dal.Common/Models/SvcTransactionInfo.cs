﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudRecovery.Dal.Common.Models
{
	public class SvcTransactionInfo
	{
		public string SvcTransactionId { get; set; }
		public SvcCardInfo SvcCard { get; set; }
	}
}
