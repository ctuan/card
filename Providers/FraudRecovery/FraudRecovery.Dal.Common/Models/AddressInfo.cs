﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudRecovery.Dal.Common.Models
{
    [DataContract(Name = "address")]
    public class AddressInfo
    {
        [DataMember(Name = "addressId", IsRequired = false)]
		public string AddressId { get; set; }

        [DataMember(Name = "addressDetails", IsRequired = false)]
		public Address AddressDetails { get; set; }
	}
}
