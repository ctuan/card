﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starbucks.FraudRecovery.Dal.Common.Models
{
	public class SvcCardInfo
	{
		public SvcCardInfo() : this (-1, string.Empty, string.Empty, string.Empty)
		{
		}

		public SvcCardInfo(int id, string number, string pin, string market)
		{
			Id = id;
			Number = number;
			Pin = pin;
			Market = market;
		}

		public int Id { get; set; }
		public string Number { get; set; }
		public string Pin { get; set; }
		public string Market { get; set; }
	}
}
