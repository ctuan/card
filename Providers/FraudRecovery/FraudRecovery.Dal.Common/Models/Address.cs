﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudRecovery.Dal.Common.Models
{
    [DataContract(Name = "addressInfo")]
    public class Address
    {
        [DataMember(Name = "firstName", IsRequired = true)]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName", IsRequired = true)]
        public string LastName { get; set; }

        [DataMember(Name = "addressLine1", IsRequired = true)]
        public string AddressLine1 { get; set; }

        [DataMember(Name = "addressLine2", IsRequired = false)]
        public string AddressLine2 { get; set; }

        [DataMember(Name = "city", IsRequired = true)]
        public string City { get; set; }

        [DataMember(Name = "countrySubdivision", IsRequired = true)]
        public string CountrySubdivision { get; set; }

        [DataMember(Name = "postalCode", IsRequired = true)]
        public string PostalCode { get; set; }

        [DataMember(Name = "country", IsRequired = true)]
        public string Country { get; set; }

        [DataMember(Name = "phoneNumber", IsRequired = true)]
        public string PhoneNumber { get; set; }

		[DataMember(Name = "isTemporary", IsRequired = false)]
		public bool IsTemporary { get; set; }

		[DataMember(Name = "addressTpyeId", IsRequired = false)]
		public int AddressTypeId { get; set; }

		[DataMember(Name = "emailAddress", IsRequired = false)]
		public string EmailAddress { get; set; }
    }
}
