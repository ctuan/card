﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Starbucks.FraudRecovery.Dal.Common.Models
{
    [DataContract(Name = "paymentMethod")]
	public class PaymentMethodInfo
    {
        [DataMember(Name = "paymentId", IsRequired = false)]
        public string PaymentId { get; set; }

        [DataMember(Name = "creditCardDetails", IsRequired = false)]
        public CreditCard CreditCard { get; set; }

	}
}
