﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starbucks.FraudRecovery.Dal.Common.Models;

namespace Starbucks.FraudRecovery.Dal.Common
{
	public interface IFraudRecoveryDal
	{
		SvcCardInfo GetSvcCard(int decryptCardId);
		SvcCardInfo GetSvcCard(string cardNumber);

		OrderInfo GetOrder(string orderId);
		SvcTransactionInfo GetTransaction(long transactionId);
        CyberSourceMerchantInfo GetCyberSourceMerchantInfo(string submarketCode, string currency, string transactionType);

		AddressInfo GetAddress(string userId, string addressId);
		BillingInfo GetBilling(string userId, string paymentMethodId);
	}
}
