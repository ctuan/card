﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Helpers;
using Starbucks.FraudRecovery.Common.Models;
using Starbucks.FraudRecovery.Provider;

namespace Starbucks.FraudRecovery.Tests
{
	/// <summary>
	/// Summary description for FraudRecoveryTests
	/// </summary>
	[TestClass]
	public class FraudRecoveryTests : TestBase
	{
		public FraudRecoveryTests()
		{
		}

		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		//
		// You can use the following additional attributes as you write your tests:
		//
		// Use ClassInitialize to run code before running the first test in the class
		// [ClassInitialize()]
		// public static void MyClassInitialize(TestContext testContext) { }
		//
		// Use ClassCleanup to run code after all tests in a class have run
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		// Use TestInitialize to run code before running each test 
		// [TestInitialize()]
		// public void MyTestInitialize() { }
		//
		// Use TestCleanup to run code after each test has run
		// [TestCleanup()]
		// public void MyTestCleanup() { }
		//
		#endregion

		#region card id related tests

		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_Provider_CardGetBalace_HappyPath()
		{
			IFraudRecoveryProvider provider = GetProvider();
			int cardId = InitFraudRecoveryProviderTest(provider);

			VerifyGetCardBalance(provider, cardId, TestInitCardAmount);
		}

		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_Provider_CardRevokeAmount_HappyPath()
		{
			IFraudRecoveryProvider provider = GetProvider();
			int cardId = InitFraudRecoveryProviderTest(provider);

			decimal amount = 3m;
			var result = VerifyCardRevokeAmount(provider, cardId, amount);
			Assert.IsTrue(result.Result.EndingBalance == TestInitCardAmount - amount);

			VerifyGetCardBalance(provider, cardId, TestInitCardAmount - amount);
		}


		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
        [Ignore]
		public void FraudRecovery_Provider_CardLoadAmount_HappyPath()
		{
			IFraudRecoveryProvider provider = GetProvider();
			int cardId = InitFraudRecoveryProviderTest(provider);

			decimal amount = 3m;
			var result = VerifyCardloadAmount(provider, cardId, amount);
			Assert.IsTrue(result.Result.EndingBalance == TestInitCardAmount + amount);

			VerifyGetCardBalance(provider, cardId, TestInitCardAmount + amount);
		}


		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
        [Ignore]
		public void FraudRecovery_Provider_CardCashout_HappyPath()
		{
			IFraudRecoveryProvider provider = GetProvider();
			int cardId = InitFraudRecoveryProviderTest(provider);

			var result = VerifyCardCashOut(provider, cardId, TestInitCardAmount);

			VerifyGetCardBalance(provider, cardId, 0);
		}

		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
        [Ignore]
		public void FraudRecovery_Provider_CardFreeze_HappyPath()
		{
			IFraudRecoveryProvider provider = GetProvider();
			int cardId = InitFraudRecoveryProviderTest(provider);

			VerifyCardFreeze(provider, cardId, TestInitCardAmount);

			VerifyCardUnFreeze(provider, cardId, TestInitCardAmount);

		}


		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_Provider_CardUnfreeze_HappyPath()
		{
			IFraudRecoveryProvider provider = GetProvider();
			int cardId = InitFraudRecoveryProviderTest(provider);

			VerifyCardFreeze(provider, cardId, TestInitCardAmount);

			decimal amount = GetUnfreezeEndingBalance(TestInitCardAmount);
            VerifyCardUnFreeze(provider, cardId, TestInitCardAmount);

			VerifyGetCardBalance(provider, cardId, amount);
		}

		#endregion

		#region transaction id related tests
		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_Provider_TransactionRevoke_HappyPath()
		{
			IFraudRecoveryProvider provider = GetProvider();
			int cardId = InitFraudRecoveryProviderTest(provider, TestTransactionSvcCardNumber);
			VerifyGetCardBalance(provider, cardId, TestInitCardAmount);

			var result = provider.RevokeTransaction(TestOrderId);
			Assert.IsTrue(result != null && result.Status == OperationStatus.Succeeded);
			Assert.IsTrue(result.Results != null && result.Results.Count == 1);
			Assert.IsTrue(result.Results[0].Status == OperationStatus.Succeeded);

			VerifyGetCardBalance(provider, cardId, TestInitCardAmount - 10);
		}

        [TestMethod]
        [TestCategory("FraudRecovery_Integration")]
        [Ignore]
        public void FraudRecovery_Provider_TransactionRevokeCAD_HappyPath()
        {
            IFraudRecoveryProvider provider = GetProvider();
            int cardId = InitFraudRecoveryProviderTest(provider, TestTransactionSvcCardNumber);
            VerifyGetCardBalance(provider, cardId, TestInitCardAmount);

            var result = provider.RevokeTransaction(TestOrderCADId);
            Assert.IsTrue(result != null && result.Status == OperationStatus.Succeeded);
            Assert.IsTrue(result.Results != null && result.Results.Count == 1);
            Assert.IsTrue(result.Results[0].Status == OperationStatus.Succeeded);
        }

		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_Provider_TransactionFreeze_HappyPath()
		{
			IFraudRecoveryProvider provider = GetProvider();
			int cardId = InitFraudRecoveryProviderTest(provider, TestTransactionSvcCardNumber);

			var result = provider.FreezeTransaction(TestOrderId);
			Assert.IsTrue(result != null && result.Status == OperationStatus.Succeeded);
			Assert.IsTrue(result.Results != null && result.Results.Count == 1);
			Assert.IsTrue(result.Results[0].Status == OperationStatus.Succeeded);
			Assert.IsTrue(result.Results[0].Reason == null);
			Assert.IsTrue(result.Results[0].Result != null);
			Assert.IsTrue(result.Results[0].Result.LockAmount == TestInitCardAmount);
		}

		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_Provider_TransactionUnfreeze_HappyPath()
		{
			IFraudRecoveryProvider provider = GetProvider();
			int cardId = InitFraudRecoveryProviderTest(provider, TestTransactionSvcCardNumber);

			var freeze = provider.FreezeTransaction(TestOrderId);
			Assert.IsTrue(freeze != null && freeze.Status == OperationStatus.Succeeded);

			var result = provider.UnfreezeTransaction(TestOrderId);
			Assert.IsTrue(result != null && result.Status == OperationStatus.Succeeded);
			Assert.IsTrue(result.Results != null && result.Results.Count == 1);
			Assert.IsTrue(result.Results[0].Status == OperationStatus.Succeeded);
			Assert.IsTrue(result.Results[0].Reason == null);
			Assert.IsTrue(result.Results[0].Result != null);
			Assert.IsTrue(result.Results[0].Result.LockAmount == 0);
			Assert.IsTrue(result.Results[0].Result.EndingBalance == GetUnfreezeEndingBalance(TestInitCardAmount));
		}

		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_Provider_TransactionRefund()
		{
			
			IFraudRecoveryProvider provider = GetProvider();
			int cardId = InitFraudRecoveryProviderTest(provider, TestTransactionSvcCardNumber);

			var result = provider.RefundTransaction(TestOrderId3);
			Assert.IsTrue(result.Result != null);
		}
		#endregion


		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_Provider_ReasonCode ()
		{
			foreach (ReasonTypes errType in Enum.GetValues(typeof(ReasonTypes)))
			{
				string str = errType.GetErrorCodeFromResource ();
				Assert.IsTrue(!string.IsNullOrWhiteSpace(str), "unable to get error code from resource: " + errType.ToString());

				str = errType.GetErrorMessageFromResource();
				Assert.IsTrue(!string.IsNullOrWhiteSpace(str), "unable to get error messge from resource: " + errType.ToString());
			}
		}
	}
}
