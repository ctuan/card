﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.FraudRecovery.Dal.Common.Models;
using Starbucks.Platform.Security;

namespace Starbucks.FraudRecovery.Tests
{
	/// <summary>
	/// Summary description for FraudRecoveryDalTests
	/// </summary>
	[TestClass]
	public class FraudRecoveryDalTests : TestBase
	{
		public FraudRecoveryDalTests()
		{
		}

		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		//
		// You can use the following additional attributes as you write your tests:
		//
		// Use ClassInitialize to run code before running the first test in the class
		// [ClassInitialize()]
		// public static void MyClassInitialize(TestContext testContext) { }
		//
		// Use ClassCleanup to run code after all tests in a class have run
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		// Use TestInitialize to run code before running each test 
		// [TestInitialize()]
		// public void MyTestInitialize() { }
		//
		// Use TestCleanup to run code after each test has run
		// [TestCleanup()]
		// public void MyTestCleanup() { }
		//
		#endregion

		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_Dal_GetOrder()
		{
			var dal = GetFraudRecoveryDal();

			OrderInfo order = dal.GetOrder(TestOrderId);
			Assert.IsTrue(order != null && order.Items != null && order.Items.Count == 1);
			Assert.IsTrue( !string.IsNullOrWhiteSpace(order.Items[0].SvcTransactionId) );

			System.Diagnostics.Debug.WriteLine(order.Items[0].SvcTransactionId);
		}


		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_Dal_GetTransaction()
		{
			var dal = GetFraudRecoveryDal();

			SvcTransactionInfo info = dal.GetTransaction(TestTransactionId);
			Assert.IsTrue(info != null && info.SvcCard != null);
			Assert.IsTrue(!string.IsNullOrWhiteSpace(info.SvcCard.Number));

			System.Diagnostics.Debug.WriteLine(info.SvcCard.Number);
		}


		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_Dal_GetCardByNumber()
		{
			var dal = GetFraudRecoveryDal();
			SvcCardInfo info = dal.GetSvcCard(TestTransactionSvcCardNumber);
			Assert.IsTrue(info != null);
			Assert.IsTrue(!string.IsNullOrWhiteSpace(info.Number));
			Assert.IsTrue(!string.IsNullOrWhiteSpace(info.Pin));

			System.Diagnostics.Debug.WriteLine(info.Number);
		}

		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
        [Ignore]
		public void FraudRecovery_Dal_GetPaymentMethod()
		{
			var dal = GetFraudRecoveryDal();
            string paymentId = Encryption.EncryptCardId(TestPaymentMethodRawId);
            var info = dal.GetBilling(TestUserAccount, paymentId);

			Assert.IsTrue(info != null && info.PaymentMethodInfo != null && info.PaymentMethodInfo.CreditCard != null);
			Assert.IsTrue(info != null && info.AddressInfo != null && info.AddressInfo.AddressDetails != null);
		}
	}
}
