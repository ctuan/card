﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Helpers;
using Starbucks.FraudRecovery.Common.Models;
using Starbucks.FraudRecovery.Provider;
using Starbucks.FraudRecovery.Provider.Commands;
using Sdk = Starbucks.Api.Sdk.PaymentService;
using SdkModels = Starbucks.Api.Sdk.PaymentService.Models;
using DalModels = Starbucks.FraudRecovery.Dal.Common.Models;
using Starbucks.FraudRecovery.Dal.Common.Models;
using Starbucks.FraudRecovery.Common.Logging;

namespace Starbucks.FraudRecovery.Tests
{
	/// <summary>
	/// Summary description for FraudRecoveryTests
	/// </summary>
	[TestClass]
	public class FraudRecoveryCommonTests : TestBase
	{
		public FraudRecoveryCommonTests()
		{
		}

		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		#endregion

        protected DalModels.PaymentMethodInfo GetPaymentMethodInfo()
        {
            DalModels.PaymentMethodInfo paymentMethodInfo = new DalModels.PaymentMethodInfo()
            {
                CreditCard = new CreditCard()
                {
                    Currency = "USD",
                    Cvn = "678",
                    ExpireYear = 2019,
                    ExpireMonth = 12,
                    LastFourNumber = "6789"
                },

                PaymentId = "123"
            };
            
            return paymentMethodInfo;
        }

        protected SdkModels.BillingInfo GetSdkBillingInfo()
        {
            SdkModels.BillingInfo sdkBillingInfo = new SdkModels.BillingInfo();
            sdkBillingInfo.AddressInfo = new SdkModels.AddressInfo();
            sdkBillingInfo.AddressInfo.AddressDetails = new SdkModels.Address() { Country = "US" , City = "Seattle", AddressLine1 = "401 Utah St." };
            SdkModels.PaymentMethod paymentMethod = new SdkModels.PaymentMethod();
            //paymentMethod.PaymentId = "789";
            SdkModels.CreditCardDetails creditCardDetails = new SdkModels.CreditCardDetails();
            creditCardDetails.ExpireMonth = 12;
            creditCardDetails.ExpireYear = 2019;
            creditCardDetails.FullName = "test test";
            creditCardDetails.SurrogateNumber = "12345678";
            creditCardDetails.Type = SdkModels.CreditCardTypes.Visa;

            paymentMethod.CreditCardDetails = creditCardDetails;

            sdkBillingInfo.PaymentMethod = paymentMethod;

            return sdkBillingInfo;
        }

        protected DalModels.BillingInfo GetBillingInfo()
        {
            DalModels.BillingInfo billingInfo = new DalModels.BillingInfo();
            billingInfo.PaymentMethodInfo = new PaymentMethodInfo();
            billingInfo.PaymentMethodInfo.PaymentId = "123";
            billingInfo.PaymentMethodInfo.CreditCard = new CreditCard()
            {
                Currency = "USD",
                Cvn = "678",
                ExpireMonth = 12,
                ExpireYear = 2019,
                IsTemporary = false,
                LastFourNumber = "6789",
                SurrogateNumber = "ABABA",
                FullName = "Test Test",
                Type = CreditCardTypes.Visa
            };
            billingInfo.AddressInfo = new AddressInfo();
            billingInfo.Email = "test123@sbx.com";
            billingInfo.AddressInfo.AddressDetails = new Starbucks.FraudRecovery.Dal.Common.Models.Address() { Country = "US"};

            return billingInfo;
        }

        protected Sdk.Models.CreditRequest GetCreditRequest()
        {
            Sdk.Models.CreditRequest creditRequest = new Sdk.Models.CreditRequest();
            creditRequest.Basket = new List<SdkModels.OrderItem>() { GetSdkModelsOrderItem((decimal)5.0), GetSdkModelsOrderItem((decimal)10.0) };
            creditRequest.Market = "US";
            creditRequest.CurrencyCode = "USD";
            creditRequest.MerchantReferenceCode = "123";
            creditRequest.TransactionType = SdkModels.TransactionTypes.Reload;
            creditRequest.BillingInfo = GetSdkBillingInfo();

            return creditRequest;
        }

        protected Sdk.Models.CreditResult GetCreditResult()
        {
            Sdk.Models.CreditResult creditResult = new Sdk.Models.CreditResult();
            creditResult.Amount = "10";
            creditResult.DateTime = DateTime.Now.ToString();
            creditResult.Decision = SdkModels.DecisionType.Accept;
            creditResult.ReasonCode = new SdkModels.DecisionReason();

            return creditResult;
        }

        protected SdkModels.OrderItem GetSdkModelsOrderItem(decimal value)
        {
            SdkModels.OrderItem item = new SdkModels.OrderItem()
            {
                Amount = (decimal)value
            };

            return item;
        }

        protected DalModels.OrderItem GetOrderItem(int quantity, decimal price)
        {
            DalModels.OrderItem item = new DalModels.OrderItem()
            {
                UnitPrice = price,
                Quantity = quantity
            };

            return item;
        }

        protected DalModels.OrderItem GetOrderItem(decimal total)
        {
            DalModels.OrderItem item = new DalModels.OrderItem()
            {
                Total = (decimal)total
            };

            return item;
        }

        protected DalModels.OrderInfo GetOrderInfo()
        {
            DalModels.OrderInfo info = new DalModels.OrderInfo();
            List<OrderItem> orderItems = new List<OrderItem>() { GetOrderItem((decimal)5.0), GetOrderItem((decimal)10.0) };
            info.Items = orderItems;
            return info;
        }

        protected DalModels.AddressInfo GetAddressInfo()
        {
            AddressInfo addressInfo = new AddressInfo();
            addressInfo.AddressDetails = new Starbucks.FraudRecovery.Dal.Common.Models.Address() { Country = "US", AddressLine1 = "111 main st", City = "Seattle", FirstName = "First", LastName = "Last_Name" };

            return addressInfo;
        }


		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
        [Ignore]
        public void FraudRecovery_Provider_Common_CreditResult_ToModel()
		{
            Sdk.Models.CreditResult creditResult = GetCreditResult();
            CreditCardOperationResult creditCardOperationResult = ExtensionHelper.ToModel(creditResult, new OperationResponse("Test"), "USD", null, null, null);
            Assert.IsTrue(creditCardOperationResult.Amount == creditResult.Amount);
		}

        [TestMethod]
        [TestCategory("FraudRecovery_Integration")]
        public void FraudRecovery_Provider_Common_BillingInfo_ToMarket()
        {
            DalModels.BillingInfo billingInfo = GetBillingInfo();
            string market = ExtensionHelper.ToMarket(billingInfo);
            Assert.IsTrue(market == "US");
        }

        [TestMethod]
        [TestCategory("FraudRecovery_Integration")]
        public void FraudRecovery_Provider_Common_BillingInfo_ToNative()
        {
            DalModels.BillingInfo billingInfo = GetBillingInfo();
            SdkModels.BillingInfo sdkBillingInfo = ExtensionHelper.ToNative(billingInfo);
            Assert.IsTrue(billingInfo.Email == sdkBillingInfo.Email);
        }

        [TestMethod]
        [TestCategory("FraudRecovery_Integration")]
        public void FraudRecovery_Provider_Common_OrderItem_Total()
        {
            DalModels.OrderItem item = GetOrderItem((decimal)10.0);
            decimal total = ExtensionHelper.ToOrderItemTotal(item);
            Assert.IsTrue(total == item.Total);
        }

        [TestMethod]
        [TestCategory("FraudRecovery_Integration")]
        [Ignore]
        public void FraudRecovery_Provider_Common_OrderItem_ToTransactionType()
        {
            DalModels.OrderItem item = GetOrderItem((decimal)10.0);
            SdkModels.TransactionTypes transactionTypes = ExtensionHelper.ToTransactionType(item);
            Assert.IsTrue(transactionTypes == SdkModels.TransactionTypes.Reload);
        }

        [TestMethod]
        [TestCategory("FraudRecovery_Integration")]
        public void FraudRecovery_Provider_Common_OrderItem_ToOrderItemTotal1()
        {
            DalModels.OrderItem item = GetOrderItem((decimal)10.0);
            decimal total = ExtensionHelper.ToOrderItemTotal(item);
            Assert.IsTrue(total == 10.0m);
        }

        [TestMethod]
        [TestCategory("FraudRecovery_Integration")]
        public void FraudRecovery_Provider_Common_OrderItem_ToOrderItemTotal2()
        {
            DalModels.OrderItem item = GetOrderItem(3, 10.0m); // number of units and unit price 
            decimal total = ExtensionHelper.ToOrderItemTotal(item);
            Assert.IsTrue(total == 30.0m);
        }

        [TestMethod]
        [TestCategory("FraudRecovery_Integration")]
        public void FraudRecovery_Provider_Common_OrderInfo_ToBasket()
        {
            DalModels.OrderInfo info = GetOrderInfo();
            List<SdkModels.OrderItem> basket = ExtensionHelper.ToBasket(info);
            Assert.IsTrue(basket.Count == info.Items.Count);
        }

        [TestMethod]
        [TestCategory("FraudRecovery_Integration")]
        public void FraudRecovery_Provider_Common_AddressInfo_ToNative()
        {
            DalModels.AddressInfo addressInfo = GetAddressInfo();
            SdkModels.AddressInfo sdkModelAddressInfo = ExtensionHelper.ToNative(addressInfo);
            Assert.IsTrue(sdkModelAddressInfo.AddressDetails.AddressLine1 == addressInfo.AddressDetails.AddressLine1);
        }

        [TestMethod]
        [TestCategory("FraudRecovery_Integration")]
        public void FraudRecovery_Provider_Common_PaymentMethodInfo_ToNative()
        {
            DalModels.PaymentMethodInfo paymentMethodInfo = GetPaymentMethodInfo();
            SdkModels.PaymentMethod sdkModelsPaymentMethod = ExtensionHelper.ToNative(paymentMethodInfo);
            Assert.IsTrue(sdkModelsPaymentMethod.PaymentId == paymentMethodInfo.PaymentId);
        }

        [TestMethod]
        [TestCategory("FraudRecovery_Integration")]
        public void FraudRecovery_Provider_Common_GetCreditRequest_IsValid()
        {
            Sdk.Models.CreditRequest creditRequest = GetCreditRequest();
            bool valid = ExtensionHelper.IsValid(creditRequest);
            Assert.IsTrue(valid == true);
        }

        [TestMethod]
        [TestCategory("FraudRecovery_Integration")]
        public void FraudRecovery_Provider_Common_GetCreditRequest_IsValid_Empty_Market()
        {
            Sdk.Models.CreditRequest creditRequest = GetCreditRequest();
            creditRequest.Market = null;
            bool valid = ExtensionHelper.IsValid(creditRequest);
            Assert.IsTrue(valid == false);
        }

        [TestMethod]
        [TestCategory("FraudRecovery_Integration")]
        public void FraudRecovery_Provider_Common_GetCreditRequest_IsValid_Empty_CurrencyCode()
        {
            Sdk.Models.CreditRequest creditRequest = GetCreditRequest();
            creditRequest.CurrencyCode = null;
            bool valid = ExtensionHelper.IsValid(creditRequest);
            Assert.IsTrue(valid == false);
        }

        [TestMethod]
        [TestCategory("FraudRecovery_Integration")]
        public void FraudRecovery_Provider_Common_GetCreditRequest_IsValid_Empty_MerchantReferenceCode()
        {
            Sdk.Models.CreditRequest creditRequest = GetCreditRequest();
            creditRequest.MerchantReferenceCode = null;
            bool valid = ExtensionHelper.IsValid(creditRequest);
            Assert.IsTrue(valid == false);
        }

        [TestMethod]
        [TestCategory("FraudRecovery_Integration")]
        public void FraudRecovery_Provider_Common_GetCreditRequest_IsValid_Empty_TransactionTypes()
        {
            Sdk.Models.CreditRequest creditRequest = GetCreditRequest();
            creditRequest.TransactionType = SdkModels.TransactionTypes.None;
            bool valid = ExtensionHelper.IsValid(creditRequest);
            Assert.IsTrue(valid == false);
        }


        // This unit test is added for code coverage
        [TestMethod]
        [TestCategory("FraudRecovery_Integration")]
        public void FraudRecovery_Provider_Common_Logging()
        {
            try
            { 
                DebugLoggingUtility logger = new DebugLoggingUtility(true);
                logger.EnsureObjectNotNull("TestMessage");
                LoggingEntry loggingEntry = new LoggingEntry();
                loggingEntry.Title = "TestTitle";
                loggingEntry.Message = "TestMessage";
                logger.AddEntry(loggingEntry);
                logger.Log(LogType.Error, "TestTitle", "TestMessage");
                logger.LogCounter("test", 10);
                logger.Commit();
                logger.Cleanup();
            }
            catch(Exception ex)
            {
                Assert.Fail("Caught exception in log method" + ex.Message);
            }        
        }

        // This unit test is added for code coverage
        [TestMethod]
        [TestCategory("FraudRecovery_Integration")]
        public void FraudRecovery_Provider_Common_DebugLoggingUtility()
        {
            try
            {
                DebugLoggingUtility.PerformLogCounter("test", 10);
            }
            catch (Exception ex)
            {
                Assert.Fail("Caught exception in log method" + ex.Message);
            }  
        }
	}
}
