﻿using System;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Starbucks.Api.Sdk.PaymentService;
using Starbucks.Api.Sdk.WebRequestWrapper;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;
using Starbucks.FraudRecovery.Dal;
using Starbucks.FraudRecovery.Dal.Common;
using Starbucks.FraudRecovery.Provider;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.Card.Dal.Sql;
using Starbucks.Card.Dal.Sql.Configuration;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.SvDot;
using Starbucks.CardIssuer.Dal.SvDot.Configuration;
using Starbucks.CardIssuer.Dal.SvDot.Db;
using Starbucks.CardIssuer.Dal.SvDot.Requestor;
using Starbucks.CardIssuer.Dal.SvDot.TransportModels;
using Starbucks.FraudRecovery.Provider.Configuration;
using Starbucks.FraudRecovery.Provider.Facades;
using Starbucks.LogCounter.Provider;
using Starbucks.MessageBroker.Common;
using Starbucks.MessageBroker.Common.Models;
using Starbucks.PaymentMethod.Provider;
using Starbucks.Platform.Security;
using SettingsProvider = Starbucks.Settings.Provider.SettingsProvider;
using Account.Provider.Common;


namespace Starbucks.FraudRecovery.Tests
{
    public class TestBase
    {
        protected readonly static string[] TestSvCardNumbers = { "7777083002943493", "7777083002953493", "7777083002964149" };
        protected readonly static string[] TestSvCardPins = { "33151451", "25567528", "56387208" };
        protected const string UnitedStatesTippingMarketId = "99030169997";
        protected const string USMid = "99910439997";
        //protected const string USMid = "99030169997";


        protected const string StoreId = "00303";
        protected const string TerminalId = "0001";

        protected static readonly int[] TestCardId =
            {
                80196712,
                80196714,
                80196715,
                80196716,
                80196717,
                80196718,
                80196719,
                80196743,
            };

        protected const string TestOrderId = "D1B80M50V2394L7TQ6GFAQAQ31";
        protected const string TestOrderCADId = "98BB735QS9E45E2DU3240A4H26";
        protected const string TestOrderId2 = "T261EA27F7GC4U3NN5V12T4DG2";
        protected const string TestOrderId3 = "19N241941563545BA7R41P45U4";
        protected const long TestTransactionId = 2147992144;
        protected const string TestTransactionSvcCardNumber = "7777079661318020";
        //protected const string TestTransactionSvcCardNumber = "7777082998298438";

        //protected const string TestTransactionSvcCardNumber = "7777080270536355";

        protected const decimal TestInitCardAmount = 35.8m;

        public const string TestUserAccount = "8A63A02C-7E4C-490E-B9F0-12C27211FB9B";
        public const string TestShippingAddressId = "81876354-d157-4d32-80dd-75766a930b99";
        public const string TestUserBillingAddressId = "b75e2d47-be9c-494c-96fa-d4e17cd01828";
        //public const string TestCardId = "571616";
        public const string TestSurrogateAccountNumber = "NDExMTExMTExMTExMTExMQ==";
        public const int TestPaymentMethodRawId = 593842;
        public const string TestCreditCardNumber = "4111111111111111";

        //protected const string TestUserId = "C96F8FDD-BB9E-4286-931F-EB731739CD6B";
        //protected const string TestUserBillingAddressId = "86e4dda7-bece-47a1-b716-9d25831b8176";
        //protected const int TestPaymentRawId = 659969;

        protected decimal GetUnfreezeEndingBalance(decimal expectedValue)
        {
            return expectedValue - Convert.ToDecimal(ConfigurationManager.AppSettings["unfreezeRedeem"]);
        }

        protected ICardIssuerDal GetCardIssuer(IMessageBroker messageBroker)
        {
            var cardIssuer = new SvDotCardIssuer
                (
                new ServiceRequestor
                    (
                    new CardTransactionDal("CardTransactions", "Commerce"),
                    new SvDotTransportXml(
                        ConfigurationManager.GetSection("svDotConfiguration") as SvDotConfigurationSettings),
                    ConfigurationManager.GetSection("svDotConfiguration") as SvDotConfigurationSettings,
                    LogCounterManager.Instance()
                    ),
                new CardTransactionDal("CardTransactions", "Commerce"),
                messageBroker,
                ConfigurationManager.GetSection("svDotConfiguration") as SvDotConfigurationSettings,
                new Starbucks.Settings.Provider.SettingsProvider(),
                new CardTransactionDal("CardTransactions", "Commerce"),
                LogCounterManager.Instance()
                );
            return cardIssuer;
        }

        protected IMerchantInfo GetMerchantInfo(ICardIssuerDal cardIssuer, string market = "GSUS", string storeId = StoreId,
                                              string mid = USMid, string terminalId = TerminalId)
        {
            IMerchantInfo merchantInfo = cardIssuer.GetMerchantInfo(market);
            Assert.IsTrue(merchantInfo != null);

            merchantInfo.AlternateMid = storeId;
            merchantInfo.Mid = mid;
            merchantInfo.TerminalId = terminalId;

            return merchantInfo;
        }

        public IFraudRecoveryDal GetFraudRecoveryDal(IFraudRecoveyConfiguration config, ILoggingUtility logging)
        {

            IFraudRecoveryDal dal = new FraudRecoveryDal(config, DalObjectFactory.GetCardDal(),
                                                         DalObjectFactory.GetAddressDataProvider(),
                                                         DalObjectFactory.GetPaymentMethodDataProvider(),
                                                         logging);
            return dal;
        }

        public IFraudRecoveryDal GetFraudRecoveryDal()
        {
            ILoggingUtility logger = new DebugLoggingUtility();
            IFraudRecoveyConfiguration config = ProviderConfigurationSection.GetConfiguration(logger);
            return GetFraudRecoveryDal(config, logger);
        }

        protected IFraudRecoveryProvider GetProvider()
        {
            var mockBroker = new Mock<IMessageBroker>();
            IMessageBroker broker = mockBroker.Object;
            ILoggingUtility logger = new DebugLoggingUtility();
            IFraudRecoveyConfiguration config = ProviderConfigurationSection.GetConfiguration(logger);

            IPaymentServiceApiClient paymentService = new PaymentServiceApiClient(new WebRequestWrapper());
            ISqlDbFacade sqlFacade = new SqlDbFacade(logger);
            IDependencyFactory factory = new DependencyFactory(GetCardIssuer(broker), GetFraudRecoveryDal(config, logger), paymentService, sqlFacade, config, logger);

            //TODO, use mock ?
            IAccountProvider accountProvider = null;

            var cardTransactionDal = new Mock<ICardTransactionDal>();
            var fraudRecoveryDal = new Mock<IFraudRecoveryDal>();
            var merchantInfo = new Mock<IMerchantInfo>();
            var orderAddressInfo = new Mock<IOrderAddressInfo>();

            merchantInfo.SetupProperty(x => x.SubMarketId, "US");
            merchantInfo.SetupProperty(x => x.CurrencyCode, "USD");

            orderAddressInfo.SetupProperty(x => x.AddressLine1, "123 main st.");
            orderAddressInfo.SetupProperty(x => x.City, "Seattle");
            orderAddressInfo.SetupProperty(x => x.Country, "US");

            cardTransactionDal.Setup(x => x.GetMerchantInfo(It.IsAny<string>(), It.IsAny<string>())).Returns(merchantInfo.Object);
            cardTransactionDal.Setup(x => x.GetOrderGroupAddress(It.IsAny<string>())).Returns(orderAddressInfo.Object);

            var provider = new FraudRecoveryProvider(factory, accountProvider, cardTransactionDal.Object, fraudRecoveryDal.Object);

            return provider;
        }

        protected int InitFraudRecoveryProviderTest(IFraudRecoveryProvider provider, string cardNumber)
        {
            IFraudRecoveryDal dal = GetFraudRecoveryDal();
            var cardInfo = dal.GetSvcCard(cardNumber);

            int rowId = Encryption.DecryptCardId(cardInfo.Id.ToString());

            return InitFraudRecoveryProviderTest(provider, cardInfo.Id);
        }

        protected int InitFraudRecoveryProviderTest(IFraudRecoveryProvider provider)
        {
            int cardId = TestCardId[5];
            return InitFraudRecoveryProviderTest(provider, cardId);
        }

        protected int InitFraudRecoveryProviderTest(IFraudRecoveryProvider provider, int cardId)
        {
            provider.SvcCardUnfreeze(cardId);

            decimal balance = GetCardBalance(provider, cardId);
            if (balance > 0m)
            {
                var revoke = provider.SvcCardRevokeAmount(cardId, balance);
                Assert.IsTrue(revoke != null && revoke.Status == OperationStatus.Succeeded && revoke.Result != null);
                Assert.IsTrue(revoke.Result.EndingBalance == 0);
            }

            VerifyCardloadAmount(provider, cardId, TestInitCardAmount);
            return cardId;
        }

        public decimal GetCardBalance(IFraudRecoveryProvider provider, int cardId)
        {
            var result = provider.SvcCardBalance(cardId);
            Assert.IsTrue(result != null && result.Status == OperationStatus.Succeeded);
            Assert.IsTrue(result.Result != null && result.Reason == null);

            return result.Result.EndingBalance;
        }

        public void VerifyGetCardBalance(IFraudRecoveryProvider provider, int cardId, decimal amount)
        {
            var balance = GetCardBalance(provider, cardId);
            Assert.IsTrue(balance == amount);
        }

        public SvcCardCashOutResponse VerifyCardCashOut(IFraudRecoveryProvider provider, int cardId, decimal amount)
        {
            var result = provider.SvcCardCashOut(cardId);

            Assert.IsTrue(result != null && result.Status == OperationStatus.Succeeded);
            Assert.IsTrue(result.Result != null && result.Reason == null);
            Assert.IsTrue(result.Result.Amount == amount);
            Assert.IsTrue(result.Result.BeginningBalance == amount);
            Assert.IsTrue(result.Result.EndingBalance == 0m);

            return result;
        }

        public SvcCardLoadAmountResponse VerifyCardloadAmount(IFraudRecoveryProvider provider, int cardId, decimal amount)
        {
            var result = provider.SvcCardLoadAmount(cardId, amount);

            Assert.IsTrue(result != null && result.Status == OperationStatus.Succeeded);
            Assert.IsTrue(result.Result != null && result.Reason == null);
            Assert.IsTrue(result.Result.Amount == amount);
            Assert.IsTrue((result.Result.EndingBalance - result.Result.BeginningBalance) == amount);

            return result;
        }

        public SvcCardRevokeAmountResponse VerifyCardRevokeAmount(IFraudRecoveryProvider provider, int cardId, decimal amount)
        {
            var result = provider.SvcCardRevokeAmount(cardId, amount);

            Assert.IsTrue(result != null && result.Status == OperationStatus.Succeeded);
            Assert.IsTrue(result.Result != null && result.Reason == null);
            Assert.IsTrue(result.Result.Amount == amount);
            Assert.IsTrue((result.Result.BeginningBalance - result.Result.EndingBalance) == amount);

            return result;
        }

        public SvcCardFreezeResponse VerifyCardFreeze(IFraudRecoveryProvider provider, int cardId, decimal amount)
        {
            var result = provider.SvcCardFreeze(cardId);
            Assert.IsTrue(result != null && result.Status == OperationStatus.Succeeded);
            Assert.IsTrue(result.Result != null && result.Result.LockAmount == amount);

            return result;
        }

        public SvcCardUnfreezeResponse VerifyCardUnFreeze(IFraudRecoveryProvider provider, int cardId, decimal amount)
        {
            var result = provider.SvcCardUnfreeze(cardId);
            Assert.IsTrue(result != null && result.Status == OperationStatus.Succeeded);


            Assert.IsTrue(result.Result != null && result.Result.LockAmount == 0 && result.Result.EndingBalance == GetUnfreezeEndingBalance(amount));

            return result;
        }
    }
}


/*
order_id                   order_type                d_DateCreated           d_DateLastChanged       svc_trans_id         lineitem_id                      quantity    cy_unit_price         cy_lineitem_total     saved_cc_expiration saved_cc_number                                                                                      saved_cy_total_total  user_id                              user_email_address                                                               user_first_name                                                  user_last_name                                                   cc_auth_date cc_auth_number                                                                                       cc_auth_request_id                                                                                   cc_bill_request_id                                                                                   cc_total              cc_type         billing_currency svc_trans_amount      PaymentMethodId MerchantId
-------------------------- ------------------------- ----------------------- ----------------------- -------------------- -------------------------------- ----------- --------------------- --------------------- ------------------- ---------------------------------------------------------------------------------------------------- --------------------- ------------------------------------ -------------------------------------------------------------------------------- ---------------------------------------------------------------- ---------------------------------------------------------------- ------------ ---------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------- --------------------- --------------- ---------------- --------------------- --------------- -----------
V5UD0P1E491D4L9U59WE6T5LL5 SbuxAuld                  2014-07-24 08:50:07.303 2014-07-24 08:50:07.303 3000001161           1                                1           25.00                 25.00                 012016              806377FC                                                                                             25.00                 79CD6279-643F-4814-9226-B7DAC70D0604 nat@usen.com                                                                     Nata                                                             K                                                                NULL         NULL                                                                                                 NULL                                                                                                 NULL                                                                                                 25.00                 Paid MasterCard USD              NULL                  304402          
C65CDB7EM0Q05R9LQ3XC4F7GV1 SbuxReld                  2014-07-24 09:23:23.803 2014-07-24 09:23:23.803 3000001248           1                                1           11.00                 11.00                 012016              856672F9                                                                                             11.00                 B8C9607A-AC08-45C8-A8CD-A896EE043D30 sbuxuserus@gmail.com                                                             Automation                                                       TesterUS                                                         NULL         NULL                                                                                                 NULL                                                                                                 NULL                                                                                                 11.00                 Paid Visa       USD              NULL                  43215           
CA3DB74P26B5459VV5J56P3XE0 SbuxReld                  2014-07-24 09:23:57.473 2014-07-24 09:23:57.473 3000001268           1                                1           11.00                 11.00                 012016              856672F9                                                                                             11.00                 B8C9607A-AC08-45C8-A8CD-A896EE043D30 sbuxuserus@gmail.com                                                             Automation                                                       TesterUS                                                         NULL         NULL                                                                                                 NULL                                                                                                 NULL                                                                                                 11.00                 Paid Visa       USD              NULL                  43215           
Q8P21S2A30GD5C9X23253Q2GA2 SbuxReld                  2014-07-24 09:29:39.583 2014-07-24 09:29:39.583 3000001352           1                                1           10.00                 10.00                 062016              856672F9                                                                                             10.00                 C96F8FDD-BB9E-4286-931F-EB731739CD6B shivmuk@rediffmail.com                                                           Mukesh                                                           Singh                                                            NULL         NULL                                                                                                 NULL                                                                                                 NULL                                                                                                 10.00                 Paid Visa       USD              NULL                  304152          
U8NFBT31B5B85F3JR5TDA52DB0 SbuxReld                  2014-07-24 09:29:54.310 2014-07-24 09:29:54.310 3000001362           1                                1           10.00                 10.00                 122016              856672F9                                                                                             10.00                 B718D53A-4203-4AF7-98EE-5CE47B62201A SbuxUserCAFR@gmail.com                                                           Automation                                                       TesterCA                                                         NULL         NULL                                                                                                 NULL                                                                                                 NULL                                                                                                 10.00                 Paid Visa       CAD              NULL                  45021           
23F44T71KA485E7JF5T84F13W6 SbuxReld                  2014-07-24 09:30:28.937 2014-07-24 09:30:28.937 3000001371           1                                1           10.00                 10.00                 122016              856672F9                                                                                             10.00                 B718D53A-4203-4AF7-98EE-5CE47B62201A SbuxUserCAFR@gmail.com                                                           Automation                                                       TesterCA                                                         NULL         NULL                                                                                                 NULL                                                                                                 NULL                                                                                                 10.00                 Paid Visa       CAD              NULL                  45021           
G3A87C4S66TD4841F631AN56K5 SbuxReld                  2014-07-24 09:32:15.953 2014-07-24 09:32:15.953 3000001379           1                                1           100.00                100.00                112025              NULL                                                                                                 100.00                8FAC832F-1543-4A81-B01B-E36E521A86BC tipt@data.com                                                                    Tipper                                                           O'Toole                                                          NULL         NULL                                                                                                 NULL                                                                                                 NULL                                                                                                 100.00                Paid Visa       USD              NULL                  304477          
W85EEK69D6534F9JJAEEAM6XM0 SbuxReld                  2014-07-24 09:32:35.530 2014-07-24 09:32:35.530 3000001390           1                                1           100.00                100.00                112025              NULL                                                                                                 100.00                8FAC832F-1543-4A81-B01B-E36E521A86BC tipt@data.com                                                                    Tipper                                                           O'Toole                                                          NULL         NULL                                                                                                 NULL                                                                                                 NULL                                                                                                 100.00                Paid Visa       USD              NULL                  304477          
D2K4CU7UJ5PA5X7JU2B55Q7987 IntlReld                  2014-07-24 09:40:13.100 2014-07-24 09:40:13.100 3000001407           1                                1           10.00                 10.00                 042018              856672F9                                                                                             10.00                 D181BAB5-1CE1-4C66-B1E2-E4071EB36AAE alenatest2@test.com                                                              test                                                             al                                                               NULL         NULL                                                                                                 NULL                                                                                                 NULL                                                                                                 10.00                 Paid Visa       BRL              NULL                  304694          
48353E1CV6QF4J8N11V1E297K6 SbuxReld                  2014-07-24 09:43:56.920 2014-07-24 09:43:56.920 3000001421           1                                1           25.00                 25.00                 122018              NULL                                                                                                 25.00                 99F7BE56-4BFD-427C-BBFA-014852837446 us08271@test.com                                                                 Us                                                               Test                                                             NULL         NULL                                                                                                 NULL                                                                                                 NULL                                                                                                 25.00                 Paid MasterCard USD              NULL                  262776          

*/
