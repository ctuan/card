﻿using System;
using System.Configuration;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Starbucks.Address.Dal.Common;
using Starbucks.Address.Dal.Common.Models;
using Starbucks.Address.Dal.Sql;
using Starbucks.Address.Dal.Sql.Configuration;
using Starbucks.BadWords.Provider.Common;
using Starbucks.FraudRecovery.Dal;
using Starbucks.PaymentMethod.Provider;
using Starbucks.PaymentMethod.Provider.SqlHybrid;
using Starbucks.PaymentMethod.Provider.SqlHybrid.Configuration;
using Starbucks.Platform.Security;

namespace Starbucks.FraudRecovery.Tests
{
	/// <summary>
	/// Summary description for PaymentMethodTests
	/// </summary>
	[TestClass]
	public class PaymentMethodTests : TestBase
	{
		public PaymentMethodTests()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get { return testContextInstance; }
			set { testContextInstance = value; }
		}

		#region Additional test attributes

		//
		// You can use the following additional attributes as you write your tests:
		//
		// Use ClassInitialize to run code before running the first test in the class
		// [ClassInitialize()]
		// public static void MyClassInitialize(TestContext testContext) { }
		//
		// Use ClassCleanup to run code after all tests in a class have run
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		// Use TestInitialize to run code before running each test 
		// [TestInitialize()]
		// public void MyTestInitialize() { }
		//
		// Use TestCleanup to run code after each test has run
		// [TestCleanup()]
		// public void MyTestCleanup() { }
		//

		#endregion




		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_PaymentMethod_CreateProviderTest()
		{
			var provider = DalObjectFactory.GetPaymentMethodDataProvider();
			Assert.IsTrue(provider != null);
		}

		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
        [Ignore]
		public void FraudRecovery_PaymentMethod_SanityTest()
		{
			var provider = DalObjectFactory.GetPaymentMethodDataProvider();
			Assert.IsTrue(provider != null);

            string paymentId = Encryption.EncryptCardId(TestPaymentMethodRawId);
			var paymentMethod = provider.GetPaymentMethod(paymentId);

			Assert.IsTrue(paymentMethod != null && !string.IsNullOrWhiteSpace(paymentMethod.BillingAddressId));
			Assert.IsTrue(paymentMethod.PaymentTypeId == 1);
		}


		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
        [Ignore]
		public void FraudRecovery_PaymentMethod_GetBillingAddress()
		{
			IAddressDataProvider provider = DalObjectFactory.GetAddressDataProvider();
			Assert.IsTrue(provider != null);

            var address = provider.GetUserAddress(TestUserAccount, TestUserBillingAddressId);
			Assert.IsTrue(address != null && address.AddressType == UserAddressType.Registration);
		}

	}
}
