﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Dal;
using Starbucks.FraudRecovery.Provider;
using Starbucks.FraudRecovery.Provider.Configuration;
using Starbucks.FraudRecovery.Provider.Facades;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.Card.Dal.Sql;
using Starbucks.Card.Dal.Sql.Configuration;
using Starbucks.Platform.Security;
using SettingsProvider = Starbucks.Settings.Provider.SettingsProvider;

namespace Starbucks.FraudRecovery.Tests
{
	[TestClass]
	public class CardDalTests : TestBase
	{
		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_CardDalTest_GetCardById()
		{
			var cardDal = DalObjectFactory.GetCardDal();
			string cardId = Encryption.EncryptCardId(TestCardId[0]);
			var card = cardDal.GetCard(cardId);

			Assert.IsTrue(card != null);
			string cardNumber = Encryption.DecryptCardNumber(card.Number);
			string pin = Encryption.DecryptPin(card.Pin);

			Assert.IsTrue(!string.IsNullOrEmpty(cardNumber));
			Assert.IsTrue(!string.IsNullOrEmpty(pin));
		}


		[TestMethod]
		[TestCategory("FraudRecovery_Integration")]
		public void FraudRecovery_CardDalTest_GetCardById_WithFacade()
		{
			ILoggingUtility logger = new DebugLoggingUtility();
			IFraudRecoveyConfiguration config = ProviderConfigurationSection.GetConfiguration(logger);
			var dal = GetFraudRecoveryDal (config, logger);

			var result = dal.GetSvcCard(TestCardId[1]);

			Assert.IsTrue( result != null );
			Assert.IsTrue(!string.IsNullOrEmpty(result.Market));
			Assert.IsTrue(!string.IsNullOrEmpty(result.Number));
			Assert.IsTrue(!string.IsNullOrEmpty(result.Pin));
		}
	}
}
