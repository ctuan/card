﻿using System;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Models;
using Starbucks.CardIssuer.Dal.SvDot;
using Starbucks.CardIssuer.Dal.SvDot.Configuration;
using Starbucks.CardIssuer.Dal.SvDot.Db;
using Starbucks.CardIssuer.Dal.SvDot.Requestor;
using Starbucks.CardIssuer.Dal.SvDot.TransportModels;
using Starbucks.LogCounter.Provider;
using Starbucks.MessageBroker.Common;
using Starbucks.MessageBroker.Common.Models;

namespace Starbucks.FraudRecovery.Tests
{
	[TestClass]
	public class CardIssuerTests
	{
		public class FraudRecoveryToolTests : TestBase
		{
			[TestMethod]
			[TestCategory("FraudRecovery_Integration")]
			public void FraudRecovery_GetBalance_Test()
			{
				var mockBroker = new Mock<IMessageBroker>();
				IMessageBroker broker = mockBroker.Object;
				var cardIssuer = GetCardIssuer(broker);
				Assert.IsTrue(cardIssuer != null);

				IMerchantInfo merchantInfo = GetMerchantInfo(cardIssuer);
				Assert.IsTrue(merchantInfo != null);

				ICardTransaction transaction = GetBalanceAndVerify(cardIssuer, merchantInfo, TestSvCardNumbers[1], TestSvCardPins[1]);

				mockBroker.Verify(c => c.Publish(It.IsAny<SvcCardNotification>()), Times.Once());
			}

			[TestMethod]
			[TestCategory("FraudRecovery_Integration")]
			public void FraudRecovery_Reload_Test()
			{
				var mockBroker = new Mock<IMessageBroker>();
				IMessageBroker broker = mockBroker.Object;
				var cardIssuer = GetCardIssuer(broker);
				Assert.IsTrue(cardIssuer != null);

				IMerchantInfo merchantInfo = GetMerchantInfo(cardIssuer);
				Assert.IsTrue(merchantInfo != null);
				string cardNumber = TestSvCardNumbers[1];
				string pin = TestSvCardPins[1];
				decimal amount = 10m;

				ICardTransaction transaction = GetBalanceAndVerify(cardIssuer, merchantInfo, cardNumber, pin);
				mockBroker.Verify(c => c.Publish(It.IsAny<SvcCardNotification>()), Times.Once());
				decimal balance = transaction.EndingBalance;

				try
				{
					transaction = ReloadAndVerify(cardIssuer, merchantInfo, cardNumber, pin, amount);
					Assert.IsTrue(transaction.BeginningBalance == balance);
					Assert.IsTrue((transaction.EndingBalance - balance) == amount);
				}
				catch (Exception)
				{
					throw;
				}
				finally
				{
					transaction = RedeemAndVerify(cardIssuer, merchantInfo, cardNumber, pin, amount);
					Assert.IsTrue(transaction.EndingBalance == balance);
				}

			}

			[TestMethod]
			[TestCategory("FraudRecovery_Integration")]
			public void FraudRecovery_BalanceLock_Test()
			{
				var mockBroker = new Mock<IMessageBroker>();
				IMessageBroker broker = mockBroker.Object;
				var cardIssuer = GetCardIssuer(broker);
				Assert.IsTrue(cardIssuer != null);

				IMerchantInfo merchantInfo = GetMerchantInfo(cardIssuer);
				Assert.IsTrue(merchantInfo != null);

				string cardNumber = TestSvCardNumbers[1];
				string pin = TestSvCardPins[1];

				ICardTransaction transaction = BalanceLockAndVerify(cardIssuer, merchantInfo, cardNumber, pin);

				mockBroker.Verify(c => c.Publish(It.IsAny<SvcCardNotification>()), Times.Once());

			}

			#region helper functions
			protected ICardTransaction GetBalanceAndVerify(ICardIssuerDal cardIssuer, IMerchantInfo merchantInfo, string cardNumber, string pin)
			{
				ICardTransaction transaction = cardIssuer.GetBalance(merchantInfo, cardNumber, pin);

				Assert.IsTrue(transaction != null && transaction.TransactionSucceeded);
				Assert.IsTrue(cardNumber == transaction.CardNumber);
				Assert.AreEqual(merchantInfo.AlternateMid, transaction.StoreId);
				Assert.AreEqual(merchantInfo.Mid, transaction.MerchantId);
				Assert.AreEqual(merchantInfo.TerminalId, transaction.TerminalId);
				return transaction;
			}

			protected ICardTransaction ReloadAndVerify(ICardIssuerDal cardIssuer, IMerchantInfo merchantInfo, string cardNumber, string pin, decimal amount)
			{
				ICardTransaction transaction = cardIssuer.Reload(merchantInfo, cardNumber, pin, amount);

				Assert.IsTrue(transaction != null && transaction.TransactionSucceeded);
				Assert.IsTrue(cardNumber == transaction.CardNumber);
				Assert.AreEqual(merchantInfo.AlternateMid, transaction.StoreId);
				Assert.AreEqual(merchantInfo.Mid, transaction.MerchantId);
				Assert.AreEqual(merchantInfo.TerminalId, transaction.TerminalId);

				return transaction;
			}

			protected ICardTransaction RedeemAndVerify(ICardIssuerDal cardIssuer, IMerchantInfo merchantInfo, string cardNumber, string pin, decimal amount)
			{
				ICardTransaction transaction = cardIssuer.Redeem(merchantInfo, cardNumber, pin, amount);

				Assert.IsTrue(transaction != null && transaction.TransactionSucceeded);
				Assert.IsTrue(cardNumber == transaction.CardNumber);
				Assert.AreEqual(merchantInfo.AlternateMid, transaction.StoreId);
				Assert.AreEqual(merchantInfo.Mid, transaction.MerchantId);
				Assert.AreEqual(merchantInfo.TerminalId, transaction.TerminalId);

				return transaction;
			}

			protected ICardTransaction BalanceLockAndVerify(ICardIssuerDal cardIssuer, IMerchantInfo merchantInfo, string cardNumber, string pin)
			{
				ICardTransaction transaction = cardIssuer.BalanceLock(merchantInfo, cardNumber, pin);

				Assert.IsTrue(transaction != null && transaction.TransactionSucceeded);
				Assert.IsTrue(cardNumber == transaction.CardNumber);
				Assert.AreEqual(merchantInfo.AlternateMid, transaction.StoreId);
				Assert.AreEqual(merchantInfo.Mid, transaction.MerchantId);
				Assert.AreEqual(merchantInfo.TerminalId, transaction.TerminalId);

				return transaction;
			}

			#endregion
		}

	}
}
