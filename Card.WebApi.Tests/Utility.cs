﻿//using System;
//using System.Net.Http;
//using System.Reflection;
//using System.Web.Http;
//using System.Web.Http.Hosting;
//using Account.Provider.Common;
//using Autofac;
//using Autofac.Integration.WebApi;
//using Card.WebApi.Controllers;
//using Card.WebApi.Models;
//using Moq;
//using Starbucks.CardTransaction.Provider.Common;
//using Starbucks.PaymentMethod.Provider.Common;
//using Starbucks.Card.Provider.Common;

//namespace Card.WebApi.Tests
//{
//    public class Utility
//    {
//        public static Models.PaymentMethod GetPaymentMethod()
//        {
//            return new Models.PaymentMethod()
//                {
//                    AccountNumber = "123",
//                    AccountNumberLastFour = "1234",
//                    BillingAddressId = "1",
//                    Cvn = "123",
//                    ExpirationMonth = 9,
//                    ExpirationYear = 15,
//                    FullName = "bob",
//                    IsDefault = true,
//                    Nickname = "card",
//                    PaymentMethodId = "2",
//                    PaymentType = "CC"

//                };
//        }

//        public static CardController GetCardController<T, U, V>()
//            where T : ICardProvider
//            where U : IPaymentMethodProvider
//            where V : IAccountProvider
//        {
//            var cardProvider = Activator.CreateInstance<T>();
//            var paymentProvider = Activator.CreateInstance<U>();
//            var accountProvider = Activator.CreateInstance<V>();
//            var controller = new CardController(new Mock<ICardTransactionProvider>().Object)
//            {
//                Request = new HttpRequestMessage()
//            };
//            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
//            return controller;
//        }

//        public static Card.WebApi.Models.AutoReloadProfile GetAutoReloadProfile()
//        {
//            return new AutoReloadProfile()
//                {
//                    Amount = 50,
//                    AutoReloadId = null,
//                    AutoReloadType = "Amount",
//                    // BillingAgreementId = null,
//                    CardId = null,
//                    Day = null,
//                    DisableUntilDate = null,
//                    PaymentMethodId = null,
//                    //  PaymentType = null,
//                    Status = "active",
//                    StoppedDate = null,
//                    TriggerAmount = 10,
//                    UserId = null
//                };
//        }

//        public static void RegisterDependencies<T, U>(HttpConfiguration httpConfiguration)
//            where T : ICardProvider, new()
//            where U : IPaymentMethodProvider, new()
//        {

//            var builder = new ContainerBuilder();

//            builder.RegisterType<T>().As<ICardProvider>();
//            builder.Register(c => new T()).As<ICardProvider>();

//            builder.RegisterType<U>().As<IPaymentMethodProvider>();
//            builder.Register(c => new U()).As<IPaymentMethodProvider>();

//            var t =
//                Type.GetType(
//                    "Card.WebApi.Controllers.CardController, Card.WebApi");
//            if (t != null)
//                builder.RegisterApiControllers(Assembly.GetAssembly(t));

//            var container = builder.Build();
//            var resolver = new AutofacWebApiDependencyResolver(container);

//            httpConfiguration.DependencyResolver = resolver;
//        }

//        public const string BaseUrl = "http://localhost:31024";
//    }
//}
