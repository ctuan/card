﻿using System.Web.Http.Controllers;

using Card.WebApi.Filters;
using Card.WebApi.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Card.WebApi.Tests.Filters
{
    [TestClass]
    public class RegisterCardFilterTest
    {
        private StarbucksCardNumberAndPin GetStarbucksCardNumberAndPin()
        {
            return new StarbucksCardNumberAndPin() {Pin = "12345678", CardNumber = "0123456789101112"};
        }

        [TestMethod]
        public void RegisterCardFilterTestValid()
        {
            var rf = new RegisterCardFilter();
            var context = new HttpActionContext();
            context.ActionArguments.Add("userId", "1234567891011121");
            var cnap = GetStarbucksCardNumberAndPin();
            context.ActionArguments.Add("starbucksCardNumberAndPin", cnap);
            rf.OnActionExecuting(context);
        }

      
    }
}
