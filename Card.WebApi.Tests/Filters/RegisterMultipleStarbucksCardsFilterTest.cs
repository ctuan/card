﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using Card.WebApi.Filters;
using Card.WebApi.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;


namespace Card.WebApi.Tests.Filters
{
    [TestClass]
    public class RegisterMultipleStarbucksCardsFilterTest
    {
        [TestMethod]
        public void RegisterMultipleStarbucksCardsFilterValid()
        {
            var rf = new RegisterMultipleStarbucksCardsFilter();
            var context = new HttpActionContext();
            context.ActionArguments.Add("userId", "1234567891011121");
            var cards = new List<StarbucksCardNumberAndPin>();
            var card = new StarbucksCardNumberAndPin() {CardNumber = "1234567891011121", Pin = "12345678"};
            var card2 = new StarbucksCardNumberAndPin() {CardNumber = "1234567891011122", Pin = "12345678"};
            cards.Add(card);
            cards.Add(card2);
            context.ActionArguments.Add("starbucksCardNumberAndPins", cards);
            rf.OnActionExecuting(context);
        }

        [TestMethod]
        public void RegisterMultipleStarbucksCardsCardsMissing()
        {
            ApiException apiException = null;
            try
            {
                var rf = new RegisterMultipleStarbucksCardsFilter();
                var context = new HttpActionContext();
                context.ActionArguments.Add("userId", "1234567891011121");
                //var cards = new List<StarbucksCardNumberAndPin>();
                //var card = new StarbucksCardNumberAndPin() { Pin = "12345678" };
                //var card2 = new StarbucksCardNumberAndPin() { CardNumber = "1234567891011122", Pin = "12345678" };
                //cards.Add(card);
                //cards.Add(card2);
                //context.ActionArguments.Add("cards", cards);
                rf.OnActionExecuting(context);
            }
            catch (ApiException ex)
            {
                apiException = ex;
            }
            Assert.IsNotNull(apiException);
            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationNoRequestCode );
        }      
    }
}
