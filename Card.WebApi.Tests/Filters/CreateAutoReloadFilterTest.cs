﻿using System;
using System.Net;
using System.Web.Http.Controllers;
using Card.WebApi.Filters;
using Card.WebApi.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;


namespace Card.WebApi.Tests.Filters
{
    [TestClass]
    public class CreateAutoReloadFilterTest
    {
       [TestMethod]
        public void CreateAutoReloadFilterTestMissingRequest()
        {
            ApiException apiException = null;
            try
            {
                var rf = new CreateAutoReloadFilter();
                var context = new HttpActionContext();
                context.ActionArguments.Add("userId", "123");
                context.ActionArguments.Add("cardId", "123");
                
                //context.ActionArguments.Add("autoReloadProfile", null);
                rf.OnActionExecuting(context);
            }
            catch (ApiException ex)
            {
                apiException = ex;
            }
            Assert.IsNotNull(apiException);
            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationNoRequestCode );
            Assert.AreEqual(apiException.StatusCode, HttpStatusCode.BadRequest);
        }


      
    }
}
