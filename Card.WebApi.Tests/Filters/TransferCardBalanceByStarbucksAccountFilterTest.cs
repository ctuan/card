﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using Card.WebApi.Filters;
using Card.WebApi.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;


namespace Card.WebApi.Tests.Filters
{
    [TestClass]
    public class TransferCardBalanceByStarbucksAccountFilterTest
    {
        [TestMethod]
        public void TransferCardBalanceByStarbucksAccountFilterValid()
        {
            var rf = new TransferCardBalanceByStarbucksAccountFilter();
            var context = new HttpActionContext();
            context.ActionArguments.Add("userId", "1234567891011121");
            var balanceTransfer = new BalanceTransfer
                {
                    Amount = 100,
                    TargetCardId = "1234567891011121",
                    SourceCardId = "1234567891011122"
                };

            context.ActionArguments.Add("balanceTransfer", balanceTransfer);
            rf.OnActionExecuting(context);
        }      

        [TestMethod]
        public void TransferCardBalanceByStarbucksAccountFilterMissingBalanceTransfer()
        {
            ApiException apiException = null;
            try
            {
                var rf = new TransferCardBalanceByStarbucksAccountFilter();
                var context = new HttpActionContext();
                context.ActionArguments.Add("userId", "1234567891011121");               
                rf.OnActionExecuting(context);
            }
            catch (ApiException ex)
            {
                apiException = ex;
            }
            Assert.IsNotNull(apiException);
            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationNoRequestCode );
        }

    }
}
