﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using Card.WebApi.Filters;
using Card.WebApi.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;

namespace Card.WebApi.Tests.Filters
{
    [TestClass]
    public class UpdateAutoReloadFilterTest
    {
        [TestMethod]
        public void UpdateAutoReloadFilterValid()
        {
            var rf = new UpdateAutoReloadFilter();
            var context = new HttpActionContext();
            context.ActionArguments.Add("userId", "1234567891011121");
            context.ActionArguments.Add("cardId", "1234567891011121");
            var autoReloadProfile = new AutoReloadProfile
                {
                    Amount = 100,
                    AutoReloadType = "Amount",
                    TriggerAmount = 100,
                    PaymentMethodId = "1"
                };

            context.ActionArguments.Add("autoReloadProfile", autoReloadProfile);
            rf.OnActionExecuting(context);
        }

        [TestMethod]
        public void UpdateAutoReloadFilterTestMissingRequest()
        {
            ApiException apiException = null;
            try
            {
                var rf = new UpdateAutoReloadFilter();
                var context = new HttpActionContext();
                context.ActionArguments.Add("userId", "123");
                context.ActionArguments.Add("cardId", "123");

                //context.ActionArguments.Add("autoReloadProfile", null);
                rf.OnActionExecuting(context);
            }
            catch (ApiException ex)
            {
                apiException = ex;
            }
            Assert.IsNotNull(apiException);
            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationNoRequestCode);
            Assert.AreEqual(apiException.StatusCode, HttpStatusCode.BadRequest);
        }     
    }
}
