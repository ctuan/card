﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using Card.WebApi.Filters;
using Card.WebApi.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;

namespace Card.WebApi.Tests.Filters
{
    [TestClass]
    public class ReloadCardByCardNumberPinFilterTest
    {
        [TestMethod]
        public void ReloadCardByCardNumberPinFilterValid()
        {
            var rf = new ReloadCardByCardNumberPinFilter();
            var context = new HttpActionContext();
            context.ActionArguments.Add("cardNumber", "1234567891011121");
            context.ActionArguments.Add("pin", "12345678");

            var reload = new ReloadForStarbucksCard() {Amount = 100, PaymentMethodId = "1"};
            context.ActionArguments.Add("reloadForStarbucksCard", reload);
            rf.OnActionExecuting(context);
        }

        [TestMethod]
        public void ReloadCardByCardNumberPinFilterMissingReload()
        {
            ApiException apiException = null;
            try
            {
                var rf = new ReloadCardByCardNumberPinFilter();
                var context = new HttpActionContext();
                context.ActionArguments.Add("cardNumber", "1234567891011121");
                context.ActionArguments.Add("pin", "12345678");

                
                rf.OnActionExecuting(context);
            }
            catch (ApiException ex)
            {
                apiException = ex;
            }
            Assert.IsNotNull(apiException);
            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationNoRequestCode );
            Assert.AreEqual(apiException.StatusCode, HttpStatusCode.BadRequest);
        }      
    }
}
