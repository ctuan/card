﻿using Account.Provider.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Card.WebApi.Tests
{
    public class FakeUser: IUser
    {
        public string UserId { get; set; }
        public string ExternalUserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public bool IsPartner { get; set; }
        public bool IsEligibleForDiscount { get; set; }
        public IBirthDate BirthDate { get; set; }
        public string SubMarket { get; set; }
        public DateTime? DateCreated { get; set; }
        public string PartnerNumber { get; set; }
        public string LoyaltyMembershipId { get; set; }
    }
}
