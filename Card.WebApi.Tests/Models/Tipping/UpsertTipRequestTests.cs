﻿using Card.WebApi.Models.Tipping;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Card.WebApi.Tests.Models.Tipping
{
    [TestClass]
    public class UpsertTipRequestTests
    {
        #region Amount Tests

        [TestMethod]
        public void Amount_GivenValueThatNeedsToBeRoundedDown_IsRoundedDown()
        {
            var upsertTipRequest = new UpsertTipRequest
                {
                    StringAmount = "1.234",
                };

            const decimal expectedAmount = 1.23m;
            decimal actualAmount = upsertTipRequest.Amount;

            Assert.AreEqual(expectedAmount, actualAmount);
        }

        [TestMethod]
        public void Amount_GivenValueThatNeedsToBeRoundedUp_IsRoundedUp()
        {
            var upsertTipRequest = new UpsertTipRequest
            {
                StringAmount = "1.236",
            };

            const decimal expectedAmount = 1.24m;
            decimal actualAmount = upsertTipRequest.Amount;

            Assert.AreEqual(expectedAmount, actualAmount);
        }

        [TestMethod]
        public void Amount_GivenValueThatEndsWithFive_IsRoundedUp()
        {
            var upsertTipRequest = new UpsertTipRequest
            {
                StringAmount = "1.235",
            };

            const decimal expectedAmount = 1.24m;
            decimal actualAmount = upsertTipRequest.Amount;

            Assert.AreEqual(expectedAmount, actualAmount);
        }

        [TestMethod]
        public void Amount_GivenValueDoesNotNeedRounding_IsNotRounded()
        {
            var upsertTipRequest = new UpsertTipRequest
            {
                StringAmount = "1",
            };

            const decimal expectedAmount = 1m;
            decimal actualAmount = upsertTipRequest.Amount;

            Assert.AreEqual(expectedAmount, actualAmount);
        }

        [TestMethod]
        public void TryParseAmount_Exponent_ReturnsFalse()
        {
            const string input = "2E4";
            const bool expectedResult = false;

            decimal amount;
            var upsertTipRequest = new UpsertTipRequest();
            bool actualResult = upsertTipRequest.TryParseAmount(input, out amount);

            Assert.AreEqual(expectedResult, actualResult);
        }

        #endregion

        #region TryParseAmount

        [TestMethod]
        public void TryParseAmount_Hexidecimal_ReturnsFalse()
        {
            const string input = "0x123";
            const bool expectedResult = false;

            decimal amount;
            var upsertTipRequest = new UpsertTipRequest();
            bool actualResult = upsertTipRequest.TryParseAmount(input, out amount);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TryParseAmount_IncludesCurrencySymbol_ReturnsFalse()
        {
            const string input = "$1.23";
            const bool expectedResult = false;

            decimal amount;
            var upsertTipRequest = new UpsertTipRequest();
            bool actualResult = upsertTipRequest.TryParseAmount(input, out amount);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TryParseAmount_IncludesDecimal_ReturnsTrue()
        {
            const string input = "1.23";
            const bool expectedResult = true;

            decimal amount;
            var upsertTipRequest = new UpsertTipRequest();
            bool actualResult = upsertTipRequest.TryParseAmount(input, out amount);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TryParseAmount_IncludesDecimal_AmountIsExpected()
        {
            const string input = "1.23";
            const decimal expectedAmount = 1.23m;

            decimal actualAmount;
            var upsertTipRequest = new UpsertTipRequest();
            upsertTipRequest.TryParseAmount(input, out actualAmount);

            Assert.AreEqual(expectedAmount, actualAmount);
        }

        [TestMethod]
        public void TryParseAmount_IncludesLeadingSign_ReturnsTrue()
        {
            const string input = "-1.23";
            const bool expectedResult = true;

            decimal amount;
            var upsertTipRequest = new UpsertTipRequest();
            bool actualResult = upsertTipRequest.TryParseAmount(input, out amount);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TryParseAmount_IncludesLeadingSign_AmountIsExpected()
        {
            const string input = "-1.23";
            const decimal expectedAmount = -1.23m;

            decimal actualAmount;
            var upsertTipRequest = new UpsertTipRequest();
            upsertTipRequest.TryParseAmount(input, out actualAmount);

            Assert.AreEqual(expectedAmount, actualAmount);
        }

        [TestMethod]
        public void TryParseAmount_IncludesLeadingWhitespace_ReturnsTrue()
        {
            const string input = "\r  1.23";
            const bool expectedResult = true;

            decimal amount;
            var upsertTipRequest = new UpsertTipRequest();
            bool actualResult = upsertTipRequest.TryParseAmount(input, out amount);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TryParseAmount_IncludesLeadingWhitespace_AmountIsExpected()
        {
            const string input = "\r  1.23";
            const decimal expectedAmount = 1.23m;

            decimal actualAmount;
            var upsertTipRequest = new UpsertTipRequest();
            upsertTipRequest.TryParseAmount(input, out actualAmount);

            Assert.AreEqual(expectedAmount, actualAmount);
        }

        [TestMethod]
        public void TryParseAmount_IncludesNumberGroupSeparator_ReturnsFalse()
        {
            const string input = "1,23";
            const bool expectedResult = false;

            decimal amount;
            var upsertTipRequest = new UpsertTipRequest();
            bool actualResult = upsertTipRequest.TryParseAmount(input, out amount);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TryParseAmount_IncludesParenthases_ReturnsTrue()
        {
            const string input = "(1.23)";
            const bool expectedResult = true;

            decimal amount;
            var upsertTipRequest = new UpsertTipRequest();
            bool actualResult = upsertTipRequest.TryParseAmount(input, out amount);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TryParseAmount_IncludesParanthases_AmountIsExpected()
        {
            const string input = "(1.23)";
            const decimal expectedAmount = -1.23m;

            decimal actualAmount;
            var upsertTipRequest = new UpsertTipRequest();
            upsertTipRequest.TryParseAmount(input, out actualAmount);

            Assert.AreEqual(expectedAmount, actualAmount);
        }

        [TestMethod]
        public void TryParseAmount_IncludesTrailingSign_ReturnsTrue()
        {
            const string input = "1.23-";
            const bool expectedResult = true;

            decimal amount;
            var upsertTipRequest = new UpsertTipRequest();
            bool actualResult = upsertTipRequest.TryParseAmount(input, out amount);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TryParseAmount_IncludesTrailingSign_AmountIsExpected()
        {
            const string input = "1.23-";
            const decimal expectedAmount = -1.23m;

            decimal actualAmount;
            var upsertTipRequest = new UpsertTipRequest();
            upsertTipRequest.TryParseAmount(input, out actualAmount);

            Assert.AreEqual(expectedAmount, actualAmount);
        }

        [TestMethod]
        public void TryParseAmount_IncludesTrailingWhitespace_ReturnsTrue()
        {
            const string input = "1.23 \t";
            const bool expectedResult = true;

            decimal amount;
            var upsertTipRequest = new UpsertTipRequest();
            bool actualResult = upsertTipRequest.TryParseAmount(input, out amount);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TryParseAmount_IncludesTrailingWhitespace_AmountIsExpected()
        {
            const string input = "1.23 \t";
            const decimal expectedAmount = 1.23m;

            decimal actualAmount;
            var upsertTipRequest = new UpsertTipRequest();
            upsertTipRequest.TryParseAmount(input, out actualAmount);

            Assert.AreEqual(expectedAmount, actualAmount);
        }

        [TestMethod]
        public void TryParseAmount_NonNumericCharacters_ReturnsFalse()
        {
            const string input = "abcdefg";
            const bool expectedResult = false;

            decimal amount;
            var upsertTipRequest = new UpsertTipRequest();
            bool actualResult = upsertTipRequest.TryParseAmount(input, out amount);

            Assert.AreEqual(expectedResult, actualResult);
        }

        #endregion
    }
}
