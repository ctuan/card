﻿using System;
using Account.Provider.Common.Models;

namespace Card.WebApi.Tests
{
    public class FakeFavoriteStore : IFavoriteStore
    {
        public string UserId { get; set; }
        public int StoreId { get; set; }
        public string Nickname { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}
