﻿using System.Collections.Generic;
using System.Web.Http.Controllers;
using Card.WebApi.Authorization;
using Card.WebApi.Tests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Card.WebApi.Tests.Authorization
{
    [TestClass]
    public class SecureHashAuthorizationProviderTests
    {
        private static SecureHashAuthorizationProvider SetupSecureHashAuthorizationProvider(string salt, IEnumerable<string> setupAuthorizations)
        {
            var mockSecureHashAuthorizationProviderSettings = new Mock<ISecureHashAuthorizationProviderSettings>();

            mockSecureHashAuthorizationProviderSettings
                .SetupGet(s => s.Salt)
                .Returns(salt);

            var mockAuthorizationDataProvider = new Mock<IAuthorizationDataProvider>();

            mockAuthorizationDataProvider
                .Setup(p => p.GetAuthorizations(It.IsAny<HttpActionContext>()))
                .Returns(setupAuthorizations);

            return new SecureHashAuthorizationProvider(mockSecureHashAuthorizationProviderSettings.Object, mockAuthorizationDataProvider.Object);
        }

        [TestMethod]
        public void VerifyTransformAuthorizeePositive()
        {
            var salt = SecureHashAuthorizationProvider.GenerateRandomSalt();
            var authorizedUser = "someUser1";
            var authorizedUserHash = SecureHashAuthorizationProvider.GenerateSaltedHash(authorizedUser, salt);

            var secureHashAuthorizationProvider = SetupSecureHashAuthorizationProvider(salt, new[] { authorizedUserHash });

            var actionContext = ContextUtil.CreateActionContext();

            var isAuthorized = secureHashAuthorizationProvider.IsAuthorized(actionContext, authorizedUser);

            Assert.IsTrue(isAuthorized, "Expected IsAuthorized to return true");
        }

        [TestMethod]
        public void VerifyTransformAuthorizeePreHashedValueNotAuthorized()
        {
            var salt = SecureHashAuthorizationProvider.GenerateRandomSalt();
            var authorizedUser = "someUser1";
            var authorizedUserHash = SecureHashAuthorizationProvider.GenerateSaltedHash(authorizedUser, salt);

            var secureHashAuthorizationProvider = SetupSecureHashAuthorizationProvider(salt, new[] { authorizedUserHash });

            var actionContext = ContextUtil.CreateActionContext();

            var isAuthorized = secureHashAuthorizationProvider.IsAuthorized(actionContext, authorizedUserHash);

            Assert.IsFalse(isAuthorized, "Expected IsAuthorized to return false");
        }
    }
}
