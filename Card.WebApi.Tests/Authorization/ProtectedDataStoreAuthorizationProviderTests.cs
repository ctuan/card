﻿using System.Collections.Generic;
using System.Web.Http.Controllers;
using Card.WebApi.Authorization;
using Card.WebApi.Tests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Card.WebApi.Tests.Authorization
{
    [TestClass]
    public class ProtectedDataStoreAuthorizationProviderTests
    {
        private static ProtectedDataStoreAuthorizationProvider SetupProtectedDataStoreAuthorizationProvider(IEnumerable<string> setupAuthorizations)
        {
            var mockAuthorizationDataProvider = new Mock<IAuthorizationDataProvider>();

            mockAuthorizationDataProvider
                .Setup(p => p.GetAuthorizations(It.IsAny<HttpActionContext>()))
                .Returns(setupAuthorizations);

            var mockProtectedDataStore = new Mock<IProtectedDataStore>();

            mockProtectedDataStore
                .Setup(pds => pds.GetUnprotectedDataAsString(It.IsAny<string>()))
                .Returns((string authorization) => UnprotectString(authorization));

            return new ProtectedDataStoreAuthorizationProvider(mockAuthorizationDataProvider.Object, mockProtectedDataStore.Object);
        }

        private static string UnprotectString(string value)
        {
            return string.Format("{0}-unprotected", value);
        }

        [TestMethod]
        public void VerifyTransformAuthorizationPositive()
        {
            var authorization = "someKey1";

            var setupAuthorizations = new[] { authorization };

            var protectedDataStoreAuthorizationProvider = SetupProtectedDataStoreAuthorizationProvider(setupAuthorizations);

            var actionContext = ContextUtil.CreateActionContext();

            var isAuthorized = protectedDataStoreAuthorizationProvider.IsAuthorized(actionContext, UnprotectString(authorization));

            Assert.IsTrue(isAuthorized, "Expected IsAuthorized to return true");
        }

        [TestMethod]
        public void VerifyTransformAuthorizationKeyValueNotAuthorized()
        {
            var authorization = "someKey1";

            var setupAuthorizations = new[] { authorization };

            var protectedDataStoreAuthorizationProvider = SetupProtectedDataStoreAuthorizationProvider(setupAuthorizations);

            var actionContext = ContextUtil.CreateActionContext();

            var isAuthorized = protectedDataStoreAuthorizationProvider.IsAuthorized(actionContext, authorization);

            Assert.IsFalse(isAuthorized, "Expected IsAuthorized to return false");
        }
    }
}
