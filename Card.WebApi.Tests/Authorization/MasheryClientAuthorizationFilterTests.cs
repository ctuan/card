﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using Card.WebApi.Authorization;
using Card.WebApi.Tests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Card.WebApi.Tests.Authorization
{
    [TestClass]
    public class MasheryClientAuthorizationFilterTests
    {
        private static void VerifyInResponseBody(string responseBody, string expectedInResponseBody)
        {
            Assert.IsTrue(responseBody != null && responseBody.IndexOf(expectedInResponseBody, StringComparison.OrdinalIgnoreCase) >= 0, string.Format("Expected response body to contain '{0}'", expectedInResponseBody));
        }

        private static MasheryClientAuthorizationFilter SetupMasheryClientAuthorizationFilter(string authorizedValue)
        {
            var mockAuthorizationProvider = new Mock<IAuthorizationProvider>();

            mockAuthorizationProvider
                .Setup(ap => ap.IsAuthorized(It.IsAny<HttpActionContext>(), authorizedValue))
                .Returns(true);

            return new MasheryClientAuthorizationFilter(mockAuthorizationProvider.Object);
        }

        private static void VerifyNullResponse(HttpResponseMessage response)
        {
            Assert.IsNull(response, "Expected HttpResponseMessage to be null");
        }

        private static void VerifyUnauthorizedResponse(HttpResponseMessage response)
        {
            Assert.IsNotNull(response, "Expected HttpResponseMessage to be non-null");

            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);

            Assert.IsNotNull(response.Content, "Expected HttpResponseMessage.Content to be non-null");

            var responseBody = response.Content.ReadAsStringAsync().Result;

            VerifyInResponseBody(responseBody, CardWebAPIErrorResource.UnauthorizedMasheryClientErrorCode);
            VerifyInResponseBody(responseBody, CardWebAPIErrorResource.UnauthorizedMasheryClientErrorMessage);
        }

        [TestMethod]
        public void AuthorizedMasheryClientSuccess()
        {
            const string AuthorizedMasheryClientToVerify = "realMasheryClient1";

            var masheryClientAuthorizationFilter = SetupMasheryClientAuthorizationFilter(AuthorizedMasheryClientToVerify);

            var request = new HttpRequestMessage();
            request.Headers.Add("X-Mashery-Oauth-Client-Id", AuthorizedMasheryClientToVerify);

            var actionContext = ContextUtil.CreateActionContext(null, null, null, null, request);

            masheryClientAuthorizationFilter.OnAuthorization(actionContext);

            VerifyNullResponse(actionContext.Response);
        }

        [TestMethod]
        public void UnauthorizedMasheryClientUnauthorizedResponse()
        {
            const string UnauthorizedMasheryClient = "realMasheryClient1";

            var masheryClientAuthorizationFilter = SetupMasheryClientAuthorizationFilter("someOtherMasheryClient");

            var request = new HttpRequestMessage();
            request.Headers.Add("X-Mashery-Oauth-Client-Id", UnauthorizedMasheryClient);

            var actionContext = ContextUtil.CreateActionContext(null, null, null, null, request);

            masheryClientAuthorizationFilter.OnAuthorization(actionContext);

            VerifyUnauthorizedResponse(actionContext.Response);
        }

        [TestMethod]
        public void MissingMasheryClientHeaderUnauthorizedResponse()
        {
            var masheryClientAuthorizationFilter = SetupMasheryClientAuthorizationFilter("masheryClientThatDoesNotMatter");

            var actionContext = ContextUtil.CreateActionContext();

            masheryClientAuthorizationFilter.OnAuthorization(actionContext);

            VerifyUnauthorizedResponse(actionContext.Response);
        }
    }
}
