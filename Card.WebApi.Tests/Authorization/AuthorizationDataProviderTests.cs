﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Card.WebApi.Authorization;
using Card.WebApi.Configuration;
using Card.WebApi.Tests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using AuthorizationClass = Card.WebApi.Configuration.Authorization;

namespace Card.WebApi.Tests.Authorization
{
    [TestClass]
    public class AuthorizationDataProviderTests
    {
        private static AuthorizationDataProvider SetupAuthorizationDataProvider(IEnumerable<IAuthorization> setupAuthorizations)
        {
            var mockAuthorizationData = new Mock<IAuthorizationData>();

            mockAuthorizationData
                .SetupGet(ad => ad.Authorizations)
                .Returns(setupAuthorizations);

            return new AuthorizationDataProvider(mockAuthorizationData.Object);
        }

        private static void VerifyAuthorizations(IEnumerable<string> expectedAuthorizations, IEnumerable<string> actualAuthorizations)
        {
            Assert.IsNotNull(actualAuthorizations, "Expected 'actualAuthorizations' to be non-null");

            Assert.AreEqual(expectedAuthorizations.Count(), actualAuthorizations.Count());

            foreach (var expectedAuthorization in expectedAuthorizations)
            {
                var foundAuthorization = actualAuthorizations.FirstOrDefault(u => u == expectedAuthorization);

                Assert.IsNotNull(foundAuthorization, string.Format("Authorization '{0}' expected but not found", expectedAuthorization));
            }
        }

        [TestMethod]
        public void AllAuthorizationsMatch()
        {
            var setupAuthorizations = new[]
            {
                new AuthorizationClass { Value = "user1" },
                new AuthorizationClass { Value = "user2" },
                new AuthorizationClass { Value = "user3" }
            };

            var expectedAuthorizations = setupAuthorizations
                .Select(a => a.Value)
                .ToList();

            var authorizationDataProvider = SetupAuthorizationDataProvider(setupAuthorizations);

            var actionContext = ContextUtil.CreateActionContext();

            var actualAuthorizations = authorizationDataProvider.GetAuthorizations(actionContext);

            VerifyAuthorizations(expectedAuthorizations, actualAuthorizations);
        }

        [TestMethod]
        public void ControllerAuthorizationsFiltered()
        {
            const string ControllerNameToVerify = "RealController";

            var setupAuthorizations = new[]
            {
                new AuthorizationClass { Controller = "DummyController", Value = "user1" },
                new AuthorizationClass { Controller = ControllerNameToVerify, Value = "user2" },
                new AuthorizationClass { Controller = ControllerNameToVerify, Value = "user3" }
            };

            var expectedAuthorizations = setupAuthorizations
                .Where(a => a.Controller == ControllerNameToVerify)
                .Select(a => a.Value)
                .ToList();

            var authorizationDataProvider = SetupAuthorizationDataProvider(setupAuthorizations);

            var actionContext = ContextUtil.CreateActionContext(null, ControllerNameToVerify);

            var actualAuthorizations = authorizationDataProvider.GetAuthorizations(actionContext);

            VerifyAuthorizations(expectedAuthorizations, actualAuthorizations);
        }

        [TestMethod]
        public void ActionAuthorizationsFiltered()
        {
            const string ControllerName = "AnyController";
            const string ActionNameToVerify = "RealAction";

            var setupAuthorizations = new[]
            {
                new AuthorizationClass { Controller = ControllerName, Action = "DummyAction", Value = "user1" },
                new AuthorizationClass { Controller = ControllerName, Action = ActionNameToVerify, Value = "user2" },
                new AuthorizationClass { Controller = ControllerName, Action = ActionNameToVerify, Value = "user3" }
            };

            var expectedAuthorizations = setupAuthorizations
                .Where(a => a.Controller == ControllerName && a.Action == ActionNameToVerify)
                .Select(a => a.Value)
                .ToList();

            var authorizationDataProvider = SetupAuthorizationDataProvider(setupAuthorizations);

            var actionContext = ContextUtil.CreateActionContext(null, ControllerName, ActionNameToVerify);

            var actualAuthorizations = authorizationDataProvider.GetAuthorizations(actionContext);

            VerifyAuthorizations(expectedAuthorizations, actualAuthorizations);
        }

        [TestMethod]
        public void RequestMethodAuthorizationsFiltered()
        {
            const string ControllerName = "AnyController";
            const string ActionName = "AnyAction";
            var RequestMethodToVerify = HttpMethod.Get;

            var setupAuthorizations = new[]
            {
                new AuthorizationClass { Controller = ControllerName, Action = ActionName, RequestMethod = "forGETmeNot", Value = "user1" },
                new AuthorizationClass { Controller = ControllerName, Action = ActionName, RequestMethod = RequestMethodToVerify.ToString(), Value = "user2" },
                new AuthorizationClass { Controller = ControllerName, Action = ActionName, RequestMethod = RequestMethodToVerify.ToString(), Value = "user3" }
            };

            var expectedAuthorizations = setupAuthorizations
                .Where(a => a.Controller == ControllerName && a.Action == ActionName && a.RequestMethod == RequestMethodToVerify.ToString())
                .Select(a => a.Value)
                .ToList();

            var authorizationDataProvider = SetupAuthorizationDataProvider(setupAuthorizations);

            var actionContext = ContextUtil.CreateActionContext(null, ControllerName, ActionName, null, new HttpRequestMessage { Method = RequestMethodToVerify });

            var actualAuthorizations = authorizationDataProvider.GetAuthorizations(actionContext);

            VerifyAuthorizations(expectedAuthorizations, actualAuthorizations);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullActionContextErrorsOut()
        {
            var authorizationDataProvider = SetupAuthorizationDataProvider(Enumerable.Empty<IAuthorization>());

            authorizationDataProvider.GetAuthorizations(null);
        }
    }
}
