﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Controllers;
using Card.WebApi.Authorization;
using Card.WebApi.Tests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Card.WebApi.Tests.Authorization
{
    [TestClass]
    public class AuthorizationProviderTests
    {
        private static AuthorizationProvider SetupAuthorizationProvider(IEnumerable<string> setupAuthorizations)
        {
            var mockAuthorizationDataProvider = new Mock<IAuthorizationDataProvider>();

            mockAuthorizationDataProvider
                .Setup(p => p.GetAuthorizations(It.IsAny<HttpActionContext>()))
                .Returns(setupAuthorizations);

            return new AuthorizationProvider(mockAuthorizationDataProvider.Object);
        }

        [TestMethod]
        public void BasicValueIsAuthorized()
        {
            var authorizedUser = "someUser1";

            var setupAuthorizations = new[] { authorizedUser, "anotherUser1", "anotherUser2" };

            var authorizationProvider = SetupAuthorizationProvider(setupAuthorizations);

            var actionContext = ContextUtil.CreateActionContext();

            var isAuthorized = authorizationProvider.IsAuthorized(actionContext, authorizedUser);

            Assert.IsTrue(isAuthorized, "Expected IsAuthorized to return true");
        }

        [TestMethod]
        public void BasicValueNotAuthorized()
        {
            var setupAuthorizations = new[] { "anotherUser1", "anotherUser2", "anotherUser3" };

            var authorizationProvider = SetupAuthorizationProvider(setupAuthorizations);

            var actionContext = ContextUtil.CreateActionContext();

            var isAuthorized = authorizationProvider.IsAuthorized(actionContext, "someUser1");

            Assert.IsFalse(isAuthorized, "Expected IsAuthorized to return false");
        }

        [TestMethod]
        public void EmptyValueNotAuthorized()
        {
            var setupAuthorizations = new[] { "someUser1" };

            var authorizationProvider = SetupAuthorizationProvider(setupAuthorizations);

            var actionContext = ContextUtil.CreateActionContext();

            var isAuthorized = authorizationProvider.IsAuthorized(actionContext, string.Empty);

            Assert.IsFalse(isAuthorized, "Expected IsAuthorized to return false");
        }

        [TestMethod]
        public void EmptyAuthorizationsNotAuthorized()
        {
            var authorizationProvider = SetupAuthorizationProvider(Enumerable.Empty<string>());

            var actionContext = ContextUtil.CreateActionContext();

            var isAuthorized = authorizationProvider.IsAuthorized(actionContext, "someUser1");

            Assert.IsFalse(isAuthorized, "Expected IsAuthorized to return false");
        }
    }
}
