﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Card.WebApi.Authorization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Card.WebApi.Tests.Authorization
{
    [TestClass]
    public class ProtectedDataStoreTests
    {
        private static ProtectedDataStore SetupProtectedDataStore()
        {
            var mockProtectedDataStoreSettings = new Mock<IProtectedDataStoreSettings>();

            mockProtectedDataStoreSettings
                .SetupGet(s => s.Path)
                .Returns(@".\");

            return new ProtectedDataStore(mockProtectedDataStoreSettings.Object);
        }

        [TestMethod]
        public void CanProtectDataAndLoadFromFileSystem()
        {
            var protectedDataStore = SetupProtectedDataStore();

            var key = "mykey:mykey1-nprod-20161017";
            var unprotectedData = Guid.NewGuid().ToString();

            protectedDataStore.ProtectData(key, unprotectedData);

            var loadedUnprotectedData = protectedDataStore.GetUnprotectedDataAsString(key);

            Assert.AreEqual(unprotectedData, loadedUnprotectedData);
        }

        [TestMethod]
        public void CanProtectDataAndLoadFromFileSystemCompareByteArrays()
        {
            var protectedDataStore = SetupProtectedDataStore();

            var key = "mykey:mykey1-nprod-20161017";
            var unprotectedData = Guid.NewGuid().ToString();
            var unprotectedDataBytes = Encoding.UTF8.GetBytes(unprotectedData);

            protectedDataStore.ProtectData(key, unprotectedData);

            var loadedUnprotectedDataBytes = protectedDataStore.GetUnprotectedData(key);

            Assert.IsTrue(Enumerable.SequenceEqual(unprotectedDataBytes, loadedUnprotectedDataBytes), "Expected byte arrays to be equal");
        }

        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void UnknownKeyErrorsOut()
        {
            var protectedDataStore = SetupProtectedDataStore();

            protectedDataStore.GetUnprotectedData("SomeKeyThatDoesNotExist");
        }

        [TestMethod]
        public void CanProtectMultipleKeys()
        {
            var protectedDataStore = SetupProtectedDataStore();

            var keys = new[]
            {
                new { Key = "somekey:mykey1-nprod-20161017", UnprotectedData = Guid.NewGuid().ToString() },
                new { Key = "somekey:mykey2-nprod-20161017", UnprotectedData = Guid.NewGuid().ToString() },
                new { Key = "somekey:mykey3-nprod-20161017", UnprotectedData = Guid.NewGuid().ToString() }
            };

            foreach (var key in keys)
            {
                protectedDataStore.ProtectData(key.Key, key.UnprotectedData);

                var loadedUnprotectedData = protectedDataStore.GetUnprotectedDataAsString(key.Key);

                Assert.AreEqual(key.UnprotectedData, loadedUnprotectedData);
            }
        }
    }
}
