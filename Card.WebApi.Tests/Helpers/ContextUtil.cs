﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.Routing;
using Moq;

namespace Card.WebApi.Tests.Helpers
{
    public static class ContextUtil
    {
        public static HttpControllerContext CreateControllerContext(IHttpController controller = null, string controllerName = null, IHttpRouteData routeData = null, HttpRequestMessage request = null)
        {
            var config = new HttpConfiguration();
            var route = routeData ?? new HttpRouteData(new HttpRoute());
            var req = request ?? new HttpRequestMessage();

            req.SetConfiguration(config);
            req.SetRouteData(route);

            var context = new HttpControllerContext(config, route, req);

            if (controller != null)
            {
                context.Controller = controller;
            }

            context.ControllerDescriptor = new HttpControllerDescriptor
            {
                Configuration = config,
                ControllerName = controllerName ?? "Foo"
            };

            return context;
        }

        public static HttpActionContext CreateActionContext(IHttpController controller = null, string controllerName = null, string actionName = null, IHttpRouteData routeData = null, HttpRequestMessage request = null)
        {
            var context = CreateControllerContext(controller, controllerName, routeData, request);

            var descriptor = CreateActionDescriptor(actionName);
            descriptor.ControllerDescriptor = context.ControllerDescriptor;

            return new HttpActionContext(context, descriptor);
        }

        public static HttpActionExecutedContext CreateActionExecutedContext(IHttpController controller = null, string controllerName = null, string actionName = null, IHttpRouteData routeData = null, HttpRequestMessage request = null, HttpResponseMessage response = null)
        {
            var actionContext = CreateActionContext(controller, controllerName, actionName, routeData, request);

            return new HttpActionExecutedContext(actionContext, null)
            {
                Response = response
            };
        }

        private static HttpActionDescriptor CreateActionDescriptor(string actionName = null)
        {
            var mock = new Mock<HttpActionDescriptor>
            {
                CallBase = true
            };

            mock.SetupGet(d => d.ActionName)
                .Returns(actionName ?? "Bar");

            return mock.Object;
        }
    }
}
