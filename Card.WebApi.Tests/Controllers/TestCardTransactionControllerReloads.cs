﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using Card.WebApi.Controllers;
using Card.WebApi.Models;
using Starbucks.Card.Dal.Sql.EntLibLess;
using Starbucks.Card.Dal.Sql.EntLibLess.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Starbucks.Card.Provider;
using Starbucks.CardTransaction.Provider.Common;
using Starbucks.CardTransaction.Provider.Common.Model;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;
using Starbucks.Platform.Security;
using Account.Provider.Common;

namespace Card.WebApi.Tests.Controllers
{
    [TestClass]
    public class TestCardTransactionControllerReloads
    {
        private const string UserId = "E6638971-8C22-443C-B6F6-CC272A2A52AE";
        private const string ClientId = "qwerty";
        private static CardTransactionController _controller;
        private static SelectedCardController _selectedCardController;
        private static SelectedCardProvider _selectedCardProvider;
        private static Mock<ICardTransactionProvider> _cardTransactionProvider;
        private static Mock<IAccountProvider> _accountProvider;

        private readonly int[] _testCards = new[] { 1791008, 967 };

        private IEnumerable<string> TestCards
        {
            get { return _testCards.Select(Encryption.EncryptCardId); }
        }

        [TestInitialize]
        public void Initialize()
        {
            BuildMockController();
        }

        [TestCleanup]
        public void Cleanup()
        {
//            _controller.RemoveSelectedCards(UserId, ClientId, new SelectedCardsRequest { Cards = TestCards });
        }

        private static void BuildMockController()
        {
            var settingsSection =
                ConfigurationManager.GetSection("cardDalEntLibLessSettings") as CardDalEntLibLessSqlSettingsSection;
            var selectedCardDal = new SelectedCardDal(settingsSection);
            _selectedCardProvider = new SelectedCardProvider(selectedCardDal);

            _selectedCardController = new SelectedCardController(_selectedCardProvider, selectedCardDal)
            {
                Request = new HttpRequestMessage()
            };

            _cardTransactionProvider = new Mock<ICardTransactionProvider>();
            _accountProvider = new Mock<IAccountProvider>();
            _controller = new CardTransactionController(_cardTransactionProvider.Object, _accountProvider.Object) { Request = new HttpRequestMessage() };

            var config = new HttpConfiguration();
            WebApiConfig.Register(config);
            _controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, config);
        }

        [TestMethod]
        public void TestReloadCardSuccess()
        {
            string userId = "59C216AC-B5C3-4008-B4F7-12CF7968F724";
            string cardId = "8C6772F19ED711AC";

            _selectedCardProvider.AssociateSelectedCardsWithClient(UserId, ClientId, new string[] { cardId });

            var reload = new Card.WebApi.Models.ReloadForStarbucksCard { Amount = 15, PaymentMethodId = "876472FD9BD6", Platform = "Web" };
            reload.RiskFields = new WebApi.Models.Risk();
            reload.RiskFields.Market = "US";
            reload.RiskFields.Platform = "windows";
            reload.RiskFields.IsLoggedIn = true;
            reload.RiskFields.Reputation = new WebApi.Models.Reputation();
            reload.RiskFields.Reputation.IpAddress = "10.10.10.10";
            reload.RiskFields.Reputation.DeviceFingerprint = "asfasdfasdfadsfasdfasdfsdafsadfasdfsdaf";
            var cardTransaction = new Mock<ICardTransaction>();

            _cardTransactionProvider.Setup(pr => pr.ReloadStarbucksCard(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IReloadForStarbucksCard>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(cardTransaction.Object);
            HttpResponseMessage result = _controller.ReloadCardByAccount(userId, cardId, reload);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void TestReloadCard_CardTransaction_ReloadStarbucksCard_Exception_Failure()
        {
            string userId = "59C216AC-B5C3-4008-B4F7-12CF7968F724";
            string cardId = "8C6772F19ED711AC";

            _selectedCardProvider.AssociateSelectedCardsWithClient(UserId, ClientId, new string[] { cardId });

            var reload = new Card.WebApi.Models.ReloadForStarbucksCard { Amount = 15, PaymentMethodId = "876472FD9BD6", Platform = "Web" };
            reload.RiskFields = new WebApi.Models.Risk();
            reload.RiskFields.Market = "US";
            reload.RiskFields.Platform = "windows";
            reload.RiskFields.IsLoggedIn = true;
            reload.RiskFields.Reputation = new WebApi.Models.Reputation();
            reload.RiskFields.Reputation.IpAddress = "10.10.10.10";
            reload.RiskFields.Reputation.DeviceFingerprint = "asfasdfasdfadsfasdfasdfsdafsadfasdfsdaf";
            _cardTransactionProvider.Setup(pr => pr.ReloadStarbucksCard(It.IsAny<string>(), It.IsAny<string>(), 
                It.IsAny<IReloadForStarbucksCard>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Throws<Exception>();

            HttpResponseMessage result = _controller.ReloadCardByAccount(userId, cardId, reload);
        }

        [TestMethod]
        public void TestReloadCard_With_PayPal_Success()
        {
            string userId = "59C216AC-B5C3-4008-B4F7-12CF7968F724";
            string cardId = "8C6772F19ED711AC";

            _selectedCardProvider.AssociateSelectedCardsWithClient(UserId, ClientId, new string[] { cardId });

            var billingAddress = new Address { AddressLine1 = "123 Main St.", City = "Seattle", Country = "US"};

            var reload = new Card.WebApi.Models.ReloadForStarbucksCard
            { Amount = 15, Platform = "Web", PaymentToken = new Guid().ToString(), BillingAddress = billingAddress };
            reload.RiskFields = new WebApi.Models.Risk();
            reload.RiskFields.Market = "US";
            reload.RiskFields.Platform = "windows";
            reload.RiskFields.IsLoggedIn = true;
            reload.RiskFields.Reputation = new WebApi.Models.Reputation();
            reload.RiskFields.Reputation.IpAddress = "10.10.10.10";
            reload.RiskFields.Reputation.DeviceFingerprint = "asfasdfasdfadsfasdfasdfsdafsadfasdfsdaf";
            _cardTransactionProvider.Setup(pr => pr.ReloadStarbucksCard(It.IsAny<string>(), It.IsAny<string>(),
                It.IsAny<IReloadForStarbucksCard>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).
                Returns(new Mock<ICardTransaction>().Object);

            HttpResponseMessage result = _controller.ReloadCardByAccount(userId, cardId, reload);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void Given_A_ChasePay_Mobile_CardTReload_When_Billing_Not_Null()
        {
            var request = new Card.WebApi.Models.ReloadForStarbucksCard()
            {
                Amount = 5.5m,
                PaymentMethodId = null,
                SessionId = "123456",
                EmailAddress = "test234@sdktest.com",
                PaymentTokenType = "ChasePay",
                IpAddress = "10.20.30.40",
                Platform = "iOS",
                PaymentToken = "4266841007476417",
                PaymentTokenExtension = new PaymentTokenExtension()
                {
                    PaymentCryptogram = "9829062416072217222464173HZ=",
                    ExpirationMonth = 2,
                    ExpirationYear = 2021,
                    ECIndicator = "7",
                    TokenRequestorId = "40010074892",

                    MobilePOS = new MobilePOS()
                    {
                        CombinedTags = "123",
                        TerminalId = "123",
                        TrackData = "123",
                        TransactionReferenceKey = "123"
                    }

                },
                BillingAddress = new Card.WebApi.Models.Address()
                {
                    FirstName = "SDK FirstName123",
                    LastName = "Integration LastName123",
                    PhoneNumber = "425-123-9966",
                    PostalCode = "98998",
                    AddressLine1 = "1234 1st Ave S.",
                    AddressLine2 = "5678",
                    City = "SeaTTle",
                    CountrySubdivision = "WA",
                    Country = "US",
                }
            };

            CardTransactionController.SetBillingAddressToNullForChasePayMobilePos(request);
            Assert.IsTrue(request.BillingAddress == null);
        }

        [TestMethod]
        public void Given_A_ChasePay_Web_CardTReload_When_Billing_Not_Null()
        {
            var request = new Card.WebApi.Models.ReloadForStarbucksCard()
            {
                Amount = 5.5m,
                PaymentMethodId = null,
                SessionId = "123456",
                EmailAddress = "test234@sdktest.com",
                PaymentTokenType = "ChasePay",
                IpAddress = "10.20.30.40",
                Platform = "iOS",
                PaymentToken = "4266841007476417",
                PaymentTokenExtension = new PaymentTokenExtension()
                {
                    PaymentCryptogram = "9829062416072217222464173HZ=",
                    ExpirationMonth = 2,
                    ExpirationYear = 2021,
                    ECIndicator = "7",
                    TokenRequestorId = "40010074892",
                },
                BillingAddress = new Card.WebApi.Models.Address()
                {
                    FirstName = "SDK FirstName123",
                    LastName = "Integration LastName123",
                    PhoneNumber = "425-123-9966",
                    PostalCode = "98998",
                    AddressLine1 = "1234 1st Ave S.",
                    AddressLine2 = "5678",
                    City = "SeaTTle",
                    CountrySubdivision = "WA",
                    Country = "US",
                }
            };

            CardTransactionController.SetBillingAddressToNullForChasePayMobilePos(request);
            Assert.IsTrue(request.BillingAddress != null);
        }

    }
}
