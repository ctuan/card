﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net.Http;
//using System.Text;
//using System.Threading.Tasks;
//using System.Web.Http;
//using Card.WebApi.Controllers;
//using Card.WebApi.Models;
//using Card.WebApi.Tests.Providers;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Starbucks.OpenApi.ServiceExtensions.Exceptions;
//using Starbucks.Platform.Security;

//namespace Card.WebApi.Tests.Controllers
//{
//    [TestClass]
//    public class CardControllerTestMockCardExceptionProvider
//    {
//        private string _cardId;
//        private string _cardNum;
//        private string _cardPin;
//        private string _userId;

//        [TestInitialize]
//        public void Initialize()
//        {
//            _cardNum = "836074FF9AD01EA39DF0961F28BAF4AB";
//            _userId = "F4348ABF-D475-4502-BD59-A100F8075472";
//            _cardId = Encryption.EncryptCardId(974);
//            _cardPin = "826271FA98D11BA9";
//        }

//        [TestMethod]
//        [ExpectedException(typeof(ApiException))]
//        public void MockGetCardByNumberAndPinCardException()
//        {
//            CardController controller = Utility.GetCardController<MockCardProviderCardException, MockPaymentMethodProvider, MockAccountProvider>();
//            string dp = Encryption.DecryptPin(_cardPin);
//            string dn = Encryption.DecryptCardNumber(_cardNum);

//            HttpResponseMessage result = controller.GetCardByNumberAndPin(dn, dp);
//            StarbucksCard card;
//            result.TryGetContentValue(out card);
//            Assert.IsNotNull(card);

//        }

//        [TestMethod]
//        [ExpectedException(typeof(ApiException))]
//        public void MockGetCardByCardIdCardException()
//        {
//            CardController controller = Utility.GetCardController<MockCardProviderCardException, MockPaymentMethodProvider, MockAccountProvider>();
//            HttpResponseMessage result = controller.GetCardById(_userId, _cardId);
//            StarbucksCard card;
//            result.TryGetContentValue(out card);
//            Assert.IsNotNull(card);
//        }

//        [TestMethod]
//        [ExpectedException(typeof(ApiException))]
//        public void MockGetCardsByCardIdCardException()
//        {
//            CardController controller = Utility.GetCardController<MockCardProviderCardException, MockPaymentMethodProvider, MockAccountProvider>();
//            HttpResponseMessage result = controller.GetCardsByUserId("123");
//            StarbucksCard card;
//            result.TryGetContentValue(out card);
//            Assert.IsNotNull(card);
//        }

//        [TestMethod]
//        [ExpectedException(typeof(ApiException))]
//        public void MockGetCardImageUrlByCardNumberCardException()
//        {
//            CardController controller = Utility.GetCardController<MockCardProviderCardException, MockPaymentMethodProvider, MockAccountProvider>();
//            HttpResponseMessage result = controller.GetCardImageUrlByCardNumber(_cardNum);
//            IEnumerable<StarbucksCardImage> images;
//            result.TryGetContentValue(out images);
//            Assert.IsTrue(images.Any());
//        }

//        [TestMethod]
//        [ExpectedException(typeof(ApiException))]
//        public void MockGetStatusesForUserCardsCardException()
//        {
//            CardController controller = Utility.GetCardController<MockCardProviderCardException, MockPaymentMethodProvider, MockAccountProvider>();
//            var cardIds = new List<string> { _cardId };
//            HttpResponseMessage result = controller.GetStatusesForUserCards(_userId, cardIds);
//            IEnumerable<StarbucksCardStatus> statuses;
//            result.TryGetContentValue(out statuses);

//            StarbucksCardStatus[] starbucksCardStatuses = statuses as StarbucksCardStatus[] ?? statuses.ToArray();
//            Assert.IsTrue(starbucksCardStatuses.Any());

//            string id = starbucksCardStatuses[0].CardId;
//            Assert.AreEqual(_cardId, id);
//        }

//        [TestMethod]
//        [ExpectedException(typeof(ApiException))]
//        public void MockRefreshStarbucksCardBalanceCardException()
//        {
//            CardController controller = Utility.GetCardController<MockCardProviderCardException, MockPaymentMethodProvider, MockAccountProvider>();
//            HttpResponseMessage result = controller.GetStarbucksCardBalance(_userId, _cardId);
//            Assert.IsNotNull(result);
//            StarbucksCardBalance balance;
//            result.TryGetContentValue(out balance);
//            Assert.IsNotNull(balance);
//        }

//        [TestMethod]
//        [ExpectedException(typeof(ApiException))]
//        public void MockRefreshStarbucksCardBalanceByCardNumberPinCardException()
//        {
//            var controller = Utility.GetCardController<MockCardProviderCardException, MockPaymentMethodProvider, MockAccountProvider>();
//            var result = controller.GetStarbucksCardBalanceByCardNumber(_cardNum, _cardPin);
//            Assert.IsNotNull(result);
//            StarbucksCardBalance balance = null;
//            result.TryGetContentValue(out balance);
//            Assert.IsNotNull(balance);
//        }

//        //[TestMethod]
//        //[ExpectedException(typeof(ApiException))]
//        //public void MockGetStarbucksCardTransactionCardException()
//        //{
//        //    CardController controller = Utility.GetCardController<MockCardProviderCardException>();
//        //    HttpResponseMessage result = controller.GetStarbucksCardTransaction(_userId, _cardId);
//        //    Assert.IsNotNull(result);

//        //    IEnumerable<StarbucksCardTransaction> transactions;
//        //    result.TryGetContentValue(out transactions);
//        //    Assert.IsTrue(transactions.Any());
//        //}

//        [TestMethod]
//        [ExpectedException(typeof(ApiException))]
//        public void MockRegisterStarbucksCardCardException()
//        {
//            CardController controller = Utility.GetCardController<MockCardProviderCardException, MockPaymentMethodProvider, MockAccountProvider>();
//            var result = controller.RegisterStarbucksCard(_userId, new StarbucksCardNumberAndPin() { CardNumber = _cardNum, Pin = _cardPin });
//            StarbucksCard card;
//            result.TryGetContentValue(out card);
//            Assert.IsNotNull(card);
//        }

//        [TestMethod]
//        [ExpectedException(typeof(ApiException))]
//        public void MockActivateAndRegisterStarbucksCardCardException()
//        {
//            CardController controller = Utility.GetCardController<MockCardProviderCardException, MockPaymentMethodProvider, MockAccountProvider>();
//            var result = controller.ActivateAndRegisterStarbucksCard(_userId);
//            StarbucksCard card;
//            result.TryGetContentValue(out card);
//            Assert.IsNotNull(card);
//        }

//        [TestMethod]
//        [ExpectedException(typeof(ApiException))]
//        public void MockRegisterMultipleStarbucksCardsCardException()
//        {
//            CardController controller = Utility.GetCardController<MockCardProviderCardException, MockPaymentMethodProvider, MockAccountProvider>();
//            var result = controller.RegisterMultipleStarbucksCards(_userId,
//                                                                   new List<StarbucksCardNumberAndPin>
//                                                                       {
//                                                                           new StarbucksCardNumberAndPin
//                                                                               {
//                                                                                   CardNumber = "123",
//                                                                                   Pin = "1234"
//                                                                               }
//                                                                       });
//            List<CardRegistrationStatus> registrationStatuses;
//            result.TryGetContentValue(out registrationStatuses);
//            Assert.IsNotNull(registrationStatuses);
//        }

//        [TestMethod]
//        [ExpectedException(typeof(ApiException))]
//        public void MockUnregisterStarbucksCardCardException()
//        {
//            CardController controller = Utility.GetCardController<MockCardProviderCardException, MockPaymentMethodProvider, MockAccountProvider>();
//            var result = controller.UnregisterStarbucksCard(_userId, _cardId);
//            StarbucksCardUnregisteredResult unregisteredResult;
//            result.TryGetContentValue(out unregisteredResult);
//            Assert.IsNotNull(unregisteredResult);
//        }

//        //[TestMethod]
//        //[ExpectedException(typeof(ApiException))]
//        //public void MockReloadStarbucksCardCardException()
//        //{
//        //    CardController controller = Utility.GetCardController<MockCardProviderCardException, MockPaymentMethodProvider, MockAccountProvider>();
//        //    var result = controller.ReloadCardByAccount(_userId, _cardId, new ReloadForStarbucksCard() { Amount = 100, PaymentMethodId = "1" });

//        //    StarbucksCardBalance transaction;
//        //    result.TryGetContentValue(out transaction);
//        //    Assert.IsNotNull(transaction);
//        //}

//        //[TestMethod]
//        //[ExpectedException(typeof(ApiException))]
//        //public void MockReloadStarbucksCardByNumberException()
//        //{
//        //    CardController controller = Utility.GetCardController<MockCardProviderCardException, MockPaymentMethodProvider, MockAccountProvider>();
//        //    var result = controller.ReloadCardByCardNumber(_cardNum, _cardPin, new ReloadForStarbucksCard() { Amount = 100, PaymentMethodId = "1" });

//        //    StarbucksCardBalance transaction;
//        //    result.TryGetContentValue(out transaction);
//        //    Assert.IsNotNull(transaction);


//        //}

//        //[TestMethod]
//        //public void MockReloadStarbucksCardUsingPayPalRefTransaction()
//        //{
//        //    CardController controller = Utility.GetCardController<MockCardProvider>();
//        //    var result = controller.ReloadStarbucksCardUsingPayPalRefTransaction( _userId, _cardId,
//        //                                                new ReloadForStarbucksCard()
//        //                                                {
//        //                                                    Amount = 200,
//        //                                                    CardBalance = 100,
//        //                                                    PaymentMethodId = "1",
//        //                                                    CardBalanceCurrency = "USD",
//        //                                                    ClientSessionId = "123",
//        //                                                    Type = "A"
//        //                                                });

//        //    StarbucksCardTransaction transaction;
//        //    result.TryGetContentValue(out transaction);
//        //    Assert.IsNotNull(transaction);
//        //}

//        [TestMethod]
//        [ExpectedException(typeof(ApiException))]
//        public void MockSetupAutoReloadCardException()
//        {
//            CardController controller = Utility.GetCardController<MockCardProviderCardException, MockPaymentMethodProvider, MockAccountProvider>();
//            var result = controller.CreateAutoReload(_userId, _cardId, Utility.GetAutoReloadProfile());
//            AutoReloadProfile autoReloadProfile;
//            result.TryGetContentValue(out autoReloadProfile);
//            Assert.IsNotNull(autoReloadProfile);
//        }

//        [TestMethod]
//        [ExpectedException(typeof(ApiException))]
//        public void MockUpdateAutoReloadCardException()
//        {
//            CardController controller = Utility.GetCardController<MockCardProviderCardException, MockPaymentMethodProvider, MockAccountProvider>();
//            var result = controller.UpdateAutoReload(_userId, _cardId, Utility.GetAutoReloadProfile());
//            AutoReloadProfile autoReloadProfile;
//            result.TryGetContentValue(out autoReloadProfile);
//            Assert.IsNotNull(autoReloadProfile);
//        }

//        [TestMethod]
//        [ExpectedException(typeof(ApiException))]
//        public void MockEnableAutoReloadCardException()
//        {
//            CardController controller = Utility.GetCardController<MockCardProviderCardException, MockPaymentMethodProvider, MockAccountProvider>();

//            HttpResponseMessage result = controller.EnableAutoReload(_userId, _cardId);
//            AutoReloadProfile autoReloadProfile = null;
//            result.TryGetContentValue(out autoReloadProfile);
//            Assert.AreEqual(autoReloadProfile.Status, "Enabled");
//        }

//        [TestMethod]
//        [ExpectedException(typeof(ApiException))]
//        public void MockDisableAutoReloadCardException()
//        {
//            CardController controller = Utility.GetCardController<MockCardProviderCardException, MockPaymentMethodProvider, MockAccountProvider>();
//            HttpResponseMessage cardResult = controller.GetCardById(_userId, _cardId);

//            HttpResponseMessage result = controller.DisableAutoReload(_userId, "123");
//            AutoReloadProfile autoReloadProfile = null;
//            result.TryGetContentValue(out autoReloadProfile);
//            Assert.IsNotNull(autoReloadProfile);
//        }

//        [TestMethod]
//        [ExpectedException(typeof(ApiException))]
//        public void MockTransferStarbucksCardBalanceCardException()
//        {
//            CardController controller = Utility.GetCardController<MockCardProviderCardException, MockPaymentMethodProvider, MockAccountProvider>();
//            var balanceTransfer = new BalanceTransfer() { Amount = 100, SourceCardId = "123", TargetCardId = "456" };
//            var result = controller.TransferStarbucksCardBalance(_userId, balanceTransfer);
//            StarbucksCardBalance transaction;
//            result.TryGetContentValue(out transaction);
//            Assert.IsNotNull(transaction);
//        }
//    }
//}
