﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using Card.WebApi.Controllers;
using Card.WebApi.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Starbucks.Card.Provider.Common.Enums;
using Starbucks.Card.Provider.Common;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.CardTransaction.Provider.Common;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;
using Starbucks.Platform.Security;
using ICardTransaction = Starbucks.CardTransaction.Provider.Common.Model.ICardTransaction;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.Card.Provider.Common.Exceptions;
using Starbucks.Card.Provider.Common.ErrorResources;
using StarbucksAccountProvider = Account.Provider;

namespace Card.WebApi.Tests.Controllers
{
    [TestClass]
    public class MockCardControllerTest
    {
        [TestInitialize]
        public void Initialize()
        {
            Encryption.EncryptCardId(974);
        }

        private static void BuildUpMockController(out CardController controller, out Mock<Starbucks.Card.Provider.Common.ICardProvider> cardProvider, out Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider)
        {
            cardProvider = new Mock<ICardProvider>();
            accountProvider = new Mock<StarbucksAccountProvider.Common.IAccountProvider>();
            var log = new Mock<Starbucks.OpenApi.Logging.Common.ILogRepository>();
            controller = new CardController(cardProvider.Object, accountProvider.Object, null, log.Object) { Request = new HttpRequestMessage() };
            var config = new HttpConfiguration();
            WebApi.WebApiConfig.Register(config);

            var user = new Mock<StarbucksAccountProvider.Common.Models.IUser>();
            user.SetupProperty(x => x.SubMarket, "en-US");
            accountProvider.Setup(x => x.GetUser(It.IsAny<string>())).Returns(user.Object);

            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, config);
        }

        private static void BuildUpMockController(out CardTransactionController controller, out Mock<ICardTransactionProvider> cardTransactionProvider, out Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider)
        {
            cardTransactionProvider = new Mock<ICardTransactionProvider>();
            accountProvider = new Mock<StarbucksAccountProvider.Common.IAccountProvider>();
            controller = new CardTransactionController(cardTransactionProvider.Object, accountProvider.Object) { Request = new HttpRequestMessage() };
            var config = new HttpConfiguration();
            WebApi.WebApiConfig.Register(config);
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, config);
        }

        private Risk getRisk()
        {
            Risk risk = new Risk()
            {
                CcAgentName = "cc",
                IsLoggedIn = true,
                Market = "US",
                Platform = "iOS",
                Reputation = new Reputation()
                {
                    IpAddress = "10.10.10.10",
                    DeviceFingerprint = "aaaaaaa"
                }
            };

            return risk;
        }

        [TestMethod]
        public void GetCardByNumberAndPin()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);
            var card = new Mock<ICard>();

            cardProvider.Setup(pr => pr.GetCardByNumberAndPin(It.IsAny<string>(), It.IsAny<string>(), VisibilityLevel.Decrypted, It.IsAny<string>())).Returns(card.Object);

            var result = controller.GetCardByNumberAndPin("123", "123");

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }


        [TestMethod]
        public void GetCardByCardId()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);
            var card = new Mock<IAssociatedCard>();

            cardProvider.Setup(pr => pr.GetCardById(It.IsAny<string>(), It.IsAny<string>(),
                It.IsAny<VisibilityLevel>(), It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<string>())).Returns(card.Object);

            var result = controller.GetCardById("123", "123");

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void UpdateCardNickname()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);
            var card = new Mock<ICard>();

            cardProvider.Setup(pr => pr.UpdateCardNickname(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));

            var result = controller.UpdateCard("123", "123", new UpdateCardRequest() { NickName = "Bob" });

        }

        [TestMethod]
        public void UpdateDefaultCard()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);
            var card = new Mock<ICard>();

            cardProvider.Setup(pr => pr.UpdateDefaultCard(It.IsAny<string>(), It.IsAny<string>()));

            var result = controller.UpdateDefaultCard("123", "123");

        }

        [TestMethod]
        public void GetCardImageUrlByCardNumber()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            var images = new Mock<List<IStarbucksCardImage>>();

            var image = new Mock<IStarbucksCardImage>();
            images.Object.Add(image.Object);

            cardProvider.Setup(pr => pr.GetStarbucksCardImageUrlByCardNumber(It.IsAny<string>()))
                            .Returns(images.Object);

            var result = controller.GetCardImageUrlByCardNumber("123");

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void RefreshStarbucksCardBalance()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            var balance = new Mock<IStarbucksCardBalance>();
            cardProvider.Setup(pr => pr.GetCardBalance(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                            .Returns(balance.Object);

            var result = controller.GetStarbucksCardBalance("123", "123");

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void RefreshStarbucksCardBalanceRealtime()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            var balance = new Mock<IStarbucksCardBalance>();
            cardProvider.Setup(pr => pr.GetCardBalanceRealTime(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                            .Returns(balance.Object);

            var result = controller.GetStarbucksCardBalanceRealTime("123", "123");

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void RefreshStarbucksCardBalanceByCardNumberPin()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            var balance = new Mock<IStarbucksCardBalance>();

            cardProvider.Setup(pr => pr.GetCardBalanceByCardNumberPinNumber(It.IsAny<string>(), It.IsAny<string>(), "US"))
                            .Returns(balance.Object);


            var result = controller.GetStarbucksCardBalanceByCardNumber("0123456789101112", "12345678", "US");

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void RefreshStarbucksCardBalanceByCardNumberPinRealTime()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            var balance = new Mock<IStarbucksCardBalance>();

            cardProvider.Setup(pr => pr.GetCardBalanceByCardNumberPinNumberRealTime(It.IsAny<string>(), It.IsAny<string>(), "US"))
                            .Returns(balance.Object);


            var result = controller.GetStarbucksCardBalanceByCardNumberRealTime("0123456789101112", "12345678", "US");

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void RegisterCard()
        {

            CardController controller;
            Mock<ICardProvider> cardProvider;
            setupStarbucksCard(out controller, out cardProvider);

            StarbucksCardNumberAndPin starbucksCardNumberAndPin = new StarbucksCardNumberAndPin() { CardNumber = "123", Pin = "23" };
            CardRegistrationSource cardRegistrationSource = new CardRegistrationSource() { Marketing = "US", Platform = "iOS" };
            starbucksCardNumberAndPin.RegistrationSource = cardRegistrationSource;
            starbucksCardNumberAndPin.risk = getRisk();

            var result = controller.RegisterStarbucksCard("123", starbucksCardNumberAndPin);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void RegisterCard_ReturnNullCard()
        {

            CardController controller;
            Mock<ICardProvider> cardProvider;
            setupStarbucksCard(out controller, out cardProvider);

            cardProvider.Setup(pr => pr.RegisterStarbucksCard(It.IsAny<string>(),
                                                            It.IsAny<IStarbucksCardHeader>(),
                                                            It.IsAny<string>()));

            cardProvider.Setup(pr => pr.GetCardById(It.IsAny<string>(),
                                                    It.IsAny<string>(),
                                                    It.IsAny<VisibilityLevel>(),
                                                    It.IsAny<bool>(),
                                                            It.IsAny<string>(),
                                                            It.IsAny<string>(),
                                                            It.IsAny<bool>(),
                                                            It.IsAny<string>())).Returns((IAssociatedCard)null);

            var result = controller.RegisterStarbucksCard("123", new StarbucksCardNumberAndPin() { CardNumber = "123", Pin = "23" });

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void RegisterCard_WithCardProviderException1()
        {

            CardController controller;
            Mock<ICardProvider> cardProvider;
            setupStarbucksCard(out controller, out cardProvider);

            cardProvider.Setup(pr => pr.RegisterStarbucksCard(It.IsAny<string>(),
                                                            It.IsAny<IStarbucksCardHeader>(),
                                                            It.IsAny<string>())).Throws(new CardProviderException(CardProviderErrorResource.CannotRegisterInvalidCardNumberCode, "message"));

            var result = controller.RegisterStarbucksCard("123", new StarbucksCardNumberAndPin() { CardNumber = "123", Pin = "23" });

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void RegisterCard_WithCardProviderException2()
        {

            CardController controller;
            Mock<ICardProvider> cardProvider;
            setupStarbucksCard(out controller, out cardProvider);

            cardProvider.Setup(pr => pr.RegisterStarbucksCard(It.IsAny<string>(),
                                                            //It.IsAny<IEnumerable<IStarbucksCardHeader>>()
                                                            It.IsAny<IStarbucksCardHeader>(),
                                                            It.IsAny<string>())).Throws(new CardProviderException(CardProviderErrorResource.InvalidCardNumberCode, "message"));

            var result = controller.RegisterStarbucksCard("123", new StarbucksCardNumberAndPin() { CardNumber = "123", Pin = "23" });

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void RegisterCard_WithCardProviderException3()
        {

            CardController controller;
            Mock<ICardProvider> cardProvider;
            setupStarbucksCard(out controller, out cardProvider);

            cardProvider.Setup(pr => pr.RegisterStarbucksCard(It.IsAny<string>(),
                                                            //It.IsAny<IEnumerable<IStarbucksCardHeader>>()
                                                            It.IsAny<IStarbucksCardHeader>(),
                                                            It.IsAny<string>())).Throws(new CardProviderException(CardProviderErrorResource.InvalidPinCode, "message"));

            var result = controller.RegisterStarbucksCard("123", new StarbucksCardNumberAndPin() { CardNumber = "123", Pin = "23" });

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void RegisterCard_WithCardIssuerException1()
        {

            CardController controller;
            Mock<ICardProvider> cardProvider;
            setupStarbucksCard(out controller, out cardProvider);

            cardProvider.Setup(pr => pr.RegisterStarbucksCard(It.IsAny<string>(),
                                                            //It.IsAny<IEnumerable<IStarbucksCardHeader>>()
                                                            It.IsAny<IStarbucksCardHeader>(),
                                                            It.IsAny<string>())).Throws(new CardIssuerException(CardProviderErrorResource.InvalidPinCode, "message"));

            var result = controller.RegisterStarbucksCard("123", new StarbucksCardNumberAndPin() { CardNumber = "123", Pin = "23" });

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void RegisterCard_WithCardIssuerException2()
        {

            CardController controller;
            Mock<ICardProvider> cardProvider;
            setupStarbucksCard(out controller, out cardProvider);

            cardProvider.Setup(pr => pr.RegisterStarbucksCard(It.IsAny<string>(),
                                                            //It.IsAny<IEnumerable<IStarbucksCardHeader>>()
                                                            It.IsAny<IStarbucksCardHeader>(),
                                                            It.IsAny<string>())).Throws(new CardIssuerException(CardProviderErrorResource.InvalidCardNumberCode, "message"));

            var result = controller.RegisterStarbucksCard("123", new StarbucksCardNumberAndPin() { CardNumber = "123", Pin = "23" });

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void RegisterCard_WithException()
        {

            CardController controller;
            Mock<ICardProvider> cardProvider;
            setupStarbucksCard(out controller, out cardProvider);

            cardProvider.Setup(pr => pr.RegisterStarbucksCard(It.IsAny<string>(),
                                                            //It.IsAny<IEnumerable<IStarbucksCardHeader>>()
                                                            It.IsAny<IStarbucksCardHeader>(),
                                                            It.IsAny<string>())).Throws(new Exception("exception"));

            var result = controller.RegisterStarbucksCard("123", new StarbucksCardNumberAndPin() { CardNumber = "123", Pin = "23" });

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void ActivateAndRegisterStarbucksCard()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);
            var cardHeader = new Mock<Starbucks.Card.Provider.Common.Models.IStarbucksCardHeader>()
            .SetupProperty(p => p.Nickname, "test card");
            cardProvider.Setup(pr => pr.ActivateAndRegisterCard(It.IsAny<string>(), "US", cardHeader.Object))
                               .Returns("");
            var card = new Mock<IAssociatedCard>();
            cardProvider.Setup(pr => pr.GetCardById(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<VisibilityLevel>(),
                It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<string>())).Returns(card.Object);

            var address = new Mock<StarbucksAccountProvider.Common.Models.IAddress>().SetupProperty(p => p.AddressType, "Registration");
            accountProvider.Setup(ap => ap.GetAddresses(It.IsAny<string>())).Returns(new List<StarbucksAccountProvider.Common.Models.IAddress>() { address.Object });

            var result = controller.ActivateAndRegisterStarbucksCard("123", null);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void ActivateAndRegisterCardWithCardHeader()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;

            BuildUpMockController(out controller, out cardProvider, out accountProvider);
            var cardHeader = new Mock<Starbucks.Card.Provider.Common.Models.IStarbucksCardHeader>()
            .SetupProperty(p => p.Nickname, "test card");
            cardProvider.Setup(pr => pr.ActivateAndRegisterCard(It.IsAny<string>(), "US", cardHeader.Object))
                               .Returns("");
            var card = new Mock<IAssociatedCard>();
            cardProvider.Setup(pr => pr.GetCardById(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<VisibilityLevel>(),
                It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<string>())).Returns(card.Object);

            var address = new Mock<StarbucksAccountProvider.Common.Models.IAddress>().SetupProperty(p => p.AddressType, "Registration");
            accountProvider.Setup(ap => ap.GetAddresses(It.IsAny<string>())).Returns(new List<StarbucksAccountProvider.Common.Models.IAddress>() { address.Object });

            var result = controller.ActivateAndRegisterStarbucksCard("123", new StarbucksCardHeader { CardNumber = "123" });

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void RegisterMultipleStarbucksCardsCard()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);
            var cards = new List<ICardRegistrationStatus>();

            cardProvider.Setup(pr => pr.RegisterMultipleCards(It.IsAny<string>(), It.IsAny<IEnumerable<IStarbucksCardHeader>>(), "US"))
                               .Returns(cards);

            var address = new Mock<StarbucksAccountProvider.Common.Models.IAddress>().SetupProperty(p => p.AddressType, "Registration");
            accountProvider.Setup(ap => ap.GetAddresses(It.IsAny<string>())).Returns(new List<StarbucksAccountProvider.Common.Models.IAddress>() { address.Object });

            var result = controller.RegisterMultipleStarbucksCards("123", new List<StarbucksCardNumberAndPin> { new StarbucksCardNumberAndPin() { CardNumber = "123", Pin = "23" } });


            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        //        [ExpectedException(typeof(ApiException))]
        public void RegisterMultipleStarbucksCardsCard_ReturnMultipleCards()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);
            var cards = new List<ICardRegistrationStatus>();
            cards.Add(new Card.WebApi.ProviderModels.CardRegistrationStatus()
            {
                CardId = "111",
                CardNumber = "222",
                Code = "code",
                Message = "message",
                Successful = false
            }
                    );

            cardProvider.Setup(pr => pr.RegisterMultipleCards(It.IsAny<string>(), It.IsAny<IEnumerable<IStarbucksCardHeader>>(), It.IsAny<string>())).Returns(cards);

            var address = new Mock<StarbucksAccountProvider.Common.Models.IAddress>().SetupProperty(p => p.AddressType, "Registration");
            accountProvider.Setup(ap => ap.GetAddresses(It.IsAny<string>())).Returns(new List<StarbucksAccountProvider.Common.Models.IAddress>() { address.Object });

            var result = controller.RegisterMultipleStarbucksCards("123", new List<StarbucksCardNumberAndPin> { new StarbucksCardNumberAndPin() { CardNumber = "123", Pin = "23" } });


            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }


        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void RegisterMultipleStarbucksCardsCard_WithException()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);
            var cards = new List<ICardRegistrationStatus>();

            cardProvider.Setup(pr => pr.RegisterMultipleCards(It.IsAny<string>(), It.IsAny<IEnumerable<IStarbucksCardHeader>>(), It.IsAny<string>())).Throws(new Exception("aaa"));

            var address = new Mock<StarbucksAccountProvider.Common.Models.IAddress>().SetupProperty(p => p.AddressType, "Registration");
            accountProvider.Setup(ap => ap.GetAddresses(It.IsAny<string>())).Returns(new List<StarbucksAccountProvider.Common.Models.IAddress>() { address.Object });

            var result = controller.RegisterMultipleStarbucksCards("123", new List<StarbucksCardNumberAndPin> { new StarbucksCardNumberAndPin() { CardNumber = "123", Pin = "23" } });


            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void RegisterCardWithOutNickname()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            setupStarbucksCard(out controller, out cardProvider);

            var result = controller.RegisterStarbucksCard("123", new StarbucksCardNumberAndPin() { CardNumber = "123", Pin = "23" });

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);

        }

        [TestMethod]
        public void RegisterCardWithNickname()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            setupStarbucksCard(out controller, out cardProvider);

            cardProvider.Setup(
                pr => pr.UpdateCardNickname(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));

            var result = controller.RegisterStarbucksCard("123", new StarbucksCardNumberAndPin() { CardNumber = "123", Pin = "23", Nickname = "Bob" });

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);

        }

        [TestMethod]
        public void RegisterCardWithDefaultCard()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            setupStarbucksCard(out controller, out cardProvider);

            cardProvider.Setup(
                pr => pr.UpdateDefaultCard(It.IsAny<string>(), It.IsAny<string>()));
            var result = controller.RegisterStarbucksCard("123", new StarbucksCardNumberAndPin() { CardNumber = "123", Pin = "23", Primary = true });

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void RegisterCardWithNicknameAndDefaultCard()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            setupStarbucksCard(out controller, out cardProvider);

            cardProvider.Setup(
                pr => pr.UpdateDefaultCard(It.IsAny<string>(), It.IsAny<string>()));

            var result = controller.RegisterStarbucksCard("123", new StarbucksCardNumberAndPin() { CardNumber = "123", Pin = "23", Nickname = "Bob" });

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void AddCardAssociationByNumber()
        {
            CardController controller;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            Mock<ICardProvider> cardProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            var address = new Mock<StarbucksAccountProvider.Common.Models.IAddress>().SetupProperty(p => p.AddressType, "Registration");
            accountProvider.Setup(ap => ap.GetAddresses(It.IsAny<string>())).Returns(new List<StarbucksAccountProvider.Common.Models.IAddress>() { address.Object });

            var cardHeader = new AssociateCardHeader
            {
                CardNumber = "123",
                Nickname = "Bob",
                RegistrationSource = new Card.WebApi.Models.CardRegistrationSource { Marketing = "Starbucks Card", Platform = "Web" }
            };
            cardProvider.Setup(
                pr => pr.AddCardAssociation(It.IsAny<string>(), It.IsAny<string>(), cardHeader.ToProvider("US"), "US"));

            var card = new Mock<IAssociatedCard>();
            cardProvider.Setup(pr => pr.GetCardById(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(card.Object);
            cardProvider.Setup(pr => pr.GetCardIdByCardNumber(It.IsAny<string>())).Returns("123");
            var result = controller.AddCardAssociationByNumber("123", cardHeader);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);


        }

        private static void setupStarbucksCard(out CardController controller, out Mock<ICardProvider> cardProvider)
        {

            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            var address = new Mock<StarbucksAccountProvider.Common.Models.IAddress>().SetupProperty(p => p.AddressType, "Registration");
            accountProvider.Setup(ap => ap.GetAddresses(It.IsAny<string>())).Returns(new List<StarbucksAccountProvider.Common.Models.IAddress>() { address.Object });

            var cardHeader = new StarbucksCardNumberAndPin //StarbucksCardHeader
            {
                CardNumber = "123",
                Pin = "23",
                Primary = true,
                Nickname = "Bob",
                RegistrationSource = new Card.WebApi.Models.CardRegistrationSource { Marketing = "Starbucks Card", Platform = "Web" }
            };
            cardProvider.Setup(
                pr => pr.RegisterStarbucksCard(It.IsAny<string>(), cardHeader.ToProvider(), "US"))
                            .Returns("132");
            var card = new Mock<IAssociatedCard>();
            cardProvider.Setup(pr => pr.GetCardById(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<VisibilityLevel>(),
                It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<string>())).Returns(card.Object);
        }


        [TestMethod]
        public void UnregisterStarbucksCard()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            cardProvider.Setup(pr => pr.UnregisterCard(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<IRisk>())).Returns(true);

            var result = controller.UnregisterStarbucksCard("123", "123");

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }


        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void UnregisterStarbucksCard_WithCardProviderException()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            cardProvider.Setup(pr => pr.UnregisterCard(
                                        It.IsAny<string>(),
                                        It.IsAny<string>(),
                                        It.IsAny<bool>(),
                                        It.IsAny<string>(),
                                        It.IsAny<IRisk>())).Throws(new CardProviderException(CardProviderErrorResource.CardNotFoundCode, "message"));

            var result = controller.UnregisterStarbucksCard("123", "123");

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void UnregisterStarbucksCard_WithException()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            cardProvider.Setup(pr => pr.UnregisterCard(
                                        It.IsAny<string>(),
                                        It.IsAny<string>(),
                                        It.IsAny<bool>(),
                                        It.IsAny<string>(),
                                        It.IsAny<IRisk>())).Throws(new Exception("aaa"));

            var result = controller.UnregisterStarbucksCard("123", "123");

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void UnregisterStarbucksCardWithByHttpPost()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            cardProvider.Setup(pr => pr.UnregisterCard(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<IRisk>())).Returns(true);

            UnregisterCardRisk unregisterCardRisk = new UnregisterCardRisk();
            unregisterCardRisk.RiskFields = getRisk();

            var result = controller.UnregisterStarbucksCardByHttpPost("123", "123", unregisterCardRisk);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void UnregisterStarbucksCardWithByHttpPost_WithCardProviderException()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            cardProvider.Setup(pr => pr.UnregisterCard(
                                        It.IsAny<string>(),
                                        It.IsAny<string>(),
                                        It.IsAny<bool>(),
                                        It.IsAny<string>(),
                                        It.IsAny<IRisk>())).Throws(new CardProviderException(CardProviderErrorResource.CardNotFoundCode, "message"));

            var result = controller.UnregisterStarbucksCardByHttpPost("123", "123", null);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }


        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void UnregisterStarbucksCardWithByHttpPost_WithException()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            cardProvider.Setup(pr => pr.UnregisterCard(
                                        It.IsAny<string>(),
                                        It.IsAny<string>(),
                                        It.IsAny<bool>(),
                                        It.IsAny<string>(),
                                        It.IsAny<IRisk>())).Throws(new Exception("aaa"));

            var result = controller.UnregisterStarbucksCardByHttpPost("123", "123", null);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }


        [TestMethod]
        public void GetStatusesForUserCards()
        {

            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            var statuses = new Mock<List<IStarbucksCardStatus>>();
            var status = new Mock<IStarbucksCardStatus>();
            statuses.Object.Add(status.Object);

            cardProvider.Setup(pr => pr.GetStatusesForUserCards(It.IsAny<string>(), It.IsAny<IEnumerable<string>>())).Returns(statuses.Object);

            var result = controller.GetStatusesForUserCards("123", new List<string> { "123" });

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void DisableAutoReload()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            cardProvider.Setup(pr => pr.DisableAutoReload(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IRisk>()));

            EnableDisableReloadRisk enableDisableReloadRisk = new EnableDisableReloadRisk();
            enableDisableReloadRisk.RiskFields = getRisk();

            var result = controller.DisableAutoReload("123", "123", enableDisableReloadRisk, "US");

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void DisableAutoReload_WithException()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            cardProvider.Setup(pr => pr.DisableAutoReload(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IRisk>())).Throws(new Exception("aaa"));

            EnableDisableReloadRisk enableDisableReloadRisk = new EnableDisableReloadRisk();
            enableDisableReloadRisk.RiskFields = getRisk();

            var result = controller.DisableAutoReload("123", "123", enableDisableReloadRisk, "US");

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void EnableAutoReload()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            cardProvider.Setup(pr => pr.EnableAutoReload(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IRisk>()));

            EnableDisableReloadRisk enableDisableReloadRisk = new EnableDisableReloadRisk();
            enableDisableReloadRisk.RiskFields = getRisk();

            var result = controller.EnableAutoReload("123", "123", enableDisableReloadRisk, "US");

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void EnableAutoReload_WithEception()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            cardProvider.Setup(pr => pr.EnableAutoReload(
                                    It.IsAny<string>(),
                                    It.IsAny<string>(),
                                    It.IsAny<string>(),
                                    It.IsAny<IRisk>())).Throws(new Exception("aaa"));

            EnableDisableReloadRisk enableDisableReloadRisk = new EnableDisableReloadRisk();
            enableDisableReloadRisk.RiskFields = getRisk();

            var result = controller.EnableAutoReload("123", "123", enableDisableReloadRisk, "US");

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void GetCardsById()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            var cards = new Mock<List<IAssociatedCard>>();
            cards.Object.Add(new Mock<IAssociatedCard>().Object);

            cardProvider.Setup(pr => pr.GetCards(It.IsAny<string>(), It.IsAny<VisibilityLevel>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<string>())).Returns(cards.Object);

            var result = controller.GetCardsByUserId("123");

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }


        [TestMethod]
        public void CreateAutoload()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            var mockAutoReload = new Mock<IAutoReloadProfile>();

            cardProvider.Setup(pr => pr.SetupAutoReload(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IAutoReloadProfile>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IRisk>())).Returns(mockAutoReload.Object);

            var result = controller.CreateAutoReload("123", "13", new AutoReloadProfileWithRisk(), "US");

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void UpdateAutoload()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            var mockAutoReload = new Mock<IAutoReloadProfile>();

            cardProvider.Setup(pr => pr.UpdateAutoReload(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IAutoReloadProfile>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IRisk>())).Returns(mockAutoReload.Object);

            var result = controller.UpdateAutoReload("123", "13", new AutoReloadProfileWithRisk());

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }


        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void UpdateAutoload_WithException()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            var mockAutoReload = new Mock<IAutoReloadProfile>();

            cardProvider.Setup(pr => pr.UpdateAutoReload(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IAutoReloadProfile>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IRisk>())).Throws(new Exception("aaa"));

            var result = controller.UpdateAutoReload("123", "13", new AutoReloadProfileWithRisk());

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }


        [TestMethod]
        public void ReloadCard()
        {
            CardTransactionController controller;
            Mock<ICardTransactionProvider> cardTransactionProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardTransactionProvider, out accountProvider);

            var balance = new Mock<Starbucks.CardTransaction.Provider.Common.Model.ICardTransaction>();

            cardTransactionProvider.Setup(pr => pr.ReloadStarbucksCard(It.IsAny<string>(), It.IsAny<string>(),
                It.IsAny<Starbucks.CardTransaction.Provider.Common.Model.IReloadForStarbucksCard>(),
                It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(balance.Object);

            ReloadForStarbucksCard reloadForStarbucksCard = new ReloadForStarbucksCard();
            reloadForStarbucksCard.RiskFields = getRisk();

            var result = controller.ReloadCardByAccount("123", "13", reloadForStarbucksCard);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void ReloadCard_WithException()
        {
            CardTransactionController controller;
            Mock<ICardTransactionProvider> cardTransactionProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardTransactionProvider, out accountProvider);

            var balance = new Mock<Starbucks.CardTransaction.Provider.Common.Model.ICardTransaction>();

            cardTransactionProvider.Setup(pr => pr.ReloadStarbucksCard(It.IsAny<string>(), It.IsAny<string>(),
                It.IsAny<Starbucks.CardTransaction.Provider.Common.Model.IReloadForStarbucksCard>(),
                It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns((ICardTransaction)null);

            ReloadForStarbucksCard reloadForStarbucksCard = new ReloadForStarbucksCard();
            reloadForStarbucksCard.RiskFields = getRisk();

            var result = controller.ReloadCardByAccount("123", "13", reloadForStarbucksCard);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void ReloadCardByCardNumber()
        {
            CardTransactionController controller;
            Mock<ICardTransactionProvider> cardTransactionProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardTransactionProvider, out accountProvider);

            var balance = new Mock<Starbucks.CardTransaction.Provider.Common.Model.ICardTransaction>();

            cardTransactionProvider.Setup(pr => pr.ReloadStarbucksCardByCardNumberPin(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Starbucks.CardTransaction.Provider.Common.Model.IReloadForStarbucksCard>(), It.IsAny<string>(), "US")).Returns(balance.Object);

            var result = controller.ReloadCardByCardNumber("123", "13", new ReloadForStarbucksCard(), "US");

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void ReloadCardByCardNumber_FailsWith_Exception()
        {
            CardTransactionController controller;
            Mock<ICardTransactionProvider> cardTransactionProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardTransactionProvider, out accountProvider);

            var balance = new Mock<Starbucks.CardTransaction.Provider.Common.Model.ICardTransaction>();

            cardTransactionProvider.Setup(pr => pr.ReloadStarbucksCardByCardNumberPin(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Starbucks.CardTransaction.Provider.Common.Model.IReloadForStarbucksCard>(), It.IsAny<string>(), "US"))
                .Returns((ICardTransaction)null);

            var result = controller.ReloadCardByCardNumber("123", "13", new ReloadForStarbucksCard(), "US");

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }


        [TestMethod]
        public void TransferBalance()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            var balance = new Mock<ITransferBalanceResult>();
            balance.SetupProperty(p => p.StarbucksCardTransaction, new Mock<IStarbucksCardTransaction>().Object);
            balance.SetupProperty(p => p.Balances, new Dictionary<string, decimal>());

            var bal = new Mock<List<IStarbucksCardBalance>>();
            //var balanceTransfer = new BalanceTransfer { Amount = 15, SourceCardId = "8C6772F19ED91FA9", TargetCardId = "8C6772F19ED91FA8" };
            var balanceTransfer = new BalanceTransfer { Amount = 15, SourceCardId = "8C6772F19ED91FA9", TargetCardId = "8C6772F19ED91FA8" };
            balanceTransfer.RiskFields = getRisk();

            cardProvider.Setup(pr => pr.TransferRegistered(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<decimal?>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IRisk>())).Returns(bal.Object);

            var result = controller.TransferStarbucksCardBalance("59C216AC-B5C3-4008-B4F7-12CF7968F724", balanceTransfer);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void TransferBalance_ReturnNull()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            var balance = new Mock<ITransferBalanceResult>();
            balance.SetupProperty(p => p.StarbucksCardTransaction, new Mock<IStarbucksCardTransaction>().Object);
            balance.SetupProperty(p => p.Balances, new Dictionary<string, decimal>());

            var bal = new Mock<List<IStarbucksCardBalance>>();
            //var balanceTransfer = new BalanceTransfer { Amount = 15, SourceCardId = "8C6772F19ED91FA9", TargetCardId = "8C6772F19ED91FA8" };
            var balanceTransfer = new BalanceTransfer { Amount = 15, SourceCardId = "8C6772F19ED91FA9", TargetCardId = "8C6772F19ED91FA8" };
            balanceTransfer.RiskFields = getRisk();

            cardProvider.Setup(pr => pr.TransferRegistered(It.IsAny<string>(),
                                                            It.IsAny<string>(),
                                                            It.IsAny<string>(),
                                                            It.IsAny<decimal?>(),
                                                            It.IsAny<string>(),
                                                            It.IsAny<string>(),
                                                            It.IsAny<IRisk>())).Returns((List<IStarbucksCardBalance>)null);


            var result = controller.TransferStarbucksCardBalance("59C216AC-B5C3-4008-B4F7-12CF7968F724", balanceTransfer);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void TransferBalance_WithCardIssuerException()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            var balance = new Mock<ITransferBalanceResult>();
            balance.SetupProperty(p => p.StarbucksCardTransaction, new Mock<IStarbucksCardTransaction>().Object);
            balance.SetupProperty(p => p.Balances, new Dictionary<string, decimal>());

            var bal = new Mock<List<IStarbucksCardBalance>>();
            var balanceTransfer = new BalanceTransfer { Amount = 15, SourceCardId = "8C6772F19ED91FA9", TargetCardId = "8C6772F19ED91FA8" };
            balanceTransfer.RiskFields = getRisk();

            cardProvider.Setup(pr => pr.TransferRegistered(It.IsAny<string>(),
                                                            It.IsAny<string>(),
                                                            It.IsAny<string>(),
                                                            It.IsAny<decimal?>(),
                                                            It.IsAny<string>(),
                                                            It.IsAny<string>(),
                                                            It.IsAny<IRisk>())).Throws(new CardIssuerException("code", "message"));

            var result = controller.TransferStarbucksCardBalance("59C216AC-B5C3-4008-B4F7-12CF7968F724", balanceTransfer);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void TransferBalance_WithCardProviderException()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            var balance = new Mock<ITransferBalanceResult>();
            balance.SetupProperty(p => p.StarbucksCardTransaction, new Mock<IStarbucksCardTransaction>().Object);
            balance.SetupProperty(p => p.Balances, new Dictionary<string, decimal>());

            var bal = new Mock<List<IStarbucksCardBalance>>();
            var balanceTransfer = new BalanceTransfer { Amount = 15, SourceCardId = "8C6772F19ED91FA9", TargetCardId = "8C6772F19ED91FA8" };
            balanceTransfer.RiskFields = getRisk();

            cardProvider.Setup(pr => pr.TransferRegistered(It.IsAny<string>(),
                                                            It.IsAny<string>(),
                                                            It.IsAny<string>(),
                                                            It.IsAny<decimal?>(),
                                                            It.IsAny<string>(),
                                                            It.IsAny<string>(),
                                                            It.IsAny<IRisk>())).Throws(new CardProviderException("code", "message"));

            var result = controller.TransferStarbucksCardBalance("59C216AC-B5C3-4008-B4F7-12CF7968F724", balanceTransfer);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ApiException))]
        public void TransferBalance_WithException()
        {
            CardController controller;
            Mock<ICardProvider> cardProvider;
            Mock<StarbucksAccountProvider.Common.IAccountProvider> accountProvider;
            BuildUpMockController(out controller, out cardProvider, out accountProvider);

            var balance = new Mock<ITransferBalanceResult>();
            balance.SetupProperty(p => p.StarbucksCardTransaction, new Mock<IStarbucksCardTransaction>().Object);
            balance.SetupProperty(p => p.Balances, new Dictionary<string, decimal>());

            var bal = new Mock<List<IStarbucksCardBalance>>();
            var balanceTransfer = new BalanceTransfer { Amount = 15, SourceCardId = "8C6772F19ED91FA9", TargetCardId = "8C6772F19ED91FA8" };
            balanceTransfer.RiskFields = getRisk();

            cardProvider.Setup(pr => pr.TransferRegistered(It.IsAny<string>(),
                                                            It.IsAny<string>(),
                                                            It.IsAny<string>(),
                                                            It.IsAny<decimal?>(),
                                                            It.IsAny<string>(),
                                                            It.IsAny<string>(),
                                                            It.IsAny<IRisk>())).Throws(new Exception("code"));

            var result = controller.TransferStarbucksCardBalance("59C216AC-B5C3-4008-B4F7-12CF7968F724", balanceTransfer);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        //[TestMethod]
        //[ExpectedException(typeof(ApiException))]
        //public void ReloadStarbucksCardByNumberException()
        //{
        //    CardController controller;
        //    Mock<ICardProvider> cardProvider;
        //    Mock<IAccountProvider> accountProvider;
        //    BuildUpMockController(out controller, out cardProvider, out accountProvider);
        //    var result = controller.ReloadCardByCardNumber(_cardNum, _cardPin, new ReloadForStarbucksCard() { Amount = 100, PaymentMethodId = "1" });

        //    StarbucksCardBalance transaction;
        //    result.TryGetContentValue(out transaction);
        //    Assert.IsNotNull(transaction);


        //}

        //        //[TestMethod]
        //        //public void ReloadStarbucksCardUsingPayPalRefTransaction()
        //        //{
        //        //    CardController controller = Utility.GetCardController<MockCardProvider>();
        //        //    var result = controller.ReloadStarbucksCardUsingPayPalRefTransaction( _userId, _cardId,
        //        //                                                new ReloadForStarbucksCard()
        //        //                                                {
        //        //                                                    Amount = 200,
        //        //                                                    CardBalance = 100,
        //        //                                                    PaymentMethodId = "1",
        //        //                                                    CardBalanceCurrency = "USD",
        //        //                                                    ClientSessionId = "123",
        //        //                                                    Type = "A"
        //        //                                                });

        //        //    StarbucksCardTransaction transaction;
        //        //    result.TryGetContentValue(out transaction);
        //        //    Assert.IsNotNull(transaction);
        //        //}

        //        [TestMethod]
        //        [ExpectedException(typeof(ApiException))]
        //        public void SetupAutoReloadCardException()
        //        {
        //            CardController controller = Utility.GetCardController<MockCardProviderCardException>();
        //            var result = controller.CreateAutoReload(_userId, _cardId, Utility.GetAutoReloadProfile());
        //            AutoReloadProfile autoReloadProfile;
        //            result.TryGetContentValue(out autoReloadProfile);
        //            Assert.IsNotNull(autoReloadProfile);
        //        }

        //        [TestMethod]
        //        [ExpectedException(typeof(ApiException))]
        //        public void UpdateAutoReloadCardException()
        //        {
        //            CardController controller = Utility.GetCardController<MockCardProviderCardException>();
        //            var result = controller.UpdateAutoReload(_userId, _cardId, Utility.GetAutoReloadProfile());
        //            AutoReloadProfile autoReloadProfile;
        //            result.TryGetContentValue(out autoReloadProfile);
        //            Assert.IsNotNull(autoReloadProfile);
        //        }

        //        [TestMethod]
        //        [ExpectedException(typeof(ApiException))]
        //        public void EnableAutoReloadCardException()
        //        {
        //            CardController controller = Utility.GetCardController<MockCardProviderCardException>();

        //            HttpResponseMessage result = controller.EnableAutoReload(_userId, _cardId);
        //            AutoReloadProfile autoReloadProfile = null;
        //            result.TryGetContentValue(out autoReloadProfile);
        //            Assert.AreEqual(autoReloadProfile.Status, "Enabled");
        //        }

        //        [TestMethod]
        //        [ExpectedException(typeof(ApiException))]
        //        public void DisableAutoReloadCardException()
        //        {
        //            CardController controller = Utility.GetCardController<MockCardProviderCardException>();
        //            HttpResponseMessage cardResult = controller.GetCardById(_userId, _cardId);

        //            HttpResponseMessage result = controller.DisableAutoReload(_userId, "123");
        //            AutoReloadProfile autoReloadProfile = null;
        //            result.TryGetContentValue(out autoReloadProfile);
        //            Assert.IsNotNull(autoReloadProfile);
        //        }

        //        [TestMethod]
        //        [ExpectedException(typeof(ApiException))]
        //        public void TransferStarbucksCardBalanceCardException()
        //        {
        //            CardController controller = Utility.GetCardController<MockCardProviderCardException>();
        //            var balanceTransfer = new BalanceTransfer() { Amount = 100, SourceCard = "123", DestinationCard = "456" };
        //            var result = controller.TransferStarbucksCardBalance(_userId, balanceTransfer);
        //            StarbucksCardBalance transaction;
        //            result.TryGetContentValue(out transaction);
        //            Assert.IsNotNull(transaction);
        //        }
    }
}
