﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Net.Http.Formatting;
//using System.Reflection;
//using System.Runtime.Serialization;
//using System.Runtime.Serialization.Json;
//using System.Web.Http;
//using Autofac;
//using Autofac.Integration.WebApi;
//using Card.WebApi.Models;
//using Card.WebApi.Tests.Providers;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using PaymentMethod.Provider.Common;

//namespace Card.WebApi.Tests.Controllers
//{
//    [TestClass]
//    public class CardControllerTest
//    {

//        private string _cardNum;
//        private string _userId;
//        private string _cardId;
//        private string _cardPin;

//        [TestInitialize]
//        public void Initialize()
//        {
//            //_cardNum =  Starbucks.Platform.Security.Encryption.DecryptCardNumber( "836074FF9AD01EA39DF0961F28BAF4AB");
//            //_userId = "F4348ABF-D475-4502-BD59-A100F8075472";

//            //_cardPin =Starbucks.Platform.Security.Encryption.DecryptPin( "826271FA98D11BA9"); 

//            //_cardId = Starbucks.Platform.Security.Encryption.EncryptCardId(974);

//            _cardNum = Starbucks.Platform.Security.Encryption.DecryptCardNumber("836074FF9AD61BAB90FF961F27BFF3A7");


//            _cardPin = Starbucks.Platform.Security.Encryption.DecryptPin("8C6F7AF099D211A8");

//            System.Diagnostics.Debug.WriteLine(_cardNum);
//            System.Diagnostics.Debug.WriteLine(_cardPin);
//            //836074FF9AD61BAB90FF961F27BFF3A7
//            //8C6F7AF099D211A8
//        }

//        private const string BaseUrl = "http://localhost:31024";

//        [TestMethod]
//        public void CardImageUrlByCardNumber()
//        {
//            var config = new HttpConfiguration();

//            WebApiConfig.Register(config);
//            Utility.RegisterDependencies<MockCardProvider, MockPaymentMethodProvider>(config);

//            var server = new HttpServer(config);
//            var client = new HttpClient(server);

//            var response =
//                client.GetAsync(string.Format("{0}/cards/{1}/images", Utility.BaseUrl, "0123456789101112")).Result;


//            IEnumerable<StarbucksCardImage> cardImage;
//            response.TryGetContentValue(out cardImage);
//            Assert.IsNotNull(cardImage);
//        }

//        [TestMethod]
//        public void GetCardByNumberAndPin()
//        {
//            var config = new HttpConfiguration();

//            WebApiConfig.Register(config);
//            Utility.RegisterDependencies<MockCardProvider, MockPaymentMethodProvider>(config);

//            var server = new HttpServer(config);
//            var client = new HttpClient(server);

//            var response =
//                client.GetAsync(string.Format("{0}/cards/{1}/{2}", BaseUrl, "0123456789101112", "12345678")).Result;


//            StarbucksCard card;
//            response.TryGetContentValue(out card);
//            Assert.IsNotNull(card);
//        }

//        [TestMethod]
//        public void GetCardById()
//        {
//            var config = new HttpConfiguration();

//            WebApiConfig.Register(config);
//            Utility.RegisterDependencies<MockCardProvider, MockPaymentMethodProvider>(config);

//            var server = new HttpServer(config);
//            var client = new HttpClient(server);

//            var response =
//                client.GetAsync(string.Format("{0}/users/{1}/cards/{2}", BaseUrl, "0123456789101112", "12345678")).Result;


//            StarbucksCard card;
//            response.TryGetContentValue(out card);
//            Assert.IsNotNull(card);
//        }


//        [TestMethod]
//        public void CardsByUserId()
//        {
//            var config = new HttpConfiguration();

//            WebApiConfig.Register(config);
//            Utility.RegisterDependencies<MockCardProvider, MockPaymentMethodProvider>(config);

//            var server = new HttpServer(config);
//            var client = new HttpClient(server);


//            var response = client.GetAsync(string.Format("{0}/users/{1}/cards", BaseUrl, "0123456789101112")).Result;


//            IEnumerable<StarbucksCard> cards;
//            response.TryGetContentValue(out cards);
//            Assert.IsNotNull(cards);
//        }

//        [TestMethod]
//        public void CardStatuses()
//        {
//            var config = new HttpConfiguration();

//            WebApiConfig.Register(config);
//            Utility.RegisterDependencies<MockCardProvider, MockPaymentMethodProvider>(config);

//            var server = new HttpServer(config);
//            var client = new HttpClient(server);

//            HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Get,
//                                                                string.Format("{0}/users/{1}/cards/validate", BaseUrl,
//                                                                              "0123456789101112"));

//            var data = new List<string>() { "1", "2" };
//            MediaTypeFormatter jsonFormatter = new JsonMediaTypeFormatter();

//            message.Content = new ObjectContent<List<string>>(data, jsonFormatter);

//            var response = client.SendAsync(message).Result;


//            IEnumerable<StarbucksCardStatus> statuses;
//            response.TryGetContentValue(out statuses);
//            Assert.IsNotNull(statuses);
//        }


//        [TestMethod]
//        public void CardsBalanceByCardNumber()
//        {
//            var config = new HttpConfiguration();

//            WebApiConfig.Register(config);
//            Utility.RegisterDependencies<MockCardProvider, MockPaymentMethodProvider>(config);

//            var server = new HttpServer(config);
//            var client = new HttpClient(server);

//            var response = client.GetAsync(string.Format("{0}/cards/{1}/{2}/balance", BaseUrl, "0123456789101112", "12345678")).Result;


//            StarbucksCardBalance balance;
//            response.TryGetContentValue(out balance);
//            Assert.IsNotNull(balance);
//        }


//        [TestMethod]
//        public void CardsBalanceByStarbucksAccount()
//        {
//            var config = new HttpConfiguration();

//            WebApiConfig.Register(config);
//            Utility.RegisterDependencies<MockCardProvider, MockPaymentMethodProvider>(config);

//            var server = new HttpServer(config);
//            var client = new HttpClient(server);

//            var response = client.GetAsync(string.Format("{0}/users/{1}/cards/{2}/balance", BaseUrl, "0123456789101112", "12345678")).Result;


//            StarbucksCardBalance balance;
//            response.TryGetContentValue(out balance);
//            Assert.IsNotNull(balance);
//        }

//        [TestMethod]
//        public void RegisterAPhysicalCard()
//        {
//            var config = new HttpConfiguration();

//            WebApiConfig.Register(config);
//            Utility.RegisterDependencies<MockCardProvider, MockPaymentMethodProvider>(config);

//            var server = new HttpServer(config);
//            var client = new HttpClient(server);

//            var pinCard = new Card.WebApi.Models.StarbucksCardNumberAndPin() { CardNumber = "0123456789101112", Pin = "12345678" };

//            var response = client.PostAsJsonAsync(string.Format("{0}/users/{1}/cards/register", BaseUrl, "0123456789101112"), pinCard).Result;

//            StarbucksCard card;
//            response.TryGetContentValue(out card);
//            Assert.IsNotNull(card);
//        }

//        [TestMethod]
//        public void RegisterADigitalCard()
//        {
//            var config = new HttpConfiguration();

//            WebApiConfig.Register(config);
//            Utility.RegisterDependencies<MockCardProvider, MockPaymentMethodProvider>(config);

//            var server = new HttpServer(config);
//            var client = new HttpClient(server);


//            HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Post,
//                                                               string.Format("{0}/users/{1}/cards/register-digital", BaseUrl,
//                                                                             "0123456789101112"));
//            var response = client.SendAsync(message).Result;

//            StarbucksCard card;
//            response.TryGetContentValue(out card);
//            Assert.IsNotNull(card);
//        }

//        [TestMethod]
//        public void RegistermultiplePhysicalCard()
//        {
//            var config = new HttpConfiguration();

//            WebApiConfig.Register(config);
//            Utility.RegisterDependencies<MockCardProvider, MockPaymentMethodProvider>(config);

//            var server = new HttpServer(config);
//            var client = new HttpClient(server);

//            var pinCard = new List<Card.WebApi.Models.StarbucksCardNumberAndPin> { new StarbucksCardNumberAndPin() { CardNumber = "0123456789101112", Pin = "12345678" } };

//            var response = client.PostAsJsonAsync(string.Format("{0}/users/{1}/cards/register-multiple", BaseUrl, "0123456789101112"), pinCard).Result;

//            IEnumerable<CardRegistrationStatus> card;
//            response.TryGetContentValue(out card);
//            Assert.IsNotNull(card);
//        }


//        [TestMethod]
//        public void UnregisterACard()
//        {
//            var config = new HttpConfiguration();

//            WebApiConfig.Register(config);
//            Utility.RegisterDependencies<MockCardProvider, MockPaymentMethodProvider>(config);

//            var server = new HttpServer(config);
//            var client = new HttpClient(server);


//            HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Delete,
//                                                               string.Format("{0}/users/{1}/cards/{2}", BaseUrl,
//                                                                             "0123456789101112", "123214124124"));
//            var response = client.SendAsync(message).Result;

//            Assert.IsTrue(response.IsSuccessStatusCode);

//            StarbucksCardUnregisteredResult starbucksCardUnregisteredResult;
//            response.TryGetContentValue(out starbucksCardUnregisteredResult);
//            Assert.IsNotNull(starbucksCardUnregisteredResult);
//        }

//        [TestMethod]
//        public void ReloadCardByAccount()
//        {
//            var config = new HttpConfiguration();

//            WebApiConfig.Register(config);
//            Utility.RegisterDependencies<MockCardProvider, MockPaymentMethodProvider>(config);

//            var server = new HttpServer(config);
//            var client = new HttpClient(server);


//            var reload = new ReloadForStarbucksCard() { Amount = 100, PaymentMethodId = "1" };

//            var response = client.PostAsJsonAsync(string.Format("{0}/users/{1}/cards/{2}/reload", BaseUrl, "0123456789101112", "123214124124"), reload).Result;

//            Assert.IsTrue(response.IsSuccessStatusCode);

//            StarbucksCardBalance starbucksCardTransaction;
//            response.TryGetContentValue(out starbucksCardTransaction);
//            Assert.IsNotNull(starbucksCardTransaction);
//        }

//        [TestMethod]
//        public void ReloadCardByCardNumber()
//        {
//            var config = new HttpConfiguration();

//            WebApiConfig.Register(config);
//            Utility.RegisterDependencies<MockCardProvider, MockPaymentMethodProvider>(config);

//            var server = new HttpServer(config);
//            var client = new HttpClient(server);


//            var reload = new ReloadForStarbucksCard() { Amount = 100, PaymentMethodId = "1" };

//            var response = client.PostAsJsonAsync(string.Format("{0}/cards/{1}/{2}/reload", BaseUrl, "0123456789101112", "12345678"), reload).Result;

//            Assert.IsTrue(response.IsSuccessStatusCode);

//            StarbucksCardBalance starbucksCardTransaction;
//            response.TryGetContentValue(out starbucksCardTransaction);
//            Assert.IsNotNull(starbucksCardTransaction);
//        }

//        [TestMethod]
//        public void CreateAutoReload()
//        {
//            var config = new HttpConfiguration();

//            WebApiConfig.Register(config);
//            Utility.RegisterDependencies<MockCardProvider, MockPaymentMethodProvider>(config);

//            var server = new HttpServer(config);
//            var client = new HttpClient(server);


//            var reload = new AutoReloadProfile() { Amount = 100, PaymentMethodId = "1", AutoReloadType = "Amount", TriggerAmount = 10 };

//            var response = client.PostAsJsonAsync(string.Format("{0}/users/{1}/cards/{2}/autoreload", BaseUrl, "0123456789101112", "123214124124"), reload).Result;

//            Assert.IsTrue(response.IsSuccessStatusCode);


//        }

//        [TestMethod]
//        public void UpdateAutoReload()
//        {
//            var config = new HttpConfiguration();

//            WebApiConfig.Register(config);
//            Utility.RegisterDependencies<MockCardProvider, MockPaymentMethodProvider>(config);

//            var server = new HttpServer(config);
//            var client = new HttpClient(server);


//            var reload = new AutoReloadProfile() { Amount = 100, PaymentMethodId = "1", AutoReloadType = "Amount", TriggerAmount = 10 };

//            var response = client.PutAsJsonAsync(string.Format("{0}/users/{1}/cards/{2}/autoreload", BaseUrl, "0123456789101112", "123214124124"), reload).Result;

//            Assert.IsTrue(response.IsSuccessStatusCode);

//            AutoReloadProfile autoReloadProfile;
//            response.TryGetContentValue(out autoReloadProfile);
//            Assert.IsNotNull(autoReloadProfile);
//        }

//        [TestMethod]
//        public void EnableAutoReload()
//        {
//            var config = new HttpConfiguration();

//            WebApiConfig.Register(config);
//            Utility.RegisterDependencies<MockCardProvider, MockPaymentMethodProvider>(config);

//            var server = new HttpServer(config);
//            var client = new HttpClient(server);

//            var message = new HttpRequestMessage(HttpMethod.Put, string.Format("{0}/users/{1}/cards/{2}/autoreload/enable", BaseUrl, "0123456789101112", "123214124124"));
//            var response = client.SendAsync(message).Result;

//            Assert.IsTrue(response.IsSuccessStatusCode);

//        }

//        [TestMethod]
//        public void DisableAutoReload()
//        {
//            var config = new HttpConfiguration();

//            WebApiConfig.Register(config);
//            Utility.RegisterDependencies<MockCardProvider, MockPaymentMethodProvider>(config);

//            var server = new HttpServer(config);
//            var client = new HttpClient(server);

//            var message = new HttpRequestMessage(HttpMethod.Put, string.Format("{0}/users/{1}/cards/{2}/autoreload/disable", BaseUrl, "0123456789101112", "123214124124"));
//            var response = client.SendAsync(message).Result;

//            Assert.IsTrue(response.IsSuccessStatusCode);

//        }

//        [TestMethod]
//        public void TransferBalance()
//        {
//            var config = new HttpConfiguration();

//            WebApiConfig.Register(config);
//            Utility.RegisterDependencies<MockCardProvider, MockPaymentMethodProvider>(config);

//            var server = new HttpServer(config);
//            var client = new HttpClient(server);


//            var reload = new BalanceTransfer() { Amount = 100, DestinationCard = "0123456789101112", SourceCard = "0123456789101111" };

//            var response = client.PostAsJsonAsync(string.Format("{0}/users/{1}/cards/transfer", BaseUrl, "0123456789101112"), reload).Result;

//            Assert.IsTrue(response.IsSuccessStatusCode);

//            IEnumerable<StarbucksCardBalance> transferBalanceResult;
//            response.TryGetContentValue(out transferBalanceResult);
//            Assert.IsNotNull(transferBalanceResult);
//        }

//        //    [TestMethod]
//        //    public void GetCardByNumberAndPin()
//        //    {
//        //        var controller = Utility.GetCardController<CardProvider, PaymentMethodProviderFacade>();
//        //        var dp = Starbucks.Platform.Security.Encryption.DecryptPin(_cardPin);
//        //        var dn = Starbucks.Platform.Security.Encryption.DecryptCardNumber(_cardNum);
//        //        var result = controller.GetCardByNumberAndPin(dn, dp);
//        //        StarbucksCard card = null;
//        //        result.TryGetContentValue(out card);
//        //        Assert.IsNotNull(card);
//        //    }

//        //    [TestMethod]
//        //    public void GetCardByCardId()
//        //    {
//        //        var controller = Utility.GetCardController<CardProvider, PaymentMethodProviderFacade>();
//        //        var result = controller.GetCardById(_userId , _cardId );
//        //        StarbucksCard card = null;
//        //        result.TryGetContentValue(out card);
//        //        Assert.IsNotNull(card);
//        //    }

//        //    [TestMethod]
//        //    public void GetCardImageUrlByCardNumber()
//        //    {
//        //        var controller = Utility.GetCardController<CardProvider, PaymentMethodProviderFacade>();
//        //        var result = controller.GetCardImageUrlByCardNumber(_cardNum);
//        //        IEnumerable<StarbucksCardImage > images = null;
//        //        result.TryGetContentValue(out images);
//        //        Assert.IsTrue(images.Any());
//        //    }

//        //    [TestMethod]
//        //    public void GetStatusesForUserCards()
//        //    {
//        //        var controller = Utility.GetCardController<CardProvider, PaymentMethodProviderFacade>();
//        //        var cardIds = new List<string> {_cardId};
//        //        var result = controller.GetStatusesForUserCards(_userId, cardIds);
//        //        IEnumerable<StarbucksCardStatus> statuses;
//        //        result.TryGetContentValue(out statuses);

//        //        var starbucksCardStatuses = statuses as StarbucksCardStatus[] ?? statuses.ToArray();
//        //        Assert.IsTrue(starbucksCardStatuses.Any());

//        //        var id = starbucksCardStatuses[0].CardId;
//        //        Assert.AreEqual( _cardId, id);
//        //    }

//        //    [TestMethod]
//        //    public void RefreshStarbucksCardBalance()
//        //    {
//        //        var controller = Utility.GetCardController<CardProvider, PaymentMethodProviderFacade>();
//        //        var result = controller.GetStarbucksCardBalance( _userId, _cardId);
//        //        Assert.IsNotNull(result);
//        //        StarbucksCardBalance balance = null;
//        //        result.TryGetContentValue(out balance);
//        //        Assert.IsNotNull(balance);
//        //    }

//        //    [TestMethod]
//        //    public void RefreshStarbucksCardBalanceByCardNumberPin()
//        //    {
//        //        var controller = Utility.GetCardController<CardProvider, PaymentMethodProviderFacade>();
//        //        var result = controller.GetStarbucksCardBalance(_cardNum ,_cardPin );
//        //        Assert.IsNotNull(result);
//        //        StarbucksCardBalance balance = null;
//        //        result.TryGetContentValue(out balance);
//        //        Assert.IsNotNull(balance);
//        //    }

//        //    [TestMethod]
//        //    public void GetStarbucksCardTransaction()
//        //    {
//        //        var controller = Utility.GetCardController<CardProvider, PaymentMethodProviderFacade>();         
//        //        var result = controller.GetStarbucksCardTransaction(_userId, _cardId);           
//        //        Assert.IsNotNull(result);

//        //        IEnumerable<StarbucksCardTransaction> transactions = null;
//        //        result.TryGetContentValue(out transactions);
//        //        Assert.IsTrue(transactions.Any());
//        //    }

//        //    [TestMethod]
//        //    public void RegisterStarbucksCard()
//        //    {

//        //    }

//        //    [TestMethod]
//        //    public void ActivateAndRegisterStarbucksCard()
//        //    {
//        //        var controller = Utility.GetCardController<CardProvider, PaymentMethodProviderFacade>();
//        //    }

//        //    [TestMethod]
//        //    public void RegisterMultipleStarbucksCards()
//        //    {
//        //        var controller = Utility.GetCardController<CardProvider, PaymentMethodProviderFacade>();
//        //    }

//        //    [TestMethod]
//        //    public void UnregisterStarbucksCard()
//        //    {
//        //        var controller = Utility.GetCardController<CardProvider, PaymentMethodProviderFacade>();
//        //    }

//        //    [TestMethod]
//        //    public void ReloadStarbucksCard()
//        //    {
//        //        var controller = Utility.GetCardController<CardProvider, PaymentMethodProviderFacade>();
//        //    }

//        //    [TestMethod]
//        //    public void ReloadStarbucksCardUsingPayPalRefTransaction()
//        //    {
//        //        var controller = Utility.GetCardController<CardProvider, PaymentMethodProviderFacade>();
//        //    }

//        //    [TestMethod]
//        //    public void SetupAutoReload()
//        //    {
//        //        var controller = Utility.GetCardController<CardProvider, PaymentMethodProviderFacade>();
//        //    }

//        //    [TestMethod]
//        //    public void UpdateAutoReload()
//        //    {
//        //        var controller = Utility.GetCardController<CardProvider, PaymentMethodProviderFacade>();
//        //    }

//        //    [TestMethod]
//        //    public void EnableAutoReload()
//        //    {
//        //        var controller = Utility.GetCardController<CardProvider, PaymentMethodProviderFacade>();
//        //        var cardResult = controller.GetCardById(_userId, _cardId);
//        //        StarbucksCard card;
//        //        cardResult.TryGetContentValue(out card);      
//        //        var result = controller.UpdateAutoReload(_userId, _cardId, Utility.GetAutoReloadProfile() );
//        //        AutoReloadProfile autoReloadProfile = null;
//        //        result.TryGetContentValue(out autoReloadProfile);
//        //        Assert.AreEqual(autoReloadProfile.Status, "Enabled");
//        //    }

//        //    [TestMethod]
//        //    public void DisableAutoReload()
//        //    {
//        //        var controller = Utility.GetCardController<CardProvider, PaymentMethodProviderFacade>();
//        //        var cardResult = controller.GetCardById( _userId, _cardId);
//        //        StarbucksCard card;
//        //        cardResult.TryGetContentValue(out card);            
//        //        var result = controller.DisableAutoReload(_userId, card.AutoReloadProfile.AutoReloadId );
//        //        AutoReloadProfile autoReloadProfile = null;
//        //        result.TryGetContentValue(out autoReloadProfile);
//        //        Assert.AreEqual( autoReloadProfile.Status , "Disabled");
//        //    }

//        //    [TestMethod]
//        //    public void TransferStarbucksCardBalance()
//        //    {


//        //    }


//    }
//}
