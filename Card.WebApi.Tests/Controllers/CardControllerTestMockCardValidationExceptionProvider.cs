﻿//using System.Collections.Generic;
//using System.Net;
//using System.Net.Http;
//using System.Web.Http;
//using System.Web.Http.Hosting;
//using Account.Provider.Common;
//using Card.WebApi.Controllers;
//using Card.WebApi.Models;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Moq;
//using Starbucks.Card.Provider.Common.ErrorResources;
//using Starbucks.CardTransaction.Provider.Common;
//using Starbucks.PaymentMethod.Provider.Common;
//using Starbucks.Card.Provider.Common;
//using Starbucks.Card.Provider.Common.Models;
//using Starbucks.OpenApi.ServiceExtensions.Exceptions;
//using Starbucks.Platform.Security;

//namespace Card.WebApi.Tests.Controllers
//{
//    [TestClass ]
//    public class CardControllerTestMockCardValidationExceptionProvider
//    {
//        [TestInitialize]
//        public void Initialize()
//        {
//            Encryption.EncryptCardId(974);
//        }

//        private static CardController MockCardProvider(out Mock<ICardTransactionProvider> mockCardProvider,
//                                                       out Mock<IPaymentMethodProvider> mockPaymentProvider,
//                                                        out Mock<IAccountProvider> mockAccountProvider)
//        {
//            mockCardProvider = new Mock<ICardTransactionProvider>();
//            mockPaymentProvider = new Mock<IPaymentMethodProvider>();
//            mockAccountProvider = new Mock<IAccountProvider>();
//            var controller = new CardController(mockCardProvider.Object)
//                {
//                    Request = new HttpRequestMessage()
//                };
//            var config = new HttpConfiguration();
//            WebApi.WebApiConfig.Register(config);
//            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, config);
//            return controller;
//        }

//        [TestMethod]        
//        public void MockGetCardByNumberAndPinCardValidationExceptionMissingNumber()
//        {
            
//            Mock<ICardTransactionProvider> mockCardProvider;
//            Mock<IPaymentMethodProvider> mockPaymentMethodProvider;
//            Mock<IAccountProvider> mockAccountProvider;
//            CardController controller = MockCardProvider(out mockCardProvider, out mockPaymentMethodProvider , out mockAccountProvider );

//            var exception = new Starbucks.Card.Provider.Common.Exceptions.CardProviderValidationException(Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardNumberMissingMessage
//                , Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardNumberMissingCode );


//            mockCardProvider.Setup(pr => pr.GetCardByNumberAndPin( It.IsAny<string>(), It.IsAny<string>()))
//                            .Throws(exception);
                    
            
//            ApiException apiException = null;
//            try
//            {
//                controller.GetCardByNumberAndPin("123", "123");
//            }
//            catch (ApiException ae)
//            {
//                apiException = ae;
//            }

//            Assert.IsNotNull(apiException);
//            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationCardNumberMissingCode);
//            Assert.AreEqual(apiException.ApiErrorMessage, CardWebAPIErrorResource.ValidationCardNumberMissingMessage);
//            Assert.AreEqual(apiException.StatusCode, HttpStatusCode.BadRequest );
//        }

//        [TestMethod]
//        public void MockGetCardByNumberAndPinCardValidationExceptionMissingPin()
//        {
//            Mock<ICardTransactionProvider> mockCardProvider;
//            Mock<IPaymentMethodProvider> mockPaymentMethodProvider;
//            Mock<IAccountProvider> mockAccountProvider;
//            CardController controller = MockCardProvider(out mockCardProvider, out mockPaymentMethodProvider, out mockAccountProvider);
//            var exception = new Starbucks.Card.Provider.Common.Exceptions.CardProviderValidationException(Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardPinMissingMessage
//                , Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardPinMissingCode);


//            mockCardProvider.Setup(pr => pr.GetCardByNumberAndPin( It.IsAny<string>(), It.IsAny<string>()))
//                            .Throws(exception);

         
            
//            ApiException apiException = null;
//            try
//            {
//                controller.GetCardByNumberAndPin("123", "123");
//            }
//            catch (ApiException ae)
//            {
//                apiException = ae;
//            }

//            Assert.IsNotNull(apiException);
//            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationCardPinMissingCode);
//            Assert.AreEqual(apiException.ApiErrorMessage, CardWebAPIErrorResource.ValidationCardPinMissingMessage);
//            Assert.AreEqual(apiException.StatusCode, HttpStatusCode.BadRequest);
//        }

//        [TestMethod]  
//        public void MockGetCardByCardIdCardException()
//        {
//            Mock<ICardTransactionProvider> mockCardProvider;
//            Mock<IPaymentMethodProvider> mockPaymentMethodProvider;
//            Mock<IAccountProvider> mockAccountProvider;
//            CardController controller = MockCardProvider(out mockCardProvider, out mockPaymentMethodProvider, out mockAccountProvider);

//            var exception = new Starbucks.Card.Provider.Common.Exceptions.CardProviderValidationException(Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardCardIdMissingMessage
//                , Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardCardIdMissingCode);


//            mockCardProvider.Setup(pr => pr.GetCardByCardIdUserId(  It.IsAny<string>(), It.IsAny<string>()))
//                            .Throws(exception);

           
            
//            ApiException apiException = null;
//            try
//            {
//                controller.GetCardById("123", "123");
//            }
//            catch (ApiException ae)
//            {
//                apiException = ae;
//            }

//            Assert.IsNotNull(apiException);
//            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationCardCardIdMissingCode);
//            Assert.AreEqual(apiException.ApiErrorMessage, CardWebAPIErrorResource.ValidationCardCardIdMissingMessage);
//            Assert.AreEqual(apiException.StatusCode, HttpStatusCode.BadRequest);
//        }        

//        [TestMethod]
//        public void MockGetCardImageUrlByCardNumberCardException()
//        {
//            Mock<ICardTransactionProvider> mockCardProvider;
//            Mock<IPaymentMethodProvider> mockPaymentMethodProvider;
//            Mock<IAccountProvider> mockAccountProvider;
//            CardController controller = MockCardProvider(out mockCardProvider, out mockPaymentMethodProvider, out mockAccountProvider);

//            var exception = new Starbucks.Card.Provider.Common.Exceptions.CardProviderValidationException(Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardNumberMissingMessage
//                , Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardNumberMissingCode);


//            mockCardProvider.Setup(pr => pr.GetStarbucksCardImageUrlByCardNumber(  It.IsAny<string>()))
//                            .Throws(exception);

          
            
//            ApiException apiException = null;
//            try
//            {
//                controller.GetCardImageUrlByCardNumber( "123");
//            }
//            catch (ApiException ae)
//            {
//                apiException = ae;
//            }

//            Assert.IsNotNull(apiException);
//            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationCardNumberMissingCode);
//            Assert.AreEqual(apiException.ApiErrorMessage, CardWebAPIErrorResource.ValidationCardNumberMissingMessage);
//            Assert.AreEqual(apiException.StatusCode, HttpStatusCode.BadRequest);
//        }
       
//        [TestMethod]       
//        public void MockRefreshStarbucksCardBalanceCardException()
//        {
//            Mock<ICardTransactionProvider> mockCardProvider;
//            Mock<IPaymentMethodProvider> mockPaymentMethodProvider;
//            Mock<IAccountProvider> mockAccountProvider;
//            CardController controller = MockCardProvider(out mockCardProvider, out mockPaymentMethodProvider, out mockAccountProvider);

//            var exception = new Starbucks.Card.Provider.Common.Exceptions.CardProviderValidationException(Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardCardIdMissingMessage
//                , Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardCardIdMissingCode);


//            mockCardProvider.Setup(pr => pr.GetBalanceRegistered(   It.IsAny<string>(), It.IsAny<string>()))
//                            .Throws(exception);

        
            
//            ApiException apiException = null;
//            try
//            {
//                controller.GetStarbucksCardBalance( "123", "123");
//            }
//            catch (ApiException ae)
//            {
//                apiException = ae;
//            }

//            Assert.IsNotNull(apiException);
//            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationCardCardIdMissingCode);
//            Assert.AreEqual(apiException.ApiErrorMessage, CardWebAPIErrorResource.ValidationCardCardIdMissingMessage);
//            Assert.AreEqual(apiException.StatusCode, HttpStatusCode.BadRequest);
//        }

//        [TestMethod]        
//        public void MockRefreshStarbucksCardBalanceByCardNumberPinCardExceptionMissingNumber()
//        {
//            Mock<ICardTransactionProvider> mockCardProvider;
//            Mock<IPaymentMethodProvider> mockPaymentMethodProvider;
//            Mock<IAccountProvider> mockAccountProvider;
//            CardController controller = MockCardProvider(out mockCardProvider, out mockPaymentMethodProvider, out mockAccountProvider);

//            var exception = new Starbucks.Card.Provider.Common.Exceptions.CardProviderValidationException(Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardNumberMissingMessage
//                , Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardNumberMissingCode);


//            mockCardProvider.Setup(pr => pr.GetBalanceByNumber(  It.IsAny<string>(), It.IsAny<string>()))
//                            .Throws(exception);
         
            
//            ApiException apiException = null;
//            try
//            {
//                controller.GetStarbucksCardBalanceByCardNumber( "123", "123");
//            }
//            catch (ApiException ae)
//            {
//                apiException = ae;
//            }

//            Assert.IsNotNull(apiException);
//            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationCardNumberMissingCode);
//            Assert.AreEqual(apiException.ApiErrorMessage, CardWebAPIErrorResource.ValidationCardNumberMissingMessage);
//            Assert.AreEqual(apiException.StatusCode, HttpStatusCode.BadRequest);
//        }

//        [TestMethod]
//        public void MockRefreshStarbucksCardBalanceByCardNumberPinCardExceptionMissingPin()
//        {
//            Mock<ICardTransactionProvider> mockCardProvider;
//            Mock<IPaymentMethodProvider> mockPaymentMethodProvider;
//            Mock<IAccountProvider> mockAccountProvider;
//            CardController controller = MockCardProvider(out mockCardProvider, out mockPaymentMethodProvider, out mockAccountProvider);

//            var exception = new Starbucks.Card.Provider.Common.Exceptions.CardProviderValidationException(Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardPinMissingMessage
//                , Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardPinMissingCode);


//            mockCardProvider.Setup(pr => pr.GetBalanceByNumber( It.IsAny<string>(), It.IsAny<string>()))
//                            .Throws(exception);
           
            
//            ApiException apiException = null;
//            try
//            {
//                controller.GetStarbucksCardBalanceByCardNumber("123", "123");
//            }
//            catch (ApiException ae)
//            {
//                apiException = ae;
//            }

//            Assert.IsNotNull(apiException);
//            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationCardPinMissingCode);
//            Assert.AreEqual(apiException.ApiErrorMessage, CardWebAPIErrorResource.ValidationCardPinMissingMessage);
//            Assert.AreEqual(apiException.StatusCode, HttpStatusCode.BadRequest);
//        }

//        [TestMethod]
//        public void MockRegisterCardExceptionMissingNumber()
//        {
//            Mock<ICardTransactionProvider> mockCardProvider;
//            Mock<IPaymentMethodProvider> mockPaymentMethodProvider;
//            Mock<IAccountProvider> mockAccountProvider;
//            CardController controller = MockCardProvider(out mockCardProvider, out mockPaymentMethodProvider, out mockAccountProvider);

//            var exception = new Starbucks.Card.Provider.Common.Exceptions.CardProviderValidationException(Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardNumberMissingMessage
//                , Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardNumberMissingCode);


//            mockCardProvider.Setup(pr => pr.RegisterStarbucksCard(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
//                            .Throws(exception);

        
            
//            ApiException apiException = null;
//            try
//            {
//                controller.RegisterStarbucksCard( "123", new StarbucksCardNumberAndPin() {CardNumber = "123", Pin= "23"});
//            }
//            catch (ApiException ae)
//            {
//                apiException = ae;
//            }

//            Assert.IsNotNull(apiException);
//            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationCardNumberMissingCode);
//            Assert.AreEqual(apiException.ApiErrorMessage, CardWebAPIErrorResource.ValidationCardNumberMissingMessage);
//            Assert.AreEqual(apiException.StatusCode, HttpStatusCode.BadRequest);
//        }

//        [TestMethod]
//        public void MockRegisterExceptionMissingPin()
//        {
//            Mock<ICardTransactionProvider> mockCardProvider;
//            Mock<IPaymentMethodProvider> mockPaymentMethodProvider;
//            Mock<IAccountProvider> mockAccountProvider;
//            CardController controller = MockCardProvider(out mockCardProvider, out mockPaymentMethodProvider, out mockAccountProvider);

//            var exception = new Starbucks.Card.Provider.Common.Exceptions.CardProviderValidationException(Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardPinMissingMessage
//                , Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardPinMissingCode);


//            mockCardProvider.Setup(pr => pr.RegisterStarbucksCard(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
//                               .Throws(exception);
         
            
//            ApiException apiException = null;
//            try
//            {
//                controller.RegisterStarbucksCard("123", new StarbucksCardNumberAndPin() { CardNumber = "123", Pin = "23" });
//            }
//            catch (ApiException ae)
//            {
//                apiException = ae;
//            }
//            Assert.IsNotNull(apiException);
//            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationCardPinMissingCode);
//            Assert.AreEqual(apiException.ApiErrorMessage, CardWebAPIErrorResource.ValidationCardPinMissingMessage);
//            Assert.AreEqual(apiException.StatusCode, HttpStatusCode.BadRequest);
//        }

//        [TestMethod]        
//        public void MockActivateAndRegisterStarbucksCardCardException()
//        {
//            Mock<ICardTransactionProvider> mockCardProvider;
//            Mock<IPaymentMethodProvider> mockPaymentMethodProvider;
//            Mock<IAccountProvider> mockAccountProvider;
//            CardController controller = MockCardProvider(out mockCardProvider, out mockPaymentMethodProvider, out mockAccountProvider);

//            var exception = new Starbucks.Card.Provider.Common.Exceptions.CardProviderValidationException(Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardNoUserMessage
//                , Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardNoUserCode);


//            mockCardProvider.Setup(pr => pr.ActivateAndRegisterCard( It.IsAny<string>()))
//                               .Throws(exception);
           
            
//            ApiException apiException = null;
//            try
//            {
//                controller.ActivateAndRegisterStarbucksCard("123");
//            }
//            catch (ApiException ae)
//            {
//                apiException = ae;
//            }
//            Assert.IsNotNull(apiException);
//            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationCardNoUserCode);
//            Assert.AreEqual(apiException.ApiErrorMessage, CardWebAPIErrorResource.ValidationCardNoUserMessage);
//            Assert.AreEqual(apiException.StatusCode, HttpStatusCode.BadRequest);
//        }

//        [TestMethod]       
//        public void MockRegisterMultipleStarbucksCardsCardExceptionMissingPin()
//        {
//            Mock<ICardTransactionProvider> mockCardProvider;
//            Mock<IPaymentMethodProvider> mockPaymentMethodProvider;
//            Mock<IAccountProvider> mockAccountProvider;
//            CardController controller = MockCardProvider(out mockCardProvider, out mockPaymentMethodProvider, out mockAccountProvider);

//            var exception = new Starbucks.Card.Provider.Common.Exceptions.CardProviderValidationException(Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardPinMissingMessage
//                , Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardPinMissingCode);


//            mockCardProvider.Setup(pr => pr.RegisterMultipleCards(  It.IsAny<string>(), It.IsAny<IEnumerable<IStarbucksCardNumberAndPin>>()))
//                               .Throws(exception);

            
            
//            ApiException apiException = null;
//            try
//            {
//                controller.RegisterMultipleStarbucksCards( "123", new List<StarbucksCardNumberAndPin>{new StarbucksCardNumberAndPin() { CardNumber = "123", Pin = "23" }});
//            }
//            catch (ApiException ae)
//            {
//                apiException = ae;
//            }
//            Assert.IsNotNull(apiException);
//            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationCardPinMissingCode);
//            Assert.AreEqual(apiException.ApiErrorMessage, CardWebAPIErrorResource.ValidationCardPinMissingMessage);
//            Assert.AreEqual(apiException.StatusCode, HttpStatusCode.BadRequest);
//        }

//        [TestMethod]
//        public void MockRegisterMultipleStarbucksCardsCardExceptionMissingNumber()
//        {
//            Mock<ICardTransactionProvider> mockCardProvider;
//            Mock<IPaymentMethodProvider> mockPaymentMethodProvider;
//            Mock<IAccountProvider> mockAccountProvider;
//            CardController controller = MockCardProvider(out mockCardProvider, out mockPaymentMethodProvider, out mockAccountProvider);

//            var exception = new Starbucks.Card.Provider.Common.Exceptions.CardProviderValidationException(Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardNumberMissingMessage
//                , Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardNumberMissingCode);


//            mockCardProvider.Setup(pr => pr.RegisterMultipleCards( It.IsAny<string>(), It.IsAny<IEnumerable<IStarbucksCardNumberAndPin>>())).Throws(exception);
            
            
//            ApiException apiException = null;
//            try
//            {
//                controller.RegisterMultipleStarbucksCards("123", new List<StarbucksCardNumberAndPin> { new StarbucksCardNumberAndPin() { CardNumber = "123", Pin = "23" } });
//            }
//            catch (ApiException ae)
//            {
//                apiException = ae;
//            }
//            Assert.IsNotNull(apiException);
//            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationCardNumberMissingCode);
//            Assert.AreEqual(apiException.ApiErrorMessage, CardWebAPIErrorResource.ValidationCardNumberMissingMessage);
//            Assert.AreEqual(apiException.StatusCode, HttpStatusCode.BadRequest);
//        }

//        [TestMethod]
//        public void MockUnregisterStarbucksCardCardException()
//        {
//            Mock<ICardTransactionProvider> mockCardProvider;
//            Mock<IPaymentMethodProvider> mockPaymentMethodProvider;
//            Mock<IAccountProvider> mockAccountProvider;
//            CardController controller = MockCardProvider(out mockCardProvider, out mockPaymentMethodProvider, out mockAccountProvider);

//            var exception = new Starbucks.Card.Provider.Common.Exceptions.CardProviderValidationException(Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardCardIdMissingMessage
//                , Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardCardIdMissingCode);


//            mockCardProvider.Setup(pr => pr.UnregisterStarbucksCard(It.IsAny<string>(), It.IsAny<string>(), It.IsAny< bool>())).Throws(exception);
            
            
//            ApiException apiException = null;
//            try
//            {
//                controller.UnregisterStarbucksCard( "123", "123");
//            }
//            catch (ApiException ae)
//            {
//                apiException = ae;
//            }
//            Assert.IsNotNull(apiException);
//            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationCardCardIdMissingCode);
//            Assert.AreEqual(apiException.ApiErrorMessage, CardWebAPIErrorResource.ValidationCardCardIdMissingMessage);
//            Assert.AreEqual(apiException.StatusCode, HttpStatusCode.BadRequest);
//        }

//        [TestMethod]
//        public void MockUnregisterStarbucksCardCardExceptionUndefined()
//        {
//            Mock<ICardTransactionProvider> mockCardProvider;
//            Mock<IPaymentMethodProvider> mockPaymentMethodProvider;
//            Mock<IAccountProvider> mockAccountProvider;
//            CardController controller = MockCardProvider(out mockCardProvider, out mockPaymentMethodProvider, out mockAccountProvider);



//            var exception = new Starbucks.Card.Provider.Common.Exceptions.CardProviderException(CardProviderErrorResource.UnregisterNonZeroBalanceCode, CardProviderErrorResource.UnregisterNonZeroBalanceCode);


//            mockCardProvider.Setup(pr => pr.UnregisterStarbucksCard(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).Throws(exception);
            
            
//            ApiException apiException = null;
//            try
//            {
//                controller.UnregisterStarbucksCard("123", "123");
//            }
//            catch (ApiException ae)
//            {
//                apiException = ae;
//            }
//            Assert.IsNotNull(apiException);
//            //Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.);
//            //Assert.AreEqual(apiException.ApiErrorMessage, CardWebAPIErrorResource.ValidationCardCardIdMissingMessage);
//            Assert.AreEqual(apiException.StatusCode, HttpStatusCode.BadRequest);
//        }

//        [TestMethod]
//        public void GetStatusesForUserCardsValidationException()
//        {
//            Mock<ICardTransactionProvider> mockCardProvider;
//            Mock<IPaymentMethodProvider> mockPaymentMethodProvider;
//            Mock<IAccountProvider> mockAccountProvider;
//            CardController controller = MockCardProvider(out mockCardProvider, out mockPaymentMethodProvider, out mockAccountProvider);

//            var exception = new Starbucks.Card.Provider.Common.Exceptions.CardProviderValidationException(Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardNoUserMessage
//                , Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardNoUserCode);


//            mockCardProvider.Setup(pr => pr.GetStatusesForUserCards( It.IsAny<string>(), It.IsAny<IEnumerable<string>>())).Throws(exception);
            
           
//            ApiException apiException = null;
//            try
//            {
//                controller.GetStatusesForUserCards("123", new List<string>{ "123"});
//            }
//            catch (ApiException ae)
//            {
//                apiException = ae;
//            }
//            Assert.IsNotNull(apiException);
//            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationCardNoUserCode);
//            Assert.AreEqual(apiException.ApiErrorMessage, CardWebAPIErrorResource.ValidationCardNoUserMessage);
//            Assert.AreEqual(apiException.StatusCode, HttpStatusCode.BadRequest);
//        }

//        [TestMethod]        
//        public void MockDisableAutoReloadValidationExcetionNoUser()
//        {
//            Mock<ICardTransactionProvider> mockCardProvider;
//            Mock<IPaymentMethodProvider> mockPaymentMethodProvider;
//            Mock<IAccountProvider> mockAccountProvider;
//            CardController controller = MockCardProvider(out mockCardProvider, out mockPaymentMethodProvider, out mockAccountProvider);

//            var exception = new Starbucks.Card.Provider.Common.Exceptions.CardProviderValidationException(Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardNoUserMessage
//                , Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardNoUserCode);


//            mockCardProvider.Setup(pr => pr.DisableAutoReload(It.IsAny<string>(), It.IsAny<string>())).Throws(exception);
            
            
//            ApiException apiException = null;
//            try
//            {
//                controller.DisableAutoReload("123", "123");
//            }
//            catch (ApiException ae)
//            {
//                apiException = ae;
//            }
//            Assert.IsNotNull(apiException);
//            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationCardNoUserCode);
//            Assert.AreEqual(apiException.ApiErrorMessage, CardWebAPIErrorResource.ValidationCardNoUserMessage);
//            Assert.AreEqual(apiException.StatusCode, HttpStatusCode.BadRequest);
//        }

//        [TestMethod]
//        public void MockGetCardsByIdValidationException()
//        {
//             Mock<ICardTransactionProvider> mockCardProvider;
//            Mock<IPaymentMethodProvider> mockPaymentMethodProvider;
//            Mock<IAccountProvider> mockAccountProvider;
//            CardController controller = MockCardProvider(out mockCardProvider, out mockPaymentMethodProvider, out mockAccountProvider);

//            var exception = new Starbucks.Card.Provider.Common.Exceptions.CardProviderValidationException(Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardNoUserMessage
//                , Starbucks.Card.Provider.Common.ErrorResources.CardProviderValidationErrorResource.ValidationCardNoUserCode);


//            mockCardProvider.Setup(pr => pr.GetStatusesForUserCards(It.IsAny<string>(),  It.IsAny<List<string>>())).Throws(exception);
           
            
//            ApiException apiException = null;
//            try
//            {
//                controller.GetCardsByUserId( "123");
//            }
//            catch (ApiException ae)
//            {
//                apiException = ae;
//            }
//            Assert.IsNotNull(apiException);
//            Assert.AreEqual(apiException.ApiErrorCode, CardWebAPIErrorResource.ValidationCardNoUserCode);
//            Assert.AreEqual(apiException.ApiErrorMessage, CardWebAPIErrorResource.ValidationCardNoUserMessage);
//            Assert.AreEqual(apiException.StatusCode, HttpStatusCode.BadRequest);
//        }

//        //[TestMethod]
//        //[ExpectedException(typeof(ApiException))]
//        //public void MockReloadStarbucksCardByNumberException()
//        //{
//        //    CardController controller = Utility.GetCardController<MockCardProviderCardException>();
//        //    var result = controller.ReloadCardByCardNumber( _cardNum , _cardPin , new ReloadForStarbucksCard() { Amount = 100, PaymentMethodId = "1" });

//        //    StarbucksCardBalance  transaction;
//        //    result.TryGetContentValue(out transaction);
//        //    Assert.IsNotNull(transaction);


//        //}

//        ////[TestMethod]
//        ////public void MockReloadStarbucksCardUsingPayPalRefTransaction()
//        ////{
//        ////    CardController controller = Utility.GetCardController<MockCardProvider>();
//        ////    var result = controller.ReloadStarbucksCardUsingPayPalRefTransaction( _userId, _cardId,
//        ////                                                new ReloadForStarbucksCard()
//        ////                                                {
//        ////                                                    Amount = 200,
//        ////                                                    CardBalance = 100,
//        ////                                                    PaymentMethodId = "1",
//        ////                                                    CardBalanceCurrency = "USD",
//        ////                                                    ClientSessionId = "123",
//        ////                                                    Type = "A"
//        ////                                                });

//        ////    StarbucksCardTransaction transaction;
//        ////    result.TryGetContentValue(out transaction);
//        ////    Assert.IsNotNull(transaction);
//        ////}

//        //[TestMethod]
//        //[ExpectedException(typeof(ApiException))]
//        //public void MockSetupAutoReloadCardException()
//        //{
//        //    CardController controller = Utility.GetCardController<MockCardProviderCardException>();
//        //    var result = controller.CreateAutoReload(_userId, _cardId, Utility.GetAutoReloadProfile());
//        //    AutoReloadProfile autoReloadProfile;
//        //    result.TryGetContentValue(out autoReloadProfile);
//        //    Assert.IsNotNull(autoReloadProfile);
//        //}

//        //[TestMethod]
//        //[ExpectedException(typeof(ApiException))]
//        //public void MockUpdateAutoReloadCardException()
//        //{
//        //    CardController controller = Utility.GetCardController<MockCardProviderCardException>();
//        //    var result = controller.UpdateAutoReload(_userId, _cardId, Utility.GetAutoReloadProfile());
//        //    AutoReloadProfile autoReloadProfile;
//        //    result.TryGetContentValue(out autoReloadProfile);
//        //    Assert.IsNotNull(autoReloadProfile);
//        //}

//        //[TestMethod]
//        //[ExpectedException(typeof(ApiException))]
//        //public void MockEnableAutoReloadCardException()
//        //{
//        //    CardController controller = Utility.GetCardController<MockCardProviderCardException>();

//        //    HttpResponseMessage result = controller.EnableAutoReload(_userId, _cardId);
//        //    AutoReloadProfile autoReloadProfile = null;
//        //    result.TryGetContentValue(out autoReloadProfile);
//        //    Assert.AreEqual(autoReloadProfile.Status, "Enabled");
//        //}

//        //[TestMethod]
//        //[ExpectedException(typeof(ApiException))]
//        //public void MockDisableAutoReloadCardException()
//        //{
//        //    CardController controller = Utility.GetCardController<MockCardProviderCardException>();
//        //    HttpResponseMessage cardResult = controller.GetCardById(_userId, _cardId);

//        //    HttpResponseMessage result = controller.DisableAutoReload(_userId, "123");
//        //    AutoReloadProfile autoReloadProfile = null;
//        //    result.TryGetContentValue(out autoReloadProfile);
//        //    Assert.IsNotNull(autoReloadProfile);
//        //}

//        //[TestMethod]
//        //[ExpectedException(typeof(ApiException))]
//        //public void MockTransferStarbucksCardBalanceCardException()
//        //{
//        //    CardController controller = Utility.GetCardController<MockCardProviderCardException>();
//        //    var balanceTransfer = new BalanceTransfer() { Amount = 100, SourceCard = "123", DestinationCard = "456" };
//        //    var result = controller.TransferStarbucksCardBalance(_userId, balanceTransfer);
//        //    StarbucksCardBalance transaction;
//        //    result.TryGetContentValue(out transaction);
//        //    Assert.IsNotNull(transaction);
//        //}      
//    }
//}
