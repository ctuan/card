﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using Card.WebApi.Controllers;
using Card.WebApi.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.Card.Dal.Sql.EntLibLess;
using Starbucks.Card.Dal.Sql.EntLibLess.Configuration;
using Starbucks.Card.Provider;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.Platform.Security;

namespace Card.WebApi.Tests.Controllers
{
    [TestClass]
    public class SelectedCardControllerTest
    {
        private const string UserId = "BA966072-8E56-445C-97FE-7018A8B6A81B";

        private const string ClientId = "qwerty";
        private static SelectedCardController _controller;
        private readonly int[] _testCards = new[] { 80220633, 80220634 };

        private IEnumerable<string> TestCards
        {
            get { return _testCards.Select(Encryption.EncryptCardId); }
        }

        [TestInitialize]
        public void Initialize()
        {
            BuildUpMockController();
        }

        [TestCleanup]
        public void Cleanup()
        {
            _controller.RemoveSelectedCards(UserId, ClientId, new SelectedCardsRequest { Cards = TestCards });
        }

        private static void BuildUpMockController()
        {
            var settingsSection =
                ConfigurationManager.GetSection("cardDalEntLibLessSettings") as CardDalEntLibLessSqlSettingsSection;
            var selectedCardDal = new SelectedCardDal(settingsSection);
            var selectedCardProvider = new SelectedCardProvider(selectedCardDal);

            _controller = new SelectedCardController(selectedCardProvider, selectedCardDal)
                {
                    Request = new HttpRequestMessage()
                };
            var config = new HttpConfiguration();
            WebApiConfig.Register(config);
            _controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, config);
        }

        [TestMethod]
        public void CanRemoveSelectedCards()
        {
            var selectedCardsRequest = new SelectedCardsRequest { Cards = TestCards };
            _controller.AssociateSelectedCards(UserId, ClientId, selectedCardsRequest);
            SelectedCardsRequest request = selectedCardsRequest;
            HttpResponseMessage result = _controller.RemoveSelectedCards(UserId, ClientId, request);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void CanAssociateSelectedCards()
        {
            var request = new SelectedCardsRequest { Cards = new[] { TestCards.First() } };
            HttpResponseMessage result = _controller.AssociateSelectedCards(UserId, ClientId, request);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);

            result = _controller.GetSelectedCards(UserId, ClientId);
            IEnumerable<int> data;
            if (result.TryGetContentValue(out data))
            {
                Assert.AreEqual(data.First(), request.Cards.First());
            }
        }

        [TestMethod]
        public void CanUpdateSelectedCards()
        {
            var request = new SelectedCardsRequest { Cards = new[] { TestCards.First() } };
            _controller.AssociateSelectedCards(UserId, ClientId, request);
            var updateRequest = new SelectedCardsRequest
                {
                    Cards = new[] { TestCards.Last() }
                };
            HttpResponseMessage result = _controller.AssociateSelectedCards(UserId, ClientId, updateRequest);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);

            result = _controller.GetSelectedCards(UserId, ClientId);
            IEnumerable<string> data;
            if (result.TryGetContentValue(out data))
            {
                Assert.AreEqual(data.First(), updateRequest.Cards.First());
            }
        }

        [TestMethod]
        public void CanUpsertSelectedCards()
        {
            var request = new SelectedCardsRequest { Cards = new[] { TestCards.First() } };
            _controller.AssociateSelectedCards(UserId, ClientId, request);
            var updateRequest = new SelectedCardsRequest { Cards = TestCards };
            HttpResponseMessage result = _controller.AssociateSelectedCards(UserId, ClientId, updateRequest);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);

            result = _controller.GetSelectedCards(UserId, ClientId);
            IEnumerable<string> data;
            if (result.TryGetContentValue(out data))
            {
                Assert.IsTrue(TestCards.All(c => data.Contains(c)));
            }
        }

        [TestMethod]
        public void CanGetSelectedCards()
        {
            CanAssociateSelectedCards();
            HttpResponseMessage result = _controller.GetSelectedCards(UserId, ClientId);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
            IEnumerable<int> data;
            if (result.TryGetContentValue(out data))
            {
                Assert.AreEqual(data.First(), TestCards.First());
            }
        }

        [TestMethod]
        public void CanGetAllSelectedCards()
        {
            var request = new SelectedCardsRequest { Cards = TestCards };
            _controller.AssociateSelectedCards(UserId, ClientId, request);
            HttpResponseMessage result = _controller.GetSelectedCards(UserId);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
            IEnumerable<ISelectedCard> data;
            if (result.TryGetContentValue(out data))
            {
                Assert.IsTrue(data.Count() == 2);
                Assert.IsTrue(data.All(c => TestCards.Any(tc => tc == c.CardId)));
            }
        }

        [TestMethod]
        public void CanVerifyIfCardIsLinked()
        {
            CanAssociateSelectedCards();
            HttpResponseMessage result = _controller.IsCardLinked(UserId, TestCards.First());

            Assert.IsNotNull(result);
            Assert.IsTrue(result.IsSuccessStatusCode);
            bool data;
            if (result.TryGetContentValue(out data))
            {
                Assert.IsTrue(data);
            }
        }
    }
}