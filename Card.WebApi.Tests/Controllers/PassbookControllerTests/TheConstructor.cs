﻿using System;
using Account.Provider.Common;
using Card.WebApi.Controllers;
using Card.WebApi.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.Card.Provider.Common;
using Starbucks.Common.Tests.Utilities;
using Starbucks.OpenApi.Utilities.Common.Helpers;
using Starbucks.OpenApi.WebApi.Common.Helpers;
using Starbucks.Settings.Provider.Common;

namespace Card.WebApi.Tests.Controllers.PassbookControllerTests
{
    [TestClass]
    public class TheConstructor : TestBase
    {
        [TestMethod]
        public void WillThrowArgumentNullExceptionIfLogCounterWrapperIsNull()
        {
            var exception = AssertException<ArgumentNullException>(
                () => new PassbookController(null, It.IsAny<ILogHelper>(), 
                    It.IsAny<ICardProvider>(), It.IsAny<IAccountProvider>(), It.IsAny<ILocationHelper>(), 
                    It.IsAny<IRequestHelper>(),  It.IsAny<ICardDal>(), It.IsAny<ISettingsProvider>(),It.IsAny<IPassbookService>()));

            AssertString("logCounterWrapper", exception.ParamName);
        }

        [TestMethod]
        public void WillThrowArgumentNullExceptionIfLogHelperIsNull()
        {
            var exception = AssertException<ArgumentNullException>(
                () => new PassbookController(new Mock<ILogCounterWrapper>().Object, null, 
                   It.IsAny<ICardProvider>(), It.IsAny<IAccountProvider>(), 
                    It.IsAny<ILocationHelper>(), It.IsAny<IRequestHelper>(), 
                    It.IsAny<ICardDal>(),It.IsAny<ISettingsProvider>(),It.IsAny<IPassbookService>()));

            AssertString("logHelper", exception.ParamName);
        }       

        [TestMethod]
        public void WillThrowArgumentNullExceptionIfCardProviderIsNull()
        {
            var exception = AssertException<ArgumentNullException>(
                () => new PassbookController(new Mock<ILogCounterWrapper>().Object,
                    new Mock<ILogHelper>().Object, 
                    null, It.IsAny<IAccountProvider>(), It.IsAny<ILocationHelper>(), 
                    It.IsAny<IRequestHelper>(), 
                    It.IsAny<ICardDal>(), It.IsAny<ISettingsProvider>(),It.IsAny<IPassbookService>()));

            AssertString("cardProvider", exception.ParamName);
        }

        [TestMethod]
        public void WillThrowArgumentNullExceptionIfAccountProviderIsNull()
        {
            var exception = AssertException<ArgumentNullException>(
                () => new PassbookController(new Mock<ILogCounterWrapper>().Object,
                    new Mock<ILogHelper>().Object, 
                    new Mock<ICardProvider>().Object, null, It.IsAny<ILocationHelper>(), 
                    It.IsAny<IRequestHelper>(), 
                    It.IsAny<ICardDal>(), It.IsAny<ISettingsProvider>(),It.IsAny<IPassbookService>()));

            AssertString("accountProvider", exception.ParamName);
        }

        [TestMethod]
        public void WillThrowArgumentNullExceptionIfLocationHelperIsNull()
        {
            var exception = AssertException<ArgumentNullException>(
                () => new PassbookController(new Mock<ILogCounterWrapper>().Object,
                    new Mock<ILogHelper>().Object, 
                    new Mock<ICardProvider>().Object, new Mock<IAccountProvider>().Object, null, 
                    It.IsAny<IRequestHelper>(), 
                    It.IsAny<ICardDal>(), It.IsAny<ISettingsProvider>(),It.IsAny<IPassbookService>()));

            AssertString("locationHelper", exception.ParamName);
        }

        [TestMethod]
        public void WillThrowArgumentNullExceptionIfRequestHelperIsNull()
        {
            var exception = AssertException<ArgumentNullException>(
                () => new PassbookController(new Mock<ILogCounterWrapper>().Object,
                    new Mock<ILogHelper>().Object, 
                    new Mock<ICardProvider>().Object, new Mock<IAccountProvider>().Object,
                    new Mock<ILocationHelper>().Object, null,
                    It.IsAny<ICardDal>(), It.IsAny<ISettingsProvider>(),It.IsAny<IPassbookService>()));

            AssertString("requestHelper", exception.ParamName);
        }


        [TestMethod]
        public void WillThrowArgumentNullExceptionIfCardDalIsNull()
        {
            var exception = AssertException<ArgumentNullException>(
                () => new PassbookController(new Mock<ILogCounterWrapper>().Object,
                    new Mock<ILogHelper>().Object, 
                    new Mock<ICardProvider>().Object, new Mock<IAccountProvider>().Object,
                    new Mock<ILocationHelper>().Object, new Mock<IRequestHelper>().Object,
                     null, It.IsAny<ISettingsProvider>(),It.IsAny<IPassbookService>()));

            AssertString("cardDal", exception.ParamName);
        }
    }
}
