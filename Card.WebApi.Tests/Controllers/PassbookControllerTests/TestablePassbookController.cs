﻿using Account.Provider.Common;
using Card.WebApi.Controllers;
using Card.WebApi.Helpers;
using Moq;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.Card.Provider.Common;
using Starbucks.OpenApi.Utilities.Common.Helpers;
using Starbucks.OpenApi.WebApi.Common.Helpers;
using Starbucks.Settings.Provider.Common;

namespace Card.WebApi.Tests.Controllers.PassbookControllerTests
{
    public class TestablePassbookController : PassbookController
    {
        public Mock<ILogCounterWrapper> LogCounterWrapper { get; private set; }
        public Mock<ILogHelper> LogHelper { get; private set; }
        public Mock<ICardProvider> CardProvider { get; private set; }
        public Mock<IAccountProvider> AccountProvider { get; private set; }
        public Mock<ILocationHelper> LocationHelper { get; private set; }
        public Mock<IRequestHelper> RequestHelper { get; private set; }
        public Mock<IConverterWrapper> ConverterWrapper { get; private set; }
        public Mock<ICardDal> CardDal { get; private set; }
        public Mock<ISettingsProvider> SettingsProvider { get; private set; }
        public Mock<IPassbookService> PassbookService { get; private set; }

        public TestablePassbookController()
            : this(new Mock<ILogCounterWrapper>(), new Mock<ILogHelper>(),
             new Mock<ICardProvider>(), new Mock<IAccountProvider>(),
            new Mock<ILocationHelper>(), new Mock<IRequestHelper>(), new Mock<IConverterWrapper>(),
            new Mock<ICardDal>(), new Mock<ISettingsProvider>(), new Mock<IPassbookService>())
        {
        }

        private TestablePassbookController(Mock<ILogCounterWrapper> logCounterWrapper, Mock<ILogHelper> logHelper,
             Mock<ICardProvider> cardProvider, Mock<IAccountProvider> accountProvider,
            Mock<ILocationHelper> locationHelper, Mock<IRequestHelper> requestHelper, Mock<IConverterWrapper> converterWrapper,
            Mock<ICardDal> cardDal, Mock<ISettingsProvider> settingsProvider, Mock<IPassbookService> passbookService)
            : base(logCounterWrapper.Object, logHelper.Object, cardProvider.Object,
                accountProvider.Object, locationHelper.Object, requestHelper.Object,
                cardDal.Object, settingsProvider.Object, passbookService.Object)
        {
            LogCounterWrapper = logCounterWrapper;
            LogHelper = logHelper;
            CardProvider = cardProvider;
            AccountProvider = accountProvider;
            LocationHelper = locationHelper;
            RequestHelper = requestHelper;
            ConverterWrapper = converterWrapper;
            CardDal = cardDal;
            SettingsProvider = settingsProvider;
            PassbookService = passbookService;
        }

        public new void ValidateLocaleAndMarket(string market, string locale)
        {
            base.ValidateLocaleAndMarket(market, locale);
        }
    }
}
