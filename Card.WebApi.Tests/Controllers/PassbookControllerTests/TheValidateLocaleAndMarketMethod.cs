﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Starbucks.Common.Tests.Utilities;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;
using Starbucks.OpenApi.Utilities.Common.Exceptions;

namespace Card.WebApi.Tests.Controllers.PassbookControllerTests
{
    [TestClass]
    public class TheValidateLocaleAndMarketMethod 
        : TestBase<TestablePassbookController>
    {
        [TestMethod]
        public void WillThrowArgumentNullExceptionIfMarketIsNull()
        {
            ExecuteTest(() =>
                {
                    var exception = AssertException<ArgumentNullException>(
                        () => ItemInTest.ValidateLocaleAndMarket(null, It.IsAny<string>()));

                    AssertString("market", exception.ParamName);
                });
        }

        [TestMethod]
        public void WillThrowArgumentEmptyExceptionIfMarketIsEmpty()
        {
            ExecuteTest(() =>
                {
                    var exception = AssertException<ArgumentEmptyException>(
                        () => ItemInTest.ValidateLocaleAndMarket(String.Empty, It.IsAny<string>()));

                    AssertString("market", exception.ParamName);
                });
        }

        [TestMethod]
        public void WillThrowArgumentNullExceptionIfLocaleIsNull()
        {
            ExecuteTest(() =>
                {
                    var exception = AssertException<ArgumentNullException>(
                        () => ItemInTest.ValidateLocaleAndMarket(DataGenerator.GenerateString(),
                            null));

                    AssertString("locale", exception.ParamName);
                });
        }

        [TestMethod]
        public void WillThrowArgumentEmptyExceptionIfLocaleIsEmpty()
        {
            ExecuteTest(() =>
                {
                    var exception = AssertException<ArgumentEmptyException>(
                        () => ItemInTest.ValidateLocaleAndMarket(DataGenerator.GenerateString(), 
                            String.Empty));

                    AssertString("locale", exception.ParamName);
                });
        }

        [TestMethod]
        public void WillInvokeSettingsProviderGetCurrencyCodeForMarketCode()
        {
            var locale = DataGenerator.GenerateString();
            SetupAndExecuteTest(() =>
                {
                    Setup();

                    ItemInTest.SettingsProvider
                        .Setup(x => x.GetCurrencyCodeForMarketCode(It.IsAny<string>()))
                        .Returns(DataGenerator.GenerateString());

                    ItemInTest.SettingsProvider
                        .Setup(x => x.GetLocaleByMarket(It.IsAny<string>()))
                        .Returns(new List<string>() { locale });
                }, () =>
                {
                    ItemInTest.ValidateLocaleAndMarket(DataGenerator.GenerateString(), locale);

                    ItemInTest.SettingsProvider
                        .Verify(x => x.GetCurrencyCodeForMarketCode(It.IsAny<string>()), 
                            Times.Once());
                });
        }
        
        [TestMethod]
        public void WillThrowApiExceptionIfMarketIsNotFound()
        {
            SetupAndExecuteTest(() =>
                {
                    Setup();

                    ItemInTest.SettingsProvider
                        .Setup(x => x.GetCurrencyCodeForMarketCode(It.IsAny<string>()))
                        .Returns((String)null);
                }, () =>
                {
                    var exception = AssertException<ApiException>(
                        () => ItemInTest.ValidateLocaleAndMarket(DataGenerator.GenerateString(), DataGenerator.GenerateString()));

                    AssertString("Invalid market provided.", exception.ApiErrorMessage);
                    AssertString("121054", exception.ApiErrorCode);
                    Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
                });
        }

        [TestMethod]
        public void WillInvokeSettingsProviderGetLocaleByMarket()
        {
            var locale = DataGenerator.GenerateString();
            SetupAndExecuteTest(() =>
                {
                    Setup();

                    ItemInTest.SettingsProvider
                        .Setup(x => x.GetCurrencyCodeForMarketCode(It.IsAny<string>()))
                        .Returns(DataGenerator.GenerateString());

                    ItemInTest.SettingsProvider
                        .Setup(x => x.GetLocaleByMarket(It.IsAny<string>()))
                        .Returns(new List<string>(){locale });
                }, () =>
                {
                    ItemInTest.ValidateLocaleAndMarket(DataGenerator.GenerateString(), locale);

                    ItemInTest.SettingsProvider
                        .Verify(x => x.GetLocaleByMarket(It.IsAny<string>()), Times.Once());
                });
        }

        [TestMethod]
        public void WillThrowApiExceptionIfLocaleIsNotFoundForMarket()
        {
            SetupAndExecuteTest(() =>
                {
                    Setup();

                    ItemInTest.SettingsProvider
                        .Setup(x => x.GetCurrencyCodeForMarketCode(It.IsAny<string>()))
                        .Returns(DataGenerator.GenerateString());

                    ItemInTest.SettingsProvider
                        .Setup(x => x.GetLocaleByMarket(It.IsAny<string>()))
                        .Returns(Enumerable.Empty<string>());
                }, () =>
                {
                    var exception = AssertException<ApiException>(
                        () => ItemInTest.ValidateLocaleAndMarket(DataGenerator.GenerateString(), DataGenerator.GenerateString()));

                    //AssertString("Locale provided is invalid for the market.",
                    //    exception.ApiErrorMessage);

                    AssertString("121055", exception.ApiErrorCode);

                    Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
                });
        }

        [TestMethod]
        public void WillNotThrowExceptionIfMarketAndLocalesAreReturned()
        {
            var locale = DataGenerator.GenerateString();
            SetupAndExecuteTest(() =>
                {
                    Setup();

                    ItemInTest.SettingsProvider
                        .Setup(x => x.GetCurrencyCodeForMarketCode(It.IsAny<string>()))
                        .Returns(DataGenerator.GenerateString());

                    ItemInTest.SettingsProvider
                        .Setup(x => x.GetLocaleByMarket(It.IsAny<string>()))
                        .Returns(new List<string>(){locale});
                }, 
                () => AssertExceptionNotThrown(() => ItemInTest.ValidateLocaleAndMarket(
                    DataGenerator.GenerateString(),locale)));
        }
    }
}
