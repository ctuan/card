﻿using System;
using System.Linq;
using System.Messaging;
using Autofac;
using Card.WebApi.App_Start;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NServiceBus;
using NServiceBus.Unicast;
using Starbucks.OrderManagement.QueueService.Common.Commands;

namespace Card.WebApi.Tests
{
    /// <summary>
    /// These tests require MSMQ. See http://docs.particular.net/nservicebus/msmq/ for more information. To enable this Windows OS feature, run the following command:
    ///
    /// Windows 7:
    ///     -DISM.exe /Online /NoRestart /English /Enable-Feature /FeatureName:MSMQ-Container /FeatureName:MSMQ-Server
    /// Windows 8+, Windows Server 2008 R2+:
    ///     -DISM.exe /Online /NoRestart /English /Enable-Feature /all /FeatureName:MSMQ-Server
    ///
    /// </summary>
    [TestClass]
    public class NServiceBusTests
    {
        private static ISendOnlyBus _bus;

        [ClassInitialize]
        public static void ClassInit(TestContext testContext)
        {
            var container = new ContainerBuilder().Build();

            NsbConfig.ConfigureSendOnlyBus(container);

            _bus = container.Resolve<ISendOnlyBus>();
        }

        private static void EnsureQueueExists<T>()
        {
            const string QueueNameFormat = "{0}\\Private$\\{1}";

            var destAddress = (_bus as UnicastBus)
                .MessageRouter
                .GetDestinationFor(typeof(T))
                .SingleOrDefault();

            Assert.IsNotNull(destAddress, "NServiceBus is incorrectly configured for message type '{0}'; please fix", typeof(T).FullName);

            var queueName = string.Format(QueueNameFormat, destAddress.Machine, destAddress.Queue);

            if (!MessageQueue.Exists(queueName))
            {
                MessageQueue.Create(queueName, true);
            }
        }

        [TestMethod]
        public void OrderManagementSaveFailedOrder()
        {
            EnsureQueueExists<ISaveFailedOrder>();

            _bus.Send<ISaveFailedOrder>(fo => fo.OrderId = "1");
        }
    }
}
