﻿//using System;
//using System.Collections.Generic;
//using Starbucks.Card.Provider.Common;
//using Starbucks.Card.Provider.Common.Models;


//namespace Card.WebApi.Tests.Providers
//{
//    public class MockCardProviderException : ICardProvider
//    {
//        public IStarbucksCard GetStarbucksCardByNumberAndPin(string cardNumber, string pin)
//        {
//            throw new NotImplementedException();
//        }

//        public IEnumerable< IStarbucksCardImage> GetStarbucksCardImageUrlByCardNumber(string cardNumber)
//        {
//            throw new NotImplementedException();
//        }

//        public IEnumerable<IStarbucksCard> GetStarbucksCards(string userId, bool enableBalanceUpdatesInCompleteProfile)
//        {
//            throw new NotImplementedException();
//        }

//        public IEnumerable<IStarbucksCardStatus> GetStatusesForUserCards(string userId, IEnumerable<string> cardIds)
//        {
//            throw new NotImplementedException();
//        }

//        public IStarbucksCard GetStarbucksCardById(string userId, string cardId)
//        {
//            throw new NotImplementedException();
//        }

//        public IStarbucksCardBalance RefreshStarbucksCardBalance(string userId, string cardId)
//        {
//            throw new NotImplementedException();
//        }

//        public IStarbucksCardBalance RefreshStarbucksCardBalanceByCardNumberPinNumber(string cardNumber, string pin)
//        {
//            throw new NotImplementedException();
//        }

//        public IEnumerable<IStarbucksCardTransaction> GetStarbucksCardTransactions(string userId, string cardId)
//        {
//            throw new NotImplementedException();
//        }

//        public IStarbucksCard RegisterStarbucksCard(string userId, string cardNumber, string pin)
//        {
//            throw new NotImplementedException();
//        }

//        public IStarbucksCard ActivateAndRegisterCard(string userId)
//        {
//            throw new NotImplementedException();
//        }

//        public IEnumerable<ICardRegistrationStatus> RegisterMultipleStarbucksCards(string userId, IEnumerable<IStarbucksCardNumberAndPin> cards)
//        {
//            throw new NotImplementedException();
//        }

//        public IStarbucksCardUnregisterResult UnregisterStarbucksCard(string userId, string cardId)
//        {
//            throw new NotImplementedException();
//        }

//        public IStarbucksCardBalance ReloadStarbucksCard(string userId, string cardId, IReloadForStarbucksCard reloadForStarbucksCard,
//                                                         IPaymentMethodData paymentMethodData)
//        {
//            throw new NotImplementedException();
//        }

//        public IStarbucksCardBalance ReloadStarbucksCardByCardNumberPin(string cardNumber, string pin,
//                                                                        IReloadForStarbucksCard reloadForStarbucksCard,
//                                                                        IPaymentMethodData paymentMethodData)
//        {
//            throw new NotImplementedException();
//        }

//        public IStarbucksCardBalance ReloadStarbucksCard(string userId, string cardId,
//                                                             IReloadForStarbucksCard reloadForStarbucksCard)
//        {
//            throw new NotImplementedException();
//        }

//        public IStarbucksCardBalance  ReloadStarbucksCardByCardNumberPin(string cardNumber, string pin,
//                                                                            IReloadForStarbucksCard reloadForStarbucksCard)
//        {
//            throw new NotImplementedException();
//        }

//        public IAutoReloadProfile SetupAutoReload(string userId, string cardId, IAutoReloadProfile autoReloadProfile)
//        {
//            throw new NotImplementedException();
//        }

//        public IAutoReloadProfile UpdateAutoReload(string userId, string cardId, IAutoReloadProfile autoReloadProfile)
//        {
//            throw new NotImplementedException();
//        }

//        public void EnableAutoReload(string userId, string cardId)
//        {
//            throw new NotImplementedException();
//        }

//        public void DisableAutoReload(string userId, string cardId)
//        {
//            throw new NotImplementedException();
//        }

//        public ITransferBalanceResult TransferStarbucksCardBalance(string userId, string fromCardId, string toCardId, decimal? amount)
//        {
//            throw new NotImplementedException();
//        }

//        public IActivateAndReloadCardResonse ActivateAndReloadCardAsGift(decimal amount, string paymentMethodId, string paymentType)
//        {
//            throw new NotImplementedException();
//        }

//        public ICardTransaction ReloadAsGiftContribution(string cardNumber, decimal amount, string paymentMethdodId, string paymentType)
//        {
//            throw new NotImplementedException();
//        }
//    }
//}
