﻿//using System;
//using System.Collections.Generic;
//using Card.WebApi.ProviderModels;
//using Starbucks.Card.Provider.Common;
//using Starbucks.Card.Provider.Common.Enums;
//using Starbucks.Card.Provider.Common.Models;

//namespace Card.WebApi.Tests.Providers
//{
//    public class MockCardProvider : ICardProvider
//    {
//        public IStarbucksCard GetStarbucksCardByNumberAndPin(string cardNumber, string pin)
//        {
//            return GetStarbucksCard();
//        }     

//        public IEnumerable<IStarbucksCardImage> GetStarbucksCardImageUrlByCardNumber(string cardNumber)
//        {
//            return new List<IStarbucksCardImage>(){new StarbucksCardImage{Type =CardImageType.ImageIcon , Uri="http://"}};
//        }

//        public IEnumerable<IStarbucksCard> GetStarbucksCards(string userId, bool enableBalanceUpdatesInCompleteProfile)
//        {
//            return new List<IStarbucksCard>()
//                {
//                    GetStarbucksCard() 
//                };
//        }

//        public IEnumerable<IStarbucksCardStatus> GetStatusesForUserCards(string userId, IEnumerable<string> cardIds)
//        {
//            return new List<IStarbucksCardStatus>() {new StarbucksCardStatus() {CardId = "123", Valid = true}};
//        }

//        public IStarbucksCard GetStarbucksCardById(string userId, string cardId)
//        {
//            return GetStarbucksCard();
//        }

//        public IStarbucksCardBalance RefreshStarbucksCardBalance(string userId, string cardId)
//        {
//            return new StarbucksCardBalance()
//                {
//                    Balance = 120,
//                    BalanceCurrencyCode = "USD",
//                    BalanceDate = DateTime.Now,
//                    CardId = "123"
//                };
//        }

//        public IStarbucksCardBalance RefreshStarbucksCardBalanceByCardNumberPinNumber(string cardNumber, string pin)
//        {
//            return new StarbucksCardBalance()
//            {
//                Balance = 120,
//                BalanceCurrencyCode = "USD",
//                BalanceDate = DateTime.Now,
//                CardId = "123"
//            };
//        }

//        public IEnumerable<IStarbucksCardTransaction> GetStarbucksCardTransactions(string userId, string cardId)
//        {
//            return new List<IStarbucksCardTransaction> {GetTransaction()};
//        }

//        public IStarbucksCard RegisterStarbucksCard(string userId, string cardNumber, string pin)
//        {
//            return new StarbucksCard(){CardId = "123"};
//        }

//        public IStarbucksCard ActivateAndRegisterCard(string userId)
//        {
//            return new StarbucksCard();
//        }

//        public IEnumerable<ICardRegistrationStatus> RegisterMultipleStarbucksCards(string userId, IEnumerable<IStarbucksCardNumberAndPin> cards)
//        {
//            return new List<ICardRegistrationStatus>()
//                {
//                    new CardRegistrationStatus()
//                        {
//                            CardId = "123",
//                            CardNumber = "1234",
//                            Code = "",
//                            Message = "",
//                            Successful = true
//                        }
//                };
//        }

//        public IStarbucksCardUnregisterResult UnregisterStarbucksCard(string userId, string cardId)
//        {
//            return new StarbucksCardUnregisterResult() {CardId = "23"};
//        }

//        public IStarbucksCardBalance ReloadStarbucksCard(string userId, string cardId, IReloadForStarbucksCard reloadForStarbucksCard,
//                                                         IPaymentMethodData paymentMethodData)
//        {
//             return GetBalance() ;
//        }

//        public IStarbucksCardBalance ReloadStarbucksCardByCardNumberPin(string cardNumber, string pin,
//                                                                        IReloadForStarbucksCard reloadForStarbucksCard,
//                                                                        IPaymentMethodData paymentMethodData)
//        {
//            return GetBalance();
//        }


//        public IStarbucksCardTransaction ReloadStarbucksCardUsingPayPalRefTransaction(string userId, string cardId,
//                                                                                      IReloadForStarbucksCard reloadForStarbucksCard)
//        {
//            return GetTransaction() ;
//        }

//        public IAutoReloadProfile SetupAutoReload(string userId, string cardId, IAutoReloadProfile autoReloadProfile)
//        {
//            return GetAutoReloadProfile();
//        }

//        public IAutoReloadProfile UpdateAutoReload(string userId, string cardId, IAutoReloadProfile autoReloadProfile)
//        {
//            return GetAutoReloadProfile();
//        }

//        public void EnableAutoReload(string userId, string autoReloadId)
//        {
            
//        }

//        public void DisableAutoReload(string userId, string autoReloadId)
//        {
            
//        }

//        public ITransferBalanceResult TransferStarbucksCardBalance(string userId, string fromCardId, string toCardId, decimal? amount)
//        {
//            var a =  GetTransaction();
//            var balances = new Dictionary<string, decimal>() {{"123", 123}};
//            return new TransferBalanceResult() {Balances = balances, StarbucksCardTransaction = a};
//        }

//        public IActivateAndReloadCardResonse ActivateAndReloadCardAsGift(decimal amount, string paymentMethodId, string paymentType)
//        {
//            throw new NotImplementedException();
//        }

//        public ICardTransaction ReloadAsGiftContribution(string cardNumber, decimal amount, string paymentMethdodId, string paymentType)
//        {
//            throw new NotImplementedException();
//        }

//        private static AutoReloadProfile  GetAutoReloadProfile()
//        {
//            return new AutoReloadProfile()
//                {
//                    Amount = 111,
//                    AutoReloadId = "123",
//                    AutoReloadType = "Amount",
//                    BillingAgreementId = "23",
//                    //...
//                };
//        }

//        private static StarbucksCardBalance GetBalance()
//        {
//            return new StarbucksCardBalance()
//            {
//                CardNumber = "0123456789101112",
//                Balance = 124,
//                BalanceCurrencyCode = "USD",
//                BalanceDate = DateTime.Now,

//            };
//        }

//        private static StarbucksCardTransaction GetTransaction()
//        {
//            return new StarbucksCardTransaction()
//                {
//                    Amount = 100,
//                    CardId = "123",
//                    CurrencyCode = "USD",
//                    Description = "Redemption",
//                    TransactionDate = DateTime.Now,
//                    TransactionId = "1234"
//                };
//        }

//        private static StarbucksCard GetStarbucksCard()
//        {
//            return new StarbucksCard
//            {
//                AutoReloadProfile = new AutoReloadProfile() { },
//                Balance = 100,
//                BalanceCurrencyCode = "USD",
//                BalanceDate = DateTime.Now,
//                CardClass = "202",
//                CardId = "1000",
//                CardImages = new List<IStarbucksCardImage>() { new StarbucksCardImage() {Type=CardImageType.ImageIcon , Uri = "http://image"}, new StarbucksCardImage() {Type=CardImageType.ImageIconMobile , Uri = "http://mobileImage"}},
//                CardNumber = "0123456789101112",
//                CardType = "Gold",
//                CurrencyCode = "USD",
//                IsDefaultCard = true,
//                IsDigitalCard = false,
//                IsPartnerCard = false,
//                Nickname = "my card",
//                PassbookSerialNumbers = new List<string>(),
//                RegisteredUserId = "123",
//                SubmarketCode = "US"
//            };
//        }
//    }
//}
