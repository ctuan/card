﻿//using System.Collections.Generic;
//using Starbucks.Card.Provider.Common;
//using Starbucks.Card.Provider.Common.ErrorResources;
//using Starbucks.Card.Provider.Common.Exceptions;
//using Starbucks.Card.Provider.Common.Models;


//namespace Card.WebApi.Tests.Providers
//{
//    public class MockCardProviderCardException : ICardProvider
//    {
//        public IStarbucksCard GetStarbucksCardByNumberAndPin(string cardNumber, string pin)
//        {
//            throw new CardProviderException(CardProviderErrorResource.CardNotFoundMessage,
//                                            CardProviderErrorResource.CardNotFoundCode);
//        }

//        public IEnumerable<IStarbucksCardImage> GetStarbucksCardImageUrlByCardNumber(string cardNumber)
//        {

//            throw new CardProviderException(CardProviderErrorResource.CardNotFoundMessage,
//                                            CardProviderErrorResource.CardNotFoundCode);
//        }

//        public IEnumerable<IStarbucksCard> GetStarbucksCards(string userId, bool enableBalanceUpdatesInCompleteProfile)
//        {

//            throw new CardProviderException(CardProviderErrorResource.CardNotFoundMessage,
//                                            CardProviderErrorResource.CardNotFoundCode);
//        }

//        public IEnumerable<IStarbucksCardStatus> GetStatusesForUserCards(string userId, IEnumerable<string> cardIds)
//        {

//            throw new CardProviderException(CardProviderErrorResource.CardNotFoundMessage,
//                                            CardProviderErrorResource.CardNotFoundCode);
//        }

//        public IStarbucksCard GetStarbucksCardById(string userId, string cardId)
//        {

//            throw new CardProviderException(CardProviderErrorResource.CardNotFoundMessage,
//                                            CardProviderErrorResource.CardNotFoundCode);
//        }

//        public IStarbucksCardBalance RefreshStarbucksCardBalance(string userId, string cardId)
//        {

//            throw new CardProviderException(CardProviderErrorResource.CardNotFoundMessage,
//                                            CardProviderErrorResource.CardNotFoundCode);
//        }

//        public IStarbucksCardBalance RefreshStarbucksCardBalanceByCardNumberPinNumber(string cardNumber, string pin)
//        {

//            throw new CardProviderException(CardProviderErrorResource.CardNotFoundMessage,
//                                            CardProviderErrorResource.CardNotFoundCode);
//        }

//        public IEnumerable<IStarbucksCardTransaction> GetStarbucksCardTransactions(string userId, string cardId)
//        {

//            throw new CardProviderException(CardProviderErrorResource.CardNotFoundMessage,
//                                            CardProviderErrorResource.CardNotFoundCode);
//        }

//        public IStarbucksCard RegisterStarbucksCard(string userId, string cardNumber, string pin)
//        {

//            throw new CardProviderException(CardProviderErrorResource.ValueLinkHostDownMessage ,
//                                            CardProviderErrorResource.ValueLinkHostDownCode );
//        }

//        public IStarbucksCard ActivateAndRegisterCard(string userId)
//        {
//            throw new CardProviderException(CardProviderErrorResource.ValueLinkHostDownMessage,
//                                            CardProviderErrorResource.ValueLinkHostDownCode);
//        }

//        public IEnumerable<ICardRegistrationStatus> RegisterMultipleStarbucksCards(string userId, IEnumerable<IStarbucksCardNumberAndPin> cards)
//        {
//            throw new CardProviderException(CardProviderErrorResource.ValueLinkHostDownMessage,
//                                              CardProviderErrorResource.ValueLinkHostDownCode);
//        }

//        public IStarbucksCardUnregisterResult UnregisterStarbucksCard(string userId, string cardId)
//        {
//            throw new CardProviderException(CardProviderErrorResource.ValueLinkHostDownMessage,
//                                            CardProviderErrorResource.ValueLinkHostDownCode);
//        }

//        public IStarbucksCardBalance ReloadStarbucksCard(string userId, string cardId, IReloadForStarbucksCard reloadForStarbucksCard,
//                                                         IPaymentMethodData paymentMethodData)
//        {
//            throw new CardProviderException(CardProviderErrorResource.ValueLinkHostDownMessage,
//                                         CardProviderErrorResource.ValueLinkHostDownCode);
//        }

//        public IStarbucksCardBalance ReloadStarbucksCardByCardNumberPin(string cardNumber, string pin,
//                                                                        IReloadForStarbucksCard reloadForStarbucksCard,
//                                                                        IPaymentMethodData paymentMethodData)
//        {
//            throw new CardProviderException(CardProviderErrorResource.ValueLinkHostDownMessage,
//                                            CardProviderErrorResource.ValueLinkHostDownCode);
//        }

//        public IAutoReloadProfile SetupAutoReload(string userId, string cardId, IAutoReloadProfile autoReloadProfile)
//        {
//            throw new CardProviderException(CardProviderErrorResource.ValueLinkHostDownMessage,
//                                           CardProviderErrorResource.ValueLinkHostDownCode);
//        }

//        public IAutoReloadProfile UpdateAutoReload(string userId, string cardId, IAutoReloadProfile autoReloadProfile)
//        {
//             throw new CardProviderException(CardProviderErrorResource.ValueLinkHostDownMessage ,
//                                            CardProviderErrorResource.ValueLinkHostDownCode );
//        }

//        public void EnableAutoReload(string userId, string cardId)
//        {
//            throw new CardProviderException(CardProviderErrorResource.ValueLinkHostDownMessage,
//                                           CardProviderErrorResource.ValueLinkHostDownCode);
//        }

//        public void DisableAutoReload(string userId, string cardId)
//        {
//            throw new CardProviderException(CardProviderErrorResource.ValueLinkHostDownMessage,
//                                            CardProviderErrorResource.ValueLinkHostDownCode);
//        }

//        public ITransferBalanceResult TransferStarbucksCardBalance(string userId, string fromCardId, string toCardId, decimal? amount)
//        {
//            throw new CardProviderException(CardProviderErrorResource.ValueLinkHostDownMessage,
//                                           CardProviderErrorResource.ValueLinkHostDownCode);
//        }

//        public IActivateAndReloadCardResonse ActivateAndReloadCardAsGift(decimal amount, string paymentMethodId, string paymentType)
//        {
//            throw new System.NotImplementedException();
//        }

//        public ICardTransaction ReloadAsGiftContribution(string cardNumber, decimal amount, string paymentMethdodId, string paymentType)
//        {
//            throw new System.NotImplementedException();
//        }
//    }
//}
