﻿using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Card.WebApi.Tests
{
    public class FakeHttpContent : HttpContent
    {
        protected override Task SerializeToStreamAsync(Stream stream, TransportContext context)
        {
            return null;
        }

        protected override bool TryComputeLength(out long length)
        {
            length = 0;
            return true;
        }
    }
}
