﻿using Starbucks.OpenApi.Utilities.Common.Helpers;

namespace Card.WebApi.Tests
{
    public class FakeLog : ILog
    {
        public string[] Logs { get; set; }
        public string PassbookObjectId { get; set; }
        public string UserId { get; set; }
        public string SerialNumber { get; set; }
        public string CardId { get; set; }
        public string InternalDeviceId { get; set; }
        public string ExternalDeviceId { get; set; }
        public string ExternalCorrelationId { get; set; }
        public string PassTypeIdentifier { get; set; }
        public string TeamIdentifier { get; set; }
        public string Message { get; set; }
        public string MessageCategoryId { get; set; }
        public string FormattedMessage { get; set; }
        public string RequestUri { get; set; }
        public string RequestContent { get; set; }
        public string RequestHttpMethod { get; set; }
        public string RequestHeaders { get; set; }
        public string ResponseHttpStatusCode { get; set; }
        public string ResponseContent { get; set; }
    }
}
