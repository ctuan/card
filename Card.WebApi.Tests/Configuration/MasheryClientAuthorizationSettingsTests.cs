﻿using System.Configuration;
using System.Linq;
using Card.WebApi.Authorization;
using Card.WebApi.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AuthorizationClass = Card.WebApi.Configuration.Authorization;

namespace Card.WebApi.Tests.Configuration
{
    [TestClass]
    public class MasheryClientAuthorizationSettingsTests
    {
        private const string ConfigSection = "masheryClientAuthorizationSettings";

        private static MasheryClientAuthorizationSettings SetupMasheryClientAuthorizationSettings(string configFile)
        {
            var config = ConfigurationManager.OpenMappedExeConfiguration(new ExeConfigurationFileMap { ExeConfigFilename = configFile }, ConfigurationUserLevel.None);

            return (MasheryClientAuthorizationSettings)config.GetSection(ConfigSection);
        }

        private static void VerifyMasheryClientAuthorizationSettings(MasheryClientAuthorizationSettings masheryClientAuthorizationSettings, string expectedProtectedDataStorePath, string expectedSecureHashSalt)
        {
            Assert.IsNotNull(masheryClientAuthorizationSettings, "Expected MasheryClientAuthorizationSettings to be non-null");

            Assert.IsTrue(masheryClientAuthorizationSettings.IsReadOnly(), "Expected MasheryClientAuthorizationSettings.IsReadOnly() to return true");
            Assert.IsNotNull(masheryClientAuthorizationSettings.Authorizations, "Expected MasheryClientAuthorizationSettings.Authorizations to be non-null");
            Assert.AreEqual(expectedProtectedDataStorePath, masheryClientAuthorizationSettings.ProtectedDataStorePath);
            Assert.AreEqual(expectedSecureHashSalt, masheryClientAuthorizationSettings.SecureHashSalt);

            var authorizationData = (IAuthorizationData)masheryClientAuthorizationSettings;
            Assert.IsTrue(Enumerable.SequenceEqual(masheryClientAuthorizationSettings.Authorizations, authorizationData.Authorizations), "Expected Authorizations to be equal");

            var protectedDataStoreSettings = (IProtectedDataStoreSettings)masheryClientAuthorizationSettings;
            Assert.AreEqual(expectedProtectedDataStorePath, protectedDataStoreSettings.Path);

            var secureHashAuthorizationSettings = (ISecureHashAuthorizationProviderSettings)masheryClientAuthorizationSettings;
            Assert.AreEqual(expectedSecureHashSalt, secureHashAuthorizationSettings.Salt);
        }

        private static void VerifyAuthorization(AuthorizationClass authorization, string expectedController, string expectedAction, string expectedRequestMethod, string expectedValue)
        {
            Assert.IsNotNull(authorization, "Expected Authorization to be non-null");

            Assert.AreEqual(expectedController, authorization.Controller);
            Assert.AreEqual(expectedAction, authorization.Action);
            Assert.AreEqual(expectedRequestMethod, authorization.RequestMethod);
            Assert.AreEqual(expectedValue, authorization.Value);
        }

        [TestMethod]
        public void CanAddAuthorizationsInConfig()
        {
            var masheryClientAuthorizationSettings = SetupMasheryClientAuthorizationSettings(@"Configuration\CanAddAuthorizations.config");

            VerifyMasheryClientAuthorizationSettings(masheryClientAuthorizationSettings, @"c:\SecretStore", "bhagpvf2Ilx8Ao0HgPFKabwe4thBjek/");

            var authorizations = masheryClientAuthorizationSettings.Authorizations;

            Assert.AreEqual(1, authorizations.Count);
            
            VerifyAuthorization(authorizations[0], "FraudRecovery", "revoke", "POST", "FooBarClientId");
        }

        [TestMethod]
        public void CanAddMultipleAuthorizationsInConfig()
        {
            var masheryClientAuthorizationSettings = SetupMasheryClientAuthorizationSettings(@"Configuration\CanAddMultipleAuthorizations.config");

            VerifyMasheryClientAuthorizationSettings(masheryClientAuthorizationSettings, @"c:\SecretStore", "bhagpvf2Ilx8Ao0HgPFKabwe4thBjek/");

            var authorizations = masheryClientAuthorizationSettings.Authorizations;

            Assert.AreEqual(10, authorizations.Count);
            
            VerifyAuthorization(authorizations[0], string.Empty, string.Empty, string.Empty, "FooBarClientId1");
            VerifyAuthorization(authorizations[1], string.Empty, string.Empty, string.Empty, "FooBarClientId2");
            VerifyAuthorization(authorizations[2], "FraudRecovery", string.Empty, string.Empty, "FooBarClientId3");
            VerifyAuthorization(authorizations[3], "FraudRecovery", string.Empty, string.Empty, "FooBarClientId4");
            VerifyAuthorization(authorizations[4], "FraudRecovery", "revoke", string.Empty, "FooBarClientId5");
            VerifyAuthorization(authorizations[5], "FraudRecovery", "revoke", string.Empty, "FooBarClientId6");
            VerifyAuthorization(authorizations[6], "FraudRecovery", "revoke", "GET", "FooBarClientId7");
            VerifyAuthorization(authorizations[7], "FraudRecovery", "revoke", "GET", "FooBarClientId8");
            VerifyAuthorization(authorizations[8], "FraudRecovery", "revoke", "POST", "FooBarClientId9");
            VerifyAuthorization(authorizations[9], "FraudRecovery", "revoke", "POST", "FooBarClientId10");
        }

        [TestMethod]
        public void DuplicateAuthorizationsNotAddedInConfig()
        {
            var masheryClientAuthorizationSettings = SetupMasheryClientAuthorizationSettings(@"Configuration\DuplicateAuthorizationsNotAdded.config");

            VerifyMasheryClientAuthorizationSettings(masheryClientAuthorizationSettings, @"c:\SecretStore", "bhagpvf2Ilx8Ao0HgPFKabwe4thBjek/");

            Assert.AreEqual(1, masheryClientAuthorizationSettings.Authorizations.Count);
        }

        [TestMethod]
        [ExpectedException(typeof(ConfigurationErrorsException))]
        public void CannotRemoveAuthorizationsInConfig()
        {
            SetupMasheryClientAuthorizationSettings(@"Configuration\CannotRemoveAuthorizations.config");
        }

        [TestMethod]
        public void CanClearAuthorizationsInConfig()
        {
            var masheryClientAuthorizationSettings = SetupMasheryClientAuthorizationSettings(@"Configuration\CanClearAuthorizations.config");

            VerifyMasheryClientAuthorizationSettings(masheryClientAuthorizationSettings, @"c:\SecretStore", "bhagpvf2Ilx8Ao0HgPFKabwe4thBjek/");

            Assert.AreEqual(0, masheryClientAuthorizationSettings.Authorizations.Count);
        }

        [TestMethod]
        [ExpectedException(typeof(ConfigurationErrorsException))]
        public void CannotHaveEmptyProtectedDataStorePathInConfig()
        {
            SetupMasheryClientAuthorizationSettings(@"Configuration\CannotHaveEmptyProtectedDataStorePath.config");
        }

        [TestMethod]
        [ExpectedException(typeof(ConfigurationErrorsException))]
        public void CannotHaveEmptySecureHashSaltInConfig()
        {
            SetupMasheryClientAuthorizationSettings(@"Configuration\CannotHaveEmptySecureHashSalt.config");
        }

        [TestMethod]
        public void CanHaveNoAuthorizationsInConfig()
        {
            var masheryClientAuthorizationSettings = SetupMasheryClientAuthorizationSettings(@"Configuration\CanHaveNoAuthorizations.config");

            VerifyMasheryClientAuthorizationSettings(masheryClientAuthorizationSettings, @"c:\SecretStore", "bhagpvf2Ilx8Ao0HgPFKabwe4thBjek/");

            Assert.AreEqual(0, masheryClientAuthorizationSettings.Authorizations.Count);
        }
    }
}
