﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FraudRecovery.Tests.Integration;

namespace Starbucks.QA.FraudRecovery.Tests
{
    [TestClass]
    public class FraudRecoveryApiCardFreezeTests : FraudRecoveryCardTestBase
    {
        #region BVT Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(1)]
        [Description("FRA - verify card freeze for US svc card")]
        public void FraudRecoveryUSFreezeCard()
        {
            ExecuteFreezeCardAndValidateResponse();
        }
        #endregion BVT Tests

        #region Market Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card freeze for CA svc card")]
        public void FraudRecoveryCAFreezeCard()
        {
            ExecuteFreezeCardAndValidateResponse(caMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card freeze for UK svc card")]
        public void FraudRecoveryUKFreezeCard()
        {
            ExecuteFreezeCardAndValidateResponse(ukMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card freeze for IE svc card")]
        public void FraudRecoveryIEFreezeCard()
        {
            ExecuteFreezeCardAndValidateResponse(ieMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card freeze for DE svc card")]
        public void FraudRecoveryDEFreezeCard()
        {
            ExecuteFreezeCardAndValidateResponse(deMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card freeze for FR svc card")]
        public void FraudRecoveryFRFreezeCard()
        {
            ExecuteFreezeCardAndValidateResponse(frMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card freeze for BR svc card")]
        public void FraudRecoveryBRFreezeCard()
        {
            ExecuteFreezeCardAndValidateResponse(brMarket);
        }
        #endregion Market Tests

        #region FVT Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card freeze for invalid cardid")]
        public void FraudRecoveryFreezeCardInvalidCardId()
        {
            ExecuteFreezeCardInvalidCardId();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card freeze for zero balance card")]
        public void FraudRecoveryFreezeCardForZeroBalanceCard()
        {
            ExecuteFreezeCardForZeroBalanceCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card freeze for successive freeze")]
        public void FraudRecoveryFreezeCardTwice()
        {
            ExecuteFreezeCardTwice();
        }

        [TestMethod]
        [Ignore]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card freeze for stolen/lost card")]
        public void FraudRecoveryFreezeCardStolenCard()
        {
            ExecuteCardFreezeStolenCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card freeze for unregisterd card")]
        public void FraudRecoveryFreezeCardUnregisteredCard()
        {
            ExecuteCardFreezeUnregisteredCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Freeze for Register-Unregister_Registered svc card")]
        public void FraudRecoveryFreezeCardReRegisterCard()
        {
            ExecuteCardFreezeReRegisteredCard();
        }

        //[TestMethod]
        //[TestCategory("FraudRecoveryApi")]
        //[Priority(2)]
        //[Description("FRA - Verify Freeze for Associated svc card")]
        //public void FraudRecoveryFreezeCardAssociatedCard()
        //{
        //    ExecuteFreezeCardAssociatedCard();
        //}

        #endregion FVT Tests
    }
}
