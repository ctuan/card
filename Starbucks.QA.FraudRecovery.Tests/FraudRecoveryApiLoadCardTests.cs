﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FraudRecovery.Tests.Integration;

namespace Starbucks.QA.FraudRecovery.Tests
{
    [TestClass]
    public class FraudRecoveryApiLoadCardTests : FraudRecoveryCardTestBase
    {
        #region BVT Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(1)]
        [Description("FRA - Verify load amount for US svc card")]
        public void FraudRecoveryUSLoadCard()
        {
            ExecuteLoadCardAndValidateResponse();
        }
        #endregion BVT Tests

        #region Market Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify load amount for CA svc card")]
        public void FraudRecoveryCALoadCard()
        {
            ExecuteLoadCardAndValidateResponse(caMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify load amount for UK svc card")]
        public void FraudRecoveryUKLoadCard()
        {
            ExecuteLoadCardAndValidateResponse(ukMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify load amount for IE svc card")]
        public void FraudRecoveryIELoadCard()
        {
            ExecuteLoadCardAndValidateResponse(ieMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify load amount for DE svc card")]
        public void FraudRecoveryDELoadCard()
        {
            ExecuteLoadCardAndValidateResponse(deMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify load amount for FR svc card")]
        public void FraudRecoveryFRLoadCard()
        {
            ExecuteLoadCardAndValidateResponse(frMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify load amount for BR svc card")]
        public void FraudRecoveryBRLoadCard()
        {
            ExecuteLoadCardAndValidateResponse(brMarket);
        }
        #endregion Market Tests

        #region FVT Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify error for invalid svc card")]
        public void FraudRecoveryLoadCardInvalidCardId()
        {
            ExecuteLoadCardInvalidCardId();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify error for invalid load amount: 0 dollar")]
        public void FraudRecoveryLoadCardInvalidAmountZero()
        {
            ExecuteLoadCardInvalidAmountZero();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify error for invalid load amount: -10")]
        public void FraudRecoveryLoadCardInvalidNegativeAmount()
        {
            ExecuteLoadCardInvalidNegativeAmount();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify error for invalid load amount: > 500")]
        public void FraudRecoveryLoadCardInvalidMaxAmount()
        {
            ExecuteLoadCardInvalidInvalidMaxAmount();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify load amount for exact max amount: 500")]
        public void FraudRecoveryLoadCardExactMaxAmount()
        {
            ExecuteLoadCardExactMaxAmount();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify error when exceeds max limit: > 500")]
        public void FraudRecoveryLoadCardExceedsMaxAmount()
        {
            ExecuteLoadCardExceedsMaxAmount();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card load for successive loads")]
        public void FraudRecoveryLoadCardTwice()
        {
            ExecuteLoadCardTwice();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify load card for unregistered svc card")]
        public void FraudRecoveryLoadCardUnregisteredCard()
        {
            ExecuteLoadCardUnregisteredCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify load card for Register-Unregister_Registered svc card")]
        public void FraudRecoveryLoadCardReRegisterCard()
        {
            ExecuteLoadCardReRegisteredCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify load card for Associated svc card")]
        public void FraudRecoveryLoadCardAssociatedCard()
        {
            ExecuteLoadCardAssociatedCard();
        }

        [TestMethod]
        [Ignore]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify load card for stolen card")]
        public void FraudRecoveryLoadCardStolenCard()
        {
            ExecuteLoadCardStolenCard();
        }
        #endregion 
    }
}
