﻿using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.QaTestUtilities.Api;

namespace Starbucks.QA.FraudRecovery.Tests
{
    public static class FraudRecoveryTestHelper
	{
		public readonly static string[] TestSvCardNumbers = { "7777083002943493", "7777083002953493", "7777083002964149" };
		public readonly static string[] TestSvCardPins = { "33151451", "25567528", "56387208" };
		public const string UnitedStatesTippingMarketId = "99030169997";
		public const string USMid = "99910439997";
		//protected const string USMid = "99030169997";

		public const string TestOrderId = "D1B80M50V2394L7TQ6GFAQAQ31";

		public const string StoreId = "00303";
		public const string TerminalId = "0001";

		public static readonly int[] TestCardId =
			{
				80196718,
				80196743,
				80196717,
				80196712,
				80196714,
				80196715,
				80196716,
				80196719,
			};


        public static readonly int[] USTestCardId =
			{
				80196957,
                80196956,               
                80196955,
                80195671,
			};

        public static readonly int[] CATestCardId =
			{
				80195668,
                80195669,
                80196960,
			};
        public static readonly int[] UKTestCardId =
			{
				80195670,
                80195671,
                80196955,//TODO
			};
        //TODO
        public static readonly int[] IETestCardId =
			{
				80196956,
                80196957,
                80196955,
			};
        //TODO
        public static readonly int[] DETestCardId =
			{
				80196956,
                80196957,
                80196955,
			};
        //TODO
        public static readonly int[] FRTestCardId =
			{
				80196956,
                80196957,
                80196955,
			};
        //TODO
        public static readonly int[] BRTestCardId =
			{
				80196956,
                80196957,
                80196955,
			};
        private static string _defaultUserid;
        private static TestHelper _testHelper;
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            _testHelper = new TestHelper();
            //_defaultUserid = _testHelper.Account.GetOrCreateUser(_testHelper.CreateRandomTestEmail());
        }


		public static string Domain
		{
			get { return ConfigurationManager.AppSettings["ApiDomain"]; }
		}

		public static string TransactionRevokeUrl(this string id, string accesssToken)
		{
            return string.Format("{0}/cards/transaction/{1}/revoke?access_token={2}", Domain, id, accesssToken);
		}
        public static string TransactionRefundUrl(this string id, string accesssToken)
		{
            return string.Format("{0}/cards/transaction/{1}/refund?access_token={2}", Domain, id, accesssToken);
		}
        public static string TransactionFreezeUrl(this string id, string accesssToken)
		{
            return string.Format("{0}/cards/transaction/{1}/freeze?access_token={2}", Domain, id, accesssToken);
		}
        public static string TransactionUnfreezeUrl(this string id, string accesssToken)
		{
            return string.Format("{0}/cards/transaction/{1}/unfreeze?access_token={2}", Domain, id, accesssToken);
		}

        public static string CardBalanceUrl(this string id, string accesssToken)
		{
            return string.Format("{0}/cards/{1}/card-balance?access_token={2}", Domain, id, accesssToken);
		}

        public static string CardRevokeUrl(this string id, string accesssToken)
		{
            return string.Format("{0}/cards/{1}/revoke-amount?access_token={2}", Domain, id, accesssToken);
		}

        public static string CardCashOutUrl(this string id, string accesssToken)
		{
            return string.Format("{0}/cards/{1}/cash-out?access_token={2}", Domain, id, accesssToken);
		}

        public static string CardFreezeUrl(this string id, string accesssToken)
		{
            return string.Format("{0}/cards/{1}/freeze?access_token={2}", Domain, id, accesssToken);
		}

        public static string CardUnfreezeUrl(this string id, string accesssToken)
		{
            return string.Format("{0}/cards/{1}/unfreeze?access_token={2}", Domain, id, accesssToken);
		}

        public static string CardLoadAmount(this string id, string accesssToken)
		{
            return string.Format("{0}/cards/{1}/load-amount?access_token={2}", Domain, id, accesssToken);
		}
	}
}
