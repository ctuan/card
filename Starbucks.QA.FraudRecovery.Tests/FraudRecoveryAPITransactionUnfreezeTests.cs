﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FraudRecovery.Tests.Integration;

namespace Starbucks.QA.FraudRecovery.Tests
{
    [TestClass]
    public class FraudRecoveryAPITransactionUnfreezeTests : FraudRecoveryTransactionTestBase
    {
        #region BVT Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(1)]
        [Description("FRA - Verify Transaction Unfreeze API for US market")]
        public void FraudRecoveryUSTransactionUnfreeze()
        {
            ExecuteTransactionUnfreezeAndValidateResponse();
        }
        #endregion BVT Tests

        #region FVT Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Transaction unfreeze for invalid cardid")]
        public void FraudRecoveryTransactionUnfreezeInvalidOrderId()
        {
            ExecuteTransactionUnfreezeInvalidOrderId();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Transaction unfreeze for successive calls")]
        public void FraudRecoveryTransactionUnfreezeTwice()
        {
            ExecuteTransactionUnfreezeTwice();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(3)]
        [Description("FRA - Call transaction unfreeze on 'Buy a Card' order")]
        public void FraudRecoveryTransactionUnfreezeOnBuyACard()
        {
            ExecuteTransactionUnfreezeOnBuyACard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(3)]
        [Description("FRA - Call transaction Unfreeze on 'eGift' order")]
        public void FraudRecoveryTransactionUnfreezeOnEGiftCard()
        {
            ExecuteTransactionUnfreezeOnEGift();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Transaction UnFreeze after Card Freeze operation")]
        public void FraudRecoveryTransactionUnfreezeAfterSvcCardFreeze()
        {
            ExecuteTransactionUnfreezeAfterSvcCardFreeze();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Transaction UnFreeze after Transaction freeze, card unfreeze and card freeze operations")]
        public void FraudRecoveryTransactionUnfreezeAfterSvcCardFreezeAndUnfreeze()
        {
            ExecuteTransactionUnfreezeAfterSvcCardFreezeAndUnfreeze();
        }
        #endregion FVT Tests      
    }
}
