﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FraudRecovery.Tests.Integration;

namespace Starbucks.QA.FraudRecovery.Tests
{
    [TestClass]
    public class FraudRecoveryApiCashOutCardTests : FraudRecoveryCardTestBase
    {
        #region BVT Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(1)]
        [Description("FRA - Verify cashout for US svc card")]
        public void FraudRecoveryUSCashOutCard()
        {
            ExecuteCashOutCardAndValidateResponse();
        }
        #endregion BVT Tests

        #region Market Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify cashout for CA svc card")]
        public void FraudRecoveryCACashOutCard()
        {
            ExecuteCashOutCardAndValidateResponse(caMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify cashout for UK svc card")]
        public void FraudRecoveryUKCashOutCard()
        {
            ExecuteCashOutCardAndValidateResponse(ukMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify cashout for IE svc card")]
        public void FraudRecoveryIECashOutCard()
        {
            ExecuteCashOutCardAndValidateResponse(ieMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify cashout for DE svc card")]
        public void FraudRecoveryDECashOutCard()
        {
            ExecuteCashOutCardAndValidateResponse(deMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify cashout for FR svc card")]
        public void FraudRecoveryFRCashOutCard()
        {
            ExecuteCashOutCardAndValidateResponse(frMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify cashout for BR svc card")]
        public void FraudRecoveryBRCashOutCard()
        { 
            ExecuteCashOutCardAndValidateResponse(brMarket);
        }
        #endregion Market Tests

        #region FVT Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify cashout for invalid svc card")]
        public void FraudRecoveryCashOutInvalidCardId()
        {
            ExecuteCashOutCardInvalidCardId();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify cashout for zero balance svc card")]
        public void FraudRecoveryCashOutZeroBalanceCard()
        {
            ExecuteCashOutZeroBalanceCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify cashout for successive calls")]
        public void FraudRecoveryCashOutTwice()
        {
            ExecuteCashOutTwice();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify cashout for unregistered svc card")]
        public void FraudRecoveryCashoutUnregisteredCard()
        {
            ExecuteCashoutUnregisteredCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify cashout for Register-Unregister_Registered svc card")]
        public void FraudRecoveryCashoutReRegisterCard()
        {
            ExecuteCashoutReRegisteredCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify cashout for Associated svc card")]
        public void FraudRecoveryCashoutAssociatedCard()
        {
            ExecuteCashoutCardAssociatedCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify cashout for stolen svc card")]
        public void FraudRecoveryCashoutStolenCard()
        {
            ExecuteCashoutStolenCard();

            // TODO: card from config file is not valid, so test fails
            // Find a valid card number.
            // Also, make sure beginning balance > 0 and ending balance = 0.
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card load and cashout multiple times")]
        public void FraudRecoveryLoadAndCashoutCardMultipleTimes()
        {
            ExecuteCardLoadAndCashoutMultipleTimes();
        }
        #endregion FVT Tests
    }
}
