﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.FraudRecovery.Common.Models;
using Starbucks.QA.FraudRecovery.Tests;
using FraudRecovery.Tests.Integration.Resources;
using System.Data.SqlClient;
using System.Configuration;


namespace FraudRecovery.Tests.Integration
{
    public class TransactionDetails
    {
        public string userId;
        public string orderId;
        public string cardNumber;
        public int  cardId;
        public decimal cardBalance;
        public decimal transactionAmount;
    }

    public class FraudRecoveryTransactionTestBase : FraudRecoveryCardTestBase
    {
        #region Transaction revoke endpoint helper methods

        /// <summary>
        /// This helper method make calls to transaction revoke API endpoint and throws exception, if call fails 
        /// </summary>
        /// <param name="transactionDetails"></param>
        public void TransactionRevoke(TransactionDetails transactionDetails)
        {
            // Compute expected card balance after revoke
            decimal expectedCardBalance;
            if (transactionDetails.cardBalance <= transactionDetails.transactionAmount)
            {
                transactionDetails.transactionAmount = transactionDetails.cardBalance; // Recover money left on card
                expectedCardBalance = 0;
            }
            else
            {
                expectedCardBalance = transactionDetails.cardBalance - transactionDetails.transactionAmount;
            }

            var url = transactionDetails.orderId.TransactionRevokeUrl(GetAccessToken());
            Console.WriteLine("Trying to revoke transaction using url '{0}'", url);
            var raw = _client.Post<TransactionRevokeResponse>(url, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;

            var errorMessage = new StringBuilder();
            Assert.IsTrue(response != null, "TransactionRevoke response is null");
            errorMessage.Append(response.Name != "TransactionRevoke" ? string.Format("TransactionRevoke response - 'Name' field has incorrect value '{0}'", response.Name) : null);
            errorMessage.Append(response.Status != OperationStatus.Succeeded ? string.Format("TransactionRevoke response - 'Status' field has incorrect value '{0}'", response.Status) : null);
            errorMessage.Append(response.Id != transactionDetails.orderId ? string.Format("TransactionRevoke response - 'Id' field has incorrect value '{0}'", response.Id) : null);
            errorMessage.Append(response.RequestReceivedDateTime == null ? "TransactionRevoke response - 'RequestReceivedDateTime' field has null value" : null);
            errorMessage.Append(response.TraceNumber == null ? "TransactionRevoke response - 'TraceNumber' field has null value" : null);
            errorMessage.Append(response.Reason != null ? "TransactionRevoke response has error: " + response.Reason.Message : null);
            errorMessage.Append(response.FailureCount != 0 ? string.Format("TransactionRevoke response - 'FailureCount' field has incorrect value '{0}'", response.FailureCount) : null);

            Assert.IsTrue(response.Results[0] != null, "TransactionRevoke response result object is null");
            errorMessage.Append(response.Results[0].Name != "CardRevokeAmount" ? string.Format("TransactionRevoke response - 'Results[0].Name' field has incorrect value '{0}'", response.Results[0].Name) : null);
            errorMessage.Append(response.Results[0].Status != OperationStatus.Succeeded ? string.Format("TransactionRevoke response - 'Results[0].Status' field has incorrect value '{0}'", response.Results[0].Status) : null);
            errorMessage.Append(response.Results[0].Id != Convert.ToString(transactionDetails.cardId) ? string.Format("TransactionRevoke response - 'Svc card id' field has incorrect value Expected: '{0}', Actual: '{1}'", transactionDetails.cardId, response.Results[0].Id) : null);
            errorMessage.Append(response.Results[0].RequestReceivedDateTime == null ? "TransactionRevoke response - 'Results[0].RequestReceivedDateTime' field has null value" : null);
            errorMessage.Append(response.Results[0].TraceNumber == null ? "TransactionRevoke response - 'response.Results[0].TraceNumber' field has null value" : null);
            errorMessage.Append(response.Results[0].Reason != null ? "TransactionRevoke response.Results[0].Reason has error: " + response.Reason.Message : null);

            Assert.IsTrue(response.Results[0].Result != null, "TransactionRevoke response Results[0].Result object is null");
            errorMessage.Append(response.Results[0].Result.Amount != transactionDetails.transactionAmount ? string.Format("TransactionRevoke response - 'Amount' field has incorrect value '{0}'", response.Results[0].Result.Amount) : null);
            errorMessage.Append(response.Results[0].Result.BeginningBalance != transactionDetails.cardBalance ? string.Format("TransactionRevoke response - 'BeginningBalance' field has incorrect value Expected: '{0}', Actual: '{1}'", transactionDetails.cardBalance, response.Results[0].Result.BeginningBalance) : null);
            errorMessage.Append(response.Results[0].Result.EndingBalance != expectedCardBalance ? string.Format("TransactionRevoke response - 'EndingBalance' field has incorrect value Expected: '{0}', Actual: '{1}'", expectedCardBalance, response.Results[0].Result.EndingBalance) : null);
            errorMessage.Append(response.Results[0].Result.EndingBalanceDateTime == null ? "TransactionRevoke response - 'Results[0].Result.EndingBalanceDateTime' field has null value" : null);
            errorMessage.Append(response.Results[0].Result.LockAmount != 0 ? string.Format("TransactionRevoke response - 'Lock Amount' field has incorrect value '{0}'", response.Results[0].Result.Amount) : null);

            Console.WriteLine("Verify transaction revoke endpoint call is successfull");
            Assert.IsTrue(errorMessage.Length == 0, errorMessage.ToString());
        }

        /// <summary>
        /// This helper method calls transaction revoke API endpoint and returns response (use this to test negative cases so that you can verify error in response)
        /// </summary>
        /// <param name="transactionDetails"></param>
        public TransactionRevokeResponse TransactionRevokeResponse(TransactionDetails transactionDetails)
        {
            var url = transactionDetails.orderId.TransactionRevokeUrl(GetAccessToken());
            Console.WriteLine("Trying to revoke transaction using url '{0}'", url);
            var raw = _client.Post<TransactionRevokeResponse>(url, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;
            return response;
        }

        /// <summary>
        /// Verify transaction revoke endpoint call
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteTransactionRevokeAndValidateResponse(string market = "US")
        {
            string userId;
            var cardId = FraudRecoveryCardTestSetup(market, out userId);

            TransactionDetails transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            TransactionRevoke(transactionDetails);
        }

        /// <summary>
        /// Verify transaction revoke for invalid order id
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteTransactionRevokeInvalidOrderId(string market = "US")
        {
            TransactionDetails transactionDetails = new TransactionDetails();
            transactionDetails.orderId = "843B255E-3660-4FD0-812B-0A6C8A9FF8B3";
            TransactionRevokeResponse response = TransactionRevokeResponse(transactionDetails);
            VerifyTransactionRevokeReasonCode(response, ReasonResources.InvalidTransactionReasonCode, ReasonResources.InvalidTransactionReasonMessage);
        }

        /// <summary>
        /// Verify transaction revoke can be called multiple times
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteTransactionRevokeTwice(string market = "US")
        {
            string userId;
            var cardId = FraudRecoveryCardTestSetup(market, out userId);

            TransactionDetails transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            Console.WriteLine("Transaction revoke api - first call");
            TransactionRevoke(transactionDetails);
            Console.WriteLine("Transaction revoke api - second call");
            transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            TransactionRevoke(transactionDetails);
        }

        //  Verify transaction revoke on svc card with low balance
        public void ExecuteTransactionRevokeLowBalanceSvcCard(string market = "US")
        {
            string userId;
            var cardId = FraudRecoveryCardTestSetup(market, out userId);

            decimal balance = GetCardBalanceFromDB(cardId);
            Console.WriteLine("make svc card balance $1");
            decimal revokeAmount = balance - 1; 
            VerifyRevokeCard(cardId, balance, revokeAmount);

            TransactionDetails transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            TransactionRevoke(transactionDetails);
        }

        // Verify transaction revoke on svc card with zero balance
        public void ExecuteTransactionRevokeZeroBalanceSvcCard(string market = "US")
        {
            string userId;
            var cardId = FraudRecoveryCardTestSetup(market, out userId);

            decimal balance = GetCardBalanceFromDB(cardId);
            Console.WriteLine("make svc card balance zero");
            VerifyRevokeCard(cardId, balance, balance);

            TransactionDetails transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            TransactionRevoke(transactionDetails);
        }

        // Verify transaction revoke fails on 'Buy a Card' transaction
        public void ExecuteTransactionRevokeOnBuyACard(string market = "US")
        {
            TransactionDetails transactionDetails = new TransactionDetails();
            transactionDetails.orderId = ConfigurationManager.AppSettings["buyACardOrderId"].ToString();
            TransactionRevokeResponse response = TransactionRevokeResponse(transactionDetails);
            VerifyTransactionRevokeReasonCode(response, ReasonResources.NotSvcOrderReasonCode, ReasonResources.NotSvcOrderReasonMessage);
        }

        // Verify transaction revoke fails on 'eGift' transaction
        public void ExecuteTransactionRevokeOnEGift(string market = "US")
        {
            string userId;
            var orderId = FraudRecoveryeGiftTestSetup(market, out userId);
            TransactionDetails transactionDetails = new TransactionDetails();
            transactionDetails.orderId = orderId;

            TransactionRevokeResponse response = TransactionRevokeResponse(transactionDetails);
            VerifyTransactionRevokeReasonCode(response, ReasonResources.NotSvcOrderReasonCode, ReasonResources.NotSvcOrderReasonMessage);
        }

        // Verify transaction revoke after transaction freeze and unfreeze operations
        public void ExecuteTransactionRevokeAfterFreezeAndUnfreeze(string market = "US")
        {
            string userId;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);
            InititiliazeCardForFreezeTest(cardId);

            TransactionDetails transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            TransactionFreeze(transactionDetails);
            TransactionUnfreeze(transactionDetails);
            TransactionRevoke(transactionDetails);
        }

        // Verify transaction revoke after transaction freeze operation - verify operation fails
        public void ExecuteTransactionRevokeAfterTransactionFreeze(string market = "US")
        {
            string userId;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);
            InititiliazeCardForFreezeTest(cardId);

            TransactionDetails transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            TransactionFreeze(transactionDetails);
            TransactionRevokeResponse response = TransactionRevokeResponse(transactionDetails);
            Assert.IsTrue(response.Status == OperationStatus.Failed, "Transaction revoke did not fail after transaction freeze operation");
            TransactionUnfreeze(transactionDetails); 
        }
        #endregion Transaction revoke endpoint helper methods

        #region Transaction refund endpoint helper methods

        /// <summary>
        /// Helper method for transaction refund endpoint
        /// </summary>
        /// <param name="transactionDetails"></param>
        public void TransactionRefund(TransactionDetails transactionDetails)
        {
            var url = transactionDetails.orderId.TransactionRefundUrl(GetAccessToken());
            Console.WriteLine("Trying to refund transaction using url '{0}'", url); 
            var raw = _client.Post<TransactionRefundResponse>(url, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;

            var errorMessage = new StringBuilder();
            Assert.IsTrue(response != null, "TransactionRefund response is null");
            errorMessage.Append(response.Name != "TransactionRefund" ? string.Format("TransactionRefund response - 'Name' field has incorrect value '{0}'", response.Name) : null);
            errorMessage.Append(response.Status != OperationStatus.Succeeded ? string.Format("TransactionRefund response - 'Status' field has incorrect value '{0}'", response.Status) : null);
            errorMessage.Append(response.Id != transactionDetails.orderId ? string.Format("TransactionRefund response - 'Id' field has incorrect value '{0}'", response.Id) : null);
            errorMessage.Append(response.RequestReceivedDateTime == null ? "TransactionRefund response - 'RequestReceivedDateTime' field has null value" : null);
            errorMessage.Append(response.TraceNumber == null ? "TransactionRefund response - 'TraceNumber' field has null value" : null);
            errorMessage.Append(response.Reason != null ? "TransactionRefund response has error: " + response.Reason.Message : null);

            Assert.IsTrue(response.Result != null, "TransactionRefund response result object is null");

            errorMessage.Append(
                string.IsNullOrEmpty(response.Result.Amount) 
                || Convert.ToDecimal(response.Result.Amount) != transactionDetails.transactionAmount 
                ? string.Format("TransactionRefund response - 'Result.Amount' field has incorrect value Expected: '{0}', Actual: '{1}'"
                    , transactionDetails.transactionAmount, response.Result.Amount) 
                : null);
            //TODO: errorMessage.Append(response.Result.DateTime != null ? "TransactionRefund response - 'Result.DateTime' field in not null" : null);
            errorMessage.Append(response.Result.Decision != "Accept" ? string.Format("TransactionRefund response - 'Result.Decision' field has incorrect value '{0}'", response.Result.Decision) : null);
            errorMessage.Append(response.Result.PaymentGatewayId == null ? "TransactionRefund response - 'Result.PaymentGatewayId' has null value" : null);

            Console.WriteLine("Verify transaction refund endpoint call is successfull");
            Assert.IsTrue(errorMessage.Length == 0, errorMessage.ToString());
        }

        /// <summary>
        /// This helper method calls transaction refund API endpoint and returns response (use this to test negative cases so that you can verify error in response)
        /// </summary>
        /// <param name="transactionDetails"></param>
        public TransactionRefundResponse TransactionRefundResponse(TransactionDetails transactionDetails)
        {
            var url = transactionDetails.orderId.TransactionRefundUrl(GetAccessToken());
            Console.WriteLine("Trying to refund transaction using url '{0}'", url);
            var raw = _client.Post<TransactionRefundResponse>(url, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;
            return response;
        }

        /// <summary>
        /// Verify Transaction refund endpoint test case works correctly
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteTransactionRefundAndValidateResponse(string market = "US", Starbucks.QaTestUtilities.Models.PaymentService.V1.PaymentType paymentType = Starbucks.QaTestUtilities.Models.PaymentService.V1.PaymentType.Visa, bool isTempPayment = false, bool isGuestPayment = false)
        {
            string userId;
            var cardId = FraudRecoveryCardTestSetup(market, out userId, paymentType, isTempPayment, isGuestPayment);

            TransactionDetails transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            TransactionRefund(transactionDetails);
        }

        /// <summary>
        /// Verify Transaction refund endpoint for invalid order id - verify error code
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteTransactionRefundInvalidOrderId(string market = "US")
        {
            TransactionDetails transactionDetails = new TransactionDetails();
            transactionDetails.orderId = "D3N45A5R69LF4D67H6GE1N6D66";
            TransactionRefundResponse response = TransactionRefundResponse(transactionDetails);
            VerifyTransactionRefundReasonCode(response, ReasonResources.InvalidTransactionReasonCode, ReasonResources.InvalidTransactionReasonMessage);
        }

        /// <summary>
        /// Verify Transaction refund endpoint call multiple times
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteTransactionRefundTwice(string market = "US")
        {
            string userId;
            var cardId = FraudRecoveryCardTestSetup(market, out userId);

            TransactionDetails transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            Console.WriteLine("Transaction refund api - first call");
            TransactionRefund(transactionDetails);
            Console.WriteLine("Transaction refund api - second call");
            TransactionRefund(transactionDetails);
        }

        // Verify transaction refund fails on 'Buy a Card' transaction
        public void ExecuteTransactionRefundOnBuyACard(string market = "US")
        {
            TransactionDetails transactionDetails = new TransactionDetails();
            transactionDetails.orderId = ConfigurationManager.AppSettings["buyACardOrderId"].ToString();
            transactionDetails.transactionAmount = Convert.ToDecimal(ConfigurationManager.AppSettings["buyACardTransactionAmount"]);
            Console.WriteLine("Verify transaction refund fails on 'Buy a Card' transaction");
            TransactionRefundResponse response = TransactionRefundResponse(transactionDetails);
            VerifyTransactionRefundReasonCode(response, ReasonResources.FailedQueryBillingInfoReasonCode, ReasonResources.FailedQueryBillingInfoReasonMessage);
        }

        /// <summary>
        /// Verify Transaction refund endpoint call multiple times
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteTransactionRefundPayPalPayment(string market = "US")
        {
            string userId;
            var cardId = FraudRecoveryCardTestSetup(market, out userId, isPayPalPayment: true);

            TransactionDetails transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            Console.WriteLine("Transaction refund api - first call");
            TransactionRefund(transactionDetails);
        }

        // Verify transaction refund fails on 'eGift' transaction
        public void ExecuteTransactionRefundOnEGift(string market = "US")
        {
            string userId;
            var orderId = FraudRecoveryeGiftTestSetup(market, out userId);

            TransactionDetails transactionDetails = new TransactionDetails();
            transactionDetails.orderId = orderId;
            Console.WriteLine("Verify transaction refund fails on 'eGift' transaction with guest payment");
            TransactionRefundResponse response = TransactionRefundResponse(transactionDetails);
            VerifyTransactionRefundReasonCode(response, ReasonResources.CannotRefundEGiftReasonCode, ReasonResources.CannotRefundEGiftReasonMessage);
        }

        // Verify transaction refund fails on 'eGift' transaction as guest
        public void ExecuteTransactionRefundOnEGiftGuestPayment(string market = "US")
        {
            string userId;
            var orderId = FraudRecoveryeGiftTestSetup(market, out userId, isGuestPayment: true);

            TransactionDetails transactionDetails = new TransactionDetails();
            transactionDetails.orderId = orderId;
            Console.WriteLine("Verify transaction refund fails on 'eGift' transaction with guest payment");
            TransactionRefundResponse response = TransactionRefundResponse(transactionDetails);
            VerifyTransactionRefundReasonCode(response, ReasonResources.OrderWithInvalidPaymentFieldsReasonCode, ReasonResources.OrderWithInvalidPaymentFieldsReasonMessage);
        }

        /// <summary>
        /// Verify Transaction refund endpoint call for deleted payment methood
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteTransactionRefundForDeletedPaymentMethod(string market = "US")
        {
            string userId;
            var cardId = FraudRecoveryCardTestSetup(market, out userId, deletePaymentMethod: true);

            TransactionDetails transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            Console.WriteLine("Verify transaction refund fails when payment method is deleted");
            TransactionRefund(transactionDetails);
        }

        /// <summary>
        /// Verify Transaction refund for temporary payment - verify error code
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteTransactionRefundForTemporaryPayment(string market = "US", Starbucks.QaTestUtilities.Models.PaymentService.V1.PaymentType paymentType = Starbucks.QaTestUtilities.Models.PaymentService.V1.PaymentType.Visa, bool isTempPayment = false, bool isGuestPayment = false)
        {
            string userId;
            var cardId = FraudRecoveryCardTestSetup(market, out userId, paymentType, isTempPayment, isGuestPayment);

            TransactionDetails transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            TransactionRefundResponse response = TransactionRefundResponse(transactionDetails);
            VerifyTransactionRefundReasonCode(response, ReasonResources.UnexpectedInternalErrorQueryPaymentMethodReasonCode, ReasonResources.UnexpectedInternalErrorQueryPaymentMethodReasonMessage);
        }
        #endregion Transaction refund endpoint helper methods

        #region Transaction Freeze endpoint helper methods

        /// <summary>
        /// Helper method for transaction freeze endpoint
        /// </summary>
        /// <param name="transactionDetails"></param>
        public void TransactionFreeze(TransactionDetails transactionDetails)
        {
            var url = transactionDetails.orderId.TransactionFreezeUrl(GetAccessToken());
            Console.WriteLine("Trying to freeze transaction using url '{0}'", url);
            var raw = _client.Post<TransactionFreezeResponse>(url, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;

            var errorMessage = new StringBuilder();
            Assert.IsTrue(response != null, "TransactionFreeze response is null");
            errorMessage.Append(response.Name != "TransactionFreeze" ? string.Format("TransactionFreeze response - 'Name' field has incorrect value '{0}'", response.Name) : null);
            errorMessage.Append(response.Status != OperationStatus.Succeeded ? string.Format("TransactionFreeze response - 'Status' field has incorrect value '{0}'", response.Status) : null);
            errorMessage.Append(response.Id != transactionDetails.orderId ? string.Format("TransactionFreeze response - 'Id' field has incorrect value '{0}'", response.Id) : null);
            errorMessage.Append(response.RequestReceivedDateTime == null ? "TransactionFreeze response - 'RequestReceivedDateTime' field has null value" : null);
            errorMessage.Append(response.TraceNumber == null ? "TransactionFreeze response - 'TraceNumber' field has null value" : null);
            errorMessage.Append(response.Reason != null ? "TransactionFreeze response has error: " + response.Reason.Message : null);
            errorMessage.Append(response.FailureCount != 0 ? string.Format("TransactionFreeze response - 'FailureCount' field is non-zero - '{0}'", response.FailureCount) : null);

            Assert.IsTrue(response.Results[0] != null, "TransactionFreeze response result object is null");
            errorMessage.Append(response.Results[0].Name != "CardFreeze" ? string.Format("TransactionFreeze response - 'Results[0].Name' field has incorrect value '{0}'", response.Results[0].Name) : null);
            errorMessage.Append(response.Results[0].Status != OperationStatus.Succeeded ? string.Format("TransactionFreeze response - 'Results[0].Status' field has incorrect value '{0}'", response.Results[0].Status) : null);
            errorMessage.Append(response.Results[0].Id != Convert.ToString(transactionDetails.cardId) ? string.Format("TransactionFreeze response - 'Svc card id' field has incorrect value Expected: '{0}', Actual: '{1}'", transactionDetails.cardId, response.Results[0].Id) : null);
            errorMessage.Append(response.Results[0].RequestReceivedDateTime == null ? "TransactionFreeze response - 'Results[0].RequestReceivedDateTime' field has null value" : null);
            errorMessage.Append(response.Results[0].TraceNumber == null ? "TransactionFreeze response - 'response.Results[0].TraceNumber' field has null value" : null);
            errorMessage.Append(response.Results[0].Reason != null ? "TransactionFreeze response.Results[0].Reason has error: " + response.Reason.Message : null);

            Assert.IsTrue(response.Results[0].Result != null, "TransactionFreeze response Results[0].Result object is null");
            errorMessage.Append(response.Results[0].Result.Amount != 0 ? string.Format("TransactionFreeze response - 'Amount' field has incorrect value '{0}'", response.Results[0].Result.Amount) : null);
            errorMessage.Append(response.Results[0].Result.BeginningBalance != transactionDetails.cardBalance ? string.Format("TransactionFreeze response - 'BeginningBalance' field has incorrect value Expected: '{0}', Actual: '{1}'", transactionDetails.cardBalance, response.Results[0].Result.BeginningBalance) : null);
            errorMessage.Append(response.Results[0].Result.EndingBalance != transactionDetails.cardBalance ? string.Format("TransactionFreeze response - 'EndingBalance' field has incorrect value Expected: '{0}', Actual: '{1}'", transactionDetails.cardBalance, response.Results[0].Result.EndingBalance) : null);
            errorMessage.Append(response.Results[0].Result.EndingBalanceDateTime == null ? "TransactionFreeze response - 'Results[0].Result.EndingBalanceDateTime' field has null value" : null);
            errorMessage.Append(response.Results[0].Result.LockAmount != transactionDetails.cardBalance ? string.Format("TransactionFreeze response - 'Lock Amount' field has incorrect value '{0}'", response.Results[0].Result.Amount) : null);

            Console.WriteLine("Verify transaction freeze endpoint call is successfull");
            Assert.IsTrue(errorMessage.Length == 0, errorMessage.ToString());
        }

        /// <summary>
        /// This helper method calls transaction freeze API endpoint and returns response (use this to test negative cases so that you can verify error in response)
        /// </summary>
        /// <param name="transactionDetails"></param>
        public TransactionFreezeResponse TransactionFreezeResponse(TransactionDetails transactionDetails)
        {
            var url = transactionDetails.orderId.TransactionFreezeUrl(GetAccessToken());
            Console.WriteLine("Trying to freeze transaction using url '{0}'", url);
            var raw = _client.Post<TransactionFreezeResponse>(url, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;
            return response;
        }

        /// <summary>
        /// Verify transaction freeze endpoint api
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteTransactionFreezeAndValidateResponse(string market = "US")
        {
            string userId;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);

            decimal balance = GetCardBalanceFromDB(cardId);
            Console.WriteLine("make svc card balance zero");
            VerifyRevokeCard(cardId, balance, balance);

            decimal loadAmountValue = 25;
            VerifyCardLoadAmount(cardId, 0, loadAmountValue);

            TransactionDetails transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            TransactionFreeze(transactionDetails);
            Console.WriteLine("cleanup - Transaction unfreeze");
            TransactionUnfreeze(transactionDetails);
        }

        /// <summary>
        /// Verify transaction freeze for invalid order id
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteTransactionFreezeInvalidOrderId(string market = "US")
        {
            TransactionDetails transactionDetails = new TransactionDetails();
            transactionDetails.orderId = "843B255E-3660-4FD0-812B-0A6C8A9FF8B3";
            TransactionFreezeResponse response = TransactionFreezeResponse(transactionDetails);
            VerifyTransactionFreezeReasonCode(response, ReasonResources.InvalidTransactionReasonCode, ReasonResources.InvalidTransactionReasonMessage);
        }

        /// <summary>
        /// Verify transaction freeze for svc card with zero balance
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteTransactionFreezeForZeroBalanceCard(string market = "US")
        {
            string userId;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);

            decimal balance = GetCardBalanceFromDB(cardId);
            Console.WriteLine("make svc card balance zero");
            VerifyRevokeCard(cardId, balance, balance);

            TransactionDetails transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            TransactionFreezeResponse response = TransactionFreezeResponse(transactionDetails);
            VerifyTransactionFreezeReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);

            Console.WriteLine("cleanup - restore balance on svc card");
            VerifyCardLoadAmount(cardId, 0, balance);
        }

        /// <summary>
        /// Verify multiple calls to transaction freeze fail
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteTransactionFreezeTwice(string market = "US")
        {
            string userId;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);

            TransactionDetails transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            Console.WriteLine("Transaction freeze - first call");
            TransactionFreeze(transactionDetails);

            Console.WriteLine("Transaction freeze - second call");
            TransactionFreezeResponse response = TransactionFreezeResponse(transactionDetails);
            VerifyTransactionFreezeReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);

            Console.WriteLine("cleanup - unfreeze transaction");
            TransactionUnfreeze(transactionDetails);
        }

        // Verify transaction freeze fails on 'Buy a Card' transaction
        public void ExecuteTransactionFreezeOnBuyACard(string market = "US")
        {
            TransactionDetails transactionDetails = new TransactionDetails();
            transactionDetails.orderId = ConfigurationManager.AppSettings["buyACardOrderId"].ToString();
            TransactionFreezeResponse response = TransactionFreezeResponse(transactionDetails);
            VerifyTransactionFreezeReasonCode(response, ReasonResources.NotSvcOrderReasonCode, ReasonResources.NotSvcOrderReasonMessage);
        }

        // Verify transaction freeze fails on 'eGift' transaction
        public void ExecuteTransactionFreezeOnEGift(string market = "US")
        {
            TransactionDetails transactionDetails = new TransactionDetails();
            transactionDetails.orderId = ConfigurationManager.AppSettings["eGiftOrderId"].ToString();
            TransactionFreezeResponse response = TransactionFreezeResponse(transactionDetails);
            VerifyTransactionFreezeReasonCode(response, ReasonResources.NotSvcOrderReasonCode, ReasonResources.NotSvcOrderReasonMessage);
        }

        /// <summary>
        /// Verify transaction freeze after card revoke call
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteTransactionFreezeAfterCardRevoke(string market = "US")
        {
            string userId;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);

            InititiliazeCardForFreezeTest(cardId);
            decimal balance = GetCardBalanceFromDB(cardId);
            VerifyRevokeCard(cardId, balance, 10);

            TransactionDetails transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            TransactionFreeze(transactionDetails);
            Console.WriteLine("cleanup - Transaction unfreeze");
            TransactionUnfreeze(transactionDetails);
        }

        /// <summary>
        /// Verify transaction freeze on Zero balance svc card  - verify operation fails
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteTransactionFreezeForZeroBalanceSvcCard(string market = "US")
        {
            string userId;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);

            InititiliazeCardForFreezeTest(cardId);
            decimal balance = GetCardBalanceFromDB(cardId);
            CashOutHelper(cardId);

            TransactionDetails transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            TransactionFreezeResponse response = TransactionFreezeResponse(transactionDetails);
            Assert.IsTrue(response.Status == OperationStatus.Failed, "Transaction freeze did not fail on svc card with zero balance");
        }

        #endregion Transaction freeze endpoint helper methods

        #region Transaction Unfreeze endpoint helper methods

        /// <summary>
        /// Helper method for transaction unfreeze endpoint
        /// </summary>
        /// <param name="transactionDetails"></param>
        public void TransactionUnfreeze(TransactionDetails transactionDetails)
        {
            var url = transactionDetails.orderId.TransactionUnfreezeUrl(GetAccessToken());

            decimal unfreezeRedeemAnount = Convert.ToDecimal(ConfigurationManager.AppSettings["unfreezeRedeemAnount"].ToString());
            decimal beginingBalance = transactionDetails.cardBalance;
            decimal endingBalance = transactionDetails.cardBalance - unfreezeRedeemAnount;

            Console.WriteLine("Trying to unfreeze transaction using url '{0}'", url);
            var raw = _client.Post<TransactionUnfreezeResponse>(url, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;

            var errorMessage = new StringBuilder();
            Assert.IsTrue(response != null, "TransactionUnfreeze response is null");
            errorMessage.Append(response.Name != "TransactionUnfreeze" ? string.Format("TransactionUnfreeze response - 'Name' field has incorrect value '{0}'", response.Name) : null);
            errorMessage.Append(response.Status != OperationStatus.Succeeded ? string.Format("TransactionUnfreeze response - 'Status' field has incorrect value '{0}'", response.Status) : null);
            errorMessage.Append(response.Id != transactionDetails.orderId ? string.Format("TransactionUnfreeze response - 'Id' field has incorrect value '{0}'", response.Id) : null);
            errorMessage.Append(response.RequestReceivedDateTime == null ? "TransactionUnfreeze response - 'RequestReceivedDateTime' field has null value" : null);
            errorMessage.Append(response.TraceNumber == null ? "TransactionUnfreeze response - 'TraceNumber' field has null value" : null);
            errorMessage.Append(response.Reason != null ? "TransactionUnfreeze response has error: " + response.Reason.Message : null);
            errorMessage.Append(response.FailureCount != 0 ? string.Format("TransactionUnfreeze response - 'FailureCount' field is non-zero - '{0}'", response.FailureCount) : null);

            Assert.IsTrue(response.Results[0] != null, "TransactionUnfreeze response result object is null");
            errorMessage.Append(response.Results[0].Name != "CardUnfreeze" ? string.Format("TransactionUnfreeze response - 'Results[0].Name' field has incorrect value '{0}'", response.Results[0].Name) : null);
            errorMessage.Append(response.Results[0].Status != OperationStatus.Succeeded ? string.Format("TransactionUnfreeze response - 'Results[0].Status' field has incorrect value '{0}'", response.Results[0].Status) : null);
            errorMessage.Append(response.Results[0].Id != Convert.ToString(transactionDetails.cardId) ? string.Format("TransactionUnfreeze response - 'Svc card id' field has incorrect value Expected: '{0}', Actual: '{1}'", transactionDetails.cardId, response.Results[0].Id) : null);
            errorMessage.Append(response.Results[0].RequestReceivedDateTime == null ? "TransactionUnfreeze response - 'Results[0].RequestReceivedDateTime' field has null value" : null);
            errorMessage.Append(response.Results[0].TraceNumber == null ? "TransactionUnfreeze response - 'response.Results[0].TraceNumber' field has null value" : null);
            errorMessage.Append(response.Results[0].Reason != null ? "TransactionUnfreeze response.Results[0].Reason has error: " + response.Reason.Message : null);

            Assert.IsTrue(response.Results[0].Result != null, "TransactionUnfreeze response Results[0].Result object is null");
            errorMessage.Append(response.Results[0].Result.Amount != unfreezeRedeemAnount ? string.Format("TransactionUnfreeze response - 'Amount' field has incorrect value '{0}'", response.Results[0].Result.Amount) : null);
            errorMessage.Append(response.Results[0].Result.BeginningBalance != beginingBalance ? string.Format("TransactionUnfreeze response - 'BeginningBalance' field has incorrect value Expected: '{0}', Actual: '{1}'", beginingBalance, response.Results[0].Result.BeginningBalance) : null);
            errorMessage.Append(response.Results[0].Result.EndingBalance != endingBalance ? string.Format("TransactionUnfreeze response - 'EndingBalance' field has incorrect value Expected: '{0}', Actual: '{1}'", endingBalance, response.Results[0].Result.EndingBalance) : null);
            errorMessage.Append(response.Results[0].Result.EndingBalanceDateTime == null ? "TransactionUnfreeze response - 'Results[0].Result.EndingBalanceDateTime' field has null value" : null);
            errorMessage.Append(response.Results[0].Result.LockAmount != 0 ? string.Format("TransactionUnfreeze response - 'Lock Amount' field has incorrect value '{0}'", response.Results[0].Result.Amount) : null);

            Console.WriteLine("Verify transaction unfreeze endpoint call is successfull");
            Assert.IsTrue(errorMessage.Length == 0, errorMessage.ToString());
        }

        /// <summary>
        /// This helper method calls transaction unfreeze API endpoint and returns response (use this to test negative cases so that you can verify error in response)
        /// </summary>
        /// <param name="transactionDetails"></param>
        public TransactionUnfreezeResponse TransactionUnfreezeResponse(TransactionDetails transactionDetails)
        {
            var url = transactionDetails.orderId.TransactionUnfreezeUrl(GetAccessToken());
            Console.WriteLine("Trying to unfreeze transaction using url '{0}'", url);
            var raw = _client.Post<TransactionUnfreezeResponse>(url, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;
            return response;

        }

        /// <summary>
        /// Verify transaction unfreeze endpoint call
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteTransactionUnfreezeAndValidateResponse(string market = "US")
        {
            string userId;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);

            TransactionDetails transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            TransactionFreeze(transactionDetails);

            Console.WriteLine("cleanup - Transaction unfreeze");
            TransactionUnfreeze(transactionDetails);
        }

        /// <summary>
        /// Verify transaction unfreeze for invalid order id
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteTransactionUnfreezeInvalidOrderId(string market = "US")
        {
            TransactionDetails transactionDetails = new TransactionDetails();
            transactionDetails.orderId = "843B255E-3660-4FD0-812B-0A6C8A9FF8B3";
            TransactionUnfreezeResponse response = TransactionUnfreezeResponse(transactionDetails);
            VerifyTransactionUnfreezeReasonCode(response, ReasonResources.InvalidTransactionReasonCode, ReasonResources.InvalidTransactionReasonMessage);
        }

        /// <summary>
        /// Call transaction unfreeze multiple times
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteTransactionUnfreezeTwice(string market = "US")
        {
            string userId;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);

            TransactionDetails transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            Console.WriteLine("Test setup - Transaction freeze");
            TransactionFreeze(transactionDetails);

            Console.WriteLine("Transaction unfreeze - first call");
            TransactionUnfreeze(transactionDetails);
            Console.WriteLine("Transaction unfreeze - second call");
            TransactionUnfreezeResponse response = TransactionUnfreezeResponse(transactionDetails);
            VerifyTransactionUnfreezeReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);      
        }

        // Verify transaction unfreeze fails on 'Buy a Card' transaction
        public void ExecuteTransactionUnfreezeOnBuyACard(string market = "US")
        {
            TransactionDetails transactionDetails = new TransactionDetails();
            transactionDetails.orderId = ConfigurationManager.AppSettings["buyACardOrderId"].ToString();
            TransactionUnfreezeResponse response = TransactionUnfreezeResponse(transactionDetails);
            VerifyTransactionUnfreezeReasonCode(response, ReasonResources.NotSvcOrderReasonCode, ReasonResources.NotSvcOrderReasonMessage);
        }

        // Verify transaction unfreeze fails on 'eGift' transaction
        public void ExecuteTransactionUnfreezeOnEGift(string market = "US")
        {
            TransactionDetails transactionDetails = new TransactionDetails();
            transactionDetails.orderId = ConfigurationManager.AppSettings["eGiftOrderId"].ToString();
            TransactionUnfreezeResponse response = TransactionUnfreezeResponse(transactionDetails);
            VerifyTransactionUnfreezeReasonCode(response, ReasonResources.NotSvcOrderReasonCode, ReasonResources.NotSvcOrderReasonMessage);
        }
        
        /// <summary>
        /// Verify transaction unfreeze after svc card freeze operation
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteTransactionUnfreezeAfterSvcCardFreeze(string market = "US")
        {
            string userId;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);
            InititiliazeCardForFreezeTest(cardId);
            decimal balance = GetCardBalanceFromDB(cardId);
            CardFreeze(cardId, balance);

            TransactionDetails transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            TransactionUnfreeze(transactionDetails);
        }

        /// <summary>
        /// Transaction UnFreeze after Transaction freeze, card unfreeze and card freeze operations
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteTransactionUnfreezeAfterSvcCardFreezeAndUnfreeze(string market = "US")
        {
            string userId;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);
            InititiliazeCardForFreezeTest(cardId);
            TransactionDetails transactionDetails = this.GetTransactionDetails(userId, Convert.ToInt32(cardId));
            Console.WriteLine("Test setup - Transaction freeze");
            TransactionFreeze(transactionDetails);

            Console.WriteLine("Test setup - card unfreeze");
            decimal balance = GetCardBalanceFromDB(cardId);
            CardUnfreeze(cardId, balance);
            Console.WriteLine("Test setup - card freeze");
            CardFreeze(cardId, balance);

            TransactionUnfreeze(transactionDetails);
        }
        
        #endregion Transaction unfreeze endpoint helper methods

        #region miscellaneous helper methods
        /// <summary>
        /// This helper method retrieves transaction related information from Order related tables
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="cardId"></param>
        /// <returns></returns>
        private TransactionDetails GetTransactionDetails(string userId, int cardId)
        {
            string sqlConnectionString = ConfigurationManager.ConnectionStrings["StarbucksCommerceDb_QA"].ToString();
            TransactionDetails obj = new TransactionDetails();

            Console.WriteLine("Trying to get transaction details from order form header table");
            string sqlQuery = @"select distinct(c.CardId), OFH.order_id, st.TransactionID, st.amount, c.Number, c.Pin, cb.Balance, c.Currency, c. SubMarketCode, OFH.d_DateCreated
                                From starbucks_profiles.dbo.Card as c with (nolock) 
                                INNER JOIN starbucks_profiles.dbo.CardBalance as cb with (nolock)
                                       ON c.CardId = cb.CardId
                                INNER JOIN card_transactions.dbo.svc_transaction as st with (nolock)
                                       ON c.Number = st.CardNo
                                INNER JOIN starbucks_commerce.dbo.OrderFormLineItems as OFLI with (nolock)
                                       ON OFLI.svc_trans_id = st.TransactionID
                                INNER JOIN starbucks_commerce.dbo.OrderFormHeader OFH with (nolock)
                                       ON OFH.OrderForm_Id = OFLI.orderform_id
                                Where OFH.User_Id = @userid and c.CardId = @cardId order by OFH.d_DateCreated desc";

            using (SqlConnection conn = new SqlConnection(sqlConnectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(sqlQuery, conn);
                    cmd.Parameters.AddWithValue("@userid", userId);
                    cmd.Parameters.AddWithValue("@cardId", cardId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        obj.orderId = reader.GetString(reader.GetOrdinal("order_id"));
                        obj.cardId = cardId;
                        obj.cardNumber = reader.GetString(reader.GetOrdinal("Number"));
                        obj.cardBalance = reader.GetDecimal(reader.GetOrdinal("Balance"));
                        obj.transactionAmount = reader.GetDecimal(reader.GetOrdinal("amount"));
                        Console.WriteLine("Transaction details from order form header table - OrderId '{0}' Card Number '{1}'  Card balance '{2}'", obj.orderId, obj.cardNumber, obj.cardBalance);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed to get transaction details from order form header table");
                    System.Console.WriteLine("Database connection failed with error {0}", ex.Message);
                }
            }
            return obj;
        }

        /// <summary>
        /// This helper methods gets userid and cardid from config file for Freeze and Unfreeze tests
        /// </summary>
        /// <param name="market"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        string GetCardIdFromConfigurationFile(string market, out string userId)
        {
            var cardId = string.Empty;
            userId = string.Empty;

            switch (market)
            {
                case "US":
                    cardId = ConfigurationManager.AppSettings["svcCardIdUS"].ToString();
                    userId = ConfigurationManager.AppSettings["userIdUS"].ToString();
                    break;

                default:
                    Assert.IsTrue(true, "'{0}' market is not supported", market);
                    break;
            }
            return cardId;
        }

        /// <summary>
        /// Helper method to verify reason code
        /// </summary>
        public bool VerifyTransactionRevokeReasonCode(TransactionRevokeResponse response, string reasonCode, string reasonMessage)
        {
            Assert.IsTrue(response.Status == OperationStatus.Failed, string.Format("Response status is incorrect. Expected:'{0}' Actual:'{1}'", OperationStatus.Failed, response.Status));
            Assert.IsTrue(response.TraceNumber != null, "'TraceNumber' field has null value");

            // if reason object is not in the main response, then look into collection for reason code
            if (response.Reason != null)
            {
                Assert.IsTrue(response.Reason.Code == reasonCode, "Reason code does not match - Expected :'{0}' Actual :'{1}'", reasonCode, response.Reason.Code);
                Assert.IsTrue(response.Reason.Message == reasonMessage, "Reason code does not match - Expected :'{0}' Actual :'{1}'", reasonMessage, response.Reason.Message);
                Assert.IsTrue(response.Results.Count == 0, "Response result object count is not zero");
            }
            else
            {
                Assert.IsTrue(response.FailureCount != 0, "Failure count value is non-zero");
                Assert.IsTrue(response.Results[0].Reason.Code == reasonCode, "Reason code does not match - Expected :'{0}' Actual :'{1}'", reasonCode, response.Results[0].Reason.Code);
                Assert.IsTrue(response.Results[0].Reason.Message == reasonMessage, "Reason code does not match - Expected :'{0}' Actual :'{1}'", reasonMessage, response.Results[0].Reason.Message);
            }
            return true;
        }

        /// <summary>
        /// Helper method to verify reason code
        /// </summary>
        public bool VerifyTransactionRefundReasonCode(TransactionRefundResponse response, string reasonCode, string reasonMessage)
        {
            Assert.IsTrue(response.Reason.Code == reasonCode, "Reason code does not match - Expected :'{0}' Actual :'{1}'", reasonCode, response.Reason.Code);
            Assert.IsTrue(response.Reason.Message == reasonMessage, "Reason code does not match - Expected :'{0}' Actual :'{1}'", reasonMessage, response.Reason.Message);
            Assert.IsTrue(response.Status == OperationStatus.Failed, string.Format("Response status is incorrect. Expected:'{0}' Actual:'{1}'", OperationStatus.Failed, response.Status));
            Assert.IsTrue(response.Result == null, "Response result object is not null");
            Assert.IsTrue(response.TraceNumber != null, "'TraceNumber' field has null value");
            return true;
        }

        /// <summary>
        /// Helper method to verify reason code
        /// </summary>
        public bool VerifyTransactionFreezeReasonCode(TransactionFreezeResponse response, string reasonCode, string reasonMessage)
        {
            Assert.IsTrue(response.Status == OperationStatus.Failed, string.Format("Response status is incorrect. Expected:'{0}' Actual:'{1}'", OperationStatus.Failed, response.Status));
            Assert.IsTrue(response.TraceNumber != null, "'TraceNumber' field has null value");

            // if reason object is not in the main response, then look into collection for reason code
            if (response.Reason != null)
            {
                Assert.IsTrue(response.Reason.Code == reasonCode, "Reason code does not match - Expected :'{0}' Actual :'{1}'", reasonCode, response.Reason.Code);
                Assert.IsTrue(response.Reason.Message == reasonMessage, "Reason code does not match - Expected :'{0}' Actual :'{1}'", reasonMessage, response.Reason.Message);
                Assert.IsTrue(response.Results.Count == 0, "Response result object count is not zero");
            }
            else
            {
                Assert.IsTrue(response.FailureCount != 0, "Failure count value is non-zero");
                Assert.IsTrue(response.Results[0].Reason.Code == reasonCode, "Reason code does not match - Expected :'{0}' Actual :'{1}'", reasonCode, response.Results[0].Reason.Code);
                Assert.IsTrue(response.Results[0].Reason.Message == reasonMessage, "Reason code does not match - Expected :'{0}' Actual :'{1}'", reasonMessage, response.Results[0].Reason.Message);
            }
            return true;
        }

        /// <summary>
        /// Helper method to verify reason code
        /// </summary>
        public bool VerifyTransactionUnfreezeReasonCode(TransactionUnfreezeResponse response, string reasonCode, string reasonMessage)
        {
            Assert.IsTrue(response.Status == OperationStatus.Failed, string.Format("Response status is incorrect. Expected:'{0}' Actual:'{1}'", OperationStatus.Failed, response.Status));
            Assert.IsTrue(response.TraceNumber != null, "'TraceNumber' field has null value");

            // if reason object is not in the main response, then look into collection for reason code
            if (response.Reason != null)
            {
                Assert.IsTrue(response.Reason.Code == reasonCode, "Reason code does not match - Expected :'{0}' Actual :'{1}'", reasonCode, response.Reason.Code);
                Assert.IsTrue(response.Reason.Message == reasonMessage, "Reason code does not match - Expected :'{0}' Actual :'{1}'", reasonMessage, response.Reason.Message);
                Assert.IsTrue(response.Results.Count == 0, "Response result object count is not zero");
            }
            else
            {
                Assert.IsTrue(response.FailureCount != 0, "Failure count value is non-zero");
                Assert.IsTrue(response.Results[0].Reason.Code == reasonCode, "Reason code does not match - Expected :'{0}' Actual :'{1}'", reasonCode, response.Results[0].Reason.Code);
                Assert.IsTrue(response.Results[0].Reason.Message == reasonMessage, "Reason code does not match - Expected :'{0}' Actual :'{1}'", reasonMessage, response.Results[0].Reason.Message);
            }
            return true;
        }

        #endregion miscellaneous helper methods
    }
}
