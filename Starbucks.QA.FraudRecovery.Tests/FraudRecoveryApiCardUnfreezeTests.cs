﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FraudRecovery.Tests.Integration;

namespace Starbucks.QA.FraudRecovery.Tests
{
    [TestClass]
    public class FraudRecoveryApiCardUnfreezeTests : FraudRecoveryCardTestBase
    {
        #region BVT Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(1)]
        [Description("FRA - verify card unfreeze for US svc card")]
        public void FraudRecoveryUSUnfreezeCard()
        {
            ExecuteUnfreezeCardAndValidateResponse();
        }
        #endregion BVT Tests

        #region Market Tests

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card unfreeze for CA svc card")]
        public void FraudRecoveryCAUnfreezeCard()
        {
            ExecuteUnfreezeCardAndValidateResponse(caMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card unfreeze for UK svc card")]
        public void FraudRecoveryUKUnfreezeCard()
        {
            ExecuteUnfreezeCardAndValidateResponse(ukMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card unfreeze for IE svc card")]
        public void FraudRecoveryIEUnfreezeCard()
        {
            ExecuteUnfreezeCardAndValidateResponse(ieMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card unfreeze for DE svc card")]
        public void FraudRecoveryDEUnfreezeCard()
        {
            ExecuteUnfreezeCardAndValidateResponse(deMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card unfreeze for FR svc card")]
        public void FraudRecoveryFRUnfreezeCard()
        {
            ExecuteUnfreezeCardAndValidateResponse(frMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card unfreeze for BR svc card")]
        public void FraudRecoveryBRUnfreezeCard()
        {
            ExecuteUnfreezeCardAndValidateResponse(brMarket);
        }
        #endregion Market Tests

        #region FVT Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card unfreeze for invalid cardid")]
        public void FraudRecoveryUSUnfreezeCardInvalidCardId()
        {
            ExecuteUnfreezeCardInvalidCardId();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card unfreeze for second call")]
        public void FraudRecoveryUSUnfreezeCardTwice()
        {
            ExecuteUnfreezeCardTwice();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card funreeze for unregisterd card")]
        public void FraudRecoveryUnfreezeCardUnregisteredCard()
        {
            ExecuteCardUnfreezeUnregisteredCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify unfreeze for Register-Unregister_Registered svc card")]
        public void FraudRecoveryUnFreezeCardReRegisterCard()
        {
            ExecuteCardUnFreezeReRegisteredCard();
        }

        //[TestMethod]
        //[TestCategory("FraudRecoveryApi")]
        //[Priority(2)]
        //[Description("FRA - Verify unfreeze for Associated svc card")]
        //public void FraudRecoveryUnFreezeCardAssociatedCard()
        //{
        //    ExecuteUnFreezeCardAssociatedCard();
        //}

        [TestMethod]
        [Ignore]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card funreeze for stolen card")]
        public void FraudRecoveryUnfreezeStolenCard()
        {
            ExecuteCardUnfreezeStolenCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Call Unfreeze on svc card that is not freezed - verify error code")]
        public void FraudRecoveryUnfreezeCardNotFreezed()
        {
            ExecuteCardUnfreezeCardNotFreezed();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card freeze and unfreeze multiple times")]
        public void FraudRecoveryFreezeAndUnfreezeCardMultipleTimes()
        {
            ExecuteCardFreezeAndUnfreezeMultipleTimes();
        }

        #endregion FVT Tests
    }
}
