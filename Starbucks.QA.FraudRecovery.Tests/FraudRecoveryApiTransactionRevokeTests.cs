﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FraudRecovery.Tests.Integration;

namespace Starbucks.QA.FraudRecovery.Tests
{
    [TestClass]
    public class FraudRecoveryApiTransactionRevokeTests : FraudRecoveryTransactionTestBase
    {
        #region BVT Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(1)]
        [Description("FRA - Verify Transaction revoke API for US market")]
        public void FraudRecoveryUSTransactionRevoke()
        {
            ExecuteTransactionRevokeAndValidateResponse();
        }
        #endregion BVT Tests

        #region Market Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Transaction revoke API for CA market")]
        public void FraudRecoveryCATransactionRevoke()
        {
            ExecuteTransactionRevokeAndValidateResponse(caMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Transaction revoke API for UK market")]
        public void FraudRecoveryUKTransactionRevoke()
        {
            ExecuteTransactionRevokeAndValidateResponse(ukMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Transaction revoke API for IE market")]
        public void FraudRecoveryIETransactionRevoke()
        {
            ExecuteTransactionRevokeAndValidateResponse(ieMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Transaction revoke API for DE market")]
        public void FraudRecoveryDETransactionRevoke()
        {
            ExecuteTransactionRevokeAndValidateResponse(deMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Transaction revoke API for FR market")]
        public void FraudRecoveryFRTransactionRevoke()
        {
            ExecuteTransactionRevokeAndValidateResponse(frMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Transaction revoke API for BR market")]
        public void FraudRecoveryBRTransactionRevoke()
        {
            ExecuteTransactionRevokeAndValidateResponse(brMarket);
        }
        #endregion Market Tests

        #region FVT Tests

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Call Transaction revoke successively")]
        public void FraudRecoveryTransactionTwice()
        {
            ExecuteTransactionRevokeTwice();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Call Transaction revoke on svc card that has balance less than transaction amount")]
        public void FraudRecoveryTransactionRevokeLowBalanceSvcCard()
        {
            ExecuteTransactionRevokeLowBalanceSvcCard();
        }
        
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Call Transaction revoke on svc card that has Zero balance")]
        public void FraudRecoveryTransactionRevokeZeroBalanceSvcCard()
        {
            ExecuteTransactionRevokeZeroBalanceSvcCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Call Transaction revoke on invalid order id")]
        public void FraudRecoveryTransactionRevokeInvalidOrderId()
        {
            ExecuteTransactionRevokeInvalidOrderId();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(3)]
        [Description("FRA - Call Transaction revoke on 'Buy a Card' order")]
        public void FraudRecoveryTransactionRevokeOnBuyACard()
        {
            ExecuteTransactionRevokeOnBuyACard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(3)]
        [Description("FRA - Call Transaction revoke on 'eGift' order")]
        public void FraudRecoveryTransactionRevokeOnEGiftCard()
        {
            ExecuteTransactionRevokeOnEGift();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Call Transaction revoke after transaction freeze and unfreeze operations")]
        public void FraudRecoveryTransactionRevokeAfterFreezeAndUnfreeze()
        {
            ExecuteTransactionRevokeAfterFreezeAndUnfreeze();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Call Transaction revoke after transaction freeze operation - verify error code")]
        public void FraudRecoveryTransactionRevokeAfterFreeze()
        {
            ExecuteTransactionRevokeAfterTransactionFreeze();
        }
        #endregion FVT Tests
    }
}
