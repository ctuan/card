﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FraudRecovery.Tests.Integration;

namespace Starbucks.QA.FraudRecovery.Tests
{
    [TestClass]
    public class FraudRecoveryApiCardGetBalanceTests : FraudRecoveryCardTestBase
    {
        #region BVT Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(1)]
        [Description("FRA - Verify card balance for US svc card")]
        public void FraudRecoveryUSCardGetBalance()
        {
            ExecuteCardGetBalanceAndValidateResponse();
        }
        #endregion BVT Tests

        #region Market Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card balance for CA svc card")]
        public void FraudRecoveryCACardGetBalance()
        {
            ExecuteCardGetBalanceAndValidateResponse(caMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card balance for UK svc card")]
        public void FraudRecoveryUkCardGetBalance()
        {
            ExecuteCardGetBalanceAndValidateResponse(ukMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card balance for IE svc card")]
        public void FraudRecoveryIECardGetBalance()
        {
            ExecuteCardGetBalanceAndValidateResponse(ieMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card balance for DE svc card")]
        public void FraudRecoveryDECardGetBalance()
        {
            ExecuteCardGetBalanceAndValidateResponse(deMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card balance for FR svc card")]
        public void FraudRecoveryFRCardGetBalance()
        {
            ExecuteCardGetBalanceAndValidateResponse(frMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card balance for BR svc card")]
        public void FraudRecoveryBRCardGetBalance()
        {
            ExecuteCardGetBalanceAndValidateResponse(brMarket);
        }

        #endregion Market Tests

        #region FVT Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify error for invalid svc card")]
        public void FraudRecoveryCardGetBalanceInvalidCardId()
        {
            ExecuteCardGetBalancInvalidCardId();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify balance for unregistered svc card")]
        public void FraudRecoveryCardGetBalanceUnregisteredCard()
        {
            ExecuteCardGetBalanceUnregisteredCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card balance for Register-Unregister_Registered svc card")]
        public void FraudRecoveryCardGetBalanceReRegisterCard()
        {
            ExecuteCardGetBalanceReRegisteredCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card balance for Associated svc card")]
        public void FraudRecoveryCardGetBalanceAssociatedCard()
        {
            ExecuteCardGetBalanceAssociatedCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify balance for stolen/lost svc card")]
        [Ignore]
        public void FraudRecoveryCardGetBalanceStolenCard()
        {
            ExecuteCardGetBalanceStolenCard();
        }
        #endregion        
    }
}
