﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FraudRecovery.Tests.Integration;

namespace Starbucks.QA.FraudRecovery.Tests
{
    [TestClass]
    public class FraudRecoveryApiTransactionRefundTests : FraudRecoveryTransactionTestBase
    {
        #region BVT Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(1)]
        [Description("FRA - Verify Transaction Refund API for US market")]
        public void FraudRecoveryUSTransactionRefund()
        {
            ExecuteTransactionRefundAndValidateResponse();
        }
        #endregion BVT Tests

        #region market tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Transaction Refund API for CA market")]
        public void FraudRecoveryCATransactionRefund()
        {
            ExecuteTransactionRefundAndValidateResponse(caMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Transaction Refund API for UK market")]
        public void FraudRecoveryUKTransactionRefund()
        {
            ExecuteTransactionRefundAndValidateResponse(ukMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Transaction Refund API for IE market")]
        public void FraudRecoveryIETransactionRefund()
        {
            ExecuteTransactionRefundAndValidateResponse(ieMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Transaction Refund API for DE market")]
        public void FraudRecoveryDETransactionRefund()
        {
            ExecuteTransactionRefundAndValidateResponse(deMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Transaction Refund API for FR market")]
        public void FraudRecoveryFRTransactionRefund()
        {
            ExecuteTransactionRefundAndValidateResponse(frMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Transaction Refund API for BR market")]
        public void FraudRecoveryBRTransactionRefund()
        {
            ExecuteTransactionRefundAndValidateResponse(brMarket);
        }
        #endregion market tests

        #region FVT Tests

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(3)]
        [Description("FRA - Verify Transaction Refund API for Visa card payment")]
        public void FraudRecoveryTransactionRefundVisaCard()
        {
            ExecuteTransactionRefundAndValidateResponse(usMarket, QaTestUtilities.Models.PaymentService.V1.PaymentType.Visa);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(3)]
        [Description("FRA - Verify Transaction Refund API for Master card payment")]
        public void FraudRecoveryTransactionRefundMasterCard()
        {
            ExecuteTransactionRefundAndValidateResponse(usMarket, QaTestUtilities.Models.PaymentService.V1.PaymentType.MasterCard);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(3)]
        [Description("FRA - Verify Transaction Refund API for Amex card payment")]
        public void FraudRecoveryTransactionRefundAmexCard()
        {
            ExecuteTransactionRefundAndValidateResponse(usMarket, QaTestUtilities.Models.PaymentService.V1.PaymentType.Amex);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(3)]
        [Description("FRA - Verify Transaction Refund API for Discover card payment")]
        public void FraudRecoveryTransactionRefundDiscoverCard()
        {
            ExecuteTransactionRefundAndValidateResponse(usMarket, QaTestUtilities.Models.PaymentService.V1.PaymentType.Discover);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(3)]
        [Description("FRA - Verify Transaction Refund API for Visa checkout payment")]
        public void FraudRecoveryTransactionRefundVisaChecout()
        {
            ExecuteTransactionRefundAndValidateResponse(usMarket, QaTestUtilities.Models.PaymentService.V1.PaymentType.VisaCheckout);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(3)]
        [Description("FRA - Verify Transaction Refund for paypal payment fails")]
        public void FraudRecoveryTransactionRefundPayPalPayment()
        {
            ExecuteTransactionRefundPayPalPayment();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(3)]
        [Description("FRA - Verify transaction refund fails on 'eGift' transaction")]
        public void FraudRecoveryTransactionRefundOnEGift()
        {
            ExecuteTransactionRefundOnEGift();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(3)]
        [Description("FRA - Verify transaction refund fails on 'eGift' transaction as guest")]
        public void FraudRecoveryTransactionRefundOnEGiftGuestPayment()
        {
            // TODO: fails to create eGift order
            ExecuteTransactionRefundOnEGiftGuestPayment();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Transaction Refund API for invalid order id - verify error code")]
        public void FraudRecoveryTransactionRefundInvalidOrderId()
        {
            ExecuteTransactionRefundInvalidOrderId();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Call Transaction Refund twice - should succeed")]
        public void FraudRecoveryTransactionRefundTwice()
        {
            ExecuteTransactionRefundTwice();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(3)]
        [Description("FRA - Call Transaction Refund on 'Buy a Card' order - verify error code")]
        public void FraudRecoveryTransactionRefundOnBuyACard()
        {
            // TODO: verify error code 700
            ExecuteTransactionRefundOnBuyACard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(1)]
        [Description("FRA - Verify Transaction Refund API for deleted payment method")]
        public void FraudRecoveryTransactionRefundDeletedPaymentMethod()
        {
             ExecuteTransactionRefundForDeletedPaymentMethod();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(3)]
        [Description("FRA - Verify Transaction Refund API for temporary payment - verify error code")]
        public void FraudRecoveryTransactionRefundTemporaryPayment()
        {
            // TODO: Investigate if this should succeed or not.
            ExecuteTransactionRefundForTemporaryPayment(usMarket, QaTestUtilities.Models.PaymentService.V1.PaymentType.Visa, isTempPayment: true);
        }

        #endregion FVT Tests
    }
}
