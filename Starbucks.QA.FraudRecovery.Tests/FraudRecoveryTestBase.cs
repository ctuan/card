﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.Api.Sdk.WebRequestWrapper;
using Starbucks.QaTestUtilities.Api;
using Starbucks.QaTestUtilities.Sql;
using System.Collections.Generic;
using System.Linq;
using Starbucks.QaTestUtilities.Models.Reputation;
using System.Net;
using RestSharp;
using Bom = Starbucks.Platform.Bom.Shared;
using Starbucks.QaTestUtilities.Models.PaymentService.V1;
using System.Data.SqlClient;
using System.Configuration;
using Starbucks.QaTestUtilities.Models.EgiftAPI.V1;
using Starbucks.QaTestUtilities.Models.Reputation;
using Starbucks.QaTestUtilities.Models.Oauth20;
using System.Security.Cryptography;
using System.Text;


namespace Starbucks.QA.FraudRecovery.Tests
{  
    public class FraudRecoveryTestBase
    {
        protected const string usMarket = "US";
        protected const string caMarket = "CA";
        protected const string ukMarket = "UK";
        protected const string ieMarket = "IE";
        protected const string deMarket = "DE";
        protected const string frMarket = "FR";
        protected const string brMarket = "BR";

        protected Dictionary<string, string> currencyCodes = new Dictionary<string, string>
    	{
            {"US", "USD"},
            {"CA", "CAD"}, 
            {"UK", "GBP"}, 
            {"IE", "EUR"}, 
            {"DE", "EUR"},
            {"FR", "EUR"},
            {"BR", "BRL"}
    	};

        #region Initialization
        protected decimal InitCardBalance = 97.5m;
        protected TestHelper _testHelper;
        protected IWebRequestWrapper _client;
        protected const string invalidCardId = "00000000";
        protected const string vcId = "1121202451359920701";
        protected string accessToken = string.Empty;
        protected string ApiKey = "g8gp7w95652ceqvhdgryjnfn";
        protected string ClientID = "g8gp7w95652ceqvhdgryjnfn";
        protected string ClientSecret = "zBErrBvxy3vW7NwgNJGtqrGr";
        public FraudRecoveryTestBase()
        {
            _testHelper = new TestHelper();
            _client = new WebRequestWrapper();
        }
        #endregion Initialization

        #region Fraud Recovery API Helper methods
        /// <summary>
        /// Setup card for Fraud Recovery API testing
        /// </summary>
        /// <param name="market"></param>
        /// <returns></returns>
        public string FraudRecoveryCardTestSetup(string market)
        {
            string userId;
            return FraudRecoveryCardTestSetup(market, out userId);
        }

        /// <summary>
        /// Setup card and get user info as well
        /// </summary>
        /// <param name="market"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string FraudRecoveryCardTestSetup(string market, out string userId, QaTestUtilities.Models.PaymentService.V1.PaymentType paymentType = QaTestUtilities.Models.PaymentService.V1.PaymentType.Visa, bool isTempPayment = false, bool isGuestPayment = false, bool isPayPalPayment = false, bool isAssociatedCard = false, bool deletePaymentMethod = false)
        {
            userId = _testHelper.Account.GetOrCreateUser(_testHelper.CreateRandomTestEmail(), market: market);
            Starbucks.QaTestUtilities.Models.Card.V1.StarbucksCardResponse cardInfo = new Starbucks.QaTestUtilities.Models.Card.V1.StarbucksCardResponse();

            Risk riskFull = new Risk();
            riskFull.Platform = "Web";

            var paymentId = "";

            if (isPayPalPayment)
            {
                paymentId = _testHelper.Payment.CreatePaypalPaymentMethod(userId);
            }
            else if (isGuestPayment)
            {
                paymentId = _testHelper.Payment.CreateGuestPaymentMethod(paymentType, market);
            }
            else if (isTempPayment)
            {
                // Create temporory payment
                GuestPaymentMethodRequest modifyRequest = new GuestPaymentMethodRequest()
                {
                    CountrySubdivision = "WA",
                    AddressLine1 = string.Empty,
                    AddressLine2 = String.Empty,
                    City = string.Empty,
                    CountryCode = String.Empty
                };

                // Get address id for this user
                modifyRequest.AddressId = this.GetAddresIdFromDB(userId);
                paymentId = _testHelper.Payment.CreateGuestPaymentMethod(paymentType, market, modifyRequest);
            }
            else
            {
                paymentId = _testHelper.Payment.GetOrCreateCreditCardPaymentMethod(riskFull, userId, paymentType, true, vcId: vcId);
            }

            if (isAssociatedCard)
            {
                cardInfo = _testHelper.Card.AssociateRandomCard(null, userId, RepositoryCardType.Physical, RepositoryCardStatus.Active, market, 3);            
            }
            else
            {
                cardInfo = _testHelper.Card.RegisterRandomCard(null, userId, RepositoryCardType.Physical, RepositoryCardStatus.Active, market, 3);
            }

            _testHelper.Card.ValidateResponse = false;
            Assert.IsTrue((_testHelper.Card.LastResponse.StatusCode == HttpStatusCode.OK || _testHelper.Card.LastResponse.StatusCode == HttpStatusCode.Created)
                && _testHelper.Card.LastResponse.ResponseStatus == ResponseStatus.Completed, "Bad response");
            _testHelper.Card.ReloadSbuxCardRandomAmount(riskFull, userId, cardInfo.CardNumber, paymentId);
            Assert.AreEqual(HttpStatusCode.OK, _testHelper.Card.LastResponse.StatusCode, "Card reload failed"); 
            Assert.AreEqual(_testHelper.Card.LastResponse.ResponseStatus, ResponseStatus.Completed, "Incorrect card reload response status");

            if (deletePaymentMethod)
            {
                bool isSuccess = _testHelper.Payment.DeletePaymentMethod(userId, paymentId);
                if (isSuccess)
                {
                    Console.WriteLine("payment id '{0}' is deleted",paymentId);
                }
                else
                {
                    Console.WriteLine("Could not delete payment id '{0}'",paymentId);            
                }
            }
            var decryptedCardId = _testHelper.DbHelper.Card.GetDecryptedId(cardInfo.CardId);
            InitCardBalance = GetCardBalanceFromDB(decryptedCardId);
            return decryptedCardId;
        }

        /// <summary>
        /// Setup eGift order
        /// </summary>
        /// <param name="market"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string FraudRecoveryeGiftTestSetup(string market, out string userId, QaTestUtilities.Models.PaymentService.V1.PaymentType paymentType = QaTestUtilities.Models.PaymentService.V1.PaymentType.Visa, bool isTempPayment = false, bool isGuestPayment = false)
        {
            userId = _testHelper.Account.GetOrCreateUser(_testHelper.CreateRandomTestEmail(), market: market);
            Risk riskFull = new Risk();
            riskFull.Platform = "Web";

            var paymentId = "";

            if (isGuestPayment)
            {
                paymentId = _testHelper.Payment.CreateGuestPaymentMethod(paymentType, market);
            }
            else if (isTempPayment)
            {
                // Create temporory payment
                GuestPaymentMethodRequest modifyRequest = new GuestPaymentMethodRequest()
                {
                    CountrySubdivision = "WA",
                    AddressLine1 = string.Empty,
                    AddressLine2 = String.Empty,
                    City = string.Empty,
                    CountryCode = String.Empty
                };

                // Get address id for this user
                modifyRequest.AddressId = this.GetAddresIdFromDB(userId);
                paymentId = _testHelper.Payment.CreateGuestPaymentMethod(paymentType, market, modifyRequest);
            }
            else
            {
                paymentId = _testHelper.Payment.GetOrCreateCreditCardPaymentMethod(riskFull, userId, paymentType, true, vcId: vcId);
            }

            Console.WriteLine("Trying to create eGift order");
            var response = _testHelper.Egift.PlaceRandomEgiftByCreditCardWithRisk(riskFull, null, userId, paymentId, currency: "USD", market: "US", locale: "en-US");
            Assert.IsTrue(response.OrderNumber != "", "Order number is empty");
            Assert.IsTrue(response.CashstarOrderNumber != "", "Cashstart rrder number is empty");
            return response.OrderNumber;
        }        

        /// <summary>
        /// Helper method to register svc card
        /// </summary>
        /// <param name="cardId"></param>
        public void RegisterSvcCard(string userId, string cardId)
        {
            Console.WriteLine("Trying to register svc card");
            var encryptedCardId = _testHelper.DbHelper.Card.GetEncryptedId(cardId);
            var decryptedCardNumber = _testHelper.DbHelper.Card.GetDecyptedCardNumberByCardIdFromDb(encryptedCardId);
            var decryptedCardPin = _testHelper.DbHelper.Card.GetDecryptedCardPinByCardIdFromDb(Convert.ToInt32(cardId));
            Risk riskFull = new Risk();
            riskFull.Platform = "Web";
            _testHelper.Card.RegisterCard(riskFull, userId, decryptedCardNumber, decryptedCardPin);
        }

        /// <summary>
        /// Helper method to unregister svc card
        /// </summary>
        /// <param name="cardId"></param>
        public void UnregisterSvcCard(string cardId)
        {
            Console.WriteLine("Trying to unregister svc card");
            var encryptedCardId = _testHelper.DbHelper.Card.GetEncryptedId(cardId);
            var decryptedCardNumber = _testHelper.DbHelper.Card.GetDecyptedCardNumberByCardIdFromDb(encryptedCardId);
            Risk riskFull = new Risk();
            riskFull.Platform = "Web";
            _testHelper.Card.UnregisterCard(riskFull, decryptedCardNumber, false);
        }

        /// <summary>
        /// Ger addressid for user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetAddresIdFromDB(string userId)
        {

            string sqlConnectionString = ConfigurationManager.ConnectionStrings["StarbucksProfilesDb"].ToString();
            Console.WriteLine("Trying to get address");
            string sqlQuery = @"select * from starbucks_profiles.dbo.Addresses where g_id=@userid";
            string addressId = string.Empty;

            using (SqlConnection conn = new SqlConnection(sqlConnectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(sqlQuery, conn);
                    cmd.Parameters.AddWithValue("@userid", userId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        addressId = reader.GetString(reader.GetOrdinal("g_address_id"));
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed to get address for user");
                    System.Console.WriteLine("Database connection failed with error {0}", ex.Message);
                }

                return addressId;
            }
        }

        /// <summary>
        /// Verify card balance from DB
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="expectedAmount"></param>
        /// <returns>returns the balance of the card</returns>
        public void VerifyCardBalanceFromDB(string cardId, decimal expectedBalance)
        {
            Console.WriteLine("Trying to verify card balance from DB");
            var cardBalance = _testHelper.DbHelper.Card.GetBalanceByCardIdFromDb(Convert.ToInt32(cardId));

            Assert.IsTrue(cardBalance == expectedBalance, "Card balance does not match - Expected :'{0}' Actual :'{1}'", expectedBalance, cardBalance);
        }

        /// <summary>
        /// Get card balance from DB
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="expectedAmount"></param>
        /// <returns>returns the balance of the card</returns>
        public decimal GetCardBalanceFromDB(string cardId)
        {
            Console.WriteLine("Trying to get card balance from DB");
            return _testHelper.DbHelper.Card.GetBalanceByCardIdFromDb(Convert.ToInt32(cardId));
        }

        public string GetAccessToken()
        {
            List<Parameter> parameters = this.GetDefaultParameters();
            var client = new RestClient("https://testhost.openapi.starbucks.com/oauth2/v1/");
            var request = new RestRequest("token");
            request.Method = Method.POST;
            foreach (Parameter oParam in parameters)
            {
                if (oParam != null)
                {
                    switch (oParam.Type)
                    {
                        //TODO:Figure out how to handle different types of data, URL, Header, Parameters etc.
                        case ParameterType.QueryString:
                            request.AddQueryParameter(oParam.Name, oParam.Value.ToString());
                            break;

                        case ParameterType.HttpHeader:
                            request.AddHeader(oParam.Name, oParam.Value.ToString());
                            break;
                        default:
                            request.AddParameter(oParam.Name, oParam.Value);
                            break;
                    }
                }
            }
            IRestResponse<token> response = client.Execute<token>(request);
            return response.Data.access_token;
        }

        private List<Parameter> GetDefaultParameters()
        {
            return
                new List<Parameter>
                {
                   new Parameter
                   {
                       Name = "X-Api-Key",
                       Value = this.ApiKey,
                       Type = ParameterType.HttpHeader
                   },
                   new Parameter
                   {
                       Name = "client_id",
                       Value = this.ClientID,
                       Type = ParameterType.RequestBody
                   },
                   new Parameter
                   {
                       Name = "client_secret",
                       Value = this.ClientSecret,
                       Type = ParameterType.RequestBody
                   },
                   new Parameter
                   {
                       Name = "scope",
                       Value = "test_scope",
                       Type = ParameterType.RequestBody
                   },
                   new Parameter
                   {
                       Name = "grant_type",
                       Value = "client_credentials",
                       Type = ParameterType.RequestBody
                   },
                   new Parameter
                   {
                       Name = "sig",
                       Value = ComputeSignatureParameter(this.ApiKey, this.ClientSecret, DateTimeOffset.Now),
                       Type = ParameterType.QueryString
                   }};
        }

        private static string ComputeSignatureParameter(string key, string secret, DateTimeOffset instant)
        {
            using (HashAlgorithm hash = MD5.Create())
            {
                string value = string.Concat(key, secret, DateTimeOffset.Now.ToUnixTimeSeconds());

                byte[] data = Encoding.UTF8.GetBytes(value);

                return string.Concat(
                    hash.ComputeHash(data)
                        .Select(d => d.ToString("x2")));
            }
        }

        #endregion
    }
}

namespace System
{
    /* 
    TODO: remove these extensions if/when upgrading to .NET 4.6 in favor of FX method
          https://msdn.microsoft.com/en-us/library/system.datetimeoffset.tounixtimeseconds(v=vs.110).aspx 
    */
    public static class DateTimeOffsetExtensions
    {
        #region Fields

        private const long UnixEpocInSeconds = 62135596800L;

        private const long TicksPerSecond = 10000000L;

        #endregion // Fields

        #region Methods

        public static long ToUnixTimeSeconds(this DateTimeOffset dateTime)
        {
            return (dateTime.UtcTicks / DateTimeOffsetExtensions.TicksPerSecond) - DateTimeOffsetExtensions.UnixEpocInSeconds;
        }

        #endregion // Methods
    }
}