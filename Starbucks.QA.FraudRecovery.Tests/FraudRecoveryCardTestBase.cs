﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Starbucks.FraudRecovery.Common.Models;
using FraudRecovery.Tests.Integration.Resources;
using Starbucks.QA.FraudRecovery.Tests;
using System.Configuration;
using System.Net;
using RestSharp;
using Starbucks.QaTestUtilities.Sql;
using System.Threading;

namespace FraudRecovery.Tests.Integration
{
    public class FraudRecoveryCardTestBase : FraudRecoveryTestBase
    {
        public const int ApiTimeout = 15000;

        #region Card balance API

        /// <summary>
        /// Get card balance using the Fraud Recovery API
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns>Returns the balance on the card</returns>
        public decimal GetCardBalance(string cardId, string market)
        {
            var url = cardId.CardBalanceUrl(GetAccessToken());
            Console.WriteLine("Trying to get card balance using url '{0}'", url);
            var raw = _client.Get<SvcCardGetBalanceResponse>(url, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;

            var errorMessage = new StringBuilder();
            Assert.IsTrue(response != null, "CardGetBalance response is null");
            errorMessage.Append(response.Name != "CardGetBalance" ? string.Format("CardGetBalance response - 'Name' field has incorrect value '{0}'\n", response.Name) : null);
            errorMessage.Append(response.Status != OperationStatus.Succeeded ? string.Format("CardGetBalance response - 'Status' field has incorrect value '{0}'\n", response.Status) : null);
            errorMessage.Append(response.Id != cardId ? string.Format("CardGetBalance response - 'Id' field has incorrect value '{0}'\n", response.Id) : null);
            errorMessage.Append(response.RequestReceivedDateTime == null ? "CardGetBalancee response - 'RequestReceivedDateTime' field has null value\n" : null);
            errorMessage.Append(response.TraceNumber == null ? "CardGetBalancee response - 'TraceNumber' field has null value\n" : null);
            errorMessage.Append(response.Reason != null ? "CardGetBalance response has error: " + response.Reason.Message + Environment.NewLine : null);

            Assert.IsTrue(response.Result != null, "CardGetBalance response result object is null");
            errorMessage.Append(response.Result.CurrencyCode != currencyCodes[market] ? string.Format("CardGetBalance response - result:Currencycode does not match. Expected: '{0}', Actual: '{1}'\n", currencyCodes[market], response.Result.CurrencyCode) : null);
            errorMessage.Append(response.Result.BeginningBalance != response.Result.EndingBalance ? string.Format("CardGetBalance response - BeginningBalance and EndingBalance are not equal. Begining balance: '{0}', Ending balance: '{1}'\n", response.Result.BeginningBalance, response.Result.EndingBalance) : null);
            errorMessage.Append(response.Result.EndingBalanceDateTime == null ? "CardGetBalancee response - 'EndingBalanceDateTime' field has null value\n" : null);
            errorMessage.Append(response.Result.LockAmount != 0 ? "CardGetBalancee response - 'Lockamount' field is non-zero\n" : null);

            Console.WriteLine("Verify card balance endpoint call is successfull");
            Assert.IsTrue(errorMessage.Length == 0, errorMessage.ToString());
            return response.Result.EndingBalance;
        }

        /// <summary>
        /// Get card balance response using Fraud Recovery API
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns>Returns the endpoint response</returns>
        public SvcCardGetBalanceResponse GetCardBalanceResponse(string cardId)
        {
            var url = cardId.CardBalanceUrl(GetAccessToken());
            Console.WriteLine("Trying to get card balance using url '{0}'", url);
            var raw = _client.Get<SvcCardGetBalanceResponse>(url, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;
            return response;
        }

        /// <summary>
        /// Execute and validate the card get balance endpoint of Fraud Recovery Api
        /// </summary>
        /// <param name="market"></param>
        /// <param name="cashOutAmount"></param>
        public void ExecuteCardGetBalanceAndValidateResponse(string market = "US")
        {
            var cardId = FraudRecoveryCardTestSetup(market);

            // Get Balance using the Fraud Recovery API
            var balance = GetCardBalance(cardId, market);
            VerifyCardBalanceFromDB(cardId, balance);
        }

        /// <summary>
        /// Get svc card balance for invalid card id
        /// </summary>
        public void ExecuteCardGetBalancInvalidCardId()
        {
            SvcCardGetBalanceResponse response = GetCardBalanceResponse(invalidCardId.ToString());
            VerifyReasonCode(response, ReasonResources.InvalidCardReasonCode, ReasonResources.InvalidCardReasonMessage);
        }

        /// <summary>
        /// Get svc card balance for unregistered card id
        /// </summary>
        public void ExecuteCardGetBalanceUnregisteredCard(string market = "US")
        {
            var cardId = FraudRecoveryCardTestSetup(market);

            Console.WriteLine("Get balance using the Fraud Recovery API");
            decimal balance = GetCardBalance(cardId, market);
            UnregisterSvcCard(cardId);
            VerifyCardBalanceFromDB(cardId, balance);
        }


        /// <summary>
        /// Get svc card balance for register-unregister-register card
        /// </summary>
        public void ExecuteCardGetBalanceReRegisteredCard(string market = "US")
        {
            string userId;
            var cardId = FraudRecoveryCardTestSetup(market, out userId);
            UnregisterSvcCard(cardId);
            RegisterSvcCard(userId, cardId);

            Console.WriteLine("Get balance using the Fraud Recovery API");
            decimal balance = GetCardBalance(cardId, market);
            VerifyCardBalanceFromDB(cardId, balance);
        }

        /// <summary>
        /// Get svc card balance for associated card
        /// </summary>
        public void ExecuteCardGetBalanceAssociatedCard(string market = "US")
        {
            string userid;
            var cardId = FraudRecoveryCardTestSetup(market, out userid, isAssociatedCard: true);

            Console.WriteLine("Get balance using the Fraud Recovery API");
            decimal balance = GetCardBalance(cardId, market);
            VerifyCardBalanceFromDB(cardId, balance);
        }



        /// <summary>
        /// Get card balance for stolen/lost card
        /// </summary>
        public void ExecuteCardGetBalanceStolenCard()
        {
            var stolenSvcCardId = ConfigurationManager.AppSettings["stolenSvcCardId"].ToString();

            SvcCardGetBalanceResponse response = GetCardBalanceResponse(stolenSvcCardId.ToString());
            VerifyReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);
        }
        #endregion Card balance API

        #region Card revoke API

        /// <summary>
        /// Verifies revoke on the card
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="amount"></param>
        public void VerifyRevokeCard(string cardId, decimal beginingBalance, decimal revokeAmount)
        {
            var url = cardId.CardRevokeUrl(GetAccessToken());
            Console.WriteLine("Trying to revoke card using url '{0}' amount: {1}", url, revokeAmount);
            var raw = _client.Post<decimal, SvcCardRevokeAmountResponse>(url, revokeAmount, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;

            decimal endingBalance = 0;
            if (revokeAmount >= beginingBalance)
            {
                endingBalance = 0;
                revokeAmount = beginingBalance;
            }
            else
            {
                endingBalance = beginingBalance - revokeAmount;
            }

            var errorMessage = new StringBuilder();
            Assert.IsTrue(response != null, "CardRevokeAmount response is null");
            errorMessage.Append(response.Name != "CardRevokeAmount" ? "CardRevokeAmount response name is not CardRevokeAmount\n" : null);
            errorMessage.Append(response.Status != OperationStatus.Succeeded ? "CardRevokeAmount response status is not Succeeded\n" : null);
            errorMessage.Append(response.Id != cardId ? "CardRevokeAmount response's card id is different from the request's card id\n" : null);
            errorMessage.Append(response.RequestReceivedDateTime == null ? "CardRevokeAmount response has null RequestReceivedDateTime\n" : null);
            errorMessage.Append(response.TraceNumber == null ? "CardRevokeAmount response - 'TraceNumber' field has null value\n" : null);
            errorMessage.Append(response.Reason != null ? "CardRevokeAmount response has error: " + response.Reason.Message + Environment.NewLine : null);

            Assert.IsTrue(response.Result != null, "CardRevokeAmount response result object is null");
            errorMessage.Append(response.Result.Amount != revokeAmount ? string.Format("CardRevokeAmount response - Amount value is incorrect. Expected: '{0}', Actual: '{1}'\n", revokeAmount, response.Result.Amount) : null);
            errorMessage.Append(response.Result.BeginningBalance != beginingBalance ? string.Format("CardRevokeAmount response - BeginningBalance is incorrect. Expected: '{0}', Actual: '{1}'\n", beginingBalance, response.Result.BeginningBalance) : null);
            errorMessage.Append(response.Result.EndingBalance != endingBalance ? string.Format("CardRevokeAmount response - EndningBalance is incorrect. Expected: '{0}', Actual: '{1}'\n", endingBalance, response.Result.EndingBalance) : null);
            errorMessage.Append(response.Result.EndingBalanceDateTime == null ? "CardGetBalancee response - 'EndingBalanceDateTime' field has null value\n" : null);
            errorMessage.Append(response.Result.LockAmount != 0 ? "CardRevokeAmount response - 'Lockamount' field is non-zero\n" : null);

            Console.WriteLine("Verify redeem card endpoint call is successfull");
            Assert.IsTrue(errorMessage.Length == 0, errorMessage.ToString());
        }

        /// <summary>
        /// Svc revoke card with response (used for error verification)
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public SvcCardRevokeAmountResponse RevokeCardResponse(string cardId, decimal amount)
        {
            var url = cardId.CardRevokeUrl(GetAccessToken());
            Console.WriteLine("Trying to revoke card using url '{0}' amount: {1}", url, amount);
            var raw = _client.Post<decimal, SvcCardRevokeAmountResponse>(url, amount, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;
            return response;
        }

        /// <summary>
        /// Execute and validate the revoke card endpoint of Fraud Recovery Api
        /// </summary>
        /// <param name="market"></param>
        /// <param name="revokeAmount"></param>
        public void ExecuteRevokeCardAndValidateResponse(string market = "US", decimal revokeAmount = 10.0m)
        {
            var cardId = FraudRecoveryCardTestSetup(market);
            decimal balance = GetCardBalanceFromDB(cardId);
            VerifyRevokeCard(cardId, balance, revokeAmount);
            decimal expectedBalance = (revokeAmount >= balance) ? 0 : balance - revokeAmount;
            Thread.Sleep(2000);
            VerifyCardBalanceFromDB(cardId, expectedBalance);
        }

        /// <summary>
        /// Verify Revoke card endpoint with invalid card it
        /// </summary>
        public void ExecuteRevokeCardInvalidCardId()
        {
            SvcCardRevokeAmountResponse response = RevokeCardResponse(invalidCardId.ToString(), 10);
            VerifyReasonCode(response, ReasonResources.InvalidCardReasonCode, ReasonResources.InvalidCardReasonMessage);
        }

        /// <summary>
        /// Verify Revoke card endpoint for card with zero revoke amount
        /// </summary>
        public void ExecuteRevokeCardZeroAmount(string market = "US")
        {
            var cardId = FraudRecoveryCardTestSetup(market);
            SvcCardRevokeAmountResponse response = RevokeCardResponse(cardId, 0);
            VerifyReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);
        }

        /// <summary>
        /// Verify Revoke card endpoint for card with negative revoke amount
        /// </summary>
        public void ExecuteRevokeCardNegativeAmount(string market = "US")
        {
            var cardId = FraudRecoveryCardTestSetup(market);
            SvcCardRevokeAmountResponse response = RevokeCardResponse(cardId, -1);
            VerifyReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);
        }

        /// <summary>
        /// Verify Revoke card endpoint for card with full balance
        /// </summary>
        public void ExecuteRevokeCardFullBalance(string market = "US")
        {
            var cardId = FraudRecoveryCardTestSetup(market);
            decimal balance = GetCardBalanceFromDB(cardId);
            VerifyRevokeCard(cardId, balance, balance);
            VerifyCardBalanceFromDB(cardId, 0);
        }

        /// <summary>
        /// Verify Revoke card endpoint for card with revoke amount > 501  (max balance card can have is 500)
        /// </summary>
        public void ExecuteRevokeCardAmountGreaterthanMaxBalance(string market = "US")
        {
            var cardId = FraudRecoveryCardTestSetup(market);

            SvcCardRevokeAmountResponse response = RevokeCardResponse(cardId, 501);
            VerifyReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);
        }

        /// <summary>
        /// Verify Revoke card endpoint for card with revoke amount greater than actual balance on svc card
        /// </summary>
        public void ExecuteRevokeCardAmountGreatherThanBalance(string market = "US")
        {
            var cardId = FraudRecoveryCardTestSetup(market);
            decimal balance = GetCardBalanceFromDB(cardId);
            decimal revokeAmount = balance + 1;
            VerifyRevokeCard(cardId, balance, revokeAmount);
            Thread.Sleep(2000);
            decimal expectedBalance = (revokeAmount >= balance) ? 0 : balance - revokeAmount;
            VerifyCardBalanceFromDB(cardId, expectedBalance);
        }

        /// <summary>
        /// Verify Revoke card endpoint for multiple calls
        /// </summary>
        public void ExecuteRevokeCardAmountTwice(string market = "US")
        {
            var cardId = FraudRecoveryCardTestSetup(market);
            decimal balance = GetCardBalanceFromDB(cardId);
            VerifyRevokeCard(cardId, balance, 10);
            VerifyRevokeCard(cardId, balance - 10, 10);
            Thread.Sleep(2000);
            balance = (balance - 20 < 0) ? 0 : balance - 20;
            VerifyCardBalanceFromDB(cardId, balance);
        }

        /// <summary>
        /// Verify revoke card for unregistered card id
        /// </summary>
        public void ExecuteRevokeCardUnregisteredCard(string market = "US")
        {
            var cardId = FraudRecoveryCardTestSetup(market);

            Console.WriteLine("Get balance using the Fraud Recovery API");
            decimal balance = GetCardBalance(cardId, market);
            UnregisterSvcCard(cardId);
            VerifyRevokeCard(cardId, balance, 10);
            balance = (balance - 10 < 0) ? 0 : balance - 10;
            VerifyCardBalanceFromDB(cardId, balance);
        }

        /// <summary>
        /// Revoke stolen/lost card
        /// </summary>
        public void ExecuteRevokeStolenCard()
        {
            var stolenSvcCardId = ConfigurationManager.AppSettings["stolenSvcCardId"].ToString();

            SvcCardRevokeAmountResponse response = RevokeCardResponse(stolenSvcCardId, 10);
            VerifyReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);
        }

        /// <summary>
        /// Verify revoke card for ReRegistered card id
        /// </summary>
        public void ExecuteRevokeCardReRegisteredCard(string market = "US")
        {
            string userId;
            var cardId = FraudRecoveryCardTestSetup(market, out userId);

            Console.WriteLine("Get balance using the Fraud Recovery API");
            decimal balance = GetCardBalance(cardId, market);
            UnregisterSvcCard(cardId);
            RegisterSvcCard(userId, cardId);
            VerifyRevokeCard(cardId, balance, 10);
            balance = (balance - 10 < 0) ? 0 : balance - 10;
            VerifyCardBalanceFromDB(cardId, balance);
        }

        /// <summary>
        /// Verify revoke card for associated card
        /// </summary>
        public void ExecuteRevokeCardAssociatedCard(string market = "US")
        {
            string userid;
            var cardId = FraudRecoveryCardTestSetup(market, out userid, isAssociatedCard: true);

            Console.WriteLine("Get balance using the Fraud Recovery API");
            decimal balance = GetCardBalance(cardId, market);
            VerifyRevokeCard(cardId, balance, 10);
            balance = (balance - 10 < 0) ? 0 : balance - 10;
            VerifyCardBalanceFromDB(cardId, balance);
        }

        /// <summary>
        /// Verify multiple card revoke calls with low balance
        /// </summary>
        public void ExecuteMultitpleRevokeCard(string market = "US")
        {
            string userid;
            var cardId = FraudRecoveryCardTestSetup(market, out userid);

            Console.WriteLine("Verify multiple card revoke calls with low balance");
            decimal balance = GetCardBalance(cardId, market);
            // Revoke and leave only $1 on svc card
            decimal revokeAmount = balance - 1;
            VerifyRevokeCard(cardId, balance, revokeAmount);
            // Try to revoke amount greater than available balance of $1
            VerifyRevokeCard(cardId, 1, revokeAmount);
            // Verify revoke succeeds and balance is zero
            VerifyCardBalanceFromDB(cardId, 0);
        }

        /// <summary>
        /// Verify revoke card for ReRegistered card to a different User
        /// </summary>
        public void ExecuteRevokeCardReRegisteredToDifferentUser(string market = "US")
        {
            string userId;
            var cardId = FraudRecoveryCardTestSetup(market, out userId);

            Console.WriteLine("Get balance using the Fraud Recovery API");
            decimal balance = GetCardBalance(cardId, market);
            UnregisterSvcCard(cardId);
            //Get different user
            string userId2 = _testHelper.Account.GetOrCreateUser(_testHelper.CreateRandomTestEmail(), market: market);

            FraudRecoveryCardTestSetup(market, out userId2);
            //Register the previous card to this new user
            RegisterSvcCard(userId2, cardId);
            VerifyRevokeCard(cardId, balance, 10);
            balance = (balance - 10 < 0) ? 0 : balance - 10;
            VerifyCardBalanceFromDB(cardId, balance);
        }
        #endregion Card Revoke API

        #region Card load amount API

        /// <summary>
        /// Verify card load amount endpoint 
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="amount"></param>
        public void VerifyCardLoadAmount(string cardId, decimal beginningBalance, decimal amount)
        {

            var url = cardId.CardLoadAmount(GetAccessToken());
            Console.WriteLine("Trying to load amount to card using url '{0}' amount: {1}", url, amount);
            var raw = _client.Post<decimal, SvcCardLoadAmountResponse>(url, amount, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;
            var errorMessage = new StringBuilder();

            Assert.IsTrue(response != null, "CardLoadAmount response is null");
            errorMessage.Append(response.Name != "CardLoadAmount" ? "CardLoadAmount response name is not CardLoadAmount\n" : null);
            errorMessage.Append(response.Status != OperationStatus.Succeeded ? "CardLoadAmount response status is not Succeeded\n" : null);
            errorMessage.Append(response.Id != cardId ? "CardLoadAmount response's card id is different from the request's card id\n" : null);
            errorMessage.Append(response.RequestReceivedDateTime == null ? "CardLoadAmount response has null RequestReceivedDateTime\n" : null);
            errorMessage.Append(response.TraceNumber == null ? "CardLoadAmount response - 'TraceNumber' field has null value\n" : null);
            errorMessage.Append(response.Reason != null ? "CardLoadAmount response has error: " + response.Reason.Message : null);

            Assert.IsTrue(response.Result != null, "CardLoadAmount response result object is null");
            errorMessage.Append(response.Result.Amount != amount ? "CardLoadAmount response's result amount is different from the request's amount\n" : null);
            errorMessage.Append(response.Result.BeginningBalance != beginningBalance ? string.Format("CardLoadAmount response - BeginningBalance value does not match. Expected: '{0}', Actual: '{1}'", beginningBalance, response.Result.BeginningBalance) : null);
            errorMessage.Append(response.Result.EndingBalance != beginningBalance + amount ? string.Format("CardLoadAmount response - EndingBalance value does not match. Expected: '{0}', Actual: '{1}'", beginningBalance + amount, response.Result.EndingBalance) : null);
            errorMessage.Append(response.Result.EndingBalanceDateTime == null ? "CardLoadAmount response - 'EndingBalanceDateTime' field has null value\n" : null);
            errorMessage.Append(response.Result.LockAmount != 0 ? "CardLoadAmount response - 'Lockamount' field is non-zero\n" : null);

            Console.WriteLine("Verify card load amount endpoint call is successfull");
            Assert.IsTrue(errorMessage.Length == 0, errorMessage.ToString());
        }

        /// <summary>
        /// Verify card load amount endpoint 
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="amount"></param>
        public SvcCardLoadAmountResponse VerifyCardLoadAmountResponse(string cardId, decimal amount)
        {
            var url = cardId.CardLoadAmount(GetAccessToken());
            Console.WriteLine("Trying to load amount to card using url '{0}' amount: {1}", url, amount);
            var raw = _client.Post<decimal, SvcCardLoadAmountResponse>(url, amount, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;
            return response;
        }

        /// <summary>
        /// Execute and validate the load card endpoint of Fraud Recovery Api
        /// </summary>
        /// <param name="market"></param>
        /// <param name="loadAmount"></param>
        public void ExecuteLoadCardAndValidateResponse(string market = "US", decimal loadAmount = 10.0m)
        {
            var cardId = FraudRecoveryCardTestSetup(market);
            decimal beginingBalance = GetCardBalanceFromDB(cardId);
            decimal loadAmountValue = 10;
            VerifyCardLoadAmount(cardId, beginingBalance, loadAmountValue);
            Thread.Sleep(2000);
            VerifyCardBalanceFromDB(cardId, beginingBalance + loadAmountValue);
        }

        /// <summary>
        /// Verify Load card endpoint for invalid card id
        /// </summary>
        public void ExecuteLoadCardInvalidCardId()
        {
            SvcCardLoadAmountResponse response = VerifyCardLoadAmountResponse(invalidCardId.ToString(), 10);
            VerifyReasonCode(response, ReasonResources.InvalidCardReasonCode, ReasonResources.InvalidCardReasonMessage);
        }

        /// <summary>
        /// Verify Load card endpoint with invalid load amount 0
        /// </summary>
        public void ExecuteLoadCardInvalidAmountZero(string market = "US")
        {
            var cardId = FraudRecoveryCardTestSetup(market);
            SvcCardLoadAmountResponse response = VerifyCardLoadAmountResponse(cardId.ToString(), 0);
            VerifyReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);
        }

        /// <summary>
        /// Verify Load card endpoint with negative load amount
        /// </summary>
        public void ExecuteLoadCardInvalidNegativeAmount(string market = "US")
        {
            var cardId = FraudRecoveryCardTestSetup(market);
            SvcCardLoadAmountResponse response = VerifyCardLoadAmountResponse(cardId.ToString(), -10);
            VerifyReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);
        }

        /// <summary>
        /// Verify Load card endpoint with invalid max load amount
        /// </summary>
        public void ExecuteLoadCardInvalidInvalidMaxAmount(string market = "US")
        {
            var cardId = FraudRecoveryCardTestSetup(market);
            SvcCardLoadAmountResponse response = VerifyCardLoadAmountResponse(cardId.ToString(), 501);
            VerifyReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);
        }

        /// <summary>
        ///  Load amount to card exact max amount of 500, should pass
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteLoadCardExactMaxAmount(string market = "US")
        {
            var cardId = FraudRecoveryCardTestSetup(market);
            decimal beginingBalance = GetCardBalanceFromDB(cardId);
            decimal loadAmountValue = 500 - beginingBalance;
            VerifyCardLoadAmount(cardId, beginingBalance, loadAmountValue);
            VerifyCardBalanceFromDB(cardId, 500);
        }

        /// <summary>
        /// Load amount to card exceeds max amount of 500, should fail
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteLoadCardExceedsMaxAmount(string market = "US")
        {
            var cardId = FraudRecoveryCardTestSetup(market);
            decimal beginingBalance = GetCardBalanceFromDB(cardId);
            Console.WriteLine("Try to make total balance 501, api should fail");
            decimal loadAmountValue = 500 - beginingBalance + 1;
            SvcCardLoadAmountResponse response = VerifyCardLoadAmountResponse(cardId, loadAmountValue);
            VerifyReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);
        }

        /// <summary>
        /// Load amount to card twice with successive calls
        /// </summary>
        public void ExecuteLoadCardTwice(string market = "US")
        {
            var cardId = FraudRecoveryCardTestSetup(market);
            decimal beginingBalance = GetCardBalanceFromDB(cardId);
            decimal loadAmountValue = 10;
            Console.WriteLine("Load card api - first call");
            VerifyCardLoadAmount(cardId, beginingBalance, loadAmountValue);
            Console.WriteLine("Load card api - second call");
            VerifyCardLoadAmount(cardId, beginingBalance + loadAmountValue, loadAmountValue);
            VerifyCardBalanceFromDB(cardId, beginingBalance + loadAmountValue + loadAmountValue);
        }

        /// <summary>
        /// Verify load card for unregistered card id
        /// </summary>
        public void ExecuteLoadCardUnregisteredCard(string market = "US")
        {
            var cardId = FraudRecoveryCardTestSetup(market);

            Console.WriteLine("Get balance using the Fraud Recovery API");
            decimal beginingBalance = GetCardBalance(cardId, market);
            UnregisterSvcCard(cardId);
            decimal loadAmountValue = 10;
            VerifyCardLoadAmount(cardId, beginingBalance, loadAmountValue);
        }

        /// <summary>
        /// Verify load card for register-unregister-register card
        /// </summary>
        public void ExecuteLoadCardReRegisteredCard(string market = "US")
        {
            string userId;
            var cardId = FraudRecoveryCardTestSetup(market, out userId);
            UnregisterSvcCard(cardId);
            RegisterSvcCard(userId, cardId);

            Console.WriteLine("Get balance using the Fraud Recovery API");
            decimal beginingBalance = GetCardBalance(cardId, market);
            decimal loadAmountValue = 10;
            Console.WriteLine("Verify card load using the Fraud Recovery API");
            VerifyCardLoadAmount(cardId, beginingBalance, loadAmountValue);
        }

        /// <summary>
        /// Verify load card for associated card
        /// </summary>
        public void ExecuteLoadCardAssociatedCard(string market = "US")
        {
            string userid;
            var cardId = FraudRecoveryCardTestSetup(market, out userid, isAssociatedCard: true);

            Console.WriteLine("Get balance using the Fraud Recovery API");
            decimal beginingBalance = GetCardBalance(cardId, market);
            //UnregisterSvcCard(cardId);
            decimal loadAmountValue = 10;
            Console.WriteLine("Verify card load using the Fraud Recovery API");
            VerifyCardLoadAmount(cardId, beginingBalance, loadAmountValue);
        }

        /// <summary>
        /// Load amount for stolen/lost card
        /// </summary>
        public void ExecuteLoadCardStolenCard()
        {
            var stolenSvcCardId = ConfigurationManager.AppSettings["stolenSvcCardId"].ToString();
            decimal loadAmountValue = 10;

            SvcCardLoadAmountResponse response = VerifyCardLoadAmountResponse(stolenSvcCardId, loadAmountValue);
            VerifyReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);
        }
        #endregion Card load amount API

        #region Card cashout API

        /// <summary>
        /// Verify card cash out end point
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="expectedAmount"></param>
        public void VerifyCashOut(string cardId, string market = "US")
        {
            var url = cardId.CardCashOutUrl(GetAccessToken());
            Console.WriteLine("Trying to cash out card using url '{0}'", url);
            var encryptedCardId = _testHelper.DbHelper.Card.GetEncryptedId(cardId);
            InitCardBalance = GetCardBalanceFromDB(cardId);
            //var cardNumber = _testHelper.DbHelper.Card.GetDecyptedCardNumberByCardIdFromDb(encryptedCardId);
            //var decryptedCardPin = _testHelper.DbHelper.Card.GetDecryptedCardPinByCardIdFromDb(Convert.ToInt32(cardId));
            //var currencyCode = _testHelper.Card.GetCardDetailsByNumberPin(cardNumber, decryptedCardPin).CurrencyCode;
            var raw = _client.Post<SvcCardCashOutResponse>(url, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;

            var errorMessage = new StringBuilder();
            Assert.IsTrue(response != null, "CardCashOut response is null");
            errorMessage.Append(response.Name != "CardCashOut" ? "CardCashOut response name is not CardCashOut\n" : null);
            errorMessage.Append(response.Status != OperationStatus.Succeeded ? "CardCashOut response status is not Succeeded\n" : null);
            errorMessage.Append(response.Id != cardId ? "CardCashOut response's card id is different from the request's card id\n" : null);
            errorMessage.Append(response.RequestReceivedDateTime == null ? "CardCashOut response has null RequestReceivedDateTime\n" : null);
            errorMessage.Append(response.TraceNumber == null ? "CardCashOut response - 'TraceNumber' field has null value\n" : null);
            errorMessage.Append(response.Reason != null ? "CardCashOut response has error: " + response.Reason.Message : null);

            Assert.IsTrue(response.Result != null, "CardCashOut response result object is null");
            errorMessage.Append(response.Result.Amount != InitCardBalance ? "CardCashOut response's result amount is different from the request's amount\n" : null);
            errorMessage.Append(response.Result.BeginningBalance != InitCardBalance ? "CardCashOut response's result BeginningBalance is different from the card balance\n" : null);
            errorMessage.Append(response.Result.EndingBalance != 0 ? "CardCashOut response's result EndingBalance is not zero\n" : null);
            errorMessage.Append(response.Result.EndingBalanceDateTime == null ? "CardCashOut response's result EndingBalanceDateTime is null\n" : null);
            errorMessage.Append(response.Result.LockAmount != 0 ? "CardCashOut response - 'Lockamount' field is non-zero\n" : null);

            Console.WriteLine("Verify card cash out endpoint call is successfull");
            Assert.IsTrue(errorMessage.Length == 0, errorMessage.ToString());
        }

        /// <summary>
        /// Helper method to card cash out end point with response (use to verify error cases)
        /// </summary>
        public SvcCardCashOutResponse CashOutResponse(string cardId, string market = "US")
        {
            var url = cardId.CardCashOutUrl(GetAccessToken());
            Console.WriteLine("Trying to cash out card using url '{0}'", url);

            var raw = _client.Post<SvcCardCashOutResponse>(url, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;
            return response;
        }

        /// <summary>
        /// Execute and validate the cash-out card endpoint of Fraud Recovery Api
        /// </summary>
        /// <param name="market"></param>
        /// <param name="cashOutAmount"></param>
        public void ExecuteCashOutCardAndValidateResponse(string market = "US")
        {
            var cardId = FraudRecoveryCardTestSetup(market);
            VerifyCashOut(cardId, market);
            Thread.Sleep(2000);
            VerifyCardBalanceFromDB(cardId, 0);
        }

        /// <summary>
        /// Verify card cash out end point for invalid card id
        /// </summary>
        public void ExecuteCashOutCardInvalidCardId(string market = "US")
        {
            SvcCardCashOutResponse response = CashOutResponse(invalidCardId.ToString());
            VerifyReasonCode(response, ReasonResources.InvalidCardReasonCode, ReasonResources.InvalidCardReasonMessage);
        }

        /// <summary>
        /// Verify card cash out end point for card with zero balance
        /// </summary>
        public void ExecuteCashOutZeroBalanceCard(string market = "US")
        {
            var cardId = FraudRecoveryCardTestSetup(market);
            decimal balance = GetCardBalanceFromDB(cardId);
            VerifyRevokeCard(cardId, balance, balance);
            VerifyCardBalanceFromDB(cardId, 0);

            SvcCardCashOutResponse response = CashOutResponse(cardId.ToString());
            VerifyReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);
        }

        /// <summary>
        /// Verify card cash out end point for multiple calls
        /// </summary>
        public void ExecuteCashOutTwice(string market = "US")
        {
            var cardId = FraudRecoveryCardTestSetup(market);
            decimal balance = GetCardBalanceFromDB(cardId);
            VerifyCashOut(cardId, market);
            VerifyCardBalanceFromDB(cardId, 0);

            SvcCardCashOutResponse response = CashOutResponse(cardId.ToString());
            VerifyReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);
        }

        /// <summary>
        /// Verify cashout for unregistered card id
        /// </summary>
        public void ExecuteCashoutUnregisteredCard(string market = "US")
        {
            var cardId = FraudRecoveryCardTestSetup(market);

            UnregisterSvcCard(cardId);
            VerifyCashOut(cardId, market);
            VerifyCardBalanceFromDB(cardId, 0);
        }
        
        /// <summary>
        /// Verify cashout for register-unregister-register card
        /// </summary>
        public void ExecuteCashoutReRegisteredCard(string market = "US")
        {
            string userId;
            var cardId = FraudRecoveryCardTestSetup(market, out userId);
            UnregisterSvcCard(cardId);
            RegisterSvcCard(userId, cardId);

            Console.WriteLine("Verify cashout using the Fraud Recovery API"); 
            VerifyCashOut(cardId, market);
            VerifyCardBalanceFromDB(cardId, 0);
        }

        /// <summary>
        /// Verify cashout for associated card
        /// </summary>
        public void ExecuteCashoutCardAssociatedCard(string market = "US")
        {
            string userid;
            var cardId = FraudRecoveryCardTestSetup(market, out userid, isAssociatedCard: true);

            Console.WriteLine("Verify cashout using the Fraud Recovery API");
            VerifyCashOut(cardId, market);
            VerifyCardBalanceFromDB(cardId, 0);
        }

        /// <summary>
        /// Verify card cashout for stolen/lost card
        /// </summary>
        public void ExecuteCashoutStolenCard()
        {
            var stolenSvcCardId = ConfigurationManager.AppSettings["stolenSvcCardId"].ToString();

            SvcCardCashOutResponse response = CashOutResponse(stolenSvcCardId.ToString());
            VerifyReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);
        }

        /// <summary>
        /// Verify card cash out end point for multiple calls
        /// </summary>
        public void ExecuteCardLoadAndCashoutMultipleTimes(string market = "US", int noOfIterations = 10)
        {
            string userId;
           var cardId = GetCardIdFromConfigurationFile(market, out userId);

            decimal balance = GetCardBalanceFromDB(cardId);
            Console.WriteLine("Initail Card balance: {0}", balance);
            for (int i = 0; i < noOfIterations; i++)
            {
                VerifyCardLoadAmountResponse(cardId.ToString(), 10);
                VerifyCashOut(cardId, market);
            }

            SvcCardGetBalanceResponse newBalance = GetCardBalanceResponse(cardId);
            Assert.IsTrue(newBalance.Result.LockAmount == 0, "Card balance locked after multiple load and cashout operations");
            Assert.IsTrue(newBalance.Result.EndingBalance == 0, "Card balance is not zero after cashout");
        }

        #endregion Card cashout API

        #region Card Freeze API

        /// <summary>
        /// Helper method to freeze svc card
        /// </summary>
        /// <param name="cardId"></param>
        public void CardFreeze(string cardId, decimal beginingBalance)
        {
            if(Decimal.Equals(beginingBalance, 0.0))
            {
                SvcCardLoadAmountResponse loadResponse = VerifyCardLoadAmountResponse(cardId, 10);
                Assert.IsTrue(loadResponse.Status == OperationStatus.Succeeded, "Card load failed before card freeze operation");
            }
            SvcCardGetBalanceResponse balanceResponse = GetCardBalanceResponse(cardId.ToString());
            if(balanceResponse.Result.LockAmount > 0)
            {
                SvcCardUnfreezeResponse unfreezeResponse = CardUnfreezeResponse(cardId);
                Assert.IsTrue(unfreezeResponse.Status == OperationStatus.Succeeded, "Card unfreeeze failed before card freeze operation");
            }

            var url = cardId.CardFreezeUrl(GetAccessToken());
            Console.WriteLine("Trying to freeze card using url '{0}'", url);
            var raw = _client.Post<SvcCardFreezeResponse>(url, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;
            decimal endingBalance = beginingBalance;

            var errorMessage = new StringBuilder();
            Assert.IsTrue(response != null, "CardFreeze response is null");
            errorMessage.Append(response.Name != "CardFreeze" ? string.Format("CardFreeze response - 'Name' field has incorrect value '{0}'", response.Name) : null);
            errorMessage.Append(response.Status != OperationStatus.Succeeded ? string.Format("CardFreeze response - 'Status' field has incorrect value '{0}'", response.Status) : null);
            errorMessage.Append(response.Id != cardId ? string.Format("CardFreeze response - 'Id' field has incorrect value '{0}'", response.Id) : null);
            errorMessage.Append(response.Reason != null ? "CardFreeze response has error: " + response.Reason.Message : null);
            errorMessage.Append(response.RequestReceivedDateTime == null ? "CardFreeze response - 'RequestReceivedDateTime' field has null value\n" : null);
            errorMessage.Append(response.TraceNumber == null ? "CardFreeze response - 'TraceNumber' field has null value\n" : null);
            errorMessage.Append(response.Reason != null ? "CardFreeze response has error: " + response.Reason.Message : null);

            Assert.IsTrue(response.Result != null, "CardFreeze response result object is null");
            errorMessage.Append(response.Result.Amount != 0 ? string.Format("CardFreeze response - Amount value is incorrect. Expected: '0', Actual: '{0}'", response.Result.Amount) : null);
            errorMessage.Append(response.Result.BeginningBalance != beginingBalance ? string.Format("CardFreeze response - BeginningBalance is incorrect. Expected: '{0}', Actual: '{1}'", beginingBalance, response.Result.BeginningBalance) : null);
            errorMessage.Append(response.Result.EndingBalance != endingBalance ? string.Format("CardFreeze response - EndningBalance is incorrect. Expected: '{0}', Actual: '{1}'", endingBalance, response.Result.EndingBalance) : null);
            errorMessage.Append(response.Result.EndingBalanceDateTime == null ? "CardFreeze response - 'EndingBalanceDateTime' field has null value\n" : null);
            errorMessage.Append(response.Result.LockAmount != beginingBalance ? string.Format("CardFreeze response - lock amount is incorrect. Expected: '{0}', Actual: '{1}'", beginingBalance, response.Result.LockAmount) : null);

            Console.WriteLine("Verify CardFreeze endpoint call is successfull");
            Assert.IsTrue(errorMessage.Length == 0, errorMessage.ToString());
        }

        /// <summary>
        /// Helper method to get card Freeze end point response
        /// </summary>
        /// <param name="cardId"></param>
        public SvcCardFreezeResponse CardFreezeResponse(string cardId)
        {
            var url = cardId.CardFreezeUrl(GetAccessToken());
            Console.WriteLine("Trying to freeze card using url '{0}'", url);
            var raw = _client.Post<SvcCardFreezeResponse>(url, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;
            return response;
        }

        /// <summary>
        /// Execute and validate Freeze card endpoint
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteFreezeCardAndValidateResponse(string market = "US")
        {
            string userId = string.Empty;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);
            decimal balance = GetCardBalanceFromDB(cardId);
            CardFreeze(cardId, balance);
            balance = GetCardBalanceFromDB(cardId);
            Console.WriteLine("cleanup - unfreeze card");
            CardUnfreeze(cardId, balance);
        }

        /// <summary>
        /// Freeze card with invalid card id
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteFreezeCardInvalidCardId(string market = "US")
        {
            SvcCardFreezeResponse response = CardFreezeResponse(invalidCardId);
            VerifyReasonCode(response, ReasonResources.InvalidCardReasonCode, ReasonResources.InvalidCardReasonMessage);
        }

        /// <summary>
        /// Freeze card with zero balance
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteFreezeCardForZeroBalanceCard(string market = "US")
        {
            string userId = string.Empty;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);
            decimal balance = GetCardBalanceFromDB(cardId);
            if (balance != 0)
            { 
                VerifyRevokeCard(cardId, balance, balance);
                VerifyCardBalanceFromDB(cardId, 0);
            }

            SvcCardFreezeResponse response = CardFreezeResponse(cardId);
            VerifyReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);

            Console.WriteLine("cleanup - restore balance on svc card");
            VerifyCardLoadAmount(cardId, 0, balance);
        }

        /// <summary>
        /// Freeze same card multiple times
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteFreezeCardTwice(string market = "US")
        {
            string userId = string.Empty;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);
            decimal balance = GetCardBalanceFromDB(cardId);
            CardFreeze(cardId, balance);

            balance = GetCardBalanceFromDB(cardId);
            Console.WriteLine("Card balance after initial freeze: {0}", balance);
            Console.WriteLine("Call card freeze second time");
            SvcCardFreezeResponse response = CardFreezeResponse(cardId);
            VerifyReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);

            Console.WriteLine("cleanup - unfreeze card");
            CardUnfreeze(cardId, balance);
        }

        /// <summary>
        /// Verify card freeze for unregistered card id
        /// </summary>
        public void ExecuteCardFreezeUnregisteredCard(string market = "US")
        {
            string userId = string.Empty;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);

            decimal balance = GetCardBalanceFromDB(cardId);
            // Cannot unregister a digital card that has a balance greater than zero, hence call cash out 
            if (balance > 0)
                CashOutHelper(cardId);
            UnregisterSvcCard(cardId);
            decimal amount = 10;
            VerifyCardLoadAmount(cardId, 0, amount);
            CardFreeze(cardId, balance);

            Console.WriteLine("cleanup - unfreeze card");
            CardUnfreeze(cardId, balance);
        }

        /// <summary>
        /// Verify card freeze for ReRegistered card id
        /// </summary>
        public void ExecuteCardFreezeReRegisteredCard(string market = "US")
        {
            string userId = string.Empty;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);

            decimal balance = GetCardBalanceFromDB(cardId);
            // Cannot unregister a digital card that has a balance greater than zero, hence call cash out 
            if (balance > 0)
                CashOutHelper(cardId);
            UnregisterSvcCard(cardId);
            RegisterSvcCard(userId, cardId);
            decimal amount = 10;
            VerifyCardLoadAmount(cardId, 0, amount);
            CardFreeze(cardId, amount);

            Console.WriteLine("cleanup - unfreeze card");
            CardUnfreeze(cardId, amount);
        }

        /// <summary>
        /// Verify card freeze for stolen/lost card
        /// </summary>
        public void ExecuteCardFreezeStolenCard()
        {
            var stolenSvcCardId = ConfigurationManager.AppSettings["stolenSvcCardId"].ToString();

            SvcCardFreezeResponse response = CardFreezeResponse(stolenSvcCardId);
            VerifyReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);
        }

        #endregion Card freeze API

        #region Card unfreeze API

        /// <summary>
        /// Helper method to unfreeze svc card
        /// </summary>
        /// <param name="cardId"></param>
        public void CardUnfreeze(string cardId, decimal beginingBalance)
        {
            var url = cardId.CardUnfreezeUrl(GetAccessToken());
            decimal unfreezeRedeemAnount = Convert.ToDecimal(ConfigurationManager.AppSettings["unfreezeRedeemAnount"].ToString());
            decimal endingBalance = beginingBalance - unfreezeRedeemAnount;
            Console.WriteLine("Trying to Unfreeze card using url '{0}'", url);
            var raw = _client.Post<SvcCardUnfreezeResponse>(url, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;

            var errorMessage = new StringBuilder();
            Assert.IsTrue(response != null, "CardUnfreeze response is null");
            errorMessage.Append(response.Name != "CardUnfreeze" ? string.Format("CardUnfreeze response - 'Name' field has incorrect value '{0}'", response.Name) : null);
            errorMessage.Append(response.Status != OperationStatus.Succeeded ? string.Format("CardUnfreeze response - 'Status' field has incorrect value '{0}'", response.Status) : null);
            errorMessage.Append(response.Id != cardId ? string.Format("CardUnfreeze response - 'Id' field has incorrect value '{0}'", response.Id) : null);
            errorMessage.Append(response.Reason != null ? "CardUnfreeze response has error: " + response.Reason.Message : null);
            errorMessage.Append(response.RequestReceivedDateTime == null ? "CardUnfreeze response - 'RequestReceivedDateTime' field has null value\n" : null);
            errorMessage.Append(response.TraceNumber == null ? "CardUnfreeze response - 'TraceNumber' field has null value\n" : null);
            errorMessage.Append(response.Reason != null ? "CardUnfreeze response has error: " + response.Reason.Message : null);

            Assert.IsTrue(response.Result != null, "CardUnfreeze response result object is null");
            errorMessage.Append(response.Result.Amount != unfreezeRedeemAnount ? string.Format("CardUnfreeze response - Amount value is incorrect. Expected: '{0}', Actual: '{1}'", unfreezeRedeemAnount, response.Result.Amount) : null);
            errorMessage.Append(response.Result.BeginningBalance != beginingBalance ? string.Format("CardUnfreeze response - BeginningBalance is incorrect. Expected: '{0}', Actual: '{1}'", beginingBalance, response.Result.BeginningBalance) : null);
            errorMessage.Append(response.Result.EndingBalance != endingBalance ? string.Format("CardUnfreeze response - EndningBalance is incorrect. Expected: '{0}', Actual: '{1}'", endingBalance, response.Result.EndingBalance) : null);
            errorMessage.Append(response.Result.EndingBalanceDateTime == null ? "CardUnfreeze response - 'EndingBalanceDateTime' field has null value\n" : null);
            errorMessage.Append(response.Result.LockAmount != 0 ? string.Format("CardUnfreeze response - lock amount is incorrect. Expected: '0', Actual: '{0}'", response.Result.LockAmount) : null);

            Console.WriteLine("Verify CardUnfreeze endpoint call is successfull");
            Assert.IsTrue(errorMessage.Length == 0, errorMessage.ToString());
        }

        /// <summary>
        /// Helper method to get card unfreeze end point response
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public SvcCardUnfreezeResponse CardUnfreezeResponse(string cardId)
        {
            var url = cardId.CardUnfreezeUrl(GetAccessToken());
            Console.WriteLine("Trying to Unfreeze card using url '{0}'", url);
            var raw = _client.Post<SvcCardUnfreezeResponse>(url, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;
            return response;
        }

        /// <summary>
        /// Execute and validate Unfreeze card endpoint
        /// </summary>
        /// <param name="market"></param>
        /// <param name="cashOutAmount"></param>
        public void ExecuteUnfreezeCardAndValidateResponse(string market = "US")
        {
            string userId = string.Empty;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);
            Console.WriteLine("Pre-setup - freeze card");
            decimal balance = GetCardBalanceFromDB(cardId);
            CardFreeze(cardId, balance);
            balance = GetCardBalanceFromDB(cardId);
            Console.WriteLine("Balance after freeze card '{0}'", balance);

            CardUnfreeze(cardId, balance);
        }

        /// <summary>
        /// Unfreeze card with invalid card id
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteUnfreezeCardInvalidCardId(string market = "US")
        {
            SvcCardUnfreezeResponse response = CardUnfreezeResponse(invalidCardId);
            VerifyReasonCode(response, ReasonResources.InvalidCardReasonCode, ReasonResources.InvalidCardReasonMessage);
        }

        /// <summary>
        /// Unfreeze card multiple times
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteUnfreezeCardTwice(string market = "US")
        {
            string userId = string.Empty;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);
            decimal balance = GetCardBalanceFromDB(cardId);
            CardFreeze(cardId, balance);

            balance = GetCardBalanceFromDB(cardId);
            Console.WriteLine("Card balance after initial freeze: {0}", balance);
            Console.WriteLine("Call unfreeze first time");
            CardUnfreeze(cardId, balance);

            Console.WriteLine("Call unfreeze second time");
            SvcCardUnfreezeResponse response = CardUnfreezeResponse(cardId);
            VerifyReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);
        }

        /// <summary>
        /// Unfreeze unregistered card
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteCardUnfreezeUnregisteredCard(string market = "US")
        {
            string userId = string.Empty;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);

            decimal balance = GetCardBalanceFromDB(cardId);
            CardFreeze(cardId, balance);
            if (balance > 0)
                CashOutHelper(cardId);
            UnregisterSvcCard(cardId);
            CardUnfreeze(cardId, balance);
        }

        /// Verify card unfreeze for ReRegistered card id
        /// </summary>
        public void ExecuteCardUnFreezeReRegisteredCard(string market = "US")
        {
            string userId = string.Empty;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);

            decimal balance = GetCardBalanceFromDB(cardId);
            if (balance > 0)
                CashOutHelper(cardId);
            UnregisterSvcCard(cardId);
            RegisterSvcCard(userId, cardId);
            decimal amount = 10;
            VerifyCardLoadAmount(cardId, 0, amount);
            Console.WriteLine("Freeze card");
            CardFreeze(cardId, amount);

            Console.WriteLine("Unfreeze card");
            CardUnfreeze(cardId, amount);
        }

        /// <summary>
        /// Verify card unfreeze for stolen/lost card
        /// </summary>
        public void ExecuteCardUnfreezeStolenCard()
        {
            var stolenSvcCardId = ConfigurationManager.AppSettings["stolenSvcCardId"].ToString();

            SvcCardUnfreezeResponse response = CardUnfreezeResponse(stolenSvcCardId);
            VerifyReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);
        }

        /// <summary>
        /// Call Unfreeze on svc card that is not freezed - verify error code
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteCardUnfreezeCardNotFreezed(string market = "US")
        {
            string userId = string.Empty;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);
            InititiliazeCardForFreezeTest(cardId);

            SvcCardUnfreezeResponse response = CardUnfreezeResponse(cardId);
            VerifyReasonCode(response, ReasonResources.ExternalServiceResponseWithErrorReasonCode, ReasonResources.ExternalServiceResponseWithErrorReasonMessage);
        }

        /// <summary>
        /// Freeze and Unfreeze card multiple times
        /// </summary>
        /// <param name="market"></param>
        public void ExecuteCardFreezeAndUnfreezeMultipleTimes(string market = "US", int noOfIterations = 10)
        {
            string userId = string.Empty;
            var cardId = GetCardIdFromConfigurationFile(market, out userId);
            decimal balance = GetCardBalanceFromDB(cardId);
            Console.WriteLine("Initail Card balance: {0}", balance);
            for (int i = 0; i < noOfIterations; i++)
            {
                CardFreeze(cardId, balance);
                CardUnfreeze(cardId, balance);
            }

            decimal newBalance = GetCardBalanceFromDB(cardId);
            Assert.AreEqual(newBalance, balance, "Card balance mismatch after multiple freeze and unfreeze operations");
        }

        #endregion Card Unfreeze API

        #region miscellaneous helper methods
        
        /// <summary>
        /// Read svc card id from configuration file for Freeze and Unfreeze tests
        /// </summary>
        /// <param name="market"></param>
        /// <returns></returns>
        string GetCardIdFromConfigurationFile(string market, out string userId)
        {
            var cardId = string.Empty;
            userId = string.Empty;

            Console.WriteLine("Trying to read card id from configuration file");
            switch (market)
            {
                case "US":
                    cardId = ConfigurationManager.AppSettings["svcCardIdUS"].ToString();
                    userId = ConfigurationManager.AppSettings["userIdUS"].ToString();
                    break;

                case "CA":
                    cardId = ConfigurationManager.AppSettings["svcCardIdCA"].ToString();
                    userId = ConfigurationManager.AppSettings["userIdCA"].ToString();
                    break;

                case "UK":
                    cardId = ConfigurationManager.AppSettings["svcCardIdUK"].ToString();
                    userId = ConfigurationManager.AppSettings["userIdUK"].ToString();
                    break;

                case "IE":
                    cardId = ConfigurationManager.AppSettings["svcCardIdIE"].ToString();
                    userId = ConfigurationManager.AppSettings["userIdIE"].ToString();
                    break;

                case "DE":
                    cardId = ConfigurationManager.AppSettings["svcCardIdDE"].ToString();
                    userId = ConfigurationManager.AppSettings["userIdIE"].ToString();
                    break;

                case "FR":
                    cardId = ConfigurationManager.AppSettings["svcCardIdFR"].ToString();
                    userId = ConfigurationManager.AppSettings["userIdFR"].ToString();
                    break;

                case "BR":
                    cardId = ConfigurationManager.AppSettings["svcCardIdBR"].ToString();
                    userId = ConfigurationManager.AppSettings["userIdBR"].ToString();
                    break;

                default:
                    Assert.IsTrue(true, "'{0}' market is not supported", market);
                    break;
            }
            return cardId;
        }

        /// <summary>
        /// Helper method to verify reason code from card api for failed cases
        /// </summary>
        public bool VerifyReasonCode<T>(T response, string reasonCode, string reasonMessage) where T : OperationResponseWithResult<SvcCardOperationResult>
        {
            Assert.IsTrue(response.Reason.Code == reasonCode, "Reason code does not match - Expected :'{0}' Actual :'{1}'", reasonCode, response.Reason.Code);
            Assert.IsTrue(response.Reason.Message == reasonMessage, "Reason code does not match - Expected :'{0}' Actual :'{1}'", reasonMessage, response.Reason.Message);
            Assert.IsTrue(response.Status == OperationStatus.Failed, string.Format("Response status is incorrect. Expected:'{0}' Actual:'{1}'", OperationStatus.Failed, response.Status));
            //Assert.IsTrue(response.Result == null, "Response result object is not null");
            Assert.IsTrue(response.TraceNumber != null, "'TraceNumber' field has null value");

            return true;
        }

        /// <summary>
        /// Helper method to cashout entire balance on svc
        /// </summary>
        /// <param name="cardId"></param>
        /// <param name="expectedAmount"></param>
        public void CashOutHelper(string cardId, string market = "US")
        {
            var url = cardId.CardCashOutUrl(GetAccessToken());
            Console.WriteLine("Trying to cash out card using url '{0}'", url);
            var encryptedCardId = _testHelper.DbHelper.Card.GetEncryptedId(cardId);
            var raw = _client.Post<SvcCardCashOutResponse>(url, ApiTimeout);
            Assert.IsTrue(raw != null);
            var response = raw.Result;

            Assert.IsTrue(response.Status == OperationStatus.Succeeded, "CardCashOut response status is not Succeeded");
        }

        /// <summary>
        ///  Helper method to initialize balance on card for Freeze related tests
        /// </summary>
        /// <param name="cardId"></param>
        public void InititiliazeCardForFreezeTest(string cardId)
        {
            decimal balance = GetCardBalanceFromDB(cardId);
            if (balance > 0)
                CashOutHelper(cardId);
            decimal loadAmount = 25;
            VerifyCardLoadAmount(cardId, 0, loadAmount);
        }

        #endregion miscellaneous helper methods
    }
}
