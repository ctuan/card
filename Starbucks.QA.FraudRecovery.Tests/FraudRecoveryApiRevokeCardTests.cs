﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FraudRecovery.Tests.Integration;

namespace Starbucks.QA.FraudRecovery.Tests
{
    [TestClass]
    public class FraudRecoveryApiRevokeCardTests : FraudRecoveryCardTestBase
    {
        #region BVT Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(1)]
        [Description("FRA - Verify card revoke for US svc card")]
        public void FraudRecoveryUSRevokeCard()
        {
            ExecuteRevokeCardAndValidateResponse();
        }
        #endregion BVT Tests

        #region Market Tests

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card revoke for CA svc card")]
        public void FraudRecoveryCARevokeCard()
        {
            ExecuteRevokeCardAndValidateResponse(caMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card revoke for UK svc card")]
        public void FraudRecoveryUKRevokeCard()
        {
            ExecuteRevokeCardAndValidateResponse(ukMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card revoke for IE svc card")]
        public void FraudRecoveryIERevokeCard()
        {
            ExecuteRevokeCardAndValidateResponse(ieMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card revoke for DE svc card")]
        public void FraudRecoveryDERevokeCard()
        {
            ExecuteRevokeCardAndValidateResponse(deMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card revoke for FR svc card")]
        public void FraudRecoveryFRRevokeCard()
        {
            ExecuteRevokeCardAndValidateResponse(frMarket);
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card revoke for BR svc card")]
        public void FraudRecoveryBRRevokeCard()
        {
            ExecuteRevokeCardAndValidateResponse(brMarket);
        }  
        #endregion Market Tests

        #region FVT Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card revoke for invalid svc card")]
        public void FraudRecoveryRevokeCardInvalidCardId()
        {
            ExecuteRevokeCardInvalidCardId();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card revoke for zero dollars")]
        public void FraudRecoveryRevokeCardZeroAmount()
        {
            ExecuteRevokeCardZeroAmount();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card revoke for negative amount")]
        public void FraudRecoveryRevokeCardNegativeAmount()
        {
            ExecuteRevokeCardNegativeAmount();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card revoke for full balance")]
        public void FraudRecoveryRevokeCardFullbalance()
        {
            ExecuteRevokeCardFullBalance();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card revoke with amount greater than the available balance of the card")]
        public void FraudRecoveryRevokeCardAmountGreatherThanBalance()
        {
            ExecuteRevokeCardAmountGreatherThanBalance();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card revoke for amount grater than max svc balance limit (> 500) ")]
        public void FraudRecoveryRevokeCardAmountGreaterthanMaxBalance()
        {
            ExecuteRevokeCardAmountGreaterthanMaxBalance();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card revoke for successive revokes")]
        public void FraudRecoveryRevokeCardAmountTwice()
        {
            ExecuteRevokeCardAmountTwice();
        }

        [TestMethod]
        [Ignore]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify revoke for stolen card")]
        public void FraudRecoveryRevokeCardStolenCard()
        {
            ExecuteRevokeStolenCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify revoke for unregistered svc card")]
        public void FraudRecoveryRevokeCardUnregisteredCard()
        {
            ExecuteRevokeCardUnregisteredCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify revoke for Register-Unregister_Registered svc card")]
        public void FraudRecoveryRevokeCardReRegisterCard()
        {
            ExecuteRevokeCardReRegisteredCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify revoke for Associated svc card")]
        public void FraudRecoveryRevokeCardAssociatedCard()
        {
            ExecuteRevokeCardAssociatedCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify multiple card revoke calls with low balance")]
        public void FraudRecoveryMultitpleRevokeCard()
        {
            ExecuteMultitpleRevokeCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify card revoke when the card is reregistered to a different user")]
        public void FraudRecoveryRevokeCardReRegisteredToDifferentUser()
        {
            ExecuteRevokeCardReRegisteredToDifferentUser();
        }
        #endregion FVT Tests
    
    }
}
