﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FraudRecovery.Tests.Integration;

namespace Starbucks.QA.FraudRecovery.Tests
{
    [TestClass]
    public class FraudRecoveryAPITransactionFreezeTests : FraudRecoveryTransactionTestBase
    {
        #region BVT Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(1)]
        [Description("FRA - Verify Transaction Freeze API for US market")]
        public void FraudRecoveryTransactionFreeze()
        {
            ExecuteTransactionFreezeAndValidateResponse();
        }
        #endregion BVT Tests

        #region FVT Tests
        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Transaction freeze for invalid cardid")]
        public void FraudRecoveryTransactionFreezeInvalidOrderId()
        {
            ExecuteTransactionFreezeInvalidOrderId();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Transaction freeze for zero balance card")]
        public void FraudRecoveryTransactionFreezeForZeroBalanceCard()
        {
            ExecuteTransactionFreezeForZeroBalanceCard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(2)]
        [Description("FRA - Verify Transaction freeze for successive calls")]
        public void FraudRecoveryTransactionFreezeTwice()
        {
            ExecuteTransactionFreezeTwice();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(3)]
        [Description("FRA - Call transaction freeze on 'Buy a Card' order")]
        public void FraudRecoveryTransactionFreezeOnBuyACard()
        {
            ExecuteTransactionFreezeOnBuyACard();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(3)]
        [Description("FRA - Call transaction freeze on 'eGift' order")]
        public void FraudRecoveryTransactionFreezeOnEGiftCard()
        {
            ExecuteTransactionFreezeOnEGift();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(1)]
        [Description("FRA - Verify Transaction Freeze works after card revoke api call")]
        public void FraudRecoveryTransactionFreezeAfterCardRevoke()
        {
            ExecuteTransactionFreezeAfterCardRevoke();
        }

        [TestMethod]
        [TestCategory("FraudRecoveryApi")]
        [Priority(1)]
        [Description("FRA - Verify transaction freeze on Zero balance svc card  - verify operation fails")]
        public void FraudRecoveryTransactionFreezeForZeroBalanceSvcCard()
        {
            ExecuteTransactionFreezeForZeroBalanceSvcCard();
        }
        #endregion FVT Tests
    }
}
