﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Common.Models;
using Starbucks.CardIssuer.Dal.Common;

namespace Card.WebApi.Controllers
{
	public class FraudRecoveryController : BaseController
	{
		public FraudRecoveryController(IFraudRecoveryProvider provider, ILoggingUtility logging)
		{
			logging.EnsureObjectNotNull("logging is required");
			provider.EnsureObjectNotNull("provider is required");

			this.Provider = provider;
			this.Logginer = logging;
		}

		protected IFraudRecoveryProvider Provider { get; private set; }
		protected ILoggingUtility Logginer { get; private set; }

		#region Transaction related actions

		[HttpPost]
		[ActionName("revoke")]
		public TransactionRevokeResponse RevokeTransaction([FromUri] string transactionId)
		{
			var result = Provider.RevokeTransaction(transactionId);
			return result;
		}

		[HttpPost]
		[ActionName("refund")]
		public TransactionRefundResponse RefundTransaction([FromUri] string transactionId)
		{
			var result = Provider.RefundTransaction(transactionId);
			return result;
		}

		[HttpPost]
		[ActionName("freeze")]
		public TransactionFreezeResponse FreezeTransaction([FromUri] string transactionId)
		{
			var result = Provider.FreezeTransaction(transactionId);
			return result;
		}

		[HttpPost]
		[ActionName("unfreeze")]
		public TransactionUnfreezeResponse UnfreezeTransaction([FromUri] string transactionId)
		{
			var result = Provider.UnfreezeTransaction(transactionId);
			return result;
		}

		#endregion

		[HttpGet]
		[ActionName("card-balance")]
		public SvcCardGetBalanceResponse CardBalance([FromUri] int cardId)
		{
			var result = Provider.SvcCardBalance(cardId);
			return result;
		}

		[HttpPost]
		[ActionName("revoke-amount")]
		public SvcCardRevokeAmountResponse CardRevokeAmount([FromUri] int cardId, [FromBody] decimal amount)
		{
			var result = Provider.SvcCardRevokeAmount(cardId, amount);
			return result;
		}

		[HttpPost]
		[ActionName("cash-out")]
		public SvcCardCashOutResponse CardCashOut([FromUri] int cardId)
		{
			var result = Provider.SvcCardCashOut(cardId);
			return result;
		}

		[HttpPost]
		[ActionName ("freeze")]
		public SvcCardFreezeResponse CardFreeze([FromUri] int cardId)
		{
			var result = Provider.SvcCardFreeze(cardId);
			return result;
		}

		[HttpPost]
		[ActionName("unfreeze")]
		public SvcCardUnfreezeResponse CardUnfreeze([FromUri] int cardId)
		{
			var result = Provider.SvcCardUnfreeze(cardId);
			return result;
		}

		[HttpPost]
		[ActionName("load-amount")]
		public SvcCardLoadAmountResponse CardLoadAmount([FromUri] int cardId, [FromBody] decimal amount)
		{
			var result = Provider.SvcCardLoadAmount(cardId, amount);
			return result;
		}
	}
}