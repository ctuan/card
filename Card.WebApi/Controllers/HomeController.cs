﻿using Starbucks.Api.Sdk.WebRequestWrapper;
using System;
using System.Configuration;
using System.ServiceModel.Configuration;
using System.Text;
using System.Web.Mvc;

namespace PaymentMethod.WebApi.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        [HttpGet]
        public ViewResult Index()
        {
            ViewData.Add("Deployment", GetDeploymentInfo());
            ViewData.Add("hasDetail", false.ToString());
            return View();
        }

        public ViewResult Configuration()
        {
            ViewData.Add("Deployment", GetDeploymentInfo());

            ViewData.Add("hasDetail", true.ToString());
            ViewData.Add("paymentService", GetPaymentServiceConfig());
            ViewData.Add("SoapEndpoints", GetSoapEndpointsConfig());
            ViewData.Add("ConnectionStrings", GetConnectionStrings());


            return View("Index");
        }

        #region configuration reader

        private void GetPaymentServiceEndpointConfig(StringBuilder builder, string name, string container = "ApiSdkHttpClientConfigurations")
        {
            IRestClientSettings<IHttpSettings> config = ConfigurationHelper.GetConfiguration(name, container);
            if (config == null)
            {
                builder.AppendFormat("Name: ({0}): unable to find endpoint <br/>", Bold(name));
                return;
            }

            builder.AppendFormat("Name: ({0}): {1}.   {2}: {3} <br/>", Bold(name), config.EndPoint.ToString(),
                Bold("Timeout"), (config.HttpSettings != null ? config.HttpSettings.Timeout.ToString() : "<NULL>"));
        }

        private string GetPaymentServiceConfig()
        {
            try
            {
                StringBuilder builder = new StringBuilder();

                GetPaymentServiceEndpointConfig(builder, "paymentServiceAuth");
                GetPaymentServiceEndpointConfig(builder, "paymentServiceBill");
                GetPaymentServiceEndpointConfig(builder, "paymentServiceZeroAuth");
                GetPaymentServiceEndpointConfig(builder, "paymentServiceVoidAuth");
                GetPaymentServiceEndpointConfig(builder, "paymentServiceFraudCheck");

                return builder.ToString();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        private string GetSoapEndpointsConfig()
        {
            try
            {
                StringBuilder builder = new StringBuilder();
                ClientSection clientSection = ConfigurationManager.GetSection("system.serviceModel/client") as ClientSection;
                if (clientSection == null)
                {
                    return "<system.serviceModel/client>   returns  NULL.";
                }

                foreach (ChannelEndpointElement endpoint in clientSection.Endpoints)
                {
                    builder.AppendFormat("Name ({0}) : {1}<br/>", Bold(endpoint.Name), endpoint.Contract);
                    builder.AppendFormat("Address ({0}) <br/>", Bold(endpoint.Address.ToString()));
                }

                return builder.ToString();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        private string GetConnectionStrings()
        {
            try
            {
                StringBuilder builder = new StringBuilder();
                var settings = ConfigurationManager.ConnectionStrings;

                if (settings == null)
                {
                    return "there is no any connection string";
                }

                foreach (ConnectionStringSettings cs in settings)
                {
                    builder.AppendFormat("{0} : {1} <br/>", Bold(cs.Name), cs.ProviderName);
                    builder.AppendFormat("   {0}<br/>", cs.ConnectionString);
                }

                return builder.ToString();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

        }
        private string GetDeploymentInfo()
        {
            try
            {
                System.Reflection.Assembly assembly = this.GetType().Assembly;
                int stringlen = "file:///".Length;
                string filePath = assembly.CodeBase.Substring(stringlen);
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(filePath);
                var assemblyName = assembly.GetName();

                StringBuilder builder = new StringBuilder();
                builder.AppendFormat("File = {0}<br/>     Last Modified = {1}<br/>     Version = {2}<br/>", Bold(fileInfo.Name), fileInfo.LastWriteTime, assemblyName.Version);
                return builder.ToString();

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        private string Bold(string input)
        {
            return string.Format("<b>{0}</b>", input ?? "<NULL>");
        }

        #endregion
    }
}
