﻿using System;
using System.Net;
using System.Runtime.CompilerServices;
using System.Web.Http;
using Starbucks.Common.ResourceMapping;
using Starbucks.LogCounter.Provider;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;

namespace Card.WebApi.Controllers
{
    public abstract class BaseController : BaseApiController
    {               
        protected ResourceMap ResourceMap =
            new ResourceMap(new ResourceMapItem() {StatusCode = HttpStatusCode.InternalServerError});
        

        protected string GetCodeProperty(Exception exception)
        {
            try
            {
                dynamic exc = exception;
                return exc.Code;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        protected  ApiException GetApiException(Exception exception)
        {
            if (exception is ApiException)
            {
                return exception as ApiException;
            }

            var code = GetCodeProperty(exception);
            var rmi = ResourceMap.GetMappedResource(exception.GetType(), code);
            if (rmi != null)
                return new ApiException(rmi.StatusCode, rmi.TargetCode, rmi.TargetMessage, exception);
            return new ApiException(HttpStatusCode.InternalServerError,
                                   CardWebAPIErrorResource.InternalServerErrorCode,
                                   CardWebAPIErrorResource.InternalServerErrorMessage, exception);

        }

        protected static  void IncrementLogCounter([CallerMemberName] string memberName = "")
        {
            LogCounterManager.Instance().Increment(string.Format("Card_{0}", memberName));
        }

        protected static void IncrementLogCounterError([CallerMemberName] string memberName = "")
        {
            LogCounterManager.Instance().Increment(string.Format("Card_{0}_Error", memberName));
        }
    }
}
