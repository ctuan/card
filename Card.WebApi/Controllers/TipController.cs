﻿using Account.Provider.Common;
using Card.WebApi.Filters;
using Card.WebApi.Models.Tipping;
using Card.WebApi.ProviderModels.Tipping;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Resources;
using Starbucks.CardTransaction.Provider.Common;
using Starbucks.CardTransaction.Provider.Common.ErrorResource;
using Starbucks.Common.ResourceMapping;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;
using Starbucks.Tipping.Dal.Common;
using Starbucks.Tipping.Dal.Common.ErrorResources;
using Starbucks.Tipping.Provider.Common;
using Starbucks.Tipping.Provider.Common.ErrorResources;
using Starbucks.Tipping.Provider.Common.Exceptions;
using Starbucks.Tipping.Provider.Common.Models;
using Starbucks.TransactionHistory.Common.Enums;
using Starbucks.TransactionHistory.Dal.Common.ErrorResources;
using Starbucks.TransactionHistory.Dal.Common.Exceptions;
using Starbucks.TransactionHistory.Provider.Common;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Card.WebApi.Models;
using OAuth2.Provider.Web;

namespace Card.WebApi.Controllers
{
    public class TipController : BaseController
    {
        private readonly ITippingProvider _tippingProvider;
        private readonly ICardTransactionProvider _cardTransactionProvider;
        private readonly ITransactionHistoryProvider _transactionHistoryProvider;
        private readonly IAccountProvider _accountProvider;

        private void LoadResourceMap()
        {

            #region TipProviderException

            ResourceMap.Map(new ResourceMapItem(typeof(TippingProviderException),
                                                TippingProviderErrors.CannotDeleteTipWithCurrentStatusCode,
                                                CardWebAPIErrorResource.TipCanNoLongerBeDeletedCode,
                                                CardWebAPIErrorResource.TipCanNoLongerBeDeletedMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(TippingProviderException),
                                                TippingProviderErrors.CannotModifyTipWithCurrentStatusCode,
                                                CardWebAPIErrorResource.TransactionIsNoLongerEligibleForTippingCode,
                                                CardWebAPIErrorResource.TransactionIsNoLongerEligibleForTippingMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(TippingProviderException),
                                                TippingProviderErrors.CannotUpdateTipStatusWithCurrentStatusCode,
                                                CardWebAPIErrorResource.InvalidTipStatusToProcessCode,
                                                CardWebAPIErrorResource.InvalidTipStatusToProcessMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(TippingProviderException),
                                                TippingProviderErrors.InvalidAmountCode,
                                                CardWebAPIErrorResource.TipIsTooSmallCode,
                                                CardWebAPIErrorResource.TipIsTooSmallMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(TippingProviderException),
                                                TippingProviderErrors.InvalidOriginalTransactionIdCode,
                                                CardWebAPIErrorResource.HistoryIdNotFoundOrIsInvalidCode,
                                                CardWebAPIErrorResource.HistoryIdNotFoundOrIsInvalidMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(TippingProviderException),
                                                TippingProviderErrors.MessageBrokerIsRequiredCode,
                                                HttpStatusCode.InternalServerError.ToString(),
                                                "Configuration Error",
                                                HttpStatusCode.InternalServerError))
                       .Map(new ResourceMapItem(typeof(TippingProviderException),
                                                TippingProviderErrors.OriginalTransactionNotFoundCode,
                                                CardWebAPIErrorResource.HistoryIdNotFoundOrIsInvalidCode,
                                                CardWebAPIErrorResource.HistoryIdNotFoundOrIsInvalidMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(TippingProviderException),
                                                TippingProviderErrors.TipExpirationTimeIsRequiredCode,
                                                CardWebAPIErrorResource.TransactionIsNotEligibleForTippingCode,
                                                CardWebAPIErrorResource.TransactionIsNotEligibleForTippingMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(TippingProviderException),
                                                TippingProviderErrors.TippingDataProviderIsRequiredCode,
                                                HttpStatusCode.InternalServerError.ToString(),
                                                "Configuration Error",
                                                HttpStatusCode.InternalServerError))
                       .Map(new ResourceMapItem(typeof(TippingProviderException),
                                                TippingProviderErrors.TipWindowHasExpiredCode,
                                                CardWebAPIErrorResource.TransactionIsNoLongerEligibleForTippingCode,
                                                CardWebAPIErrorResource.TransactionIsNoLongerEligibleForTippingMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(TippingProviderException),
                                                TippingProviderErrors.TransactionHistoryDataProviderIsRequiredCode,
                                                HttpStatusCode.InternalServerError.ToString(),
                                                "Configuration Error",
                                                HttpStatusCode.InternalServerError))
                       .Map(new ResourceMapItem(typeof(TippingProviderException),
                                                TippingProviderErrors.TransactionIsNotTippableCode,
                                                CardWebAPIErrorResource.TransactionIsNotEligibleForTippingCode,
                                                CardWebAPIErrorResource.TransactionIsNotEligibleForTippingMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(TippingProviderException),
                                                TippingProviderErrors.UserIdIsRequiredCode,
                                                CardWebAPIErrorResource.UserIdNotFoundOrIsInvalidCode,
                                                CardWebAPIErrorResource.UserIdNotFoundOrIsInvalidMessage,
                                                HttpStatusCode.BadRequest))
                                                ;

            #endregion

            #region TippingDalException

            ResourceMap.Map(new ResourceMapItem(typeof(TippingDalException),
                                                TippingDalErrors.TipNotFoundCode,
                                                CardWebAPIErrorResource.TipNotFoundCode,
                                                CardWebAPIErrorResource.TipNotFoundMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(TippingDalException),
                                                TippingDalErrors.UserIdNotFoundCode,
                                                CardWebAPIErrorResource.UserIdNotFoundOrIsInvalidCode,
                                                CardWebAPIErrorResource.UserIdNotFoundOrIsInvalidMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(TippingDalException),
                                                TippingDalErrors.TransactionDoesNotBelongToUserCode,
                                                "",
                                                "",
                                                HttpStatusCode.Forbidden));

            #endregion

            #region TransactionHistoryDalException

            ResourceMap.Map(new ResourceMapItem(typeof(TransactionHistoryDalException),
                                                DalErrors.TransactionDoesNotBelongToUserCode,
                                                null,
                                                null,
                                                HttpStatusCode.Forbidden));
            #endregion

            #region CardTransactionErrors

            ResourceMap.Map(new ResourceMapItem(typeof (CardTransactionException),
                                                CardTransactionErrorResource.CannotTipCode,
                                                CardWebAPIErrorResource.ErrorWhileTippingCode,
                                                CardWebAPIErrorResource.ErrorWhileTippingMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof (CardTransactionException),
                                                CardTransactionErrorResource.InvalidTipStatusCode,
                                                CardWebAPIErrorResource.InvalidTipStatusToProcessCode,
                                                CardWebAPIErrorResource.InvalidTipStatusToProcessMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof (CardTransactionException),
                                                CardTransactionErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundMessage,
                                                HttpStatusCode.BadRequest));
            #endregion

            #region CardTransactionValidationErrors

            ResourceMap.Map(new ResourceMapItem(typeof (CardTransactionValidationException),
                                                CardTransactionValidationErrorResource.TipOriginalCurrencyRequiredCode,
                                                CardWebAPIErrorResource.TipOriginalCurrencyCodeRequiredCode,
                                                CardWebAPIErrorResource.TipOriginalCurrencyCodeRequiredMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof (CardTransactionValidationException),
                                                CardTransactionValidationErrorResource.TipUserMarketRequiredCode,
                                                CardWebAPIErrorResource.UserMarketIsRequiredCode,
                                                CardWebAPIErrorResource.UserMarketIsRequiredMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof (CardTransactionValidationException),
                                                CardTransactionValidationErrorResource
                                                    .TipInvalidUserCurrencyTransactionCurrencyCode,
                                                CardWebAPIErrorResource.TipInvalidCurrencyCodeForTipCode,
                                                CardWebAPIErrorResource.TipInvalidCurrencyCodeForTipMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof (CardTransactionValidationException),
                                                CardTransactionValidationErrorResource
                                                    .TipAmountInvalidCode ,
                                                CardWebAPIErrorResource.TipIsTooSmallCode ,
                                                CardWebAPIErrorResource.TipIsTooSmallMessage  ,
                                                HttpStatusCode.BadRequest));
                        


            #endregion

            #region CardIssuerException

            ResourceMap.Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.AccountIsClosedCode,
                                                CardWebAPIErrorResource.CardIsClosedCode,
                                                CardWebAPIErrorResource.CardIsClosedMessage,
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CardIsAlreadyRegisteredCode,
                                                CardWebAPIErrorResource.CardAlreadyRegisteredCode,
                                                CardWebAPIErrorResource.CardAlreadyRegisteredMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CardIsInactiveCode,
                                                CardWebAPIErrorResource.CardIsInactiveCode,
                                                CardWebAPIErrorResource.CardIsInactiveMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CardNotFoundCode,
                                                "",
                                                "",
                                                HttpStatusCode.NotFound))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CardNotRegisteredToUserCode,
                                                "",
                                                "",
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CardReportedLostStolenCode,
                                                CardWebAPIErrorResource.CardIsLostOrStolenCode,
                                                CardWebAPIErrorResource.CardIsLostOrStolenMessage,
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CardReportedLostStolenInternetDisabledCode,
                                               CardWebAPIErrorResource.CardIsLostOrStolenCode,
                                                CardWebAPIErrorResource.CardIsLostOrStolenMessage,
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CreditCardNumberDoesNotMatchTypeCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CreditCardNumberInvalidCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.GeneralPaymentErrorCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.InsufficientFundsForTransactionCode,
                                                CardWebAPIErrorResource.InsufficientFundsCode,
                                                CardWebAPIErrorResource.InsufficientFundsMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource
                                                    .InvalidAmountMoreOrLessThanMinOrMaxAmountSpecifiedCode,
                                                CardWebAPIErrorResource.InvalidAmountMoreOrLessThanMinOrMaxAmountSpecifiedCode,
                                                CardWebAPIErrorResource.InvalidAmountMoreOrLessThanMinOrMaxAmountSpecifiedMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.InvalidCardNumberCode,
                                                CardWebAPIErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.InvalidPinCode,
                                                CardWebAPIErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.InvalidSubMarketCode,
                                                "",
                                                "",
                                                HttpStatusCode.NotFound))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.MaximumBalanceExceededCode,
                                                CardWebAPIErrorResource.MaxBalanceExceededCode,
                                                CardWebAPIErrorResource.MaxBalanceExceededMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.NoRegistrationAddressOnFileCode,
                                                CardWebAPIErrorResource.NoRegistrationAddressOnFileCode,
                                                CardWebAPIErrorResource.NoRegistrationAddressOnFileMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.RequestNotPermittedByThisAccountCode,
                                                "",
                                                "",
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.UnknownErrorCode,
                                                CardWebAPIErrorResource.InternalServerErrorCode,
                                                CardWebAPIErrorResource.InternalServerErrorMessage,
                                                HttpStatusCode.InternalServerError))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.ValueLinkHostDownCode,
                                                CardWebAPIErrorResource.FirstDataServiceUnavailableCode,
                                                CardWebAPIErrorResource.FirstDataServiceUnavailableMessage,
                                                HttpStatusCode.BadGateway));

            #endregion
        }

        public TipController(ITippingProvider tippingProvider, ICardTransactionProvider cardTransactionProvider, ITransactionHistoryProvider transactionHistoryProvider, IAccountProvider accountProvider)
        {
            LoadResourceMap();
            _tippingProvider = tippingProvider;
            _cardTransactionProvider = cardTransactionProvider;
            _transactionHistoryProvider = transactionHistoryProvider;
            _accountProvider = accountProvider;
        }

        [ActionName("UpsertTip"), HttpPut, HttpPost, UpsertTipFilter]
        [ApiOAuth2UserId]
        public HttpResponseMessage UpsertTip(string userId, string historyId, UpsertTipRequest tipRequest)
        {
            IncrementLogCounter();
            var validHistoryId = ParseHistoryId(historyId);
            try
            {
                var history = _transactionHistoryProvider.GetHistoryItemForUser(userId, validHistoryId);
                _tippingProvider.UpsertTip(userId, validHistoryId, tipRequest.Amount, Starbucks.Platform.Security.Encryption.DecryptCardId(history.CardId));
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception exception)
            {
                IncrementLogCounterError();
                throw GetApiException(exception);
            }
        }

        [ActionName("DeleteTip"), HttpDelete]
        [ApiOAuth2UserId]
        public HttpResponseMessage DeleteTip(string userId, string historyId)
        {
            IncrementLogCounter();
            var validHistoryId = ParseHistoryId(historyId);
            try
            {
                IDeleteTipResult result = _tippingProvider.DeleteTip(userId, validHistoryId);
                if (result.CurrentTipStatus == Starbucks.Tipping.Dal.Common.Enums.TipStatus.Deleted && result.CurrentTipStatus != result.PreviousTipStatus)
                {
                    // Return a 202 (Accepted) if the tip was actually deleted.
                    return Request.CreateResponse(HttpStatusCode.Accepted);
                }
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception exception)
            {
                IncrementLogCounterError();
                throw GetApiException(exception);
            }
        }

        [ActionName("ProcessTip"), HttpPost]
        [ApiOAuth2UserId]
        public HttpResponseMessage ProcessTip(string userId, string historyId, [FromBody] ProcessTipRequest processTip)
        {
            IncrementLogCounter();
            var validHistoryId = ParseHistoryId(historyId);
            try
            {
                var user = _accountProvider.GetUser(userId);
                if (user == null)
                {
                    throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.UserIdNotFoundOrIsInvalidCode, CardWebAPIErrorResource.UserIdNotFoundOrIsInvalidMessage);
                }

                var history = _transactionHistoryProvider.GetHistoryItemForUser(userId, validHistoryId);
                if (history == null)
                {
                    throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.HistoryIdNotFoundOrIsInvalidCode, CardWebAPIErrorResource.HistoryIdNotFoundOrIsInvalidMessage);
                }

                if (history.TransactionDetails.TipInfo.TipStatus == Starbucks.TransactionHistory.Common.Enums.TipStatus.None)
                {
                    // If the TipStatus is None it means a Tip was never created.
                    throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.TipNotFoundCode, CardWebAPIErrorResource.TipNotFoundMessage);
                }
                var tip = history.ToProcessTip(user, validHistoryId, processTip);
                _cardTransactionProvider.Tip(tip);
            }
            catch (Exception exception)
            {
                IncrementLogCounterError();
                throw GetApiException(exception);
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }
        private static long ParseHistoryId(string historyId)
        {
            long validHistoryId;
            if (!Int64.TryParse(historyId, out validHistoryId))
            {
                throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.HistoryIdNotFoundOrIsInvalidCode,
                                       CardWebAPIErrorResource.HistoryIdNotFoundOrIsInvalidMessage);
            }
            return validHistoryId;
        }
    }
}