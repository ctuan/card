﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Card.WebApi.Models;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.Card.Provider.Common;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.OpenApi.Utilities.Common;

namespace Card.WebApi.Controllers
{
    public class SelectedCardController : BaseController
    {
        private readonly ISelectedCardProvider _selectedCardProvider;

        public SelectedCardController(ISelectedCardProvider selectedCardProvider,
                                      ISelectedCardDal selectedCardDal)
        {
            _selectedCardProvider = Guard.EnsureArgumentIsNotNull(selectedCardProvider, "cardProvider");
            Guard.EnsureArgumentIsNotNull(selectedCardDal, "selectedCardDal");
        }

        [HttpPost, HttpPut]
        [ActionName("AssociateSelectedCards")]
        public HttpResponseMessage AssociateSelectedCards(string userId, string clientId,
                                                          SelectedCardsRequest cardIds)
        {
            IncrementLogCounter();

            try
            {
                _selectedCardProvider.AssociateSelectedCardsWithClient(userId, clientId, cardIds.Cards.ToArray());

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
        }

        [HttpGet]
        [ActionName("GetSelectedCards")]
        public HttpResponseMessage GetSelectedCards(string userId, string clientId)
        {
            IncrementLogCounter();

            try
            {
                IEnumerable<string> cards = _selectedCardProvider.GetSelectedCards(userId, clientId);

                return Request.CreateResponse(HttpStatusCode.OK, cards);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
        }

        [HttpGet]
        [ActionName("GetSelectedCards")]
        public HttpResponseMessage GetSelectedCards(string userId)
        {
            IncrementLogCounter();

            try
            {
               IEnumerable<ISelectedCard> cards = _selectedCardProvider.GetSelectedCards(userId);

                return Request.CreateResponse(HttpStatusCode.OK, cards);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
        }

        [HttpGet]
        [ActionName("IsCardLinked")]
        public HttpResponseMessage IsCardLinked(string userId, string cardId)
        {
            IncrementLogCounter();

            try
            {
                bool isLinked = _selectedCardProvider.IsCardLinked(userId, cardId);

                return Request.CreateResponse(HttpStatusCode.OK, isLinked);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
        }

        [HttpPost]
        [ActionName("RemoveSelectedCards")]
        public HttpResponseMessage RemoveSelectedCards(string userId, string clientId, SelectedCardsRequest cardIds)
        {
            IncrementLogCounter();

            try
            {
                IncrementLogCounter();

                _selectedCardProvider.RemoveSelectedCards(userId, clientId, cardIds.Cards.ToArray());

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
        }
    }
}