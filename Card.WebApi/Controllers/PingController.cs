﻿using Starbucks.Common.ResourceMapping;
using Starbucks.OpenApi.WebApi.Common.Controllers;
using Starbucks.OpenApi.WebApi.Common.ErrorResources;
using Starbucks.OpenApi.WebApi.Common.Exceptions;
using Starbucks.OpenApi.WebApi.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Card.WebApi.Controllers
{
    public class PingController : BasePingController
    {
        static PingController()
        {
            PingResourceMap.Map(new ResourceMapItem(typeof(PingException),
                                    PingControllerErrors.InvalidPingLevelCode,
                                    CardWebAPIErrorResource.PingLevelInvalidCode,
                                    CardWebAPIErrorResource.PingLevelInvalidMessage,
                                    HttpStatusCode.BadRequest))
           .Map(new ResourceMapItem(typeof(PingException),
                                    PingControllerErrors.UnsupporetedPingLevelCode,
                                    CardWebAPIErrorResource.PingLevelUnsupportedCode,
                                    CardWebAPIErrorResource.PingLevelUnsupportedMessage,
                                    HttpStatusCode.BadRequest));
        }

        public PingController()
            : base(PingLevels.Alive)
        {
        }

    }
}
