﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Account.Provider.Common;
using Card.WebApi.Helpers;
using Card.WebApi.Models;
using OAuth2.Provider.Web;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.Card.Provider.Common;
using Starbucks.Card.Provider.Common.Enums;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;
using Starbucks.OpenApi.Utilities.Common;
using Starbucks.OpenApi.Utilities.Common.Extensions;
using Starbucks.OpenApi.Utilities.Common.Helpers;
using Starbucks.OpenApi.WebApi.Common.Helpers;
using Starbucks.Settings.Provider.Common;
using System.Diagnostics.CodeAnalysis;

namespace Card.WebApi.Controllers
{
    [ExcludeFromCodeCoverage]
    // IMPORTANT NOTE RE async and await
    // SEE http://stackoverflow.com/questions/25043738/why-is-taskhttpresponsemessage-not-awaitable-in-my-project
    // Oh, I thought async/await was available in both 4.0 and 4.5. Thanks! – freakinthesun Jul 30 '14 at 20:19
    // @freakinthesun: It is for desktop/mobile apps, but not ASP.NET. – Stephen Cleary Jul 30 '14 at 20:39
    public class PassbookController : ApiController
    {
        private readonly ISettingsProvider _settingsProvider;
        private readonly ILogCounterWrapper _logCounterWrapper;
        private readonly ILogHelper _logHelper;
        private readonly ICardProvider _cardProvider;
        private readonly IAccountProvider _accountProvider;
        private readonly ILocationHelper _locationHelper;
        private readonly IRequestHelper _requestHelper;
        private readonly ICardDal _cardDal;
        private readonly IPassbookService _passbookService;
        public PassbookController(
            ILogCounterWrapper logCounterWrapper,
            ILogHelper logHelper,
            ICardProvider cardProvider,
            IAccountProvider accountProvider,
            ILocationHelper locationHelper,
            IRequestHelper requestHelper,
            ICardDal cardDal,
            ISettingsProvider settingsProvider,
            IPassbookService passbookService)
        {
            _settingsProvider = settingsProvider;
            _logCounterWrapper = Guard.EnsureArgumentIsNotNull(logCounterWrapper, "logCounterWrapper");
            _logHelper = Guard.EnsureArgumentIsNotNull(logHelper, "logHelper");
            _cardProvider = Guard.EnsureArgumentIsNotNull(cardProvider, "cardProvider");
            _accountProvider = Guard.EnsureArgumentIsNotNull(accountProvider, "accountProvider");
            _locationHelper = Guard.EnsureArgumentIsNotNull(locationHelper, "locationHelper");
            _requestHelper = Guard.EnsureArgumentIsNotNull(requestHelper, "requestHelper");
            _cardDal = Guard.EnsureArgumentIsNotNull(cardDal, "cardDal");
            _passbookService = Guard.EnsureArgumentIsNotNull(passbookService, "passbookService");
        }

        // Call volume is 30K /day or one every three seconds
        //TODO Implementing await will have insignificant impact. Deferred
        [HttpPost]
        [ActionName("CreatePassbookByStarbucksAccount")]
        [ApiOAuth2UserId]
        public async Task<HttpResponseMessage> CreatePassbookByStarbucksAccount(string userId, string cardId, [FromBody] Device device, string market = null, string locale = null)
        {
            _logCounterWrapper.Increment("Card_CreatePassbookByStarbucksAccount");

            try
            {
                Guard.EnsureArgumentIsNotNullOrEmpty(userId, "userId");
                Guard.EnsureArgumentIsNotNullOrEmpty(cardId, "cardId");

                //TODO: Fix in Localization Helper in passbook
                if (!string.IsNullOrWhiteSpace(market))
                {
                    market = market.ToUpper();
                }

                if (device == null || string.IsNullOrWhiteSpace(device.DeviceId))
                {
                    throw new ApiException(HttpStatusCode.BadRequest, "121020", "Please supply a device id.");
                }

                if (string.IsNullOrWhiteSpace(market) || string.IsNullOrWhiteSpace(locale))
                {
                    MapLocaleMarket(userId, ref market, ref locale);
                }
                ValidateLocaleAndMarket(market, locale);

                var card = _cardProvider.GetCardById(userId, cardId, VisibilityLevel.Decrypted, true, market, market);

                if (card == null)
                {
                    throw new ApiException(
                        HttpStatusCode.BadRequest,
                        CardWebAPIErrorResource.CardNotFoundCode,
                        CardWebAPIErrorResource.CardNotFoundMessage);
                }
                var cardInfo = Converters.GetCardInfo(card);
                var passResponse = await _passbookService.CreatePkPass(cardInfo, device.DeviceId,
                    GetFavoriteStores(card.RegisteredUserId),
                    new PassbookLocaleInfo { Market = market, Locale = locale });

                if (passResponse == null || passResponse.Headers.Location == null)
                {
                    return _requestHelper.CreateResponse(this, HttpStatusCode.ServiceUnavailable).Message as HttpResponseMessage; ;
                }
                var auditLog = new Log
                {
                    UserId = card.RegisteredUserId.Substring(0,card.RegisteredUserId.Length/2),
                    CardId = card.CardId.Substring(0,card.CardId.Length/2),
                    InternalDeviceId = device.DeviceId,
                    RequestUri = _requestHelper.GetRequestUri(this).ToString(),
                    Message = "CreatePassbookByStarbucksAccount called"
                };

                _logHelper.WriteAuditLog("APILog", "CreatePassbookByStarbucksAccount", "APILog", "CreatePassbookByStarbucksCalled", auditLog);

                var response = _requestHelper.CreateResponse(this, HttpStatusCode.Created);
                response.Headers.Location = passResponse.Headers.Location;

                return response.Message as HttpResponseMessage;
            }
            catch (Exception exc)
            {
                _logHelper.WriteErrorLog("Card_CreatePassbookByStarbucksAccount_Error", _requestHelper.GetContent(this).ToString(), exc);
                _logCounterWrapper.Increment("Card_CreatePassbookByStarbucksAccount_Error");

                throw;
            }
        }

        private string MapLocaleMarket(string userId, ref string market, ref string locale)
        {
            var initialLocale = locale;

            if (string.IsNullOrWhiteSpace(market))
            {
                market = _accountProvider.GetUserSubMarket(userId);
            }

            if (string.IsNullOrWhiteSpace(locale))
            {
                var validLocales = _settingsProvider.GetLocaleByMarket(market);

                locale = validLocales.FirstOrDefault();
            }

            return locale;
        }

        private List<PassbookFavoriteLocation> GetFavoriteStores(string userId)
        {
            var accountFavoriteStores = _accountProvider.GetFavoriteStores(userId);

            var favoriteStores = accountFavoriteStores
                .SafeSelect(s => Converters.ToFavoriteStore(
                    _locationHelper.GetStoreById(s.StoreId), s))
                .Where(s => s != null);

            return favoriteStores.ToList();
        }

        protected void ValidateLocaleAndMarket(string market, string locale)
        {
            Guard.EnsureArgumentIsNotNullOrEmpty(market, "market");
            Guard.EnsureArgumentIsNotNullOrEmpty(locale, "locale");
            string currencyCode = null;
            IEnumerable<string> locales = null;
            try
            {
                currencyCode = _settingsProvider.GetCurrencyCodeForMarketCode(market);
            }
            catch
            {
                throw new ApiException(HttpStatusCode.BadRequest, "121054", "Invalid market provided.");
            }

            if (currencyCode == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "121054", "Invalid market provided.");
            }

            try
            {
                locales = _settingsProvider.GetLocaleByMarket(market);
            }
            catch
            {
                throw new ApiException(HttpStatusCode.BadRequest, "121055", "Invalid locale provided.");
            }
            var isValid = locales.Any(p => locale.Equals(p, StringComparison.OrdinalIgnoreCase));

            if (!isValid)
            {
                throw new ApiException(HttpStatusCode.BadRequest, "121055", "Invalid locale provided.");
            }
        }
    }
}