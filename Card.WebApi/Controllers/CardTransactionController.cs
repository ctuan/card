﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;
using Card.WebApi.Helpers;
using Card.WebApi.Filters;
using OAuth2.Provider.Web;
using Starbucks.Card.Provider.Common.ErrorResources;
using Starbucks.Card.Provider.Common.Exceptions;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Resources;
using Starbucks.CardTransaction.Provider.Common;
using Starbucks.CardTransaction.Provider.Common.ErrorResource;
using Starbucks.CardTransaction.Provider.Common.Model;
using Starbucks.Common.ResourceMapping;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;
using Starbucks.SecurePayPalPayment.Provider.Common;
using Starbucks.SecurePayment.Provider.Common;
using Account.Provider.Common;
using Starbucks.OpenApi.IpAddress.ParameterBindings;
using Card.WebApi.HttpParameterTransforms;

namespace Card.WebApi.Controllers
{
    public class CardTransactionController : BaseController
    {
        private readonly ICardTransactionProvider _cardTransactionProvider;
        private readonly IAccountProvider _accountProvider;

        public CardTransactionController(ICardTransactionProvider cardTransactionProvider, IAccountProvider accountProvider)
        {
            LoadResourceMap();
            _cardTransactionProvider = cardTransactionProvider;
            if (_cardTransactionProvider == null)
            {
                throw new ArgumentNullException("cardTransactionProvider");
            }
            _accountProvider = accountProvider;
        }

        private void LoadResourceMap()
        {
            #region CardTransactionValidationException
            ResourceMap.Map(new ResourceMapItem(typeof(CardTransactionValidationException),
                                                CardTransactionValidationErrorResource.InvalidDestinationCardIdCode,
                                                CardWebAPIErrorResource.ValidationNoDestinationCardIdCode,
                                                CardWebAPIErrorResource.ValidationNoDestinationCardIdMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardTransactionValidationException),
                                                CardTransactionValidationErrorResource.InvalidSourceCardIdCode,
                                                CardWebAPIErrorResource.ValidationNoSourceCardIdCode,
                                                CardWebAPIErrorResource.ValidationNoSourceCardIdMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardTransactionValidationException),
                                                CardTransactionValidationErrorResource.InvalidPaymentMethodIdCode,
                                                CardWebAPIErrorResource.ValidationNoPaymentMethodCode,
                                                CardWebAPIErrorResource.ValidationNoPaymentMethodMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardTransactionValidationException),
                                                CardTransactionValidationErrorResource.InvalidEmailAddressCode,
                                                CardWebAPIErrorResource.ValidationInvalidEmailAddressCode,
                                                CardWebAPIErrorResource.ValidationInvalidEmailAddressMessage,
                                                HttpStatusCode.BadRequest))
                        .Map(new ResourceMapItem(typeof(CardTransactionValidationException),
                                                CardTransactionValidationErrorResource.InvalidReloadAmountCode,
                                                CardWebAPIErrorResource.ValidationInvalidReloadAmountCode,
                                                CardWebAPIErrorResource.ValidationInvalidReloadAmountMessage,
                                                HttpStatusCode.BadRequest))
                        .Map(new ResourceMapItem(typeof(CardTransactionValidationException),
                                                CardTransactionValidationErrorResource.InvalidBillingAgreementIdCode,
                                                CardWebAPIErrorResource.PaypalBillingAgreementInvalidCode,
                                                CardWebAPIErrorResource.PaypalBillingAgreementInvalidMessage,
                                                HttpStatusCode.BadRequest))
                                                ;
            #endregion

            #region CardTransactionProviderException

            ResourceMap.Map(new ResourceMapItem(typeof(CardTransactionException),
                                                CardTransactionErrorResource.UserNotFoundCode,
                                                CardWebAPIErrorResource.GeneralNotFoundCode,
                                                CardWebAPIErrorResource.GeneralNotFoundMessage,
                                                HttpStatusCode.NotFound))
                       .Map(new ResourceMapItem(typeof(CardTransactionException),
                                                CardTransactionErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardTransactionException),
                                                CardTransactionErrorResource.PaymentMethodNotFoundCode,
                                                CardWebAPIErrorResource.InvalidPaymentMethodIdCode,
                                                CardWebAPIErrorResource.InvalidPaymentMethodIdMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardTransactionException),
                                                CardTransactionErrorResource.BillingAddressNotFoundCode,
                                                CardWebAPIErrorResource.MissingBillingAddressCode,
                                                CardWebAPIErrorResource.MissingBillingAddressMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardTransactionException),
                                                CardTransactionErrorResource.BillingInfoNotFoundCode,
                                                CardWebAPIErrorResource.MissingBillingInfoCode,
                                                CardWebAPIErrorResource.MissingBillingInfoMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardTransactionException),
                                                CardTransactionErrorResource.MerchantInfoNotFoundCode,
                                                CardWebAPIErrorResource.MissingMerchantInfoCode,
                                                CardWebAPIErrorResource.MissingMerchantInfoMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardTransactionException),
                                                CardTransactionErrorResource.CreditCardBillFailedCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardTransactionException),
                                                CardTransactionErrorResource.GeneralPaymentErrorCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardTransactionException),
                                                CardTransactionErrorResource.CannotReloadCardClassCode,
                                                CardWebAPIErrorResource.InvalidOperationForCardClassCode,
                                                CardWebAPIErrorResource.InvalidOperationForCardClassMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardTransactionException),
                                                CardTransactionErrorResource.CreditCardBillingAddressErrorCode,
                                                CardWebAPIErrorResource
                                                    .InvalidOrMissingBillingAddressDataAssociatedToPaymentMethodCode,
                                                CardWebAPIErrorResource
                                                    .InvalidOrMissingBillingAddressDataAssociatedToPaymentMethodMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardTransactionException),
                                                CardTransactionErrorResource.CannotAuthorizeCreditCardCode,
                                                CardWebAPIErrorResource.RequestDeniedCode,
                                                CardWebAPIErrorResource.RequestDeniedMessage,
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardTransactionException),
                                                CardTransactionErrorResource.FraudCheckFailedCode,
                                                CardWebAPIErrorResource.RequestDeniedCode,
                                                CardWebAPIErrorResource.RequestDeniedMessage,
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardTransactionException),
                                                CardTransactionErrorResource.CannotAuthorizePayPalCode ,
                                                CardWebAPIErrorResource.PaypalAuthorizationFailedCode ,
                                                CardWebAPIErrorResource.PaypalAuthorizationFailedMessage ,
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardTransactionException),
                                                CardTransactionErrorResource.CannotReloadCardMarketCode,
                                                CardWebAPIErrorResource.InvalidOperationForMarketCode,
                                                CardWebAPIErrorResource.InvalidOperationForMarketMessage,
                                                HttpStatusCode.BadRequest))
                                                ;
            #endregion

            #region CardProviderValidationException

            ResourceMap.Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationCardNumberMissingCode,
                                                CardWebAPIErrorResource.ValidationCardNumberMissingCode,
                                                CardWebAPIErrorResource.ValidationCardNumberMissingMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationCardPinMissingCode,
                                                CardWebAPIErrorResource.ValidationCardPinMissingCode,
                                                CardWebAPIErrorResource.ValidationCardPinMissingMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationNoAutoReloadAmountCode,
                                                CardWebAPIErrorResource.ValidationNoAutoReloadAmountCode,
                                                CardWebAPIErrorResource.ValidationNoAutoReloadAmountMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationNoAutoReloadDayCode,
                                                CardWebAPIErrorResource.ValidationNoAutoReloadDayCode,
                                                CardWebAPIErrorResource.ValidationNoAutoReloadDayMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource
                                                    .ValidationNoAutoReloadTriggerAmountCode,
                                                CardWebAPIErrorResource.ValidationNoAutoReloadTriggerAmountCode,
                                                CardWebAPIErrorResource.ValidationNoAutoReloadTriggerAmountMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationNoAutoReloadTypeCode,
                                                CardWebAPIErrorResource.ValidationNoAutoReloadTypeCode,
                                                CardWebAPIErrorResource.ValidationNoAutoReloadTypeMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationNoDestinationCardIdCode,
                                                CardWebAPIErrorResource.ValidationNoDestinationCardIdCode,
                                                CardWebAPIErrorResource.ValidationNoDestinationCardIdMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationNoPaymentMethodCode,
                                                CardWebAPIErrorResource.ValidationNoPaymentMethodCode,
                                                CardWebAPIErrorResource.ValidationNoPaymentMethodMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationNoReloadAmountCode,
                                                CardWebAPIErrorResource.ValidationNoReloadAmountCode,
                                                CardWebAPIErrorResource.ValidationNoReloadAmountMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationNoRequestCode,
                                                CardWebAPIErrorResource.ValidationNoRequestCode,
                                                CardWebAPIErrorResource.ValidationNoRequestMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationNoSourceCardIdCode,
                                                CardWebAPIErrorResource.ValidationNoSourceCardIdCode,
                                                CardWebAPIErrorResource.ValidationNoSourceCardIdMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource
                                                    .ValidationRegisterMultipleNoCardNumberCode,
                                                CardWebAPIErrorResource.ValidationCardNumberMissingCode,
                                                CardWebAPIErrorResource.ValidationCardNumberMissingMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationRegisterMultipleNoPinCode,
                                                CardWebAPIErrorResource.ValidationCardPinMissingCode,
                                                CardWebAPIErrorResource.ValidationCardPinMissingMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationNoNicknameCode,
                                                CardWebAPIErrorResource.ValidationNoNicknameCode,
                                                CardWebAPIErrorResource.ValidationNoNicknameMessage,
                                                HttpStatusCode.BadRequest));

            #endregion

            #region CardProviderException

            ResourceMap.Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.AutoReloadNotAssociatedToPaymentMethodCode,
                                                CardWebAPIErrorResource.AutoReloadNotAssociatedToPaymentMethodCode,
                                                CardWebAPIErrorResource.AutoReloadNotAssociatedToPaymentMethodMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.AutoReloadNotFoundCode,
                                                CardWebAPIErrorResource.GeneralNotFoundCode,
                                                CardWebAPIErrorResource.GeneralNotFoundMessage,
                                                HttpStatusCode.NotFound))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.CardIsAlreadyRegisteredCode,
                                                CardWebAPIErrorResource.CardAlreadyRegisteredCode,
                                                CardWebAPIErrorResource.CardAlreadyRegisteredMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.CardNotRegisteredToUserCode,
                                                CardWebAPIErrorResource.GeneralForbiddenCode,
                                                CardWebAPIErrorResource.GeneralForbiddenMessage,
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.InsufficientFundsForTransactionCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.InvalidCardNumberCode,
                                                CardWebAPIErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundCode,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.CannotRegisterInvalidCardNumberCode,
                                                CardWebAPIErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.InvalidPinCode,
                                                CardWebAPIErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.UnknownErrorCode,
                                                CardWebAPIErrorResource.InternalServerErrorCode,
                                                CardWebAPIErrorResource.InternalServerErrorMessage,
                                                HttpStatusCode.InternalServerError))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.UnregisterNonZeroBalanceCode,
                                                CardWebAPIErrorResource.CannotUnregisterCardWithNonZeroBalanceCode,
                                                CardWebAPIErrorResource.CannotUnregisterCardWithNonZeroBalanceMessage,
                                                HttpStatusCode.BadRequest));

            #endregion

            #region CardIssuerException

            ResourceMap.Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.AccountIsClosedCode,
                                                CardWebAPIErrorResource.CardIsClosedCode,
                                                CardWebAPIErrorResource.CardIsClosedMessage,
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CardIsAlreadyRegisteredCode,
                                                CardWebAPIErrorResource.CardAlreadyRegisteredCode,
                                                CardWebAPIErrorResource.CardAlreadyRegisteredMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CardIsInactiveCode,
                                                CardWebAPIErrorResource.CardIssuerForbiddenCode,
                                                CardWebAPIErrorResource.CardIssuerForbiddenMessage,
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CardNotFoundCode,
                                               CardWebAPIErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CardNotRegisteredToUserCode,
                                                CardWebAPIErrorResource.CardIssuerForbiddenCode,
                                                CardWebAPIErrorResource.CardIssuerForbiddenMessage,
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CardReportedLostStolenCode,
                                                CardWebAPIErrorResource.CardIssuerForbiddenCode,
                                                CardWebAPIErrorResource.CardIssuerForbiddenMessage,
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CardReportedLostStolenInternetDisabledMessage,
                                                CardWebAPIErrorResource.CardIssuerForbiddenCode,
                                                CardWebAPIErrorResource.CardIssuerForbiddenMessage,
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CreditCardNumberDoesNotMatchTypeCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CreditCardNumberInvalidCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.GeneralPaymentErrorCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.InsufficientFundsForTransactionCode,
                                                CardWebAPIErrorResource.InsufficientFundsCode,
                                                CardWebAPIErrorResource.InsufficientFundsMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource
                                                    .InvalidAmountMoreOrLessThanMinOrMaxAmountSpecifiedCode,
                                                CardWebAPIErrorResource.InvalidAmountMoreOrLessThanMinOrMaxAmountSpecifiedCode,
                                                CardWebAPIErrorResource.InvalidAmountMoreOrLessThanMinOrMaxAmountSpecifiedMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.InvalidCardNumberCode,
                                                CardWebAPIErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.InvalidPinCode,
                                                CardWebAPIErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.InvalidSubMarketCode,
                                                CardWebAPIErrorResource.GeneralNotFoundCode,
                                                CardWebAPIErrorResource.GeneralNotFoundMessage,
                                                HttpStatusCode.NotFound))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.MaximumBalanceExceededCode,
                                                CardWebAPIErrorResource.MaxBalanceExceededCode,
                                                CardWebAPIErrorResource.MaxBalanceExceededMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.NoRegistrationAddressOnFileCode,
                                                CardWebAPIErrorResource.NoRegistrationAddressOnFileCode,
                                                CardWebAPIErrorResource.NoRegistrationAddressOnFileMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.RequestNotPermittedByThisAccountCode,
                                                CardWebAPIErrorResource.CardIssuerForbiddenCode,
                                                CardWebAPIErrorResource.CardIssuerForbiddenMessage,
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.UnknownErrorCode,
                                                CardWebAPIErrorResource.InternalServerErrorCode,
                                                CardWebAPIErrorResource.InternalServerErrorMessage,
                                                HttpStatusCode.InternalServerError))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.ValueLinkHostDownCode,
                                                CardWebAPIErrorResource.FirstDataServiceUnavailableCode,
                                                CardWebAPIErrorResource.FirstDataServiceUnavailableMessage,
                                                HttpStatusCode.BadGateway));

            #endregion

            #region SecurePaymentException

            ResourceMap.Map(new ResourceMapItem(typeof(SecurePaymentException),
                                                SecurePaymentErrorResource.AFSFailedCode,
                                                CardWebAPIErrorResource.CreditCardFraudCode,
                                                CardWebAPIErrorResource.CreditCardFraudMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePaymentException),
                                                SecurePaymentErrorResource.AVSFailedCode,
                                                CardWebAPIErrorResource.CreditCardFraudCode,
                                                CardWebAPIErrorResource.CreditCardFraudMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePaymentException),
                                                SecurePaymentErrorResource.BillingInformationContainsInvalidFieldsCode,
                                                CardWebAPIErrorResource
                                                    .InvalidOrMissingBillingAddressDataAssociatedToPaymentMethodCode,
                                                CardWebAPIErrorResource
                                                    .InvalidOrMissingBillingAddressDataAssociatedToPaymentMethodMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePaymentException),
                                                SecurePaymentErrorResource.BillingInformationMissingFieldsCode,
                                                CardWebAPIErrorResource
                                                    .InvalidOrMissingBillingAddressDataAssociatedToPaymentMethodCode,
                                                CardWebAPIErrorResource
                                                    .InvalidOrMissingBillingAddressDataAssociatedToPaymentMethodMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePaymentException),
                                                SecurePaymentErrorResource.CreditCardInsufficientFundsCode,
                                                CardWebAPIErrorResource.CreditCardNsfCode,
                                                CardWebAPIErrorResource.CreditCardNsfMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePaymentException),
                                                SecurePaymentErrorResource.CreditCardIsExpiredCode,
                                                CardWebAPIErrorResource.CreditCardExpiredCode,
                                                CardWebAPIErrorResource.CreditCardExpiredMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePaymentException),
                                                SecurePaymentErrorResource.CreditCardNumberDoesNotMatchTypeCode,
                                                CardWebAPIErrorResource.CreditCardNumberInvalidTypeCode,
                                                CardWebAPIErrorResource.CreditCardNumberInvalidTypeMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePaymentException),
                                                SecurePaymentErrorResource.CreditCardNumberIsInvalidCode,
                                                CardWebAPIErrorResource.CreditCardNumberInvalidCode,
                                                CardWebAPIErrorResource.CreditCardNumberInvalidMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePaymentException),
                                                SecurePaymentErrorResource.FraudScoreExceedsThresholdCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePaymentException),
                                                SecurePaymentErrorResource.GeneralPaymentErrorCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePaymentException),
                                                SecurePaymentErrorResource.InvalidCVNCode,
                                                CardWebAPIErrorResource.CreditCardCvnInvalidCode,
                                                CardWebAPIErrorResource.CreditCardCvnInvalidMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePaymentException),
                                                SecurePaymentErrorResource.OrderRejectedByDecisionManagerCode,
                                                CardWebAPIErrorResource.CreditCardFraudCode,
                                                CardWebAPIErrorResource.CreditCardFraudMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePaymentException),
                                                SecurePaymentErrorResource.TimeoutProcessingPaymentInformationCode,
                                                CardWebAPIErrorResource.PaymentProcessingGatewayErrorCode,
                                                CardWebAPIErrorResource.PaymentProcessingGatewayErrorMessage,
                                                HttpStatusCode.BadGateway))
                       .Map(new ResourceMapItem(typeof(SecurePaymentException),
                                                SecurePaymentErrorResource.UnexpectedErrorCode,
                                                CardWebAPIErrorResource.PaymentProcessingGatewayErrorCode,
                                                CardWebAPIErrorResource.PaymentProcessingGatewayErrorMessage,
                                                HttpStatusCode.BadGateway));


            #endregion
            #region SecurePayPalPaymentService

            ResourceMap.Map(new ResourceMapItem(typeof(SecurePayPalPaymentException),
                                                SecurePayPalPaymentErrorResource.BillingAgreementInvalidCode,
                                                CardWebAPIErrorResource.PaypalBillingAgreementInvalidCode,
                                                CardWebAPIErrorResource.PaypalBillingAgreementInvalidMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePayPalPaymentException),
                                                SecurePayPalPaymentErrorResource.UserActionRequiredCode,
                                                CardWebAPIErrorResource.PaypalUserActionRequiredCode,
                                                CardWebAPIErrorResource.PaypalUserActionRequiredMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePayPalPaymentException),
                                                SecurePayPalPaymentErrorResource.AccountClosedCode,
                                                CardWebAPIErrorResource.PaypalAccountClosedCode,
                                                CardWebAPIErrorResource.PaypalAccountClosedMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePayPalPaymentException),
                                                SecurePayPalPaymentErrorResource.RiskCode,
                                                CardWebAPIErrorResource.PaypalRiskCode,
                                                CardWebAPIErrorResource.PaypalRiskMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePayPalPaymentException),
                                                SecurePayPalPaymentErrorResource.DuplicateTransactionCode,
                                                CardWebAPIErrorResource.PaypalDuplicateTransactionCode,
                                                CardWebAPIErrorResource.PaypalDuplicateTransactionMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePayPalPaymentException),
                                                SecurePayPalPaymentErrorResource.TemporaryFailureCode,
                                                CardWebAPIErrorResource.PaypalTemporaryFailureCode,
                                                CardWebAPIErrorResource.PaypalTemporaryFailureMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePayPalPaymentException),
                                                SecurePayPalPaymentErrorResource.InvalidCCVCode,
                                                CardWebAPIErrorResource.PaypalInvalidCCVCode,
                                                CardWebAPIErrorResource.PaypalInvalidCCVMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePayPalPaymentException),
                                                SecurePayPalPaymentErrorResource.InvalidCreditCardNumberCode,
                                                CardWebAPIErrorResource.PaypalInvalidCreditCardNumberCode,
                                                CardWebAPIErrorResource.PaypalInvalidCreditCardNumberMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePayPalPaymentException),
                                                SecurePayPalPaymentErrorResource.InvalidFundingSourceCode,
                                                CardWebAPIErrorResource.PaypalInvalidFundingSourceCode,
                                                CardWebAPIErrorResource.PaypalInvalidFundingSourceMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePayPalPaymentException),
                                                SecurePayPalPaymentErrorResource.FraudReviewCode,
                                                CardWebAPIErrorResource.PaypalFraudReviewCode,
                                                CardWebAPIErrorResource.PaypalFraudReviewMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePayPalPaymentException),
                                                SecurePayPalPaymentErrorResource.BlockedByAmexCode,
                                                CardWebAPIErrorResource.PaypalBlockedByAmexCode,
                                                CardWebAPIErrorResource.PaypalBlockedByAmexMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePayPalPaymentException),
                                                SecurePayPalPaymentErrorResource.BuyerCannotPayCode,
                                                CardWebAPIErrorResource.PaypalBuyerCannotPayCode,
                                                CardWebAPIErrorResource.PaypalBuyerCannotPayMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(SecurePayPalPaymentException),
                                                SecurePayPalPaymentErrorResource.UnknownCode,
                                                CardWebAPIErrorResource.PaypalUnknownCode,
                                                CardWebAPIErrorResource.PaypalUnknownMessage,
                                                HttpStatusCode.BadRequest));

            #endregion

        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.ActionName("ReloadByAccount")]
        [ApiOAuth2UserId]
        [ReloadCardByStarbucksAccountFilter]
        public HttpResponseMessage ReloadCardByAccount(string userId, string cardId, [TransformFromBody(typeof(ModelWithRiskTransform))] Card.WebApi.Models.ReloadForStarbucksCard reloadForStarbucksCard)
        {
            IncrementLogCounter();
            ICardTransaction transaction = null;
            try
            {
                ValidateBillingAddress(reloadForStarbucksCard);
                ValidateToken(reloadForStarbucksCard);

                //in case we allow ?market= at some point
                var market = GetMarket(userId).ToUpper();

                transaction = _cardTransactionProvider.ReloadStarbucksCard(userId, cardId,
                                                                           reloadForStarbucksCard.ToProvider(market),
                                                                           market, market, reloadForStarbucksCard.EmailAddress);
            }
            catch (CardTransactionException cpe)
            {
                IncrementLogCounterError();
                if (cpe.Code == CardTransactionErrorResource.CannotAuthorizeCreditCardCode)
                {
                    if (reloadForStarbucksCard.RiskFields == null) // if no risk container is provided, it means we are called from a legacy client, so don't return new error status
                    {
                        throw new ApiException(HttpStatusCode.InternalServerError, CardWebAPIErrorResource.RequestDeniedCode,
                                               CardWebAPIErrorResource.RequestDeniedMessage);
                    }
                    else
                    {
                        throw new ApiException(HttpStatusCode.Forbidden, CardWebAPIErrorResource.RequestDeniedCode,
                                               CardWebAPIErrorResource.RequestDeniedMessage);
                    }
                }
                throw GetApiException(cpe);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }

            if (transaction == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, CardWebAPIErrorResource.GeneralNotFoundCode,
                                       CardWebAPIErrorResource.GeneralNotFoundMessage);
            }

            return Request.CreateResponse(HttpStatusCode.OK, transaction.ToApi());
        }

        private static void ValidateToken(Card.WebApi.Models.ReloadForStarbucksCard reloadForStarbucksCard)
        {
            if (reloadForStarbucksCard == null)
            {
                return;
            }

            if (!string.IsNullOrWhiteSpace(reloadForStarbucksCard.PaymentTokenType))
            {
                if (!string.Equals(reloadForStarbucksCard.PaymentTokenType, "ChasePay", StringComparison.InvariantCultureIgnoreCase) && 
                    !string.Equals(reloadForStarbucksCard.PaymentTokenType, "ApplePay", StringComparison.InvariantCultureIgnoreCase))
                {
                    throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.InvalidPaymentTokenTypeCode,
                                           CardWebAPIErrorResource.InvalidPaymentTokenTypeMessage);

                }
            }

            if (reloadForStarbucksCard.PaymentTokenExtension != null)
            {
                if (string.IsNullOrWhiteSpace(reloadForStarbucksCard.PaymentTokenExtension.PaymentCryptogram))
                {
                    string msg = CardWebAPIErrorResource.MissingTokenFieldMessage;
                    string errMsg = string.Format(msg, "paymentCryptogram");
                    throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.MissingTokenFieldCode,
                                           errMsg);
                }

                if (string.IsNullOrWhiteSpace(reloadForStarbucksCard.PaymentTokenExtension.TokenRequestorId))
                {
                    string msg = CardWebAPIErrorResource.MissingTokenFieldMessage;
                    string errMsg = string.Format(msg, "tokenRequestorId");
                    throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.MissingTokenFieldCode,
                                           errMsg);
                }

                if (reloadForStarbucksCard.PaymentTokenExtension.ExpirationYear != null ||
                    reloadForStarbucksCard.PaymentTokenExtension.ExpirationMonth != null)
                {
                    int expirationYear = reloadForStarbucksCard.PaymentTokenExtension.ExpirationYear.Value;
                    int expirationMonth = reloadForStarbucksCard.PaymentTokenExtension.ExpirationMonth.Value;

                    DateTime currentDataTime = DateTime.Now;
                    if (expirationYear > currentDataTime.Year)
                    {
                        return;
                    }
                    else if (expirationYear == currentDataTime.Year)
                    {
                        if (expirationMonth < currentDataTime.Month)
                        {
                            throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.PaymentTokenDateHasExpiredCode,
                                                   CardWebAPIErrorResource.PaymentTokenDateHasExpiredMessage);
                        }
                    }
                    else // expiration year < current year
                    {
                        throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.PaymentTokenDateHasExpiredCode,
                                               CardWebAPIErrorResource.PaymentTokenDateHasExpiredMessage);
                    }
                }
            }

        }

        private static void ValidateBillingAddress(Card.WebApi.Models.ReloadForStarbucksCard reloadForStarbucksCard)
        {
            if (reloadForStarbucksCard == null || 
                reloadForStarbucksCard.PaymentToken == null ||
                string.IsNullOrWhiteSpace(reloadForStarbucksCard.PaymentTokenType))
            {
                return;
            }

            bool validateAddress = false;
            // ApplePay
            if (reloadForStarbucksCard.PaymentTokenType.Equals("ApplePay", StringComparison.InvariantCultureIgnoreCase))
            { 
                validateAddress = true;
            }
            else if (reloadForStarbucksCard.PaymentTokenType.Equals("ChasePay", StringComparison.InvariantCultureIgnoreCase))
            {
                if (reloadForStarbucksCard.PaymentTokenExtension != null && 
                    reloadForStarbucksCard.PaymentTokenExtension.MobilePOS == null)
                {
                    validateAddress = true;
                }
            }
    
            if (validateAddress)
            {
                if (reloadForStarbucksCard.BillingAddress == null ||
                    string.IsNullOrWhiteSpace(reloadForStarbucksCard.BillingAddress.AddressLine1) ||
                    string.IsNullOrWhiteSpace(reloadForStarbucksCard.BillingAddress.City) ||
                    string.IsNullOrWhiteSpace(reloadForStarbucksCard.BillingAddress.Country))
                {
                    throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.InvalidOrMissingBillingAddressDataAssociatedToPaymentMethodCode,
                        CardWebAPIErrorResource.InvalidOrMissingBillingAddressDataAssociatedToPaymentMethodMessage);
                }
            }

            // don't need billing address for chase pay's POS reload
            SetBillingAddressToNullForChasePayMobilePos(reloadForStarbucksCard);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.ActionName("ReloadByCardNumber")]
        [ReloadCardByCardNumberPinFilter]
        public HttpResponseMessage ReloadCardByCardNumber(string cardNumber, string pin, [TransformFromBody(typeof(ModelWithRiskTransform))] Card.WebApi.Models.ReloadForStarbucksCard reloadForStarbucksCard, [FromUri] string market = "US")
        {
            IncrementLogCounter();
            ICardTransaction transaction;
            try
            {
                ValidateBillingAddress(reloadForStarbucksCard);
                ValidateToken(reloadForStarbucksCard);

                var reload = reloadForStarbucksCard.ToProvider(market);
                if (reloadForStarbucksCard.Platform == null)
                {
                    reloadForStarbucksCard.Platform = ConfigurationManager.AppSettings["Platform"];
                }

                transaction = _cardTransactionProvider.ReloadStarbucksCardByCardNumberPin(cardNumber, pin, reload, reloadForStarbucksCard.EmailAddress, market);

            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }

            if (transaction == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, CardWebAPIErrorResource.GeneralNotFoundCode,
                                       CardWebAPIErrorResource.GeneralNotFoundMessage);
            }
            return Request.CreateResponse(HttpStatusCode.OK, transaction.ToApi());
        }

        internal static void SetBillingAddressToNullForChasePayMobilePos(Models.ReloadForStarbucksCard reloadForStarbucksCard)
        {
            if (!string.IsNullOrWhiteSpace(reloadForStarbucksCard.PaymentTokenType) &&
                reloadForStarbucksCard.PaymentTokenType.Equals("ChasePay", StringComparison.InvariantCultureIgnoreCase))
            {
                if (reloadForStarbucksCard.PaymentTokenExtension != null &&
                    reloadForStarbucksCard.PaymentTokenExtension.MobilePOS != null)
                {
                    reloadForStarbucksCard.BillingAddress = null;
                }
            }
        }

        private string GetMarket(string userId)
        {
            var user = _accountProvider.GetUser(userId);
            return user != null ? user.SubMarket : string.Empty;
        }
    }
}
