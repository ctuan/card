﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Card.WebApi.Filters;
using Card.WebApi.Helpers;
using Card.WebApi.Models;
using OAuth2.Provider.Web;
using Starbucks.Card.Provider.Common;
using Starbucks.Card.Provider.Common.Enums;
using Starbucks.Card.Provider.Common.ErrorResources;
using Starbucks.Card.Provider.Common.Exceptions;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.Common.Resources;
using Starbucks.CardTransaction.Provider.Common;
using Starbucks.CardTransaction.Provider.Common.ErrorResource;
using Starbucks.Common.ResourceMapping;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;
using Starbucks.OpenApi.WebApi.Common.Filters;
using StarbucksCard = Card.WebApi.Models.StarbucksCard;
using StarbucksCardImage = Card.WebApi.Models.StarbucksCardImage;
using StarbucksCardNumberAndPin = Card.WebApi.Models.StarbucksCardNumberAndPin;
using Starbucks.OpenApi.Logging.Common;
using System.Diagnostics;
using Account.Provider.Common;
using Account.Provider.Common.Models;
using Starbucks.OpenApi.IpAddress.ParameterBindings;
using Card.WebApi.HttpParameterTransforms;

namespace Card.WebApi.Controllers
{
    public class CardController : BaseController
    {
        private static ICardProvider _cardProvider;
        private readonly IAccountProvider _accountProvider;
        private readonly ViewModelStore _viewModelStore;
        private readonly ILogRepository _logRepository;

        private void LoadResourceMap()
        {
            #region CardTransactionValidationException

            ResourceMap.Map(new ResourceMapItem(typeof(CardTransactionValidationException),
                                                CardTransactionValidationErrorResource.InvalidDestinationCardIdCode,
                                                CardWebAPIErrorResource.ValidationNoDestinationCardIdCode,
                                                CardWebAPIErrorResource.ValidationNoDestinationCardIdMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardTransactionValidationException),
                                                CardTransactionValidationErrorResource.InvalidSourceCardIdCode,
                                                CardWebAPIErrorResource.ValidationNoSourceCardIdCode,
                                                CardWebAPIErrorResource.ValidationNoSourceCardIdMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardTransactionValidationException),
                                                CardTransactionValidationErrorResource.InvalidPaymentMethodIdCode,
                                                CardWebAPIErrorResource.InvalidPaymentMethodIdCode,
                                                CardWebAPIErrorResource.InvalidPaymentMethodIdMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardTransactionValidationException),
                                                CardTransactionValidationErrorResource.InvalidEmailAddressCode,
                                                CardWebAPIErrorResource.ValidationInvalidEmailAddressCode,
                                                CardWebAPIErrorResource.ValidationInvalidEmailAddressMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardTransactionValidationException),
                                                CardTransactionValidationErrorResource.InvalidReloadAmountCode,
                                                CardWebAPIErrorResource.ValidationInvalidReloadAmountCode,
                                                CardWebAPIErrorResource.ValidationInvalidReloadAmountMessage,
                                                HttpStatusCode.BadRequest));

            #endregion

            #region CardTransactionProviderException

            ResourceMap.Map(new ResourceMapItem(typeof(CardTransactionException),
                                                CardTransactionErrorResource.UserNotFoundCode,
                                                CardWebAPIErrorResource.GeneralNotFoundCode,
                                                CardWebAPIErrorResource.GeneralNotFoundMessage,
                                                HttpStatusCode.NotFound))
                       .Map(new ResourceMapItem(typeof(CardTransactionException),
                                                CardTransactionErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardTransactionException),
                                                CardTransactionErrorResource.PaymentMethodNotFoundCode,
                                                CardWebAPIErrorResource.InvalidPaymentMethodIdCode,
                                                CardWebAPIErrorResource.InvalidPaymentMethodIdMessage,
                                                HttpStatusCode.BadRequest));

            #endregion

            #region CardProviderValidationException

            ResourceMap.Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationCardNumberMissingCode,
                                                CardWebAPIErrorResource.ValidationCardNumberMissingCode,
                                                CardWebAPIErrorResource.ValidationCardNumberMissingMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationCardPinMissingCode,
                                                CardWebAPIErrorResource.ValidationCardPinMissingCode,
                                                CardWebAPIErrorResource.ValidationCardPinMissingMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationNoAutoReloadAmountCode,
                                                CardWebAPIErrorResource.ValidationNoAutoReloadAmountCode,
                                                CardWebAPIErrorResource.ValidationNoAutoReloadAmountMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationNoAutoReloadDayCode,
                                                CardWebAPIErrorResource.ValidationNoAutoReloadDayCode,
                                                CardWebAPIErrorResource.ValidationNoAutoReloadDayMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource
                                                    .ValidationNoAutoReloadTriggerAmountCode,
                                                CardWebAPIErrorResource.ValidationNoAutoReloadTriggerAmountCode,
                                                CardWebAPIErrorResource.ValidationNoAutoReloadTriggerAmountMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationNoAutoReloadTypeCode,
                                                CardWebAPIErrorResource.ValidationNoAutoReloadTypeCode,
                                                CardWebAPIErrorResource.ValidationNoAutoReloadTypeMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationNoDestinationCardIdCode,
                                                CardWebAPIErrorResource.ValidationNoDestinationCardIdCode,
                                                CardWebAPIErrorResource.ValidationNoDestinationCardIdMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationNoPaymentMethodCode,
                                                CardWebAPIErrorResource.ValidationNoPaymentMethodCode,
                                                CardWebAPIErrorResource.ValidationNoPaymentMethodMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationNoReloadAmountCode,
                                                CardWebAPIErrorResource.ValidationNoReloadAmountCode,
                                                CardWebAPIErrorResource.ValidationNoReloadAmountMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationNoRequestCode,
                                                CardWebAPIErrorResource.ValidationNoRequestCode,
                                                CardWebAPIErrorResource.ValidationNoRequestMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationNoSourceCardIdCode,
                                                CardWebAPIErrorResource.ValidationNoSourceCardIdCode,
                                                CardWebAPIErrorResource.ValidationNoSourceCardIdMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource
                                                    .ValidationRegisterMultipleNoCardNumberCode,
                                                CardWebAPIErrorResource.ValidationCardNumberMissingCode,
                                                CardWebAPIErrorResource.ValidationCardNumberMissingMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationRegisterMultipleNoPinCode,
                                                CardWebAPIErrorResource.ValidationCardPinMissingCode,
                                                CardWebAPIErrorResource.ValidationCardPinMissingMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource.ValidationNoNicknameCode,
                                                CardWebAPIErrorResource.ValidationNoNicknameCode,
                                                CardWebAPIErrorResource.ValidationNoNicknameMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderValidationException),
                                                CardProviderValidationErrorResource
                                                    .CannotTransferABalanceAmountOfZeroCode,
                                                CardWebAPIErrorResource.CannotTransferABalanceAmountOfZeroCode,
                                                CardWebAPIErrorResource.CannotTransferABalanceAmountOfZeroMessage,
                                                HttpStatusCode.BadRequest));

            #endregion

            #region CardProviderException

            ResourceMap.Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.AutoReloadNotAssociatedToPaymentMethodCode,
                                                CardWebAPIErrorResource.AutoReloadNotAssociatedToPaymentMethodCode,
                                                CardWebAPIErrorResource.AutoReloadNotAssociatedToPaymentMethodMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.AutoReloadNotFoundCode,
                                                CardWebAPIErrorResource.GeneralNotFoundCode,
                                                CardWebAPIErrorResource.GeneralNotFoundMessage,
                                                HttpStatusCode.NotFound))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.CardIsAlreadyRegisteredCode,
                                                CardWebAPIErrorResource.CardAlreadyRegisteredCode,
                                                CardWebAPIErrorResource.CardAlreadyRegisteredMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.CardNotRegisteredToUserCode,
                                                CardWebAPIErrorResource.GeneralForbiddenCode,
                                                CardWebAPIErrorResource.GeneralForbiddenMessage,
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.InsufficientFundsForTransactionCode,
                                                CardWebAPIErrorResource.InsufficientFundsCode,
                                                CardWebAPIErrorResource.InsufficientFundsMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.RequestDeniedCode,
                                                CardWebAPIErrorResource.RequestDeniedCode,
                                                CardWebAPIErrorResource.RequestDeniedMessage,
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.InvalidCardNumberCode,
                                                CardWebAPIErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.CannotRegisterInvalidCardNumberCode,
                                                CardWebAPIErrorResource.CannotRegisterInvalidCardNumberCode,
                                                CardWebAPIErrorResource.CannotRegisterInvalidCardNumberMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.InvalidPinCode,
                                                CardWebAPIErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.UnknownErrorCode,
                                                CardWebAPIErrorResource.InternalServerErrorCode,
                                                CardWebAPIErrorResource.InternalServerErrorMessage,
                                                HttpStatusCode.InternalServerError))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.UnregisterNonZeroBalanceCode,
                                                CardWebAPIErrorResource.CannotUnregisterCardWithNonZeroBalanceCode,
                                                CardWebAPIErrorResource.CannotUnregisterCardWithNonZeroBalanceMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.CannotRegisterInvalidCardClassCode,
                                                CardWebAPIErrorResource.InvalidOperationForCardClassCode,
                                                CardWebAPIErrorResource.InvalidOperationForCardClassMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.CannotTransferFromCardClassCode,
                                                CardWebAPIErrorResource.InvalidOperationForCardClassCode,
                                                CardWebAPIErrorResource.InvalidOperationForCardClassMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.CannotTransferToCardClassCode,
                                                CardWebAPIErrorResource.InvalidOperationForCardClassCode,
                                                CardWebAPIErrorResource.InvalidOperationForCardClassMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.CannotAutoReloadMarketCode,
                                                CardWebAPIErrorResource.InvalidOperationForMarketCode,
                                                CardWebAPIErrorResource.InvalidOperationForMarketMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.InvalidSubMarketCode,
                                                CardWebAPIErrorResource.InvalidSubMarketCode,
                                                CardWebAPIErrorResource.InvalidSubMarketMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.UnableToFindPromoCode,
                                                CardWebAPIErrorResource.UnableToFindPromoCode,
                                                CardWebAPIErrorResource.UnableToFindPromoCodeMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.CannotTransferMarketCode,
                                                CardWebAPIErrorResource.InvalidOperationForMarketCode,
                                                CardWebAPIErrorResource.InvalidOperationForMarketMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardProviderException),
                                                CardProviderErrorResource.CannotTransferAfterReloadCode,
                                                 CardWebAPIErrorResource.GeneralForbiddenCode,
                                                CardWebAPIErrorResource.GeneralForbiddenMessage,
                                                HttpStatusCode.Forbidden))
                        .Map(new ResourceMapItem(typeof(CardProviderException),
                                                    CardProviderErrorResource.InvalidCardTransferCode,
                                                    CardWebAPIErrorResource.GeneralForbiddenCode,
                                                    CardWebAPIErrorResource.GeneralForbiddenMessage,
                                                    HttpStatusCode.Forbidden))
                        .Map(new ResourceMapItem(typeof(CardProviderException),
                                                    CardProviderErrorResource.InvalidAmountCode,
                                                    CardWebAPIErrorResource.InvalidAmountMoreOrLessThanMinOrMaxAmountSpecifiedCode,
                                                    CardWebAPIErrorResource.InvalidAmountMoreOrLessThanMinOrMaxAmountSpecifiedMessage,
                                                    HttpStatusCode.BadRequest));

            #endregion

            #region CardIssuerException

            ResourceMap.Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.AccountIsClosedCode,
                                                CardWebAPIErrorResource.CardIsClosedCode,
                                                CardWebAPIErrorResource.CardIsClosedMessage,
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CardIsAlreadyRegisteredCode,
                                                CardWebAPIErrorResource.CardAlreadyRegisteredCode,
                                                CardWebAPIErrorResource.CardAlreadyRegisteredMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CardIsInactiveCode,
                                                CardWebAPIErrorResource.CardIsInactiveCode,
                                                CardWebAPIErrorResource.CardIsInactiveMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.GeneralNotFoundCode,
                                                CardWebAPIErrorResource.GeneralNotFoundMessage,
                                                HttpStatusCode.NotFound))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CardNotRegisteredToUserCode,
                                                CardWebAPIErrorResource.CardIssuerForbiddenCode,
                                                CardWebAPIErrorResource.CardIssuerForbiddenMessage,
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CardReportedLostStolenCode,
                                                CardWebAPIErrorResource.CardIsLostOrStolenCode,
                                                CardWebAPIErrorResource.CardIsLostOrStolenMessage,
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CardReportedLostStolenInternetDisabledCode,
                                                CardWebAPIErrorResource.CardIsLostOrStolenCode,
                                                CardWebAPIErrorResource.CardIsLostOrStolenMessage,
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CreditCardNumberDoesNotMatchTypeCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.CreditCardNumberInvalidCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.GeneralPaymentErrorCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorCode,
                                                CardWebAPIErrorResource.GeneralPaymentErrorMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.InsufficientFundsForTransactionCode,
                                                CardWebAPIErrorResource.InsufficientFundsCode,
                                                CardWebAPIErrorResource.InsufficientFundsMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource
                                                    .InvalidAmountMoreOrLessThanMinOrMaxAmountSpecifiedCode,
                                                CardWebAPIErrorResource
                                                    .InvalidAmountMoreOrLessThanMinOrMaxAmountSpecifiedCode,
                                                CardWebAPIErrorResource
                                                    .InvalidAmountMoreOrLessThanMinOrMaxAmountSpecifiedMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.InvalidCardNumberCode,
                                                CardWebAPIErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.InvalidPinCode,
                                                CardWebAPIErrorResource.CardNotFoundCode,
                                                CardWebAPIErrorResource.CardNotFoundMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.InvalidSubMarketCode,
                                                CardWebAPIErrorResource.GeneralNotFoundCode,
                                                CardWebAPIErrorResource.GeneralNotFoundMessage,
                                                HttpStatusCode.NotFound))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.MaximumBalanceExceededCode,
                                                CardWebAPIErrorResource.MaxBalanceExceededCode,
                                                CardWebAPIErrorResource.MaxBalanceExceededMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.NoRegistrationAddressOnFileCode,
                                                CardWebAPIErrorResource.NoRegistrationAddressOnFileCode,
                                                CardWebAPIErrorResource.NoRegistrationAddressOnFileMessage,
                                                HttpStatusCode.BadRequest))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.RequestNotPermittedByThisAccountCode,
                                                CardWebAPIErrorResource.CardIssuerForbiddenCode,
                                                CardWebAPIErrorResource.CardIssuerForbiddenMessage,
                                                HttpStatusCode.Forbidden))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.UnknownErrorCode,
                                                CardWebAPIErrorResource.InternalServerErrorCode,
                                                CardWebAPIErrorResource.InternalServerErrorMessage,
                                                HttpStatusCode.InternalServerError))
                       .Map(new ResourceMapItem(typeof(CardIssuerException),
                                                CardIssuerErrorResource.ValueLinkHostDownCode,
                                                CardWebAPIErrorResource.FirstDataServiceUnavailableCode,
                                                CardWebAPIErrorResource.FirstDataServiceUnavailableMessage,
                                                HttpStatusCode.BadGateway));

            #endregion
        }

        public CardController(ICardProvider cardProvider, IAccountProvider accountProvider, ViewModelStore viewModelStore, ILogRepository logRepository)
        {
            LoadResourceMap();
            _cardProvider = cardProvider;
            _accountProvider = accountProvider;
            _viewModelStore = viewModelStore;
            _logRepository = logRepository;

            if (_cardProvider == null)
            {
                throw new ArgumentNullException("cardProvider");
            }
            if (_accountProvider == null)
            {
                throw new ArgumentNullException("accountProvider");
            }
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.ActionName("CreateAutoReload")]
        [ApiOAuth2UserId]
        [CreateAutoReloadFilter]
        public HttpResponseMessage CreateAutoReload(string userId, string cardId, [TransformFromBody(typeof(ModelWithRiskTransform))] AutoReloadProfileWithRisk autoReloadProfile, [FromUri] string market = null)
        {
            IncrementLogCounter();
            try
            {
                var user = GetUser(userId);
                market = !string.IsNullOrWhiteSpace(market) ? market : user.SubMarket;

                var profile = _cardProvider.SetupAutoReload(userId, cardId,
                                                            autoReloadProfile.ToProvider(),
                                                            market, user.EmailAddress,
                                                            autoReloadProfile.RiskFields.ToCardProvider(market));

                HttpResponseMessage response = profile == null
                                                   ? Request.CreateResponse(HttpStatusCode.NotFound)
                                                   : Request.CreateResponse(HttpStatusCode.OK, profile.ToApi());

                if (response.IsSuccessStatusCode)
                {
                    string uri = Url.Route("CardByUserIdCardId",
                                           new Dictionary<string, object> { { "userId", "me" }, { "cardId", cardId } });
                    if (uri != null) response.Headers.Location = new Uri(uri, UriKind.Relative);
                }

                return response;
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                ProcessRequestDeniedExcepion(autoReloadProfile.RiskFields, ex);
                throw GetApiException(ex);
            }
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.ActionName("UpdateAutoReload")]
        [ApiOAuth2UserId]
        [UpdateAutoReloadFilter]
        public HttpResponseMessage UpdateAutoReload(string userId, string cardId, [TransformFromBody(typeof(ModelWithRiskTransform))] AutoReloadProfileWithRisk autoReloadProfile, [FromUri] string market = null)
        {
            IncrementLogCounter();
            IAutoReloadProfile profile;
            try
            {
                var user = GetUser(userId);
                market = !string.IsNullOrWhiteSpace(market) ? market : user.SubMarket;

                profile = _cardProvider.UpdateAutoReload(userId, cardId,
                                                         autoReloadProfile.ToProvider(),
                                                         market, user.EmailAddress,
                                                         autoReloadProfile.RiskFields.ToCardProvider(market));

            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                ProcessRequestDeniedExcepion(autoReloadProfile.RiskFields, ex);
                throw GetApiException(ex);
            }

            if (profile == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, CardWebAPIErrorResource.GeneralNotFoundCode,
                                       CardWebAPIErrorResource.GeneralNotFoundMessage);
            }
            return Request.CreateResponse(HttpStatusCode.OK, profile.ToApi());
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.ActionName("EnableAutoReload")]
        [ApiOAuth2UserId]
        public HttpResponseMessage EnableAutoReload(string userId, string cardId, [TransformFromBody(typeof(ModelWithRiskTransform))] EnableDisableReloadRisk enableDisableReloadRisk, [FromUri] string market = null)
        {
            IncrementLogCounter();
            try
            {
                var user = GetUser(userId);
                market = !string.IsNullOrWhiteSpace(market) ? market : user.SubMarket;

                _cardProvider.EnableAutoReload(userId, cardId, market,
                                                (enableDisableReloadRisk == null) ? Converters.ToCardProvider((Risk)null, market) : enableDisableReloadRisk.RiskFields.ToCardProvider(market));

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (NullReferenceException)
            {
                IncrementLogCounterError();
                throw new ApiException(HttpStatusCode.NotFound, string.Empty, string.Empty);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();

                if (enableDisableReloadRisk != null)
                {
                    ProcessRequestDeniedExcepion(enableDisableReloadRisk.RiskFields, ex);
                }

                throw GetApiException(ex);
            }
        }


        [System.Web.Http.HttpPut]
        [System.Web.Http.ActionName("DisableAutoReload")]
        [ApiOAuth2UserId]
        public HttpResponseMessage DisableAutoReload(string userId, string cardId, EnableDisableReloadRisk enableDisableReloadRisk, [FromUri] string market = null)
        {
            IncrementLogCounter();
            try
            {
                if (enableDisableReloadRisk != null)
                {
                    PlatformValidator.ValidatePlatformString(enableDisableReloadRisk.RiskFields);
                }

                var user = GetUser(userId);
                market = !string.IsNullOrWhiteSpace(market) ? market : user.SubMarket;

                _cardProvider.DisableAutoReload(userId, cardId, market,
                                                (enableDisableReloadRisk == null) ? Converters.ToCardProvider((Risk)null, market) : enableDisableReloadRisk.RiskFields.ToCardProvider(market));

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (NullReferenceException)
            {
                IncrementLogCounterError();
                throw new ApiException(HttpStatusCode.NotFound, string.Empty, string.Empty);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("CardByNumberAndPin")]
        [GetCardByNumberAndPinFilter]
        [FieldsAdaptor(typeof(StarbucksCard))]
        public HttpResponseMessage GetCardByNumberAndPin(string cardNumber, string pin, [FromUri] string market = null)
        {
            IncrementLogCounter();
            ICard card;
            try
            {
                card = _cardProvider.GetCardByNumberAndPin(cardNumber, pin, VisibilityLevel.Decrypted, market);
                //var parentModel = card.ToApi();
                //var viewModel = _viewModelStore.GetModel(Role, "StarbucksCard", parentModel);                                  

            }
            catch (CardProviderException cardProviderException)
            {
                IncrementLogCounterError();
                if (cardProviderException.Code == CardProviderErrorResource.CannotRegisterInvalidCardNumberCode
                    || cardProviderException.Code == CardProviderErrorResource.InvalidPinCode
                    || cardProviderException.Code == CardProviderErrorResource.InvalidCardNumberCode)
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                throw GetApiException(cardProviderException);
            }
            catch (CardIssuerException cardIssuerException)
            {
                IncrementLogCounterError();
                if (cardIssuerException.Code == CardIssuerErrorResource.CardNotFoundCode
                    || cardIssuerException.Code == CardIssuerErrorResource.InvalidPinCode
                    || cardIssuerException.Code == CardIssuerErrorResource.InvalidCardNumberCode)
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                throw GetApiException(cardIssuerException);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
            if (card == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, CardWebAPIErrorResource.GeneralNotFoundCode,
                                       CardWebAPIErrorResource.GeneralNotFoundMessage);
            }

            return Request.CreateResponse(HttpStatusCode.OK, card.ToApi());

        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("CardByUserIdCardId")]
        [ApiOAuth2UserId]
        [FieldsAdaptor(typeof(StarbucksCard))]
        public HttpResponseMessage GetCardById(string userId, string cardId, [FromUri] string market = null)
        {
            IncrementLogCounter();
            IAssociatedCard card;
            try
            {
                var userMarket = GetMarket(userId);
                market = market ?? userMarket;
                card = _cardProvider.GetCardById(userId, cardId, VisibilityLevel.Decrypted, true, market, userMarket);
                //var cardModel = card.ToApi();
                // var dyn = ConvertForRole(cardModel);               
            }
            catch (CardProviderException cardProviderException)
            {
                IncrementLogCounterError();
                if (cardProviderException.Code == CardProviderErrorResource.CannotRegisterInvalidCardNumberCode
                    || cardProviderException.Code == CardProviderErrorResource.InvalidPinCode
                    || cardProviderException.Code == CardProviderErrorResource.InvalidCardNumberCode)
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                throw GetApiException(cardProviderException);
            }
            catch (CardIssuerException cardIssuerException)
            {
                IncrementLogCounterError();
                if (cardIssuerException.Code == CardIssuerErrorResource.CardNotFoundCode
                    || cardIssuerException.Code == CardIssuerErrorResource.InvalidPinCode
                    || cardIssuerException.Code == CardIssuerErrorResource.InvalidCardNumberCode)
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                throw GetApiException(cardIssuerException);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
            if (card == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, CardWebAPIErrorResource.GeneralNotFoundCode,
                                       CardWebAPIErrorResource.GeneralNotFoundMessage);
            }

            return Request.CreateResponse(HttpStatusCode.OK, card.ToApi());
        }


        //private static dynamic ConvertForRole(StarbucksCard cardModel)
        //{
        //var fields = new List<string>() {"CardId, IsDigitalCard"};

        //dynamic dyn = new DynamicSerializable();
        //dynamic  source = cardModel;
        //foreach (var field in fields)
        //{
        //    dyn[field] = ;
        //    dyn[field] = source.field;
        //}
        //return dyn;
        //}

        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("CardPrimaryCardByUserId")]
        [ApiOAuth2UserId]
        [FieldsAdaptor(typeof(StarbucksCard))]
        public HttpResponseMessage CardPrimaryCardByUserId(string userId, [FromUri] string market = null)
        {
            IncrementLogCounter();
            IAssociatedCard card = null;
            try
            {
                var userMarket = GetMarket(userId);
                market = market ?? userMarket;
                card = _cardProvider.GetPrimaryCard(userId, VisibilityLevel.Decrypted, true, market, userMarket);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
            if (card == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, CardWebAPIErrorResource.GeneralNotFoundCode,
                                      CardWebAPIErrorResource.GeneralNotFoundMessage);
            }
            return Request.CreateResponse(HttpStatusCode.OK, card.ToApi());
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.ActionName("UpdateCard")]
        [ApiOAuth2UserId]
        [UpdateCardFilter]
        public HttpResponseMessage UpdateCard(string userId, string cardId, UpdateCardRequest updateCardRequest)
        {
            IncrementLogCounter();
            try
            {
                _cardProvider.UpdateCardNickname(userId, cardId, updateCardRequest.NickName);

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.ActionName("SetDefaultCard")]
        [ApiOAuth2UserId]
        public HttpResponseMessage UpdateDefaultCard(string userId, string cardId)
        {
            IncrementLogCounter();
            try
            {
                _cardProvider.UpdateDefaultCard(userId, cardId);

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("CardsByUserId")]
        [ApiOAuth2UserId]
        [FieldsAdaptor(typeof(StarbucksCard))]
        public HttpResponseMessage GetCardsByUserId(string userId, [FromUri] string market = null, [FromUri] bool includeAssociatedCards = false)
        {
            IncrementLogCounter();
            IEnumerable<IAssociatedCard> cards;
            try
            {
                var userMarket = GetMarket(userId);
                market = market ?? userMarket;
                cards = _cardProvider.GetCards(userId, VisibilityLevel.Decrypted, true, true, market, userMarket);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
            if (cards == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, CardWebAPIErrorResource.GeneralNotFoundCode,
                                      CardWebAPIErrorResource.GeneralNotFoundMessage);
            }
            return Request.CreateResponse(HttpStatusCode.OK, cards.Where(p => p.IsOwner || includeAssociatedCards).Select(c => c.ToApi()));
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("CardImageUrlByCardNumber")]
        [FieldsAdaptor(typeof(StarbucksCardImage))]
        public HttpResponseMessage GetCardImageUrlByCardNumber(string cardNumber)
        {
            IncrementLogCounter();

            IEnumerable<IStarbucksCardImage> imageUrls;
            try
            {
                imageUrls = _cardProvider.GetStarbucksCardImageUrlByCardNumber(cardNumber);

            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
            if (imageUrls == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, CardWebAPIErrorResource.GeneralNotFoundCode,
                                       CardWebAPIErrorResource.GeneralNotFoundMessage);
            }
            var starbucksCardImages = imageUrls as IList<IStarbucksCardImage> ?? imageUrls.ToList();

            return Request.CreateResponse(HttpStatusCode.OK, starbucksCardImages.Select(i => i.ToApi()));

        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("CardStatuses")]
        [ApiOAuth2UserId]
        public HttpResponseMessage GetStatusesForUserCards(string userId, [FromUri]IEnumerable<string> cardIds)
        {
            IncrementLogCounter();
            try
            {
                var statuses = _cardProvider.GetStatusesForUserCards(userId, cardIds);
                IStarbucksCardStatus[] starbucksCardStatuses = statuses as IStarbucksCardStatus[] ?? statuses.ToArray();
                if (statuses != null && starbucksCardStatuses.Any())
                    return Request.CreateResponse(HttpStatusCode.OK, starbucksCardStatuses.Select(s => s.ToApi()));
                return Request.CreateResponse(HttpStatusCode.OK, new List<IStarbucksCardStatus>());
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
        }


        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("GetCardBalanceByCardId")]
        [ApiOAuth2UserId]
        public HttpResponseMessage GetStarbucksCardBalance(string userId, string cardId, [FromUri] string market = null)
        {
            IncrementLogCounter();
            IStarbucksCardBalance cardBalance;
            try
            {
                var userMarket = GetMarket(userId);
                market = market ?? userMarket;
                cardBalance = _cardProvider.GetCardBalance(userId, cardId, market, userMarket);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
            if (cardBalance == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, CardWebAPIErrorResource.GeneralNotFoundCode,
                                     CardWebAPIErrorResource.GeneralNotFoundMessage);
            }
            return Request.CreateResponse(HttpStatusCode.OK, cardBalance.ToApi());
        }


        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("GetCardBalanceByCardNumber")]
        public HttpResponseMessage GetStarbucksCardBalanceByCardNumber(string cardNumber, string pin, [FromUri] string market = null)
        {
            IncrementLogCounter();
            IStarbucksCardBalance cardBalance = null;
            try
            {
                cardBalance = _cardProvider.GetCardBalanceByCardNumberPinNumber(cardNumber, pin, market);

            }
            catch (CardProviderException cardProviderException)
            {
                IncrementLogCounterError();
                if (cardProviderException.Code == CardProviderErrorResource.CardNotFoundCode
                    || cardProviderException.Code == CardProviderErrorResource.InvalidCardNumberCode
                    || cardProviderException.Code == CardProviderErrorResource.InvalidPinCode)
                {
                    throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.CardNotFoundCode,
                                           CardWebAPIErrorResource.CardNotFoundMessage);
                }
                throw GetApiException(cardProviderException);

            }
            catch (CardIssuerException cardIssuerException)
            {
                IncrementLogCounterError();
                if (cardIssuerException.Code == CardIssuerErrorResource.CardNotFoundCode
                    || cardIssuerException.Code == CardIssuerErrorResource.InvalidCardNumberCode
                    || cardIssuerException.Code == CardIssuerErrorResource.InvalidPinCode)
                {
                    throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.CardNotFoundCode,
                                           CardWebAPIErrorResource.CardNotFoundMessage);
                }
                throw GetApiException(cardIssuerException);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
            if (cardBalance == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, CardWebAPIErrorResource.GeneralNotFoundCode,
                                       CardWebAPIErrorResource.GeneralNotFoundMessage);
            }
            return Request.CreateResponse(HttpStatusCode.OK, cardBalance.ToApi());
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("GetCardBalanceByCardIdRealTime")]
        [ApiOAuth2UserId]
        public HttpResponseMessage GetStarbucksCardBalanceRealTime(string userId, string cardId, [FromUri] string market = null)
        {
            IncrementLogCounter();
            IStarbucksCardBalance cardBalance;
            try
            {
                var userMarket = GetMarket(userId);
                market = market ?? userMarket;
                cardBalance = _cardProvider.GetCardBalanceRealTime(userId, cardId, market, userMarket);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
            if (cardBalance == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, CardWebAPIErrorResource.GeneralNotFoundCode,
                                       CardWebAPIErrorResource.GeneralNotFoundMessage);
            }
            return Request.CreateResponse(HttpStatusCode.OK, cardBalance.ToApi());
        }


        [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("GetCardBalanceByCardNumberRealTime")]
        public HttpResponseMessage GetStarbucksCardBalanceByCardNumberRealTime(string cardNumber, string pin, [FromUri] string market = null)
        {
            IncrementLogCounter();
            IStarbucksCardBalance cardBalance;
            try
            {
                cardBalance =
                _cardProvider.GetCardBalanceByCardNumberPinNumberRealTime(cardNumber, pin, market);

            }
            catch (CardProviderException cardProviderException)
            {
                IncrementLogCounterError();
                if (cardProviderException.Code == CardProviderErrorResource.CardNotFoundCode
                    || cardProviderException.Code == CardProviderErrorResource.InvalidCardNumberCode
                    || cardProviderException.Code == CardProviderErrorResource.InvalidPinCode)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
                throw GetApiException(cardProviderException);

            }
            catch (CardIssuerException cardIssuerException)
            {
                IncrementLogCounterError();
                if (cardIssuerException.Code == CardIssuerErrorResource.CardNotFoundCode
                    || cardIssuerException.Code == CardIssuerErrorResource.InvalidCardNumberCode
                    || cardIssuerException.Code == CardIssuerErrorResource.InvalidPinCode)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
                throw GetApiException(cardIssuerException);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
            if (cardBalance == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, CardWebAPIErrorResource.GeneralNotFoundCode,
                                       CardWebAPIErrorResource.GeneralNotFoundMessage);
            }
            return Request.CreateResponse(HttpStatusCode.OK, cardBalance.ToApi());
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.ActionName("RegisterByCardNumberPin")]
        [ApiOAuth2UserId]
        [RegisterCardFilter]
        public HttpResponseMessage RegisterStarbucksCard(string userId, StarbucksCardNumberAndPin starbucksCardNumberAndPin)
        {
            IncrementLogCounter();
            ValidateRegistrationAddressExists(userId);

            IAssociatedCard card = null;

            try
            {
                if (starbucksCardNumberAndPin != null)
                {
                    PlatformValidator.ValidatePlatformString(starbucksCardNumberAndPin.risk);
                }

                var cardHeader = starbucksCardNumberAndPin.ToCardHeader();

                var market = GetMarket(userId);

                var subMarket = !string.IsNullOrEmpty(cardHeader.Submarket) ? cardHeader.Submarket : market;

                string cardId = _cardProvider.RegisterStarbucksCard(userId, cardHeader, market);
                card = _cardProvider.GetCardById(userId, cardId, VisibilityLevel.Decrypted, true, subMarket, market);
            }
            catch (CardProviderException cardProviderException)
            {
                IncrementLogCounterError();
                if (cardProviderException.Code == CardProviderErrorResource.CannotRegisterInvalidCardNumberCode)
                    throw new ApiException(HttpStatusCode.BadRequest,
                                           CardWebAPIErrorResource.CannotRegisterInvalidCardNumberCode,
                                           CardWebAPIErrorResource.CannotRegisterInvalidCardNumberMessage);
                if (cardProviderException.Code == CardProviderErrorResource.InvalidCardNumberCode)
                    throw new ApiException(HttpStatusCode.BadRequest,
                                           CardWebAPIErrorResource.CannotRegisterInvalidCardNumberCode,
                                           CardWebAPIErrorResource.CannotRegisterInvalidCardNumberMessage);
                if (cardProviderException.Code == CardProviderErrorResource.InvalidPinCode)
                    throw new ApiException(HttpStatusCode.BadRequest,
                                           CardWebAPIErrorResource.CannotRegisterInvalidCardNumberCode,
                                           CardWebAPIErrorResource.CannotRegisterInvalidCardNumberMessage);
                throw GetApiException(cardProviderException);
            }
            catch (CardIssuerException cie)
            {
                IncrementLogCounterError();
                if (cie.Code == CardIssuerErrorResource.InvalidPinCode)
                {
                    throw new ApiException(HttpStatusCode.BadRequest,
                                           CardWebAPIErrorResource.CannotRegisterInvalidCardNumberCode,
                                           CardWebAPIErrorResource.CannotRegisterInvalidCardNumberMessage);
                }
                if (cie.Code == CardIssuerErrorResource.InvalidCardNumberCode)
                {
                    throw new ApiException(HttpStatusCode.BadRequest,
                                           CardWebAPIErrorResource.CannotRegisterInvalidCardNumberCode,
                                           CardWebAPIErrorResource.CannotRegisterInvalidCardNumberMessage);
                }
                throw GetApiException(cie);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }

            if (card == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, CardWebAPIErrorResource.GeneralNotFoundCode,
                                       CardWebAPIErrorResource.GeneralNotFoundMessage);
            }

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, card.ToApi());

            string uri = Url.Route("CardByUserIdCardId",
                                   new Dictionary<string, object> { { "userId", "me" }, { "cardId", card.CardId } });
            if (uri != null) response.Headers.Location = new Uri(uri, UriKind.Relative);

            return response;
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.ActionName("AddCardAssociationByNumber")]
        [ApiOAuth2UserId]
        [AddAssociationCardFilter]
        public HttpResponseMessage AddCardAssociationByNumber(string userId, AssociateCardHeader associateCardHeader)
        {
            IncrementLogCounter();
            string cardId;
            IAssociatedCard card = null;
            try
            {
                var market = GetMarket(userId);
                var subMarket = !string.IsNullOrEmpty(associateCardHeader.Submarket) ? associateCardHeader.Submarket : market;

                var cardHeader = associateCardHeader.ToProvider(subMarket);
                cardId = _cardProvider.GetCardIdByCardNumber(cardHeader.CardNumber);
                _cardProvider.AddCardAssociation(userId, cardId, cardHeader, market);
                card = _cardProvider.GetCardById(userId, cardId, cardHeader.CardNumber, subMarket, market);
                if (card != null && string.IsNullOrEmpty(cardId))
                {
                    cardId = card.CardId;
                }
            }
            catch (CardProviderException cardProviderException)
            {
                IncrementLogCounterError();
                if (cardProviderException.Code == CardProviderErrorResource.CannotRegisterInvalidCardNumberCode)
                    throw new ApiException(HttpStatusCode.BadRequest,
                                           CardWebAPIErrorResource.CannotRegisterInvalidCardNumberCode,
                                           CardWebAPIErrorResource.CannotRegisterInvalidCardNumberMessage);
                if (cardProviderException.Code == CardProviderErrorResource.InvalidCardNumberCode)
                    throw new ApiException(HttpStatusCode.BadRequest,
                                           CardWebAPIErrorResource.CannotRegisterInvalidCardNumberCode,
                                           CardWebAPIErrorResource.CannotRegisterInvalidCardNumberMessage);
                if (cardProviderException.Code == CardProviderErrorResource.InvalidPinCode)
                    throw new ApiException(HttpStatusCode.BadRequest,
                                           CardWebAPIErrorResource.CannotRegisterInvalidCardNumberCode,
                                           CardWebAPIErrorResource.CannotRegisterInvalidCardNumberMessage);
                throw GetApiException(cardProviderException);
            }
            catch (CardIssuerException cie)
            {
                IncrementLogCounterError();
                if (cie.Code == CardIssuerErrorResource.InvalidPinCode)
                {
                    throw new ApiException(HttpStatusCode.BadRequest,
                                           CardWebAPIErrorResource.ValidationCardPinMissingCode,
                                           CardWebAPIErrorResource.ValidationCardPinMissingMessage);
                }
                if (cie.Code == CardIssuerErrorResource.InvalidCardNumberCode)
                {
                    throw new ApiException(HttpStatusCode.BadRequest,
                                           CardWebAPIErrorResource.ValidationCardNumberMissingCode,
                                           CardWebAPIErrorResource.ValidationCardNumberMissingMessage);
                }
                throw GetApiException(cie);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }

            if (string.IsNullOrEmpty(cardId))
            {
                throw new ApiException(HttpStatusCode.NotFound, CardWebAPIErrorResource.GeneralNotFoundCode,
                                       CardWebAPIErrorResource.GeneralNotFoundMessage);
            }

            if (card == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, CardWebAPIErrorResource.GeneralNotFoundCode,
                                       CardWebAPIErrorResource.GeneralNotFoundMessage);
            }

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, card.ToApi());


            string uri = Url.Route("CardByUserIdCardId",
                                   new Dictionary<string, object> { { "userId", "me" }, { "cardId", cardId } });
            if (uri != null) response.Headers.Location = new Uri(uri, UriKind.Relative);

            return response;
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.ActionName("RegisterByUserId")]
        [ApiOAuth2UserId]
        public HttpResponseMessage ActivateAndRegisterStarbucksCard(string userId, StarbucksCardHeader starbucksCard)
        {
            IncrementLogCounter();
            ValidateRegistrationAddressExists(userId);

            if (starbucksCard != null)
            {
                PlatformValidator.ValidatePlatformString(starbucksCard.risk);
            }

            if (starbucksCard == null)
            {
                starbucksCard = new StarbucksCardHeader();
            }
            IAssociatedCard card;
            try
            {
                var market = GetMarket(userId);
                var subMarket = !string.IsNullOrEmpty(starbucksCard.Submarket) ? starbucksCard.Submarket : market;

                string cardId = _cardProvider.ActivateAndRegisterCard(userId, market, starbucksCard.ToProvider(subMarket));
                card = _cardProvider.GetCardById(userId, cardId, VisibilityLevel.Decrypted, true, subMarket, market);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }

            if (card == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, CardWebAPIErrorResource.GeneralNotFoundCode,
                                       CardWebAPIErrorResource.GeneralNotFoundMessage);
            }

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, card.ToApi());

            string uri = Url.Route("CardByUserIdCardId",
                                   new Dictionary<string, object> { { "userId", "me" }, { "cardId", card.CardId } });
            if (uri != null) response.Headers.Location = new Uri(uri, UriKind.Relative);

            return response;
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.ActionName("RegisterMultipleCards")]
        [ApiOAuth2UserId]
        [RegisterMultipleStarbucksCardsFilter]
        public HttpResponseMessage RegisterMultipleStarbucksCards(string userId,
                                                                  IEnumerable<StarbucksCardNumberAndPin>
                                                                      starbucksCardNumberAndPins)
        {
            IncrementLogCounter();
            ValidateRegistrationAddressExists(userId);

            try
            {
                var market = GetMarket(userId);

                IEnumerable<IStarbucksCardHeader> providerCards =
                    starbucksCardNumberAndPins.Select(c => c.ToCardHeader());


                IEnumerable<ICardRegistrationStatus> cardRegistrationStatus =
                    _cardProvider.RegisterMultipleCards(userId, providerCards, market);

                var cardRegistrationStatuses = cardRegistrationStatus as IList<ICardRegistrationStatus> ??
                                               cardRegistrationStatus.ToList();

                foreach (var reg in cardRegistrationStatuses)
                {
                    if (!reg.Successful)
                    {
                        var t = reg.ExceptionType;
                        var error = ResourceMap.GetMappedResource(t, reg.Code);
                        if (error != null)
                        {
                            reg.Code = error.TargetCode;
                            reg.Message = error.TargetMessage;
                        }
                    }
                }

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK,
                                                                      cardRegistrationStatuses.Select(c => c.ToApi()));

                string uri = Url.Route("CardsByUserId", new Dictionary<string, object> { { "userId", "me" } });
                if (uri != null) response.Headers.Location = new Uri(uri, UriKind.Relative);

                return response;
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.ActionName("UnregisterCardByHttpPost")]
        [ApiOAuth2UserId]
        public HttpResponseMessage UnregisterStarbucksCardByHttpPost(string userId, string cardId, UnregisterCardRisk risk)
        {
            IncrementLogCounter();
            try
            {
                if (risk != null)
                {
                    PlatformValidator.ValidatePlatformString(risk.RiskFields);
                }

                var userMarket = GetMarket(userId);

                bool unregisterCardResult = _cardProvider.UnregisterCard(
                                                        userId,
                                                        cardId,
                                                        false,
                                                        userMarket,
                                                        (risk == null) ? Converters.ToCardProvider(null, userMarket) : risk.RiskFields.ToCardProvider(userMarket));
                return unregisterCardResult == true
                           ? Request.CreateResponse(HttpStatusCode.OK)
                           : Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
            catch (CardProviderException cpe)
            {
                IncrementLogCounterError();
                if (cpe.Code == CardProviderErrorResource.CardNotFoundCode)
                {
                    throw new ApiException(HttpStatusCode.NotFound, CardWebAPIErrorResource.GeneralNotFoundCode,
                                           CardWebAPIErrorResource.GeneralNotFoundMessage);
                }
                throw GetApiException(cpe);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
        }


        [System.Web.Http.HttpDelete]
        [System.Web.Http.ActionName("UnregisterCard")]
        [ApiOAuth2UserId]
        public HttpResponseMessage UnregisterStarbucksCard(string userId, string cardId)
        {
            IncrementLogCounter();
            try
            {
                var userMarket = GetMarket(userId);
                bool unregisterCardResult = _cardProvider.UnregisterCard(userId, cardId, false, userMarket);
                return unregisterCardResult == true
                           ? Request.CreateResponse(HttpStatusCode.OK)
                           : Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
            catch (CardProviderException cpe)
            {
                IncrementLogCounterError();
                if (cpe.Code == CardProviderErrorResource.CardNotFoundCode)
                {
                    throw new ApiException(HttpStatusCode.NotFound, CardWebAPIErrorResource.GeneralNotFoundCode,
                                           CardWebAPIErrorResource.GeneralNotFoundMessage);
                }
                throw GetApiException(cpe);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
        }



        //[System.Web.Http.HttpPut]
        //[System.Web.Http.ActionName("ReloadPaypal")]
        //[ApiOAuth2UserId]
        //public HttpResponseMessage ReloadStarbucksCardUsingPayPalRefTransaction(string userId, string cardId, ReloadForStarbucksCard reloadForStarbucksCard)
        //{
        //    try
        //    {
        //        var transaction = _cardProvider.ReloadStarbucksCard(userId, cardId, reloadForStarbucksCard.ToProvider() );
        //        return transaction == null
        //                   ? Request.CreateResponse(HttpStatusCode.NotFound)
        //                   : Request.CreateResponse(HttpStatusCode.OK, transaction.ToApi() );
        //    }
        //    catch (CardProviderException cardProviderException)
        //    {
        //        throw new ApiException(HttpStatusCode.InternalServerError, "500",
        //                               string.Format("Error retrieving card"), cardProviderException); //TODO: update error codes;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ApiException(HttpStatusCode.InternalServerError, "500",
        //                               string.Format("Error retrieving card"), ex); //TODO: update error codes;
        //    }
        //}




        [System.Web.Http.HttpPost]
        [System.Web.Http.ActionName("TransferBalance")]
        [TransferCardBalanceByStarbucksAccountFilter]
        [ApiOAuth2UserId]
        public HttpResponseMessage TransferStarbucksCardBalance(string userId, [TransformFromBody(typeof(ModelWithRiskTransform))] BalanceTransfer balanceTransfer)
        {
            IncrementLogCounter();
            IEnumerable<IStarbucksCardBalance> transferBalanceResult;
            try
            {
                var user = GetUser(userId);
                transferBalanceResult = _cardProvider.TransferRegistered(userId, balanceTransfer.SourceCardId,
                                                                         balanceTransfer.TargetCardId,
                                                                         balanceTransfer.Amount,
                                                                         user.SubMarket,
                                                                         user.EmailAddress,
                                                                         balanceTransfer.RiskFields.ToCardProvider(user.SubMarket));
            }
            catch (CardIssuerException cie)
            {
                IncrementLogCounterError();
                if (cie.Code == CardIssuerErrorResource.InvalidAmountMoreOrLessThanMinOrMaxAmountSpecifiedCode)
                {
                    throw new ApiException(HttpStatusCode.BadRequest,
                                           CardWebAPIErrorResource.InvalidAmountMoreOrLessThanMinOrMaxAmountSpecifiedCode,
                                           CardWebAPIErrorResource.InvalidAmountMoreOrLessThanMinOrMaxAmountSpecifiedMessage);
                }
                throw GetApiException(cie);
            }
            catch (CardProviderException cpe)
            {
                IncrementLogCounterError();
                if (cpe.Code == CardProviderErrorResource.CardNotFoundCode)
                {
                    throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.CardNotFoundCode,
                                           CardWebAPIErrorResource.CardNotFoundMessage);
                }

                if (cpe.Code == CardProviderErrorResource.CardNotRegisteredToUserCode)
                {
                    throw new ApiException(HttpStatusCode.Forbidden, CardWebAPIErrorResource.GeneralForbiddenCode,
                                           CardWebAPIErrorResource.GeneralForbiddenMessage);
                }

                ProcessRequestDeniedExcepion(balanceTransfer.RiskFields, cpe);

                throw GetApiException(cpe);
            }
            catch (Exception ex)
            {
                IncrementLogCounterError();
                throw GetApiException(ex);
            }
            if (transferBalanceResult == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, CardWebAPIErrorResource.GeneralNotFoundCode,
                                       CardWebAPIErrorResource.GeneralNotFoundMessage);
            }
            return Request.CreateResponse(HttpStatusCode.OK, transferBalanceResult.Select(p => p.ToApi()));
        }

        private IUser GetUser(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId) || userId.Equals(Guid.Empty.ToString()))
            {
                throw new ApiException(HttpStatusCode.BadRequest,
                                        CardWebAPIErrorResource.UserIdNotFoundOrIsInvalidCode,
                                        CardWebAPIErrorResource.UserIdNotFoundOrIsInvalidMessage);
            }

            var user = _accountProvider.GetUser(userId);
            if (user == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest,
                                        CardWebAPIErrorResource.UserIdNotFoundOrIsInvalidCode,
                                        CardWebAPIErrorResource.UserIdNotFoundOrIsInvalidMessage);
            }
            return user;
        }


        private string GetMarket(string userId)
        {
            var user = _accountProvider.GetUser(userId);
            if (user == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest,
                                        CardWebAPIErrorResource.UserIdNotFoundOrIsInvalidCode,
                                        CardWebAPIErrorResource.UserIdNotFoundOrIsInvalidMessage);
            }
            return user.SubMarket;
        }

        private void ValidateRegistrationAddressExists(string userId)
        {
            var addresses = _accountProvider.GetAddresses(userId);
            if (!addresses.Exists(p => p.AddressType == "Registration"))
                throw new ApiException(HttpStatusCode.BadRequest,
                                       CardWebAPIErrorResource.NoRegistrationAddressOnFileCode,
                                       CardWebAPIErrorResource.NoRegistrationAddressOnFileMessage);
        }

        private static void ProcessRequestDeniedExcepion(RiskWithoutIovation risk, Exception ex)
        {
            if (ex is CardProviderException)
            {
                CardProviderException cpe = ex as CardProviderException;
                if (cpe.Code == CardProviderErrorResource.RequestDeniedCode)
                {
                    if (risk == null) // if no risk container is provided, it means we are called from a legacy client, so don't return new error status
                    {
                        throw new ApiException(HttpStatusCode.InternalServerError, CardWebAPIErrorResource.RequestDeniedCode,
                                               CardWebAPIErrorResource.RequestDeniedMessage);
                    }
                    else
                    {
                        throw new ApiException(HttpStatusCode.Forbidden, CardWebAPIErrorResource.RequestDeniedCode,
                                               CardWebAPIErrorResource.RequestDeniedMessage);
                    }
                }
            }
        }
    }
}