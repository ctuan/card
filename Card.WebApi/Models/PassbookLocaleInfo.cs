﻿namespace Card.WebApi.Models
{
    public class PassbookLocaleInfo
    {
        public string Market { get; set; }
        public string Locale { get; set; }
    }
}