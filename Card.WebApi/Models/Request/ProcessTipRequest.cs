﻿using System.Runtime.Serialization;

namespace Card.WebApi.Models
{
    [DataContract(Name = "processTip")]
    public class ProcessTipRequest
    {
        [DataMember(Name =  "merchantId")]
        public string MerchantId { get; set; }
    }
}