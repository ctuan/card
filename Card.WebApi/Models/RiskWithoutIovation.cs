﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Card.WebApi.Models
{
    [DataContract(Name = "risk", Namespace = Constants.ContractNamespace)]
    public class RiskWithoutIovation
    {
        [DataMember(Name = "platform", Order = 1, IsRequired = true)]
        public string Platform { get; set; }

        [DataMember(Name = "market", Order = 2, IsRequired = false)]
        public string Market { get; set; }

        [DataMember(Name = "isLoggedIn", Order = 3, IsRequired = false)]
        public bool? IsLoggedIn { get; set; }

        [DataMember(Name = "ccAgentName", Order = 4, IsRequired = false)]
        public string CcAgentName { get; set; }
    }
}