﻿namespace Card.WebApi.Models
{
    public interface IModelWithRisk
    {
        Risk RiskFields { get; set; }
    }
}