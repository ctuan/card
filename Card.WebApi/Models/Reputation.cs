﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Card.WebApi.Models
{
    [DataContract(Name = "reputation", Namespace = Constants.ContractNamespace)]
    public class Reputation
    {
        [DataMember(Name = "IPAddress", Order = 0, IsRequired = false)]
        public string IpAddress { get; set; }

        [DataMember(Name = "deviceFingerprint", Order = 1, IsRequired = false)]
        public string DeviceFingerprint { get; set; }
    }
}