﻿using System;
namespace Card.WebApi.Models
{
    public class PassbookCardInfo
    {
        public string UserId { get; set; }
        public bool Active { get; set; }
        public string RegisteredUserId { get; set; }
        public decimal? Balance { get; set; }
        public DateTime? BalanceDate { get; set; }
        public string BalanceCurrency { get; set; }
        public int CardId { get; set; }
        public string SubMarketCode { get; set; }
        public string Currency { get; set; }
        public string NickName { get; set; }
        public string ImageSmall { get; set; }
        public string ImageLarge { get; set; }
        public string CardNumber { get; set; }
        public string EncryptedCardId { get; set; }
        public bool IsCardRegistered { get; set; }
        public string Name { get; set; }
    }
}