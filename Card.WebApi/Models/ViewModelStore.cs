﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac.Features.Metadata;

namespace Card.WebApi.Models
{
     
    public class ViewModelStore
    {

        private const string PublicRole = "public";
        private readonly IEnumerable<Meta<IViewModel, ViewModelMetaData>> _viewModels;

        public ViewModelStore(IEnumerable<Meta<IViewModel, ViewModelMetaData>> viewModels)
        {
            _viewModels = viewModels;
        }

        public IViewModel GetModel(string role, string className, StarbucksCard parentModel)
        {
            var t = _viewModels.SingleOrDefault(
                p =>
                p.Metadata.Role.Equals(role, StringComparison.InvariantCultureIgnoreCase) &&
                p.Value.GetType().Name == className) ?? _viewModels.SingleOrDefault(
                    p =>
                    p.Metadata.Role.Equals(PublicRole, StringComparison.InvariantCultureIgnoreCase) &&
                    p.Value.GetType().Name == className);

            if (t == null) return null;
            if (t.Value.GetType() == parentModel.GetType())
            {
                return parentModel;
            }
            t.Value.HydrateFrom(parentModel);
            return t.Value;
        }
    }
}