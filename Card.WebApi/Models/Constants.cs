﻿namespace Card.WebApi.Models
{
    public enum CardImageType
    {
        ImageIcon,
        ImageSmall,
        ImageMedium,
        ImageLarge,
        iosThumb,
        iosThumbHighRes,
        iosLarge,
        iosLargeHighRes,
        iosImageStripMedium,
        iosImageStripLarge,
        androidThumbMdpi,
        androidThumbHdpi,
        androidThumbXhdpi,
        androidThumbXxhdpi,
        androidFullMdpi,
        androidFullHdpi,
        androidFullXhdpi,
        androidFullXxhdpi,
        ImageStrip,
    }

    public class Constants
    {       
        public const string ContractNamespace = "https://openapi.starbucks.com/v1";
    }
}