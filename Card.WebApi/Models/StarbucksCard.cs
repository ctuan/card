﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Starbucks.Card.Provider.Common.Models;


namespace Card.WebApi.Models
{
    [KnownType(typeof (StarbucksCardImage))]
    [KnownType( typeof (AutoReloadProfile ))]
    [DataContract(Name = "StarbucksCard", Namespace = Constants.ContractNamespace)]
    public class StarbucksCard : IViewModel
    {        
        [DataMember(Name = "cardId", Order = 0)]
        public string CardId { get; set; }

        [DataMember(Name = "cardNumber", Order = 1)]
        public string CardNumber { get; set; }

        [DataMember(Name = "nickname", Order = 2)]
        public string Nickname { get; set; }

        [DataMember(Name = "class", Order = 3)]
        public string CardClass { get; set; }

        [DataMember(Name = "type", Order = 4)]
        public string CardType { get; set; }
       
        [DataMember(Name = "submarketCode", Order = 6)]
        public string SubmarketCode { get; set; }

        [DataMember(Name = "balance", Order = 7)]
        public decimal? Balance { get; set; }

        [DataMember(Name = "balanceDate", Order = 8)]
        public DateTime? BalanceDate { get; set; }

        [DataMember(Name = "balanceCurrencyCode", Order = 5)]
        public string BalanceCurrencyCode { get; set; }
       
        [DataMember(Name = "imageUrls", Order = 9)]
        public IEnumerable<StarbucksCardImage> CardImages { get; set; } 
     
        [DataMember(Name = "primary", Order = 12)]
        public bool? IsDefaultCard { get; set; }

        [DataMember(Name = "partner", Order = 13)]
        public bool? IsPartnerCard { get; set; }

        [DataMember(Name = "autoReloadProfile", Order = 14)]
        public AutoReloadProfile AutoReloadProfile { get; set; }

        [DataMember(Name = "digital", Order = 15)]
        public bool? IsDigitalCard { get; set; }              

        [DataMember(Name = "owner", Order = 17)]
        public bool IsOwner { get; set; }

        [DataMember(Name = "actions", Order = 18)]
        public IEnumerable<string> Actions { get; set; }

        [DataMember(Name = "cardCurrency")]
        public string CurrencyCode { get; set; }

        public void HydrateFrom<T>(T providerModel)
        {
            
        }
    }
}