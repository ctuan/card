﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Card.WebApi.Models
{
    public class Coordinates
    {
        [DataMember(Name = "latitude", IsRequired = true, Order = 1)]
        public string Latitude { get; set; }
        [DataMember(Name = "longitude", IsRequired = true, Order = 1)]
        public string Longitude { get; set; }
    }
}