﻿using System;
using System.Runtime.Serialization;

namespace Card.WebApi.Models
{
    [DataContract(Name = "StarbucksCardBalance", Namespace = Constants.ContractNamespace)]
    public class StarbucksCardBalance
    {
        [DataMember(Name = "cardId", Order = 0)]
        public string CardId { get; set; }

        [DataMember(Name = "balance", Order = 1)]
        public decimal? Balance { get; set; }

        [DataMember(Name = "balanceDate", Order = 2)]
        public DateTime? BalanceDate { get; set; }

        [DataMember(Name = "balanceCurrencyCode", Order = 3)]
        public string BalanceCurrencyCode { get; set; }

        [DataMember(Name = "cardNumber", Order = 4)]
        public string CardNumber { get; set; }
    }
}