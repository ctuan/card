﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Card.WebApi.Models
{   
    public interface  IViewModel
    {
        void HydrateFrom<T>(T parentViewModel);
    }
}