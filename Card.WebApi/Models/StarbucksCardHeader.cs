﻿using System.Runtime.Serialization;

namespace Card.WebApi.Models
{
    [DataContract(Name = "starbucksCardHeader", Namespace = Constants.ContractNamespace)]
    public class StarbucksCardHeader
    {
        [DataMember(Name = "cardNumber", IsRequired = false, Order = 0)]
        public string CardNumber { get; set; }

        [DataMember(Name = "pin", IsRequired = false, Order = 1)]
        public string Pin { get; set; }

        [DataMember(Name = "nickname", IsRequired = false, Order = 2)]
        public string Nickname { get; set; }

        [DataMember(Name = "submarket", IsRequired = false, Order = 3)]
        public string Submarket { get; set; }

        [DataMember(Name = "primary", IsRequired = false, Order = 4)]
        public bool Primary { get; set; }

        [DataMember(Name = "registrationSource", IsRequired = false, Order = 5)]
        public CardRegistrationSource RegistrationSource { get; set; }

        [DataMember(Name = "platform", Order = 6, IsRequired = false)]
        public string Platform { get; set; }

        [DataMember(Name = "marketing", Order = 7, IsRequired = false)]
        public string Market { get; set; }

        [DataMember(Name = "risk", Order = 8, IsRequired = false)]
        public RiskWithoutIovation risk { get; set; }
    }
}

