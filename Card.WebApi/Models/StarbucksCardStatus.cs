﻿using System.Runtime.Serialization;

namespace Card.WebApi.Models
{
    [DataContract(Name = "StarbucksCardStatus", Namespace = Constants.ContractNamespace)]
    public class StarbucksCardStatus
    {
        [DataMember(Name = "cardId", IsRequired = true, Order = 0)]
        public string CardId { get; set; }

        [DataMember(Name = "valid", IsRequired = true, Order = 1)]
        public bool Valid { get; set; }
    }
}