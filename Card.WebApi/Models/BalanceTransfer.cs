﻿using System.Runtime.Serialization;

namespace Card.WebApi.Models
{
    [DataContract(Name = "BalanceTransfer", Namespace = Constants.ContractNamespace)]
    public class BalanceTransfer : IModelWithRisk
    {
        [DataMember(Name = "sourceCardId", Order = 1, IsRequired = true)]
        public string SourceCardId { get; set; }

        [DataMember(Name = "targetCardId", Order = 2, IsRequired = true)]
        public string TargetCardId { get; set; }

        [DataMember(Name = "amount", Order = 3, IsRequired = false)]
        public decimal? Amount { get; set; }

        [DataMember(Name = "risk", Order = 4, IsRequired = false)]
        public Risk RiskFields { get; set; }
    }
}