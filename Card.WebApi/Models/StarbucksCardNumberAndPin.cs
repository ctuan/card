﻿using System.Runtime.Serialization;

namespace Card.WebApi.Models
{   /// TODO: the name is misleading and need to be changed to StarbucksCardHeader in the next iterations. But leaving it for now for backward compatiblity.
    [DataContract(Name = "StarbucksCardNumberAndPin", Namespace = Constants.ContractNamespace)]
    public class StarbucksCardNumberAndPin
    {
        [DataMember(Name = "cardNumber", IsRequired = true, Order = 0)]
        public string CardNumber { get; set; }

        [DataMember(Name = "pin", IsRequired = true, Order = 1)]
        public string Pin { get; set; }

        [DataMember(Name = "nickname", IsRequired = false, Order = 2)]
        public string Nickname { get; set; }

        [DataMember(Name = "submarket", IsRequired = false, Order = 3)]
        public string Submarket { get; set; }

        [DataMember(Name = "primary", IsRequired = false, Order = 4)]
        public bool Primary { get; set; }

        [DataMember(Name = "register", IsRequired = false, Order = 5)]
        public bool Register { get; set; }

        [DataMember(Name = "registrationSource", IsRequired = false, Order = 6)]
        public CardRegistrationSource RegistrationSource { get; set; }

        [DataMember(Name = "risk", IsRequired = false, Order = 7)]
        public RiskWithoutIovation risk { get; set; }
    }
}