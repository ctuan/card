﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Card.WebApi.Models
{
    public class Store
    {
        [DataMember(Name = "name", IsRequired = true, Order = 1)]
        public string Name { get; set; }
        [DataMember(Name = "coordinates", IsRequired = true, Order = 2)]
        public Coordinates Coordinates { get; set; }
    }
}