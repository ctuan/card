﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Card.WebApi.Models
{
    [DataContract(Name = "associateCardHeader", Namespace = Constants.ContractNamespace)]
    public class AssociateCardHeader
    {
        [DataMember(Name = "cardNumber", IsRequired = false, Order = 0)]
        public string CardNumber { get; set; }

        [DataMember(Name = "nickname", IsRequired = false, Order = 1)]
        public string Nickname { get; set; }

        [DataMember(Name = "submarket", IsRequired = false, Order = 2)]
        public string Submarket { get; set; }

        [DataMember(Name = "registrationSource", IsRequired = false, Order = 4)]
        public CardRegistrationSource RegistrationSource { get; set; }

        [DataMember(Name = "risk", Order = 5, IsRequired = false)]
        public RiskWithoutIovation Risk { get; set; }
    }
}