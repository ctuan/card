﻿using System.Runtime.Serialization;

namespace Card.WebApi.Models
{
    [DataContract(Name = "mobilePOSdetails", Namespace = Constants.ContractNamespace)]
    public class MobilePOS
    {
        [DataMember(Name = "transactionReferenceKey", Order = 1, IsRequired = true)]
        public string TransactionReferenceKey { get; set; }

        [DataMember(Name = "trackData", Order = 3, IsRequired = true)]
        public string TrackData { get; set; }

        [DataMember(Name = "combinedTags", Order = 5, IsRequired = true)]
        public string CombinedTags { get; set; }

        [DataMember(Name = "terminalId", Order = 7, IsRequired = true)]
        public string TerminalId { get; set; }
    }
}