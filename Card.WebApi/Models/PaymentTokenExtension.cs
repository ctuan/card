﻿using System.Runtime.Serialization;

namespace Card.WebApi.Models
{
    [DataContract(Name = "paymentTokenExtension", Namespace = Constants.ContractNamespace)]
    public class PaymentTokenExtension
    {
        [DataMember(Name = "paymentCryptogram", Order = 1, IsRequired = true)]
        public string PaymentCryptogram { get; set; }

        [DataMember(Name = "expirationMonth", Order = 2, IsRequired = false)]
        public int? ExpirationMonth { get; set; }

        [DataMember(Name = "expirationYear", Order = 3, IsRequired = false)]
        public int? ExpirationYear { get; set; }

        [DataMember(Name = "ecIndicator", Order = 4, IsRequired = false)]
        public string ECIndicator { get; set; }

        [DataMember(Name = "tokenRequestorId", Order = 5, IsRequired = true)]
        public string TokenRequestorId { get; set; }

        [DataMember(Name = "mobilePOSdetails", Order = 7, IsRequired = false)]
        public MobilePOS MobilePOS { get; set; }

    }
}