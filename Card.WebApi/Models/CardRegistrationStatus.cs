﻿using System.Runtime.Serialization;

namespace Card.WebApi.Models
{
    [DataContract(Name = "CardRegistrationStatus", Namespace = Constants.ContractNamespace)]
    public class CardRegistrationStatus
    {
        [DataMember(Name = "cardId", IsRequired = true, Order = 0)]
        public string CardId { get; set; }

        [DataMember(Name = "cardNumber", IsRequired = true, Order = 1)]
        public string CardNumber { get; set; }

        [DataMember(Name = "success", IsRequired = true, Order = 2)]
        public bool Successful { get; set; }

        [DataMember(Name = "code", IsRequired = false, Order = 3)]
        public string Code { get; set; }

        [DataMember(Name = "message", IsRequired = false, Order = 3)]
        public string Message { get; set; }
    }
}