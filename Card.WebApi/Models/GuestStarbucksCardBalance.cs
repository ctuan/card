﻿using System;
using System.Runtime.Serialization;

namespace Card.WebApi.Models
{
    [DataContract(Name = "GuestStarbucksCardBalance", Namespace = Constants.ContractNamespace)]
    public class GuestStarbucksCardBalance
    {
        [DataMember(Name = "cardNumber", Order = 0)]
        public string CardNumber { get; set; }

        [DataMember(Name = "balance", Order = 1)]
        public decimal? Balance { get; set; }

        [DataMember(Name = "balanceDate", Order = 2)]
        public DateTime? BalanceDate { get; set; }

        [DataMember(Name = "balanceCurrencyCode", Order = 3)]
        public string BalanceCurrencyCode { get; set; }
    }
}