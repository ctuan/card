﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Card.WebApi.Models
{
    [DataContract(Name = "SelectedCardsRequest", Namespace = Constants.ContractNamespace)]
    public class SelectedCardsRequest
    {
        [DataMember(Name = "cardIds")]
        public IEnumerable<string> Cards { get; set; }
    }
}