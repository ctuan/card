﻿using System.Runtime.Serialization;

namespace Card.WebApi.Models
{
    [DataContract(Name = "SetupAutoReloadProfile", Namespace = Constants.ContractNamespace)]
    public class SetupAutoReloadProfile
    {
        //[DataMember(Name = "userId", Order = 1)]
        //public string UserId { get; set; }

        [DataMember(Name = "cardId", Order = 2)]
        public string CardId { get; set; }

        [DataMember(Name = "autoReloadType", Order = 3)]
        public string AutoReloadType { get; set; }

        [DataMember(Name = "day", Order = 5)]
        public int? Day { get; set; }

        [DataMember(Name = "triggerAmount", Order = 6)]
        public decimal? TriggerAmount { get; set; }

        [DataMember(Name = "amount", Order = 7)]
        public decimal Amount { get; set; }

        [DataMember(Name = "paymentType", Order = 8, IsRequired = true)]
        public string PaymentType { get; set; }

        [DataMember(Name = "paymentMethodId", Order = 9)]
        public string PaymentMethodId { get; set; }

        [DataMember(Name = "payPalECToken", Order = 10)]
        public string PayPalEcToken { get; set; }
    }
}