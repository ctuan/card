﻿using Account.Provider.Common.Models;
using Card.WebApi.ProviderModels.Tipping;
using Starbucks.CardTransaction.Provider.Common.Model;
using Starbucks.TransactionHistory.Common.Models.Response;

namespace Card.WebApi.Models
{
    public static class Conversion
    {
        public static IProcessTip ToProcessTip(this IHistoryItem history, IUser user, long validHistoryId, ProcessTipRequest processTipRequest)
        {
            var tip = new ProcessTip
            {
                Amount = history.TransactionDetails.TipInfo.Amount.GetValueOrDefault(),
                CardId = history.CardId,
                LocalTransactionId = history.TransactionDetails.LocalTransactionId,
                LocalTransactionTime = history.TransactionDetails.LocalTime,
                UserId = history.UserId,
                OrginalMerchantId = history.TransactionDetails.MerchantId,
                UserMarket = user.SubMarket,
                OriginalTransactionCurrency = history.TransactionDetails.Currency,
                OriginalTransactionId = validHistoryId,
                OriginalStoreId = history.TransactionDetails.StoreId,
                OriginalTerminalId = history.TransactionDetails.TerminalId
            };
            if (processTipRequest != null && !string.IsNullOrWhiteSpace(processTipRequest.MerchantId))
            {
                tip.MerchantId = processTipRequest.MerchantId;
            }
            return tip;
        }
    }
}