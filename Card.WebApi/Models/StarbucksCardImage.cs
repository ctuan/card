﻿using System;
using System.Runtime.Serialization;

namespace Card.WebApi.Models
{
    [DataContract(Name = "StarbucksCardImage", Namespace = Constants.ContractNamespace)]
    public class StarbucksCardImage
    {
        [IgnoreDataMember  ]
        public CardImageType Type { get; set; }
      
        [DataMember(Name = "imageType", Order = 0)]
        public string ImageType 
        {
            get
            {
                return Enum.GetName(Type.GetType() , Type);
            } 
            set
            {
                CardImageType t;
                Enum.TryParse(value, true, out t);
                Type = t;
            }
           
        }

        [DataMember(Name = "uri", Order = 1)]
        public string Uri { get; set; }
    }
}