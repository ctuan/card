﻿namespace Card.WebApi.Models
{
    public class PassbookFavoriteLocation
    {
        public string InternalId { get; set; }
        public string RelevantText { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }
}