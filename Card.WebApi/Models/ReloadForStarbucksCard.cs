﻿using System.Runtime.Serialization;

namespace Card.WebApi.Models
{
    [DataContract(Name = "ReloadForStarbucksCard", Namespace = Constants.ContractNamespace)]
    public class ReloadForStarbucksCard : IModelWithRisk
    {
        [DataMember(Name = "amount", Order = 1, IsRequired = true)]
        public decimal Amount { get; set; }

        [DataMember(Name = "paymentMethodId", Order = 2, IsRequired = true)]
        public string PaymentMethodId { get; set; }

        [DataMember(Name = "sessionId", Order=3, IsRequired = false)]
        public string SessionId { get; set; }

        [DataMember(Name = "emailAddress", Order = 5, IsRequired = false)]
        public string EmailAddress { get; set; }

        [DataMember(Name = "ipAddress", Order = 6, IsRequired = false)]
        public string IpAddress { get; set; }

        [DataMember(Name = "platform", Order = 7, IsRequired = true)]
        public string Platform { get; set; }

        [DataMember(Name = "purchaserSessionDuration", Order = 8, IsRequired = false)]
        public decimal PurchaserSessionDuration { get; set; }

        [DataMember(Name = "paymentToken", Order = 9, IsRequired = false)]
        public string PaymentToken { get; set; }

        [DataMember(Name = "PaymentTokenType", Order = 10, IsRequired = false)]
        public string PaymentTokenType { get; set; }        

        [DataMember(Name = "paymentAddress", Order = 11, IsRequired = false)]
        public Address BillingAddress { get; set; }

        [DataMember(Name = "risk", Order = 12, IsRequired = false)]
        public Risk RiskFields { get; set; }

        [DataMember(Name = "paymentTokenExtension", Order = 13, IsRequired = false)]
        public PaymentTokenExtension PaymentTokenExtension { get; set; }
    }
}