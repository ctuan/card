﻿using System;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;
using System.Globalization;
using System.Net;
using System.Runtime.Serialization;

namespace Card.WebApi.Models.Tipping
{
    [DataContract]
    public class UpsertTipRequest
    {
        [IgnoreDataMember]
        public decimal Amount
        {
            get
            {
                decimal amount;
                if (TryParseAmount(StringAmount, out amount))
                {
                    return Math.Round(amount, 2, MidpointRounding.ToEven);
                }

                throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.TipIsNotANumberCode, CardWebAPIErrorResource.TipIsNotANumberMessage);
            }
        }

        [DataMember(Name = "amount")]
        public string StringAmount { get; set; }

        public virtual bool TryParseAmount(string input, out decimal output)
        {
            return decimal.TryParse(input, NumberStyles.Currency & ~NumberStyles.AllowCurrencySymbol & ~NumberStyles.AllowThousands, NumberFormatInfo.InvariantInfo, out output);
        }
    }
}