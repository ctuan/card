﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Starbucks.Card.Provider.Common.Models;



namespace Card.WebApi.Models.Public
{
    [KnownType(typeof(StarbucksCardImage))]
    [DataContract(Name = "StarbucksCard", Namespace = Constants.ContractNamespace)]
    public class StarbucksCard : IViewModel
    {
        private Card.WebApi.Models.StarbucksCard _starbucksCard;

        [DataMember(Name = "nickname", Order = 2)]
        public string Nickname { get { return _starbucksCard.Nickname; } }

        [DataMember(Name = "class", Order = 3)]
        public string CardClass { get { return _starbucksCard.CardClass; } }

        [DataMember(Name = "type", Order = 4)]
        public string CardType { get{ return _starbucksCard.CardType; } }

        [DataMember(Name = "submarketCode", Order = 6)]
        public string SubmarketCode { get{ return _starbucksCard.SubmarketCode; } }

        [DataMember(Name = "balance", Order = 7)]
        public decimal? Balance { get{ return _starbucksCard.Balance; }}

        [DataMember(Name = "balanceDate", Order = 8)]
        public DateTime? BalanceDate { get{ return _starbucksCard.BalanceDate; } }

        [DataMember(Name = "balanceCurrencyCode", Order = 5)]
        public string BalanceCurrencyCode { get{ return _starbucksCard.BalanceCurrencyCode; }}
 
        [DataMember(Name = "actions", Order = 18)]
        public IEnumerable<string> Actions { get{ return _starbucksCard.Actions; } }     

        public void HydrateFrom<T>(T providerModel)
        {
            _starbucksCard = providerModel as Card.WebApi.Models.StarbucksCard;
        }
    }
}