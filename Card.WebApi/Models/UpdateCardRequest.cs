﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Card.WebApi.Models
{
    [DataContract(Name = "UpdateCardRequest", Namespace = Constants.ContractNamespace)]
    public class UpdateCardRequest
    {
        [DataMember(Name = "nickname")]
        public string NickName { get; set; }

    }
}