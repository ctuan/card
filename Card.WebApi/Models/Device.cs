﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Card.WebApi.Models
{
    [DataContract(Name = "device", Namespace = Constants.ContractNamespace)]
    public class Device
    {
         [DataMember(Name = "deviceId", Order = 1, IsRequired = true)]
        public string DeviceId { get; set; }
    }
}