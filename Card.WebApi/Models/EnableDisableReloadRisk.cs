﻿using System;
using System.Runtime.Serialization;

namespace Card.WebApi.Models
{
    [DataContract(Name = "EnableDisableReloadRisk", Namespace = Constants.ContractNamespace)]
    public class EnableDisableReloadRisk : IModelWithRisk
    {
        [DataMember(Name = "risk", Order = 1, IsRequired = false)]
        public Risk RiskFields { get; set; }
    }
}