﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Card.WebApi.Models
{
    [DataContract(Name = "risk", Namespace = Constants.ContractNamespace)]
    public class Risk : RiskWithoutIovation
    {
        [DataMember(Name = "reputation", Order = 5, IsRequired = false)]
        public Reputation Reputation { get; set; }
    }
}