﻿using System.Runtime.Serialization;

namespace Card.WebApi.Models
{
    [DataContract(Name = "UnregisterCardResult", Namespace = Constants.ContractNamespace)]
    public class StarbucksCardUnregisteredResult
    {
        [DataMember(Name = "cardId", Order = 0)]
        public string CardId { get; set; }
    }
}