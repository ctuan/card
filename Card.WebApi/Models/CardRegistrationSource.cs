﻿using System.Runtime.Serialization;

namespace Card.WebApi.Models
{
    [DataContract(Name = "CardRegistrationSource", Namespace = Constants.ContractNamespace)]
    public class CardRegistrationSource
    {
        [DataMember(Name = "platform", IsRequired = false, Order = 0)]
        public string Platform { get; set; }

        [DataMember(Name = "marketing", IsRequired = false, Order = 1)]
        public string Marketing { get; set; }
    }
}