﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Card.WebApi.Models
{
    [DataContract(Name = "address", Namespace = Constants.ContractNamespace)]
    public class Address
    {
        [DataMember(Name = "firstName", Order = 1, IsRequired = false)]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName", Order = 2, IsRequired = false)]
        public string LastName { get; set; }

        [DataMember(Name = "phoneNumber", Order = 3, IsRequired = false)]
        public string PhoneNumber { get; set; }

        [DataMember(Name = "addressLine1", Order = 4, IsRequired = false)]
        public string AddressLine1 { get; set; }

        [DataMember(Name = "addressLine2", Order = 5, IsRequired = false)]
        public string AddressLine2 { get; set; }

        [DataMember(Name = "city", Order = 6, IsRequired = false)]
        public string City { get; set; }

        [DataMember(Name = "countrySubdivision", Order = 7, IsRequired = false)]
        public string CountrySubdivision { get; set; }

        [DataMember(Name = "country", Order = 8, IsRequired = false)]
        public string Country { get; set; }

        [DataMember(Name = "postalCode", Order = 9, IsRequired = false)]
        public string PostalCode { get; set; }
    }
}