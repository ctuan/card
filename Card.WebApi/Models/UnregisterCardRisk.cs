﻿using System;
using System.Runtime.Serialization;

namespace Card.WebApi.Models
{

    [DataContract(Name = "UnregisterCardRisk", Namespace = Constants.ContractNamespace)]
    public class UnregisterCardRisk
    {
        [DataMember(Name = "risk", Order = 1, IsRequired = false)]
        public RiskWithoutIovation RiskFields { get; set; }
    }
}