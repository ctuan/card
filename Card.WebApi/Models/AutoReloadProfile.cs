﻿using System;
using System.Runtime.Serialization;

namespace Card.WebApi.Models
{
    [DataContract(Name = "AutoReloadProfileWithRisk", Namespace = Constants.ContractNamespace)]
    public class AutoReloadProfileWithRisk : AutoReloadProfile, IModelWithRisk
    {
        [DataMember(Name = "risk", Order = 1, IsRequired = false)]
        public Risk RiskFields { get; set; }
    }

    [DataContract(Name = "AutoReloadProfile", Namespace = Constants.ContractNamespace )]
    public class AutoReloadProfile
    {
        [DataMember(Name = "cardId", Order = 2)]
        public string CardId { get; set; }

        [DataMember(Name = "autoReloadType", Order = 3)]
        public string AutoReloadType { get; set; }

        [DataMember(Name = "day", Order = 5)]
        public int? Day { get; set; }

        [DataMember(Name = "triggerAmount", Order = 6)]
        public decimal? TriggerAmount { get; set; }

        [DataMember(Name = "amount", Order = 7)]
        public decimal Amount { get; set; }

        //[DataMember(Name = "paymentType", Order = 8, IsRequired = true)]
        //public string PaymentType { get; set; }

        [DataMember(Name = "paymentMethodId", Order = 9)]
        public string PaymentMethodId { get; set; }

        [DataMember(Name = "autoReloadId", Order = 0)]
        public string AutoReloadId { get; set; }

        [DataMember(Name = "status", Order = 4)]
        public string Status { get; set; }

        [DataMember(Name = "disableUntilDate", Order = 11)]
        public DateTime? DisableUntilDate { get; set; }

        [DataMember(Name = "stoppedDate", Order = 12)]
        public DateTime? StoppedDate { get; set; }
    }
}