﻿using System;
using Starbucks.CardTransaction.Provider.Common.Model;

namespace Card.WebApi.ProviderModels.Tipping
{
    public class ProcessTip : IProcessTip
    {
        public string UserId { get; set; }
        public string CardId { get; set; }
        public decimal Amount { get; set; }
        public DateTime LocalTransactionTime { get; set; }
        public string LocalTransactionId { get; set; }
        public string OrginalMerchantId { get; set; }
        public long OriginalTransactionId { get; set; }
        public string UserMarket { get; set; }
        public string OriginalTransactionCurrency { get; set; }
        public string MerchantId { get; set; }


        public string OriginalStoreId { get; set; }

        public string OriginalTerminalId { get; set; }
    }
}