﻿using Starbucks.Card.Provider.Common.Models;

namespace Card.WebApi.ProviderModels
{
    public class StarbucksCardNumberAndPin : IStarbucksCardNumberAndPin
    {
        public string CardNumber { get; set; }
        public string CardPin { get; set; }
    }
}
