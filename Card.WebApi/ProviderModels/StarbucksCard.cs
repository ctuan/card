﻿using System;
using System.Collections.Generic;
using Starbucks.Card.Provider.Common.Models;

namespace Card.WebApi.ProviderModels
{
    public class StarbucksCard : IStarbucksCard
    {
        public string CardId { get; set; }
        public string CardNumber { get; set; }
        public string Nickname { get; set; }
        public string CardClass { get; set; }
        public string CardType { get; set; }
        public string CurrencyCode { get; set; }
        public string SubmarketCode { get; set; }
        public decimal? Balance { get; set; }
        public DateTime? BalanceDate { get; set; }
        public string BalanceCurrencyCode { get; set; }
        //public string ImageIconUrl { get; set; }
        //public string ImageUrl { get; set; }
        public IEnumerable<IStarbucksCardImage> CardImages { get; set; } 
        public string RegisteredUserId { get; set; }
        public bool? IsDefaultCard { get; set; }
        public bool? IsPartnerCard { get; set; }
        public IAutoReloadProfile AutoReloadProfile { get; set; }
        public bool? IsDigitalCard { get; set; }
        public IEnumerable<string> PassbookSerialNumbers { get; set; }
        public string AutoReloadId { get; set; }
        public DateTime? RegistrationDate { get; set; }
    }
}
