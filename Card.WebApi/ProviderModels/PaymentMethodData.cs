﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Starbucks.Card.Provider.Common.Models;

namespace Card.WebApi.ProviderModels
{
    public class PaymentMethodData : IPaymentMethodData
    {
        public string Cvn { get; set; }
        public string PaymentMethodType { get; set; }
        public string PaymentMethodId { get; set; }
        public string BillingAddressId { get; set; }
    }
}