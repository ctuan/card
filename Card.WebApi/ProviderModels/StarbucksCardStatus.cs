﻿using Starbucks.Card.Provider.Common.Models;


namespace Card.WebApi.ProviderModels
{
    public class StarbucksCardStatus : IStarbucksCardStatus
    {
        public string CardId { get; set; }
        public bool Valid { get; set; }
    }
}
