﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Starbucks.CardTransaction.Provider.Common.Model;

namespace Card.WebApi.ProviderModels
{
    public class Address:IAddress
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string CountrySubdivision { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
    }
}