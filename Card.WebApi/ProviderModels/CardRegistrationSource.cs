﻿using Starbucks.Card.Provider.Common.Models;

namespace Card.WebApi.ProviderModels
{

    public class CardRegistrationSource
    {   
        public string Marketing { get; set; }
        public string Platform { get; set; }
    }
}