﻿using Starbucks.Card.Provider.Common.Models;

namespace Card.WebApi.ProviderModels
{
    public class ReloadForStarbucksCardGuest : IReloadForStarbucksCardGuest
    {
        public decimal Amount { get; set; }
        public string PaymentMethodId { get; set; }
        public string SessionId { get; set; }        
    }
}
