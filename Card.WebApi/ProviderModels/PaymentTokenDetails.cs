﻿using Starbucks.CardTransaction.Provider.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Card.WebApi.ProviderModels
{
    public class PaymentTokenDetails : Starbucks.CardTransaction.Provider.Common.Model.IPaymentTokenDetails
    {
        public Starbucks.CardTransaction.Provider.Common.Model.PaymentTokenType PaymentTokenType { get; set; }

        public string PaymentToken { get; set; }


        public string PaymentCryptogram { get; set; }


        public int? ExpirationMonth { get; set; }


        public int? ExpirationYear { get; set; }


        public string ECIndicator { get; set; }


        public string TokenRequestorId { get; set; }

        public IMobilePOS MobilePOS { get; set; }
    }
}