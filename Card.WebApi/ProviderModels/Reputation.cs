﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Starbucks.CardTransaction.Provider.Common.Model;

namespace Card.WebApi.ProviderModels
{
    public class Reputation : IReputation
    {
        public string Platform { get; set; }
        public string Market { get; set; }
        public bool IsLoggedIn { get; set; }
        public string IpAddress { get; set; }
        public string DeviceFingerprint { get; set; }
    }
}