﻿using System;
using Starbucks.Card.Provider.Common.Models;

namespace Card.WebApi.ProviderModels
{
    public class StarbucksCardBalance : IStarbucksCardBalance
    {
        public string CardId { get; set; }
        public decimal? Balance { get; set; }
        public DateTime? BalanceDate { get; set; }
        public string BalanceCurrencyCode { get; set; }
        public string CardNumber { get; set; }
    }
}
