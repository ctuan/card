﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Card.WebApi.ProviderModels
{
    public class MobilePOS : Starbucks.CardTransaction.Provider.Common.Model.IMobilePOS
                                 {
        public string TransactionReferenceKey { get; set; }

        public string TrackData { get; set; }

        public string CombinedTags { get; set; }

        public string TerminalId { get; set; }
    }
}