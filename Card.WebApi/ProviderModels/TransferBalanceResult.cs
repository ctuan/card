﻿using System.Collections.Generic;
using Starbucks.Card.Provider.Common.Models;

namespace Card.WebApi.ProviderModels
{
    public class TransferBalanceResult : ITransferBalanceResult
    {
        public IStarbucksCardTransaction StarbucksCardTransaction { get; set; }
        public IDictionary<string, decimal> Balances { get; set; }
    }
}