﻿using Starbucks.Card.Provider.Common.Models;


namespace Card.WebApi.ProviderModels
{
    public class StarbucksCardUnregisterResult : IStarbucksCardUnregisterResult
    {
        public string CardId { get; set; }
    }
}
