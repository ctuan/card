﻿using Starbucks.CardTransaction.Provider.Common.Model;


namespace Card.WebApi.ProviderModels
{
    public class ReloadForStarbucksCard : IReloadForStarbucksCard
    {
        public string Type { get; set; }
        public string IpAddress { get; set; }
        public string Platform { get; set; }
        public decimal PurchaserSessionDuration { get; set; }
        public IAddress BillingAddress { get; set; }
        public PaymentTokenType? PaymentTokenType { get; set; }
        public string PaymentToken { get; set; }
        public string PaymentTokenAddressId { get; set; }
        public decimal Amount { get; set; }
        public string SessionId { get; set; }
        public string PaymentMethodId { get; set; }
        public IRisk Risk { get; set; }
        public IPaymentTokenDetails PaymentTokenDetails { get; set; }
    }
}
