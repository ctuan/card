﻿using Starbucks.Card.Provider.Common.Models;

namespace Card.WebApi.ProviderModels
{
    public class ReloadForStarbucksCardWithGuestPayPal : IReloadForStarbucksCardGuestWithPayPal

    {
        public decimal Amount { get; set; }
        public string PaymentMethodId { get; set; }
        public string SessionId { get; set; }        
        public string EmailAddress { get; set; }
    }
}
