﻿namespace Card.WebApi.Authorization
{
    public class ProtectedDataStoreAuthorizationProvider : AuthorizationProvider
    {
        private readonly IProtectedDataStore _protectedDataStore;

        public ProtectedDataStoreAuthorizationProvider(IAuthorizationDataProvider authorizationDataProvider, IProtectedDataStore protectedDataStore) : base(authorizationDataProvider)
        {
            _protectedDataStore = protectedDataStore;
        }

        protected override string TransformAuthorization(string authorization)
        {
            return _protectedDataStore.GetUnprotectedDataAsString(authorization);
        }
    }
}