﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Controllers;

namespace Card.WebApi.Authorization
{
    public interface IAuthorizationDataProvider
    {
        IEnumerable<string> GetAuthorizations(HttpActionContext actionContext);
    }

    public class AuthorizationDataProvider : IAuthorizationDataProvider
    {
        private static bool IsMatch(string baseValue, string compareValue)
        {
            return string.IsNullOrWhiteSpace(baseValue) || string.Equals(baseValue, compareValue, StringComparison.OrdinalIgnoreCase);
        }

        private readonly IAuthorizationData _authorizationData;

        public AuthorizationDataProvider(IAuthorizationData authorizationData)
        {
            _authorizationData = authorizationData;
        }

        public IEnumerable<string> GetAuthorizations(HttpActionContext actionContext)
        {
            if (actionContext == null)
                throw new ArgumentNullException("actionContext");

            var controller = actionContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var action = actionContext.ActionDescriptor.ActionName;
            var requestMethod = actionContext.Request.Method.Method;

            return _authorizationData
                .Authorizations
                .Where(a => IsMatch(a.Controller, controller) && IsMatch(a.Action, action) && IsMatch(a.RequestMethod, requestMethod))
                .Select(a => a.Value)
                .ToList();
        }
    }
}
