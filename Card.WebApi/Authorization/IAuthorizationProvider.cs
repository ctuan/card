﻿using System.Web.Http.Controllers;

namespace Card.WebApi.Authorization
{
    public interface IAuthorizationProvider
    {
        bool IsAuthorized(HttpActionContext actionContext, string value);
    }
}