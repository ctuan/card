﻿using System.Collections.Generic;
using Card.WebApi.Configuration;

namespace Card.WebApi.Authorization
{
    public interface IAuthorizationData
    {
        IEnumerable<IAuthorization> Authorizations { get; }
    }
}