﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Card.WebApi.Authorization
{
    public class SecureHashAuthorizationProvider : AuthorizationProvider
    {
        private const int SaltByteLength = 24;

        private readonly ISecureHashAuthorizationProviderSettings _settings;

        public SecureHashAuthorizationProvider(ISecureHashAuthorizationProviderSettings settings, IAuthorizationDataProvider authorizationDataProvider) : base(authorizationDataProvider)
        {
            _settings = settings;
        }

        protected override string TransformAuthorizee(string authorizee)
        {
            return GenerateSaltedHash(authorizee, _settings.Salt);
        }

        public static string GenerateSaltedHash(string value, string salt)
        {
            var valueBytes = Encoding.UTF8.GetBytes(value);
            var saltBytes = Convert.FromBase64String(salt);

            var preHashBytes = new byte[valueBytes.Length + saltBytes.Length];

            Buffer.BlockCopy(valueBytes, 0, preHashBytes, 0, valueBytes.Length);
            Buffer.BlockCopy(saltBytes, 0, preHashBytes, valueBytes.Length, saltBytes.Length);

            var hashedBytes = GenerateHash(preHashBytes);

            return Convert.ToBase64String(hashedBytes);
        }

        public static string GenerateRandomSalt()
        {
            return Convert.ToBase64String(GenerateRandomSaltBytes());
        }

        private static byte[] GenerateHash(byte[] value)
        {
            using (var hasher = new SHA256Managed())
            {
                return hasher.ComputeHash(value);
            }
        }

        private static byte[] GenerateRandomSaltBytes()
        {
            using (var csprng = new RNGCryptoServiceProvider())
            {
                var saltBytes = new byte[SaltByteLength];
                csprng.GetBytes(saltBytes);
                return saltBytes;
            }
        }
    }
}