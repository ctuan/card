﻿namespace Card.WebApi.Authorization
{
    public interface ISecureHashAuthorizationProviderSettings
    {
        string Salt { get; }
    }
}