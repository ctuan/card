﻿namespace Card.WebApi.Authorization
{
    public interface IProtectedDataStoreSettings
    {
        string Path { get; }
    }
}