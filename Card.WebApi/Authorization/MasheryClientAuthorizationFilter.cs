﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http.Controllers;
using Autofac.Integration.WebApi;
using Starbucks.OpenApi.ServiceExtensions.CommonResources;

namespace Card.WebApi.Authorization
{
    public class MasheryClientAuthorizationFilter : IAutofacAuthorizationFilter
    {
        private const string MasheryClientHeaderName = "X-Mashery-Oauth-Client-Id";

        private static IEnumerable<string> GetHeaderValues(HttpRequestHeaders headers, string name)
        {
            IEnumerable<string> values;
        
            return !headers.TryGetValues(name, out values) || values == null
                ? Enumerable.Empty<string>()
                : values
                    .Where(v => !string.IsNullOrWhiteSpace(v))
                    .Select(s => s.Trim())
                    .ToList();
        }

        private static HttpResponseMessage BuildUnauthorizedResponse(HttpRequestMessage request)
        {
            var error = new ErrorResourceBase
            {
                Code = CardWebAPIErrorResource.UnauthorizedMasheryClientErrorCode,
                Message = CardWebAPIErrorResource.UnauthorizedMasheryClientErrorMessage
            };

            return request.CreateResponse(HttpStatusCode.Unauthorized, error);
        }

        private readonly IAuthorizationProvider _authorizationProvider;

        public MasheryClientAuthorizationFilter(IAuthorizationProvider authorizationProvider)
        {
            _authorizationProvider = authorizationProvider;
        }

        public void OnAuthorization(HttpActionContext actionContext)
        {
            var masheryClient = GetHeaderValues(actionContext.Request.Headers, MasheryClientHeaderName)
                .FirstOrDefault();

            if (!_authorizationProvider.IsAuthorized(actionContext, masheryClient))
            {
                actionContext.Response = BuildUnauthorizedResponse(actionContext.Request);
            }
        }
    }
}