﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Card.WebApi.Authorization
{
    public interface IProtectedDataStore
    {
        byte[] ProtectData(byte[] value);
        byte[] ProtectData(string value);
        void ProtectData(string key, string value);
        byte[] UnprotectData(byte[] value);
        string UnprotectDataAsString(byte[] value);
        byte[] GetProtectedData(string key);
        byte[] GetUnprotectedData(string key);
        string GetUnprotectedDataAsString(string key);
    }

    public class ProtectedDataStore : IProtectedDataStore
    {
        private static readonly byte[] _entropy = Encoding.Unicode.GetBytes("N_throw_P3e");

        private readonly ConcurrentDictionary<string, byte[]> _keyToProtectedData = new ConcurrentDictionary<string, byte[]>(StringComparer.OrdinalIgnoreCase);
        private readonly IProtectedDataStoreSettings _settings;

        public ProtectedDataStore(IProtectedDataStoreSettings settings)
        {
            _settings = settings;
        }

        public byte[] ProtectData(byte[] value)
        {
            return ProtectedData.Protect(value, _entropy, DataProtectionScope.LocalMachine);
        }

        public byte[] ProtectData(string value)
        {
            return ProtectData(Encoding.UTF8.GetBytes(value));
        }

        public void ProtectData(string key, string value)
        {
            var protectedDataFilePath = GetProtectedDataFilePath(key);

            // create or update in file system
            using (var fileStream = new FileStream(protectedDataFilePath, FileMode.Create, FileAccess.Write))
            {
                var protectedData = ProtectData(value);

                fileStream.Write(protectedData, 0, protectedData.Length);

                fileStream.Flush(flushToDisk: true);
            }
        }

        public byte[] UnprotectData(byte[] value)
        {
            return ProtectedData.Unprotect(value, _entropy, DataProtectionScope.LocalMachine);
        }

        public string UnprotectDataAsString(byte[] value)
        {
            return Encoding.UTF8.GetString(UnprotectData(value));
        }

        public byte[] GetProtectedData(string key)
        {
            return _keyToProtectedData.GetOrAdd(key, LoadProtectedData);
        }

        public byte[] GetUnprotectedData(string key)
        {
            return UnprotectData(GetProtectedData(key));
        }

        public string GetUnprotectedDataAsString(string key)
        {
            return UnprotectDataAsString(GetProtectedData(key));
        }

        private byte[] LoadProtectedData(string key)
        {
            var protectedDataFilePath = GetProtectedDataFilePath(key);

            if (!File.Exists(protectedDataFilePath))
            {
                throw new FileNotFoundException(string.Format("File not found for specified key '{0}'", key), protectedDataFilePath);
            }

            return File.ReadAllBytes(protectedDataFilePath);
        }

        private string GetProtectedDataFilePath(string key)
        {
            return Path.ChangeExtension(Path.Combine(_settings.Path, key.Replace(':', ';')), ".key");
        }
    }
}
