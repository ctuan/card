﻿using System;
using System.Linq;
using System.Web.Http.Controllers;

namespace Card.WebApi.Authorization
{
    public class AuthorizationProvider : IAuthorizationProvider
    {
        private readonly IAuthorizationDataProvider _authorizationDataProvider;

        public AuthorizationProvider(IAuthorizationDataProvider authorizationDataProvider)
        {
            _authorizationDataProvider = authorizationDataProvider;
        }

        public bool IsAuthorized(HttpActionContext actionContext, string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return false;
            }

            var authorizations = _authorizationDataProvider
                .GetAuthorizations(actionContext)
                .Select(TransformAuthorization)
                .ToList();

            if (authorizations.Count == 0)
            {
                return false;
            }

            var transformedValue = TransformAuthorizee(value);

            return authorizations.Contains(transformedValue, StringComparer.Ordinal);
        }

        protected virtual string TransformAuthorization(string authorization)
        {
            return authorization;
        }

        protected virtual string TransformAuthorizee(string authorizee)
        {
            return authorizee;
        }
    }
}