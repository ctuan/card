[CmdletBinding()]
Param(
  [Parameter(Mandatory=$True,Position=1)]
  [string]$valueToHash,
  
  [Parameter(Mandatory=$True,Position=2)]
  [string]$salt
)

$cardApiPath = (Get-Item -Path ".\bin\Card.WebApi.dll").FullName

[System.Reflection.Assembly]::LoadFile($cardApiPath) | Out-Null

[Card.WebApi.Authorization.SecureHashAuthorizationProvider]::GenerateSaltedHash($valueToHash, $salt)
