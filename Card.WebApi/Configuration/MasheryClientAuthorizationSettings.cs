﻿using System.Collections.Generic;
using System.Configuration;
using Card.WebApi.Authorization;

namespace Card.WebApi.Configuration
{
    public class MasheryClientAuthorizationSettings : ConfigurationSection, IAuthorizationData, IProtectedDataStoreSettings, ISecureHashAuthorizationProviderSettings
    {
        public override bool IsReadOnly()
        {
            return true;
        }

        [ConfigurationProperty("authorizations")]
        [ConfigurationCollection(typeof(AuthorizationCollection), AddItemName = "add", ClearItemsName = "clear", RemoveItemName = "remove")]
        public AuthorizationCollection Authorizations
        {
            get { return (AuthorizationCollection)base["authorizations"]; }
        }

        [ConfigurationProperty("protectedDataStorePath", IsRequired = true)]
        public string ProtectedDataStorePath
        {
            get { return (string)this["protectedDataStorePath"]; }
        }

        [ConfigurationProperty("secureHashSalt", IsRequired = true)]
        public string SecureHashSalt
        {
            get { return (string)this["secureHashSalt"]; }
        }

        IEnumerable<IAuthorization> IAuthorizationData.Authorizations
        {
            get { return Authorizations; }
        }

        string IProtectedDataStoreSettings.Path
        {
            get { return ProtectedDataStorePath; }
        }

        string ISecureHashAuthorizationProviderSettings.Salt
        {
            get { return SecureHashSalt; }
        }
    }
}