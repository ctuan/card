﻿using System.Configuration;

namespace Card.WebApi.Configuration
{
    public interface IAuthorization
    {
        string Controller { get; set; }
        string Action { get; set; }
        string RequestMethod { get; set; }
        string Value { get; set; }
    }

    public class Authorization: ConfigurationElement, IAuthorization
    {
        [ConfigurationProperty("controller")]
        public string Controller
        {
            get { return (string)this["controller"]; }
            set { this["controller"] = value; }
        }

        [ConfigurationProperty("action")]
        public string Action
        {
            get { return (string)this["action"]; }
            set { this["action"] = value; }
        }

        [ConfigurationProperty("requestMethod")]
        public string RequestMethod
        {
            get { return (string)this["requestMethod"]; }
            set { this["requestMethod"] = value; }
        }

        [ConfigurationProperty("value", IsRequired = true)]
        public string Value
        {
            get { return (string)this["value"]; }
            set { this["value"] = value; }
        }
    }
}
