﻿using System.Collections.Generic;
using System.Configuration;

namespace Card.WebApi.Configuration
{
    public class AuthorizationCollection : ConfigurationElementCollection, IEnumerable<Authorization>
    {
        public Authorization this[int index]
        {
            get { return (Authorization)BaseGet(index); }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new Authorization();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            var authorization = (Authorization)element;

            return string.Format("{0}/{1}/{2}/{3}", authorization.Controller, authorization.Action, authorization.RequestMethod, authorization.Value);
        }

        IEnumerator<Authorization> IEnumerable<Authorization>.GetEnumerator()
        {
            for (int i = 0; i < Count; i++)
            {
                yield return this[i];
            }
        }
    }
}
