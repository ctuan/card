﻿using System.Configuration;
using System.Reflection;
using System.Web.Http;
using Account.Provider;
using Account.Provider.Common;
using Autofac;
using Autofac.Configuration;
using Autofac.Core;
using Autofac.Integration.WebApi;
using Card.WebApi.Authorization;
using Card.WebApi.Configuration;
using Card.WebApi.Controllers;
using Card.WebApi.Helpers;
using Card.WebApi.HttpParameterTransforms;
using Card.WebApi.Models;
using Services.LoyaltyClient.LoyaltyHostServiceProxy;
using Starbucks.Address.Dal.Common;
using Starbucks.Address.Dal.Sql;
using Starbucks.Address.Dal.Sql.Configuration;
using Starbucks.Api.Sdk.PaymentService;
using Starbucks.BadWords.Dal.Provider.Sql;
using Starbucks.BadWords.Dal.Provider.Sql.Configuration;
using Starbucks.BadWords.Provider;
using Starbucks.BadWords.Provider.Common;
using Starbucks.Card.Dal.Common.Interfaces;
using Starbucks.Card.Dal.Sql.Configuration;
using Starbucks.Card.Dal.Sql.EntLibLess;
using Starbucks.Card.Dal.Sql.EntLibLess.Configuration;
using Starbucks.Card.Provider;
using Starbucks.Card.Provider.Common;
using Starbucks.Card.Provider.Configuration;
using Starbucks.CardIssuer.Dal.Common;
using Starbucks.CardIssuer.Dal.SvDot;
using Starbucks.CardIssuer.Dal.SvDot.Common;
using Starbucks.CardIssuer.Dal.SvDot.Configuration;
using Starbucks.CardIssuer.Dal.SvDot.Db;
using Starbucks.CardIssuer.Dal.SvDot.TransportModels;
using Starbucks.CardTransaction.Provider;
using Starbucks.CardTransaction.Provider.Common;
using Starbucks.CardTransaction.Provider.Configuration;
using Starbucks.FraudRecovery.Common;
using Starbucks.FraudRecovery.Common.Logging;
using Starbucks.FraudRecovery.Dal;
using Starbucks.FraudRecovery.Dal.Common;
using Starbucks.FraudRecovery.Provider;
using Starbucks.LogCounter.Provider;
using Starbucks.LogCounter.Provider.Common;
using Starbucks.OpenApi.Caching.AppFabric;
using Starbucks.OpenApi.Caching.AppFabric.Configuration;
using Starbucks.OpenApi.Caching.Common;
using Starbucks.OpenApi.Caching.Dictionary;
using Starbucks.OpenApi.Caching.Dictionary.Configuration;
using Starbucks.OpenApi.IpAddress.Helpers;
using Starbucks.OpenApi.Logging.Common;
using Starbucks.OpenApi.Logging.EnterpriseLibrary;
using Starbucks.OpenApi.Utilities.Common.Helpers;
using Starbucks.OpenApi.WebApi.Common.Helpers;
using Starbucks.OrderManagement.Provider.Common;
using Starbucks.OrderManagement.Provider.Nsb;
using Starbucks.PaymentMethod.Provider;
using Starbucks.PaymentMethod.Provider.Common;
using Starbucks.PaymentMethod.Provider.SqlHybrid;
using Starbucks.PaymentMethod.Provider.SqlHybrid.Configuration;
using Starbucks.Rewards.Dal.Common;
using Starbucks.Rewards.Dal.LoyaltyService;
using Starbucks.Rewards.Provider;
using Starbucks.Rewards.Provider.Common;
using Starbucks.Settings.Provider.Common;
using Starbucks.TransactionHistory.Dal.Common;
using Starbucks.TransactionHistory.Provider.Common;
using Starbucks.TransactionHistory.Provider.Configuration;
using CardDal = Starbucks.Card.Dal.Sql.CardDal;

namespace Card.WebApi.App_Start
{
    public class DependencyConfig
    {
        public static void Configure(HttpConfiguration httpConfiguration)
        {
            var builder = new ContainerBuilder();

            builder.Register(c => ConfigurationManager.GetSection("badWordProviderSettings") as BadWordsSection)
                   .As<BadWordsSection>();

            builder.Register(c => ConfigurationManager.GetSection("socialProfileDataSettings") as Starbucks.SocialProfile.Dal.Sql.Configuration.SocialProfileConfig)
                 .As<Starbucks.SocialProfile.Dal.Sql.Configuration.SocialProfileConfig>();

            builder.Register(
                c => ConfigurationManager.GetSection("paymentMethodProviderSettings") as PaymentMethodProviderSection)
                   .As<PaymentMethodProviderSection>();
            builder.Register(
                c => ConfigurationManager.GetSection("addressProviderSettings") as AddressDataProviderSection)
                   .As<AddressDataProviderSection>();

            builder.RegisterType<Account.Dal.Sql.AccountSqlDataProvider>().WithParameter(new PositionalParameter(0, "Profiles")).As<Account.Dal.IAccountDataProvider>();

            builder.RegisterType<AddressDataProvider>().As<IAddressDataProvider>();

            builder.RegisterType<Starbucks.SocialProfile.Dal.Sql.SocialProfileDataProvider>().As<Starbucks.SocialProfile.Dal.Common.ISocialProfileDataProvider>();

            builder.RegisterType<AccountProvider>().As<IAccountProvider>();

            builder.RegisterType<CardDal>().As<ICardDal>();

            builder.Register(c => ConfigurationManager.GetSection("cardDalSettings") as CardDalSqlSettingsSection)
                 .As<CardDalSqlSettingsSection>();

            builder.Register(c => ConfigurationManager.GetSection("cardProviderSettings") as CardProviderSettingsSection)
                   .As<Starbucks.Card.Provider.Configuration.CardProviderSettingsSection>();

            builder.RegisterType<CardLogger>().As<ICardLogger>();

			#region FraudRecovery section

            builder.Register(c => ConfigurationManager.GetSection("fraudRecoverySettings") as IFraudRecoveyConfiguration)
                   .As<IFraudRecoveyConfiguration>().InstancePerApiRequest();

            builder.RegisterType<DebugLoggingUtility>().As<ILoggingUtility>().InstancePerApiRequest();
            builder.RegisterType<FraudRecoveryDal>().As<IFraudRecoveryDal>().InstancePerApiRequest();
            builder.RegisterType<DependencyFactory>().As<IDependencyFactory>().InstancePerApiRequest();
            builder.RegisterType<FraudRecoveryProvider>().As<IFraudRecoveryProvider>().InstancePerApiRequest();
			
			#endregion

            builder.Register(
                c =>
                ConfigurationManager.GetSection("cardTransactionProviderSettingsSection") as
                CardTransactionProviderSettingsSection)
                   .As<CardTransactionProviderSettingsSection>();


            builder.Register(
                c =>
                ConfigurationManager.GetSection("svDotConfiguration") as
                SvDotConfigurationSettings)
                   .As<SvDotConfigurationSettings>();

            builder.RegisterType<LogRepository>().As<ILogRepository>().As<LogRepository>();

            builder.RegisterType<CardTransactionDal>()
                   .WithParameter(new NamedParameter("databaseName", "CardTransactions"))
                   .WithParameter(new NamedParameter("commerce_databaseName", "Commerce"))
                   .Named<ICardTransactionDal>("CardTransactionDalImplementor");

            builder.Register(c => new CachedCardTransactionDal(c.ResolveNamed<ICardTransactionDal>("CardTransactionDalImplementor")))
                   .As<ICardTransactionDal>();

            builder.RegisterType<SvDotTransportXml>().As<ISvDotTransport>();
            builder.RegisterType<PassbookService>().As<IPassbookService>();
            builder.RegisterType<SvDotCardIssuer>().As<ICardIssuerDal>();

            builder.Register(c => LogCounterManager.Instance()).As<ILogCounterManager>();

            builder.RegisterType<PaymentMethodProvider>().As<IPaymentMethodProvider>();
            builder.RegisterType<PaymentMethodDataProvider>().As<IPaymentMethodDataProvider>();

            builder.RegisterType<BadWordsProvider>().As<IBadWordsProvider>();
            builder.RegisterType<BadWordsDataProvider>().As<IBadWordsDataProvider>();

            builder.RegisterType<Starbucks.SecurePayPalPayment.Provider.SecurePayPalPaymentClient>()
                   .As<Starbucks.SecurePayPalPayment.Provider.Common.ISecurePayPalPaymentClient>();

            builder.RegisterType<Starbucks.MessageBroker.LoyaltyHostServiceListener.LoyaltyHostServiceListener>().Named<Starbucks.MessageBroker.Common.IMessageListener>("loyaltyHostListener");

            builder.RegisterType<Starbucks.Api.Sdk.WebRequestWrapper.WebRequestWrapper>().As<Starbucks.Api.Sdk.WebRequestWrapper.IWebRequestWrapper>();
            builder.RegisterType<PaymentServiceApiClient>().As<IPaymentServiceApiClient>();

            builder.RegisterType<CardProvider>()
                .WithParameter((pi, ctx) => pi.ParameterType == typeof(Starbucks.MessageBroker.Common.IMessageListener) && pi.Name == "loyaltyHostListener",
                              (pi, ctx) => ctx.ResolveNamed<Starbucks.MessageBroker.Common.IMessageListener>("loyaltyHostListener"))
                .As<ICardProvider>();

            builder.RegisterType<CardTransactionProvider>().As<ICardTransactionProvider>();

            builder.RegisterType<ViewModelStore>().As<ViewModelStore>();
            builder.RegisterType<Starbucks.Settings.Provider.SettingsProvider>().As<ISettingsProvider>();

            #region Passbook

            builder.RegisterType<RequestHelper>().As<IRequestHelper>();
          
            builder.RegisterType<ConverterWrapper>().As<IConverterWrapper>();

            builder.Register(c =>
                    new DictionaryCacheProvider(DictionaryCacheProviderSettings.Settings, c.Resolve<IDataCacheManager>())
                )
                .Named<IDataCacheProvider>("dictionaryCacheProvider");
            builder.RegisterType<LogRepository>().As<ILogRepository>();
            builder.Register(
                c => new AppFabricCacheProvider(AppFabricCacheProviderSettings.Settings,
                    c.Resolve<IDataCacheManager>()))
                .Named<IDataCacheProvider>("appFabricCacheProvider");
            builder.Register(s => ConfigurationManager.GetSection("locationSettings") as LocationSettings)
                .As<ILocationSettings>();
            builder.RegisterType<LocationHelper>()
                .As<ILocationHelper>();
            builder.RegisterType<RetryHelper>().As<IRetryHelper>();
            builder.RegisterType<HttpWebRequestWrapperFactory>().As<IRequestWrapperFactory>();
            builder.RegisterType<HttpWebRequestWrapper>().As<IRequestWrapper>();
            builder.RegisterType<ServicePointManager>().As<IServicePointManager>();
            builder.RegisterType<StreamWriterWrapper>().As<IStreamWriterWrapper>();
            builder.RegisterType<Starbucks.OpenApi.Utilities.Common.Helpers.LogHelper>()
                .As<ILogHelper>();
            builder.RegisterType<LogWrapper>()
                .As<ILogWrapper>();
            builder.RegisterType<LogEntryFactory>()
                .As<ILogEntryFactory>();
            builder.RegisterType<LogCounterWrapper>().As<ILogCounterWrapper>();
           builder.RegisterType<EnumParser>().As<IEnumParser>();

            #endregion

            #region Tip

            //TODO: Get from config
            builder.RegisterType<Starbucks.Tipping.Dal.Sql.TippingDataProvider>()
                   .As<Starbucks.Tipping.Dal.Common.ITippingDataProvider>().WithParameter(new PositionalParameter(0, "Profiles")); ;

            builder.RegisterType<Starbucks.Tipping.Provider.TippingProvider>()
                   .As<Starbucks.Tipping.Provider.Common.ITippingProvider>();

            //TODO: Get from config
            builder.RegisterType<Starbucks.TransactionHistory.Dal.Sql.TransactionHistorySqlDataProvider>()
                   .As<ITransactionHistoryDataProvider>();

            builder.RegisterType<TransactionHistorySqlDataProviderSettings>()
                   .As<ITransactionHistorySqlDataProviderSettings>();
            //builder.RegisterType<LoyaltyDal>().As<IRewardsDal>();

            //builder.RegisterType<RewardsCheckSqlDataProvider>()
            //       .As<IRewardsCheckDataProvider>()
            //       .WithParameter(new PositionalParameter(0, "TransactionHistory"));

            //builder.RegisterType<TransactionHistoryProvider>().As<ITransactionHistoryProvider>();

            builder.Register(
                p =>
                new Starbucks.TransactionHistory.Provider.TransactionHistoryProvider(
                    p.Resolve<ITransactionHistoryDataProvider>(), TransactionHistoryProviderSettings.Settings)).As<ITransactionHistoryProvider>();
            #endregion

            #region SelectedCards
            builder.Register(
                c =>
                ConfigurationManager.GetSection("cardDalEntLibLessSettings") as
                CardDalEntLibLessSqlSettingsSection)
                   .As<CardDalEntLibLessSqlSettingsSection>();
            builder.RegisterType<SelectedCardProvider>().As<ISelectedCardProvider>();
            builder.RegisterType<SelectedCardDal>().As<ISelectedCardDal>();

            #endregion

            #region Rewards
            builder.RegisterType<RewardsProvider>().As<IRewardsProvider>();
            builder.RegisterType<LoyaltyDal>().As<IRewardsDal>();


            #endregion

            builder.RegisterType<Services.LoyaltyClient.LoyaltyHostServiceProxy.LoyaltyHostServiceClient>().WithParameter(new NamedParameter("endPointName", "WSHttpBinding_ILoyaltyService")).As<ILoyaltyHostService>();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterModule(new ConfigurationSettingsReader("autofac"));

            builder.RegisterType<NsbOrderManagementProvider>().As<IOrderManagementProvider>();

            // register autofac modules in Starbucks.OpenApi.IpAddress
            builder.RegisterAssemblyModules(typeof(IpAddressUtility).Assembly);

            // register ModelWithRiskTransform
            builder.RegisterType<ModelWithRiskTransform>()
                .AsSelf();

            const string MasheryClientAuthorizationKey = "MasheryClientAuthorization";
            const string ProtectedDataStoreAuthorizationProviderKey = MasheryClientAuthorizationKey + "-ProtectedDataStore";
            const string SecureHashAuthorizationProviderKey = MasheryClientAuthorizationKey + "-SecureHash";

            builder.Register(c => (MasheryClientAuthorizationSettings)ConfigurationManager.GetSection("masheryClientAuthorizationSettings"))
                .Named<IAuthorizationData>(MasheryClientAuthorizationKey)
                .Named<IProtectedDataStoreSettings>(MasheryClientAuthorizationKey)
                .Named<ISecureHashAuthorizationProviderSettings>(MasheryClientAuthorizationKey)
                .SingleInstance();

            builder.RegisterType<AuthorizationDataProvider>()
                .WithParameter(ResolvedParameter.ForNamed<IAuthorizationData>(MasheryClientAuthorizationKey))
                .Named<IAuthorizationDataProvider>(MasheryClientAuthorizationKey)
                .SingleInstance();

            builder.RegisterType<ProtectedDataStore>()
                .WithParameter(ResolvedParameter.ForNamed<IProtectedDataStoreSettings>(MasheryClientAuthorizationKey))
                .Named<IProtectedDataStore>(MasheryClientAuthorizationKey)
                .SingleInstance();

            builder.RegisterType<ProtectedDataStoreAuthorizationProvider>()
                .WithParameter(ResolvedParameter.ForNamed<IAuthorizationDataProvider>(MasheryClientAuthorizationKey))
                .WithParameter(ResolvedParameter.ForNamed<IProtectedDataStore>(MasheryClientAuthorizationKey))
                .Named<IAuthorizationProvider>(ProtectedDataStoreAuthorizationProviderKey)
                .SingleInstance();

            builder.RegisterType<SecureHashAuthorizationProvider>()
                .WithParameter(ResolvedParameter.ForNamed<ISecureHashAuthorizationProviderSettings>(MasheryClientAuthorizationKey))
                .WithParameter(ResolvedParameter.ForNamed<IAuthorizationDataProvider>(MasheryClientAuthorizationKey))
                .Named<IAuthorizationProvider>(SecureHashAuthorizationProviderKey)
                .SingleInstance();

            builder.RegisterWebApiFilterProvider(httpConfiguration);

            builder.RegisterType<MasheryClientAuthorizationFilter>()
                .WithParameter(ResolvedParameter.ForNamed<IAuthorizationProvider>(SecureHashAuthorizationProviderKey))
                .AsWebApiAuthorizationFilterFor<FraudRecoveryController>()
                .SingleInstance();

            // Build the Autofac container
            var container = builder.Build();

            // Configure NSB SendOnlyBus and add it to the container
            NsbConfig.ConfigureSendOnlyBus(container);

            // Wire-up the WebApi DependencyResolver
            httpConfiguration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }

    public class TransactionHistorySqlDataProviderSettings : ITransactionHistorySqlDataProviderSettings
    {
        public TransactionHistorySqlDataProviderSettings()
        {
            ConnectionStringName = "TransactionHistory";
            UpsertTransactionCommandTimeout = 1000 * 5;
            UpsertTransactionsCommandTimeout = 1000 * 5;
            ReadTransactionsTimeoutSeconds = 2;
        }
        public string ConnectionStringName { get; set; }

        public int ReadTransactionsTimeoutSeconds { get; set; }
        public int UpsertTransactionCommandTimeout { get; set; }
        public int UpsertTransactionsCommandTimeout { get; set; }
    }
}