﻿using System.Net.Http.Formatting;
using Card.WebApi.Models;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Starbucks.OpenApi.WebApi.Common.MediaTypeFormatters;
using Starbucks.OpenApi.WebApi.Common.Serializer;

namespace Card.WebApi.App_Start
{
  
        public class FormattersConfig
        {

            public static void RegisterFormatters(MediaTypeFormatterCollection formatters)
            {
                var settings = new SbuxSerializerSettings()
                {
                    SourceAssembly = typeof(Card.WebApi.Models.StarbucksCard).Assembly,
                    SourceNamespace = typeof(Card.WebApi.Models.StarbucksCard).Namespace,                   
                };
                SbuxConvert.Initialize(settings);

                var jsonFormatter = new SbuxJsonpMediaTypeFormatter();
                var xmlFormatter = new SbuxXmlMediaTypeFormatter();

                xmlFormatter.SbuxXmlWriterSettings.RootNamespace = Constants.ContractNamespace;

                formatters.Remove(formatters.XmlFormatter);
                formatters.Remove(formatters.JsonFormatter);

                formatters.Add(xmlFormatter );
                formatters.Add(jsonFormatter);

                // for json.net serialization, use pretty printing, make dictionaries look more json, have enums use string values (vs int), camel casing throughout all properties
                jsonFormatter.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
                jsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());
                jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            }
        }
}