﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using NServiceBus;
using NServiceBus.Features;
using Starbucks.OrderManagement.QueueService.Common.Commands;

namespace Card.WebApi.App_Start
{
    public static class NsbConfig
    {
        public static void ConfigureSendOnlyBus(IContainer container)
        {
            if (container == null)
                throw new ArgumentNullException("container");

            var busConfig = new BusConfiguration();

            // ExistingLifetimeScope ensures that the IBus is added to the container
            busConfig.UseContainer<AutofacBuilder>(c => c.ExistingLifetimeScope(container));

            busConfig.DisableFeature<SecondLevelRetries>();
            busConfig.DisableFeature<Sagas>();
            busConfig.DisableFeature<TimeoutManager>();

            busConfig.Conventions().DefiningCommandsAs(NsbConventions.TypeIsCommand);
            busConfig.Conventions().DefiningEventsAs(NsbConventions.TypeIsEvent);
            busConfig.Conventions().DefiningMessagesAs(NsbConventions.TypeIsMessage);

            busConfig.AssembliesToScan(GetAssembliesToScan());

            Bus.CreateSendOnly(busConfig);
        }

        private static IEnumerable<Assembly> GetAssembliesToScan()
        {
            return new[]
            {
                AssemblyFromType<ISaveFailedOrder>(),
                AssemblyFromType<Starbucks.FraudData.QueueService.Common.Models.ReputationCheckTypes>(),
                AssemblyFromType<Starbucks.ChasePayConfirmation.QueueService.Common.Commands.ConfirmPay>()
               
            };
        }

        private static Assembly AssemblyFromType<T>() { return typeof(T).Assembly; }

        private static class NsbConventions
        {
            private const string CommandsNamespace = "QueueService.Common.Commands";
            private const string EventsNamespace = "QueueService.Common.Events";
            private const string MessagesNamespace = "QueueService.Common.Messages";

            private static readonly Type[] NsbMessageTypes = new[] { typeof(ICommand), typeof(IEvent), typeof(IMessage) };

            public static bool TypeIsCommand(Type t) { return TypeIsMessage<ICommand>(t, CommandsNamespace); }

            public static bool TypeIsEvent(Type t) { return TypeIsMessage<IEvent>(t, EventsNamespace); }

            public static bool TypeIsMessage(Type t) { return TypeIsMessage<IMessage>(t, MessagesNamespace); }

            private static bool TypeIsMessage<TMessageSubtype>(Type t, string namespaceConvention) where TMessageSubtype : IMessage
            {
                // This allows us to define NSB message types using a naming convention while maintaining support for explicit marker interfaces
                return !NsbMessageTypes.Contains(t) && (typeof(TMessageSubtype).IsAssignableFrom(t) || (t.Namespace != null && t.Namespace.EndsWith(namespaceConvention, StringComparison.OrdinalIgnoreCase)));
            }
        }
    }
}