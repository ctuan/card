﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Web;
using Services.Extension.ShutdownListener.ProviderSettings;
using Starbucks.OpenApi.ServiceExtensions.ServiceShutdownHandler;
using Card.WebApi.Handlers;

namespace Card.WebApi.App_Start
{
    public class MessageHandlersConfig
    {
        public static void RegisterMessageHandlers(Collection<DelegatingHandler> messageHandlers)
        {
            if (ShutdownProviderSettings.Settings.ServiceShutdownEnabled)
            {
                messageHandlers.Add(new ServiceShutdownMessageHandler(ShutdownProviderSettings.Settings));
            }
            messageHandlers.Add(new CorrelationIdMessageHandler());
        }

    }
}