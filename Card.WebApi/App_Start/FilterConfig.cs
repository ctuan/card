﻿using System.Web.Http;
using System.Web.Http.Filters;
using Starbucks.OpenApi.ServiceExtensions.Filters;
using Starbucks.OpenApi.Logging.Common;

namespace Card.WebApi
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(HttpFilterCollection filters)
        {
            var logRepository = GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(ILogRepository)) as ILogRepository;
            filters.Add(new ApiExceptionFilter(logRepository));
        }
    }
}