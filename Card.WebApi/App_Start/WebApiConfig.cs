﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;
using System.Web.Mvc;

namespace Card.WebApi
{
    public static class WebApiConfig
    {
	    public static void Register(HttpConfiguration config)
	    {
		    // ReSharper disable RedundantArgumentName

		    config.Routes.MapHttpRoute(
			    name: "Ping",
			    routeTemplate: "ping/{level}",
			    defaults: new {controller = "Ping", action = "Ping", level = UrlParameter.Optional},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Get)}
			    );

			#region Fraud Recovery Tool

			config.Routes.MapHttpRoute(
				name: "BulkSvcCardBalanceRountes",
				routeTemplate: "cards/{cardId}/card-balance",
				defaults: new { controller = "FraudRecovery", action = "card-balance" },
				constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) }
				);

			#endregion

		    config.Routes.MapHttpRoute(
			    name: "CreateAutoReload",
			    routeTemplate: "users/{userId}/cards/{cardId}/autoreload",
			    defaults: new {controller = "Card", action = "CreateAutoReload"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Post)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "UpdateAutoReload",
			    routeTemplate: "users/{userId}/cards/{cardId}/autoreload",
			    defaults: new {controller = "Card", action = "UpdateAutoReload"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Put)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "EnableAutoReload",
			    routeTemplate: "users/{userId}/cards/{cardId}/autoreload/enable",
			    defaults: new {controller = "Card", action = "EnableAutoReload"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Put)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "DisableAutoReload",
			    routeTemplate: "users/{userId}/cards/{cardId}/autoreload/disable",
			    defaults: new {controller = "Card", action = "DisableAutoReload"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Put)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "CardImageUrlByCardNumber",
			    routeTemplate: "cards/{cardNumber}/images",
			    defaults: new {controller = "Card", action = "CardImageUrlByCardNumber"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Get)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "CardByCardNumber",
			    routeTemplate: "cards/{cardNumber}/{pin}",
			    defaults: new {controller = "Card", action = "CardByNumberAndPin"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Get)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "CardStatuses",
			    routeTemplate: "users/{userId}/cards/validate-registration",
			    defaults: new {controller = "Card", action = "CardStatuses"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Get)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "CardPrimaryCardByUserId",
			    routeTemplate: "users/{userId}/cards/primary",
			    defaults: new {controller = "Card", action = "CardPrimaryCardByUserId"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Get)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "CardByUserIdCardId",
			    routeTemplate: "users/{userId}/cards/{cardId}",
			    defaults: new {controller = "Card", action = "CardByUserIdCardId"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Get)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "UpdateCard",
			    routeTemplate: "users/{userId}/cards/{cardId}",
			    defaults: new {controller = "Card", action = "UpdateCard"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Put)}
			    );


		    config.Routes.MapHttpRoute(
			    name: "SetDefaultCard",
			    routeTemplate: "users/{userId}/cards/{cardId}/primary",
			    defaults: new {controller = "Card", action = "SetDefaultCard"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Put)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "CardsByUserId",
			    routeTemplate: "users/{userId}/cards",
			    defaults: new {controller = "Card", action = "CardsByUserId"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Get)}
			    );



		    config.Routes.MapHttpRoute(
			    name: "GetCardBalanceByCardNumber",
			    routeTemplate: "cards/{cardNumber}/{pin}/balance",
			    defaults: new {controller = "Card", action = "GetCardBalanceByCardNumber"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Get)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "GetCardBalanceByCardId",
			    routeTemplate: "users/{userId}/cards/{cardId}/balance",
			    defaults: new {controller = "Card", action = "GetCardBalanceByCardId"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Get)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "GetCardBalanceByCardNumberRealTime",
			    routeTemplate: "cards/{cardNumber}/{pin}/balance-realtime",
			    defaults: new {controller = "Card", action = "GetCardBalanceByCardNumberRealTime"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Get)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "GetCardBalanceByCardIdRealTime",
			    routeTemplate: "users/{userId}/cards/{cardId}/balance-realtime",
			    defaults: new {controller = "Card", action = "GetCardBalanceByCardIdRealTime"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Get)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "RegisterByCardNumberPin",
			    routeTemplate: "users/{userId}/cards/register",
			    defaults: new {controller = "Card", action = "RegisterByCardNumberPin"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Post)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "RegisterByUserId",
			    routeTemplate: "users/{userId}/cards/register-digital",
			    defaults: new {controller = "Card", action = "RegisterByUserId"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Post)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "RegisterMultipleCards",
			    routeTemplate: "users/{userId}/cards/register-multiple",
			    defaults: new {controller = "Card", action = "RegisterMultipleCards"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Post)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "UnregisterCardByHttpPost",
			    routeTemplate: "users/{userId}/cards/{cardId}/unregister",
			    defaults: new {controller = "Card", action = "UnregisterCardByHttpPost"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Post)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "UnregisterCard",
			    routeTemplate: "users/{userId}/cards/{cardId}",
			    defaults: new {controller = "Card", action = "UnregisterCard"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Delete)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "ReloadByAccount",
			    routeTemplate: "users/{userId}/cards/{cardId}/reload",
			    defaults: new {controller = "CardTransaction", action = "ReloadByAccount"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Post)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "ReloadByCardNumber",
			    routeTemplate: "cards/{cardNumber}/{pin}/reload",
			    defaults: new {controller = "CardTransaction", action = "ReloadByCardNumber"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Post)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "TransferBalance",
			    routeTemplate: "users/{userId}/cards/transfer",
			    defaults: new {controller = "Card", action = "TransferBalance"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Post)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "CreatePassbookFromCardNumber",
			    routeTemplate: "cards/{cardNumber}/{pin}/passbook",
			    defaults: new {controller = "Passbook", action = "CreatePassbookFromCardNumber"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Post)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "CreatePassbookByStarbucksAccount",
			    routeTemplate: "users/{userId}/cards/{cardId}/passbook",
			    defaults: new {controller = "Passbook", action = "CreatePassbookByStarbucksAccount"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Post)}
			    );


		    config.Routes.MapHttpRoute(
			    name: "UpsertTip",
			    routeTemplate: "users/{userId}/cards/history/{historyId}/tip",
			    defaults: new {controller = "Tip", action = "UpsertTip"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Post, HttpMethod.Put)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "DeleteTip",
			    routeTemplate: "users/{userId}/cards/history/{historyId}/tip",
			    defaults: new {controller = "Tip", action = "DeleteTip"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Delete)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "ProcessTip",
			    routeTemplate: "users/{userId}/cards/history/{historyId}/tip/process",
			    defaults: new {controller = "Tip", action = "ProcessTip"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Post)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "AssociateSelectedCards",
			    routeTemplate: "users/{userId}/clients/{clientId}/selectedcards/associate",
			    defaults: new {controller = "SelectedCard", action = "AssociateSelectedCards"},
			    constraints: new {httpMethod = new HttpMethodConstraint(new[] {HttpMethod.Post, HttpMethod.Put})}
			    );

		    config.Routes.MapHttpRoute(
			    name: "GetSelectedCards",
			    routeTemplate: "users/{userId}/clients/{clientId}/selectedcards",
			    defaults: new {controller = "SelectedCard", action = "GetSelectedCards"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Get)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "GetAllSelectedCards",
			    routeTemplate: "users/{userId}/selectedcards",
			    defaults: new {controller = "SelectedCard", action = "GetSelectedCards"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Get)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "RemoveSelectedCards",
			    routeTemplate: "users/{userId}/clients/{clientId}/selectedcards/remove",
			    defaults: new {controller = "SelectedCard", action = "RemoveSelectedCards"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Post)}
			    );

		    config.Routes.MapHttpRoute(
			    name: "IsCardLinked",
			    routeTemplate: "users/{userId}/cards/{cardId}/selected",
			    defaults: new {controller = "SelectedCard", action = "IsCardLinked"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Get)}
			    );
		    config.Routes.MapHttpRoute(
			    name: "AddCardAssociationByNumber",
			    routeTemplate: "users/{userId}/cards/associated",
			    defaults: new {controller = "Card", action = "AddCardAssociationByNumber"},
			    constraints: new {httpMethod = new HttpMethodConstraint(HttpMethod.Post)}
			    );

			#region Fraud Recovery Tool

			config.Routes.MapHttpRoute(
				name: "BulkTransactionRoutes",
				routeTemplate: "cards/transaction/{transactionId}/{action}",
				defaults: new { controller = "FraudRecovery" },
				constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) }
				);

			config.Routes.MapHttpRoute(
				name: "BulkSvcCardRountes",
				routeTemplate: "cards/{cardId}/{action}",
				defaults: new { controller = "FraudRecovery" },
				constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) }
				);

			#endregion
	    }
    }
}
