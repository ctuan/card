﻿using System;
using System.Collections.Generic;
using System.Linq;
using Account.Provider.Common.Models;
using Card.WebApi.Models;
using Starbucks.Card.Provider.Common.Models;
using Starbucks.CardTransaction.Provider.Common.Model;
using Starbucks.Platform.Security;
using ReloadForStarbucksCard = Starbucks.CardTransaction.Provider.Common.Model.ReloadForStarbucksCard;
using Reputation = Card.WebApi.Models.Reputation;

namespace Card.WebApi
{
    public static class Converters
    {
        private const bool DefaultIsLoggedIn = true;

        public static IEnumerable<StarbucksCardBalance> ToApi(this ITransferBalanceResult transferBalanceResult)
        {
            return transferBalanceResult.Balances.Select(balance => new StarbucksCardBalance
            {
                CardNumber = balance.Key,
                Balance = balance.Value,
                BalanceCurrencyCode = transferBalanceResult.StarbucksCardTransaction.CurrencyCode,
                BalanceDate = transferBalanceResult.StarbucksCardTransaction.TransactionDate
            }).ToList();
        }

        //autoreload_status_key	status_name
        //0	inactive
        //1	active
        //2	suspended
        //3	disabled
        //4	temporary
        private static string GetAutoReloadProfileStatus(int status)
        {
            switch (status)
            {
                case 0:
                    return "inactive";
                case 1:
                    return "active";
                case 2:
                    return "suspended";
                case 3:
                    return "disabled";
                case 4:
                    return "temporary";
                default:
                    return string.Empty;
            }
        }

        private static int GetAutoReloadProfileStatus(string status)
        {
            status = string.IsNullOrEmpty(status) ? "" : status.ToLower();
            switch (status)
            {
                case "inactive":
                    return 0;
                case "active":
                    return 1;
                case "suspended":
                    return 2;
                case "disabled":
                    return 3;
                case "temporary":
                    return 4;
                default:
                    return 1;
            }
        }

        public static IAutoReloadProfile ToProvider(this AutoReloadProfile autoReloadProfile)
        {
            return new ProviderModels.AutoReloadProfile
            {
                Amount = autoReloadProfile.Amount,
                AutoReloadId = autoReloadProfile.AutoReloadId,
                AutoReloadType = autoReloadProfile.AutoReloadType,
                //  BillingAgreementId = autoReloadProfile.BillingAgreementId,
                CardId = autoReloadProfile.CardId,
                Day = autoReloadProfile.Day,
                DisableUntilDate = autoReloadProfile.DisableUntilDate,
                PaymentMethodId = autoReloadProfile.PaymentMethodId,
                //  PaymentType = autoReloadProfile.PaymentType,
                Status = GetAutoReloadProfileStatus(autoReloadProfile.Status),
                StoppedDate = autoReloadProfile.StoppedDate,
                TriggerAmount = autoReloadProfile.TriggerAmount
            };
        }

        public static Starbucks.CardTransaction.Provider.Common.Model.IBalanceTransfer ToProvider(
            this Card.WebApi.Models.BalanceTransfer transferStarbucksCard, string market)
        {
            return new Starbucks.CardTransaction.Provider.Common.Model.BalanceTransfer
            {
                SourceCardId = transferStarbucksCard.SourceCardId,
                TargetCardId = transferStarbucksCard.TargetCardId,
                Amount = transferStarbucksCard.Amount,
                Risk = transferStarbucksCard.RiskFields.ToProvider(market)
            };
        }

        public static Starbucks.CardTransaction.Provider.Common.Model.IReloadForStarbucksCard ToProvider(
            this Card.WebApi.Models.ReloadForStarbucksCard reloadForStarbucksCard, string market = "US")
        {
            return new ReloadForStarbucksCard
                {
                    Amount = reloadForStarbucksCard.Amount,
                    SessionId = reloadForStarbucksCard.SessionId,
                    PaymentMethodId = reloadForStarbucksCard.PaymentMethodId,
                    IpAddress = reloadForStarbucksCard.IpAddress,
                    Platform = reloadForStarbucksCard.Platform,
                    PurchaserSessionDuration = reloadForStarbucksCard.PurchaserSessionDuration,
                    BillingAddress = reloadForStarbucksCard.BillingAddress.ToProvider(),
                    PaymentTokenDetails = reloadForStarbucksCard.ToPaymentToken(),
                    Risk = reloadForStarbucksCard.RiskFields.ToProvider(market)
                };
        }

        public static Starbucks.CardTransaction.Provider.Common.Model.PaymentTokenType ToPaymentTokenType(this string tokenTypeString)
        {
            if (!string.IsNullOrWhiteSpace(tokenTypeString))
            {
                Starbucks.CardTransaction.Provider.Common.Model.PaymentTokenType tokenType;

                if (Enum.TryParse<Starbucks.CardTransaction.Provider.Common.Model.PaymentTokenType>(tokenTypeString, out tokenType))
                {
                    return tokenType;
                }
            }

            throw new ArgumentException("Unknown Payment Token Type: " + tokenTypeString ?? "<NULL>");
        }


        public static Starbucks.CardTransaction.Provider.Common.Model.IPaymentTokenDetails ToPaymentToken(
            this Card.WebApi.Models.ReloadForStarbucksCard reloadForStarbucksCard)
        {
            if (reloadForStarbucksCard == null || 
                string.IsNullOrWhiteSpace(reloadForStarbucksCard.PaymentTokenType))
            {
                return null;
            }

            Starbucks.CardTransaction.Provider.Common.Model.PaymentTokenType tokenType = reloadForStarbucksCard.PaymentTokenType.ToPaymentTokenType();

            var result = new ProviderModels.PaymentTokenDetails()
            {
                PaymentToken = reloadForStarbucksCard.PaymentToken,
                PaymentTokenType = tokenType,
            };

            var details = reloadForStarbucksCard.PaymentTokenExtension;
            if (details != null)
            {
                result.ExpirationMonth = details.ExpirationMonth;
                result.ExpirationYear = details.ExpirationYear;
                result.ECIndicator = details.ECIndicator;
                result.TokenRequestorId = details.TokenRequestorId;
                result.PaymentCryptogram = details.PaymentCryptogram;

                if (details.MobilePOS != null)
                {
                    result.MobilePOS = new ProviderModels.MobilePOS()
                    {
                        CombinedTags = details.MobilePOS.CombinedTags,
                        TerminalId = details.MobilePOS.TerminalId,
                        TrackData = details.MobilePOS.TrackData,
                        TransactionReferenceKey = details.MobilePOS.TransactionReferenceKey
                    };

                    result.PaymentToken = details.MobilePOS.TrackData; // to avoid validation failure in underlying service
                }
            }

            return result;
        }

        public static Starbucks.Card.Provider.Common.Models.IRisk ToCardProvider(this Card.WebApi.Models.Risk risk, string market)
        {
            if (risk == null)
            {
                var cardRisk = new Starbucks.Card.Provider.Models.Risk
                {
                    Platform = string.Empty,
                    Market = market,
                    IsLoggedIn = DefaultIsLoggedIn,
                    CcAgentName = null
                };

                cardRisk.IovationFields = new Starbucks.Card.Provider.Models.Reputation
                {
                    IpAddress = null,
                    DeviceFingerprint = null
                };

                return cardRisk;
            }
            else
            {
                var cardRisk = new Starbucks.Card.Provider.Models.Risk
                {
                    Platform = risk.Platform ?? string.Empty,
                    Market = risk.Market ?? market,
                    IsLoggedIn = risk.IsLoggedIn ?? DefaultIsLoggedIn,
                    CcAgentName = risk.CcAgentName
                };

                cardRisk.IovationFields = null;
                if (risk.Reputation != null)
                {
                    cardRisk.IovationFields = new Starbucks.Card.Provider.Models.Reputation
                    {
                        IpAddress = risk.Reputation.IpAddress,
                        DeviceFingerprint = risk.Reputation.DeviceFingerprint
                    };
                }

                return cardRisk;
            }
        }

        public static Starbucks.Card.Provider.Common.Models.IRisk ToCardProvider(this Card.WebApi.Models.RiskWithoutIovation risk, string market)
        {
            if (risk == null)
            {
                var cardRisk = new Starbucks.Card.Provider.Models.Risk
                {
                    Platform = string.Empty,
                    Market = market,
                    IsLoggedIn = DefaultIsLoggedIn,
                    CcAgentName = null
                };

                cardRisk.IovationFields = null;

                return cardRisk;
            }
            else
            {
                var cardRisk = new Starbucks.Card.Provider.Models.Risk
                {
                    Platform = risk.Platform,
                    Market = risk.Market,
                    IsLoggedIn = risk.IsLoggedIn,
                    CcAgentName = risk.CcAgentName
                };

                cardRisk.IovationFields = null;

                return cardRisk;
            }
        }

        public static Starbucks.CardTransaction.Provider.Common.Model.IRisk ToProvider(this Card.WebApi.Models.Risk risk, string market)
        {
            if (risk == null)
            {
                Starbucks.CardTransaction.Provider.Common.Model.Risk providerRisk = new Starbucks.CardTransaction.Provider.Common.Model.Risk
                {
                    Platform = string.Empty,
                    Market = market,
                    IsLoggedIn = DefaultIsLoggedIn,
                    CcAgentName = null
                };

                providerRisk.IovationFields = new Starbucks.CardTransaction.Provider.Common.Model.Reputation
                {
                    IpAddress = null,
                    DeviceFingerprint = null
                };

                return providerRisk;
            }
            else
            {
                Starbucks.CardTransaction.Provider.Common.Model.Risk providerRisk = new Starbucks.CardTransaction.Provider.Common.Model.Risk
                {
                    Platform = risk.Platform ?? string.Empty,
                    Market = risk.Market ?? market,
                    IsLoggedIn = risk.IsLoggedIn ?? DefaultIsLoggedIn,
                    CcAgentName = risk.CcAgentName
                };

                providerRisk.IovationFields = null;
                if (risk.Reputation != null)
                {
                    providerRisk.IovationFields = new Starbucks.CardTransaction.Provider.Common.Model.Reputation
                    {
                        IpAddress = risk.Reputation.IpAddress,
                        DeviceFingerprint = risk.Reputation.DeviceFingerprint
                    };
                }

                return providerRisk;
            }
        }

        public static Starbucks.CardTransaction.Provider.Common.Model.IAddress ToProvider(this Address address)
        {
            if (address == null) { return null; }
            return new ProviderModels.Address
            {
                AddressLine1 = address.AddressLine1,
                AddressLine2 = address.AddressLine2,
                City = address.City,
                Country = address.Country,
                CountrySubdivision = address.CountrySubdivision,
                FirstName = address.FirstName,
                LastName = address.LastName,
                PhoneNumber = address.PhoneNumber,
                PostalCode = address.PostalCode
            };
        }

        private static PaymentTokenType? GetPaymenTokenType(string paymentTokenType)
        {
            PaymentTokenType tokenType;
            if (Enum.TryParse(paymentTokenType, true, out tokenType))
                return tokenType;
            return null;
        }

        public static IStarbucksCardHeader ToProvider(this StarbucksCardNumberAndPin cardNumberAndPin)
        {
            var cardHeader = new Starbucks.Card.Provider.Models.StarbucksCardHeader
            {
                CardNumber = cardNumberAndPin.CardNumber,
                CardPin = cardNumberAndPin.Pin,
                Nickname = cardNumberAndPin.Nickname,
                Primary = cardNumberAndPin.Primary,
                Submarket = cardNumberAndPin.Submarket,
                RegistrationSource = new Starbucks.Card.Provider.Models.CardRegistrationSource()
            };

            if (cardNumberAndPin.RegistrationSource != null)
            {
                cardHeader.RegistrationSource.Platform = cardNumberAndPin.RegistrationSource.Platform;
                cardHeader.RegistrationSource.Marketing = cardNumberAndPin.RegistrationSource.Marketing;
            }
            return cardHeader;
        }

        public static StarbucksCardUnregisteredResult ToApi(
            this IStarbucksCardUnregisterResult starbucksCardUnregisterResult)
        {
            return new StarbucksCardUnregisteredResult
            {
                CardId = starbucksCardUnregisterResult.CardId
            };
        }

        public static CardRegistrationStatus ToApi(this ICardRegistrationStatus cardRegistrationStatus)
        {
            return new CardRegistrationStatus
            {
                CardId = cardRegistrationStatus.CardId,
                CardNumber = cardRegistrationStatus.CardNumber,
                Code = cardRegistrationStatus.Code,
                Message = cardRegistrationStatus.Message,
                Successful = cardRegistrationStatus.Successful
            };
        }

        public static StarbucksCard ToApi(this ICard card)
        {
            return new StarbucksCard
            {
                //AutoReloadProfile = card.AutoReloadProfile != null ? card.AutoReloadProfile.ToApi() :null,
                Balance = card.Balance.ToCurrency(),
                BalanceCurrencyCode = card.BalanceCurrency,
                BalanceDate = card.BalanceDate,
                CardClass = card.Class,
                CardId = card.CardId,
                CardNumber = card.Number,
                CardType = card.Type.ToString(),
                //CurrencyCode = card.Currency,
                //ImageIconUrl = null,
                //ImageUrl = null,
                CardImages =
                        card.CardImages != null
                            ? card.CardImages.Select(ci => ci.ToApi())
                            : new List<StarbucksCardImage>(),
                //IsDefaultCard = card.IsDefaultCard.HasValue ? card.IsDefaultCard.Value : (bool?) null,
                IsDigitalCard = card.IsDigitalCard,
                //IsPartnerCard = card.IsPartnerCard.HasValue ? card.IsPartnerCard.Value : (bool?) null,
                //IsPartnerCard = card.IsPartner,
                //Nickname = card.Nickname,
                //PassbookSerialNumbers = card.PassbookSerialNumbers,
                //RegisteredUserId = string.Empty ,
                SubmarketCode = card.SubMarketCode,
                CurrencyCode = card.Currency
            };
        }

        private static decimal? ToCurrency(this decimal? val)
        {
            return val.HasValue ? val.Value.ToCurrency() : val;
        }

        private static decimal ToCurrency(this decimal val)
        {
            return decimal.Round(val, 2, MidpointRounding.AwayFromZero);
        }

        public static StarbucksCard ToApi(this IAssociatedCard card)
        {
            return new StarbucksCard
            {
                AutoReloadProfile = card.AutoReloadProfile != null ? card.AutoReloadProfile.ToApi() : null,
                Balance = card.Balance.ToCurrency(),
                BalanceCurrencyCode = card.BalanceCurrency,
                BalanceDate = card.BalanceDate,
                CardClass = card.Class,
                CardId = card.CardId,
                CardNumber = card.Number,
                CardType = card.Type.ToString(),
                //CurrencyCode = card.Currency,
                CardImages = card.CardImages != null ? card.CardImages.Select(ci => ci.ToApi()) : new List<StarbucksCardImage>(),
                IsDefaultCard = card.IsDefault,
                IsDigitalCard = card.IsDigitalCard,
                //IsPartnerCard = card.IsPartner.HasValue ? card.IsPartnerCard.Value : (bool?)null,
                IsPartnerCard = card.IsPartner,
                Nickname = card.Nickname,
                // PassbookSerialNumbers = card.PassbookSerialNumbers,
                //RegisteredUserId = card.RegisteredUserId,
                SubmarketCode = card.SubMarketCode,
                IsOwner = card.IsOwner,
                Actions = card.Actions,
                CurrencyCode = card.Currency
            };
        }

        public static StarbucksCardStatus ToApi(this IStarbucksCardStatus starbucksCardStatus)
        {
            return new StarbucksCardStatus
            {
                CardId = starbucksCardStatus.CardId,
                Valid = starbucksCardStatus.Valid
            };
        }

        public static StarbucksCardImage ToApi(this IStarbucksCardImage starbucksCardImage)
        {
            return new StarbucksCardImage
            {
                Type = starbucksCardImage.Type.ToApi(),
                Uri = starbucksCardImage.Uri
            };
        }

        public static CardImageType ToApi(this Starbucks.Card.Provider.Common.Enums.CardImageType imageType)
        {
            switch (imageType)
            {
                case Starbucks.Card.Provider.Common.Enums.CardImageType.ImageIcon:
                    return CardImageType.ImageIcon;
                case Starbucks.Card.Provider.Common.Enums.CardImageType.iosThumb:
                    return CardImageType.iosThumb;
                case Starbucks.Card.Provider.Common.Enums.CardImageType.ImageLarge:
                    return CardImageType.ImageLarge;
                case Starbucks.Card.Provider.Common.Enums.CardImageType.iosLargeHighRes:
                    return CardImageType.iosLargeHighRes;
                case Starbucks.Card.Provider.Common.Enums.CardImageType.ImageMedium:
                    return CardImageType.ImageMedium;
                case Starbucks.Card.Provider.Common.Enums.CardImageType.iosLarge:
                    return CardImageType.iosLarge;
                case Starbucks.Card.Provider.Common.Enums.CardImageType.ImageSmall:
                    return CardImageType.ImageSmall;
                case Starbucks.Card.Provider.Common.Enums.CardImageType.iosImageStripMedium:
                    return CardImageType.iosImageStripMedium;
                case Starbucks.Card.Provider.Common.Enums.CardImageType.iosImageStripLarge:
                    return CardImageType.iosImageStripLarge;
                case Starbucks.Card.Provider.Common.Enums.CardImageType.iosThumbHighRes:
                    return CardImageType.iosThumbHighRes;
                case Starbucks.Card.Provider.Common.Enums.CardImageType.androidFullHdpi:
                    return CardImageType.androidFullHdpi;
                case Starbucks.Card.Provider.Common.Enums.CardImageType.androidFullMdpi:
                    return CardImageType.androidFullMdpi;
                case Starbucks.Card.Provider.Common.Enums.CardImageType.androidFullXhdpi:
                    return CardImageType.androidFullXhdpi;
                case Starbucks.Card.Provider.Common.Enums.CardImageType.androidFullXxhdpi:
                    return CardImageType.androidFullXxhdpi;
                case Starbucks.Card.Provider.Common.Enums.CardImageType.androidThumbHdpi:
                    return CardImageType.androidThumbHdpi;
                case Starbucks.Card.Provider.Common.Enums.CardImageType.androidThumbMdpi:
                    return CardImageType.androidThumbMdpi;
                case Starbucks.Card.Provider.Common.Enums.CardImageType.androidThumbXhdpi:
                    return CardImageType.androidThumbXhdpi;
                case Starbucks.Card.Provider.Common.Enums.CardImageType.androidThumbXxhdpi:
                    return CardImageType.androidThumbXxhdpi;
                case Starbucks.Card.Provider.Common.Enums.CardImageType.ImageStrip:
                    return CardImageType.ImageStrip;
                default:
                    return CardImageType.ImageIcon;
            }
        }

        public static StarbucksCardBalance ToApi(this Starbucks.CardTransaction.Provider.Common.Model.ICardTransaction starbucksCardBalance)
        {
            return new StarbucksCardBalance
            {
                Balance = starbucksCardBalance.EndingBalance.ToCurrency(),
                BalanceCurrencyCode = starbucksCardBalance.Currency,
                BalanceDate = starbucksCardBalance.TransactionDate,
                CardId = starbucksCardBalance.CardId,
                CardNumber = starbucksCardBalance.CardNumber
            };
        }

        public static StarbucksCardBalance ToApi(this IStarbucksCardBalance starbucksCardBalance)
        {
            return new StarbucksCardBalance
            {
                Balance = starbucksCardBalance.Balance.ToCurrency(),
                BalanceCurrencyCode = starbucksCardBalance.BalanceCurrencyCode,
                BalanceDate = starbucksCardBalance.BalanceDate,
                CardId = starbucksCardBalance.CardId,
                CardNumber = starbucksCardBalance.CardNumber
            };
        }

        public static AutoReloadProfile ToApi(this IAutoReloadProfile reloadProfile)
        {
            return new AutoReloadProfile
            {
                Amount = reloadProfile.Amount.ToCurrency(),
                AutoReloadId = reloadProfile.AutoReloadId,
                AutoReloadType = reloadProfile.AutoReloadType,
                //  BillingAgreementId = reloadProfile.BillingAgreementId,
                CardId = reloadProfile.CardId,
                Day = reloadProfile.Day,
                DisableUntilDate = reloadProfile.DisableUntilDate,
                PaymentMethodId = reloadProfile.PaymentMethodId,
                //   PaymentType = reloadProfile.PaymentType,
                Status = GetAutoReloadProfileStatus(reloadProfile.Status),
                StoppedDate = reloadProfile.StoppedDate,
                TriggerAmount = reloadProfile.TriggerAmount.ToCurrency(),
                //UserId = reloadProfile.UserId
            };
        }

        public static PassbookCardInfo GetCardInfo(IAssociatedCard card)
        {
            if (card == null) return null;

            return new PassbookCardInfo
            {
                    UserId = card.RegisteredUserId,
                    Active = card.Active,
                    RegisteredUserId = card.RegisteredUserId,
                    Balance = card.Balance.ToCurrency(),
                    BalanceDate = card.BalanceDate,
                    BalanceCurrency = card.BalanceCurrency,
                    CardId = Encryption.DecryptCardId(card.CardId),
                    EncryptedCardId = card.CardId,
                    SubMarketCode = card.SubMarketCode,
                    Currency = card.Currency,
                    NickName = card.Nickname,
                    ImageSmall = card.CardImages == null ? String.Empty : card.CardImages.First(c => c.Type == Starbucks.Card.Provider.Common.Enums.CardImageType.ImageSmall).Uri,
                    ImageLarge = card.CardImages == null ? String.Empty : card.CardImages.First(c => c.Type == Starbucks.Card.Provider.Common.Enums.CardImageType.ImageLarge).Uri,
                    CardNumber = card.Number,
                    IsCardRegistered = card.RegistrationDate.HasValue,
                    Name = card.Name,
                };
        }

        public static PassbookFavoriteLocation ToFavoriteStore(Store store, IFavoriteStore favoriteStore)
        {
            if (store == null)
            {
                return null;
            }

            return new PassbookFavoriteLocation
            {
                Latitude = Convert.ToDouble(store.Coordinates.Latitude),
                Longitude = Convert.ToDouble(store.Coordinates.Longitude),
                RelevantText = store.Name,
                InternalId = favoriteStore.StoreId.ToString()
            };
        }

        public static IStarbucksCardHeader ToCardHeader(this StarbucksCardNumberAndPin card)
        {
            if (card.RegistrationSource == null)
            {
                card.RegistrationSource = new Card.WebApi.Models.CardRegistrationSource();
            }

            Starbucks.Card.Provider.Models.StarbucksCardHeader starbucksCardHeader = new Starbucks.Card.Provider.Models.StarbucksCardHeader
            {
                CardNumber = card.CardNumber,
                CardPin = card.Pin,
                Nickname = card.Nickname,
                Submarket = card.Submarket,
                Primary = card.Primary,
                RegistrationSource = new Starbucks.Card.Provider.Models.CardRegistrationSource { Marketing = card.RegistrationSource.Marketing, Platform = card.RegistrationSource.Platform }
            };

            if (card.risk != null)
            {
                starbucksCardHeader.Risk = new Starbucks.Card.Provider.Models.RiskWithoutIovation();
                starbucksCardHeader.Risk.CcAgentName = card.risk.CcAgentName;
                starbucksCardHeader.Risk.IsLoggedIn = card.risk.IsLoggedIn;
                starbucksCardHeader.Risk.Market = card.risk.Market;
                starbucksCardHeader.Risk.Platform = card.risk.Platform;
            }
            else
            {
                starbucksCardHeader.Risk = new Starbucks.Card.Provider.Models.RiskWithoutIovation();
                starbucksCardHeader.Risk.CcAgentName = null;
                starbucksCardHeader.Risk.IsLoggedIn = DefaultIsLoggedIn;
                starbucksCardHeader.Risk.Market = card.Submarket;
                starbucksCardHeader.Risk.Platform = string.Empty;
            }

            return starbucksCardHeader;
        }

        public static IStarbucksCardHeader ToProvider(this StarbucksCardHeader card, string market)
        {
            if (card.RegistrationSource == null)
            {
                card.RegistrationSource = new Card.WebApi.Models.CardRegistrationSource();
            }

            Starbucks.Card.Provider.Models.StarbucksCardHeader starbucksCardHeader = new Starbucks.Card.Provider.Models.StarbucksCardHeader
            {
                CardNumber = card.CardNumber,
                CardPin = card.Pin,
                Nickname = card.Nickname,
                Submarket = card.Submarket,
                Primary = card.Primary,
                RegistrationSource = new Starbucks.Card.Provider.Models.CardRegistrationSource { Marketing = card.RegistrationSource.Marketing, Platform = card.RegistrationSource.Platform }
            };

            if (card.risk != null)
            {
                starbucksCardHeader.Risk = new Starbucks.Card.Provider.Models.RiskWithoutIovation();
                starbucksCardHeader.Risk.CcAgentName = card.risk.CcAgentName;
                starbucksCardHeader.Risk.IsLoggedIn = card.risk.IsLoggedIn;
                starbucksCardHeader.Risk.Market = card.risk.Market;
                starbucksCardHeader.Risk.Platform = card.risk.Platform;
            }
            else
            {
                starbucksCardHeader.Risk = new Starbucks.Card.Provider.Models.RiskWithoutIovation();
                starbucksCardHeader.Risk.CcAgentName = null;
                starbucksCardHeader.Risk.IsLoggedIn = DefaultIsLoggedIn;
                starbucksCardHeader.Risk.Market = market;
                starbucksCardHeader.Risk.Platform = string.Empty;
            }


            return starbucksCardHeader;
        }

        public static IStarbucksCardHeader ToProvider(this AssociateCardHeader card, string market)
        {
            if (card.RegistrationSource == null)
            {
                card.RegistrationSource = new Card.WebApi.Models.CardRegistrationSource();
            }

            return new Starbucks.Card.Provider.Models.StarbucksCardHeader
            {
                CardNumber = card.CardNumber,
                Nickname = card.Nickname,
                Submarket = card.Submarket,
                Risk = card.Risk.ToCardProvider(market),
                RegistrationSource = new Starbucks.Card.Provider.Models.CardRegistrationSource { Marketing = card.RegistrationSource.Marketing, Platform = card.RegistrationSource.Platform }
            };
        }
    }
}