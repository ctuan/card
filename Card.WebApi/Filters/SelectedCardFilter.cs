﻿using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Card.WebApi.Filters
{
    public class SelectedCardFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var controller = actionContext.ControllerContext.Controller as ApiController;

            base.OnActionExecuting(actionContext);
        }
    }
}