﻿using System.Net;
using System.Web.Http.Filters;
using Card.WebApi.Models;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;

namespace Card.WebApi.Filters
{
    public class ReloadCardByCardNumberPinFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var actionArguments = actionContext.ActionArguments;
            var number = actionArguments.ContainsKey("cardNumber") ? actionArguments["cardNumber"].ToString() : string.Empty;
            var pin = actionArguments.ContainsKey("pin") ? actionArguments["pin"].ToString() : string.Empty;
            var reloadForStarbucksCard = actionArguments.ContainsKey("reloadForStarbucksCard") ? actionArguments["reloadForStarbucksCard"] as ReloadForStarbucksCard : null;
            
            if (reloadForStarbucksCard == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.ValidationNoRequestCode,
                                       CardWebAPIErrorResource.ValidationNoRequestMessage);
            }
          

            base.OnActionExecuting(actionContext);
        }
    }
}