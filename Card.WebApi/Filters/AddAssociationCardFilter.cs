﻿using System.Net;
using System.Web.Http.Filters;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;

namespace Card.WebApi.Filters
{
    public class AddAssociationCardFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var actionArguments = actionContext.ActionArguments;

            var cardHeader = actionArguments.ContainsKey("associateCardHeader") ? actionArguments["associateCardHeader"] as Models.AssociateCardHeader : null;
            var userId = actionArguments.ContainsKey("userId") ? actionArguments["userId"].ToString() : string.Empty;

            if (cardHeader == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.ValidationNoRequestCode,
                                       CardWebAPIErrorResource.ValidationNoRequestMessage);
            }
            
            base.OnActionExecuting(actionContext);
        }
    }
}
