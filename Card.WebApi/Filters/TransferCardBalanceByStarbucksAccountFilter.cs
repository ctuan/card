﻿using System.Net;
using System.Web.Http.Filters;
using Card.WebApi.Models;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;

namespace Card.WebApi.Filters
{
    public class TransferCardBalanceByStarbucksAccountFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var actionArguments = actionContext.ActionArguments;
            //var userId = actionArguments.ContainsKey("userId") ? actionArguments["userId"].ToString() : string.Empty;
            var balanceTransfer = actionArguments.ContainsKey("balanceTransfer")
                ? actionArguments["balanceTransfer"] as BalanceTransfer: null;

            if (balanceTransfer == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.ValidationNoRequestCode,
                                       CardWebAPIErrorResource.ValidationNoRequestMessage);
            }          

            base.OnActionExecuting(actionContext);
        }
    }
}