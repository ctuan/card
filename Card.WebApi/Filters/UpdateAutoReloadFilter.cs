﻿using System;
using System.Net;
using System.Web.Http.Filters;
using Card.WebApi.Models;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;

namespace Card.WebApi.Filters
{
    public class UpdateAutoReloadFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var actionArguments = actionContext.ActionArguments;         
            var autoReloadProfile = actionArguments.ContainsKey("autoReloadProfile")
                                        ? actionArguments["autoReloadProfile"] as AutoReloadProfile
                                        : null;      

            if (autoReloadProfile == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest,
                                       CardWebAPIErrorResource.ValidationNoRequestCode ,
                                       CardWebAPIErrorResource.ValidationNoRequestMessage);

            }
            if (autoReloadProfile.TriggerAmount.HasValue && autoReloadProfile.TriggerAmount.Value > 101)
            {
                autoReloadProfile.TriggerAmount = 10.0M;
                autoReloadProfile.Status = "disabled";
            }


            //if (string.IsNullOrEmpty(autoReloadProfile.Status))
            //    throw new ApiException(HttpStatusCode.BadRequest,
            //                           CardWebAPIErrorResource.ValidationNoAutoReloadStatusCode,
            //                           CardWebAPIErrorResource.ValidationNoAutoReloadStatusMessage);

            //if (!autoReloadProfile.Status.Equals("active", StringComparison.CurrentCultureIgnoreCase) && !autoReloadProfile.Status.Equals("disabled", StringComparison.CurrentCultureIgnoreCase))
            //    throw new ApiException(HttpStatusCode.BadRequest,
            //                           CardWebAPIErrorResource.ValidationNoAutoReloadStatusCode,
            //                           CardWebAPIErrorResource.ValidationNoAutoReloadStatusMessage);


            base.OnActionExecuting(actionContext);
        }
    }
}