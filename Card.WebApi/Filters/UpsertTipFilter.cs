﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http.Filters;
using Card.WebApi.Models.Tipping;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;

namespace Card.WebApi.Filters
{
    public class UpsertTipFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var actionArguments = actionContext.ActionArguments;
            var upsertTip = actionArguments.ContainsKey("tipRequest")
                                        ? actionArguments["tipRequest"] as UpsertTipRequest
                                        : null;

            if (upsertTip == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest,
                                       CardWebAPIErrorResource.ValidationNoRequestCode,
                                       CardWebAPIErrorResource.ValidationNoRequestMessage);
            }


            base.OnActionExecuting(actionContext);
        }
         
    }
}