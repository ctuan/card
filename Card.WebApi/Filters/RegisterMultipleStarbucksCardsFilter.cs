﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http.Filters;
using Card.WebApi.Models;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;

namespace Card.WebApi.Filters
{
    public class RegisterMultipleStarbucksCardsFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var actionArguments = actionContext.ActionArguments;                      
            var cards = actionArguments.ContainsKey("starbucksCardNumberAndPins")
                            ? actionArguments["starbucksCardNumberAndPins"] as IEnumerable<StarbucksCardNumberAndPin>
                            : null;          
            
            if (cards == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.ValidationNoRequestCode,
                                       CardWebAPIErrorResource.ValidationNoRequestMessage);
            }
            
            base.OnActionExecuting(actionContext);
        }
    }
}