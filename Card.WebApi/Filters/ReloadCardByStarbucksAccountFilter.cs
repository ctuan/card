﻿using System.Net;
using System.Web.Http.Filters;
using Card.WebApi.Models;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;

namespace Card.WebApi.Filters
{
    public class ReloadCardByStarbucksAccountFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var actionArguments = actionContext.ActionArguments;
            //var cardId = actionArguments.ContainsKey("cardId") ? actionArguments["cardId"].ToString() : string.Empty;
            //var userId = actionArguments.ContainsKey("userId") ? actionArguments["userId"].ToString() : string.Empty;
            var reloadForStarbucksCard = actionArguments.ContainsKey("reloadForStarbucksCard") ? actionArguments["reloadForStarbucksCard"] as ReloadForStarbucksCard : null;
           
            if (reloadForStarbucksCard == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.ValidationNoRequestCode,
                                       CardWebAPIErrorResource.ValidationNoRequestMessage);
            }
           

            base.OnActionExecuting(actionContext);
        }
    }
}