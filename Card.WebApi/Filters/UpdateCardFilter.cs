﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http.Filters;
using Card.WebApi.Models;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;

namespace Card.WebApi.Filters
{
    public class UpdateCardFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var actionArguments = actionContext.ActionArguments;
            var autoReloadProfile = actionArguments.ContainsKey("updateCardRequest")
                                        ? actionArguments["updateCardRequest"] as UpdateCardRequest
                                        : null;

            if (autoReloadProfile == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest,
                                       CardWebAPIErrorResource.ValidationNoRequestCode,
                                       CardWebAPIErrorResource.ValidationNoRequestMessage);
            }


            base.OnActionExecuting(actionContext);
        }
    }
}