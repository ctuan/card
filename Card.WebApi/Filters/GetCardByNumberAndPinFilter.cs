﻿using System.Net;
using System.Web.Http.Filters;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;

namespace Card.WebApi.Filters
{
    public class GetCardByNumberAndPinFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var actionArguments = actionContext.ActionArguments;
            var number = actionArguments.ContainsKey("cardNumber") ? actionArguments["cardNumber"].ToString() : string.Empty;
            var pin = actionArguments.ContainsKey("pin") ? actionArguments["pin"].ToString() : string.Empty;

            if (string.IsNullOrEmpty(number))
            {
                throw new ApiException(HttpStatusCode.BadRequest,
                                       CardWebAPIErrorResource.ValidationCardNumberMissingCode,
                                       CardWebAPIErrorResource.ValidationCardNumberMissingMessage);
            }

            if (number.Length != 16)
            {
                throw new ApiException(HttpStatusCode.NotFound,
                                       CardWebAPIErrorResource.ValidationCardNumberMissingCode,
                                       CardWebAPIErrorResource.ValidationCardNumberMissingMessage);
            }

            if (string.IsNullOrEmpty(pin))
            {
                throw new ApiException(HttpStatusCode.BadRequest,
                                       CardWebAPIErrorResource.ValidationCardPinMissingCode,
                                       CardWebAPIErrorResource.ValidationCardPinMissingMessage);
            }

            if (pin.Length != 8)
            {
                throw new ApiException(HttpStatusCode.BadRequest,
                                       CardWebAPIErrorResource.ValidationCardPinMissingCode,
                                       CardWebAPIErrorResource.ValidationCardPinMissingMessage);
            }

            base.OnActionExecuting(actionContext);
        }
    }
}