﻿using System.Net;
using System.Web.Http.Filters;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;

namespace Card.WebApi.Filters
{
    public class RegisterCardFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var actionArguments = actionContext.ActionArguments;
           
            var cardNumberAndPin = actionArguments.ContainsKey("starbucksCardNumberAndPin") ? actionArguments["starbucksCardNumberAndPin"] as Models.StarbucksCardNumberAndPin : null;
            var userId = actionArguments.ContainsKey("userId") ? actionArguments["userId"].ToString() : string.Empty;

            if (cardNumberAndPin == null)
            {
                throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.ValidationNoRequestCode,
                                       CardWebAPIErrorResource.ValidationNoRequestMessage);
            }
            
            base.OnActionExecuting(actionContext);
        }
    }
}