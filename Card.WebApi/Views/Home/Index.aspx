﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Card Open Api</title>
</head>

<body>
    <div>
        <b>Card Open api</b>
        <hr />
    </div>
    <div>
	    <ul>
			<li>
				<h3>Machine</h3>
				<p><%=System.Environment.MachineName %></p>
			</li>
			<li>
				<h3>Deployment</h3>
				<p><%=ViewData["Deployment"]%> </p>
			</li>	
            
            <% 
                var hasDetailString = ViewData["hasDetail"];
                var hasDetail = hasDetailString.Equals("True");
                
                if (hasDetail)
                { 
                %>
<%--			            <li>
				            <p><%=ViewData["paymentService"]%> </p>
			            </li>	
                        <li>
				            <p><%=ViewData["dataSafe"]%> </p>
			            </li>		
			            <li>
				            <p><%=ViewData["SoapEndpoints"]%> </p>
			            </li>
			            <li>
				            <p><%=ViewData["ConnectionStrings"]%> </p>
			            </li>--%>

            <%
            }
             %>
	    </ul>
    </div>
</body>
</html>
