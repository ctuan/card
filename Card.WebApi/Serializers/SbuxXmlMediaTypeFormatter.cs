﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Xml;
using Starbucks.OpenApi.WebApi.Common.Filters;
using Starbucks.OpenApi.WebApi.Common.Serializer;

namespace Card.WebApi.App_Start
{
    public class SbuxXmlMediaTypeFormatter : XmlMediaTypeFormatter
    {
        public SbuxXmlWriterSettings SbuxXmlWriterSettings { get; set; }

        HttpRequestMessage _request;

        /// <summary>
        /// Need to use this in combination with SBuxSerializer. 
        /// Using this MediaTypeFormatter will replace the inbox json and/or xml MediaTypeFormatter 
        /// http://digitalarchitecture.starbucks.net/index.php?title=Web_Api_Common#Developer_guidelines
        /// </summary>
        public SbuxXmlMediaTypeFormatter()
        {
            SbuxXmlWriterSettings = new SbuxXmlWriterSettings()
                {
                    XmlWriterSettings = new XmlWriterSettings()
                        {
                            CloseOutput = false,
                            OmitXmlDeclaration = true
                        },
                    RootNamespaceSuffix = SbuxConvert.Settings.SourceNamespace
                };
        }

        public override MediaTypeFormatter GetPerRequestFormatterInstance(Type type, System.Net.Http.HttpRequestMessage request, MediaTypeHeaderValue mediaType)
        {
            var ret = new SbuxXmlMediaTypeFormatter()
                {
                    _request = request
                };

            ret.SbuxXmlWriterSettings.RootNamespace = this.SbuxXmlWriterSettings.RootNamespace;
            ret.SbuxXmlWriterSettings.RootNamespaceSuffix = this.SbuxXmlWriterSettings.RootNamespaceSuffix;
            ret.SbuxXmlWriterSettings.XmlWriterSettings = this.SbuxXmlWriterSettings.XmlWriterSettings;

            return ret;
        }

#if DEBUG
        public override void SetDefaultContentHeaders(Type type, HttpContentHeaders headers, MediaTypeHeaderValue mediaType)
        {
            base.SetDefaultContentHeaders(type, headers, mediaType);

            if (SbuxConvert.IsSerializable(type))
                headers.Add("Serializer", "SBux-Xml");
            else
                headers.Add("Serializer", "Inbox-Xml");
        }
#endif

        public override Task WriteToStreamAsync(Type type, object value, Stream writeStream, HttpContent contentHeaders, TransportContext transportContext)
        {
            if (SbuxConvert.IsSerializable(type))
            {
                return Task.Factory.StartNew(() =>
                    {
                        FieldsAdaptorData selectorData = FieldsAdaptorManager.GetSelectorData(_request);
                        SerializeContext fieldsAdaptor = null;

                        switch (selectorData.Type)
                        {
                            case FieldsAdaptorTypeEnum.SingleLevel:
                                fieldsAdaptor = new SingleLevelFieldsAdaptor(selectorData.RootType, selectorData.Mask, selectorData.Logic);
                                break;
                            case FieldsAdaptorTypeEnum.MultiLevel:
                                fieldsAdaptor = new MultiLevelFieldsAdaptor(selectorData.RootType, selectorData.Filters, selectorData.Logic);
                                break;
                            default:
                                fieldsAdaptor = new SerializeContext(); // no fields adaptor
                                break;
                        };

                        SbuxXmlWriterSettings.XmlWriterSettings.CloseOutput = false;
                        SbuxXmlWriterSettings.XmlWriterSettings.OmitXmlDeclaration = true;
                        SbuxXmlWriterSettings.XmlWriterSettings.Encoding = SelectCharacterEncoding(contentHeaders == null ? null : contentHeaders.Headers);

                        using (var serializer = new SbuxXmlSerializer(writeStream, fieldsAdaptor, SbuxXmlWriterSettings))
                        {
                            SbuxConvert.SerializeObject(type, value, serializer);
                        }

                    });
            }
            else
            {
                if (value != null)
                    type = value.GetType();
                return base.WriteToStreamAsync(type, value, writeStream, contentHeaders, transportContext);
            }
        }

    }
}