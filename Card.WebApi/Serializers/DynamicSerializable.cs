﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Card.WebApi.Serializers
{
    [Serializable]
    public class DynamicSerializable : DynamicObject, ISerializable
    {
        private readonly Dictionary<string, object> _fieldCollection = new Dictionary<string, object>();

        public DynamicSerializable()
        {
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            return (_fieldCollection.TryGetValue(binder.Name, out result));
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            _fieldCollection.Add(binder.Name, value);
            return true;
        }

        public object this[string prop]
        {
            get
            {
                object value;
                return _fieldCollection.TryGetValue(prop, out value) ? value : null;
            }
            set { _fieldCollection[prop] = value; }
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            foreach (KeyValuePair<string, object> kvp in _fieldCollection)
            {
                info.AddValue(kvp.Key, kvp.Value);
            }
        }

        protected DynamicSerializable(SerializationInfo info, StreamingContext context)
        {
            // TODO: validate inputs before deserializing. See http://msdn.microsoft.com/en-us/library/ty01x675(VS.80).aspx
            foreach (SerializationEntry entry in info)
            {
                _fieldCollection.Add(entry.Name, entry.Value);
            }
        }
    }
}