﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Web.Http.Controllers;
using System.Web.Http.Metadata;
using Card.WebApi.Helpers;
using Card.WebApi.Models;
using Starbucks.OpenApi.IpAddress.Extensions;
using Starbucks.OpenApi.IpAddress.Helpers;
using Starbucks.OpenApi.IpAddress.HttpParameterTransforms;
using Starbucks.OpenApi.Logging.Common;

namespace Card.WebApi.HttpParameterTransforms
{
    public class ModelWithRiskTransform : IHttpParameterTransform
    {
        private readonly IIpAddressUtility _ipAddressUtility;
        private readonly ILogRepository _logRepository;

        public ModelWithRiskTransform(IIpAddressUtility ipAddressUtility, ILogRepository logRepository)
        {
            _ipAddressUtility = ipAddressUtility;
            _logRepository = logRepository;
        }

        public object Transform(HttpParameterDescriptor descriptor, ModelMetadataProvider metadataProvider, HttpActionContext actionContext, object model)
        {
            return Transform(descriptor, actionContext, (IModelWithRisk)model);
        }

        private IModelWithRisk Transform(HttpParameterDescriptor descriptor, HttpActionContext actionContext, IModelWithRisk modelWithRisk)
        {
            if (modelWithRisk == null)
                return null;

            var clientIpAddressFromXff = new Lazy<IPAddress>(() => _ipAddressUtility.ResolveClientIp(actionContext.Request.Headers));

            Func<Risk, string> getRiskIpAddress = r => r != null && r.Reputation != null
                ? r.Reputation.IpAddress ?? string.Empty
                : string.Empty;

            var risk = modelWithRisk.RiskFields;

            var origRiskIpAddress = getRiskIpAddress(risk);

            if (risk == null)
            {
                modelWithRisk.RiskFields = risk = new Risk()
                {
                    // create default Risk.Reputation () object.  
                    // old client calls this api without passing in the risk object,
                    // we create default risk.Reputation.   So that, PSApi can call iovation with client ipAddress.
                    Reputation = new Reputation()
                    {
                        DeviceFingerprint = null,
                    },
                };
            }
            else
            {
                // validate the Platform field on existing Risk object
                PlatformValidator.ValidatePlatformString(risk);
            }

            if (risk.Reputation == null)
            {
                // if client calls this api without risk.Reputation object, it signals that it wants to bypass iovation.
                // Therefore, we don't need to create risk.Reputation object when calling PS Api.
            }
            else
            {
                // resolve IpAddress on existing Reputation object

                var riskIpAddress = _ipAddressUtility.ToIPAddress(risk.Reputation.IpAddress);

                var clientIpAddress = _ipAddressUtility.ResolveClientIp(riskIpAddress) ?? clientIpAddressFromXff.Value ?? riskIpAddress;

                risk.Reputation.IpAddress = _ipAddressUtility.ToIpString(clientIpAddress);
            }

            _logRepository.Log(
                "APILog",
                "ModelWithRiskTransform",
                "Successfully transformed IModelWithRisk HttpParameter",
                TraceEventType.Information,
                new Dictionary<string, string>
                {
                    { "RequestMethod", actionContext.Request.Method.Method },
                    { "RequestUri", actionContext.Request.RequestUri.PathAndQuery },
                    { "XForwardedFor", actionContext.Request.Headers.GetHeaderValue("X-Forwarded-For") },
                    { "ClientIpAddressFromXFF", clientIpAddressFromXff.IsValueCreated ? _ipAddressUtility.ToIpString(clientIpAddressFromXff.Value) : "<not evaluated>" },
                    { "Role", "OpenApi" },
                    { "Service", "Card" },
                    { "Controller", actionContext.ActionDescriptor.ControllerDescriptor.ControllerName },
                    { "Action", actionContext.ActionDescriptor.ActionName },
                    { "ParameterName", descriptor.ParameterName },
                    { "ParameterType", descriptor.ParameterType.FullName },
                    { "OriginalRiskIpAddress", origRiskIpAddress },
                    { "ResolvedRiskIpAddress", getRiskIpAddress(risk) }
                });

            return modelWithRisk;
        }
    }
}
