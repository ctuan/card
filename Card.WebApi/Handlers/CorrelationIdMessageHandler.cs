﻿using Card.WebApi.Logging;
using System;
using System.Linq;
using System.Net.Http;
using System.Runtime.Remoting.Messaging;

namespace Card.WebApi.Handlers
{
    /// <summary>
    /// This handler should be first in line.
    /// It captures a correlationId from the http request,
    /// and makes the value available to custom log formatters.
    /// This serves as a substitute for the CorrelationManager / ActivityId combination,
    /// which loses continuity under async + await context switching. 
    /// </summary>
    public class CorrelationIdMessageHandler : DelegatingHandler
    {

        protected override System.Threading.Tasks.Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            System.Threading.CancellationToken cancellationToken)
        {   
            // Capture and hold value (a GUID) as a pseudo ActivityId.
            var correlationId = request.GetCorrelationId();
            CallContext.LogicalSetData(LoggingResources.CorrelationIdCallContextKey, correlationId);

            // Adding api URL and Payload for auditing.
            var requestMethod = request.Method.ToString();
            var requestUrl = request.RequestUri;
            var requestContentString = (request.Content == null) ? "" : request.Content.ReadAsStringAsync().Result;
            var requestContentLength = 0;
            if (requestContentString != null)
                requestContentLength = requestContentString.Length;

            var requestContent = "Request content cannot be logged for security concerns. Potentially contains personal payment confidential data. Request content size is " + requestContentLength;

            var apiCommand = string.Format("verb: {0} url: {1} content: {2}", requestMethod, requestUrl, requestContent);
            CallContext.LogicalSetData(LoggingResources.ApiCommandCallContextKey, apiCommand);

            return base.SendAsync(request, cancellationToken);
        }
    }
}