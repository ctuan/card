﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Card.WebApi.Models;
using Newtonsoft.Json;
using Starbucks.OpenApi.Utilities.Common;

namespace Card.WebApi.Helpers
{
    public class LocationHelper : ILocationHelper
    {
        private readonly ILocationSettings _settings;

        public LocationHelper(ILocationSettings settings)
        {
            _settings = Guard.EnsureArgumentIsNotNull(settings, "settings");
        }

        public virtual Store GetStoreById(int storeId)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = GetResponseFromClient(client, String.Format(_settings.StoreUri, storeId));
            return GetStoreInternal(response);
        }

        private Store GetStoreInternal(HttpResponseMessage response)
        {
            Store store = null;

            if (response.IsSuccessStatusCode)
            {
                var jsonContent = GetContentFromResponse(response);
                store = JsonConvert.DeserializeObject<Store>(jsonContent);
            }

            return store;
        }

        private HttpResponseMessage GetResponseFromClient(HttpClient client, string requestUri)
        {
            Task<HttpResponseMessage> contentTask = client.GetAsync(requestUri);
            contentTask.Wait();
            return contentTask.Result;
        }

        private string GetContentFromResponse(HttpResponseMessage response)
        {
            Task<string> contentTask = response.Content.ReadAsStringAsync();
            contentTask.Wait();
            return contentTask.Result;
        }
    }
}