﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Card.WebApi.Models;
using Starbucks.OpenApi.Utilities.Common;
using Starbucks.OpenApi.Utilities.Common.Helpers;

namespace Card.WebApi.Helpers
{
    public class PassbookService : IPassbookService
    {
        private readonly ILogCounterWrapper _logCounterWrapper = null;
        private readonly ILogHelper _logHelper = null;
        private readonly string _passbookCreatePassbookUri = null;
        private static HttpClient SharedClient
        {
            get
            {
                if (_sharedClient != null)
                    return _sharedClient;

                int _iPhoneTimeOutMilliSeconds = 20000; //hard coded in iPhone application
                if (ConfigurationManager.AppSettings["iPhoneTimeOutMilliSeconds"] != null)
                {
                    int.TryParse(ConfigurationManager.AppSettings["iPhoneTimeOutMilliSeconds"].ToString(), out _iPhoneTimeOutMilliSeconds);
                }
                _sharedClient = new HttpClient();
                _sharedClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // 20 seconds is currently the timeout in the iPhone application calling code.                
                _sharedClient.Timeout = TimeSpan.FromSeconds(_iPhoneTimeOutMilliSeconds);
                return _sharedClient;
            }
        }
        private static HttpClient _sharedClient = null;
        public PassbookService(ILogCounterWrapper logCounterWrapper,
            ILogHelper logHelper)
        {
            _logCounterWrapper = Guard.EnsureArgumentIsNotNull(logCounterWrapper, "logCounterWrapper");
            _logHelper = Guard.EnsureArgumentIsNotNull(logHelper, "logHelper");
            _passbookCreatePassbookUri = ConfigurationManager.AppSettings["PassbookCreateUrl"];
            Guard.EnsureArgumentIsNotNullOrEmpty(_passbookCreatePassbookUri, "PassbookCreateUrl");
        }

        /// <summary>
        /// For post to Passbook we use structures to reduce visual complexity
        /// </summary>
        /// <param name="cardInfo"></param>
        /// <param name="deviceId"></param>
        /// <param name="favoriteStores"></param>
        /// <param name="localeInfo"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> CreatePkPass(PassbookCardInfo cardInfo, string deviceId, List<PassbookFavoriteLocation> favoriteStores, PassbookLocaleInfo localeInfo)
        {
            Guard.EnsureArgumentIsNotNull(cardInfo, "cardInfo");
            Guard.EnsureArgumentIsNotNull(deviceId, "deviceId");

            _logCounterWrapper.Increment("Card_CreatePkPass");
            // This is an internal call and likely would not need retry...
            //  BUT it is a call and should have extra protection (1st call seen to fail in Test environment triggered this change)
            var retries = 3;
            while (retries > 0)
            {
                try
                {
                    _logCounterWrapper.Increment("Passbook_CreatePkPass");
                    var model = new
                    {
                        cardInfo = cardInfo,
                        favoriteStores = favoriteStores,
                        deviceId = deviceId,
                        localeInfo = localeInfo
                    };
                    return await SharedClient.PostAsJsonAsync(_passbookCreatePassbookUri, model);
                }
                catch (Exception exc)
                {
                    _logCounterWrapper.Increment("Card_CreatePkPass_Error");
                    _logHelper.WriteErrorLog("Card_CreatePkPass_Error", string.Format("Retry remainings {0}", retries), exc);
                    retries--;
                }
            }
            return null;
        }
    }
}