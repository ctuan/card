﻿using System.Net;
using System.Collections.Generic;
using Card.WebApi.Models;
using Starbucks.CardIssuer.Dal.SvDot.Models;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;

namespace Card.WebApi.Helpers
{
    public class PlatformValidator
    {
        public static void ValidatePlatformString(RiskWithoutIovation risk)
        {
            if (risk != null)
            {
                string platform = risk.Platform;
                if (platform != null)
                    platform = platform.Trim();

                if (string.IsNullOrEmpty(platform))
                {
                    throw new ApiException(HttpStatusCode.BadRequest, CardWebAPIErrorResource.InvalidPlatformCode, CardWebAPIErrorResource.InvalidPlatformMessage);
                }
            }
        }
    }
}