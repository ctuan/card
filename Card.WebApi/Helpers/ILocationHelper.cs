﻿using Card.WebApi.Models;

namespace Card.WebApi.Helpers
{
    public interface ILocationHelper
    {
        Store GetStoreById(int storeId);
    }
}
