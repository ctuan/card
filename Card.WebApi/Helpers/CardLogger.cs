﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Starbucks.CardIssuer.Dal.SvDot;

namespace Card.WebApi.Helpers
{
    public class CardLogger : ICardLogger
    {
        public void LogCard(Starbucks.CardIssuer.Dal.SvDot.Models.ICardLog cardLog, string errorMessage, System.Diagnostics.TraceEventType eventType)
        {
            var logEntry = new LogEntry
                {
                    ActivityId = Guid.NewGuid(),
                    Title = "SVC_Transaction_Update",
                    Severity = eventType,
                    Message = errorMessage
                };
            logEntry.ExtendedProperties.Add("__Type", "CardError");
            logEntry.ExtendedProperties.Add("UserId", cardLog.UserId);
            logEntry.ExtendedProperties.Add("CardId", cardLog.CardId);
            logEntry.ExtendedProperties.Add("CardNumber", cardLog.CardNumber);

            Logger.Write(logEntry);
        }
    }
}