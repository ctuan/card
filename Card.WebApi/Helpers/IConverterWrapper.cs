﻿using Card.WebApi.Models;
using Starbucks.Card.Provider.Common.Models;


namespace Card.WebApi.Helpers
{
    public interface IConverterWrapper
    {
        PassbookCardInfo GetCardInfo(IAssociatedCard card);
    }
}