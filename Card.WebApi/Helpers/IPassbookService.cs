﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Card.WebApi.Models;

namespace Card.WebApi.Helpers
{
    public interface IPassbookService
    {
        Task<HttpResponseMessage> CreatePkPass(PassbookCardInfo cardInfo, string deviceId, List<PassbookFavoriteLocation> favoriteStores, PassbookLocaleInfo localeInfo);
    }
}