﻿using System;

namespace Card.WebApi.Helpers
{
    public interface ICardInfo
    {
        string UserId { get; set; }
        bool Active { get; set; }
        string RegisteredUserId { get; set; }
        decimal? Balance { get; set; }
        DateTime? BalanceDate { get; set; }
        string BalanceCurrency { get; set; }
        int CardId { get; set; }
        string SubMarketCode { get; set; }
        string Currency { get; set; }
        string NickName { get; set; }
        string ImageSmall { get; set; }
        string ImageLarge { get; set; }
        string CardNumber { get; set; }
        string EncryptedCardId { get; set; }
        bool IsCardRegistered { get; set; }
        string Name { get; set; }
    }
}