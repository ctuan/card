﻿namespace Card.WebApi.Helpers
{
    public interface ILocationSettings
    {
        string StoreUri { get; set; }
    }
}