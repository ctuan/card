﻿using System.Collections.Generic;
using Starbucks.CardIssuer.Dal.SvDot.Models;

namespace Card.WebApi.Helpers
{
    public class CardLog : ICardLog
    {
        public string UserId { get; set; }

        public string CardNumber { get; set; }

        public string Message { get; set; }

        public string Submarket { get; set; }

        public IDictionary<string, string> ParameterCollection { get; set; }

        public string CardId { get; set; }

        public string CardName { get; set; }

        public decimal Amount { get; set; }

        public string TransactionId { get; set; }
    }
}