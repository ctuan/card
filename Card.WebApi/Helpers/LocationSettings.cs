﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Card.WebApi.Helpers
{
    public class LocationSettings : ConfigurationSection, ILocationSettings
    {
        [ConfigurationProperty("storeUri", IsRequired = true)]
        public string StoreUri
        {
            get
            {
                return this["storeUri"].ToString();
            }
            set
            {
                this["storeUri"] = value;
            }
        }
    }
}