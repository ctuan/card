﻿using System;
using System.Net;
using Account.Provider.Common.Models;
using Card.WebApi;
using Starbucks.OpenApi.ServiceExtensions.Exceptions;

namespace Account.Provider.Common
{
    public static class IAccountProviderExtensions
    {
        public static string GetUserSubMarket(this IAccountProvider accountProvider, string userId)
        {
            if (accountProvider == null)
            {
                throw new ArgumentNullException("accountProvider");
            }

            if (string.IsNullOrWhiteSpace("userId"))
            {
                throw new ArgumentNullException("userId");
            }

            IUser user = accountProvider.GetUser(userId);

            if (user == null)
            {
                throw new ApiException(
                    HttpStatusCode.BadRequest,
                    CardWebAPIErrorResource.UserIdNotFoundOrIsInvalidCode,
                    CardWebAPIErrorResource.UserIdNotFoundOrIsInvalidMessage);
            }

            return user.SubMarket ?? string.Empty;
        }
    }
}