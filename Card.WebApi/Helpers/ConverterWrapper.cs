﻿using Card.WebApi.Models;
using Starbucks.Card.Provider.Common.Models;

namespace Card.WebApi.Helpers
{
    public class ConverterWrapper : IConverterWrapper
    {
        public PassbookCardInfo GetCardInfo(IAssociatedCard card)
        {
            return Converters.GetCardInfo(card);
        }
    }
}