USE [starbucks_profiles]
GO

IF NOT EXISTS(
	SELECT * FROM sys.columns 
    WHERE Name = N'FileLocationPath' AND Object_ID = Object_ID(N'PassbookObject'))    
BEGIN
   ALTER TABLE [dbo].[PassbookObject]
   ADD [FileLocationPath] NVARCHAR(1000) NULL; 
END

IF NOT EXISTS(
	SELECT * FROM sys.columns 
    WHERE Name = N'PassFilename' AND Object_ID = Object_ID(N'PassbookObject'))    
BEGIN
   ALTER TABLE [dbo].[PassbookObject]
   ADD [PassFilename] NVARCHAR(1000) NULL; 
END