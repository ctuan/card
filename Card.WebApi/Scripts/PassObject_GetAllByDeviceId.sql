USE [starbucks_profiles]
GO
/****** Object:  StoredProcedure [dbo].[PassObject_GetAllByDeviceId]    Script Date: 10/10/2013 16:04:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mitch vitrano
-- Create date: 08/07/2012
-- Description:	Inserts a PassbookObject record
-- Modified by: Sen Wen
-- Modified date: 10/10/2012
-- Description:	Added nolock to select * statement

-- =============================================
ALTER PROCEDURE [dbo].[PassObject_GetAllByDeviceId]
	@DeviceId nvarchar(100),
	@ModifiedSince datetime = null
AS
BEGIN

	DECLARE @LastPassId int;
	DECLARE @PassIdToHandle int;
	SET @LastPassId = 0;
		
	SELECT TOP 1 @PassIdToHandle = y.PassbookObjectId FROM (
								SELECT x.PassbookObjectId, x.RowNumber FROM (
									SELECT po.PassbookObjectId, ROW_NUMBER() OVER(ORDER BY po.PassbookObjectId) as RowNumber 
										FROM PassbookObject  po (nolock)
										RIGHT JOIN PassbookObjectRegistration r (nolock) on r.PassbookObjectId = po.PassbookObjectId WHERE r.DeviceId = @DeviceId AND (@ModifiedSince IS NULL OR DateModified > @ModifiedSince))x 
									WHERE  x.PassbookObjectId > @LastPassId 
								)y


	WHILE @PassIdToHandle IS NOT NULL
	BEGIN
		SELECT * FROM PassbookObject (nolock) WHERE PassbookObjectId = @PassIdToHandle;
	
		EXEC PassObjectDetail_GetByPassObjectId @PassIdToHandle;
		EXEC PassObjectLocation_GetByPassObjectId @PassIdToHandle;
		EXEC PassObjectCardDetail_GetByPassObjectId @PassIdToHandle;
		EXEC PassObjectAsset_GetByPassObjectId @PassIdToHandle;
		EXEC PassObjectRegistration_GetByPassObjectId @PassIdToHandle;
		
		SET @LastPassId = @PassIdToHandle;
		SET @PassIdToHandle = NULL;
		
		SELECT TOP 1 @PassIdToHandle = y.PassbookObjectId FROM (
								SELECT x.PassbookObjectId, x.RowNumber FROM (
									SELECT po.PassbookObjectId, ROW_NUMBER() OVER(ORDER BY po.PassbookObjectId) as RowNumber 
										FROM PassbookObject  po (nolock)
										RIGHT JOIN PassbookObjectRegistration r (nolock) on r.PassbookObjectId = po.PassbookObjectId 
										WHERE r.DeviceId = @DeviceId 
										AND (@ModifiedSince IS NULL OR DateModified > @ModifiedSince)
										AND (r.[Removed] = 0 OR Removed IS NULL)) x 
									WHERE  x.PassbookObjectId > @LastPassId 
								)y
	END
END

