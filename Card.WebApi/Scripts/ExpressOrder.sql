﻿USE [starbucks_profiles]
GO

/****** Object:  Table [dbo].[XOPPreference]    Script Date: 3/19/2014 2:33:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[XOPPreference](
	[UserId] [nvarchar](255) NOT NULL,
	[DisplayName] [nvarchar](255) NULL,
	[OptInDate] [datetime] NULL,
	[OptOutDate] [datetime] NULL,
	[OptInVersion] [decimal](18, 7) NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NOT NULL,
 CONSTRAINT [PK_XOPPreference] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Starbucks.com Stored Procedure            
-- =============================================            
-- Date Created          3/19/2014
-- Developer             Dana Benson
-- Stored Procedure Name 'ExpressOrder_GetXOPPreference'            
-- Stored Procedure Purpose 'Retrieves a user's XOP preferences' 
-- Database 'Starbucks_Profiles'            
-- Change Log:
-- =============================================  
CREATE PROCEDURE [dbo].[ExpressOrder_GetXOPPreference] 
	@userId nvarchar(255)
AS
BEGIN
	SET NOCOUNT ON;

 SELECT [UserId]
      ,[DisplayName]
      ,[OptInDate]
      ,[OptOutDate]
      ,[OptInVersion]
      ,[DateCreated]
      ,[DateModified]
  FROM [dbo].[XOPPreference]
  WHERE
	UserId = @userId

END
GO

SET ansi_nulls ON 

go 

SET quoted_identifier ON 

go 

-- =============================================             
-- Starbucks.com Stored Procedure             
-- =============================================             
-- Date Created          3/19/2014 
-- Developer             Dana Benson 
-- Stored Procedure Name 'ExpressOrder_UpsertXOPPreference'             
-- Stored Procedure Purpose 'inserts or updates user's XOP preferences'  
-- Database 'Starbucks_Profiles'             
-- Change Log: 
-- =============================================   
CREATE PROCEDURE [dbo].[Expressorder_UpsertXOPPreference] 
	@userId       NVARCHAR(255), 
    @displayName  NVARCHAR(255), 
    @optInDate    DATETIME, 
    @optOutDate   DATETIME, 
    @optInVersion DECIMAL (18,7)
AS 
  BEGIN 
      -- SET NOCOUNT ON added to prevent extra result sets from 
      -- interfering with SELECT statements. 
      SET nocount ON; 

      IF EXISTS (SELECT userid 
                 FROM   [dbo].[XOPPreference] 
                 WHERE  userid = @userId) 
        BEGIN 
            UPDATE [dbo].[XOPPreference] 
            SET    [displayname] = @displayName, 
                   [optindate] = @optInDate, 
                   [optoutdate] = @optOutDate, 
                   [optinversion] = @optInVersion, 
                   [datemodified] = Getdate() 
            WHERE  userid = @userId 
        END 
      ELSE 
        BEGIN 
            INSERT INTO [dbo].[XOPPreference] 
                        ([userid], 
                         [displayname], 
                         [optindate], 
                         [optoutdate], 
                         [optinversion], 
                         [datecreated], 
                         [datemodified]) 
            VALUES      (@userId, 
                         @displayName, 
                         @optInDate, 
                         @optOutDate, 
                         @optInVersion, 
                         Getdate(), 
                         Getdate()) 
        END 
  END 

go 