USE [starbucks_profiles]
GO
/****** Object:  StoredProcedure [dbo].[PassObject_INSERT]    Script Date: 07/20/2013 12:17:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mitch vitrano
-- Create date: 08/07/2012
-- Description:	Inserts a PassbookObject record
-- =============================================
ALTER PROCEDURE [dbo].[PassObject_INSERT]
	@PassObjectId int OUT,
	@SerialNumber nvarchar(100) OUT,
	@PassTypeIdentifier nvarchar(100),
	@Style nvarchar(50) = null,
	@OrganizationName nvarchar(100),
	@CardId int,
	@EncryptedCardId nvarchar(100) = null,
	@Description nvarchar(100),
	@TeamIdentifier nvarchar(100),
	@FormatVersion nvarchar(10),
	@UserId nvarchar(255),
	@InternalDeviceId nvarchar(100),
	@LogoText nvarchar(100),
	@BarcodeMessageFormat nvarchar(50),
	@BarcodeMessageEncoding nvarchar(50),
	@BarcodeMessage ntext,
	@BarcodeAltText nvarchar(50),
	@RelevantDate date = null,
	@ForegroundColor nvarchar(100) = null,
	@BackgroundColor nvarchar(100) = null,
	@WebServiceURL nvarchar(max) = null,
	@WebServiceAuthenticationToken nvarchar(200) = null,
	@SourceOfChange nvarchar(50),
	--PassbookObjectCardDetail
	@PassTransactionId int = null,
	@PassBalance float = null,
	@PassBalanceDate datetime = null,
	@PassBalanceCurrency nvarchar(10) = null,
	@CandidateTransactionId int = null,
	@CandidateBalance float = null,
	@CandidateBalanceDate datetime = null,
	@CandidateBalanceCurrency nvarchar(10) = null,
	@Source nvarchar(10),
	@SubMarket nvarchar(50) = null,
	--End PassbookObjectCardDetail
	@DetailItems XML = null,
	@Locations XML = null,
	@Assets XML = null, 
	@AssociatedStoreIdentifier nvarchar(50) = NULL,
	@PassFileLocation NVARCHAR(1000) = NULL,
	@PassFilename NVARCHAR(300) = NULL
AS
BEGIN
	SET NOCOUNT ON
	SET ARITHABORT ON

	BEGIN TRANSACTION
	
	---SET Defaults
	IF @PassTypeIdentifier IS NULL OR @PassTypeIdentifier = ''
	BEGIN
		SELECT @PassTypeIdentifier = Value FROM PassbookObjectDefaults (nolock) WHERE [KEY] = 'passTypeIdentifier';
	END
	
	IF @OrganizationName IS NULL OR @OrganizationName = ''
	BEGIN
		SELECT @OrganizationName = Value FROM PassbookObjectDefaults (nolock) WHERE [KEY] = 'organizationName';
	END
	
	IF @Description IS NULL OR @Description = ''
	BEGIN
		SELECT @Description = Value FROM PassbookObjectDefaults (nolock) WHERE  [KEY] = 'description';
	END
	
	IF @TeamIdentifier IS NULL OR @TeamIdentifier = ''
	BEGIN
		SELECT @TeamIdentifier = Value FROM PassbookObjectDefaults (nolock) WHERE [KEY] = 'teamIdentifier';
	END
	
	IF @BackgroundColor IS NULL OR @BackgroundColor = ''
	BEGIN
		SELECT @BackgroundColor = Value FROM PassbookObjectDefaults (nolock) WHERE [KEY] = 'backgroundColor';
	END
	
	IF @ForegroundColor IS NULL OR @ForegroundColor = ''
	BEGIN
		SELECT @ForegroundColor = Value FROM PassbookObjectDefaults (nolock) WHERE [KEY] = 'foregroundColor';
	END
	
	IF @LogoText IS NULL OR @LogoText = ''
	BEGIN
		SELECT @LogoText = Value FROM PassbookObjectDefaults (nolock) WHERE  [KEY] = 'logoText';
	END
	
	IF @WebServiceURL IS NULL OR @WebServiceURL = ''
	BEGIN
		SELECT @WebServiceURL = Value FROM PassbookObjectDefaults  (nolock) WHERE [KEY] = 'webServiceUrl';
	END
	
	IF @WebServiceAuthenticationToken IS NULL OR @WebServiceAuthenticationToken = ''
	BEGIN
		SELECT @WebServiceAuthenticationToken = Value FROM PassbookObjectDefaults (nolock) WHERE [KEY] = 'authenticationToken';
	END
	
	IF @FormatVersion IS NULL OR @FormatVersion = ''
	BEGIN
		SELECT @FormatVersion = Value FROM PassbookObjectDefaults (nolock) WHERE [KEY] = 'formatVersion';
	END
	
	IF @BarcodeMessageFormat IS NULL OR @BarcodeMessageFormat = ''
	BEGIN
		SELECT @BarcodeMessageFormat = Value FROM PassbookObjectDefaults (nolock) WHERE [KEY] = 'barcodeMessageFormat';
	END
	
	IF @BarcodeMessageEncoding IS NULL OR @BarcodeMessageEncoding = ''
	BEGIN
		SELECT @BarcodeMessageEncoding = Value FROM PassbookObjectDefaults (nolock) WHERE [KEY] = 'barcodeMessageEncoding';
	END
	
	IF @AssociatedStoreIdentifier IS NULL OR @AssociatedStoreIdentifier = ''
	BEGIN
		SELECT @AssociatedStoreIdentifier = Value FROM PassbookObjectDefaults (nolock) WHERE [KEY] = 'associatedStoreIdentifier';
	END
	
	-- Rollback the transaction if there were any errors
	IF @@ERROR <> 0
	 BEGIN
		-- Rollback the transaction
		ROLLBACK
		SET ARITHABORT OFF
		-- Raise an error and return
		RAISERROR ('Error in Setting defaults', 16, 1)
		RETURN
	 END
	
	SELECT @SerialNumber = SerialNumber FROM PassbookObject (nolock) WHERE UserId = @UserId AND CardId = @CardId
	
	IF @SerialNumber IS NOT NULL
	BEGIN		
		SELECT @PassObjectId = PassbookObjectId FROM PassbookObject (nolock) WHERE SerialNumber = @SerialNumber;
	END
	
	-- Rollback the transaction if there were any errors
	IF @@ERROR <> 0
	 BEGIN
		-- Rollback the transaction
		ROLLBACK
		SET ARITHABORT OFF
		-- Raise an error and return
		RAISERROR ('Error in generating the Serial Number', 16, 1)
		RETURN
	 END
	
	IF @PassObjectId IS NOT NULL
		BEGIN
			UPDATE PassbookObject SET
				 PassbookObjectTypeIdentifier = @PassTypeIdentifier, 
				 Style = @Style,
				 OrganizationName = @OrganizationName, 
				 SerialNumber = @SerialNumber, 
				 Description = @Description, 
				 TeamIdentifier = @TeamIdentifier, 
				 FormatVersion = @FormatVersion,
				 UserId = @UserId, 
				 InternalDeviceId = @InternalDeviceId,
				 LogoText = @LogoText, 
				 BarcodeMessageFormat = @BarcodeMessageFormat, 
				 BarcodeMessageEncoding = @BarcodeMessageEncoding, 
				 BarcodeMessage = @BarcodeMessage, 
				 BarcodeAltText = @BarcodeAltText, 
				 RelevantDate = @RelevantDate,
				 ForegroundColor = @ForegroundColor, 
				 BackgroundColor = @BackgroundColor, 
				 WebServiceURL = @WebServiceURL, 
				 WebServiceAuthenticationToken = @WebServiceAuthenticationToken, 
				 DateModified = SYSUTCDATETIME(),
				 SourceOfChange = @SourceOfChange, 
				 SubMarket = @SubMarket,
				 AssociatedStoreIdentifier = @AssociatedStoreIdentifier,
				 FileLocationPath = @PassFileLocation,
				 PassFilename = @PassFilename
			WHERE PassbookObjectId = @PassObjectId;
			
			EXEC PassObjectDetail_DELETE_ByPassObjectId @PassObjectId;
			EXEC PassObjectLocation_DELETE_ByPassObjectId @PassObjectId;
			DELETE FROM PassbookObjectCardDetail WHERE PassbookObjectId = @PassObjectId;
			EXEC PassObjectAsset_DELETE_ByPassObjectId @PassObjectId;
			-- Rollback the transaction if there were any errors
			IF @@ERROR <> 0
			 BEGIN
				-- Rollback the transaction
				ROLLBACK
				SET ARITHABORT OFF
				-- Raise an error and return
				RAISERROR ('Error in updating the existing passbook record', 16, 1)
				RETURN
			 END					
		END
	ELSE
		BEGIN		
		    
		    SET @SerialNumber = NEWID();
		    
		    IF @EncryptedCardId IS NOT NULL
		    BEGIN
				SET @SerialNumber = @EncryptedCardId + '-' + @SerialNumber;
			END
				
			INSERT INTO PassbookObject
			(PassbookObjectTypeIdentifier, Style, OrganizationName, SerialNumber, CardId, Description, TeamIdentifier, FormatVersion,
			 UserId, InternalDeviceId, LogoText, BarcodeMessageFormat, BarcodeMessageEncoding, BarcodeMessage, BarcodeAltText, RelevantDate,
			 ForegroundColor, BackgroundColor, WebServiceURL, WebServiceAuthenticationToken,
			 DateModified, SourceOfChange, SubMarket, AssociatedStoreIdentifier, FileLocationPath, PassFilename)
			 VALUES
			 (@PassTypeIdentifier, @Style, @OrganizationName, @SerialNumber, @CardId, @Description, @TeamIdentifier, @FormatVersion,
			 @UserId, @InternalDeviceId, @LogoText, @BarcodeMessageFormat, @BarcodeMessageEncoding, @BarcodeMessage, @BarcodeAltText, @RelevantDate,
			 @ForegroundColor, @BackgroundColor, @WebServiceURL, @WebServiceAuthenticationToken,
			 SYSUTCDATETIME(), @SourceOfChange, @SubMarket, @AssociatedStoreIdentifier,
			 @PassFileLocation, @PassFilename);
			 
			SET @PassObjectId = SCOPE_IDENTITY();
			
			-- Rollback the transaction if there were any errors
			IF @@ERROR <> 0
			 BEGIN
				-- Rollback the transaction
				ROLLBACK
				SET ARITHABORT OFF
				-- Raise an error and return
				RAISERROR ('Error in adding the Passbook Record', 16, 1)
				RETURN
			 END
		END
		
	 IF @DetailItems IS NOT NULL
	 BEGIN
		EXEC PassObjectDetail_INSERT_ByXml @PassObjectId, @DetailItems;
		
		-- Rollback the transaction if there were any errors
		IF @@ERROR <> 0
		 BEGIN
			-- Rollback the transaction
			ROLLBACK
			SET ARITHABORT OFF
			-- Raise an error and return
			RAISERROR ('Error adding passbook object details', 16, 1)
			RETURN
		 END
	 END
	 
	 ---Do the Default terms/conditions
	 DECLARE @ItemValue nvarchar(500);
	 DECLARE @ItemLabel nvarchar(100);
	 
	 SELECT @ItemValue = [Value], @ItemLabel = Label FROM PassbookObjectDefaults WHERE [KEY] = 'terms';	 
	 EXEC PassObjectDetail_INSERT @PassObjectId = @PassObjectId, @ItemKey = 'terms', @ItemValue = @ItemValue, @ItemLabel = @ItemLabel, @Type = 'BackField';
	
	 IF @Locations IS NOT NULL
	 BEGIN
		EXEC PassObjectLocation_INSERT_ByXml @PassObjectId, @Locations;
		
		-- Rollback the transaction if there were any errors
		IF @@ERROR <> 0
		 BEGIN
			-- Rollback the transaction
			ROLLBACK
			SET ARITHABORT OFF
			-- Raise an error and return
			RAISERROR ('Error Adding passbook locations by xml', 16, 1)
			RETURN
		 END
	 END
	 
	 EXEC PassObjectCardDetail_INSERT @PassObjectId, @PassTransactionId, @PassBalance, @PassBalanceDate, @PassBalanceCurrency,
										  @CandidateTransactionId, @CandidateBalance, @CandidateBalanceDate, @CandidateBalanceCurrency, @Source;
		
	
	  -- Rollback the transaction if there were any errors
      IF @@ERROR <> 0
	    BEGIN
			-- Rollback the transaction
			ROLLBACK
			SET ARITHABORT OFF
			-- Raise an error and return
			RAISERROR ('Error adding Passbook card detail', 16, 1)
			RETURN
	    END
	 		
	 IF @Assets IS NOT NULL
	 BEGIN
		EXEC PassObjectAsset_INSERT_ByXml @PassObjectId, @Assets;
	 END	
	 
	 --do the default assets (Images)
	 
	 DECLARE @AssetKey nvarchar(100)
	 DECLARE @AssetUrl nvarchar(500)
	 DECLARE @AssetVersion int
	 
	 SELECT @AssetKey = [Key], @AssetUrl = Value, @AssetVersion = Label FROM PassbookObjectDefaults (nolock) WHERE [KEY] = 'logo.png'
	 EXEC PassObjectAsset_INSERT @PassObjectId = @PassObjectId, @Key = @AssetKey, @Url = @AssetUrl, @Version = @AssetVersion;
	 
	 SELECT @AssetKey = [Key], @AssetUrl = Value, @AssetVersion = Label FROM PassbookObjectDefaults (nolock) WHERE [KEY] = 'logo@2x.png'
	 EXEC PassObjectAsset_INSERT @PassObjectId = @PassObjectId, @Key = @AssetKey, @Url = @AssetUrl, @Version = @AssetVersion;
	 		
	 SELECT @AssetKey = [Key], @AssetUrl = Value, @AssetVersion = Label FROM PassbookObjectDefaults (nolock) WHERE [KEY] = 'icon.png'
	 EXEC PassObjectAsset_INSERT @PassObjectId = @PassObjectId, @Key = @AssetKey, @Url = @AssetUrl, @Version = @AssetVersion;			  
	 
	 SELECT @AssetKey = [Key], @AssetUrl = Value, @AssetVersion = Label FROM PassbookObjectDefaults (nolock) WHERE [KEY] = 'icon@2x.png'
	 EXEC PassObjectAsset_INSERT @PassObjectId = @PassObjectId, @Key = @AssetKey, @Url = @AssetUrl, @Version = @AssetVersion;
	
	-- Rollback the transaction if there were any errors
      IF @@ERROR <> 0
	  BEGIN
		-- Rollback the transaction
		ROLLBACK
		SET ARITHABORT OFF
		-- Raise an error and return
		RAISERROR ('Error adding Passbook assets', 16, 1)
		RETURN
	  END
	    
	 COMMIT TRANSACTION
	 
	 SET ARITHABORT OFF
END

