﻿/****** Object:  StoredProcedure [dbo].[SocialProfile_GetTwitter]    Script Date: 08/27/2013 13:55:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jon Harkness
-- Create date: 2013/08/27
-- Description:	Gets a users' social profile twitter data
-- =============================================
Create PROCEDURE [dbo].[SocialProfile_GetTwitterByTwitterUserId] 
	@twitterUserId int
AS
BEGIN
	select s.UserId, 
		coalesce(s.DateCreated, getUtcDate()) as DateCreated,
		coalesce(s.DateUpdated, getUtcDate()) as DateUpdated,
		s.TwitterUserId, s.TwitterAccessToken, s.TwitterAccessTokenSecret, s.TwitterConsumerKey
	from SocialProfile_Twitter (nolock) s
	where s.TwitterUserId = @twitterUserId;
END
