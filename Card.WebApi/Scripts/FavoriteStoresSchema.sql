﻿USE [Starbucks_Profiles]
GO

/****** Object:  Table [dbo].[FavoriteStore]    Script Date: 06/03/2013 15:34:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FavoriteStore](
	[UserId] [nvarchar](255) NOT NULL,
	[StoreId] [int] NOT NULL,
	[Nickname] [nvarchar](255) NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
 CONSTRAINT [PK_FavoriteStore] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[StoreId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[FavoriteStore]  WITH CHECK ADD  CONSTRAINT [FK_FavoriteStore_UserObject] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserObject] ([g_user_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[FavoriteStore] CHECK CONSTRAINT [FK_FavoriteStore_UserObject]
GO


USE [Starbucks_Profiles]
GO

/****** Object:  StoredProcedure [dbo].[FavoriteStore_Delete]    Script Date: 06/03/2013 15:35:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jon Harkness
-- Create date: 2013/6/3
-- Description:	Removes a favourite store from user.
-- =============================================
CREATE PROCEDURE [dbo].[FavoriteStore_Delete]
	@userId nvarchar(255),
	@storeId int = null
AS
BEGIN
	
	delete from FavoriteStore
	where UserId = @userId and (@storeId is null or StoreId = @storeId);
	
END

GO


USE [Starbucks_Profiles]
GO

/****** Object:  StoredProcedure [dbo].[FavoriteStore_Get]    Script Date: 06/03/2013 15:35:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jon Harkness
-- Create date: 2013/6/3
-- Description:	Gets a user's favourite stores
-- =============================================
CREATE PROCEDURE [dbo].[FavoriteStore_Get]
	@userId nvarchar(255)
AS
BEGIN
	
	select UserId, StoreId, Nickname, DateCreated, DateUpdated
	from FavoriteStore f
	where f.UserId = @userId;
	
END

GO


USE [Starbucks_Profiles]
GO

/****** Object:  StoredProcedure [dbo].[FavoriteStore_Save]    Script Date: 06/03/2013 15:36:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jon Harkness
-- Create date: 2013/6/3
-- Description:	Adds users' favorite store to db
-- =============================================
CREATE PROCEDURE [dbo].[FavoriteStore_Save]
	@userId nvarchar(255),
	@storeId int,
	@nickname nvarchar(255) = null
AS
BEGIN
	
	with args as (select @userId as UserId, @storeId as StoreId, @nickname as Nickname)
	merge into FavoriteStore T
	using args S on S.UserId = T.UserId and S.StoreId = T.StoreId
	when matched then
		update set T.Nickname = S.Nickname, T.DateUpdated = getUtcDate()
	when not matched then
		insert (UserId, StoreId, Nickname, DateCreated, DateUpdated)
		values (S.UserId, S.StoreId, S.Nickname, getUtcDate(), getUtcDate());		
	
END

GO

