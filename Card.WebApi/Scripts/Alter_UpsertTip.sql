USE [starbucks_profiles]
GO
/****** Object:  StoredProcedure [dbo].[UpsertTip]    Script Date: 2/13/2014 2:10:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bryce Lindsey
-- Create date: 7/23/2013
-- Description:	Inserts or Updates a Tip.
-- Modified: 2/10/2014 titurner Add cardId, tipAmount, tipAmountCharged, tipTransactionId
-- =============================================
ALTER PROCEDURE [dbo].[UpsertTip] 
	@originalTransactionId bigint, 
	@userId nvarchar(255),
	@statusId int = NULL,
	@cardId int = NULL,
	@tipAmount money = NULL,
	@tipAmountCharged money = NULL,
	@tipTransactionId money = NULL
AS
BEGIN
	DECLARE @now datetime = GETUTCDATE();

	DECLARE @noneTipStatus int
	SET @noneTipStatus = 0
	DECLARE @insertedTipStatus int
	SET @insertedTipStatus = 1
	DECLARE @updatedTipStatus int
	SET @updatedTipStatus = 2
	DECLARE @currentStatusId int
	--SET @currentStatusId = @noneTipStatus -- default to None

	-- Check for originalTransactionId that belongs to a different userId.
	IF EXISTS (SELECT 1 FROM Tip (nolock) WHERE OriginalTransactionId = @originalTransactionId AND UserId != @userId)
	BEGIN
		RAISERROR(N'Transaction ''%I64d'' does not belong to user ''%s''.', 16, 1, @originalTransactionId, @userId);
		RETURN;
	END;

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	BEGIN TRANSACTION;

	-- Check for anything other than an Insert for a tip that doesn't exist
	IF NOT EXISTS (SELECT 1 FROM Tip (nolock) WHERE OriginalTransactionId = @originalTransactionId AND UserId = @userId)
	AND @statusId != @insertedTipStatus
	BEGIN
		RAISERROR(N'Tip not found for transaction ''%I64d''.', 16, 2, @originalTransactionId);
		COMMIT TRANSACTION;
		RETURN;
	END;

	SELECT @currentStatusId = StatusId FROM Tip (nolock) WHERE OriginalTransactionId = @originalTransactionId AND UserId = @userId;

	-- Can the current tip status be changed to the new tip status?
	IF @currentStatusId IS NOT NULL AND NOT EXISTS 
	(
		SELECT 1 FROM TipStatusChangeMatrix tscm (nolock)
		WHERE FromStatusId = @currentStatusId AND ToStatusId = ISNULL(@statusId, @updatedTipStatus)
	)
	BEGIN
		DECLARE @fromStatusName nvarchar(255)
		SELECT @fromStatusName = Name FROM TipStatus (nolock) WHERE Id = @currentStatusId
		DECLARE @toStatusName nvarchar(255)
		SELECT @toStatusName = Name FROM TipStatus (nolock) WHERE Id = @statusId
		RAISERROR(N'Cannot change tip status from ''%s'' to ''%s''.', 16, 3, @fromStatusName, @toStatusName);
		COMMIT TRANSACTION;
		RETURN;
	END;
    
	MERGE INTO Tip WITH (HOLDLOCK) AS t
	USING (VALUES(@userId, @originalTransactionId)) AS Source(UserId, OriginalTransactionId)
	ON t.UserId = Source.UserId 
		AND t.OriginalTransactionId = Source.OriginalTransactionId
	WHEN MATCHED		
	THEN UPDATE SET
		StatusId = ISNULL(@statusId, @updatedTipStatus),		
		CardId = IsNull(@cardId, CardId),
		TipAmount = IsNull(@tipAmount, TipAmount),
		TipAmountCharged = IsNull(@tipAmountCharged, TipAmountCharged),
		TipTransactionId = IsNull(@tipTransactionId, TipTransactionId),
		LastModified = @now
	WHEN NOT MATCHED
	THEN
		INSERT
			(UserId,
			OriginalTransactionId,			
			StatusId,
			DateCreated,
			LastModified,
			CardId,
			TipAmount,
			TipAmountCharged,
			TipTransactionId)
		VALUES
			(@userId,
			@originalTransactionId,			
			@insertedTipStatus,
			@now,
			@now,
			@cardId,
			@tipAmount,
			@tipAmountCharged,
			@tipTransactionId)
	OUTPUT ISNULL(@currentStatusId, @noneTipStatus) AS PreviousStatusId, INSERTED.StatusId AS CurrentStatusId;

	COMMIT TRANSACTION;
END;

