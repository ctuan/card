﻿

USE starbucks_profiles
SET IDENTITY_INSERT [dbo].[Currency] ON 

GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (1, N'Afghani', N'AFA', 4)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (2, N'Algerian Dinar', N'DZD', 12)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (3, N'Andorran Peseta', N'ADP', 20)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (4, N'Antillian Guilder', N'ANG', 532)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (5, N'Argentine Peso', N'ARS', 32)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (6, N'Armenian Dram', N'AMD', 51)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (7, N'Aruban Guilder', N'AWG', 533)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (8, N'Australian Dollar', N'AUD', 36)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (9, N'Azerbaijanian Manat', N'AZM', 31)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (10, N'Bahamian Dollar', N'BSD', 44)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (11, N'Bahraini Dinar', N'BHD', 48)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (12, N'Baht', N'THB', 764)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (13, N'Balboa', N'PAB', 590)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (14, N'Barbados Dollar', N'BBD', 52)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (15, N'Belarussian Ruble', N'BYR', 974)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (16, N'Belgian Franc', N'BEF', 56)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (17, N'Belize Dollar', N'BZD', 84)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (18, N'Bermudian Dollar', N'BMD', 60)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (19, N'Bolivar', N'VEB', 862)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (20, N'Brazilian Real', N'BRL', 986)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (21, N'Brunei Dollar', N'BND', 96)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (22, N'Bulgarian LEV', N'BGN', 975)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (23, N'Burundi Franc', N'BIF', 108)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (24, N'Canadian Currency', N'CAD', 124)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (25, N'Cape Verde Escudo', N'CVE', 132)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (26, N'Cayman Islands Dollar', N'KYD', 136)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (27, N'Cedi', N'GHC', 288)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (28, N'CFA Franc BCEAO', N'XOF', 952)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (29, N'CFA Franc BEAC', N'XAF', 950)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (30, N'CFP Franc', N'XPF', 953)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (31, N'Chilean Peso', N'CLP', 152)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (32, N'Codes specifically reserved for testing purposes', N'XTS', 963)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (33, N'Colombian Peso', N'COP', 170)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (34, N'Comoro Franc', N'KMF', 174)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (35, N'Convertible Marks', N'BAM', 977)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (36, N'Cordoba Oro', N'NIO', 558)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (37, N'Costa Rican Colon', N'CRC', 188)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (38, N'Cuban Peso', N'CUP', 192)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (39, N'Cyprus Pound', N'CYP', 196)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (40, N'Czech Koruna', N'CZK', 203)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (41, N'Dalasi', N'GMD', 270)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (42, N'Danish Krone', N'DKK', 208)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (43, N'Denar', N'MKD', 807)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (44, N'Deutsche Mark', N'DEM', 280)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (45, N'Djibouti Franc', N'DJF', 262)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (46, N'Dobra', N'STD', 678)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (47, N'Dominican Peso', N'DOP', 214)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (48, N'Dong', N'VND', 704)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (49, N'Drachma', N'GRD', 300)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (50, N'East Caribbean Dollar', N'XCD', 951)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (51, N'Egyptian Pound', N'EGP', 818)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (52, N'El Salvador Colon', N'SVC', 222)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (53, N'Ethiopian Birr', N'ETB', 230)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (54, N'Euro', N'EUR', 978)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (55, N'European Composite Unit (EURCO)', N'XBA', 955)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (56, N'European Monetary Unit (E.M.U.-6)', N'XBB', 956)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (57, N'European Unit of Account 17 (E.U.A.- 17)', N'XBD', 958)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (58, N'European Unit of Account 9 (E.U.A.- 9)', N'XBC', 957)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (59, N'Fiji Dollar', N'FJD', 242)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (60, N'Forint', N'HUF', 348)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (61, N'Franc Congolais', N'CDF', 976)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (62, N'French Franc', N'FRF', 250)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (63, N'Gibraltar Pound', N'GIP', 292)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (64, N'Gold Bond Markets Units', N'XAU', 959)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (65, N'Gourde', N'HTG', 332)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (66, N'Guarani', N'PYG', 600)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (67, N'Guinea Franc', N'GNF', 324)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (68, N'Guinea-Bissau Peso', N'GWP', 624)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (69, N'Guyana Dollar', N'GYD', 328)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (70, N'Hong Kong Dollar', N'HKD', 344)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (71, N'Hryvnia', N'UAH', 980)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (72, N'Iceland Krona', N'ISK', 352)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (73, N'Indian Rupee', N'INR', 356)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (74, N'Iranian Rial', N'IRR', 364)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (75, N'Iraqi Dinar', N'IQD', 368)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (76, N'Irish Pound', N'IEP', 372)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (77, N'Italian Lira', N'ITL', 380)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (78, N'Jamaican Dollar', N'JMD', 388)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (79, N'Jordanian Dinar', N'JOD', 400)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (80, N'Kenyan Shilling', N'KES', 404)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (81, N'Kina', N'PGK', 598)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (82, N'Kip', N'LAK', 418)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (83, N'Kroon', N'EEK', 233)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (84, N'Kuna', N'HRK', 191)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (85, N'Kuwaiti Dinar', N'KWD', 414)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (86, N'Kwacha', N'MWK', 454)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (87, N'Kwacha', N'ZMK', 894)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (88, N'Kwanza Reajustado', N'AOR', 982)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (89, N'Kyat', N'MMK', 104)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (90, N'Lari', N'GEL', 981)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (91, N'Latvian Lats', N'LVL', 428)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (92, N'Lebanese Pound', N'LBP', 422)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (93, N'Lek', N'ALL', 8)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (94, N'Lempira', N'HNL', 340)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (95, N'Leone', N'SLL', 694)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (96, N'Leu', N'ROL', 642)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (97, N'Lev', N'BGL', 100)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (98, N'Liberian Dollar', N'LRD', 430)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (99, N'Libyan Dinar', N'LYD', 434)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (100, N'Lilangeni', N'SZL', 748)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (101, N'Lithuanian Litas', N'LTL', 440)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (102, N'Loti', N'LSL', 426)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (103, N'Luxembourg Franc', N'LUF', 442)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (104, N'Malagasy Franc', N'MGF', 450)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (105, N'Malaysian Ringgit', N'MYR', 458)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (106, N'Maltese Lira', N'MTL', 470)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (107, N'Manat', N'TMM', 795)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (108, N'Markka', N'FIM', 246)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (109, N'Mauritius Rupee', N'MUR', 480)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (110, N'Metical', N'MZM', 508)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (111, N'Mexican Peso', N'MXN', 484)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (112, N'Mexican Unidad de Inversion (UDI)', N'MXV', 979)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (113, N'Moldovan Leu', N'MDL', 498)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (114, N'Moroccan Dirham', N'MAD', 504)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (115, N'Naira', N'NGN', 566)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (116, N'Nakfa', N'ERN', 232)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (117, N'Namibia Dollar', N'NAD', 516)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (118, N'Nepalese Rupee', N'NPR', 524)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (119, N'Netherlands Guilder', N'NLG', 528)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (120, N'New Dinar', N'YUM', 891)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (121, N'New Israeli Sheqel', N'ILS', 376)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (122, N'New Kwanza', N'AON', 24)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (123, N'New Taiwan Dollar', N'TWD', 901)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (124, N'New Zaire', N'ZRN', 180)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (125, N'New Zealand Dollar', N'NZD', 554)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (126, N'Ngultrum', N'BTN', 64)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (127, N'North Korean Won', N'KPW', 408)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (128, N'Norwegian Krone', N'NOK', 578)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (129, N'Nuevo Sol', N'PEN', 604)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (130, N'Ouguiya', N'MRO', 478)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (131, N'Pa''anga', N'TOP', 776)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (132, N'Pakistan Rupee', N'PKR', 586)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (133, N'Palladium', N'XPD', 964)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (134, N'Pataca', N'MOP', 446)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (135, N'Peso Uruguayo', N'UYU', 858)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (136, N'Philippine Peso', N'PHP', 608)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (137, N'Platinum', N'XPT', 962)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (138, N'Portuguese Escudo', N'PTE', 620)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (139, N'Pound', N'FKP', 238)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (140, N'Pound Sterling', N'GBP', 826)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (141, N'Pula', N'BWP', 72)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (142, N'Qatari Rial', N'QAR', 634)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (143, N'Quetzal', N'GTQ', 320)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (144, N'Rand', N'ZAR', 710)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (145, N'Rial Omani', N'OMR', 512)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (146, N'Riel', N'KHR', 116)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (147, N'Rufiyaa', N'MVR', 462)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (148, N'Rupiah', N'IDR', 360)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (149, N'Russian Ruble', N'RUB', 643)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (150, N'Russian Ruble', N'RUR', 810)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (151, N'Rwanda Franc', N'RWF', 646)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (152, N'Saudi Riyal', N'SAR', 682)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (153, N'Schilling', N'ATS', 40)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (154, N'SDR', N'XDR', 960)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (155, N'Seychelles Rupee', N'SCR', 690)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (156, N'Silver', N'XAG', 961)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (157, N'Singapore Dollar', N'SGD', 702)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (158, N'Slovak Koruna', N'SKK', 703)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (159, N'Solomon Islands Dollar', N'SBD', 90)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (160, N'Som', N'KGS', 417)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (161, N'Somali Shilling', N'SOS', 706)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (162, N'Somoni', N'TJS', 972)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (163, N'Spanish Peseta', N'ESP', 724)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (164, N'Sri Lanka Rupee', N'LKR', 144)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (165, N'St Helena Pound', N'SHP', 654)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (166, N'Sucre', N'ECS', 218)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (167, N'Sudanese Dinar', N'SDD', 736)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (168, N'Surinam Guilder', N'SRG', 740)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (169, N'Swedish Krona', N'SEK', 752)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (170, N'Swiss Franc', N'CHF', 756)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (171, N'Syrian Pound', N'SYP', 760)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (172, N'Tajik Ruble (old)', N'TJR', 762)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (173, N'Taka', N'BDT', 50)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (174, N'Tala', N'WST', 882)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (175, N'Tanzanian Shilling', N'TZS', 834)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (176, N'Tenge', N'KZT', 398)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (177, N'The codes assigned for transactions where no currency is involved are:', N'XXX', 999)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (178, N'Timor Escudo', N'TPE', 626)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (179, N'Tolar', N'SIT', 705)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (180, N'Trinidad and Tobago Dollar', N'TTD', 780)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (181, N'Tugrik', N'MNT', 496)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (182, N'Tunisian Dinar', N'TND', 788)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (183, N'Turkish Lira', N'TRL', 792)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (184, N'UAE Dirham', N'AED', 784)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (185, N'Uganda Shilling', N'UGX', 800)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (186, N'Unidad de Valor Constante (UVC)', N'ECV', 983)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (187, N'Unidades de fomento', N'CLF', 990)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (188, N'US Currency', N'USD', 840)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (189, N'Uzbekistan Sum', N'UZS', 860)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (190, N'Vatu', N'VUV', 548)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (191, N'Won', N'KRW', 410)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (192, N'Yemeni Rial', N'YER', 886)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (193, N'Yen', N'JPY', 392)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (194, N'Yuan Renminbi', N'CNY', 156)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (195, N'Zimbabwe Dollar', N'ZWD', 716)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (196, N'Zloty', N'PLN', 985)
GO
INSERT [dbo].[Currency] ([Currency_ID], [Currency], [ISOCode], [ISONo]) VALUES (197, N'Leu', N'RON', 946)
GO
SET IDENTITY_INSERT [dbo].[Currency] OFF
GO
