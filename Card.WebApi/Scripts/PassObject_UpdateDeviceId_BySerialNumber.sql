﻿USE [starbucks_profiles]
GO
/****** Object:  StoredProcedure [dbo].[PassObject_UpdateDeviceId_BySerialNumber]    Script Date: 10/11/2013 16:01:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[PassObject_UpdateDeviceId_BySerialNumber]
	@SerialNumber nvarchar(100),
	@DeviceId nvarchar(100),
	@PushToken nvarchar(200) = null
AS
BEGIN
	DECLARE @PassObjectId int
	DECLARE @ExistingDeviceId nvarchar(100) = null
	DECLARE @PassRegistrationTypeStatus INT = 1  -- 1 = new, 2 = update, 3 = NotFound
	
	SELECT @PassObjectId = PassbookObjectId FROM PassbookObject WHERE SerialNumber = @SerialNumber	
	
	IF @PassObjectId IS NULL
	BEGIN
		SET @PassRegistrationTypeStatus = 3;
		SELECT @PassRegistrationTypeStatus;
		
		RETURN
	END
	
	SELECT @ExistingDeviceId = DeviceId FROM PassbookObjectRegistration WHERE PassbookObjectId = @PassObjectId AND DeviceId = @DeviceId;
	
	IF @ExistingDeviceId IS NOT NULL
	BEGIN
		SET @PassRegistrationTypeStatus = 2;
		UPDATE PassbookObjectRegistration SET PushToken = @PushToken, Removed = 0 WHERE PassbookObjectId = @PassObjectId and DeviceId = @ExistingDeviceId;
    END
    ELSE
    BEGIN
		INSERT INTO PassbookObjectRegistration (PassbookObjectId, DeviceId, PushToken, Removed) VALUES (@PassObjectId, @DeviceId, @PushToken, 0)
    END
    SELECT @PassRegistrationTypeStatus;
END

