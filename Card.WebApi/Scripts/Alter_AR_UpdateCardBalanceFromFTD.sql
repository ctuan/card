﻿USE [starbucks_profiles]
GO
/****** Object:  StoredProcedure [dbo].[AR_UpdateCardBalanceFromFTD]    Script Date: 2/24/2014 9:40:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Date Created          12/29/2011   
-- Developer             John Lewis
-- Stored Procedure Name ''AR_UpdateCardBalanceFromFTD''            
-- Stored Procedure Purpose ''Gets queued candidates from database table that''            
-- Database ''Starbucks_Profiles''      
--      
-- Change Log:
-- 5/3/12	Modified for Ceylon  
-- 9/20/12  Modified by kmcdonne to allow MAXDOP 4
-- 10/9/12 Mofified by jlewis to add (readpast)
-- =============================================    
ALTER PROCEDURE [dbo].[AR_UpdateCardBalanceFromFTD]
AS
BEGIN

MERGE CardBalance WITH(READPAST) target
			USING (Select c.CardId, fd.Balance, fd.BalanceDateUTC, currency.ISOCode, localCurrency.ISOCode, fd.ExchangeRate 
					from Card c (nolock)  
					inner join FTDCardBalance fd (nolock) on fd.number=c.number
					left join [Currency] currency (nolock) on fd.CurrencyCode  = currency.ISONo
					left join [Currency] localCurrency (nolock) on fd.LocalCurrencyCode = localCurrency.ISONo) 
					AS source (CardID,Balance, BalanceDateUTC, CurrencyCode, LocalCurrencyCode, ExchangeRate)
			ON (target.cardid= source.cardid)
		WHEN MATCHED AND source.BalanceDateUTC > target.balancedate THEN
			UPDATE Set target.Balance= 
					case 
						when target.Currency = source.LocalCurrencyCode then source.balance
						when target.Currency = source.CurrencyCode then round( source.balance * ExchangeRate, 2)
						else source.balance
					end ,
				target.BalanceDate = source.BalanceDateUTC,
				target.Currency =  
					case
						when target.currency <> source.LocalCurrencyCode and target.currency <> source.CurrencyCode then  source.LocalCurrencyCode
						else target.currency
					end
		OPTION(MAXDOP 4);
END
