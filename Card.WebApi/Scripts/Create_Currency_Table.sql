﻿USE starbucks_profiles
/****** Object:  Table [dbo].[Currency]    Script Date: 3/3/2014 3:50:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Currency](
	[Currency_ID] [int] IDENTITY(1,1) NOT NULL,
	[Currency] [nvarchar](100) NOT NULL,
	[ISOCode] [nvarchar](3) NOT NULL,
	[ISONo] [int] NOT NULL,
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[Currency_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO