﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Runtime.Remoting.Messaging;

namespace Card.WebApi.Logging
{
    /// <summary>
    /// Worker class shared between text and binary log formatters.
    ///   Encrypts stored value card numbers.
    ///   Adds extended property containing correlationId.
    /// </summary>
    public static class LoggingSharedLogFormatter
    {
        /// <summary>
        /// Transforms log entry by encrypting stored value card numbers and attaching correlationId.
        /// </summary>
        /// <param name="logEntry">Entry to be processed.</param>
        public static void FormatEntry(LogEntry logEntry)
        {
            AddCorrelationProperty(logEntry);
            AddAdditionalProperties(logEntry);
            EncryptCardNumber(logEntry);
        }

        /// <summary>
        /// Locate stored value card numbers in message and properties.
        /// Apply encryption from platform security package.
        /// </summary>
        /// <param name="logEntry">Entry to be processed.</param>
        private static void EncryptCardNumber(LogEntry logEntry)
        {
            logEntry.Message = Encrypter.Encrypt16DigitIntegers(logEntry.Message);

            if (logEntry.ExtendedProperties.ContainsKey(LoggingResources.CardNumberExtendedProperty))
            {
                // No need to pass this through Regex as we already have the number. So call encrypt directly.
                var cardNumberValue = logEntry.ExtendedProperties[LoggingResources.CardNumberExtendedProperty];
                logEntry.ExtendedProperties[LoggingResources.CardNumberExtendedProperty] = Encrypter.GetEncryptedCardNumber(Convert.ToString(cardNumberValue));
            }

            if (logEntry.ExtendedProperties.ContainsKey(LoggingResources.SiebelCommandExtendedProperty))
            {
                var siebelCommandValue = logEntry.ExtendedProperties[LoggingResources.SiebelCommandExtendedProperty];
                logEntry.ExtendedProperties[LoggingResources.SiebelCommandExtendedProperty] = Encrypter.Encrypt16DigitIntegers(Convert.ToString(siebelCommandValue));
            }

            if (logEntry.ExtendedProperties.ContainsKey(LoggingResources.TitleExtendedProperty))
            {
                var titleValue = logEntry.ExtendedProperties[LoggingResources.TitleExtendedProperty];
                logEntry.ExtendedProperties[LoggingResources.TitleExtendedProperty] = Encrypter.Encrypt16DigitIntegers(Convert.ToString(titleValue));
            }

            if (logEntry.ExtendedProperties.ContainsKey(LoggingResources.ApiCommandExtendedProperty))
            {
                var apiCommandValue = logEntry.ExtendedProperties[LoggingResources.ApiCommandExtendedProperty];
                logEntry.ExtendedProperties[LoggingResources.ApiCommandExtendedProperty] = Encrypter.Encrypt16DigitIntegers(Convert.ToString(apiCommandValue));
            } 
        }

        /// <summary>
        /// Add correlationId to log entry as an extended property.
        /// Approach is similar to Trace.CorrelationManager and ActivityId, but works with async + await.
        /// Id is initially captured from http request by a handler (see Handlers\CorrelationIdMessageHandler.cs).
        /// </summary>
        /// <param name="logEntry">Entry to be processed.</param>
        private static void AddCorrelationProperty(LogEntry logEntry)
        {
            // get correlationId and add to extended properties
            Guid correlationId = Guid.Empty;

            try
            {
                var correlation = CallContext.LogicalGetData(LoggingResources.CorrelationIdCallContextKey) ?? null;
                correlationId = (correlation != null) ? (Guid)correlation : Guid.Empty;
            }
            catch { }

            logEntry.ExtendedProperties[LoggingResources.CorrelationIdExtendedProperty] = correlationId;
            // Pass along correlationId in ThreadName field to limit changes to order management log processor
            logEntry.ManagedThreadName = correlationId.ToString();
        }

        /// <summary>
        /// Add CountryCode, MsrProgramVersion, and ApiLevel to logEntry extended properties.
        /// </summary>
        /// <param name="logEntry">The log entry to add extended properties to.</param>
        private static void AddAdditionalProperties(LogEntry logEntry)
        {
            string countryCode = string.Empty;
            string msrProgramVersion = string.Empty;
            string apiCommand = string.Empty;

            // Note: We attempt to avoid having NullReferenceExceptions thrown.  We do this by using the null-coalescing operation (??) 
            // rather than explicit casting, which would throw a NullReferenceException.  try/catch blocks are in place 
            // for exceptions thrown by the CallContext.  

            try
            {
                countryCode = (string)CallContext.LogicalGetData(LoggingResources.CountryCodeCallContextKey) ?? string.Empty;
            }
            catch { }

            try
            {
                msrProgramVersion = (string)CallContext.LogicalGetData(LoggingResources.MsrProgramCallContextKey) ?? string.Empty;
            }
            catch { }

            try
            {
                apiCommand = (string)CallContext.LogicalGetData(LoggingResources.ApiCommandCallContextKey) ?? string.Empty;
            }
            catch { }

            logEntry.ExtendedProperties[LoggingResources.CountryCodeExtendedProperty] = countryCode;
            logEntry.ExtendedProperties[LoggingResources.MsrProgramExtendedProperty] = msrProgramVersion;
            logEntry.ExtendedProperties[LoggingResources.ApiCommandExtendedProperty] = apiCommand;
            logEntry.ExtendedProperties[LoggingResources.ApiVersionExtendedProperty] = LoggingResources.ApiVersion;
        }
    }
}