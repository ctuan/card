﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;
using System.Collections.Specialized;

namespace Card.WebApi.Logging
{
    [ConfigurationElementType(typeof(CustomFormatterData))]
    public class LoggingTextFormatter : TextFormatter
    {
        /// <summary>
        /// Implement Enterprise Logging extension point
        ///   encrypt stored value card numbers, add correlationId as extended property
        /// </summary>
        /// <param name="nvc">relay template to base class</param>
        public LoggingTextFormatter(NameValueCollection nvc) : base(nvc.Get("template"))
        {
        }

        /// <summary>
        /// Custom formatter performs two functions on log entry prior to delivery to sink
        ///    1) scan for stored value card numbers
        ///       when found, encrypt in place
        ///    2) add correlationId to extended property set
        ///       all records associated with an http request share the same correlationId 
        /// </summary>
        /// <param name="log">current log entry</param>
        /// <returns>string representation of entry</returns>
        public override string Format(LogEntry log)
        {
            LoggingSharedLogFormatter.FormatEntry(log);
            return base.Format(log);
        }
    }
}