﻿using Starbucks.Platform.Security;
using System.Text.RegularExpressions;

namespace Card.WebApi.Logging
{
    public static class Encrypter
    {
        // Match 16 numbers surrounded by one non number
        private const string SixteenDigitNumberRegex = @"(^|[^0-9])\d{16}([^0-9]|$)";

        // Match 16 digit numbers
        private const string NumberRegex = @"\d{16}";

        // Marker to indicate that a value was encrypted
        // so someone looking at log can recognize encrypted values easily
        private const string EncryptedValueMarkerRegEx = @"\[encrypted:*[a-zA-Z0-9_]*]";
        private const string EncryptedValueMarkerFormat = "[encrypted:{0}]";

        public static string Encrypt16DigitIntegers(string input)
        {
            var matches = Regex.Matches(input, SixteenDigitNumberRegex);

            foreach (var match in matches)
            {
                var numberMatch = Regex.Match(match.ToString(), NumberRegex);
                if (numberMatch.Success)
                {
                    var cardNumber = numberMatch.Value;
                    input = input.Replace(cardNumber, GetEncryptedCardNumber(cardNumber));
                }
            }

            return input;
        }

        public static string GetEncryptedCardNumber(string cardNumber)
        {
            return (Regex.IsMatch(cardNumber, EncryptedValueMarkerRegEx)) ? 
                cardNumber : 
                string.Format(EncryptedValueMarkerFormat, Encryption.EncryptCardNumber(cardNumber));
        }
    }
}