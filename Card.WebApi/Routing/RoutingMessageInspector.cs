﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Web;

namespace Card.WebApi.Routing
{
    public class RoutingMessageInspector : BehaviorExtensionElement, IEndpointBehavior, IClientMessageInspector
    {

        #region IClientMessageInspector Members

        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            return;
        }

        public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, IClientChannel channel)
        {
            MessageHeader siebelHeader = MessageHeader.CreateHeader("LoyaltyMessageTrackingId", "http://starbucks.com/LoyaltyRoutingService", Guid.NewGuid().ToString());
            request.Headers.Add(siebelHeader);
            return true;
        }

        #endregion

        # region BehaviorExtensionElement abstract class overrides

        public override Type BehaviorType
        {
            get { return typeof(RoutingMessageInspector); }
        }

        protected override object CreateBehavior()
        {
            return new RoutingMessageInspector();
        }

        #endregion

        #region IEndpointBehavior Members

        /// <summary>
        /// Not Implemented
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="bindingParameters"></param>
        public void AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {
            return;
        }

        /// <summary>
        /// Adds the EndpointBehavior, this object, to the ClientRuntime.MessageInspectors
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="clientRuntime"></param>
        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(this);
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="endpointDispatcher"></param>
        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            return;
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        /// <param name="endpoint"></param>
        public void Validate(ServiceEndpoint endpoint)
        {
            return;
        }

        #endregion

    }
}